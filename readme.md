# Introduction

There are possible to build and run 5 types of containers from the box: local, dev, stage, test, prod. 

All apps will be available using *.test.com address where * is env name like dev.dp.com (you can change address in /nginx/laravel.conf if you wish). Prod env will be available from any other domain, because it marked as default_server in our ./nginx/laravel.conf file.

Note that's app path's just mounts to the local env container while for other containers docker just copies all app files. This trick with local containers is very useful for development.

# Running guide:

$ git clone git@bitbucket.org:treesoftrussia/digital-platform.git

$ cd /digital-platform/web

build and run 2 app containers with dev and local environments (remove dev compose file if you don't need it)

$ docker-compose -f docker-compose.yml -f docker-compose-dev.yml -f docker-compose-local.yml up -d nginx 

$ docker exec -it web-local bash

$ ./vendor/bin/phpunit

$ exit

if you want to run local environment container on your machine you should add line "127.0.0.1		local.dp" to your /etc/hosts file and access to the site through this domain.

Make sure that's your host machine contains SSH keys in ~/.ssh directory, because it's mounts to the ~/.ssh directory of containers which uses by GIT to access to the private repositories.
