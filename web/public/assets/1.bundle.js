webpackJsonp([1],[
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function (currentRoute) {
	    this.mediator = __webpack_require__(8)();
	    var routeRegisters = [];
	    this.registerRouter = function (routeRegisterCallback) {
	        routeRegisters.push(routeRegisterCallback);
	    };

	    this.route = function () {
	        routeRegisters.forEach(function (routeRegisterCallback) {
	            routeRegisterCallback(currentRoute);
	        });
	    };
	};

/***/ },
/* 6 */,
/* 7 */,
/* 8 */
/***/ function(module, exports) {

	module.exports = function () {
	    var mediator = function () {
	        var on = function (channel, fn) {
	            if (!mediator.channels[channel]) mediator.channels[channel] = [];
	            mediator.channels[channel].push({ context: this, callback: fn });
	            return this;
	        },
	            publish = function (channel) {
	            if (!mediator.channels[channel]) return false;
	            var args = Array.prototype.slice.call(arguments, 1);
	            for (var i = 0, l = mediator.channels[channel].length; i < l; i++) {
	                var subscription = mediator.channels[channel][i];
	                subscription.callback.apply(subscription.context, args);
	            }
	            return this;
	        };

	        return {
	            channels: {},
	            publish: publish,
	            on: on,
	            installTo: function (obj) {
	                obj.on = on;
	                obj.publish = publish;
	            }
	        };
	    }();

	    return mediator;
	};

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	var toastr = __webpack_require__(14);
	var c = __webpack_require__(7);

	module.exports = function (mediator) {

	    var opts = {
	        "closeButton": true,
	        "debug": false,
	        "positionClass": "toast-bottom-left",
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "timeOut": "10000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut"
	    };

	    mediator.on('showNotification', function (message, title, success) {

	        if (Array.isArray(message)) {
	            message = message.join('<br>');
	        }

	        if (success == undefined || success) {
	            toastr.success(message, title, opts);
	        } else {
	            toastr.warning(message, title, opts);
	        }
	    });

	    mediator.on('serverError', function (error) {
	        if (!error.errorData) {
	            toastr.warning('We had received nothing from the server (no errorData). So something going wrong.', null, opts);
	            return;
	        }

	        if (error.errorCode == c.PERMISSION_DENIED) {
	            toastr.info('Session was expired. You will redirected to the login page in 5 seconds.', null, opts);
	            setTimeout(function () {
	                location.href = '/login';
	            }, 5000);
	            return;
	        }

	        if (error.errorCode == c.ROUTE_NOT_FOUND) {
	            toastr.info('Requested item was already removed by someone else.', null, opts);
	            return;
	        }

	        if (Array.isArray(error.errorData)) {
	            //validation error, do nothing
	        } else if (error.errorData.message) {
	                toastr.warning(error.errorData.message, null, opts);
	            } else if (error.errorData) {
	                toastr.warning(error.errorData, null, opts);
	            }
	    });
	};

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	var DatatableModule = __webpack_require__(46);

	module.exports = function (params) {
	    var that = this;

	    this.all = function () {
	        var datatableModule = new DatatableModule(params['all-table-id']);
	    };

	    this.add = function () {};

	    this.edit = function () {};

	    function route(currentRoute) {
	        var currentRouteParts = currentRoute.split('/');

	        if (currentRouteParts[1] && currentRouteParts[1].length) {
	            switch (currentRouteParts[1]) {
	                case 'add':
	                    that.add();
	                    break;
	                case 'edit':
	                    that.edit(currentRouteParts[2]);
	                    break;
	            }
	        } else if (currentRouteParts[0] && currentRouteParts[0].length) {
	            that.all();
	        }
	    }

	    this.registerRouter(route);
	};

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	var toastr = __webpack_require__(14);
	var parsleyjs = __webpack_require__(86);
	__webpack_require__(87);
	var c = __webpack_require__(7);

	module.exports = function (id, selector, resourceWebURL, resourceAPIURL, mode, mediator) {
	    var that = this;

	    this.form = $(selector);
	    this.additionalFields = {};
	    this.handleErrorCallback = null;

	    this.form.parsley().on('field:validated', function () {}).on('form:submit', function (e) {
	        that.postData();
	        return false;
	    });

	    $('.reset-form', this.form).on('click', function (e) {
	        e.preventDefault();
	        if (confirm(that.form.data('reset-confirm'))) {
	            that.resetForm();
	        }
	    });

	    $.fn.serializeDisabled = function () {
	        var obj = {};

	        $.each(that.additionalFields, function (k, v) {
	            obj[k] = v;
	        });

	        $(':disabled[name]', this).each(function () {
	            obj[this.name] = $(this).val();
	        });
	        $(':enabled[name]', this).each(function () {
	            obj[this.name] = $(this).val();
	        });
	        return obj;
	    };

	    this.disableButtons = function () {
	        $('.form-buttons button', that.form).attr('disabled', true);
	    };

	    this.enableButtons = function () {
	        $('.form-buttons button', that.form).attr('disabled', false);
	    };

	    this.postData = function () {
	        that.disableButtons();
	        var data = that.form.serializeDisabled();
	        $.ajax({
	            method: mode == 'create' ? "POST" : "PUT",
	            url: resourceAPIURL + (mode == 'create' ? '' : '/' + id),
	            data: JSON.stringify(data),
	            contentType: "application/json"
	        }).done(function (entity) {
	            if (mode == 'create') {
	                mediator.publish('showNotification', that.form.data('create-alert'));

	                setTimeout(function () {
	                    location.href = resourceWebURL + '/edit/' + entity.id;
	                }, 3000);
	            } else {
	                mediator.publish('showNotification', that.form.data('update-alert'));

	                that.enableButtons();
	            }
	        }).error(function (err) {
	            that.handleFormError(err);
	            that.enableButtons();
	        });
	    };

	    this.setHandleErrorCallback = function (cb) {
	        that.handleErrorCallback = cb;
	    };

	    this.handleFormError = function (err) {
	        var body = err.responseJSON;

	        that.handleErrorCallback ? that.handleErrorCallback(body) : null;

	        switch (body.errorCode) {
	            case c.VALIDATION_ERROR:
	                //validation error
	                $.each(body.errorData, function (k, v) {
	                    var field = $('[name="' + k + '"]', that.form).parsley();
	                    window.ParsleyUI.removeError(field, k);
	                    window.ParsleyUI.addError(field, k, v);
	                    field.on('field:error', function () {
	                        window.ParsleyUI.removeError(field, k);
	                    });
	                });
	                break;
	            default:
	                mediator.publish('serverError', body, null, false);
	                break;
	        }
	    };

	    this.resetForm = function () {
	        $(':enabled[name]', that.form).each(function () {
	            $(this).val('');
	        });
	    };

	    this.addField = function (id, value) {
	        that.additionalFields[id] = value;
	    };
	};

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! DataTables 1.10.12
	 * ©2008-2015 SpryMedia Ltd - datatables.net/license
	 */

	/**
	 * @summary     DataTables
	 * @description Paginate, search and order HTML tables
	 * @version     1.10.12
	 * @file        jquery.dataTables.js
	 * @author      SpryMedia Ltd (www.sprymedia.co.uk)
	 * @contact     www.sprymedia.co.uk/contact
	 * @copyright   Copyright 2008-2015 SpryMedia Ltd.
	 *
	 * This source file is free software, available under the following license:
	 *   MIT license - http://datatables.net/license
	 *
	 * This source file is distributed in the hope that it will be useful, but
	 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
	 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
	 *
	 * For details please refer to: http://www.datatables.net
	 */

	/*jslint evil: true, undef: true, browser: true */
	/*globals $,require,jQuery,define,_selector_run,_selector_opts,_selector_first,_selector_row_indexes,_ext,_Api,_api_register,_api_registerPlural,_re_new_lines,_re_html,_re_formatted_numeric,_re_escape_regex,_empty,_intVal,_numToDecimal,_isNumber,_isHtml,_htmlNumeric,_pluck,_pluck_order,_range,_stripHtml,_unique,_fnBuildAjax,_fnAjaxUpdate,_fnAjaxParameters,_fnAjaxUpdateDraw,_fnAjaxDataSrc,_fnAddColumn,_fnColumnOptions,_fnAdjustColumnSizing,_fnVisibleToColumnIndex,_fnColumnIndexToVisible,_fnVisbleColumns,_fnGetColumns,_fnColumnTypes,_fnApplyColumnDefs,_fnHungarianMap,_fnCamelToHungarian,_fnLanguageCompat,_fnBrowserDetect,_fnAddData,_fnAddTr,_fnNodeToDataIndex,_fnNodeToColumnIndex,_fnGetCellData,_fnSetCellData,_fnSplitObjNotation,_fnGetObjectDataFn,_fnSetObjectDataFn,_fnGetDataMaster,_fnClearTable,_fnDeleteIndex,_fnInvalidate,_fnGetRowElements,_fnCreateTr,_fnBuildHead,_fnDrawHead,_fnDraw,_fnReDraw,_fnAddOptionsHtml,_fnDetectHeader,_fnGetUniqueThs,_fnFeatureHtmlFilter,_fnFilterComplete,_fnFilterCustom,_fnFilterColumn,_fnFilter,_fnFilterCreateSearch,_fnEscapeRegex,_fnFilterData,_fnFeatureHtmlInfo,_fnUpdateInfo,_fnInfoMacros,_fnInitialise,_fnInitComplete,_fnLengthChange,_fnFeatureHtmlLength,_fnFeatureHtmlPaginate,_fnPageChange,_fnFeatureHtmlProcessing,_fnProcessingDisplay,_fnFeatureHtmlTable,_fnScrollDraw,_fnApplyToChildren,_fnCalculateColumnWidths,_fnThrottle,_fnConvertToWidth,_fnGetWidestNode,_fnGetMaxLenString,_fnStringToCss,_fnSortFlatten,_fnSort,_fnSortAria,_fnSortListener,_fnSortAttachListener,_fnSortingClasses,_fnSortData,_fnSaveState,_fnLoadState,_fnSettingsFromNode,_fnLog,_fnMap,_fnBindAction,_fnCallbackReg,_fnCallbackFire,_fnLengthOverflow,_fnRenderer,_fnDataSource,_fnRowAttributes*/

	(function( factory ) {
		"use strict";

		if ( true ) {
			// AMD
			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1)], __WEBPACK_AMD_DEFINE_RESULT__ = function ( $ ) {
				return factory( $, window, document );
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		}
		else if ( typeof exports === 'object' ) {
			// CommonJS
			module.exports = function (root, $) {
				if ( ! root ) {
					// CommonJS environments without a window global must pass a
					// root. This will give an error otherwise
					root = window;
				}

				if ( ! $ ) {
					$ = typeof window !== 'undefined' ? // jQuery's factory checks for a global window
						require('jquery') :
						require('jquery')( root );
				}

				return factory( $, root, root.document );
			};
		}
		else {
			// Browser
			factory( jQuery, window, document );
		}
	}
	(function( $, window, document, undefined ) {
		"use strict";

		/**
		 * DataTables is a plug-in for the jQuery Javascript library. It is a highly
		 * flexible tool, based upon the foundations of progressive enhancement,
		 * which will add advanced interaction controls to any HTML table. For a
		 * full list of features please refer to
		 * [DataTables.net](href="http://datatables.net).
		 *
		 * Note that the `DataTable` object is not a global variable but is aliased
		 * to `jQuery.fn.DataTable` and `jQuery.fn.dataTable` through which it may
		 * be  accessed.
		 *
		 *  @class
		 *  @param {object} [init={}] Configuration object for DataTables. Options
		 *    are defined by {@link DataTable.defaults}
		 *  @requires jQuery 1.7+
		 *
		 *  @example
		 *    // Basic initialisation
		 *    $(document).ready( function {
		 *      $('#example').dataTable();
		 *    } );
		 *
		 *  @example
		 *    // Initialisation with configuration options - in this case, disable
		 *    // pagination and sorting.
		 *    $(document).ready( function {
		 *      $('#example').dataTable( {
		 *        "paginate": false,
		 *        "sort": false
		 *      } );
		 *    } );
		 */
		var DataTable = function ( options )
		{
			/**
			 * Perform a jQuery selector action on the table's TR elements (from the tbody) and
			 * return the resulting jQuery object.
			 *  @param {string|node|jQuery} sSelector jQuery selector or node collection to act on
			 *  @param {object} [oOpts] Optional parameters for modifying the rows to be included
			 *  @param {string} [oOpts.filter=none] Select TR elements that meet the current filter
			 *    criterion ("applied") or all TR elements (i.e. no filter).
			 *  @param {string} [oOpts.order=current] Order of the TR elements in the processed array.
			 *    Can be either 'current', whereby the current sorting of the table is used, or
			 *    'original' whereby the original order the data was read into the table is used.
			 *  @param {string} [oOpts.page=all] Limit the selection to the currently displayed page
			 *    ("current") or not ("all"). If 'current' is given, then order is assumed to be
			 *    'current' and filter is 'applied', regardless of what they might be given as.
			 *  @returns {object} jQuery object, filtered by the given selector.
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Highlight every second row
			 *      oTable.$('tr:odd').css('backgroundColor', 'blue');
			 *    } );
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Filter to rows with 'Webkit' in them, add a background colour and then
			 *      // remove the filter, thus highlighting the 'Webkit' rows only.
			 *      oTable.fnFilter('Webkit');
			 *      oTable.$('tr', {"search": "applied"}).css('backgroundColor', 'blue');
			 *      oTable.fnFilter('');
			 *    } );
			 */
			this.$ = function ( sSelector, oOpts )
			{
				return this.api(true).$( sSelector, oOpts );
			};
			
			
			/**
			 * Almost identical to $ in operation, but in this case returns the data for the matched
			 * rows - as such, the jQuery selector used should match TR row nodes or TD/TH cell nodes
			 * rather than any descendants, so the data can be obtained for the row/cell. If matching
			 * rows are found, the data returned is the original data array/object that was used to
			 * create the row (or a generated array if from a DOM source).
			 *
			 * This method is often useful in-combination with $ where both functions are given the
			 * same parameters and the array indexes will match identically.
			 *  @param {string|node|jQuery} sSelector jQuery selector or node collection to act on
			 *  @param {object} [oOpts] Optional parameters for modifying the rows to be included
			 *  @param {string} [oOpts.filter=none] Select elements that meet the current filter
			 *    criterion ("applied") or all elements (i.e. no filter).
			 *  @param {string} [oOpts.order=current] Order of the data in the processed array.
			 *    Can be either 'current', whereby the current sorting of the table is used, or
			 *    'original' whereby the original order the data was read into the table is used.
			 *  @param {string} [oOpts.page=all] Limit the selection to the currently displayed page
			 *    ("current") or not ("all"). If 'current' is given, then order is assumed to be
			 *    'current' and filter is 'applied', regardless of what they might be given as.
			 *  @returns {array} Data for the matched elements. If any elements, as a result of the
			 *    selector, were not TR, TD or TH elements in the DataTable, they will have a null
			 *    entry in the array.
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Get the data from the first row in the table
			 *      var data = oTable._('tr:first');
			 *
			 *      // Do something useful with the data
			 *      alert( "First cell is: "+data[0] );
			 *    } );
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Filter to 'Webkit' and get all data for
			 *      oTable.fnFilter('Webkit');
			 *      var data = oTable._('tr', {"search": "applied"});
			 *
			 *      // Do something with the data
			 *      alert( data.length+" rows matched the search" );
			 *    } );
			 */
			this._ = function ( sSelector, oOpts )
			{
				return this.api(true).rows( sSelector, oOpts ).data();
			};
			
			
			/**
			 * Create a DataTables Api instance, with the currently selected tables for
			 * the Api's context.
			 * @param {boolean} [traditional=false] Set the API instance's context to be
			 *   only the table referred to by the `DataTable.ext.iApiIndex` option, as was
			 *   used in the API presented by DataTables 1.9- (i.e. the traditional mode),
			 *   or if all tables captured in the jQuery object should be used.
			 * @return {DataTables.Api}
			 */
			this.api = function ( traditional )
			{
				return traditional ?
					new _Api(
						_fnSettingsFromNode( this[ _ext.iApiIndex ] )
					) :
					new _Api( this );
			};
			
			
			/**
			 * Add a single new row or multiple rows of data to the table. Please note
			 * that this is suitable for client-side processing only - if you are using
			 * server-side processing (i.e. "bServerSide": true), then to add data, you
			 * must add it to the data source, i.e. the server-side, through an Ajax call.
			 *  @param {array|object} data The data to be added to the table. This can be:
			 *    <ul>
			 *      <li>1D array of data - add a single row with the data provided</li>
			 *      <li>2D array of arrays - add multiple rows in a single call</li>
			 *      <li>object - data object when using <i>mData</i></li>
			 *      <li>array of objects - multiple data objects when using <i>mData</i></li>
			 *    </ul>
			 *  @param {bool} [redraw=true] redraw the table or not
			 *  @returns {array} An array of integers, representing the list of indexes in
			 *    <i>aoData</i> ({@link DataTable.models.oSettings}) that have been added to
			 *    the table.
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    // Global var for counter
			 *    var giCount = 2;
			 *
			 *    $(document).ready(function() {
			 *      $('#example').dataTable();
			 *    } );
			 *
			 *    function fnClickAddRow() {
			 *      $('#example').dataTable().fnAddData( [
			 *        giCount+".1",
			 *        giCount+".2",
			 *        giCount+".3",
			 *        giCount+".4" ]
			 *      );
			 *
			 *      giCount++;
			 *    }
			 */
			this.fnAddData = function( data, redraw )
			{
				var api = this.api( true );
			
				/* Check if we want to add multiple rows or not */
				var rows = $.isArray(data) && ( $.isArray(data[0]) || $.isPlainObject(data[0]) ) ?
					api.rows.add( data ) :
					api.row.add( data );
			
				if ( redraw === undefined || redraw ) {
					api.draw();
				}
			
				return rows.flatten().toArray();
			};
			
			
			/**
			 * This function will make DataTables recalculate the column sizes, based on the data
			 * contained in the table and the sizes applied to the columns (in the DOM, CSS or
			 * through the sWidth parameter). This can be useful when the width of the table's
			 * parent element changes (for example a window resize).
			 *  @param {boolean} [bRedraw=true] Redraw the table or not, you will typically want to
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable( {
			 *        "sScrollY": "200px",
			 *        "bPaginate": false
			 *      } );
			 *
			 *      $(window).bind('resize', function () {
			 *        oTable.fnAdjustColumnSizing();
			 *      } );
			 *    } );
			 */
			this.fnAdjustColumnSizing = function ( bRedraw )
			{
				var api = this.api( true ).columns.adjust();
				var settings = api.settings()[0];
				var scroll = settings.oScroll;
			
				if ( bRedraw === undefined || bRedraw ) {
					api.draw( false );
				}
				else if ( scroll.sX !== "" || scroll.sY !== "" ) {
					/* If not redrawing, but scrolling, we want to apply the new column sizes anyway */
					_fnScrollDraw( settings );
				}
			};
			
			
			/**
			 * Quickly and simply clear a table
			 *  @param {bool} [bRedraw=true] redraw the table or not
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Immediately 'nuke' the current rows (perhaps waiting for an Ajax callback...)
			 *      oTable.fnClearTable();
			 *    } );
			 */
			this.fnClearTable = function( bRedraw )
			{
				var api = this.api( true ).clear();
			
				if ( bRedraw === undefined || bRedraw ) {
					api.draw();
				}
			};
			
			
			/**
			 * The exact opposite of 'opening' a row, this function will close any rows which
			 * are currently 'open'.
			 *  @param {node} nTr the table row to 'close'
			 *  @returns {int} 0 on success, or 1 if failed (can't find the row)
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable;
			 *
			 *      // 'open' an information row when a row is clicked on
			 *      $('#example tbody tr').click( function () {
			 *        if ( oTable.fnIsOpen(this) ) {
			 *          oTable.fnClose( this );
			 *        } else {
			 *          oTable.fnOpen( this, "Temporary row opened", "info_row" );
			 *        }
			 *      } );
			 *
			 *      oTable = $('#example').dataTable();
			 *    } );
			 */
			this.fnClose = function( nTr )
			{
				this.api( true ).row( nTr ).child.hide();
			};
			
			
			/**
			 * Remove a row for the table
			 *  @param {mixed} target The index of the row from aoData to be deleted, or
			 *    the TR element you want to delete
			 *  @param {function|null} [callBack] Callback function
			 *  @param {bool} [redraw=true] Redraw the table or not
			 *  @returns {array} The row that was deleted
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Immediately remove the first row
			 *      oTable.fnDeleteRow( 0 );
			 *    } );
			 */
			this.fnDeleteRow = function( target, callback, redraw )
			{
				var api = this.api( true );
				var rows = api.rows( target );
				var settings = rows.settings()[0];
				var data = settings.aoData[ rows[0][0] ];
			
				rows.remove();
			
				if ( callback ) {
					callback.call( this, settings, data );
				}
			
				if ( redraw === undefined || redraw ) {
					api.draw();
				}
			
				return data;
			};
			
			
			/**
			 * Restore the table to it's original state in the DOM by removing all of DataTables
			 * enhancements, alterations to the DOM structure of the table and event listeners.
			 *  @param {boolean} [remove=false] Completely remove the table from the DOM
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      // This example is fairly pointless in reality, but shows how fnDestroy can be used
			 *      var oTable = $('#example').dataTable();
			 *      oTable.fnDestroy();
			 *    } );
			 */
			this.fnDestroy = function ( remove )
			{
				this.api( true ).destroy( remove );
			};
			
			
			/**
			 * Redraw the table
			 *  @param {bool} [complete=true] Re-filter and resort (if enabled) the table before the draw.
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Re-draw the table - you wouldn't want to do it here, but it's an example :-)
			 *      oTable.fnDraw();
			 *    } );
			 */
			this.fnDraw = function( complete )
			{
				// Note that this isn't an exact match to the old call to _fnDraw - it takes
				// into account the new data, but can hold position.
				this.api( true ).draw( complete );
			};
			
			
			/**
			 * Filter the input based on data
			 *  @param {string} sInput String to filter the table on
			 *  @param {int|null} [iColumn] Column to limit filtering to
			 *  @param {bool} [bRegex=false] Treat as regular expression or not
			 *  @param {bool} [bSmart=true] Perform smart filtering or not
			 *  @param {bool} [bShowGlobal=true] Show the input global filter in it's input box(es)
			 *  @param {bool} [bCaseInsensitive=true] Do case-insensitive matching (true) or not (false)
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Sometime later - filter...
			 *      oTable.fnFilter( 'test string' );
			 *    } );
			 */
			this.fnFilter = function( sInput, iColumn, bRegex, bSmart, bShowGlobal, bCaseInsensitive )
			{
				var api = this.api( true );
			
				if ( iColumn === null || iColumn === undefined ) {
					api.search( sInput, bRegex, bSmart, bCaseInsensitive );
				}
				else {
					api.column( iColumn ).search( sInput, bRegex, bSmart, bCaseInsensitive );
				}
			
				api.draw();
			};
			
			
			/**
			 * Get the data for the whole table, an individual row or an individual cell based on the
			 * provided parameters.
			 *  @param {int|node} [src] A TR row node, TD/TH cell node or an integer. If given as
			 *    a TR node then the data source for the whole row will be returned. If given as a
			 *    TD/TH cell node then iCol will be automatically calculated and the data for the
			 *    cell returned. If given as an integer, then this is treated as the aoData internal
			 *    data index for the row (see fnGetPosition) and the data for that row used.
			 *  @param {int} [col] Optional column index that you want the data of.
			 *  @returns {array|object|string} If mRow is undefined, then the data for all rows is
			 *    returned. If mRow is defined, just data for that row, and is iCol is
			 *    defined, only data for the designated cell is returned.
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    // Row data
			 *    $(document).ready(function() {
			 *      oTable = $('#example').dataTable();
			 *
			 *      oTable.$('tr').click( function () {
			 *        var data = oTable.fnGetData( this );
			 *        // ... do something with the array / object of data for the row
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Individual cell data
			 *    $(document).ready(function() {
			 *      oTable = $('#example').dataTable();
			 *
			 *      oTable.$('td').click( function () {
			 *        var sData = oTable.fnGetData( this );
			 *        alert( 'The cell clicked on had the value of '+sData );
			 *      } );
			 *    } );
			 */
			this.fnGetData = function( src, col )
			{
				var api = this.api( true );
			
				if ( src !== undefined ) {
					var type = src.nodeName ? src.nodeName.toLowerCase() : '';
			
					return col !== undefined || type == 'td' || type == 'th' ?
						api.cell( src, col ).data() :
						api.row( src ).data() || null;
				}
			
				return api.data().toArray();
			};
			
			
			/**
			 * Get an array of the TR nodes that are used in the table's body. Note that you will
			 * typically want to use the '$' API method in preference to this as it is more
			 * flexible.
			 *  @param {int} [iRow] Optional row index for the TR element you want
			 *  @returns {array|node} If iRow is undefined, returns an array of all TR elements
			 *    in the table's body, or iRow is defined, just the TR element requested.
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Get the nodes from the table
			 *      var nNodes = oTable.fnGetNodes( );
			 *    } );
			 */
			this.fnGetNodes = function( iRow )
			{
				var api = this.api( true );
			
				return iRow !== undefined ?
					api.row( iRow ).node() :
					api.rows().nodes().flatten().toArray();
			};
			
			
			/**
			 * Get the array indexes of a particular cell from it's DOM element
			 * and column index including hidden columns
			 *  @param {node} node this can either be a TR, TD or TH in the table's body
			 *  @returns {int} If nNode is given as a TR, then a single index is returned, or
			 *    if given as a cell, an array of [row index, column index (visible),
			 *    column index (all)] is given.
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      $('#example tbody td').click( function () {
			 *        // Get the position of the current data from the node
			 *        var aPos = oTable.fnGetPosition( this );
			 *
			 *        // Get the data array for this row
			 *        var aData = oTable.fnGetData( aPos[0] );
			 *
			 *        // Update the data array and return the value
			 *        aData[ aPos[1] ] = 'clicked';
			 *        this.innerHTML = 'clicked';
			 *      } );
			 *
			 *      // Init DataTables
			 *      oTable = $('#example').dataTable();
			 *    } );
			 */
			this.fnGetPosition = function( node )
			{
				var api = this.api( true );
				var nodeName = node.nodeName.toUpperCase();
			
				if ( nodeName == 'TR' ) {
					return api.row( node ).index();
				}
				else if ( nodeName == 'TD' || nodeName == 'TH' ) {
					var cell = api.cell( node ).index();
			
					return [
						cell.row,
						cell.columnVisible,
						cell.column
					];
				}
				return null;
			};
			
			
			/**
			 * Check to see if a row is 'open' or not.
			 *  @param {node} nTr the table row to check
			 *  @returns {boolean} true if the row is currently open, false otherwise
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable;
			 *
			 *      // 'open' an information row when a row is clicked on
			 *      $('#example tbody tr').click( function () {
			 *        if ( oTable.fnIsOpen(this) ) {
			 *          oTable.fnClose( this );
			 *        } else {
			 *          oTable.fnOpen( this, "Temporary row opened", "info_row" );
			 *        }
			 *      } );
			 *
			 *      oTable = $('#example').dataTable();
			 *    } );
			 */
			this.fnIsOpen = function( nTr )
			{
				return this.api( true ).row( nTr ).child.isShown();
			};
			
			
			/**
			 * This function will place a new row directly after a row which is currently
			 * on display on the page, with the HTML contents that is passed into the
			 * function. This can be used, for example, to ask for confirmation that a
			 * particular record should be deleted.
			 *  @param {node} nTr The table row to 'open'
			 *  @param {string|node|jQuery} mHtml The HTML to put into the row
			 *  @param {string} sClass Class to give the new TD cell
			 *  @returns {node} The row opened. Note that if the table row passed in as the
			 *    first parameter, is not found in the table, this method will silently
			 *    return.
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable;
			 *
			 *      // 'open' an information row when a row is clicked on
			 *      $('#example tbody tr').click( function () {
			 *        if ( oTable.fnIsOpen(this) ) {
			 *          oTable.fnClose( this );
			 *        } else {
			 *          oTable.fnOpen( this, "Temporary row opened", "info_row" );
			 *        }
			 *      } );
			 *
			 *      oTable = $('#example').dataTable();
			 *    } );
			 */
			this.fnOpen = function( nTr, mHtml, sClass )
			{
				return this.api( true )
					.row( nTr )
					.child( mHtml, sClass )
					.show()
					.child()[0];
			};
			
			
			/**
			 * Change the pagination - provides the internal logic for pagination in a simple API
			 * function. With this function you can have a DataTables table go to the next,
			 * previous, first or last pages.
			 *  @param {string|int} mAction Paging action to take: "first", "previous", "next" or "last"
			 *    or page number to jump to (integer), note that page 0 is the first page.
			 *  @param {bool} [bRedraw=true] Redraw the table or not
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *      oTable.fnPageChange( 'next' );
			 *    } );
			 */
			this.fnPageChange = function ( mAction, bRedraw )
			{
				var api = this.api( true ).page( mAction );
			
				if ( bRedraw === undefined || bRedraw ) {
					api.draw(false);
				}
			};
			
			
			/**
			 * Show a particular column
			 *  @param {int} iCol The column whose display should be changed
			 *  @param {bool} bShow Show (true) or hide (false) the column
			 *  @param {bool} [bRedraw=true] Redraw the table or not
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Hide the second column after initialisation
			 *      oTable.fnSetColumnVis( 1, false );
			 *    } );
			 */
			this.fnSetColumnVis = function ( iCol, bShow, bRedraw )
			{
				var api = this.api( true ).column( iCol ).visible( bShow );
			
				if ( bRedraw === undefined || bRedraw ) {
					api.columns.adjust().draw();
				}
			};
			
			
			/**
			 * Get the settings for a particular table for external manipulation
			 *  @returns {object} DataTables settings object. See
			 *    {@link DataTable.models.oSettings}
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *      var oSettings = oTable.fnSettings();
			 *
			 *      // Show an example parameter from the settings
			 *      alert( oSettings._iDisplayStart );
			 *    } );
			 */
			this.fnSettings = function()
			{
				return _fnSettingsFromNode( this[_ext.iApiIndex] );
			};
			
			
			/**
			 * Sort the table by a particular column
			 *  @param {int} iCol the data index to sort on. Note that this will not match the
			 *    'display index' if you have hidden data entries
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Sort immediately with columns 0 and 1
			 *      oTable.fnSort( [ [0,'asc'], [1,'asc'] ] );
			 *    } );
			 */
			this.fnSort = function( aaSort )
			{
				this.api( true ).order( aaSort ).draw();
			};
			
			
			/**
			 * Attach a sort listener to an element for a given column
			 *  @param {node} nNode the element to attach the sort listener to
			 *  @param {int} iColumn the column that a click on this node will sort on
			 *  @param {function} [fnCallback] callback function when sort is run
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *
			 *      // Sort on column 1, when 'sorter' is clicked on
			 *      oTable.fnSortListener( document.getElementById('sorter'), 1 );
			 *    } );
			 */
			this.fnSortListener = function( nNode, iColumn, fnCallback )
			{
				this.api( true ).order.listener( nNode, iColumn, fnCallback );
			};
			
			
			/**
			 * Update a table cell or row - this method will accept either a single value to
			 * update the cell with, an array of values with one element for each column or
			 * an object in the same format as the original data source. The function is
			 * self-referencing in order to make the multi column updates easier.
			 *  @param {object|array|string} mData Data to update the cell/row with
			 *  @param {node|int} mRow TR element you want to update or the aoData index
			 *  @param {int} [iColumn] The column to update, give as null or undefined to
			 *    update a whole row.
			 *  @param {bool} [bRedraw=true] Redraw the table or not
			 *  @param {bool} [bAction=true] Perform pre-draw actions or not
			 *  @returns {int} 0 on success, 1 on error
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *      oTable.fnUpdate( 'Example update', 0, 0 ); // Single cell
			 *      oTable.fnUpdate( ['a', 'b', 'c', 'd', 'e'], $('tbody tr')[0] ); // Row
			 *    } );
			 */
			this.fnUpdate = function( mData, mRow, iColumn, bRedraw, bAction )
			{
				var api = this.api( true );
			
				if ( iColumn === undefined || iColumn === null ) {
					api.row( mRow ).data( mData );
				}
				else {
					api.cell( mRow, iColumn ).data( mData );
				}
			
				if ( bAction === undefined || bAction ) {
					api.columns.adjust();
				}
			
				if ( bRedraw === undefined || bRedraw ) {
					api.draw();
				}
				return 0;
			};
			
			
			/**
			 * Provide a common method for plug-ins to check the version of DataTables being used, in order
			 * to ensure compatibility.
			 *  @param {string} sVersion Version string to check for, in the format "X.Y.Z". Note that the
			 *    formats "X" and "X.Y" are also acceptable.
			 *  @returns {boolean} true if this version of DataTables is greater or equal to the required
			 *    version, or false if this version of DataTales is not suitable
			 *  @method
			 *  @dtopt API
			 *  @deprecated Since v1.10
			 *
			 *  @example
			 *    $(document).ready(function() {
			 *      var oTable = $('#example').dataTable();
			 *      alert( oTable.fnVersionCheck( '1.9.0' ) );
			 *    } );
			 */
			this.fnVersionCheck = _ext.fnVersionCheck;
			

			var _that = this;
			var emptyInit = options === undefined;
			var len = this.length;

			if ( emptyInit ) {
				options = {};
			}

			this.oApi = this.internal = _ext.internal;

			// Extend with old style plug-in API methods
			for ( var fn in DataTable.ext.internal ) {
				if ( fn ) {
					this[fn] = _fnExternApiFunc(fn);
				}
			}

			this.each(function() {
				// For each initialisation we want to give it a clean initialisation
				// object that can be bashed around
				var o = {};
				var oInit = len > 1 ? // optimisation for single table case
					_fnExtend( o, options, true ) :
					options;

				/*global oInit,_that,emptyInit*/
				var i=0, iLen, j, jLen, k, kLen;
				var sId = this.getAttribute( 'id' );
				var bInitHandedOff = false;
				var defaults = DataTable.defaults;
				var $this = $(this);
				
				
				/* Sanity check */
				if ( this.nodeName.toLowerCase() != 'table' )
				{
					_fnLog( null, 0, 'Non-table node initialisation ('+this.nodeName+')', 2 );
					return;
				}
				
				/* Backwards compatibility for the defaults */
				_fnCompatOpts( defaults );
				_fnCompatCols( defaults.column );
				
				/* Convert the camel-case defaults to Hungarian */
				_fnCamelToHungarian( defaults, defaults, true );
				_fnCamelToHungarian( defaults.column, defaults.column, true );
				
				/* Setting up the initialisation object */
				_fnCamelToHungarian( defaults, $.extend( oInit, $this.data() ) );
				
				
				
				/* Check to see if we are re-initialising a table */
				var allSettings = DataTable.settings;
				for ( i=0, iLen=allSettings.length ; i<iLen ; i++ )
				{
					var s = allSettings[i];
				
					/* Base check on table node */
					if ( s.nTable == this || s.nTHead.parentNode == this || (s.nTFoot && s.nTFoot.parentNode == this) )
					{
						var bRetrieve = oInit.bRetrieve !== undefined ? oInit.bRetrieve : defaults.bRetrieve;
						var bDestroy = oInit.bDestroy !== undefined ? oInit.bDestroy : defaults.bDestroy;
				
						if ( emptyInit || bRetrieve )
						{
							return s.oInstance;
						}
						else if ( bDestroy )
						{
							s.oInstance.fnDestroy();
							break;
						}
						else
						{
							_fnLog( s, 0, 'Cannot reinitialise DataTable', 3 );
							return;
						}
					}
				
					/* If the element we are initialising has the same ID as a table which was previously
					 * initialised, but the table nodes don't match (from before) then we destroy the old
					 * instance by simply deleting it. This is under the assumption that the table has been
					 * destroyed by other methods. Anyone using non-id selectors will need to do this manually
					 */
					if ( s.sTableId == this.id )
					{
						allSettings.splice( i, 1 );
						break;
					}
				}
				
				/* Ensure the table has an ID - required for accessibility */
				if ( sId === null || sId === "" )
				{
					sId = "DataTables_Table_"+(DataTable.ext._unique++);
					this.id = sId;
				}
				
				/* Create the settings object for this table and set some of the default parameters */
				var oSettings = $.extend( true, {}, DataTable.models.oSettings, {
					"sDestroyWidth": $this[0].style.width,
					"sInstance":     sId,
					"sTableId":      sId
				} );
				oSettings.nTable = this;
				oSettings.oApi   = _that.internal;
				oSettings.oInit  = oInit;
				
				allSettings.push( oSettings );
				
				// Need to add the instance after the instance after the settings object has been added
				// to the settings array, so we can self reference the table instance if more than one
				oSettings.oInstance = (_that.length===1) ? _that : $this.dataTable();
				
				// Backwards compatibility, before we apply all the defaults
				_fnCompatOpts( oInit );
				
				if ( oInit.oLanguage )
				{
					_fnLanguageCompat( oInit.oLanguage );
				}
				
				// If the length menu is given, but the init display length is not, use the length menu
				if ( oInit.aLengthMenu && ! oInit.iDisplayLength )
				{
					oInit.iDisplayLength = $.isArray( oInit.aLengthMenu[0] ) ?
						oInit.aLengthMenu[0][0] : oInit.aLengthMenu[0];
				}
				
				// Apply the defaults and init options to make a single init object will all
				// options defined from defaults and instance options.
				oInit = _fnExtend( $.extend( true, {}, defaults ), oInit );
				
				
				// Map the initialisation options onto the settings object
				_fnMap( oSettings.oFeatures, oInit, [
					"bPaginate",
					"bLengthChange",
					"bFilter",
					"bSort",
					"bSortMulti",
					"bInfo",
					"bProcessing",
					"bAutoWidth",
					"bSortClasses",
					"bServerSide",
					"bDeferRender"
				] );
				_fnMap( oSettings, oInit, [
					"asStripeClasses",
					"ajax",
					"fnServerData",
					"fnFormatNumber",
					"sServerMethod",
					"aaSorting",
					"aaSortingFixed",
					"aLengthMenu",
					"sPaginationType",
					"sAjaxSource",
					"sAjaxDataProp",
					"iStateDuration",
					"sDom",
					"bSortCellsTop",
					"iTabIndex",
					"fnStateLoadCallback",
					"fnStateSaveCallback",
					"renderer",
					"searchDelay",
					"rowId",
					[ "iCookieDuration", "iStateDuration" ], // backwards compat
					[ "oSearch", "oPreviousSearch" ],
					[ "aoSearchCols", "aoPreSearchCols" ],
					[ "iDisplayLength", "_iDisplayLength" ],
					[ "bJQueryUI", "bJUI" ]
				] );
				_fnMap( oSettings.oScroll, oInit, [
					[ "sScrollX", "sX" ],
					[ "sScrollXInner", "sXInner" ],
					[ "sScrollY", "sY" ],
					[ "bScrollCollapse", "bCollapse" ]
				] );
				_fnMap( oSettings.oLanguage, oInit, "fnInfoCallback" );
				
				/* Callback functions which are array driven */
				_fnCallbackReg( oSettings, 'aoDrawCallback',       oInit.fnDrawCallback,      'user' );
				_fnCallbackReg( oSettings, 'aoServerParams',       oInit.fnServerParams,      'user' );
				_fnCallbackReg( oSettings, 'aoStateSaveParams',    oInit.fnStateSaveParams,   'user' );
				_fnCallbackReg( oSettings, 'aoStateLoadParams',    oInit.fnStateLoadParams,   'user' );
				_fnCallbackReg( oSettings, 'aoStateLoaded',        oInit.fnStateLoaded,       'user' );
				_fnCallbackReg( oSettings, 'aoRowCallback',        oInit.fnRowCallback,       'user' );
				_fnCallbackReg( oSettings, 'aoRowCreatedCallback', oInit.fnCreatedRow,        'user' );
				_fnCallbackReg( oSettings, 'aoHeaderCallback',     oInit.fnHeaderCallback,    'user' );
				_fnCallbackReg( oSettings, 'aoFooterCallback',     oInit.fnFooterCallback,    'user' );
				_fnCallbackReg( oSettings, 'aoInitComplete',       oInit.fnInitComplete,      'user' );
				_fnCallbackReg( oSettings, 'aoPreDrawCallback',    oInit.fnPreDrawCallback,   'user' );
				
				oSettings.rowIdFn = _fnGetObjectDataFn( oInit.rowId );
				
				/* Browser support detection */
				_fnBrowserDetect( oSettings );
				
				var oClasses = oSettings.oClasses;
				
				// @todo Remove in 1.11
				if ( oInit.bJQueryUI )
				{
					/* Use the JUI classes object for display. You could clone the oStdClasses object if
					 * you want to have multiple tables with multiple independent classes
					 */
					$.extend( oClasses, DataTable.ext.oJUIClasses, oInit.oClasses );
				
					if ( oInit.sDom === defaults.sDom && defaults.sDom === "lfrtip" )
					{
						/* Set the DOM to use a layout suitable for jQuery UI's theming */
						oSettings.sDom = '<"H"lfr>t<"F"ip>';
					}
				
					if ( ! oSettings.renderer ) {
						oSettings.renderer = 'jqueryui';
					}
					else if ( $.isPlainObject( oSettings.renderer ) && ! oSettings.renderer.header ) {
						oSettings.renderer.header = 'jqueryui';
					}
				}
				else
				{
					$.extend( oClasses, DataTable.ext.classes, oInit.oClasses );
				}
				$this.addClass( oClasses.sTable );
				
				
				if ( oSettings.iInitDisplayStart === undefined )
				{
					/* Display start point, taking into account the save saving */
					oSettings.iInitDisplayStart = oInit.iDisplayStart;
					oSettings._iDisplayStart = oInit.iDisplayStart;
				}
				
				if ( oInit.iDeferLoading !== null )
				{
					oSettings.bDeferLoading = true;
					var tmp = $.isArray( oInit.iDeferLoading );
					oSettings._iRecordsDisplay = tmp ? oInit.iDeferLoading[0] : oInit.iDeferLoading;
					oSettings._iRecordsTotal = tmp ? oInit.iDeferLoading[1] : oInit.iDeferLoading;
				}
				
				/* Language definitions */
				var oLanguage = oSettings.oLanguage;
				$.extend( true, oLanguage, oInit.oLanguage );
				
				if ( oLanguage.sUrl !== "" )
				{
					/* Get the language definitions from a file - because this Ajax call makes the language
					 * get async to the remainder of this function we use bInitHandedOff to indicate that
					 * _fnInitialise will be fired by the returned Ajax handler, rather than the constructor
					 */
					$.ajax( {
						dataType: 'json',
						url: oLanguage.sUrl,
						success: function ( json ) {
							_fnLanguageCompat( json );
							_fnCamelToHungarian( defaults.oLanguage, json );
							$.extend( true, oLanguage, json );
							_fnInitialise( oSettings );
						},
						error: function () {
							// Error occurred loading language file, continue on as best we can
							_fnInitialise( oSettings );
						}
					} );
					bInitHandedOff = true;
				}
				
				/*
				 * Stripes
				 */
				if ( oInit.asStripeClasses === null )
				{
					oSettings.asStripeClasses =[
						oClasses.sStripeOdd,
						oClasses.sStripeEven
					];
				}
				
				/* Remove row stripe classes if they are already on the table row */
				var stripeClasses = oSettings.asStripeClasses;
				var rowOne = $this.children('tbody').find('tr').eq(0);
				if ( $.inArray( true, $.map( stripeClasses, function(el, i) {
					return rowOne.hasClass(el);
				} ) ) !== -1 ) {
					$('tbody tr', this).removeClass( stripeClasses.join(' ') );
					oSettings.asDestroyStripes = stripeClasses.slice();
				}
				
				/*
				 * Columns
				 * See if we should load columns automatically or use defined ones
				 */
				var anThs = [];
				var aoColumnsInit;
				var nThead = this.getElementsByTagName('thead');
				if ( nThead.length !== 0 )
				{
					_fnDetectHeader( oSettings.aoHeader, nThead[0] );
					anThs = _fnGetUniqueThs( oSettings );
				}
				
				/* If not given a column array, generate one with nulls */
				if ( oInit.aoColumns === null )
				{
					aoColumnsInit = [];
					for ( i=0, iLen=anThs.length ; i<iLen ; i++ )
					{
						aoColumnsInit.push( null );
					}
				}
				else
				{
					aoColumnsInit = oInit.aoColumns;
				}
				
				/* Add the columns */
				for ( i=0, iLen=aoColumnsInit.length ; i<iLen ; i++ )
				{
					_fnAddColumn( oSettings, anThs ? anThs[i] : null );
				}
				
				/* Apply the column definitions */
				_fnApplyColumnDefs( oSettings, oInit.aoColumnDefs, aoColumnsInit, function (iCol, oDef) {
					_fnColumnOptions( oSettings, iCol, oDef );
				} );
				
				/* HTML5 attribute detection - build an mData object automatically if the
				 * attributes are found
				 */
				if ( rowOne.length ) {
					var a = function ( cell, name ) {
						return cell.getAttribute( 'data-'+name ) !== null ? name : null;
					};
				
					$( rowOne[0] ).children('th, td').each( function (i, cell) {
						var col = oSettings.aoColumns[i];
				
						if ( col.mData === i ) {
							var sort = a( cell, 'sort' ) || a( cell, 'order' );
							var filter = a( cell, 'filter' ) || a( cell, 'search' );
				
							if ( sort !== null || filter !== null ) {
								col.mData = {
									_:      i+'.display',
									sort:   sort !== null   ? i+'.@data-'+sort   : undefined,
									type:   sort !== null   ? i+'.@data-'+sort   : undefined,
									filter: filter !== null ? i+'.@data-'+filter : undefined
								};
				
								_fnColumnOptions( oSettings, i );
							}
						}
					} );
				}
				
				var features = oSettings.oFeatures;
				
				/* Must be done after everything which can be overridden by the state saving! */
				if ( oInit.bStateSave )
				{
					features.bStateSave = true;
					_fnLoadState( oSettings, oInit );
					_fnCallbackReg( oSettings, 'aoDrawCallback', _fnSaveState, 'state_save' );
				}
				
				
				/*
				 * Sorting
				 * @todo For modularisation (1.11) this needs to do into a sort start up handler
				 */
				
				// If aaSorting is not defined, then we use the first indicator in asSorting
				// in case that has been altered, so the default sort reflects that option
				if ( oInit.aaSorting === undefined )
				{
					var sorting = oSettings.aaSorting;
					for ( i=0, iLen=sorting.length ; i<iLen ; i++ )
					{
						sorting[i][1] = oSettings.aoColumns[ i ].asSorting[0];
					}
				}
				
				/* Do a first pass on the sorting classes (allows any size changes to be taken into
				 * account, and also will apply sorting disabled classes if disabled
				 */
				_fnSortingClasses( oSettings );
				
				if ( features.bSort )
				{
					_fnCallbackReg( oSettings, 'aoDrawCallback', function () {
						if ( oSettings.bSorted ) {
							var aSort = _fnSortFlatten( oSettings );
							var sortedColumns = {};
				
							$.each( aSort, function (i, val) {
								sortedColumns[ val.src ] = val.dir;
							} );
				
							_fnCallbackFire( oSettings, null, 'order', [oSettings, aSort, sortedColumns] );
							_fnSortAria( oSettings );
						}
					} );
				}
				
				_fnCallbackReg( oSettings, 'aoDrawCallback', function () {
					if ( oSettings.bSorted || _fnDataSource( oSettings ) === 'ssp' || features.bDeferRender ) {
						_fnSortingClasses( oSettings );
					}
				}, 'sc' );
				
				
				/*
				 * Final init
				 * Cache the header, body and footer as required, creating them if needed
				 */
				
				// Work around for Webkit bug 83867 - store the caption-side before removing from doc
				var captions = $this.children('caption').each( function () {
					this._captionSide = $this.css('caption-side');
				} );
				
				var thead = $this.children('thead');
				if ( thead.length === 0 )
				{
					thead = $('<thead/>').appendTo(this);
				}
				oSettings.nTHead = thead[0];
				
				var tbody = $this.children('tbody');
				if ( tbody.length === 0 )
				{
					tbody = $('<tbody/>').appendTo(this);
				}
				oSettings.nTBody = tbody[0];
				
				var tfoot = $this.children('tfoot');
				if ( tfoot.length === 0 && captions.length > 0 && (oSettings.oScroll.sX !== "" || oSettings.oScroll.sY !== "") )
				{
					// If we are a scrolling table, and no footer has been given, then we need to create
					// a tfoot element for the caption element to be appended to
					tfoot = $('<tfoot/>').appendTo(this);
				}
				
				if ( tfoot.length === 0 || tfoot.children().length === 0 ) {
					$this.addClass( oClasses.sNoFooter );
				}
				else if ( tfoot.length > 0 ) {
					oSettings.nTFoot = tfoot[0];
					_fnDetectHeader( oSettings.aoFooter, oSettings.nTFoot );
				}
				
				/* Check if there is data passing into the constructor */
				if ( oInit.aaData )
				{
					for ( i=0 ; i<oInit.aaData.length ; i++ )
					{
						_fnAddData( oSettings, oInit.aaData[ i ] );
					}
				}
				else if ( oSettings.bDeferLoading || _fnDataSource( oSettings ) == 'dom' )
				{
					/* Grab the data from the page - only do this when deferred loading or no Ajax
					 * source since there is no point in reading the DOM data if we are then going
					 * to replace it with Ajax data
					 */
					_fnAddTr( oSettings, $(oSettings.nTBody).children('tr') );
				}
				
				/* Copy the data index array */
				oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
				
				/* Initialisation complete - table can be drawn */
				oSettings.bInitialised = true;
				
				/* Check if we need to initialise the table (it might not have been handed off to the
				 * language processor)
				 */
				if ( bInitHandedOff === false )
				{
					_fnInitialise( oSettings );
				}
			} );
			_that = null;
			return this;
		};

		
		/*
		 * It is useful to have variables which are scoped locally so only the
		 * DataTables functions can access them and they don't leak into global space.
		 * At the same time these functions are often useful over multiple files in the
		 * core and API, so we list, or at least document, all variables which are used
		 * by DataTables as private variables here. This also ensures that there is no
		 * clashing of variable names and that they can easily referenced for reuse.
		 */
		
		
		// Defined else where
		//  _selector_run
		//  _selector_opts
		//  _selector_first
		//  _selector_row_indexes
		
		var _ext; // DataTable.ext
		var _Api; // DataTable.Api
		var _api_register; // DataTable.Api.register
		var _api_registerPlural; // DataTable.Api.registerPlural
		
		var _re_dic = {};
		var _re_new_lines = /[\r\n]/g;
		var _re_html = /<.*?>/g;
		var _re_date_start = /^[\w\+\-]/;
		var _re_date_end = /[\w\+\-]$/;
		
		// Escape regular expression special characters
		var _re_escape_regex = new RegExp( '(\\' + [ '/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\', '$', '^', '-' ].join('|\\') + ')', 'g' );
		
		// http://en.wikipedia.org/wiki/Foreign_exchange_market
		// - \u20BD - Russian ruble.
		// - \u20a9 - South Korean Won
		// - \u20BA - Turkish Lira
		// - \u20B9 - Indian Rupee
		// - R - Brazil (R$) and South Africa
		// - fr - Swiss Franc
		// - kr - Swedish krona, Norwegian krone and Danish krone
		// - \u2009 is thin space and \u202F is narrow no-break space, both used in many
		//   standards as thousands separators.
		var _re_formatted_numeric = /[',$£€¥%\u2009\u202F\u20BD\u20a9\u20BArfk]/gi;
		
		
		var _empty = function ( d ) {
			return !d || d === true || d === '-' ? true : false;
		};
		
		
		var _intVal = function ( s ) {
			var integer = parseInt( s, 10 );
			return !isNaN(integer) && isFinite(s) ? integer : null;
		};
		
		// Convert from a formatted number with characters other than `.` as the
		// decimal place, to a Javascript number
		var _numToDecimal = function ( num, decimalPoint ) {
			// Cache created regular expressions for speed as this function is called often
			if ( ! _re_dic[ decimalPoint ] ) {
				_re_dic[ decimalPoint ] = new RegExp( _fnEscapeRegex( decimalPoint ), 'g' );
			}
			return typeof num === 'string' && decimalPoint !== '.' ?
				num.replace( /\./g, '' ).replace( _re_dic[ decimalPoint ], '.' ) :
				num;
		};
		
		
		var _isNumber = function ( d, decimalPoint, formatted ) {
			var strType = typeof d === 'string';
		
			// If empty return immediately so there must be a number if it is a
			// formatted string (this stops the string "k", or "kr", etc being detected
			// as a formatted number for currency
			if ( _empty( d ) ) {
				return true;
			}
		
			if ( decimalPoint && strType ) {
				d = _numToDecimal( d, decimalPoint );
			}
		
			if ( formatted && strType ) {
				d = d.replace( _re_formatted_numeric, '' );
			}
		
			return !isNaN( parseFloat(d) ) && isFinite( d );
		};
		
		
		// A string without HTML in it can be considered to be HTML still
		var _isHtml = function ( d ) {
			return _empty( d ) || typeof d === 'string';
		};
		
		
		var _htmlNumeric = function ( d, decimalPoint, formatted ) {
			if ( _empty( d ) ) {
				return true;
			}
		
			var html = _isHtml( d );
			return ! html ?
				null :
				_isNumber( _stripHtml( d ), decimalPoint, formatted ) ?
					true :
					null;
		};
		
		
		var _pluck = function ( a, prop, prop2 ) {
			var out = [];
			var i=0, ien=a.length;
		
			// Could have the test in the loop for slightly smaller code, but speed
			// is essential here
			if ( prop2 !== undefined ) {
				for ( ; i<ien ; i++ ) {
					if ( a[i] && a[i][ prop ] ) {
						out.push( a[i][ prop ][ prop2 ] );
					}
				}
			}
			else {
				for ( ; i<ien ; i++ ) {
					if ( a[i] ) {
						out.push( a[i][ prop ] );
					}
				}
			}
		
			return out;
		};
		
		
		// Basically the same as _pluck, but rather than looping over `a` we use `order`
		// as the indexes to pick from `a`
		var _pluck_order = function ( a, order, prop, prop2 )
		{
			var out = [];
			var i=0, ien=order.length;
		
			// Could have the test in the loop for slightly smaller code, but speed
			// is essential here
			if ( prop2 !== undefined ) {
				for ( ; i<ien ; i++ ) {
					if ( a[ order[i] ][ prop ] ) {
						out.push( a[ order[i] ][ prop ][ prop2 ] );
					}
				}
			}
			else {
				for ( ; i<ien ; i++ ) {
					out.push( a[ order[i] ][ prop ] );
				}
			}
		
			return out;
		};
		
		
		var _range = function ( len, start )
		{
			var out = [];
			var end;
		
			if ( start === undefined ) {
				start = 0;
				end = len;
			}
			else {
				end = start;
				start = len;
			}
		
			for ( var i=start ; i<end ; i++ ) {
				out.push( i );
			}
		
			return out;
		};
		
		
		var _removeEmpty = function ( a )
		{
			var out = [];
		
			for ( var i=0, ien=a.length ; i<ien ; i++ ) {
				if ( a[i] ) { // careful - will remove all falsy values!
					out.push( a[i] );
				}
			}
		
			return out;
		};
		
		
		var _stripHtml = function ( d ) {
			return d.replace( _re_html, '' );
		};
		
		
		/**
		 * Find the unique elements in a source array.
		 *
		 * @param  {array} src Source array
		 * @return {array} Array of unique items
		 * @ignore
		 */
		var _unique = function ( src )
		{
			// A faster unique method is to use object keys to identify used values,
			// but this doesn't work with arrays or objects, which we must also
			// consider. See jsperf.com/compare-array-unique-versions/4 for more
			// information.
			var
				out = [],
				val,
				i, ien=src.length,
				j, k=0;
		
			again: for ( i=0 ; i<ien ; i++ ) {
				val = src[i];
		
				for ( j=0 ; j<k ; j++ ) {
					if ( out[j] === val ) {
						continue again;
					}
				}
		
				out.push( val );
				k++;
			}
		
			return out;
		};
		
		
		/**
		 * DataTables utility methods
		 * 
		 * This namespace provides helper methods that DataTables uses internally to
		 * create a DataTable, but which are not exclusively used only for DataTables.
		 * These methods can be used by extension authors to save the duplication of
		 * code.
		 *
		 *  @namespace
		 */
		DataTable.util = {
			/**
			 * Throttle the calls to a function. Arguments and context are maintained
			 * for the throttled function.
			 *
			 * @param {function} fn Function to be called
			 * @param {integer} freq Call frequency in mS
			 * @return {function} Wrapped function
			 */
			throttle: function ( fn, freq ) {
				var
					frequency = freq !== undefined ? freq : 200,
					last,
					timer;
		
				return function () {
					var
						that = this,
						now  = +new Date(),
						args = arguments;
		
					if ( last && now < last + frequency ) {
						clearTimeout( timer );
		
						timer = setTimeout( function () {
							last = undefined;
							fn.apply( that, args );
						}, frequency );
					}
					else {
						last = now;
						fn.apply( that, args );
					}
				};
			},
		
		
			/**
			 * Escape a string such that it can be used in a regular expression
			 *
			 *  @param {string} val string to escape
			 *  @returns {string} escaped string
			 */
			escapeRegex: function ( val ) {
				return val.replace( _re_escape_regex, '\\$1' );
			}
		};
		
		
		
		/**
		 * Create a mapping object that allows camel case parameters to be looked up
		 * for their Hungarian counterparts. The mapping is stored in a private
		 * parameter called `_hungarianMap` which can be accessed on the source object.
		 *  @param {object} o
		 *  @memberof DataTable#oApi
		 */
		function _fnHungarianMap ( o )
		{
			var
				hungarian = 'a aa ai ao as b fn i m o s ',
				match,
				newKey,
				map = {};
		
			$.each( o, function (key, val) {
				match = key.match(/^([^A-Z]+?)([A-Z])/);
		
				if ( match && hungarian.indexOf(match[1]+' ') !== -1 )
				{
					newKey = key.replace( match[0], match[2].toLowerCase() );
					map[ newKey ] = key;
		
					if ( match[1] === 'o' )
					{
						_fnHungarianMap( o[key] );
					}
				}
			} );
		
			o._hungarianMap = map;
		}
		
		
		/**
		 * Convert from camel case parameters to Hungarian, based on a Hungarian map
		 * created by _fnHungarianMap.
		 *  @param {object} src The model object which holds all parameters that can be
		 *    mapped.
		 *  @param {object} user The object to convert from camel case to Hungarian.
		 *  @param {boolean} force When set to `true`, properties which already have a
		 *    Hungarian value in the `user` object will be overwritten. Otherwise they
		 *    won't be.
		 *  @memberof DataTable#oApi
		 */
		function _fnCamelToHungarian ( src, user, force )
		{
			if ( ! src._hungarianMap ) {
				_fnHungarianMap( src );
			}
		
			var hungarianKey;
		
			$.each( user, function (key, val) {
				hungarianKey = src._hungarianMap[ key ];
		
				if ( hungarianKey !== undefined && (force || user[hungarianKey] === undefined) )
				{
					// For objects, we need to buzz down into the object to copy parameters
					if ( hungarianKey.charAt(0) === 'o' )
					{
						// Copy the camelCase options over to the hungarian
						if ( ! user[ hungarianKey ] ) {
							user[ hungarianKey ] = {};
						}
						$.extend( true, user[hungarianKey], user[key] );
		
						_fnCamelToHungarian( src[hungarianKey], user[hungarianKey], force );
					}
					else {
						user[hungarianKey] = user[ key ];
					}
				}
			} );
		}
		
		
		/**
		 * Language compatibility - when certain options are given, and others aren't, we
		 * need to duplicate the values over, in order to provide backwards compatibility
		 * with older language files.
		 *  @param {object} oSettings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnLanguageCompat( lang )
		{
			var defaults = DataTable.defaults.oLanguage;
			var zeroRecords = lang.sZeroRecords;
		
			/* Backwards compatibility - if there is no sEmptyTable given, then use the same as
			 * sZeroRecords - assuming that is given.
			 */
			if ( ! lang.sEmptyTable && zeroRecords &&
				defaults.sEmptyTable === "No data available in table" )
			{
				_fnMap( lang, lang, 'sZeroRecords', 'sEmptyTable' );
			}
		
			/* Likewise with loading records */
			if ( ! lang.sLoadingRecords && zeroRecords &&
				defaults.sLoadingRecords === "Loading..." )
			{
				_fnMap( lang, lang, 'sZeroRecords', 'sLoadingRecords' );
			}
		
			// Old parameter name of the thousands separator mapped onto the new
			if ( lang.sInfoThousands ) {
				lang.sThousands = lang.sInfoThousands;
			}
		
			var decimal = lang.sDecimal;
			if ( decimal ) {
				_addNumericSort( decimal );
			}
		}
		
		
		/**
		 * Map one parameter onto another
		 *  @param {object} o Object to map
		 *  @param {*} knew The new parameter name
		 *  @param {*} old The old parameter name
		 */
		var _fnCompatMap = function ( o, knew, old ) {
			if ( o[ knew ] !== undefined ) {
				o[ old ] = o[ knew ];
			}
		};
		
		
		/**
		 * Provide backwards compatibility for the main DT options. Note that the new
		 * options are mapped onto the old parameters, so this is an external interface
		 * change only.
		 *  @param {object} init Object to map
		 */
		function _fnCompatOpts ( init )
		{
			_fnCompatMap( init, 'ordering',      'bSort' );
			_fnCompatMap( init, 'orderMulti',    'bSortMulti' );
			_fnCompatMap( init, 'orderClasses',  'bSortClasses' );
			_fnCompatMap( init, 'orderCellsTop', 'bSortCellsTop' );
			_fnCompatMap( init, 'order',         'aaSorting' );
			_fnCompatMap( init, 'orderFixed',    'aaSortingFixed' );
			_fnCompatMap( init, 'paging',        'bPaginate' );
			_fnCompatMap( init, 'pagingType',    'sPaginationType' );
			_fnCompatMap( init, 'pageLength',    'iDisplayLength' );
			_fnCompatMap( init, 'searching',     'bFilter' );
		
			// Boolean initialisation of x-scrolling
			if ( typeof init.sScrollX === 'boolean' ) {
				init.sScrollX = init.sScrollX ? '100%' : '';
			}
			if ( typeof init.scrollX === 'boolean' ) {
				init.scrollX = init.scrollX ? '100%' : '';
			}
		
			// Column search objects are in an array, so it needs to be converted
			// element by element
			var searchCols = init.aoSearchCols;
		
			if ( searchCols ) {
				for ( var i=0, ien=searchCols.length ; i<ien ; i++ ) {
					if ( searchCols[i] ) {
						_fnCamelToHungarian( DataTable.models.oSearch, searchCols[i] );
					}
				}
			}
		}
		
		
		/**
		 * Provide backwards compatibility for column options. Note that the new options
		 * are mapped onto the old parameters, so this is an external interface change
		 * only.
		 *  @param {object} init Object to map
		 */
		function _fnCompatCols ( init )
		{
			_fnCompatMap( init, 'orderable',     'bSortable' );
			_fnCompatMap( init, 'orderData',     'aDataSort' );
			_fnCompatMap( init, 'orderSequence', 'asSorting' );
			_fnCompatMap( init, 'orderDataType', 'sortDataType' );
		
			// orderData can be given as an integer
			var dataSort = init.aDataSort;
			if ( dataSort && ! $.isArray( dataSort ) ) {
				init.aDataSort = [ dataSort ];
			}
		}
		
		
		/**
		 * Browser feature detection for capabilities, quirks
		 *  @param {object} settings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnBrowserDetect( settings )
		{
			// We don't need to do this every time DataTables is constructed, the values
			// calculated are specific to the browser and OS configuration which we
			// don't expect to change between initialisations
			if ( ! DataTable.__browser ) {
				var browser = {};
				DataTable.__browser = browser;
		
				// Scrolling feature / quirks detection
				var n = $('<div/>')
					.css( {
						position: 'fixed',
						top: 0,
						left: 0,
						height: 1,
						width: 1,
						overflow: 'hidden'
					} )
					.append(
						$('<div/>')
							.css( {
								position: 'absolute',
								top: 1,
								left: 1,
								width: 100,
								overflow: 'scroll'
							} )
							.append(
								$('<div/>')
									.css( {
										width: '100%',
										height: 10
									} )
							)
					)
					.appendTo( 'body' );
		
				var outer = n.children();
				var inner = outer.children();
		
				// Numbers below, in order, are:
				// inner.offsetWidth, inner.clientWidth, outer.offsetWidth, outer.clientWidth
				//
				// IE6 XP:                           100 100 100  83
				// IE7 Vista:                        100 100 100  83
				// IE 8+ Windows:                     83  83 100  83
				// Evergreen Windows:                 83  83 100  83
				// Evergreen Mac with scrollbars:     85  85 100  85
				// Evergreen Mac without scrollbars: 100 100 100 100
		
				// Get scrollbar width
				browser.barWidth = outer[0].offsetWidth - outer[0].clientWidth;
		
				// IE6/7 will oversize a width 100% element inside a scrolling element, to
				// include the width of the scrollbar, while other browsers ensure the inner
				// element is contained without forcing scrolling
				browser.bScrollOversize = inner[0].offsetWidth === 100 && outer[0].clientWidth !== 100;
		
				// In rtl text layout, some browsers (most, but not all) will place the
				// scrollbar on the left, rather than the right.
				browser.bScrollbarLeft = Math.round( inner.offset().left ) !== 1;
		
				// IE8- don't provide height and width for getBoundingClientRect
				browser.bBounding = n[0].getBoundingClientRect().width ? true : false;
		
				n.remove();
			}
		
			$.extend( settings.oBrowser, DataTable.__browser );
			settings.oScroll.iBarWidth = DataTable.__browser.barWidth;
		}
		
		
		/**
		 * Array.prototype reduce[Right] method, used for browsers which don't support
		 * JS 1.6. Done this way to reduce code size, since we iterate either way
		 *  @param {object} settings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnReduce ( that, fn, init, start, end, inc )
		{
			var
				i = start,
				value,
				isSet = false;
		
			if ( init !== undefined ) {
				value = init;
				isSet = true;
			}
		
			while ( i !== end ) {
				if ( ! that.hasOwnProperty(i) ) {
					continue;
				}
		
				value = isSet ?
					fn( value, that[i], i, that ) :
					that[i];
		
				isSet = true;
				i += inc;
			}
		
			return value;
		}
		
		/**
		 * Add a column to the list used for the table with default values
		 *  @param {object} oSettings dataTables settings object
		 *  @param {node} nTh The th element for this column
		 *  @memberof DataTable#oApi
		 */
		function _fnAddColumn( oSettings, nTh )
		{
			// Add column to aoColumns array
			var oDefaults = DataTable.defaults.column;
			var iCol = oSettings.aoColumns.length;
			var oCol = $.extend( {}, DataTable.models.oColumn, oDefaults, {
				"nTh": nTh ? nTh : document.createElement('th'),
				"sTitle":    oDefaults.sTitle    ? oDefaults.sTitle    : nTh ? nTh.innerHTML : '',
				"aDataSort": oDefaults.aDataSort ? oDefaults.aDataSort : [iCol],
				"mData": oDefaults.mData ? oDefaults.mData : iCol,
				idx: iCol
			} );
			oSettings.aoColumns.push( oCol );
		
			// Add search object for column specific search. Note that the `searchCols[ iCol ]`
			// passed into extend can be undefined. This allows the user to give a default
			// with only some of the parameters defined, and also not give a default
			var searchCols = oSettings.aoPreSearchCols;
			searchCols[ iCol ] = $.extend( {}, DataTable.models.oSearch, searchCols[ iCol ] );
		
			// Use the default column options function to initialise classes etc
			_fnColumnOptions( oSettings, iCol, $(nTh).data() );
		}
		
		
		/**
		 * Apply options for a column
		 *  @param {object} oSettings dataTables settings object
		 *  @param {int} iCol column index to consider
		 *  @param {object} oOptions object with sType, bVisible and bSearchable etc
		 *  @memberof DataTable#oApi
		 */
		function _fnColumnOptions( oSettings, iCol, oOptions )
		{
			var oCol = oSettings.aoColumns[ iCol ];
			var oClasses = oSettings.oClasses;
			var th = $(oCol.nTh);
		
			// Try to get width information from the DOM. We can't get it from CSS
			// as we'd need to parse the CSS stylesheet. `width` option can override
			if ( ! oCol.sWidthOrig ) {
				// Width attribute
				oCol.sWidthOrig = th.attr('width') || null;
		
				// Style attribute
				var t = (th.attr('style') || '').match(/width:\s*(\d+[pxem%]+)/);
				if ( t ) {
					oCol.sWidthOrig = t[1];
				}
			}
		
			/* User specified column options */
			if ( oOptions !== undefined && oOptions !== null )
			{
				// Backwards compatibility
				_fnCompatCols( oOptions );
		
				// Map camel case parameters to their Hungarian counterparts
				_fnCamelToHungarian( DataTable.defaults.column, oOptions );
		
				/* Backwards compatibility for mDataProp */
				if ( oOptions.mDataProp !== undefined && !oOptions.mData )
				{
					oOptions.mData = oOptions.mDataProp;
				}
		
				if ( oOptions.sType )
				{
					oCol._sManualType = oOptions.sType;
				}
		
				// `class` is a reserved word in Javascript, so we need to provide
				// the ability to use a valid name for the camel case input
				if ( oOptions.className && ! oOptions.sClass )
				{
					oOptions.sClass = oOptions.className;
				}
		
				$.extend( oCol, oOptions );
				_fnMap( oCol, oOptions, "sWidth", "sWidthOrig" );
		
				/* iDataSort to be applied (backwards compatibility), but aDataSort will take
				 * priority if defined
				 */
				if ( oOptions.iDataSort !== undefined )
				{
					oCol.aDataSort = [ oOptions.iDataSort ];
				}
				_fnMap( oCol, oOptions, "aDataSort" );
			}
		
			/* Cache the data get and set functions for speed */
			var mDataSrc = oCol.mData;
			var mData = _fnGetObjectDataFn( mDataSrc );
			var mRender = oCol.mRender ? _fnGetObjectDataFn( oCol.mRender ) : null;
		
			var attrTest = function( src ) {
				return typeof src === 'string' && src.indexOf('@') !== -1;
			};
			oCol._bAttrSrc = $.isPlainObject( mDataSrc ) && (
				attrTest(mDataSrc.sort) || attrTest(mDataSrc.type) || attrTest(mDataSrc.filter)
			);
			oCol._setter = null;
		
			oCol.fnGetData = function (rowData, type, meta) {
				var innerData = mData( rowData, type, undefined, meta );
		
				return mRender && type ?
					mRender( innerData, type, rowData, meta ) :
					innerData;
			};
			oCol.fnSetData = function ( rowData, val, meta ) {
				return _fnSetObjectDataFn( mDataSrc )( rowData, val, meta );
			};
		
			// Indicate if DataTables should read DOM data as an object or array
			// Used in _fnGetRowElements
			if ( typeof mDataSrc !== 'number' ) {
				oSettings._rowReadObject = true;
			}
		
			/* Feature sorting overrides column specific when off */
			if ( !oSettings.oFeatures.bSort )
			{
				oCol.bSortable = false;
				th.addClass( oClasses.sSortableNone ); // Have to add class here as order event isn't called
			}
		
			/* Check that the class assignment is correct for sorting */
			var bAsc = $.inArray('asc', oCol.asSorting) !== -1;
			var bDesc = $.inArray('desc', oCol.asSorting) !== -1;
			if ( !oCol.bSortable || (!bAsc && !bDesc) )
			{
				oCol.sSortingClass = oClasses.sSortableNone;
				oCol.sSortingClassJUI = "";
			}
			else if ( bAsc && !bDesc )
			{
				oCol.sSortingClass = oClasses.sSortableAsc;
				oCol.sSortingClassJUI = oClasses.sSortJUIAscAllowed;
			}
			else if ( !bAsc && bDesc )
			{
				oCol.sSortingClass = oClasses.sSortableDesc;
				oCol.sSortingClassJUI = oClasses.sSortJUIDescAllowed;
			}
			else
			{
				oCol.sSortingClass = oClasses.sSortable;
				oCol.sSortingClassJUI = oClasses.sSortJUI;
			}
		}
		
		
		/**
		 * Adjust the table column widths for new data. Note: you would probably want to
		 * do a redraw after calling this function!
		 *  @param {object} settings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnAdjustColumnSizing ( settings )
		{
			/* Not interested in doing column width calculation if auto-width is disabled */
			if ( settings.oFeatures.bAutoWidth !== false )
			{
				var columns = settings.aoColumns;
		
				_fnCalculateColumnWidths( settings );
				for ( var i=0 , iLen=columns.length ; i<iLen ; i++ )
				{
					columns[i].nTh.style.width = columns[i].sWidth;
				}
			}
		
			var scroll = settings.oScroll;
			if ( scroll.sY !== '' || scroll.sX !== '')
			{
				_fnScrollDraw( settings );
			}
		
			_fnCallbackFire( settings, null, 'column-sizing', [settings] );
		}
		
		
		/**
		 * Covert the index of a visible column to the index in the data array (take account
		 * of hidden columns)
		 *  @param {object} oSettings dataTables settings object
		 *  @param {int} iMatch Visible column index to lookup
		 *  @returns {int} i the data index
		 *  @memberof DataTable#oApi
		 */
		function _fnVisibleToColumnIndex( oSettings, iMatch )
		{
			var aiVis = _fnGetColumns( oSettings, 'bVisible' );
		
			return typeof aiVis[iMatch] === 'number' ?
				aiVis[iMatch] :
				null;
		}
		
		
		/**
		 * Covert the index of an index in the data array and convert it to the visible
		 *   column index (take account of hidden columns)
		 *  @param {int} iMatch Column index to lookup
		 *  @param {object} oSettings dataTables settings object
		 *  @returns {int} i the data index
		 *  @memberof DataTable#oApi
		 */
		function _fnColumnIndexToVisible( oSettings, iMatch )
		{
			var aiVis = _fnGetColumns( oSettings, 'bVisible' );
			var iPos = $.inArray( iMatch, aiVis );
		
			return iPos !== -1 ? iPos : null;
		}
		
		
		/**
		 * Get the number of visible columns
		 *  @param {object} oSettings dataTables settings object
		 *  @returns {int} i the number of visible columns
		 *  @memberof DataTable#oApi
		 */
		function _fnVisbleColumns( oSettings )
		{
			var vis = 0;
		
			// No reduce in IE8, use a loop for now
			$.each( oSettings.aoColumns, function ( i, col ) {
				if ( col.bVisible && $(col.nTh).css('display') !== 'none' ) {
					vis++;
				}
			} );
		
			return vis;
		}
		
		
		/**
		 * Get an array of column indexes that match a given property
		 *  @param {object} oSettings dataTables settings object
		 *  @param {string} sParam Parameter in aoColumns to look for - typically
		 *    bVisible or bSearchable
		 *  @returns {array} Array of indexes with matched properties
		 *  @memberof DataTable#oApi
		 */
		function _fnGetColumns( oSettings, sParam )
		{
			var a = [];
		
			$.map( oSettings.aoColumns, function(val, i) {
				if ( val[sParam] ) {
					a.push( i );
				}
			} );
		
			return a;
		}
		
		
		/**
		 * Calculate the 'type' of a column
		 *  @param {object} settings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnColumnTypes ( settings )
		{
			var columns = settings.aoColumns;
			var data = settings.aoData;
			var types = DataTable.ext.type.detect;
			var i, ien, j, jen, k, ken;
			var col, cell, detectedType, cache;
		
			// For each column, spin over the 
			for ( i=0, ien=columns.length ; i<ien ; i++ ) {
				col = columns[i];
				cache = [];
		
				if ( ! col.sType && col._sManualType ) {
					col.sType = col._sManualType;
				}
				else if ( ! col.sType ) {
					for ( j=0, jen=types.length ; j<jen ; j++ ) {
						for ( k=0, ken=data.length ; k<ken ; k++ ) {
							// Use a cache array so we only need to get the type data
							// from the formatter once (when using multiple detectors)
							if ( cache[k] === undefined ) {
								cache[k] = _fnGetCellData( settings, k, i, 'type' );
							}
		
							detectedType = types[j]( cache[k], settings );
		
							// If null, then this type can't apply to this column, so
							// rather than testing all cells, break out. There is an
							// exception for the last type which is `html`. We need to
							// scan all rows since it is possible to mix string and HTML
							// types
							if ( ! detectedType && j !== types.length-1 ) {
								break;
							}
		
							// Only a single match is needed for html type since it is
							// bottom of the pile and very similar to string
							if ( detectedType === 'html' ) {
								break;
							}
						}
		
						// Type is valid for all data points in the column - use this
						// type
						if ( detectedType ) {
							col.sType = detectedType;
							break;
						}
					}
		
					// Fall back - if no type was detected, always use string
					if ( ! col.sType ) {
						col.sType = 'string';
					}
				}
			}
		}
		
		
		/**
		 * Take the column definitions and static columns arrays and calculate how
		 * they relate to column indexes. The callback function will then apply the
		 * definition found for a column to a suitable configuration object.
		 *  @param {object} oSettings dataTables settings object
		 *  @param {array} aoColDefs The aoColumnDefs array that is to be applied
		 *  @param {array} aoCols The aoColumns array that defines columns individually
		 *  @param {function} fn Callback function - takes two parameters, the calculated
		 *    column index and the definition for that column.
		 *  @memberof DataTable#oApi
		 */
		function _fnApplyColumnDefs( oSettings, aoColDefs, aoCols, fn )
		{
			var i, iLen, j, jLen, k, kLen, def;
			var columns = oSettings.aoColumns;
		
			// Column definitions with aTargets
			if ( aoColDefs )
			{
				/* Loop over the definitions array - loop in reverse so first instance has priority */
				for ( i=aoColDefs.length-1 ; i>=0 ; i-- )
				{
					def = aoColDefs[i];
		
					/* Each definition can target multiple columns, as it is an array */
					var aTargets = def.targets !== undefined ?
						def.targets :
						def.aTargets;
		
					if ( ! $.isArray( aTargets ) )
					{
						aTargets = [ aTargets ];
					}
		
					for ( j=0, jLen=aTargets.length ; j<jLen ; j++ )
					{
						if ( typeof aTargets[j] === 'number' && aTargets[j] >= 0 )
						{
							/* Add columns that we don't yet know about */
							while( columns.length <= aTargets[j] )
							{
								_fnAddColumn( oSettings );
							}
		
							/* Integer, basic index */
							fn( aTargets[j], def );
						}
						else if ( typeof aTargets[j] === 'number' && aTargets[j] < 0 )
						{
							/* Negative integer, right to left column counting */
							fn( columns.length+aTargets[j], def );
						}
						else if ( typeof aTargets[j] === 'string' )
						{
							/* Class name matching on TH element */
							for ( k=0, kLen=columns.length ; k<kLen ; k++ )
							{
								if ( aTargets[j] == "_all" ||
								     $(columns[k].nTh).hasClass( aTargets[j] ) )
								{
									fn( k, def );
								}
							}
						}
					}
				}
			}
		
			// Statically defined columns array
			if ( aoCols )
			{
				for ( i=0, iLen=aoCols.length ; i<iLen ; i++ )
				{
					fn( i, aoCols[i] );
				}
			}
		}
		
		/**
		 * Add a data array to the table, creating DOM node etc. This is the parallel to
		 * _fnGatherData, but for adding rows from a Javascript source, rather than a
		 * DOM source.
		 *  @param {object} oSettings dataTables settings object
		 *  @param {array} aData data array to be added
		 *  @param {node} [nTr] TR element to add to the table - optional. If not given,
		 *    DataTables will create a row automatically
		 *  @param {array} [anTds] Array of TD|TH elements for the row - must be given
		 *    if nTr is.
		 *  @returns {int} >=0 if successful (index of new aoData entry), -1 if failed
		 *  @memberof DataTable#oApi
		 */
		function _fnAddData ( oSettings, aDataIn, nTr, anTds )
		{
			/* Create the object for storing information about this new row */
			var iRow = oSettings.aoData.length;
			var oData = $.extend( true, {}, DataTable.models.oRow, {
				src: nTr ? 'dom' : 'data',
				idx: iRow
			} );
		
			oData._aData = aDataIn;
			oSettings.aoData.push( oData );
		
			/* Create the cells */
			var nTd, sThisType;
			var columns = oSettings.aoColumns;
		
			// Invalidate the column types as the new data needs to be revalidated
			for ( var i=0, iLen=columns.length ; i<iLen ; i++ )
			{
				columns[i].sType = null;
			}
		
			/* Add to the display array */
			oSettings.aiDisplayMaster.push( iRow );
		
			var id = oSettings.rowIdFn( aDataIn );
			if ( id !== undefined ) {
				oSettings.aIds[ id ] = oData;
			}
		
			/* Create the DOM information, or register it if already present */
			if ( nTr || ! oSettings.oFeatures.bDeferRender )
			{
				_fnCreateTr( oSettings, iRow, nTr, anTds );
			}
		
			return iRow;
		}
		
		
		/**
		 * Add one or more TR elements to the table. Generally we'd expect to
		 * use this for reading data from a DOM sourced table, but it could be
		 * used for an TR element. Note that if a TR is given, it is used (i.e.
		 * it is not cloned).
		 *  @param {object} settings dataTables settings object
		 *  @param {array|node|jQuery} trs The TR element(s) to add to the table
		 *  @returns {array} Array of indexes for the added rows
		 *  @memberof DataTable#oApi
		 */
		function _fnAddTr( settings, trs )
		{
			var row;
		
			// Allow an individual node to be passed in
			if ( ! (trs instanceof $) ) {
				trs = $(trs);
			}
		
			return trs.map( function (i, el) {
				row = _fnGetRowElements( settings, el );
				return _fnAddData( settings, row.data, el, row.cells );
			} );
		}
		
		
		/**
		 * Take a TR element and convert it to an index in aoData
		 *  @param {object} oSettings dataTables settings object
		 *  @param {node} n the TR element to find
		 *  @returns {int} index if the node is found, null if not
		 *  @memberof DataTable#oApi
		 */
		function _fnNodeToDataIndex( oSettings, n )
		{
			return (n._DT_RowIndex!==undefined) ? n._DT_RowIndex : null;
		}
		
		
		/**
		 * Take a TD element and convert it into a column data index (not the visible index)
		 *  @param {object} oSettings dataTables settings object
		 *  @param {int} iRow The row number the TD/TH can be found in
		 *  @param {node} n The TD/TH element to find
		 *  @returns {int} index if the node is found, -1 if not
		 *  @memberof DataTable#oApi
		 */
		function _fnNodeToColumnIndex( oSettings, iRow, n )
		{
			return $.inArray( n, oSettings.aoData[ iRow ].anCells );
		}
		
		
		/**
		 * Get the data for a given cell from the internal cache, taking into account data mapping
		 *  @param {object} settings dataTables settings object
		 *  @param {int} rowIdx aoData row id
		 *  @param {int} colIdx Column index
		 *  @param {string} type data get type ('display', 'type' 'filter' 'sort')
		 *  @returns {*} Cell data
		 *  @memberof DataTable#oApi
		 */
		function _fnGetCellData( settings, rowIdx, colIdx, type )
		{
			var draw           = settings.iDraw;
			var col            = settings.aoColumns[colIdx];
			var rowData        = settings.aoData[rowIdx]._aData;
			var defaultContent = col.sDefaultContent;
			var cellData       = col.fnGetData( rowData, type, {
				settings: settings,
				row:      rowIdx,
				col:      colIdx
			} );
		
			if ( cellData === undefined ) {
				if ( settings.iDrawError != draw && defaultContent === null ) {
					_fnLog( settings, 0, "Requested unknown parameter "+
						(typeof col.mData=='function' ? '{function}' : "'"+col.mData+"'")+
						" for row "+rowIdx+", column "+colIdx, 4 );
					settings.iDrawError = draw;
				}
				return defaultContent;
			}
		
			// When the data source is null and a specific data type is requested (i.e.
			// not the original data), we can use default column data
			if ( (cellData === rowData || cellData === null) && defaultContent !== null && type !== undefined ) {
				cellData = defaultContent;
			}
			else if ( typeof cellData === 'function' ) {
				// If the data source is a function, then we run it and use the return,
				// executing in the scope of the data object (for instances)
				return cellData.call( rowData );
			}
		
			if ( cellData === null && type == 'display' ) {
				return '';
			}
			return cellData;
		}
		
		
		/**
		 * Set the value for a specific cell, into the internal data cache
		 *  @param {object} settings dataTables settings object
		 *  @param {int} rowIdx aoData row id
		 *  @param {int} colIdx Column index
		 *  @param {*} val Value to set
		 *  @memberof DataTable#oApi
		 */
		function _fnSetCellData( settings, rowIdx, colIdx, val )
		{
			var col     = settings.aoColumns[colIdx];
			var rowData = settings.aoData[rowIdx]._aData;
		
			col.fnSetData( rowData, val, {
				settings: settings,
				row:      rowIdx,
				col:      colIdx
			}  );
		}
		
		
		// Private variable that is used to match action syntax in the data property object
		var __reArray = /\[.*?\]$/;
		var __reFn = /\(\)$/;
		
		/**
		 * Split string on periods, taking into account escaped periods
		 * @param  {string} str String to split
		 * @return {array} Split string
		 */
		function _fnSplitObjNotation( str )
		{
			return $.map( str.match(/(\\.|[^\.])+/g) || [''], function ( s ) {
				return s.replace(/\\./g, '.');
			} );
		}
		
		
		/**
		 * Return a function that can be used to get data from a source object, taking
		 * into account the ability to use nested objects as a source
		 *  @param {string|int|function} mSource The data source for the object
		 *  @returns {function} Data get function
		 *  @memberof DataTable#oApi
		 */
		function _fnGetObjectDataFn( mSource )
		{
			if ( $.isPlainObject( mSource ) )
			{
				/* Build an object of get functions, and wrap them in a single call */
				var o = {};
				$.each( mSource, function (key, val) {
					if ( val ) {
						o[key] = _fnGetObjectDataFn( val );
					}
				} );
		
				return function (data, type, row, meta) {
					var t = o[type] || o._;
					return t !== undefined ?
						t(data, type, row, meta) :
						data;
				};
			}
			else if ( mSource === null )
			{
				/* Give an empty string for rendering / sorting etc */
				return function (data) { // type, row and meta also passed, but not used
					return data;
				};
			}
			else if ( typeof mSource === 'function' )
			{
				return function (data, type, row, meta) {
					return mSource( data, type, row, meta );
				};
			}
			else if ( typeof mSource === 'string' && (mSource.indexOf('.') !== -1 ||
				      mSource.indexOf('[') !== -1 || mSource.indexOf('(') !== -1) )
			{
				/* If there is a . in the source string then the data source is in a
				 * nested object so we loop over the data for each level to get the next
				 * level down. On each loop we test for undefined, and if found immediately
				 * return. This allows entire objects to be missing and sDefaultContent to
				 * be used if defined, rather than throwing an error
				 */
				var fetchData = function (data, type, src) {
					var arrayNotation, funcNotation, out, innerSrc;
		
					if ( src !== "" )
					{
						var a = _fnSplitObjNotation( src );
		
						for ( var i=0, iLen=a.length ; i<iLen ; i++ )
						{
							// Check if we are dealing with special notation
							arrayNotation = a[i].match(__reArray);
							funcNotation = a[i].match(__reFn);
		
							if ( arrayNotation )
							{
								// Array notation
								a[i] = a[i].replace(__reArray, '');
		
								// Condition allows simply [] to be passed in
								if ( a[i] !== "" ) {
									data = data[ a[i] ];
								}
								out = [];
		
								// Get the remainder of the nested object to get
								a.splice( 0, i+1 );
								innerSrc = a.join('.');
		
								// Traverse each entry in the array getting the properties requested
								if ( $.isArray( data ) ) {
									for ( var j=0, jLen=data.length ; j<jLen ; j++ ) {
										out.push( fetchData( data[j], type, innerSrc ) );
									}
								}
		
								// If a string is given in between the array notation indicators, that
								// is used to join the strings together, otherwise an array is returned
								var join = arrayNotation[0].substring(1, arrayNotation[0].length-1);
								data = (join==="") ? out : out.join(join);
		
								// The inner call to fetchData has already traversed through the remainder
								// of the source requested, so we exit from the loop
								break;
							}
							else if ( funcNotation )
							{
								// Function call
								a[i] = a[i].replace(__reFn, '');
								data = data[ a[i] ]();
								continue;
							}
		
							if ( data === null || data[ a[i] ] === undefined )
							{
								return undefined;
							}
							data = data[ a[i] ];
						}
					}
		
					return data;
				};
		
				return function (data, type) { // row and meta also passed, but not used
					return fetchData( data, type, mSource );
				};
			}
			else
			{
				/* Array or flat object mapping */
				return function (data, type) { // row and meta also passed, but not used
					return data[mSource];
				};
			}
		}
		
		
		/**
		 * Return a function that can be used to set data from a source object, taking
		 * into account the ability to use nested objects as a source
		 *  @param {string|int|function} mSource The data source for the object
		 *  @returns {function} Data set function
		 *  @memberof DataTable#oApi
		 */
		function _fnSetObjectDataFn( mSource )
		{
			if ( $.isPlainObject( mSource ) )
			{
				/* Unlike get, only the underscore (global) option is used for for
				 * setting data since we don't know the type here. This is why an object
				 * option is not documented for `mData` (which is read/write), but it is
				 * for `mRender` which is read only.
				 */
				return _fnSetObjectDataFn( mSource._ );
			}
			else if ( mSource === null )
			{
				/* Nothing to do when the data source is null */
				return function () {};
			}
			else if ( typeof mSource === 'function' )
			{
				return function (data, val, meta) {
					mSource( data, 'set', val, meta );
				};
			}
			else if ( typeof mSource === 'string' && (mSource.indexOf('.') !== -1 ||
				      mSource.indexOf('[') !== -1 || mSource.indexOf('(') !== -1) )
			{
				/* Like the get, we need to get data from a nested object */
				var setData = function (data, val, src) {
					var a = _fnSplitObjNotation( src ), b;
					var aLast = a[a.length-1];
					var arrayNotation, funcNotation, o, innerSrc;
		
					for ( var i=0, iLen=a.length-1 ; i<iLen ; i++ )
					{
						// Check if we are dealing with an array notation request
						arrayNotation = a[i].match(__reArray);
						funcNotation = a[i].match(__reFn);
		
						if ( arrayNotation )
						{
							a[i] = a[i].replace(__reArray, '');
							data[ a[i] ] = [];
		
							// Get the remainder of the nested object to set so we can recurse
							b = a.slice();
							b.splice( 0, i+1 );
							innerSrc = b.join('.');
		
							// Traverse each entry in the array setting the properties requested
							if ( $.isArray( val ) )
							{
								for ( var j=0, jLen=val.length ; j<jLen ; j++ )
								{
									o = {};
									setData( o, val[j], innerSrc );
									data[ a[i] ].push( o );
								}
							}
							else
							{
								// We've been asked to save data to an array, but it
								// isn't array data to be saved. Best that can be done
								// is to just save the value.
								data[ a[i] ] = val;
							}
		
							// The inner call to setData has already traversed through the remainder
							// of the source and has set the data, thus we can exit here
							return;
						}
						else if ( funcNotation )
						{
							// Function call
							a[i] = a[i].replace(__reFn, '');
							data = data[ a[i] ]( val );
						}
		
						// If the nested object doesn't currently exist - since we are
						// trying to set the value - create it
						if ( data[ a[i] ] === null || data[ a[i] ] === undefined )
						{
							data[ a[i] ] = {};
						}
						data = data[ a[i] ];
					}
		
					// Last item in the input - i.e, the actual set
					if ( aLast.match(__reFn ) )
					{
						// Function call
						data = data[ aLast.replace(__reFn, '') ]( val );
					}
					else
					{
						// If array notation is used, we just want to strip it and use the property name
						// and assign the value. If it isn't used, then we get the result we want anyway
						data[ aLast.replace(__reArray, '') ] = val;
					}
				};
		
				return function (data, val) { // meta is also passed in, but not used
					return setData( data, val, mSource );
				};
			}
			else
			{
				/* Array or flat object mapping */
				return function (data, val) { // meta is also passed in, but not used
					data[mSource] = val;
				};
			}
		}
		
		
		/**
		 * Return an array with the full table data
		 *  @param {object} oSettings dataTables settings object
		 *  @returns array {array} aData Master data array
		 *  @memberof DataTable#oApi
		 */
		function _fnGetDataMaster ( settings )
		{
			return _pluck( settings.aoData, '_aData' );
		}
		
		
		/**
		 * Nuke the table
		 *  @param {object} oSettings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnClearTable( settings )
		{
			settings.aoData.length = 0;
			settings.aiDisplayMaster.length = 0;
			settings.aiDisplay.length = 0;
			settings.aIds = {};
		}
		
		
		 /**
		 * Take an array of integers (index array) and remove a target integer (value - not
		 * the key!)
		 *  @param {array} a Index array to target
		 *  @param {int} iTarget value to find
		 *  @memberof DataTable#oApi
		 */
		function _fnDeleteIndex( a, iTarget, splice )
		{
			var iTargetIndex = -1;
		
			for ( var i=0, iLen=a.length ; i<iLen ; i++ )
			{
				if ( a[i] == iTarget )
				{
					iTargetIndex = i;
				}
				else if ( a[i] > iTarget )
				{
					a[i]--;
				}
			}
		
			if ( iTargetIndex != -1 && splice === undefined )
			{
				a.splice( iTargetIndex, 1 );
			}
		}
		
		
		/**
		 * Mark cached data as invalid such that a re-read of the data will occur when
		 * the cached data is next requested. Also update from the data source object.
		 *
		 * @param {object} settings DataTables settings object
		 * @param {int}    rowIdx   Row index to invalidate
		 * @param {string} [src]    Source to invalidate from: undefined, 'auto', 'dom'
		 *     or 'data'
		 * @param {int}    [colIdx] Column index to invalidate. If undefined the whole
		 *     row will be invalidated
		 * @memberof DataTable#oApi
		 *
		 * @todo For the modularisation of v1.11 this will need to become a callback, so
		 *   the sort and filter methods can subscribe to it. That will required
		 *   initialisation options for sorting, which is why it is not already baked in
		 */
		function _fnInvalidate( settings, rowIdx, src, colIdx )
		{
			var row = settings.aoData[ rowIdx ];
			var i, ien;
			var cellWrite = function ( cell, col ) {
				// This is very frustrating, but in IE if you just write directly
				// to innerHTML, and elements that are overwritten are GC'ed,
				// even if there is a reference to them elsewhere
				while ( cell.childNodes.length ) {
					cell.removeChild( cell.firstChild );
				}
		
				cell.innerHTML = _fnGetCellData( settings, rowIdx, col, 'display' );
			};
		
			// Are we reading last data from DOM or the data object?
			if ( src === 'dom' || ((! src || src === 'auto') && row.src === 'dom') ) {
				// Read the data from the DOM
				row._aData = _fnGetRowElements(
						settings, row, colIdx, colIdx === undefined ? undefined : row._aData
					)
					.data;
			}
			else {
				// Reading from data object, update the DOM
				var cells = row.anCells;
		
				if ( cells ) {
					if ( colIdx !== undefined ) {
						cellWrite( cells[colIdx], colIdx );
					}
					else {
						for ( i=0, ien=cells.length ; i<ien ; i++ ) {
							cellWrite( cells[i], i );
						}
					}
				}
			}
		
			// For both row and cell invalidation, the cached data for sorting and
			// filtering is nulled out
			row._aSortData = null;
			row._aFilterData = null;
		
			// Invalidate the type for a specific column (if given) or all columns since
			// the data might have changed
			var cols = settings.aoColumns;
			if ( colIdx !== undefined ) {
				cols[ colIdx ].sType = null;
			}
			else {
				for ( i=0, ien=cols.length ; i<ien ; i++ ) {
					cols[i].sType = null;
				}
		
				// Update DataTables special `DT_*` attributes for the row
				_fnRowAttributes( settings, row );
			}
		}
		
		
		/**
		 * Build a data source object from an HTML row, reading the contents of the
		 * cells that are in the row.
		 *
		 * @param {object} settings DataTables settings object
		 * @param {node|object} TR element from which to read data or existing row
		 *   object from which to re-read the data from the cells
		 * @param {int} [colIdx] Optional column index
		 * @param {array|object} [d] Data source object. If `colIdx` is given then this
		 *   parameter should also be given and will be used to write the data into.
		 *   Only the column in question will be written
		 * @returns {object} Object with two parameters: `data` the data read, in
		 *   document order, and `cells` and array of nodes (they can be useful to the
		 *   caller, so rather than needing a second traversal to get them, just return
		 *   them from here).
		 * @memberof DataTable#oApi
		 */
		function _fnGetRowElements( settings, row, colIdx, d )
		{
			var
				tds = [],
				td = row.firstChild,
				name, col, o, i=0, contents,
				columns = settings.aoColumns,
				objectRead = settings._rowReadObject;
		
			// Allow the data object to be passed in, or construct
			d = d !== undefined ?
				d :
				objectRead ?
					{} :
					[];
		
			var attr = function ( str, td  ) {
				if ( typeof str === 'string' ) {
					var idx = str.indexOf('@');
		
					if ( idx !== -1 ) {
						var attr = str.substring( idx+1 );
						var setter = _fnSetObjectDataFn( str );
						setter( d, td.getAttribute( attr ) );
					}
				}
			};
		
			// Read data from a cell and store into the data object
			var cellProcess = function ( cell ) {
				if ( colIdx === undefined || colIdx === i ) {
					col = columns[i];
					contents = $.trim(cell.innerHTML);
		
					if ( col && col._bAttrSrc ) {
						var setter = _fnSetObjectDataFn( col.mData._ );
						setter( d, contents );
		
						attr( col.mData.sort, cell );
						attr( col.mData.type, cell );
						attr( col.mData.filter, cell );
					}
					else {
						// Depending on the `data` option for the columns the data can
						// be read to either an object or an array.
						if ( objectRead ) {
							if ( ! col._setter ) {
								// Cache the setter function
								col._setter = _fnSetObjectDataFn( col.mData );
							}
							col._setter( d, contents );
						}
						else {
							d[i] = contents;
						}
					}
				}
		
				i++;
			};
		
			if ( td ) {
				// `tr` element was passed in
				while ( td ) {
					name = td.nodeName.toUpperCase();
		
					if ( name == "TD" || name == "TH" ) {
						cellProcess( td );
						tds.push( td );
					}
		
					td = td.nextSibling;
				}
			}
			else {
				// Existing row object passed in
				tds = row.anCells;
		
				for ( var j=0, jen=tds.length ; j<jen ; j++ ) {
					cellProcess( tds[j] );
				}
			}
		
			// Read the ID from the DOM if present
			var rowNode = row.firstChild ? row : row.nTr;
		
			if ( rowNode ) {
				var id = rowNode.getAttribute( 'id' );
		
				if ( id ) {
					_fnSetObjectDataFn( settings.rowId )( d, id );
				}
			}
		
			return {
				data: d,
				cells: tds
			};
		}
		/**
		 * Create a new TR element (and it's TD children) for a row
		 *  @param {object} oSettings dataTables settings object
		 *  @param {int} iRow Row to consider
		 *  @param {node} [nTrIn] TR element to add to the table - optional. If not given,
		 *    DataTables will create a row automatically
		 *  @param {array} [anTds] Array of TD|TH elements for the row - must be given
		 *    if nTr is.
		 *  @memberof DataTable#oApi
		 */
		function _fnCreateTr ( oSettings, iRow, nTrIn, anTds )
		{
			var
				row = oSettings.aoData[iRow],
				rowData = row._aData,
				cells = [],
				nTr, nTd, oCol,
				i, iLen;
		
			if ( row.nTr === null )
			{
				nTr = nTrIn || document.createElement('tr');
		
				row.nTr = nTr;
				row.anCells = cells;
		
				/* Use a private property on the node to allow reserve mapping from the node
				 * to the aoData array for fast look up
				 */
				nTr._DT_RowIndex = iRow;
		
				/* Special parameters can be given by the data source to be used on the row */
				_fnRowAttributes( oSettings, row );
		
				/* Process each column */
				for ( i=0, iLen=oSettings.aoColumns.length ; i<iLen ; i++ )
				{
					oCol = oSettings.aoColumns[i];
		
					nTd = nTrIn ? anTds[i] : document.createElement( oCol.sCellType );
					nTd._DT_CellIndex = {
						row: iRow,
						column: i
					};
					
					cells.push( nTd );
		
					// Need to create the HTML if new, or if a rendering function is defined
					if ( (!nTrIn || oCol.mRender || oCol.mData !== i) &&
						 (!$.isPlainObject(oCol.mData) || oCol.mData._ !== i+'.display')
					) {
						nTd.innerHTML = _fnGetCellData( oSettings, iRow, i, 'display' );
					}
		
					/* Add user defined class */
					if ( oCol.sClass )
					{
						nTd.className += ' '+oCol.sClass;
					}
		
					// Visibility - add or remove as required
					if ( oCol.bVisible && ! nTrIn )
					{
						nTr.appendChild( nTd );
					}
					else if ( ! oCol.bVisible && nTrIn )
					{
						nTd.parentNode.removeChild( nTd );
					}
		
					if ( oCol.fnCreatedCell )
					{
						oCol.fnCreatedCell.call( oSettings.oInstance,
							nTd, _fnGetCellData( oSettings, iRow, i ), rowData, iRow, i
						);
					}
				}
		
				_fnCallbackFire( oSettings, 'aoRowCreatedCallback', null, [nTr, rowData, iRow] );
			}
		
			// Remove once webkit bug 131819 and Chromium bug 365619 have been resolved
			// and deployed
			row.nTr.setAttribute( 'role', 'row' );
		}
		
		
		/**
		 * Add attributes to a row based on the special `DT_*` parameters in a data
		 * source object.
		 *  @param {object} settings DataTables settings object
		 *  @param {object} DataTables row object for the row to be modified
		 *  @memberof DataTable#oApi
		 */
		function _fnRowAttributes( settings, row )
		{
			var tr = row.nTr;
			var data = row._aData;
		
			if ( tr ) {
				var id = settings.rowIdFn( data );
		
				if ( id ) {
					tr.id = id;
				}
		
				if ( data.DT_RowClass ) {
					// Remove any classes added by DT_RowClass before
					var a = data.DT_RowClass.split(' ');
					row.__rowc = row.__rowc ?
						_unique( row.__rowc.concat( a ) ) :
						a;
		
					$(tr)
						.removeClass( row.__rowc.join(' ') )
						.addClass( data.DT_RowClass );
				}
		
				if ( data.DT_RowAttr ) {
					$(tr).attr( data.DT_RowAttr );
				}
		
				if ( data.DT_RowData ) {
					$(tr).data( data.DT_RowData );
				}
			}
		}
		
		
		/**
		 * Create the HTML header for the table
		 *  @param {object} oSettings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnBuildHead( oSettings )
		{
			var i, ien, cell, row, column;
			var thead = oSettings.nTHead;
			var tfoot = oSettings.nTFoot;
			var createHeader = $('th, td', thead).length === 0;
			var classes = oSettings.oClasses;
			var columns = oSettings.aoColumns;
		
			if ( createHeader ) {
				row = $('<tr/>').appendTo( thead );
			}
		
			for ( i=0, ien=columns.length ; i<ien ; i++ ) {
				column = columns[i];
				cell = $( column.nTh ).addClass( column.sClass );
		
				if ( createHeader ) {
					cell.appendTo( row );
				}
		
				// 1.11 move into sorting
				if ( oSettings.oFeatures.bSort ) {
					cell.addClass( column.sSortingClass );
		
					if ( column.bSortable !== false ) {
						cell
							.attr( 'tabindex', oSettings.iTabIndex )
							.attr( 'aria-controls', oSettings.sTableId );
		
						_fnSortAttachListener( oSettings, column.nTh, i );
					}
				}
		
				if ( column.sTitle != cell[0].innerHTML ) {
					cell.html( column.sTitle );
				}
		
				_fnRenderer( oSettings, 'header' )(
					oSettings, cell, column, classes
				);
			}
		
			if ( createHeader ) {
				_fnDetectHeader( oSettings.aoHeader, thead );
			}
			
			/* ARIA role for the rows */
		 	$(thead).find('>tr').attr('role', 'row');
		
			/* Deal with the footer - add classes if required */
			$(thead).find('>tr>th, >tr>td').addClass( classes.sHeaderTH );
			$(tfoot).find('>tr>th, >tr>td').addClass( classes.sFooterTH );
		
			// Cache the footer cells. Note that we only take the cells from the first
			// row in the footer. If there is more than one row the user wants to
			// interact with, they need to use the table().foot() method. Note also this
			// allows cells to be used for multiple columns using colspan
			if ( tfoot !== null ) {
				var cells = oSettings.aoFooter[0];
		
				for ( i=0, ien=cells.length ; i<ien ; i++ ) {
					column = columns[i];
					column.nTf = cells[i].cell;
		
					if ( column.sClass ) {
						$(column.nTf).addClass( column.sClass );
					}
				}
			}
		}
		
		
		/**
		 * Draw the header (or footer) element based on the column visibility states. The
		 * methodology here is to use the layout array from _fnDetectHeader, modified for
		 * the instantaneous column visibility, to construct the new layout. The grid is
		 * traversed over cell at a time in a rows x columns grid fashion, although each
		 * cell insert can cover multiple elements in the grid - which is tracks using the
		 * aApplied array. Cell inserts in the grid will only occur where there isn't
		 * already a cell in that position.
		 *  @param {object} oSettings dataTables settings object
		 *  @param array {objects} aoSource Layout array from _fnDetectHeader
		 *  @param {boolean} [bIncludeHidden=false] If true then include the hidden columns in the calc,
		 *  @memberof DataTable#oApi
		 */
		function _fnDrawHead( oSettings, aoSource, bIncludeHidden )
		{
			var i, iLen, j, jLen, k, kLen, n, nLocalTr;
			var aoLocal = [];
			var aApplied = [];
			var iColumns = oSettings.aoColumns.length;
			var iRowspan, iColspan;
		
			if ( ! aoSource )
			{
				return;
			}
		
			if (  bIncludeHidden === undefined )
			{
				bIncludeHidden = false;
			}
		
			/* Make a copy of the master layout array, but without the visible columns in it */
			for ( i=0, iLen=aoSource.length ; i<iLen ; i++ )
			{
				aoLocal[i] = aoSource[i].slice();
				aoLocal[i].nTr = aoSource[i].nTr;
		
				/* Remove any columns which are currently hidden */
				for ( j=iColumns-1 ; j>=0 ; j-- )
				{
					if ( !oSettings.aoColumns[j].bVisible && !bIncludeHidden )
					{
						aoLocal[i].splice( j, 1 );
					}
				}
		
				/* Prep the applied array - it needs an element for each row */
				aApplied.push( [] );
			}
		
			for ( i=0, iLen=aoLocal.length ; i<iLen ; i++ )
			{
				nLocalTr = aoLocal[i].nTr;
		
				/* All cells are going to be replaced, so empty out the row */
				if ( nLocalTr )
				{
					while( (n = nLocalTr.firstChild) )
					{
						nLocalTr.removeChild( n );
					}
				}
		
				for ( j=0, jLen=aoLocal[i].length ; j<jLen ; j++ )
				{
					iRowspan = 1;
					iColspan = 1;
		
					/* Check to see if there is already a cell (row/colspan) covering our target
					 * insert point. If there is, then there is nothing to do.
					 */
					if ( aApplied[i][j] === undefined )
					{
						nLocalTr.appendChild( aoLocal[i][j].cell );
						aApplied[i][j] = 1;
		
						/* Expand the cell to cover as many rows as needed */
						while ( aoLocal[i+iRowspan] !== undefined &&
						        aoLocal[i][j].cell == aoLocal[i+iRowspan][j].cell )
						{
							aApplied[i+iRowspan][j] = 1;
							iRowspan++;
						}
		
						/* Expand the cell to cover as many columns as needed */
						while ( aoLocal[i][j+iColspan] !== undefined &&
						        aoLocal[i][j].cell == aoLocal[i][j+iColspan].cell )
						{
							/* Must update the applied array over the rows for the columns */
							for ( k=0 ; k<iRowspan ; k++ )
							{
								aApplied[i+k][j+iColspan] = 1;
							}
							iColspan++;
						}
		
						/* Do the actual expansion in the DOM */
						$(aoLocal[i][j].cell)
							.attr('rowspan', iRowspan)
							.attr('colspan', iColspan);
					}
				}
			}
		}
		
		
		/**
		 * Insert the required TR nodes into the table for display
		 *  @param {object} oSettings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnDraw( oSettings )
		{
			/* Provide a pre-callback function which can be used to cancel the draw is false is returned */
			var aPreDraw = _fnCallbackFire( oSettings, 'aoPreDrawCallback', 'preDraw', [oSettings] );
			if ( $.inArray( false, aPreDraw ) !== -1 )
			{
				_fnProcessingDisplay( oSettings, false );
				return;
			}
		
			var i, iLen, n;
			var anRows = [];
			var iRowCount = 0;
			var asStripeClasses = oSettings.asStripeClasses;
			var iStripes = asStripeClasses.length;
			var iOpenRows = oSettings.aoOpenRows.length;
			var oLang = oSettings.oLanguage;
			var iInitDisplayStart = oSettings.iInitDisplayStart;
			var bServerSide = _fnDataSource( oSettings ) == 'ssp';
			var aiDisplay = oSettings.aiDisplay;
		
			oSettings.bDrawing = true;
		
			/* Check and see if we have an initial draw position from state saving */
			if ( iInitDisplayStart !== undefined && iInitDisplayStart !== -1 )
			{
				oSettings._iDisplayStart = bServerSide ?
					iInitDisplayStart :
					iInitDisplayStart >= oSettings.fnRecordsDisplay() ?
						0 :
						iInitDisplayStart;
		
				oSettings.iInitDisplayStart = -1;
			}
		
			var iDisplayStart = oSettings._iDisplayStart;
			var iDisplayEnd = oSettings.fnDisplayEnd();
		
			/* Server-side processing draw intercept */
			if ( oSettings.bDeferLoading )
			{
				oSettings.bDeferLoading = false;
				oSettings.iDraw++;
				_fnProcessingDisplay( oSettings, false );
			}
			else if ( !bServerSide )
			{
				oSettings.iDraw++;
			}
			else if ( !oSettings.bDestroying && !_fnAjaxUpdate( oSettings ) )
			{
				return;
			}
		
			if ( aiDisplay.length !== 0 )
			{
				var iStart = bServerSide ? 0 : iDisplayStart;
				var iEnd = bServerSide ? oSettings.aoData.length : iDisplayEnd;
		
				for ( var j=iStart ; j<iEnd ; j++ )
				{
					var iDataIndex = aiDisplay[j];
					var aoData = oSettings.aoData[ iDataIndex ];
					if ( aoData.nTr === null )
					{
						_fnCreateTr( oSettings, iDataIndex );
					}
		
					var nRow = aoData.nTr;
		
					/* Remove the old striping classes and then add the new one */
					if ( iStripes !== 0 )
					{
						var sStripe = asStripeClasses[ iRowCount % iStripes ];
						if ( aoData._sRowStripe != sStripe )
						{
							$(nRow).removeClass( aoData._sRowStripe ).addClass( sStripe );
							aoData._sRowStripe = sStripe;
						}
					}
		
					// Row callback functions - might want to manipulate the row
					// iRowCount and j are not currently documented. Are they at all
					// useful?
					_fnCallbackFire( oSettings, 'aoRowCallback', null,
						[nRow, aoData._aData, iRowCount, j] );
		
					anRows.push( nRow );
					iRowCount++;
				}
			}
			else
			{
				/* Table is empty - create a row with an empty message in it */
				var sZero = oLang.sZeroRecords;
				if ( oSettings.iDraw == 1 &&  _fnDataSource( oSettings ) == 'ajax' )
				{
					sZero = oLang.sLoadingRecords;
				}
				else if ( oLang.sEmptyTable && oSettings.fnRecordsTotal() === 0 )
				{
					sZero = oLang.sEmptyTable;
				}
		
				anRows[ 0 ] = $( '<tr/>', { 'class': iStripes ? asStripeClasses[0] : '' } )
					.append( $('<td />', {
						'valign':  'top',
						'colSpan': _fnVisbleColumns( oSettings ),
						'class':   oSettings.oClasses.sRowEmpty
					} ).html( sZero ) )[0];
			}
		
			/* Header and footer callbacks */
			_fnCallbackFire( oSettings, 'aoHeaderCallback', 'header', [ $(oSettings.nTHead).children('tr')[0],
				_fnGetDataMaster( oSettings ), iDisplayStart, iDisplayEnd, aiDisplay ] );
		
			_fnCallbackFire( oSettings, 'aoFooterCallback', 'footer', [ $(oSettings.nTFoot).children('tr')[0],
				_fnGetDataMaster( oSettings ), iDisplayStart, iDisplayEnd, aiDisplay ] );
		
			var body = $(oSettings.nTBody);
		
			body.children().detach();
			body.append( $(anRows) );
		
			/* Call all required callback functions for the end of a draw */
			_fnCallbackFire( oSettings, 'aoDrawCallback', 'draw', [oSettings] );
		
			/* Draw is complete, sorting and filtering must be as well */
			oSettings.bSorted = false;
			oSettings.bFiltered = false;
			oSettings.bDrawing = false;
		}
		
		
		/**
		 * Redraw the table - taking account of the various features which are enabled
		 *  @param {object} oSettings dataTables settings object
		 *  @param {boolean} [holdPosition] Keep the current paging position. By default
		 *    the paging is reset to the first page
		 *  @memberof DataTable#oApi
		 */
		function _fnReDraw( settings, holdPosition )
		{
			var
				features = settings.oFeatures,
				sort     = features.bSort,
				filter   = features.bFilter;
		
			if ( sort ) {
				_fnSort( settings );
			}
		
			if ( filter ) {
				_fnFilterComplete( settings, settings.oPreviousSearch );
			}
			else {
				// No filtering, so we want to just use the display master
				settings.aiDisplay = settings.aiDisplayMaster.slice();
			}
		
			if ( holdPosition !== true ) {
				settings._iDisplayStart = 0;
			}
		
			// Let any modules know about the draw hold position state (used by
			// scrolling internally)
			settings._drawHold = holdPosition;
		
			_fnDraw( settings );
		
			settings._drawHold = false;
		}
		
		
		/**
		 * Add the options to the page HTML for the table
		 *  @param {object} oSettings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnAddOptionsHtml ( oSettings )
		{
			var classes = oSettings.oClasses;
			var table = $(oSettings.nTable);
			var holding = $('<div/>').insertBefore( table ); // Holding element for speed
			var features = oSettings.oFeatures;
		
			// All DataTables are wrapped in a div
			var insert = $('<div/>', {
				id:      oSettings.sTableId+'_wrapper',
				'class': classes.sWrapper + (oSettings.nTFoot ? '' : ' '+classes.sNoFooter)
			} );
		
			oSettings.nHolding = holding[0];
			oSettings.nTableWrapper = insert[0];
			oSettings.nTableReinsertBefore = oSettings.nTable.nextSibling;
		
			/* Loop over the user set positioning and place the elements as needed */
			var aDom = oSettings.sDom.split('');
			var featureNode, cOption, nNewNode, cNext, sAttr, j;
			for ( var i=0 ; i<aDom.length ; i++ )
			{
				featureNode = null;
				cOption = aDom[i];
		
				if ( cOption == '<' )
				{
					/* New container div */
					nNewNode = $('<div/>')[0];
		
					/* Check to see if we should append an id and/or a class name to the container */
					cNext = aDom[i+1];
					if ( cNext == "'" || cNext == '"' )
					{
						sAttr = "";
						j = 2;
						while ( aDom[i+j] != cNext )
						{
							sAttr += aDom[i+j];
							j++;
						}
		
						/* Replace jQuery UI constants @todo depreciated */
						if ( sAttr == "H" )
						{
							sAttr = classes.sJUIHeader;
						}
						else if ( sAttr == "F" )
						{
							sAttr = classes.sJUIFooter;
						}
		
						/* The attribute can be in the format of "#id.class", "#id" or "class" This logic
						 * breaks the string into parts and applies them as needed
						 */
						if ( sAttr.indexOf('.') != -1 )
						{
							var aSplit = sAttr.split('.');
							nNewNode.id = aSplit[0].substr(1, aSplit[0].length-1);
							nNewNode.className = aSplit[1];
						}
						else if ( sAttr.charAt(0) == "#" )
						{
							nNewNode.id = sAttr.substr(1, sAttr.length-1);
						}
						else
						{
							nNewNode.className = sAttr;
						}
		
						i += j; /* Move along the position array */
					}
		
					insert.append( nNewNode );
					insert = $(nNewNode);
				}
				else if ( cOption == '>' )
				{
					/* End container div */
					insert = insert.parent();
				}
				// @todo Move options into their own plugins?
				else if ( cOption == 'l' && features.bPaginate && features.bLengthChange )
				{
					/* Length */
					featureNode = _fnFeatureHtmlLength( oSettings );
				}
				else if ( cOption == 'f' && features.bFilter )
				{
					/* Filter */
					featureNode = _fnFeatureHtmlFilter( oSettings );
				}
				else if ( cOption == 'r' && features.bProcessing )
				{
					/* pRocessing */
					featureNode = _fnFeatureHtmlProcessing( oSettings );
				}
				else if ( cOption == 't' )
				{
					/* Table */
					featureNode = _fnFeatureHtmlTable( oSettings );
				}
				else if ( cOption ==  'i' && features.bInfo )
				{
					/* Info */
					featureNode = _fnFeatureHtmlInfo( oSettings );
				}
				else if ( cOption == 'p' && features.bPaginate )
				{
					/* Pagination */
					featureNode = _fnFeatureHtmlPaginate( oSettings );
				}
				else if ( DataTable.ext.feature.length !== 0 )
				{
					/* Plug-in features */
					var aoFeatures = DataTable.ext.feature;
					for ( var k=0, kLen=aoFeatures.length ; k<kLen ; k++ )
					{
						if ( cOption == aoFeatures[k].cFeature )
						{
							featureNode = aoFeatures[k].fnInit( oSettings );
							break;
						}
					}
				}
		
				/* Add to the 2D features array */
				if ( featureNode )
				{
					var aanFeatures = oSettings.aanFeatures;
		
					if ( ! aanFeatures[cOption] )
					{
						aanFeatures[cOption] = [];
					}
		
					aanFeatures[cOption].push( featureNode );
					insert.append( featureNode );
				}
			}
		
			/* Built our DOM structure - replace the holding div with what we want */
			holding.replaceWith( insert );
			oSettings.nHolding = null;
		}
		
		
		/**
		 * Use the DOM source to create up an array of header cells. The idea here is to
		 * create a layout grid (array) of rows x columns, which contains a reference
		 * to the cell that that point in the grid (regardless of col/rowspan), such that
		 * any column / row could be removed and the new grid constructed
		 *  @param array {object} aLayout Array to store the calculated layout in
		 *  @param {node} nThead The header/footer element for the table
		 *  @memberof DataTable#oApi
		 */
		function _fnDetectHeader ( aLayout, nThead )
		{
			var nTrs = $(nThead).children('tr');
			var nTr, nCell;
			var i, k, l, iLen, jLen, iColShifted, iColumn, iColspan, iRowspan;
			var bUnique;
			var fnShiftCol = function ( a, i, j ) {
				var k = a[i];
		                while ( k[j] ) {
					j++;
				}
				return j;
			};
		
			aLayout.splice( 0, aLayout.length );
		
			/* We know how many rows there are in the layout - so prep it */
			for ( i=0, iLen=nTrs.length ; i<iLen ; i++ )
			{
				aLayout.push( [] );
			}
		
			/* Calculate a layout array */
			for ( i=0, iLen=nTrs.length ; i<iLen ; i++ )
			{
				nTr = nTrs[i];
				iColumn = 0;
		
				/* For every cell in the row... */
				nCell = nTr.firstChild;
				while ( nCell ) {
					if ( nCell.nodeName.toUpperCase() == "TD" ||
					     nCell.nodeName.toUpperCase() == "TH" )
					{
						/* Get the col and rowspan attributes from the DOM and sanitise them */
						iColspan = nCell.getAttribute('colspan') * 1;
						iRowspan = nCell.getAttribute('rowspan') * 1;
						iColspan = (!iColspan || iColspan===0 || iColspan===1) ? 1 : iColspan;
						iRowspan = (!iRowspan || iRowspan===0 || iRowspan===1) ? 1 : iRowspan;
		
						/* There might be colspan cells already in this row, so shift our target
						 * accordingly
						 */
						iColShifted = fnShiftCol( aLayout, i, iColumn );
		
						/* Cache calculation for unique columns */
						bUnique = iColspan === 1 ? true : false;
		
						/* If there is col / rowspan, copy the information into the layout grid */
						for ( l=0 ; l<iColspan ; l++ )
						{
							for ( k=0 ; k<iRowspan ; k++ )
							{
								aLayout[i+k][iColShifted+l] = {
									"cell": nCell,
									"unique": bUnique
								};
								aLayout[i+k].nTr = nTr;
							}
						}
					}
					nCell = nCell.nextSibling;
				}
			}
		}
		
		
		/**
		 * Get an array of unique th elements, one for each column
		 *  @param {object} oSettings dataTables settings object
		 *  @param {node} nHeader automatically detect the layout from this node - optional
		 *  @param {array} aLayout thead/tfoot layout from _fnDetectHeader - optional
		 *  @returns array {node} aReturn list of unique th's
		 *  @memberof DataTable#oApi
		 */
		function _fnGetUniqueThs ( oSettings, nHeader, aLayout )
		{
			var aReturn = [];
			if ( !aLayout )
			{
				aLayout = oSettings.aoHeader;
				if ( nHeader )
				{
					aLayout = [];
					_fnDetectHeader( aLayout, nHeader );
				}
			}
		
			for ( var i=0, iLen=aLayout.length ; i<iLen ; i++ )
			{
				for ( var j=0, jLen=aLayout[i].length ; j<jLen ; j++ )
				{
					if ( aLayout[i][j].unique &&
						 (!aReturn[j] || !oSettings.bSortCellsTop) )
					{
						aReturn[j] = aLayout[i][j].cell;
					}
				}
			}
		
			return aReturn;
		}
		
		/**
		 * Create an Ajax call based on the table's settings, taking into account that
		 * parameters can have multiple forms, and backwards compatibility.
		 *
		 * @param {object} oSettings dataTables settings object
		 * @param {array} data Data to send to the server, required by
		 *     DataTables - may be augmented by developer callbacks
		 * @param {function} fn Callback function to run when data is obtained
		 */
		function _fnBuildAjax( oSettings, data, fn )
		{
			// Compatibility with 1.9-, allow fnServerData and event to manipulate
			_fnCallbackFire( oSettings, 'aoServerParams', 'serverParams', [data] );
		
			// Convert to object based for 1.10+ if using the old array scheme which can
			// come from server-side processing or serverParams
			if ( data && $.isArray(data) ) {
				var tmp = {};
				var rbracket = /(.*?)\[\]$/;
		
				$.each( data, function (key, val) {
					var match = val.name.match(rbracket);
		
					if ( match ) {
						// Support for arrays
						var name = match[0];
		
						if ( ! tmp[ name ] ) {
							tmp[ name ] = [];
						}
						tmp[ name ].push( val.value );
					}
					else {
						tmp[val.name] = val.value;
					}
				} );
				data = tmp;
			}
		
			var ajaxData;
			var ajax = oSettings.ajax;
			var instance = oSettings.oInstance;
			var callback = function ( json ) {
				_fnCallbackFire( oSettings, null, 'xhr', [oSettings, json, oSettings.jqXHR] );
				fn( json );
			};
		
			if ( $.isPlainObject( ajax ) && ajax.data )
			{
				ajaxData = ajax.data;
		
				var newData = $.isFunction( ajaxData ) ?
					ajaxData( data, oSettings ) :  // fn can manipulate data or return
					ajaxData;                      // an object object or array to merge
		
				// If the function returned something, use that alone
				data = $.isFunction( ajaxData ) && newData ?
					newData :
					$.extend( true, data, newData );
		
				// Remove the data property as we've resolved it already and don't want
				// jQuery to do it again (it is restored at the end of the function)
				delete ajax.data;
			}
		
			var baseAjax = {
				"data": data,
				"success": function (json) {
					var error = json.error || json.sError;
					if ( error ) {
						_fnLog( oSettings, 0, error );
					}
		
					oSettings.json = json;
					callback( json );
				},
				"dataType": "json",
				"cache": false,
				"type": oSettings.sServerMethod,
				"error": function (xhr, error, thrown) {
					var ret = _fnCallbackFire( oSettings, null, 'xhr', [oSettings, null, oSettings.jqXHR] );
		
					if ( $.inArray( true, ret ) === -1 ) {
						if ( error == "parsererror" ) {
							_fnLog( oSettings, 0, 'Invalid JSON response', 1 );
						}
						else if ( xhr.readyState === 4 ) {
							_fnLog( oSettings, 0, 'Ajax error', 7 );
						}
					}
		
					_fnProcessingDisplay( oSettings, false );
				}
			};
		
			// Store the data submitted for the API
			oSettings.oAjaxData = data;
		
			// Allow plug-ins and external processes to modify the data
			_fnCallbackFire( oSettings, null, 'preXhr', [oSettings, data] );
		
			if ( oSettings.fnServerData )
			{
				// DataTables 1.9- compatibility
				oSettings.fnServerData.call( instance,
					oSettings.sAjaxSource,
					$.map( data, function (val, key) { // Need to convert back to 1.9 trad format
						return { name: key, value: val };
					} ),
					callback,
					oSettings
				);
			}
			else if ( oSettings.sAjaxSource || typeof ajax === 'string' )
			{
				// DataTables 1.9- compatibility
				oSettings.jqXHR = $.ajax( $.extend( baseAjax, {
					url: ajax || oSettings.sAjaxSource
				} ) );
			}
			else if ( $.isFunction( ajax ) )
			{
				// Is a function - let the caller define what needs to be done
				oSettings.jqXHR = ajax.call( instance, data, callback, oSettings );
			}
			else
			{
				// Object to extend the base settings
				oSettings.jqXHR = $.ajax( $.extend( baseAjax, ajax ) );
		
				// Restore for next time around
				ajax.data = ajaxData;
			}
		}
		
		
		/**
		 * Update the table using an Ajax call
		 *  @param {object} settings dataTables settings object
		 *  @returns {boolean} Block the table drawing or not
		 *  @memberof DataTable#oApi
		 */
		function _fnAjaxUpdate( settings )
		{
			if ( settings.bAjaxDataGet ) {
				settings.iDraw++;
				_fnProcessingDisplay( settings, true );
		
				_fnBuildAjax(
					settings,
					_fnAjaxParameters( settings ),
					function(json) {
						_fnAjaxUpdateDraw( settings, json );
					}
				);
		
				return false;
			}
			return true;
		}
		
		
		/**
		 * Build up the parameters in an object needed for a server-side processing
		 * request. Note that this is basically done twice, is different ways - a modern
		 * method which is used by default in DataTables 1.10 which uses objects and
		 * arrays, or the 1.9- method with is name / value pairs. 1.9 method is used if
		 * the sAjaxSource option is used in the initialisation, or the legacyAjax
		 * option is set.
		 *  @param {object} oSettings dataTables settings object
		 *  @returns {bool} block the table drawing or not
		 *  @memberof DataTable#oApi
		 */
		function _fnAjaxParameters( settings )
		{
			var
				columns = settings.aoColumns,
				columnCount = columns.length,
				features = settings.oFeatures,
				preSearch = settings.oPreviousSearch,
				preColSearch = settings.aoPreSearchCols,
				i, data = [], dataProp, column, columnSearch,
				sort = _fnSortFlatten( settings ),
				displayStart = settings._iDisplayStart,
				displayLength = features.bPaginate !== false ?
					settings._iDisplayLength :
					-1;
		
			var param = function ( name, value ) {
				data.push( { 'name': name, 'value': value } );
			};
		
			// DataTables 1.9- compatible method
			param( 'sEcho',          settings.iDraw );
			param( 'iColumns',       columnCount );
			param( 'sColumns',       _pluck( columns, 'sName' ).join(',') );
			param( 'iDisplayStart',  displayStart );
			param( 'iDisplayLength', displayLength );
		
			// DataTables 1.10+ method
			var d = {
				draw:    settings.iDraw,
				columns: [],
				order:   [],
				start:   displayStart,
				length:  displayLength,
				search:  {
					value: preSearch.sSearch,
					regex: preSearch.bRegex
				}
			};
		
			for ( i=0 ; i<columnCount ; i++ ) {
				column = columns[i];
				columnSearch = preColSearch[i];
				dataProp = typeof column.mData=="function" ? 'function' : column.mData ;
		
				d.columns.push( {
					data:       dataProp,
					name:       column.sName,
					searchable: column.bSearchable,
					orderable:  column.bSortable,
					search:     {
						value: columnSearch.sSearch,
						regex: columnSearch.bRegex
					}
				} );
		
				param( "mDataProp_"+i, dataProp );
		
				if ( features.bFilter ) {
					param( 'sSearch_'+i,     columnSearch.sSearch );
					param( 'bRegex_'+i,      columnSearch.bRegex );
					param( 'bSearchable_'+i, column.bSearchable );
				}
		
				if ( features.bSort ) {
					param( 'bSortable_'+i, column.bSortable );
				}
			}
		
			if ( features.bFilter ) {
				param( 'sSearch', preSearch.sSearch );
				param( 'bRegex', preSearch.bRegex );
			}
		
			if ( features.bSort ) {
				$.each( sort, function ( i, val ) {
					d.order.push( { column: val.col, dir: val.dir } );
		
					param( 'iSortCol_'+i, val.col );
					param( 'sSortDir_'+i, val.dir );
				} );
		
				param( 'iSortingCols', sort.length );
			}
		
			// If the legacy.ajax parameter is null, then we automatically decide which
			// form to use, based on sAjaxSource
			var legacy = DataTable.ext.legacy.ajax;
			if ( legacy === null ) {
				return settings.sAjaxSource ? data : d;
			}
		
			// Otherwise, if legacy has been specified then we use that to decide on the
			// form
			return legacy ? data : d;
		}
		
		
		/**
		 * Data the data from the server (nuking the old) and redraw the table
		 *  @param {object} oSettings dataTables settings object
		 *  @param {object} json json data return from the server.
		 *  @param {string} json.sEcho Tracking flag for DataTables to match requests
		 *  @param {int} json.iTotalRecords Number of records in the data set, not accounting for filtering
		 *  @param {int} json.iTotalDisplayRecords Number of records in the data set, accounting for filtering
		 *  @param {array} json.aaData The data to display on this page
		 *  @param {string} [json.sColumns] Column ordering (sName, comma separated)
		 *  @memberof DataTable#oApi
		 */
		function _fnAjaxUpdateDraw ( settings, json )
		{
			// v1.10 uses camelCase variables, while 1.9 uses Hungarian notation.
			// Support both
			var compat = function ( old, modern ) {
				return json[old] !== undefined ? json[old] : json[modern];
			};
		
			var data = _fnAjaxDataSrc( settings, json );
			var draw            = compat( 'sEcho',                'draw' );
			var recordsTotal    = compat( 'iTotalRecords',        'recordsTotal' );
			var recordsFiltered = compat( 'iTotalDisplayRecords', 'recordsFiltered' );
		
			if ( draw ) {
				// Protect against out of sequence returns
				if ( draw*1 < settings.iDraw ) {
					return;
				}
				settings.iDraw = draw * 1;
			}
		
			_fnClearTable( settings );
			settings._iRecordsTotal   = parseInt(recordsTotal, 10);
			settings._iRecordsDisplay = parseInt(recordsFiltered, 10);
		
			for ( var i=0, ien=data.length ; i<ien ; i++ ) {
				_fnAddData( settings, data[i] );
			}
			settings.aiDisplay = settings.aiDisplayMaster.slice();
		
			settings.bAjaxDataGet = false;
			_fnDraw( settings );
		
			if ( ! settings._bInitComplete ) {
				_fnInitComplete( settings, json );
			}
		
			settings.bAjaxDataGet = true;
			_fnProcessingDisplay( settings, false );
		}
		
		
		/**
		 * Get the data from the JSON data source to use for drawing a table. Using
		 * `_fnGetObjectDataFn` allows the data to be sourced from a property of the
		 * source object, or from a processing function.
		 *  @param {object} oSettings dataTables settings object
		 *  @param  {object} json Data source object / array from the server
		 *  @return {array} Array of data to use
		 */
		function _fnAjaxDataSrc ( oSettings, json )
		{
			var dataSrc = $.isPlainObject( oSettings.ajax ) && oSettings.ajax.dataSrc !== undefined ?
				oSettings.ajax.dataSrc :
				oSettings.sAjaxDataProp; // Compatibility with 1.9-.
		
			// Compatibility with 1.9-. In order to read from aaData, check if the
			// default has been changed, if not, check for aaData
			if ( dataSrc === 'data' ) {
				return json.aaData || json[dataSrc];
			}
		
			return dataSrc !== "" ?
				_fnGetObjectDataFn( dataSrc )( json ) :
				json;
		}
		
		/**
		 * Generate the node required for filtering text
		 *  @returns {node} Filter control element
		 *  @param {object} oSettings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnFeatureHtmlFilter ( settings )
		{
			var classes = settings.oClasses;
			var tableId = settings.sTableId;
			var language = settings.oLanguage;
			var previousSearch = settings.oPreviousSearch;
			var features = settings.aanFeatures;
			var input = '<input type="search" class="'+classes.sFilterInput+'"/>';
		
			var str = language.sSearch;
			str = str.match(/_INPUT_/) ?
				str.replace('_INPUT_', input) :
				str+input;
		
			var filter = $('<div/>', {
					'id': ! features.f ? tableId+'_filter' : null,
					'class': classes.sFilter
				} )
				.append( $('<label/>' ).append( str ) );
		
			var searchFn = function() {
				/* Update all other filter input elements for the new display */
				var n = features.f;
				var val = !this.value ? "" : this.value; // mental IE8 fix :-(
		
				/* Now do the filter */
				if ( val != previousSearch.sSearch ) {
					_fnFilterComplete( settings, {
						"sSearch": val,
						"bRegex": previousSearch.bRegex,
						"bSmart": previousSearch.bSmart ,
						"bCaseInsensitive": previousSearch.bCaseInsensitive
					} );
		
					// Need to redraw, without resorting
					settings._iDisplayStart = 0;
					_fnDraw( settings );
				}
			};
		
			var searchDelay = settings.searchDelay !== null ?
				settings.searchDelay :
				_fnDataSource( settings ) === 'ssp' ?
					400 :
					0;
		
			var jqFilter = $('input', filter)
				.val( previousSearch.sSearch )
				.attr( 'placeholder', language.sSearchPlaceholder )
				.bind(
					'keyup.DT search.DT input.DT paste.DT cut.DT',
					searchDelay ?
						_fnThrottle( searchFn, searchDelay ) :
						searchFn
				)
				.bind( 'keypress.DT', function(e) {
					/* Prevent form submission */
					if ( e.keyCode == 13 ) {
						return false;
					}
				} )
				.attr('aria-controls', tableId);
		
			// Update the input elements whenever the table is filtered
			$(settings.nTable).on( 'search.dt.DT', function ( ev, s ) {
				if ( settings === s ) {
					// IE9 throws an 'unknown error' if document.activeElement is used
					// inside an iframe or frame...
					try {
						if ( jqFilter[0] !== document.activeElement ) {
							jqFilter.val( previousSearch.sSearch );
						}
					}
					catch ( e ) {}
				}
			} );
		
			return filter[0];
		}
		
		
		/**
		 * Filter the table using both the global filter and column based filtering
		 *  @param {object} oSettings dataTables settings object
		 *  @param {object} oSearch search information
		 *  @param {int} [iForce] force a research of the master array (1) or not (undefined or 0)
		 *  @memberof DataTable#oApi
		 */
		function _fnFilterComplete ( oSettings, oInput, iForce )
		{
			var oPrevSearch = oSettings.oPreviousSearch;
			var aoPrevSearch = oSettings.aoPreSearchCols;
			var fnSaveFilter = function ( oFilter ) {
				/* Save the filtering values */
				oPrevSearch.sSearch = oFilter.sSearch;
				oPrevSearch.bRegex = oFilter.bRegex;
				oPrevSearch.bSmart = oFilter.bSmart;
				oPrevSearch.bCaseInsensitive = oFilter.bCaseInsensitive;
			};
			var fnRegex = function ( o ) {
				// Backwards compatibility with the bEscapeRegex option
				return o.bEscapeRegex !== undefined ? !o.bEscapeRegex : o.bRegex;
			};
		
			// Resolve any column types that are unknown due to addition or invalidation
			// @todo As per sort - can this be moved into an event handler?
			_fnColumnTypes( oSettings );
		
			/* In server-side processing all filtering is done by the server, so no point hanging around here */
			if ( _fnDataSource( oSettings ) != 'ssp' )
			{
				/* Global filter */
				_fnFilter( oSettings, oInput.sSearch, iForce, fnRegex(oInput), oInput.bSmart, oInput.bCaseInsensitive );
				fnSaveFilter( oInput );
		
				/* Now do the individual column filter */
				for ( var i=0 ; i<aoPrevSearch.length ; i++ )
				{
					_fnFilterColumn( oSettings, aoPrevSearch[i].sSearch, i, fnRegex(aoPrevSearch[i]),
						aoPrevSearch[i].bSmart, aoPrevSearch[i].bCaseInsensitive );
				}
		
				/* Custom filtering */
				_fnFilterCustom( oSettings );
			}
			else
			{
				fnSaveFilter( oInput );
			}
		
			/* Tell the draw function we have been filtering */
			oSettings.bFiltered = true;
			_fnCallbackFire( oSettings, null, 'search', [oSettings] );
		}
		
		
		/**
		 * Apply custom filtering functions
		 *  @param {object} oSettings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnFilterCustom( settings )
		{
			var filters = DataTable.ext.search;
			var displayRows = settings.aiDisplay;
			var row, rowIdx;
		
			for ( var i=0, ien=filters.length ; i<ien ; i++ ) {
				var rows = [];
		
				// Loop over each row and see if it should be included
				for ( var j=0, jen=displayRows.length ; j<jen ; j++ ) {
					rowIdx = displayRows[ j ];
					row = settings.aoData[ rowIdx ];
		
					if ( filters[i]( settings, row._aFilterData, rowIdx, row._aData, j ) ) {
						rows.push( rowIdx );
					}
				}
		
				// So the array reference doesn't break set the results into the
				// existing array
				displayRows.length = 0;
				$.merge( displayRows, rows );
			}
		}
		
		
		/**
		 * Filter the table on a per-column basis
		 *  @param {object} oSettings dataTables settings object
		 *  @param {string} sInput string to filter on
		 *  @param {int} iColumn column to filter
		 *  @param {bool} bRegex treat search string as a regular expression or not
		 *  @param {bool} bSmart use smart filtering or not
		 *  @param {bool} bCaseInsensitive Do case insenstive matching or not
		 *  @memberof DataTable#oApi
		 */
		function _fnFilterColumn ( settings, searchStr, colIdx, regex, smart, caseInsensitive )
		{
			if ( searchStr === '' ) {
				return;
			}
		
			var data;
			var display = settings.aiDisplay;
			var rpSearch = _fnFilterCreateSearch( searchStr, regex, smart, caseInsensitive );
		
			for ( var i=display.length-1 ; i>=0 ; i-- ) {
				data = settings.aoData[ display[i] ]._aFilterData[ colIdx ];
		
				if ( ! rpSearch.test( data ) ) {
					display.splice( i, 1 );
				}
			}
		}
		
		
		/**
		 * Filter the data table based on user input and draw the table
		 *  @param {object} settings dataTables settings object
		 *  @param {string} input string to filter on
		 *  @param {int} force optional - force a research of the master array (1) or not (undefined or 0)
		 *  @param {bool} regex treat as a regular expression or not
		 *  @param {bool} smart perform smart filtering or not
		 *  @param {bool} caseInsensitive Do case insenstive matching or not
		 *  @memberof DataTable#oApi
		 */
		function _fnFilter( settings, input, force, regex, smart, caseInsensitive )
		{
			var rpSearch = _fnFilterCreateSearch( input, regex, smart, caseInsensitive );
			var prevSearch = settings.oPreviousSearch.sSearch;
			var displayMaster = settings.aiDisplayMaster;
			var display, invalidated, i;
		
			// Need to take account of custom filtering functions - always filter
			if ( DataTable.ext.search.length !== 0 ) {
				force = true;
			}
		
			// Check if any of the rows were invalidated
			invalidated = _fnFilterData( settings );
		
			// If the input is blank - we just want the full data set
			if ( input.length <= 0 ) {
				settings.aiDisplay = displayMaster.slice();
			}
			else {
				// New search - start from the master array
				if ( invalidated ||
					 force ||
					 prevSearch.length > input.length ||
					 input.indexOf(prevSearch) !== 0 ||
					 settings.bSorted // On resort, the display master needs to be
					                  // re-filtered since indexes will have changed
				) {
					settings.aiDisplay = displayMaster.slice();
				}
		
				// Search the display array
				display = settings.aiDisplay;
		
				for ( i=display.length-1 ; i>=0 ; i-- ) {
					if ( ! rpSearch.test( settings.aoData[ display[i] ]._sFilterRow ) ) {
						display.splice( i, 1 );
					}
				}
			}
		}
		
		
		/**
		 * Build a regular expression object suitable for searching a table
		 *  @param {string} sSearch string to search for
		 *  @param {bool} bRegex treat as a regular expression or not
		 *  @param {bool} bSmart perform smart filtering or not
		 *  @param {bool} bCaseInsensitive Do case insensitive matching or not
		 *  @returns {RegExp} constructed object
		 *  @memberof DataTable#oApi
		 */
		function _fnFilterCreateSearch( search, regex, smart, caseInsensitive )
		{
			search = regex ?
				search :
				_fnEscapeRegex( search );
			
			if ( smart ) {
				/* For smart filtering we want to allow the search to work regardless of
				 * word order. We also want double quoted text to be preserved, so word
				 * order is important - a la google. So this is what we want to
				 * generate:
				 * 
				 * ^(?=.*?\bone\b)(?=.*?\btwo three\b)(?=.*?\bfour\b).*$
				 */
				var a = $.map( search.match( /"[^"]+"|[^ ]+/g ) || [''], function ( word ) {
					if ( word.charAt(0) === '"' ) {
						var m = word.match( /^"(.*)"$/ );
						word = m ? m[1] : word;
					}
		
					return word.replace('"', '');
				} );
		
				search = '^(?=.*?'+a.join( ')(?=.*?' )+').*$';
			}
		
			return new RegExp( search, caseInsensitive ? 'i' : '' );
		}
		
		
		/**
		 * Escape a string such that it can be used in a regular expression
		 *  @param {string} sVal string to escape
		 *  @returns {string} escaped string
		 *  @memberof DataTable#oApi
		 */
		var _fnEscapeRegex = DataTable.util.escapeRegex;
		
		var __filter_div = $('<div>')[0];
		var __filter_div_textContent = __filter_div.textContent !== undefined;
		
		// Update the filtering data for each row if needed (by invalidation or first run)
		function _fnFilterData ( settings )
		{
			var columns = settings.aoColumns;
			var column;
			var i, j, ien, jen, filterData, cellData, row;
			var fomatters = DataTable.ext.type.search;
			var wasInvalidated = false;
		
			for ( i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
				row = settings.aoData[i];
		
				if ( ! row._aFilterData ) {
					filterData = [];
		
					for ( j=0, jen=columns.length ; j<jen ; j++ ) {
						column = columns[j];
		
						if ( column.bSearchable ) {
							cellData = _fnGetCellData( settings, i, j, 'filter' );
		
							if ( fomatters[ column.sType ] ) {
								cellData = fomatters[ column.sType ]( cellData );
							}
		
							// Search in DataTables 1.10 is string based. In 1.11 this
							// should be altered to also allow strict type checking.
							if ( cellData === null ) {
								cellData = '';
							}
		
							if ( typeof cellData !== 'string' && cellData.toString ) {
								cellData = cellData.toString();
							}
						}
						else {
							cellData = '';
						}
		
						// If it looks like there is an HTML entity in the string,
						// attempt to decode it so sorting works as expected. Note that
						// we could use a single line of jQuery to do this, but the DOM
						// method used here is much faster http://jsperf.com/html-decode
						if ( cellData.indexOf && cellData.indexOf('&') !== -1 ) {
							__filter_div.innerHTML = cellData;
							cellData = __filter_div_textContent ?
								__filter_div.textContent :
								__filter_div.innerText;
						}
		
						if ( cellData.replace ) {
							cellData = cellData.replace(/[\r\n]/g, '');
						}
		
						filterData.push( cellData );
					}
		
					row._aFilterData = filterData;
					row._sFilterRow = filterData.join('  ');
					wasInvalidated = true;
				}
			}
		
			return wasInvalidated;
		}
		
		
		/**
		 * Convert from the internal Hungarian notation to camelCase for external
		 * interaction
		 *  @param {object} obj Object to convert
		 *  @returns {object} Inverted object
		 *  @memberof DataTable#oApi
		 */
		function _fnSearchToCamel ( obj )
		{
			return {
				search:          obj.sSearch,
				smart:           obj.bSmart,
				regex:           obj.bRegex,
				caseInsensitive: obj.bCaseInsensitive
			};
		}
		
		
		
		/**
		 * Convert from camelCase notation to the internal Hungarian. We could use the
		 * Hungarian convert function here, but this is cleaner
		 *  @param {object} obj Object to convert
		 *  @returns {object} Inverted object
		 *  @memberof DataTable#oApi
		 */
		function _fnSearchToHung ( obj )
		{
			return {
				sSearch:          obj.search,
				bSmart:           obj.smart,
				bRegex:           obj.regex,
				bCaseInsensitive: obj.caseInsensitive
			};
		}
		
		/**
		 * Generate the node required for the info display
		 *  @param {object} oSettings dataTables settings object
		 *  @returns {node} Information element
		 *  @memberof DataTable#oApi
		 */
		function _fnFeatureHtmlInfo ( settings )
		{
			var
				tid = settings.sTableId,
				nodes = settings.aanFeatures.i,
				n = $('<div/>', {
					'class': settings.oClasses.sInfo,
					'id': ! nodes ? tid+'_info' : null
				} );
		
			if ( ! nodes ) {
				// Update display on each draw
				settings.aoDrawCallback.push( {
					"fn": _fnUpdateInfo,
					"sName": "information"
				} );
		
				n
					.attr( 'role', 'status' )
					.attr( 'aria-live', 'polite' );
		
				// Table is described by our info div
				$(settings.nTable).attr( 'aria-describedby', tid+'_info' );
			}
		
			return n[0];
		}
		
		
		/**
		 * Update the information elements in the display
		 *  @param {object} settings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnUpdateInfo ( settings )
		{
			/* Show information about the table */
			var nodes = settings.aanFeatures.i;
			if ( nodes.length === 0 ) {
				return;
			}
		
			var
				lang  = settings.oLanguage,
				start = settings._iDisplayStart+1,
				end   = settings.fnDisplayEnd(),
				max   = settings.fnRecordsTotal(),
				total = settings.fnRecordsDisplay(),
				out   = total ?
					lang.sInfo :
					lang.sInfoEmpty;
		
			if ( total !== max ) {
				/* Record set after filtering */
				out += ' ' + lang.sInfoFiltered;
			}
		
			// Convert the macros
			out += lang.sInfoPostFix;
			out = _fnInfoMacros( settings, out );
		
			var callback = lang.fnInfoCallback;
			if ( callback !== null ) {
				out = callback.call( settings.oInstance,
					settings, start, end, max, total, out
				);
			}
		
			$(nodes).html( out );
		}
		
		
		function _fnInfoMacros ( settings, str )
		{
			// When infinite scrolling, we are always starting at 1. _iDisplayStart is used only
			// internally
			var
				formatter  = settings.fnFormatNumber,
				start      = settings._iDisplayStart+1,
				len        = settings._iDisplayLength,
				vis        = settings.fnRecordsDisplay(),
				all        = len === -1;
		
			return str.
				replace(/_START_/g, formatter.call( settings, start ) ).
				replace(/_END_/g,   formatter.call( settings, settings.fnDisplayEnd() ) ).
				replace(/_MAX_/g,   formatter.call( settings, settings.fnRecordsTotal() ) ).
				replace(/_TOTAL_/g, formatter.call( settings, vis ) ).
				replace(/_PAGE_/g,  formatter.call( settings, all ? 1 : Math.ceil( start / len ) ) ).
				replace(/_PAGES_/g, formatter.call( settings, all ? 1 : Math.ceil( vis / len ) ) );
		}
		
		
		
		/**
		 * Draw the table for the first time, adding all required features
		 *  @param {object} settings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnInitialise ( settings )
		{
			var i, iLen, iAjaxStart=settings.iInitDisplayStart;
			var columns = settings.aoColumns, column;
			var features = settings.oFeatures;
			var deferLoading = settings.bDeferLoading; // value modified by the draw
		
			/* Ensure that the table data is fully initialised */
			if ( ! settings.bInitialised ) {
				setTimeout( function(){ _fnInitialise( settings ); }, 200 );
				return;
			}
		
			/* Show the display HTML options */
			_fnAddOptionsHtml( settings );
		
			/* Build and draw the header / footer for the table */
			_fnBuildHead( settings );
			_fnDrawHead( settings, settings.aoHeader );
			_fnDrawHead( settings, settings.aoFooter );
		
			/* Okay to show that something is going on now */
			_fnProcessingDisplay( settings, true );
		
			/* Calculate sizes for columns */
			if ( features.bAutoWidth ) {
				_fnCalculateColumnWidths( settings );
			}
		
			for ( i=0, iLen=columns.length ; i<iLen ; i++ ) {
				column = columns[i];
		
				if ( column.sWidth ) {
					column.nTh.style.width = _fnStringToCss( column.sWidth );
				}
			}
		
			_fnCallbackFire( settings, null, 'preInit', [settings] );
		
			// If there is default sorting required - let's do it. The sort function
			// will do the drawing for us. Otherwise we draw the table regardless of the
			// Ajax source - this allows the table to look initialised for Ajax sourcing
			// data (show 'loading' message possibly)
			_fnReDraw( settings );
		
			// Server-side processing init complete is done by _fnAjaxUpdateDraw
			var dataSrc = _fnDataSource( settings );
			if ( dataSrc != 'ssp' || deferLoading ) {
				// if there is an ajax source load the data
				if ( dataSrc == 'ajax' ) {
					_fnBuildAjax( settings, [], function(json) {
						var aData = _fnAjaxDataSrc( settings, json );
		
						// Got the data - add it to the table
						for ( i=0 ; i<aData.length ; i++ ) {
							_fnAddData( settings, aData[i] );
						}
		
						// Reset the init display for cookie saving. We've already done
						// a filter, and therefore cleared it before. So we need to make
						// it appear 'fresh'
						settings.iInitDisplayStart = iAjaxStart;
		
						_fnReDraw( settings );
		
						_fnProcessingDisplay( settings, false );
						_fnInitComplete( settings, json );
					}, settings );
				}
				else {
					_fnProcessingDisplay( settings, false );
					_fnInitComplete( settings );
				}
			}
		}
		
		
		/**
		 * Draw the table for the first time, adding all required features
		 *  @param {object} oSettings dataTables settings object
		 *  @param {object} [json] JSON from the server that completed the table, if using Ajax source
		 *    with client-side processing (optional)
		 *  @memberof DataTable#oApi
		 */
		function _fnInitComplete ( settings, json )
		{
			settings._bInitComplete = true;
		
			// When data was added after the initialisation (data or Ajax) we need to
			// calculate the column sizing
			if ( json || settings.oInit.aaData ) {
				_fnAdjustColumnSizing( settings );
			}
		
			_fnCallbackFire( settings, null, 'plugin-init', [settings, json] );
			_fnCallbackFire( settings, 'aoInitComplete', 'init', [settings, json] );
		}
		
		
		function _fnLengthChange ( settings, val )
		{
			var len = parseInt( val, 10 );
			settings._iDisplayLength = len;
		
			_fnLengthOverflow( settings );
		
			// Fire length change event
			_fnCallbackFire( settings, null, 'length', [settings, len] );
		}
		
		
		/**
		 * Generate the node required for user display length changing
		 *  @param {object} settings dataTables settings object
		 *  @returns {node} Display length feature node
		 *  @memberof DataTable#oApi
		 */
		function _fnFeatureHtmlLength ( settings )
		{
			var
				classes  = settings.oClasses,
				tableId  = settings.sTableId,
				menu     = settings.aLengthMenu,
				d2       = $.isArray( menu[0] ),
				lengths  = d2 ? menu[0] : menu,
				language = d2 ? menu[1] : menu;
		
			var select = $('<select/>', {
				'name':          tableId+'_length',
				'aria-controls': tableId,
				'class':         classes.sLengthSelect
			} );
		
			for ( var i=0, ien=lengths.length ; i<ien ; i++ ) {
				select[0][ i ] = new Option( language[i], lengths[i] );
			}
		
			var div = $('<div><label/></div>').addClass( classes.sLength );
			if ( ! settings.aanFeatures.l ) {
				div[0].id = tableId+'_length';
			}
		
			div.children().append(
				settings.oLanguage.sLengthMenu.replace( '_MENU_', select[0].outerHTML )
			);
		
			// Can't use `select` variable as user might provide their own and the
			// reference is broken by the use of outerHTML
			$('select', div)
				.val( settings._iDisplayLength )
				.bind( 'change.DT', function(e) {
					_fnLengthChange( settings, $(this).val() );
					_fnDraw( settings );
				} );
		
			// Update node value whenever anything changes the table's length
			$(settings.nTable).bind( 'length.dt.DT', function (e, s, len) {
				if ( settings === s ) {
					$('select', div).val( len );
				}
			} );
		
			return div[0];
		}
		
		
		
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * Note that most of the paging logic is done in
		 * DataTable.ext.pager
		 */
		
		/**
		 * Generate the node required for default pagination
		 *  @param {object} oSettings dataTables settings object
		 *  @returns {node} Pagination feature node
		 *  @memberof DataTable#oApi
		 */
		function _fnFeatureHtmlPaginate ( settings )
		{
			var
				type   = settings.sPaginationType,
				plugin = DataTable.ext.pager[ type ],
				modern = typeof plugin === 'function',
				redraw = function( settings ) {
					_fnDraw( settings );
				},
				node = $('<div/>').addClass( settings.oClasses.sPaging + type )[0],
				features = settings.aanFeatures;
		
			if ( ! modern ) {
				plugin.fnInit( settings, node, redraw );
			}
		
			/* Add a draw callback for the pagination on first instance, to update the paging display */
			if ( ! features.p )
			{
				node.id = settings.sTableId+'_paginate';
		
				settings.aoDrawCallback.push( {
					"fn": function( settings ) {
						if ( modern ) {
							var
								start      = settings._iDisplayStart,
								len        = settings._iDisplayLength,
								visRecords = settings.fnRecordsDisplay(),
								all        = len === -1,
								page = all ? 0 : Math.ceil( start / len ),
								pages = all ? 1 : Math.ceil( visRecords / len ),
								buttons = plugin(page, pages),
								i, ien;
		
							for ( i=0, ien=features.p.length ; i<ien ; i++ ) {
								_fnRenderer( settings, 'pageButton' )(
									settings, features.p[i], i, buttons, page, pages
								);
							}
						}
						else {
							plugin.fnUpdate( settings, redraw );
						}
					},
					"sName": "pagination"
				} );
			}
		
			return node;
		}
		
		
		/**
		 * Alter the display settings to change the page
		 *  @param {object} settings DataTables settings object
		 *  @param {string|int} action Paging action to take: "first", "previous",
		 *    "next" or "last" or page number to jump to (integer)
		 *  @param [bool] redraw Automatically draw the update or not
		 *  @returns {bool} true page has changed, false - no change
		 *  @memberof DataTable#oApi
		 */
		function _fnPageChange ( settings, action, redraw )
		{
			var
				start     = settings._iDisplayStart,
				len       = settings._iDisplayLength,
				records   = settings.fnRecordsDisplay();
		
			if ( records === 0 || len === -1 )
			{
				start = 0;
			}
			else if ( typeof action === "number" )
			{
				start = action * len;
		
				if ( start > records )
				{
					start = 0;
				}
			}
			else if ( action == "first" )
			{
				start = 0;
			}
			else if ( action == "previous" )
			{
				start = len >= 0 ?
					start - len :
					0;
		
				if ( start < 0 )
				{
				  start = 0;
				}
			}
			else if ( action == "next" )
			{
				if ( start + len < records )
				{
					start += len;
				}
			}
			else if ( action == "last" )
			{
				start = Math.floor( (records-1) / len) * len;
			}
			else
			{
				_fnLog( settings, 0, "Unknown paging action: "+action, 5 );
			}
		
			var changed = settings._iDisplayStart !== start;
			settings._iDisplayStart = start;
		
			if ( changed ) {
				_fnCallbackFire( settings, null, 'page', [settings] );
		
				if ( redraw ) {
					_fnDraw( settings );
				}
			}
		
			return changed;
		}
		
		
		
		/**
		 * Generate the node required for the processing node
		 *  @param {object} settings dataTables settings object
		 *  @returns {node} Processing element
		 *  @memberof DataTable#oApi
		 */
		function _fnFeatureHtmlProcessing ( settings )
		{
			return $('<div/>', {
					'id': ! settings.aanFeatures.r ? settings.sTableId+'_processing' : null,
					'class': settings.oClasses.sProcessing
				} )
				.html( settings.oLanguage.sProcessing )
				.insertBefore( settings.nTable )[0];
		}
		
		
		/**
		 * Display or hide the processing indicator
		 *  @param {object} settings dataTables settings object
		 *  @param {bool} show Show the processing indicator (true) or not (false)
		 *  @memberof DataTable#oApi
		 */
		function _fnProcessingDisplay ( settings, show )
		{
			if ( settings.oFeatures.bProcessing ) {
				$(settings.aanFeatures.r).css( 'display', show ? 'block' : 'none' );
			}
		
			_fnCallbackFire( settings, null, 'processing', [settings, show] );
		}
		
		/**
		 * Add any control elements for the table - specifically scrolling
		 *  @param {object} settings dataTables settings object
		 *  @returns {node} Node to add to the DOM
		 *  @memberof DataTable#oApi
		 */
		function _fnFeatureHtmlTable ( settings )
		{
			var table = $(settings.nTable);
		
			// Add the ARIA grid role to the table
			table.attr( 'role', 'grid' );
		
			// Scrolling from here on in
			var scroll = settings.oScroll;
		
			if ( scroll.sX === '' && scroll.sY === '' ) {
				return settings.nTable;
			}
		
			var scrollX = scroll.sX;
			var scrollY = scroll.sY;
			var classes = settings.oClasses;
			var caption = table.children('caption');
			var captionSide = caption.length ? caption[0]._captionSide : null;
			var headerClone = $( table[0].cloneNode(false) );
			var footerClone = $( table[0].cloneNode(false) );
			var footer = table.children('tfoot');
			var _div = '<div/>';
			var size = function ( s ) {
				return !s ? null : _fnStringToCss( s );
			};
		
			if ( ! footer.length ) {
				footer = null;
			}
		
			/*
			 * The HTML structure that we want to generate in this function is:
			 *  div - scroller
			 *    div - scroll head
			 *      div - scroll head inner
			 *        table - scroll head table
			 *          thead - thead
			 *    div - scroll body
			 *      table - table (master table)
			 *        thead - thead clone for sizing
			 *        tbody - tbody
			 *    div - scroll foot
			 *      div - scroll foot inner
			 *        table - scroll foot table
			 *          tfoot - tfoot
			 */
			var scroller = $( _div, { 'class': classes.sScrollWrapper } )
				.append(
					$(_div, { 'class': classes.sScrollHead } )
						.css( {
							overflow: 'hidden',
							position: 'relative',
							border: 0,
							width: scrollX ? size(scrollX) : '100%'
						} )
						.append(
							$(_div, { 'class': classes.sScrollHeadInner } )
								.css( {
									'box-sizing': 'content-box',
									width: scroll.sXInner || '100%'
								} )
								.append(
									headerClone
										.removeAttr('id')
										.css( 'margin-left', 0 )
										.append( captionSide === 'top' ? caption : null )
										.append(
											table.children('thead')
										)
								)
						)
				)
				.append(
					$(_div, { 'class': classes.sScrollBody } )
						.css( {
							position: 'relative',
							overflow: 'auto',
							width: size( scrollX )
						} )
						.append( table )
				);
		
			if ( footer ) {
				scroller.append(
					$(_div, { 'class': classes.sScrollFoot } )
						.css( {
							overflow: 'hidden',
							border: 0,
							width: scrollX ? size(scrollX) : '100%'
						} )
						.append(
							$(_div, { 'class': classes.sScrollFootInner } )
								.append(
									footerClone
										.removeAttr('id')
										.css( 'margin-left', 0 )
										.append( captionSide === 'bottom' ? caption : null )
										.append(
											table.children('tfoot')
										)
								)
						)
				);
			}
		
			var children = scroller.children();
			var scrollHead = children[0];
			var scrollBody = children[1];
			var scrollFoot = footer ? children[2] : null;
		
			// When the body is scrolled, then we also want to scroll the headers
			if ( scrollX ) {
				$(scrollBody).on( 'scroll.DT', function (e) {
					var scrollLeft = this.scrollLeft;
		
					scrollHead.scrollLeft = scrollLeft;
		
					if ( footer ) {
						scrollFoot.scrollLeft = scrollLeft;
					}
				} );
			}
		
			$(scrollBody).css(
				scrollY && scroll.bCollapse ? 'max-height' : 'height', 
				scrollY
			);
		
			settings.nScrollHead = scrollHead;
			settings.nScrollBody = scrollBody;
			settings.nScrollFoot = scrollFoot;
		
			// On redraw - align columns
			settings.aoDrawCallback.push( {
				"fn": _fnScrollDraw,
				"sName": "scrolling"
			} );
		
			return scroller[0];
		}
		
		
		
		/**
		 * Update the header, footer and body tables for resizing - i.e. column
		 * alignment.
		 *
		 * Welcome to the most horrible function DataTables. The process that this
		 * function follows is basically:
		 *   1. Re-create the table inside the scrolling div
		 *   2. Take live measurements from the DOM
		 *   3. Apply the measurements to align the columns
		 *   4. Clean up
		 *
		 *  @param {object} settings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnScrollDraw ( settings )
		{
			// Given that this is such a monster function, a lot of variables are use
			// to try and keep the minimised size as small as possible
			var
				scroll         = settings.oScroll,
				scrollX        = scroll.sX,
				scrollXInner   = scroll.sXInner,
				scrollY        = scroll.sY,
				barWidth       = scroll.iBarWidth,
				divHeader      = $(settings.nScrollHead),
				divHeaderStyle = divHeader[0].style,
				divHeaderInner = divHeader.children('div'),
				divHeaderInnerStyle = divHeaderInner[0].style,
				divHeaderTable = divHeaderInner.children('table'),
				divBodyEl      = settings.nScrollBody,
				divBody        = $(divBodyEl),
				divBodyStyle   = divBodyEl.style,
				divFooter      = $(settings.nScrollFoot),
				divFooterInner = divFooter.children('div'),
				divFooterTable = divFooterInner.children('table'),
				header         = $(settings.nTHead),
				table          = $(settings.nTable),
				tableEl        = table[0],
				tableStyle     = tableEl.style,
				footer         = settings.nTFoot ? $(settings.nTFoot) : null,
				browser        = settings.oBrowser,
				ie67           = browser.bScrollOversize,
				dtHeaderCells  = _pluck( settings.aoColumns, 'nTh' ),
				headerTrgEls, footerTrgEls,
				headerSrcEls, footerSrcEls,
				headerCopy, footerCopy,
				headerWidths=[], footerWidths=[],
				headerContent=[], footerContent=[],
				idx, correction, sanityWidth,
				zeroOut = function(nSizer) {
					var style = nSizer.style;
					style.paddingTop = "0";
					style.paddingBottom = "0";
					style.borderTopWidth = "0";
					style.borderBottomWidth = "0";
					style.height = 0;
				};
		
			// If the scrollbar visibility has changed from the last draw, we need to
			// adjust the column sizes as the table width will have changed to account
			// for the scrollbar
			var scrollBarVis = divBodyEl.scrollHeight > divBodyEl.clientHeight;
			
			if ( settings.scrollBarVis !== scrollBarVis && settings.scrollBarVis !== undefined ) {
				settings.scrollBarVis = scrollBarVis;
				_fnAdjustColumnSizing( settings );
				return; // adjust column sizing will call this function again
			}
			else {
				settings.scrollBarVis = scrollBarVis;
			}
		
			/*
			 * 1. Re-create the table inside the scrolling div
			 */
		
			// Remove the old minimised thead and tfoot elements in the inner table
			table.children('thead, tfoot').remove();
		
			if ( footer ) {
				footerCopy = footer.clone().prependTo( table );
				footerTrgEls = footer.find('tr'); // the original tfoot is in its own table and must be sized
				footerSrcEls = footerCopy.find('tr');
			}
		
			// Clone the current header and footer elements and then place it into the inner table
			headerCopy = header.clone().prependTo( table );
			headerTrgEls = header.find('tr'); // original header is in its own table
			headerSrcEls = headerCopy.find('tr');
			headerCopy.find('th, td').removeAttr('tabindex');
		
		
			/*
			 * 2. Take live measurements from the DOM - do not alter the DOM itself!
			 */
		
			// Remove old sizing and apply the calculated column widths
			// Get the unique column headers in the newly created (cloned) header. We want to apply the
			// calculated sizes to this header
			if ( ! scrollX )
			{
				divBodyStyle.width = '100%';
				divHeader[0].style.width = '100%';
			}
		
			$.each( _fnGetUniqueThs( settings, headerCopy ), function ( i, el ) {
				idx = _fnVisibleToColumnIndex( settings, i );
				el.style.width = settings.aoColumns[idx].sWidth;
			} );
		
			if ( footer ) {
				_fnApplyToChildren( function(n) {
					n.style.width = "";
				}, footerSrcEls );
			}
		
			// Size the table as a whole
			sanityWidth = table.outerWidth();
			if ( scrollX === "" ) {
				// No x scrolling
				tableStyle.width = "100%";
		
				// IE7 will make the width of the table when 100% include the scrollbar
				// - which is shouldn't. When there is a scrollbar we need to take this
				// into account.
				if ( ie67 && (table.find('tbody').height() > divBodyEl.offsetHeight ||
					divBody.css('overflow-y') == "scroll")
				) {
					tableStyle.width = _fnStringToCss( table.outerWidth() - barWidth);
				}
		
				// Recalculate the sanity width
				sanityWidth = table.outerWidth();
			}
			else if ( scrollXInner !== "" ) {
				// legacy x scroll inner has been given - use it
				tableStyle.width = _fnStringToCss(scrollXInner);
		
				// Recalculate the sanity width
				sanityWidth = table.outerWidth();
			}
		
			// Hidden header should have zero height, so remove padding and borders. Then
			// set the width based on the real headers
		
			// Apply all styles in one pass
			_fnApplyToChildren( zeroOut, headerSrcEls );
		
			// Read all widths in next pass
			_fnApplyToChildren( function(nSizer) {
				headerContent.push( nSizer.innerHTML );
				headerWidths.push( _fnStringToCss( $(nSizer).css('width') ) );
			}, headerSrcEls );
		
			// Apply all widths in final pass
			_fnApplyToChildren( function(nToSize, i) {
				// Only apply widths to the DataTables detected header cells - this
				// prevents complex headers from having contradictory sizes applied
				if ( $.inArray( nToSize, dtHeaderCells ) !== -1 ) {
					nToSize.style.width = headerWidths[i];
				}
			}, headerTrgEls );
		
			$(headerSrcEls).height(0);
		
			/* Same again with the footer if we have one */
			if ( footer )
			{
				_fnApplyToChildren( zeroOut, footerSrcEls );
		
				_fnApplyToChildren( function(nSizer) {
					footerContent.push( nSizer.innerHTML );
					footerWidths.push( _fnStringToCss( $(nSizer).css('width') ) );
				}, footerSrcEls );
		
				_fnApplyToChildren( function(nToSize, i) {
					nToSize.style.width = footerWidths[i];
				}, footerTrgEls );
		
				$(footerSrcEls).height(0);
			}
		
		
			/*
			 * 3. Apply the measurements
			 */
		
			// "Hide" the header and footer that we used for the sizing. We need to keep
			// the content of the cell so that the width applied to the header and body
			// both match, but we want to hide it completely. We want to also fix their
			// width to what they currently are
			_fnApplyToChildren( function(nSizer, i) {
				nSizer.innerHTML = '<div class="dataTables_sizing" style="height:0;overflow:hidden;">'+headerContent[i]+'</div>';
				nSizer.style.width = headerWidths[i];
			}, headerSrcEls );
		
			if ( footer )
			{
				_fnApplyToChildren( function(nSizer, i) {
					nSizer.innerHTML = '<div class="dataTables_sizing" style="height:0;overflow:hidden;">'+footerContent[i]+'</div>';
					nSizer.style.width = footerWidths[i];
				}, footerSrcEls );
			}
		
			// Sanity check that the table is of a sensible width. If not then we are going to get
			// misalignment - try to prevent this by not allowing the table to shrink below its min width
			if ( table.outerWidth() < sanityWidth )
			{
				// The min width depends upon if we have a vertical scrollbar visible or not */
				correction = ((divBodyEl.scrollHeight > divBodyEl.offsetHeight ||
					divBody.css('overflow-y') == "scroll")) ?
						sanityWidth+barWidth :
						sanityWidth;
		
				// IE6/7 are a law unto themselves...
				if ( ie67 && (divBodyEl.scrollHeight >
					divBodyEl.offsetHeight || divBody.css('overflow-y') == "scroll")
				) {
					tableStyle.width = _fnStringToCss( correction-barWidth );
				}
		
				// And give the user a warning that we've stopped the table getting too small
				if ( scrollX === "" || scrollXInner !== "" ) {
					_fnLog( settings, 1, 'Possible column misalignment', 6 );
				}
			}
			else
			{
				correction = '100%';
			}
		
			// Apply to the container elements
			divBodyStyle.width = _fnStringToCss( correction );
			divHeaderStyle.width = _fnStringToCss( correction );
		
			if ( footer ) {
				settings.nScrollFoot.style.width = _fnStringToCss( correction );
			}
		
		
			/*
			 * 4. Clean up
			 */
			if ( ! scrollY ) {
				/* IE7< puts a vertical scrollbar in place (when it shouldn't be) due to subtracting
				 * the scrollbar height from the visible display, rather than adding it on. We need to
				 * set the height in order to sort this. Don't want to do it in any other browsers.
				 */
				if ( ie67 ) {
					divBodyStyle.height = _fnStringToCss( tableEl.offsetHeight+barWidth );
				}
			}
		
			/* Finally set the width's of the header and footer tables */
			var iOuterWidth = table.outerWidth();
			divHeaderTable[0].style.width = _fnStringToCss( iOuterWidth );
			divHeaderInnerStyle.width = _fnStringToCss( iOuterWidth );
		
			// Figure out if there are scrollbar present - if so then we need a the header and footer to
			// provide a bit more space to allow "overflow" scrolling (i.e. past the scrollbar)
			var bScrolling = table.height() > divBodyEl.clientHeight || divBody.css('overflow-y') == "scroll";
			var padding = 'padding' + (browser.bScrollbarLeft ? 'Left' : 'Right' );
			divHeaderInnerStyle[ padding ] = bScrolling ? barWidth+"px" : "0px";
		
			if ( footer ) {
				divFooterTable[0].style.width = _fnStringToCss( iOuterWidth );
				divFooterInner[0].style.width = _fnStringToCss( iOuterWidth );
				divFooterInner[0].style[padding] = bScrolling ? barWidth+"px" : "0px";
			}
		
			// Correct DOM ordering for colgroup - comes before the thead
			table.children('colgroup').insertBefore( table.children('thead') );
		
			/* Adjust the position of the header in case we loose the y-scrollbar */
			divBody.scroll();
		
			// If sorting or filtering has occurred, jump the scrolling back to the top
			// only if we aren't holding the position
			if ( (settings.bSorted || settings.bFiltered) && ! settings._drawHold ) {
				divBodyEl.scrollTop = 0;
			}
		}
		
		
		
		/**
		 * Apply a given function to the display child nodes of an element array (typically
		 * TD children of TR rows
		 *  @param {function} fn Method to apply to the objects
		 *  @param array {nodes} an1 List of elements to look through for display children
		 *  @param array {nodes} an2 Another list (identical structure to the first) - optional
		 *  @memberof DataTable#oApi
		 */
		function _fnApplyToChildren( fn, an1, an2 )
		{
			var index=0, i=0, iLen=an1.length;
			var nNode1, nNode2;
		
			while ( i < iLen ) {
				nNode1 = an1[i].firstChild;
				nNode2 = an2 ? an2[i].firstChild : null;
		
				while ( nNode1 ) {
					if ( nNode1.nodeType === 1 ) {
						if ( an2 ) {
							fn( nNode1, nNode2, index );
						}
						else {
							fn( nNode1, index );
						}
		
						index++;
					}
		
					nNode1 = nNode1.nextSibling;
					nNode2 = an2 ? nNode2.nextSibling : null;
				}
		
				i++;
			}
		}
		
		
		
		var __re_html_remove = /<.*?>/g;
		
		
		/**
		 * Calculate the width of columns for the table
		 *  @param {object} oSettings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnCalculateColumnWidths ( oSettings )
		{
			var
				table = oSettings.nTable,
				columns = oSettings.aoColumns,
				scroll = oSettings.oScroll,
				scrollY = scroll.sY,
				scrollX = scroll.sX,
				scrollXInner = scroll.sXInner,
				columnCount = columns.length,
				visibleColumns = _fnGetColumns( oSettings, 'bVisible' ),
				headerCells = $('th', oSettings.nTHead),
				tableWidthAttr = table.getAttribute('width'), // from DOM element
				tableContainer = table.parentNode,
				userInputs = false,
				i, column, columnIdx, width, outerWidth,
				browser = oSettings.oBrowser,
				ie67 = browser.bScrollOversize;
		
			var styleWidth = table.style.width;
			if ( styleWidth && styleWidth.indexOf('%') !== -1 ) {
				tableWidthAttr = styleWidth;
			}
		
			/* Convert any user input sizes into pixel sizes */
			for ( i=0 ; i<visibleColumns.length ; i++ ) {
				column = columns[ visibleColumns[i] ];
		
				if ( column.sWidth !== null ) {
					column.sWidth = _fnConvertToWidth( column.sWidthOrig, tableContainer );
		
					userInputs = true;
				}
			}
		
			/* If the number of columns in the DOM equals the number that we have to
			 * process in DataTables, then we can use the offsets that are created by
			 * the web- browser. No custom sizes can be set in order for this to happen,
			 * nor scrolling used
			 */
			if ( ie67 || ! userInputs && ! scrollX && ! scrollY &&
			     columnCount == _fnVisbleColumns( oSettings ) &&
			     columnCount == headerCells.length
			) {
				for ( i=0 ; i<columnCount ; i++ ) {
					var colIdx = _fnVisibleToColumnIndex( oSettings, i );
		
					if ( colIdx !== null ) {
						columns[ colIdx ].sWidth = _fnStringToCss( headerCells.eq(i).width() );
					}
				}
			}
			else
			{
				// Otherwise construct a single row, worst case, table with the widest
				// node in the data, assign any user defined widths, then insert it into
				// the DOM and allow the browser to do all the hard work of calculating
				// table widths
				var tmpTable = $(table).clone() // don't use cloneNode - IE8 will remove events on the main table
					.css( 'visibility', 'hidden' )
					.removeAttr( 'id' );
		
				// Clean up the table body
				tmpTable.find('tbody tr').remove();
				var tr = $('<tr/>').appendTo( tmpTable.find('tbody') );
		
				// Clone the table header and footer - we can't use the header / footer
				// from the cloned table, since if scrolling is active, the table's
				// real header and footer are contained in different table tags
				tmpTable.find('thead, tfoot').remove();
				tmpTable
					.append( $(oSettings.nTHead).clone() )
					.append( $(oSettings.nTFoot).clone() );
		
				// Remove any assigned widths from the footer (from scrolling)
				tmpTable.find('tfoot th, tfoot td').css('width', '');
		
				// Apply custom sizing to the cloned header
				headerCells = _fnGetUniqueThs( oSettings, tmpTable.find('thead')[0] );
		
				for ( i=0 ; i<visibleColumns.length ; i++ ) {
					column = columns[ visibleColumns[i] ];
		
					headerCells[i].style.width = column.sWidthOrig !== null && column.sWidthOrig !== '' ?
						_fnStringToCss( column.sWidthOrig ) :
						'';
		
					// For scrollX we need to force the column width otherwise the
					// browser will collapse it. If this width is smaller than the
					// width the column requires, then it will have no effect
					if ( column.sWidthOrig && scrollX ) {
						$( headerCells[i] ).append( $('<div/>').css( {
							width: column.sWidthOrig,
							margin: 0,
							padding: 0,
							border: 0,
							height: 1
						} ) );
					}
				}
		
				// Find the widest cell for each column and put it into the table
				if ( oSettings.aoData.length ) {
					for ( i=0 ; i<visibleColumns.length ; i++ ) {
						columnIdx = visibleColumns[i];
						column = columns[ columnIdx ];
		
						$( _fnGetWidestNode( oSettings, columnIdx ) )
							.clone( false )
							.append( column.sContentPadding )
							.appendTo( tr );
					}
				}
		
				// Tidy the temporary table - remove name attributes so there aren't
				// duplicated in the dom (radio elements for example)
				$('[name]', tmpTable).removeAttr('name');
		
				// Table has been built, attach to the document so we can work with it.
				// A holding element is used, positioned at the top of the container
				// with minimal height, so it has no effect on if the container scrolls
				// or not. Otherwise it might trigger scrolling when it actually isn't
				// needed
				var holder = $('<div/>').css( scrollX || scrollY ?
						{
							position: 'absolute',
							top: 0,
							left: 0,
							height: 1,
							right: 0,
							overflow: 'hidden'
						} :
						{}
					)
					.append( tmpTable )
					.appendTo( tableContainer );
		
				// When scrolling (X or Y) we want to set the width of the table as 
				// appropriate. However, when not scrolling leave the table width as it
				// is. This results in slightly different, but I think correct behaviour
				if ( scrollX && scrollXInner ) {
					tmpTable.width( scrollXInner );
				}
				else if ( scrollX ) {
					tmpTable.css( 'width', 'auto' );
					tmpTable.removeAttr('width');
		
					// If there is no width attribute or style, then allow the table to
					// collapse
					if ( tmpTable.width() < tableContainer.clientWidth && tableWidthAttr ) {
						tmpTable.width( tableContainer.clientWidth );
					}
				}
				else if ( scrollY ) {
					tmpTable.width( tableContainer.clientWidth );
				}
				else if ( tableWidthAttr ) {
					tmpTable.width( tableWidthAttr );
				}
		
				// Get the width of each column in the constructed table - we need to
				// know the inner width (so it can be assigned to the other table's
				// cells) and the outer width so we can calculate the full width of the
				// table. This is safe since DataTables requires a unique cell for each
				// column, but if ever a header can span multiple columns, this will
				// need to be modified.
				var total = 0;
				for ( i=0 ; i<visibleColumns.length ; i++ ) {
					var cell = $(headerCells[i]);
					var border = cell.outerWidth() - cell.width();
		
					// Use getBounding... where possible (not IE8-) because it can give
					// sub-pixel accuracy, which we then want to round up!
					var bounding = browser.bBounding ?
						Math.ceil( headerCells[i].getBoundingClientRect().width ) :
						cell.outerWidth();
		
					// Total is tracked to remove any sub-pixel errors as the outerWidth
					// of the table might not equal the total given here (IE!).
					total += bounding;
		
					// Width for each column to use
					columns[ visibleColumns[i] ].sWidth = _fnStringToCss( bounding - border );
				}
		
				table.style.width = _fnStringToCss( total );
		
				// Finished with the table - ditch it
				holder.remove();
			}
		
			// If there is a width attr, we want to attach an event listener which
			// allows the table sizing to automatically adjust when the window is
			// resized. Use the width attr rather than CSS, since we can't know if the
			// CSS is a relative value or absolute - DOM read is always px.
			if ( tableWidthAttr ) {
				table.style.width = _fnStringToCss( tableWidthAttr );
			}
		
			if ( (tableWidthAttr || scrollX) && ! oSettings._reszEvt ) {
				var bindResize = function () {
					$(window).bind('resize.DT-'+oSettings.sInstance, _fnThrottle( function () {
						_fnAdjustColumnSizing( oSettings );
					} ) );
				};
		
				// IE6/7 will crash if we bind a resize event handler on page load.
				// To be removed in 1.11 which drops IE6/7 support
				if ( ie67 ) {
					setTimeout( bindResize, 1000 );
				}
				else {
					bindResize();
				}
		
				oSettings._reszEvt = true;
			}
		}
		
		
		/**
		 * Throttle the calls to a function. Arguments and context are maintained for
		 * the throttled function
		 *  @param {function} fn Function to be called
		 *  @param {int} [freq=200] call frequency in mS
		 *  @returns {function} wrapped function
		 *  @memberof DataTable#oApi
		 */
		var _fnThrottle = DataTable.util.throttle;
		
		
		/**
		 * Convert a CSS unit width to pixels (e.g. 2em)
		 *  @param {string} width width to be converted
		 *  @param {node} parent parent to get the with for (required for relative widths) - optional
		 *  @returns {int} width in pixels
		 *  @memberof DataTable#oApi
		 */
		function _fnConvertToWidth ( width, parent )
		{
			if ( ! width ) {
				return 0;
			}
		
			var n = $('<div/>')
				.css( 'width', _fnStringToCss( width ) )
				.appendTo( parent || document.body );
		
			var val = n[0].offsetWidth;
			n.remove();
		
			return val;
		}
		
		
		/**
		 * Get the widest node
		 *  @param {object} settings dataTables settings object
		 *  @param {int} colIdx column of interest
		 *  @returns {node} widest table node
		 *  @memberof DataTable#oApi
		 */
		function _fnGetWidestNode( settings, colIdx )
		{
			var idx = _fnGetMaxLenString( settings, colIdx );
			if ( idx < 0 ) {
				return null;
			}
		
			var data = settings.aoData[ idx ];
			return ! data.nTr ? // Might not have been created when deferred rendering
				$('<td/>').html( _fnGetCellData( settings, idx, colIdx, 'display' ) )[0] :
				data.anCells[ colIdx ];
		}
		
		
		/**
		 * Get the maximum strlen for each data column
		 *  @param {object} settings dataTables settings object
		 *  @param {int} colIdx column of interest
		 *  @returns {string} max string length for each column
		 *  @memberof DataTable#oApi
		 */
		function _fnGetMaxLenString( settings, colIdx )
		{
			var s, max=-1, maxIdx = -1;
		
			for ( var i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
				s = _fnGetCellData( settings, i, colIdx, 'display' )+'';
				s = s.replace( __re_html_remove, '' );
				s = s.replace( /&nbsp;/g, ' ' );
		
				if ( s.length > max ) {
					max = s.length;
					maxIdx = i;
				}
			}
		
			return maxIdx;
		}
		
		
		/**
		 * Append a CSS unit (only if required) to a string
		 *  @param {string} value to css-ify
		 *  @returns {string} value with css unit
		 *  @memberof DataTable#oApi
		 */
		function _fnStringToCss( s )
		{
			if ( s === null ) {
				return '0px';
			}
		
			if ( typeof s == 'number' ) {
				return s < 0 ?
					'0px' :
					s+'px';
			}
		
			// Check it has a unit character already
			return s.match(/\d$/) ?
				s+'px' :
				s;
		}
		
		
		
		function _fnSortFlatten ( settings )
		{
			var
				i, iLen, k, kLen,
				aSort = [],
				aiOrig = [],
				aoColumns = settings.aoColumns,
				aDataSort, iCol, sType, srcCol,
				fixed = settings.aaSortingFixed,
				fixedObj = $.isPlainObject( fixed ),
				nestedSort = [],
				add = function ( a ) {
					if ( a.length && ! $.isArray( a[0] ) ) {
						// 1D array
						nestedSort.push( a );
					}
					else {
						// 2D array
						$.merge( nestedSort, a );
					}
				};
		
			// Build the sort array, with pre-fix and post-fix options if they have been
			// specified
			if ( $.isArray( fixed ) ) {
				add( fixed );
			}
		
			if ( fixedObj && fixed.pre ) {
				add( fixed.pre );
			}
		
			add( settings.aaSorting );
		
			if (fixedObj && fixed.post ) {
				add( fixed.post );
			}
		
			for ( i=0 ; i<nestedSort.length ; i++ )
			{
				srcCol = nestedSort[i][0];
				aDataSort = aoColumns[ srcCol ].aDataSort;
		
				for ( k=0, kLen=aDataSort.length ; k<kLen ; k++ )
				{
					iCol = aDataSort[k];
					sType = aoColumns[ iCol ].sType || 'string';
		
					if ( nestedSort[i]._idx === undefined ) {
						nestedSort[i]._idx = $.inArray( nestedSort[i][1], aoColumns[iCol].asSorting );
					}
		
					aSort.push( {
						src:       srcCol,
						col:       iCol,
						dir:       nestedSort[i][1],
						index:     nestedSort[i]._idx,
						type:      sType,
						formatter: DataTable.ext.type.order[ sType+"-pre" ]
					} );
				}
			}
		
			return aSort;
		}
		
		/**
		 * Change the order of the table
		 *  @param {object} oSettings dataTables settings object
		 *  @memberof DataTable#oApi
		 *  @todo This really needs split up!
		 */
		function _fnSort ( oSettings )
		{
			var
				i, ien, iLen, j, jLen, k, kLen,
				sDataType, nTh,
				aiOrig = [],
				oExtSort = DataTable.ext.type.order,
				aoData = oSettings.aoData,
				aoColumns = oSettings.aoColumns,
				aDataSort, data, iCol, sType, oSort,
				formatters = 0,
				sortCol,
				displayMaster = oSettings.aiDisplayMaster,
				aSort;
		
			// Resolve any column types that are unknown due to addition or invalidation
			// @todo Can this be moved into a 'data-ready' handler which is called when
			//   data is going to be used in the table?
			_fnColumnTypes( oSettings );
		
			aSort = _fnSortFlatten( oSettings );
		
			for ( i=0, ien=aSort.length ; i<ien ; i++ ) {
				sortCol = aSort[i];
		
				// Track if we can use the fast sort algorithm
				if ( sortCol.formatter ) {
					formatters++;
				}
		
				// Load the data needed for the sort, for each cell
				_fnSortData( oSettings, sortCol.col );
			}
		
			/* No sorting required if server-side or no sorting array */
			if ( _fnDataSource( oSettings ) != 'ssp' && aSort.length !== 0 )
			{
				// Create a value - key array of the current row positions such that we can use their
				// current position during the sort, if values match, in order to perform stable sorting
				for ( i=0, iLen=displayMaster.length ; i<iLen ; i++ ) {
					aiOrig[ displayMaster[i] ] = i;
				}
		
				/* Do the sort - here we want multi-column sorting based on a given data source (column)
				 * and sorting function (from oSort) in a certain direction. It's reasonably complex to
				 * follow on it's own, but this is what we want (example two column sorting):
				 *  fnLocalSorting = function(a,b){
				 *    var iTest;
				 *    iTest = oSort['string-asc']('data11', 'data12');
				 *      if (iTest !== 0)
				 *        return iTest;
				 *    iTest = oSort['numeric-desc']('data21', 'data22');
				 *    if (iTest !== 0)
				 *      return iTest;
				 *    return oSort['numeric-asc']( aiOrig[a], aiOrig[b] );
				 *  }
				 * Basically we have a test for each sorting column, if the data in that column is equal,
				 * test the next column. If all columns match, then we use a numeric sort on the row
				 * positions in the original data array to provide a stable sort.
				 *
				 * Note - I know it seems excessive to have two sorting methods, but the first is around
				 * 15% faster, so the second is only maintained for backwards compatibility with sorting
				 * methods which do not have a pre-sort formatting function.
				 */
				if ( formatters === aSort.length ) {
					// All sort types have formatting functions
					displayMaster.sort( function ( a, b ) {
						var
							x, y, k, test, sort,
							len=aSort.length,
							dataA = aoData[a]._aSortData,
							dataB = aoData[b]._aSortData;
		
						for ( k=0 ; k<len ; k++ ) {
							sort = aSort[k];
		
							x = dataA[ sort.col ];
							y = dataB[ sort.col ];
		
							test = x<y ? -1 : x>y ? 1 : 0;
							if ( test !== 0 ) {
								return sort.dir === 'asc' ? test : -test;
							}
						}
		
						x = aiOrig[a];
						y = aiOrig[b];
						return x<y ? -1 : x>y ? 1 : 0;
					} );
				}
				else {
					// Depreciated - remove in 1.11 (providing a plug-in option)
					// Not all sort types have formatting methods, so we have to call their sorting
					// methods.
					displayMaster.sort( function ( a, b ) {
						var
							x, y, k, l, test, sort, fn,
							len=aSort.length,
							dataA = aoData[a]._aSortData,
							dataB = aoData[b]._aSortData;
		
						for ( k=0 ; k<len ; k++ ) {
							sort = aSort[k];
		
							x = dataA[ sort.col ];
							y = dataB[ sort.col ];
		
							fn = oExtSort[ sort.type+"-"+sort.dir ] || oExtSort[ "string-"+sort.dir ];
							test = fn( x, y );
							if ( test !== 0 ) {
								return test;
							}
						}
		
						x = aiOrig[a];
						y = aiOrig[b];
						return x<y ? -1 : x>y ? 1 : 0;
					} );
				}
			}
		
			/* Tell the draw function that we have sorted the data */
			oSettings.bSorted = true;
		}
		
		
		function _fnSortAria ( settings )
		{
			var label;
			var nextSort;
			var columns = settings.aoColumns;
			var aSort = _fnSortFlatten( settings );
			var oAria = settings.oLanguage.oAria;
		
			// ARIA attributes - need to loop all columns, to update all (removing old
			// attributes as needed)
			for ( var i=0, iLen=columns.length ; i<iLen ; i++ )
			{
				var col = columns[i];
				var asSorting = col.asSorting;
				var sTitle = col.sTitle.replace( /<.*?>/g, "" );
				var th = col.nTh;
		
				// IE7 is throwing an error when setting these properties with jQuery's
				// attr() and removeAttr() methods...
				th.removeAttribute('aria-sort');
		
				/* In ARIA only the first sorting column can be marked as sorting - no multi-sort option */
				if ( col.bSortable ) {
					if ( aSort.length > 0 && aSort[0].col == i ) {
						th.setAttribute('aria-sort', aSort[0].dir=="asc" ? "ascending" : "descending" );
						nextSort = asSorting[ aSort[0].index+1 ] || asSorting[0];
					}
					else {
						nextSort = asSorting[0];
					}
		
					label = sTitle + ( nextSort === "asc" ?
						oAria.sSortAscending :
						oAria.sSortDescending
					);
				}
				else {
					label = sTitle;
				}
		
				th.setAttribute('aria-label', label);
			}
		}
		
		
		/**
		 * Function to run on user sort request
		 *  @param {object} settings dataTables settings object
		 *  @param {node} attachTo node to attach the handler to
		 *  @param {int} colIdx column sorting index
		 *  @param {boolean} [append=false] Append the requested sort to the existing
		 *    sort if true (i.e. multi-column sort)
		 *  @param {function} [callback] callback function
		 *  @memberof DataTable#oApi
		 */
		function _fnSortListener ( settings, colIdx, append, callback )
		{
			var col = settings.aoColumns[ colIdx ];
			var sorting = settings.aaSorting;
			var asSorting = col.asSorting;
			var nextSortIdx;
			var next = function ( a, overflow ) {
				var idx = a._idx;
				if ( idx === undefined ) {
					idx = $.inArray( a[1], asSorting );
				}
		
				return idx+1 < asSorting.length ?
					idx+1 :
					overflow ?
						null :
						0;
			};
		
			// Convert to 2D array if needed
			if ( typeof sorting[0] === 'number' ) {
				sorting = settings.aaSorting = [ sorting ];
			}
		
			// If appending the sort then we are multi-column sorting
			if ( append && settings.oFeatures.bSortMulti ) {
				// Are we already doing some kind of sort on this column?
				var sortIdx = $.inArray( colIdx, _pluck(sorting, '0') );
		
				if ( sortIdx !== -1 ) {
					// Yes, modify the sort
					nextSortIdx = next( sorting[sortIdx], true );
		
					if ( nextSortIdx === null && sorting.length === 1 ) {
						nextSortIdx = 0; // can't remove sorting completely
					}
		
					if ( nextSortIdx === null ) {
						sorting.splice( sortIdx, 1 );
					}
					else {
						sorting[sortIdx][1] = asSorting[ nextSortIdx ];
						sorting[sortIdx]._idx = nextSortIdx;
					}
				}
				else {
					// No sort on this column yet
					sorting.push( [ colIdx, asSorting[0], 0 ] );
					sorting[sorting.length-1]._idx = 0;
				}
			}
			else if ( sorting.length && sorting[0][0] == colIdx ) {
				// Single column - already sorting on this column, modify the sort
				nextSortIdx = next( sorting[0] );
		
				sorting.length = 1;
				sorting[0][1] = asSorting[ nextSortIdx ];
				sorting[0]._idx = nextSortIdx;
			}
			else {
				// Single column - sort only on this column
				sorting.length = 0;
				sorting.push( [ colIdx, asSorting[0] ] );
				sorting[0]._idx = 0;
			}
		
			// Run the sort by calling a full redraw
			_fnReDraw( settings );
		
			// callback used for async user interaction
			if ( typeof callback == 'function' ) {
				callback( settings );
			}
		}
		
		
		/**
		 * Attach a sort handler (click) to a node
		 *  @param {object} settings dataTables settings object
		 *  @param {node} attachTo node to attach the handler to
		 *  @param {int} colIdx column sorting index
		 *  @param {function} [callback] callback function
		 *  @memberof DataTable#oApi
		 */
		function _fnSortAttachListener ( settings, attachTo, colIdx, callback )
		{
			var col = settings.aoColumns[ colIdx ];
		
			_fnBindAction( attachTo, {}, function (e) {
				/* If the column is not sortable - don't to anything */
				if ( col.bSortable === false ) {
					return;
				}
		
				// If processing is enabled use a timeout to allow the processing
				// display to be shown - otherwise to it synchronously
				if ( settings.oFeatures.bProcessing ) {
					_fnProcessingDisplay( settings, true );
		
					setTimeout( function() {
						_fnSortListener( settings, colIdx, e.shiftKey, callback );
		
						// In server-side processing, the draw callback will remove the
						// processing display
						if ( _fnDataSource( settings ) !== 'ssp' ) {
							_fnProcessingDisplay( settings, false );
						}
					}, 0 );
				}
				else {
					_fnSortListener( settings, colIdx, e.shiftKey, callback );
				}
			} );
		}
		
		
		/**
		 * Set the sorting classes on table's body, Note: it is safe to call this function
		 * when bSort and bSortClasses are false
		 *  @param {object} oSettings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnSortingClasses( settings )
		{
			var oldSort = settings.aLastSort;
			var sortClass = settings.oClasses.sSortColumn;
			var sort = _fnSortFlatten( settings );
			var features = settings.oFeatures;
			var i, ien, colIdx;
		
			if ( features.bSort && features.bSortClasses ) {
				// Remove old sorting classes
				for ( i=0, ien=oldSort.length ; i<ien ; i++ ) {
					colIdx = oldSort[i].src;
		
					// Remove column sorting
					$( _pluck( settings.aoData, 'anCells', colIdx ) )
						.removeClass( sortClass + (i<2 ? i+1 : 3) );
				}
		
				// Add new column sorting
				for ( i=0, ien=sort.length ; i<ien ; i++ ) {
					colIdx = sort[i].src;
		
					$( _pluck( settings.aoData, 'anCells', colIdx ) )
						.addClass( sortClass + (i<2 ? i+1 : 3) );
				}
			}
		
			settings.aLastSort = sort;
		}
		
		
		// Get the data to sort a column, be it from cache, fresh (populating the
		// cache), or from a sort formatter
		function _fnSortData( settings, idx )
		{
			// Custom sorting function - provided by the sort data type
			var column = settings.aoColumns[ idx ];
			var customSort = DataTable.ext.order[ column.sSortDataType ];
			var customData;
		
			if ( customSort ) {
				customData = customSort.call( settings.oInstance, settings, idx,
					_fnColumnIndexToVisible( settings, idx )
				);
			}
		
			// Use / populate cache
			var row, cellData;
			var formatter = DataTable.ext.type.order[ column.sType+"-pre" ];
		
			for ( var i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
				row = settings.aoData[i];
		
				if ( ! row._aSortData ) {
					row._aSortData = [];
				}
		
				if ( ! row._aSortData[idx] || customSort ) {
					cellData = customSort ?
						customData[i] : // If there was a custom sort function, use data from there
						_fnGetCellData( settings, i, idx, 'sort' );
		
					row._aSortData[ idx ] = formatter ?
						formatter( cellData ) :
						cellData;
				}
			}
		}
		
		
		
		/**
		 * Save the state of a table
		 *  @param {object} oSettings dataTables settings object
		 *  @memberof DataTable#oApi
		 */
		function _fnSaveState ( settings )
		{
			if ( !settings.oFeatures.bStateSave || settings.bDestroying )
			{
				return;
			}
		
			/* Store the interesting variables */
			var state = {
				time:    +new Date(),
				start:   settings._iDisplayStart,
				length:  settings._iDisplayLength,
				order:   $.extend( true, [], settings.aaSorting ),
				search:  _fnSearchToCamel( settings.oPreviousSearch ),
				columns: $.map( settings.aoColumns, function ( col, i ) {
					return {
						visible: col.bVisible,
						search: _fnSearchToCamel( settings.aoPreSearchCols[i] )
					};
				} )
			};
		
			_fnCallbackFire( settings, "aoStateSaveParams", 'stateSaveParams', [settings, state] );
		
			settings.oSavedState = state;
			settings.fnStateSaveCallback.call( settings.oInstance, settings, state );
		}
		
		
		/**
		 * Attempt to load a saved table state
		 *  @param {object} oSettings dataTables settings object
		 *  @param {object} oInit DataTables init object so we can override settings
		 *  @memberof DataTable#oApi
		 */
		function _fnLoadState ( settings, oInit )
		{
			var i, ien;
			var columns = settings.aoColumns;
		
			if ( ! settings.oFeatures.bStateSave ) {
				return;
			}
		
			var state = settings.fnStateLoadCallback.call( settings.oInstance, settings );
			if ( ! state || ! state.time ) {
				return;
			}
		
			/* Allow custom and plug-in manipulation functions to alter the saved data set and
			 * cancelling of loading by returning false
			 */
			var abStateLoad = _fnCallbackFire( settings, 'aoStateLoadParams', 'stateLoadParams', [settings, state] );
			if ( $.inArray( false, abStateLoad ) !== -1 ) {
				return;
			}
		
			/* Reject old data */
			var duration = settings.iStateDuration;
			if ( duration > 0 && state.time < +new Date() - (duration*1000) ) {
				return;
			}
		
			// Number of columns have changed - all bets are off, no restore of settings
			if ( columns.length !== state.columns.length ) {
				return;
			}
		
			// Store the saved state so it might be accessed at any time
			settings.oLoadedState = $.extend( true, {}, state );
		
			// Restore key features - todo - for 1.11 this needs to be done by
			// subscribed events
			if ( state.start !== undefined ) {
				settings._iDisplayStart    = state.start;
				settings.iInitDisplayStart = state.start;
			}
			if ( state.length !== undefined ) {
				settings._iDisplayLength   = state.length;
			}
		
			// Order
			if ( state.order !== undefined ) {
				settings.aaSorting = [];
				$.each( state.order, function ( i, col ) {
					settings.aaSorting.push( col[0] >= columns.length ?
						[ 0, col[1] ] :
						col
					);
				} );
			}
		
			// Search
			if ( state.search !== undefined ) {
				$.extend( settings.oPreviousSearch, _fnSearchToHung( state.search ) );
			}
		
			// Columns
			for ( i=0, ien=state.columns.length ; i<ien ; i++ ) {
				var col = state.columns[i];
		
				// Visibility
				if ( col.visible !== undefined ) {
					columns[i].bVisible = col.visible;
				}
		
				// Search
				if ( col.search !== undefined ) {
					$.extend( settings.aoPreSearchCols[i], _fnSearchToHung( col.search ) );
				}
			}
		
			_fnCallbackFire( settings, 'aoStateLoaded', 'stateLoaded', [settings, state] );
		}
		
		
		/**
		 * Return the settings object for a particular table
		 *  @param {node} table table we are using as a dataTable
		 *  @returns {object} Settings object - or null if not found
		 *  @memberof DataTable#oApi
		 */
		function _fnSettingsFromNode ( table )
		{
			var settings = DataTable.settings;
			var idx = $.inArray( table, _pluck( settings, 'nTable' ) );
		
			return idx !== -1 ?
				settings[ idx ] :
				null;
		}
		
		
		/**
		 * Log an error message
		 *  @param {object} settings dataTables settings object
		 *  @param {int} level log error messages, or display them to the user
		 *  @param {string} msg error message
		 *  @param {int} tn Technical note id to get more information about the error.
		 *  @memberof DataTable#oApi
		 */
		function _fnLog( settings, level, msg, tn )
		{
			msg = 'DataTables warning: '+
				(settings ? 'table id='+settings.sTableId+' - ' : '')+msg;
		
			if ( tn ) {
				msg += '. For more information about this error, please see '+
				'http://datatables.net/tn/'+tn;
			}
		
			if ( ! level  ) {
				// Backwards compatibility pre 1.10
				var ext = DataTable.ext;
				var type = ext.sErrMode || ext.errMode;
		
				if ( settings ) {
					_fnCallbackFire( settings, null, 'error', [ settings, tn, msg ] );
				}
		
				if ( type == 'alert' ) {
					alert( msg );
				}
				else if ( type == 'throw' ) {
					throw new Error(msg);
				}
				else if ( typeof type == 'function' ) {
					type( settings, tn, msg );
				}
			}
			else if ( window.console && console.log ) {
				console.log( msg );
			}
		}
		
		
		/**
		 * See if a property is defined on one object, if so assign it to the other object
		 *  @param {object} ret target object
		 *  @param {object} src source object
		 *  @param {string} name property
		 *  @param {string} [mappedName] name to map too - optional, name used if not given
		 *  @memberof DataTable#oApi
		 */
		function _fnMap( ret, src, name, mappedName )
		{
			if ( $.isArray( name ) ) {
				$.each( name, function (i, val) {
					if ( $.isArray( val ) ) {
						_fnMap( ret, src, val[0], val[1] );
					}
					else {
						_fnMap( ret, src, val );
					}
				} );
		
				return;
			}
		
			if ( mappedName === undefined ) {
				mappedName = name;
			}
		
			if ( src[name] !== undefined ) {
				ret[mappedName] = src[name];
			}
		}
		
		
		/**
		 * Extend objects - very similar to jQuery.extend, but deep copy objects, and
		 * shallow copy arrays. The reason we need to do this, is that we don't want to
		 * deep copy array init values (such as aaSorting) since the dev wouldn't be
		 * able to override them, but we do want to deep copy arrays.
		 *  @param {object} out Object to extend
		 *  @param {object} extender Object from which the properties will be applied to
		 *      out
		 *  @param {boolean} breakRefs If true, then arrays will be sliced to take an
		 *      independent copy with the exception of the `data` or `aaData` parameters
		 *      if they are present. This is so you can pass in a collection to
		 *      DataTables and have that used as your data source without breaking the
		 *      references
		 *  @returns {object} out Reference, just for convenience - out === the return.
		 *  @memberof DataTable#oApi
		 *  @todo This doesn't take account of arrays inside the deep copied objects.
		 */
		function _fnExtend( out, extender, breakRefs )
		{
			var val;
		
			for ( var prop in extender ) {
				if ( extender.hasOwnProperty(prop) ) {
					val = extender[prop];
		
					if ( $.isPlainObject( val ) ) {
						if ( ! $.isPlainObject( out[prop] ) ) {
							out[prop] = {};
						}
						$.extend( true, out[prop], val );
					}
					else if ( breakRefs && prop !== 'data' && prop !== 'aaData' && $.isArray(val) ) {
						out[prop] = val.slice();
					}
					else {
						out[prop] = val;
					}
				}
			}
		
			return out;
		}
		
		
		/**
		 * Bind an event handers to allow a click or return key to activate the callback.
		 * This is good for accessibility since a return on the keyboard will have the
		 * same effect as a click, if the element has focus.
		 *  @param {element} n Element to bind the action to
		 *  @param {object} oData Data object to pass to the triggered function
		 *  @param {function} fn Callback function for when the event is triggered
		 *  @memberof DataTable#oApi
		 */
		function _fnBindAction( n, oData, fn )
		{
			$(n)
				.bind( 'click.DT', oData, function (e) {
						n.blur(); // Remove focus outline for mouse users
						fn(e);
					} )
				.bind( 'keypress.DT', oData, function (e){
						if ( e.which === 13 ) {
							e.preventDefault();
							fn(e);
						}
					} )
				.bind( 'selectstart.DT', function () {
						/* Take the brutal approach to cancelling text selection */
						return false;
					} );
		}
		
		
		/**
		 * Register a callback function. Easily allows a callback function to be added to
		 * an array store of callback functions that can then all be called together.
		 *  @param {object} oSettings dataTables settings object
		 *  @param {string} sStore Name of the array storage for the callbacks in oSettings
		 *  @param {function} fn Function to be called back
		 *  @param {string} sName Identifying name for the callback (i.e. a label)
		 *  @memberof DataTable#oApi
		 */
		function _fnCallbackReg( oSettings, sStore, fn, sName )
		{
			if ( fn )
			{
				oSettings[sStore].push( {
					"fn": fn,
					"sName": sName
				} );
			}
		}
		
		
		/**
		 * Fire callback functions and trigger events. Note that the loop over the
		 * callback array store is done backwards! Further note that you do not want to
		 * fire off triggers in time sensitive applications (for example cell creation)
		 * as its slow.
		 *  @param {object} settings dataTables settings object
		 *  @param {string} callbackArr Name of the array storage for the callbacks in
		 *      oSettings
		 *  @param {string} eventName Name of the jQuery custom event to trigger. If
		 *      null no trigger is fired
		 *  @param {array} args Array of arguments to pass to the callback function /
		 *      trigger
		 *  @memberof DataTable#oApi
		 */
		function _fnCallbackFire( settings, callbackArr, eventName, args )
		{
			var ret = [];
		
			if ( callbackArr ) {
				ret = $.map( settings[callbackArr].slice().reverse(), function (val, i) {
					return val.fn.apply( settings.oInstance, args );
				} );
			}
		
			if ( eventName !== null ) {
				var e = $.Event( eventName+'.dt' );
		
				$(settings.nTable).trigger( e, args );
		
				ret.push( e.result );
			}
		
			return ret;
		}
		
		
		function _fnLengthOverflow ( settings )
		{
			var
				start = settings._iDisplayStart,
				end = settings.fnDisplayEnd(),
				len = settings._iDisplayLength;
		
			/* If we have space to show extra rows (backing up from the end point - then do so */
			if ( start >= end )
			{
				start = end - len;
			}
		
			// Keep the start record on the current page
			start -= (start % len);
		
			if ( len === -1 || start < 0 )
			{
				start = 0;
			}
		
			settings._iDisplayStart = start;
		}
		
		
		function _fnRenderer( settings, type )
		{
			var renderer = settings.renderer;
			var host = DataTable.ext.renderer[type];
		
			if ( $.isPlainObject( renderer ) && renderer[type] ) {
				// Specific renderer for this type. If available use it, otherwise use
				// the default.
				return host[renderer[type]] || host._;
			}
			else if ( typeof renderer === 'string' ) {
				// Common renderer - if there is one available for this type use it,
				// otherwise use the default
				return host[renderer] || host._;
			}
		
			// Use the default
			return host._;
		}
		
		
		/**
		 * Detect the data source being used for the table. Used to simplify the code
		 * a little (ajax) and to make it compress a little smaller.
		 *
		 *  @param {object} settings dataTables settings object
		 *  @returns {string} Data source
		 *  @memberof DataTable#oApi
		 */
		function _fnDataSource ( settings )
		{
			if ( settings.oFeatures.bServerSide ) {
				return 'ssp';
			}
			else if ( settings.ajax || settings.sAjaxSource ) {
				return 'ajax';
			}
			return 'dom';
		}
		

		
		
		/**
		 * Computed structure of the DataTables API, defined by the options passed to
		 * `DataTable.Api.register()` when building the API.
		 *
		 * The structure is built in order to speed creation and extension of the Api
		 * objects since the extensions are effectively pre-parsed.
		 *
		 * The array is an array of objects with the following structure, where this
		 * base array represents the Api prototype base:
		 *
		 *     [
		 *       {
		 *         name:      'data'                -- string   - Property name
		 *         val:       function () {},       -- function - Api method (or undefined if just an object
		 *         methodExt: [ ... ],              -- array    - Array of Api object definitions to extend the method result
		 *         propExt:   [ ... ]               -- array    - Array of Api object definitions to extend the property
		 *       },
		 *       {
		 *         name:     'row'
		 *         val:       {},
		 *         methodExt: [ ... ],
		 *         propExt:   [
		 *           {
		 *             name:      'data'
		 *             val:       function () {},
		 *             methodExt: [ ... ],
		 *             propExt:   [ ... ]
		 *           },
		 *           ...
		 *         ]
		 *       }
		 *     ]
		 *
		 * @type {Array}
		 * @ignore
		 */
		var __apiStruct = [];
		
		
		/**
		 * `Array.prototype` reference.
		 *
		 * @type object
		 * @ignore
		 */
		var __arrayProto = Array.prototype;
		
		
		/**
		 * Abstraction for `context` parameter of the `Api` constructor to allow it to
		 * take several different forms for ease of use.
		 *
		 * Each of the input parameter types will be converted to a DataTables settings
		 * object where possible.
		 *
		 * @param  {string|node|jQuery|object} mixed DataTable identifier. Can be one
		 *   of:
		 *
		 *   * `string` - jQuery selector. Any DataTables' matching the given selector
		 *     with be found and used.
		 *   * `node` - `TABLE` node which has already been formed into a DataTable.
		 *   * `jQuery` - A jQuery object of `TABLE` nodes.
		 *   * `object` - DataTables settings object
		 *   * `DataTables.Api` - API instance
		 * @return {array|null} Matching DataTables settings objects. `null` or
		 *   `undefined` is returned if no matching DataTable is found.
		 * @ignore
		 */
		var _toSettings = function ( mixed )
		{
			var idx, jq;
			var settings = DataTable.settings;
			var tables = $.map( settings, function (el, i) {
				return el.nTable;
			} );
		
			if ( ! mixed ) {
				return [];
			}
			else if ( mixed.nTable && mixed.oApi ) {
				// DataTables settings object
				return [ mixed ];
			}
			else if ( mixed.nodeName && mixed.nodeName.toLowerCase() === 'table' ) {
				// Table node
				idx = $.inArray( mixed, tables );
				return idx !== -1 ? [ settings[idx] ] : null;
			}
			else if ( mixed && typeof mixed.settings === 'function' ) {
				return mixed.settings().toArray();
			}
			else if ( typeof mixed === 'string' ) {
				// jQuery selector
				jq = $(mixed);
			}
			else if ( mixed instanceof $ ) {
				// jQuery object (also DataTables instance)
				jq = mixed;
			}
		
			if ( jq ) {
				return jq.map( function(i) {
					idx = $.inArray( this, tables );
					return idx !== -1 ? settings[idx] : null;
				} ).toArray();
			}
		};
		
		
		/**
		 * DataTables API class - used to control and interface with  one or more
		 * DataTables enhanced tables.
		 *
		 * The API class is heavily based on jQuery, presenting a chainable interface
		 * that you can use to interact with tables. Each instance of the API class has
		 * a "context" - i.e. the tables that it will operate on. This could be a single
		 * table, all tables on a page or a sub-set thereof.
		 *
		 * Additionally the API is designed to allow you to easily work with the data in
		 * the tables, retrieving and manipulating it as required. This is done by
		 * presenting the API class as an array like interface. The contents of the
		 * array depend upon the actions requested by each method (for example
		 * `rows().nodes()` will return an array of nodes, while `rows().data()` will
		 * return an array of objects or arrays depending upon your table's
		 * configuration). The API object has a number of array like methods (`push`,
		 * `pop`, `reverse` etc) as well as additional helper methods (`each`, `pluck`,
		 * `unique` etc) to assist your working with the data held in a table.
		 *
		 * Most methods (those which return an Api instance) are chainable, which means
		 * the return from a method call also has all of the methods available that the
		 * top level object had. For example, these two calls are equivalent:
		 *
		 *     // Not chained
		 *     api.row.add( {...} );
		 *     api.draw();
		 *
		 *     // Chained
		 *     api.row.add( {...} ).draw();
		 *
		 * @class DataTable.Api
		 * @param {array|object|string|jQuery} context DataTable identifier. This is
		 *   used to define which DataTables enhanced tables this API will operate on.
		 *   Can be one of:
		 *
		 *   * `string` - jQuery selector. Any DataTables' matching the given selector
		 *     with be found and used.
		 *   * `node` - `TABLE` node which has already been formed into a DataTable.
		 *   * `jQuery` - A jQuery object of `TABLE` nodes.
		 *   * `object` - DataTables settings object
		 * @param {array} [data] Data to initialise the Api instance with.
		 *
		 * @example
		 *   // Direct initialisation during DataTables construction
		 *   var api = $('#example').DataTable();
		 *
		 * @example
		 *   // Initialisation using a DataTables jQuery object
		 *   var api = $('#example').dataTable().api();
		 *
		 * @example
		 *   // Initialisation as a constructor
		 *   var api = new $.fn.DataTable.Api( 'table.dataTable' );
		 */
		_Api = function ( context, data )
		{
			if ( ! (this instanceof _Api) ) {
				return new _Api( context, data );
			}
		
			var settings = [];
			var ctxSettings = function ( o ) {
				var a = _toSettings( o );
				if ( a ) {
					settings = settings.concat( a );
				}
			};
		
			if ( $.isArray( context ) ) {
				for ( var i=0, ien=context.length ; i<ien ; i++ ) {
					ctxSettings( context[i] );
				}
			}
			else {
				ctxSettings( context );
			}
		
			// Remove duplicates
			this.context = _unique( settings );
		
			// Initial data
			if ( data ) {
				$.merge( this, data );
			}
		
			// selector
			this.selector = {
				rows: null,
				cols: null,
				opts: null
			};
		
			_Api.extend( this, this, __apiStruct );
		};
		
		DataTable.Api = _Api;
		
		// Don't destroy the existing prototype, just extend it. Required for jQuery 2's
		// isPlainObject.
		$.extend( _Api.prototype, {
			any: function ()
			{
				return this.count() !== 0;
			},
		
		
			concat:  __arrayProto.concat,
		
		
			context: [], // array of table settings objects
		
		
			count: function ()
			{
				return this.flatten().length;
			},
		
		
			each: function ( fn )
			{
				for ( var i=0, ien=this.length ; i<ien; i++ ) {
					fn.call( this, this[i], i, this );
				}
		
				return this;
			},
		
		
			eq: function ( idx )
			{
				var ctx = this.context;
		
				return ctx.length > idx ?
					new _Api( ctx[idx], this[idx] ) :
					null;
			},
		
		
			filter: function ( fn )
			{
				var a = [];
		
				if ( __arrayProto.filter ) {
					a = __arrayProto.filter.call( this, fn, this );
				}
				else {
					// Compatibility for browsers without EMCA-252-5 (JS 1.6)
					for ( var i=0, ien=this.length ; i<ien ; i++ ) {
						if ( fn.call( this, this[i], i, this ) ) {
							a.push( this[i] );
						}
					}
				}
		
				return new _Api( this.context, a );
			},
		
		
			flatten: function ()
			{
				var a = [];
				return new _Api( this.context, a.concat.apply( a, this.toArray() ) );
			},
		
		
			join:    __arrayProto.join,
		
		
			indexOf: __arrayProto.indexOf || function (obj, start)
			{
				for ( var i=(start || 0), ien=this.length ; i<ien ; i++ ) {
					if ( this[i] === obj ) {
						return i;
					}
				}
				return -1;
			},
		
			iterator: function ( flatten, type, fn, alwaysNew ) {
				var
					a = [], ret,
					i, ien, j, jen,
					context = this.context,
					rows, items, item,
					selector = this.selector;
		
				// Argument shifting
				if ( typeof flatten === 'string' ) {
					alwaysNew = fn;
					fn = type;
					type = flatten;
					flatten = false;
				}
		
				for ( i=0, ien=context.length ; i<ien ; i++ ) {
					var apiInst = new _Api( context[i] );
		
					if ( type === 'table' ) {
						ret = fn.call( apiInst, context[i], i );
		
						if ( ret !== undefined ) {
							a.push( ret );
						}
					}
					else if ( type === 'columns' || type === 'rows' ) {
						// this has same length as context - one entry for each table
						ret = fn.call( apiInst, context[i], this[i], i );
		
						if ( ret !== undefined ) {
							a.push( ret );
						}
					}
					else if ( type === 'column' || type === 'column-rows' || type === 'row' || type === 'cell' ) {
						// columns and rows share the same structure.
						// 'this' is an array of column indexes for each context
						items = this[i];
		
						if ( type === 'column-rows' ) {
							rows = _selector_row_indexes( context[i], selector.opts );
						}
		
						for ( j=0, jen=items.length ; j<jen ; j++ ) {
							item = items[j];
		
							if ( type === 'cell' ) {
								ret = fn.call( apiInst, context[i], item.row, item.column, i, j );
							}
							else {
								ret = fn.call( apiInst, context[i], item, i, j, rows );
							}
		
							if ( ret !== undefined ) {
								a.push( ret );
							}
						}
					}
				}
		
				if ( a.length || alwaysNew ) {
					var api = new _Api( context, flatten ? a.concat.apply( [], a ) : a );
					var apiSelector = api.selector;
					apiSelector.rows = selector.rows;
					apiSelector.cols = selector.cols;
					apiSelector.opts = selector.opts;
					return api;
				}
				return this;
			},
		
		
			lastIndexOf: __arrayProto.lastIndexOf || function (obj, start)
			{
				// Bit cheeky...
				return this.indexOf.apply( this.toArray.reverse(), arguments );
			},
		
		
			length:  0,
		
		
			map: function ( fn )
			{
				var a = [];
		
				if ( __arrayProto.map ) {
					a = __arrayProto.map.call( this, fn, this );
				}
				else {
					// Compatibility for browsers without EMCA-252-5 (JS 1.6)
					for ( var i=0, ien=this.length ; i<ien ; i++ ) {
						a.push( fn.call( this, this[i], i ) );
					}
				}
		
				return new _Api( this.context, a );
			},
		
		
			pluck: function ( prop )
			{
				return this.map( function ( el ) {
					return el[ prop ];
				} );
			},
		
			pop:     __arrayProto.pop,
		
		
			push:    __arrayProto.push,
		
		
			// Does not return an API instance
			reduce: __arrayProto.reduce || function ( fn, init )
			{
				return _fnReduce( this, fn, init, 0, this.length, 1 );
			},
		
		
			reduceRight: __arrayProto.reduceRight || function ( fn, init )
			{
				return _fnReduce( this, fn, init, this.length-1, -1, -1 );
			},
		
		
			reverse: __arrayProto.reverse,
		
		
			// Object with rows, columns and opts
			selector: null,
		
		
			shift:   __arrayProto.shift,
		
		
			sort:    __arrayProto.sort, // ? name - order?
		
		
			splice:  __arrayProto.splice,
		
		
			toArray: function ()
			{
				return __arrayProto.slice.call( this );
			},
		
		
			to$: function ()
			{
				return $( this );
			},
		
		
			toJQuery: function ()
			{
				return $( this );
			},
		
		
			unique: function ()
			{
				return new _Api( this.context, _unique(this) );
			},
		
		
			unshift: __arrayProto.unshift
		} );
		
		
		_Api.extend = function ( scope, obj, ext )
		{
			// Only extend API instances and static properties of the API
			if ( ! ext.length || ! obj || ( ! (obj instanceof _Api) && ! obj.__dt_wrapper ) ) {
				return;
			}
		
			var
				i, ien,
				j, jen,
				struct, inner,
				methodScoping = function ( scope, fn, struc ) {
					return function () {
						var ret = fn.apply( scope, arguments );
		
						// Method extension
						_Api.extend( ret, ret, struc.methodExt );
						return ret;
					};
				};
		
			for ( i=0, ien=ext.length ; i<ien ; i++ ) {
				struct = ext[i];
		
				// Value
				obj[ struct.name ] = typeof struct.val === 'function' ?
					methodScoping( scope, struct.val, struct ) :
					$.isPlainObject( struct.val ) ?
						{} :
						struct.val;
		
				obj[ struct.name ].__dt_wrapper = true;
		
				// Property extension
				_Api.extend( scope, obj[ struct.name ], struct.propExt );
			}
		};
		
		
		// @todo - Is there need for an augment function?
		// _Api.augment = function ( inst, name )
		// {
		// 	// Find src object in the structure from the name
		// 	var parts = name.split('.');
		
		// 	_Api.extend( inst, obj );
		// };
		
		
		//     [
		//       {
		//         name:      'data'                -- string   - Property name
		//         val:       function () {},       -- function - Api method (or undefined if just an object
		//         methodExt: [ ... ],              -- array    - Array of Api object definitions to extend the method result
		//         propExt:   [ ... ]               -- array    - Array of Api object definitions to extend the property
		//       },
		//       {
		//         name:     'row'
		//         val:       {},
		//         methodExt: [ ... ],
		//         propExt:   [
		//           {
		//             name:      'data'
		//             val:       function () {},
		//             methodExt: [ ... ],
		//             propExt:   [ ... ]
		//           },
		//           ...
		//         ]
		//       }
		//     ]
		
		_Api.register = _api_register = function ( name, val )
		{
			if ( $.isArray( name ) ) {
				for ( var j=0, jen=name.length ; j<jen ; j++ ) {
					_Api.register( name[j], val );
				}
				return;
			}
		
			var
				i, ien,
				heir = name.split('.'),
				struct = __apiStruct,
				key, method;
		
			var find = function ( src, name ) {
				for ( var i=0, ien=src.length ; i<ien ; i++ ) {
					if ( src[i].name === name ) {
						return src[i];
					}
				}
				return null;
			};
		
			for ( i=0, ien=heir.length ; i<ien ; i++ ) {
				method = heir[i].indexOf('()') !== -1;
				key = method ?
					heir[i].replace('()', '') :
					heir[i];
		
				var src = find( struct, key );
				if ( ! src ) {
					src = {
						name:      key,
						val:       {},
						methodExt: [],
						propExt:   []
					};
					struct.push( src );
				}
		
				if ( i === ien-1 ) {
					src.val = val;
				}
				else {
					struct = method ?
						src.methodExt :
						src.propExt;
				}
			}
		};
		
		
		_Api.registerPlural = _api_registerPlural = function ( pluralName, singularName, val ) {
			_Api.register( pluralName, val );
		
			_Api.register( singularName, function () {
				var ret = val.apply( this, arguments );
		
				if ( ret === this ) {
					// Returned item is the API instance that was passed in, return it
					return this;
				}
				else if ( ret instanceof _Api ) {
					// New API instance returned, want the value from the first item
					// in the returned array for the singular result.
					return ret.length ?
						$.isArray( ret[0] ) ?
							new _Api( ret.context, ret[0] ) : // Array results are 'enhanced'
							ret[0] :
						undefined;
				}
		
				// Non-API return - just fire it back
				return ret;
			} );
		};
		
		
		/**
		 * Selector for HTML tables. Apply the given selector to the give array of
		 * DataTables settings objects.
		 *
		 * @param {string|integer} [selector] jQuery selector string or integer
		 * @param  {array} Array of DataTables settings objects to be filtered
		 * @return {array}
		 * @ignore
		 */
		var __table_selector = function ( selector, a )
		{
			// Integer is used to pick out a table by index
			if ( typeof selector === 'number' ) {
				return [ a[ selector ] ];
			}
		
			// Perform a jQuery selector on the table nodes
			var nodes = $.map( a, function (el, i) {
				return el.nTable;
			} );
		
			return $(nodes)
				.filter( selector )
				.map( function (i) {
					// Need to translate back from the table node to the settings
					var idx = $.inArray( this, nodes );
					return a[ idx ];
				} )
				.toArray();
		};
		
		
		
		/**
		 * Context selector for the API's context (i.e. the tables the API instance
		 * refers to.
		 *
		 * @name    DataTable.Api#tables
		 * @param {string|integer} [selector] Selector to pick which tables the iterator
		 *   should operate on. If not given, all tables in the current context are
		 *   used. This can be given as a jQuery selector (for example `':gt(0)'`) to
		 *   select multiple tables or as an integer to select a single table.
		 * @returns {DataTable.Api} Returns a new API instance if a selector is given.
		 */
		_api_register( 'tables()', function ( selector ) {
			// A new instance is created if there was a selector specified
			return selector ?
				new _Api( __table_selector( selector, this.context ) ) :
				this;
		} );
		
		
		_api_register( 'table()', function ( selector ) {
			var tables = this.tables( selector );
			var ctx = tables.context;
		
			// Truncate to the first matched table
			return ctx.length ?
				new _Api( ctx[0] ) :
				tables;
		} );
		
		
		_api_registerPlural( 'tables().nodes()', 'table().node()' , function () {
			return this.iterator( 'table', function ( ctx ) {
				return ctx.nTable;
			}, 1 );
		} );
		
		
		_api_registerPlural( 'tables().body()', 'table().body()' , function () {
			return this.iterator( 'table', function ( ctx ) {
				return ctx.nTBody;
			}, 1 );
		} );
		
		
		_api_registerPlural( 'tables().header()', 'table().header()' , function () {
			return this.iterator( 'table', function ( ctx ) {
				return ctx.nTHead;
			}, 1 );
		} );
		
		
		_api_registerPlural( 'tables().footer()', 'table().footer()' , function () {
			return this.iterator( 'table', function ( ctx ) {
				return ctx.nTFoot;
			}, 1 );
		} );
		
		
		_api_registerPlural( 'tables().containers()', 'table().container()' , function () {
			return this.iterator( 'table', function ( ctx ) {
				return ctx.nTableWrapper;
			}, 1 );
		} );
		
		
		
		/**
		 * Redraw the tables in the current context.
		 */
		_api_register( 'draw()', function ( paging ) {
			return this.iterator( 'table', function ( settings ) {
				if ( paging === 'page' ) {
					_fnDraw( settings );
				}
				else {
					if ( typeof paging === 'string' ) {
						paging = paging === 'full-hold' ?
							false :
							true;
					}
		
					_fnReDraw( settings, paging===false );
				}
			} );
		} );
		
		
		
		/**
		 * Get the current page index.
		 *
		 * @return {integer} Current page index (zero based)
		 *//**
		 * Set the current page.
		 *
		 * Note that if you attempt to show a page which does not exist, DataTables will
		 * not throw an error, but rather reset the paging.
		 *
		 * @param {integer|string} action The paging action to take. This can be one of:
		 *  * `integer` - The page index to jump to
		 *  * `string` - An action to take:
		 *    * `first` - Jump to first page.
		 *    * `next` - Jump to the next page
		 *    * `previous` - Jump to previous page
		 *    * `last` - Jump to the last page.
		 * @returns {DataTables.Api} this
		 */
		_api_register( 'page()', function ( action ) {
			if ( action === undefined ) {
				return this.page.info().page; // not an expensive call
			}
		
			// else, have an action to take on all tables
			return this.iterator( 'table', function ( settings ) {
				_fnPageChange( settings, action );
			} );
		} );
		
		
		/**
		 * Paging information for the first table in the current context.
		 *
		 * If you require paging information for another table, use the `table()` method
		 * with a suitable selector.
		 *
		 * @return {object} Object with the following properties set:
		 *  * `page` - Current page index (zero based - i.e. the first page is `0`)
		 *  * `pages` - Total number of pages
		 *  * `start` - Display index for the first record shown on the current page
		 *  * `end` - Display index for the last record shown on the current page
		 *  * `length` - Display length (number of records). Note that generally `start
		 *    + length = end`, but this is not always true, for example if there are
		 *    only 2 records to show on the final page, with a length of 10.
		 *  * `recordsTotal` - Full data set length
		 *  * `recordsDisplay` - Data set length once the current filtering criterion
		 *    are applied.
		 */
		_api_register( 'page.info()', function ( action ) {
			if ( this.context.length === 0 ) {
				return undefined;
			}
		
			var
				settings   = this.context[0],
				start      = settings._iDisplayStart,
				len        = settings.oFeatures.bPaginate ? settings._iDisplayLength : -1,
				visRecords = settings.fnRecordsDisplay(),
				all        = len === -1;
		
			return {
				"page":           all ? 0 : Math.floor( start / len ),
				"pages":          all ? 1 : Math.ceil( visRecords / len ),
				"start":          start,
				"end":            settings.fnDisplayEnd(),
				"length":         len,
				"recordsTotal":   settings.fnRecordsTotal(),
				"recordsDisplay": visRecords,
				"serverSide":     _fnDataSource( settings ) === 'ssp'
			};
		} );
		
		
		/**
		 * Get the current page length.
		 *
		 * @return {integer} Current page length. Note `-1` indicates that all records
		 *   are to be shown.
		 *//**
		 * Set the current page length.
		 *
		 * @param {integer} Page length to set. Use `-1` to show all records.
		 * @returns {DataTables.Api} this
		 */
		_api_register( 'page.len()', function ( len ) {
			// Note that we can't call this function 'length()' because `length`
			// is a Javascript property of functions which defines how many arguments
			// the function expects.
			if ( len === undefined ) {
				return this.context.length !== 0 ?
					this.context[0]._iDisplayLength :
					undefined;
			}
		
			// else, set the page length
			return this.iterator( 'table', function ( settings ) {
				_fnLengthChange( settings, len );
			} );
		} );
		
		
		
		var __reload = function ( settings, holdPosition, callback ) {
			// Use the draw event to trigger a callback
			if ( callback ) {
				var api = new _Api( settings );
		
				api.one( 'draw', function () {
					callback( api.ajax.json() );
				} );
			}
		
			if ( _fnDataSource( settings ) == 'ssp' ) {
				_fnReDraw( settings, holdPosition );
			}
			else {
				_fnProcessingDisplay( settings, true );
		
				// Cancel an existing request
				var xhr = settings.jqXHR;
				if ( xhr && xhr.readyState !== 4 ) {
					xhr.abort();
				}
		
				// Trigger xhr
				_fnBuildAjax( settings, [], function( json ) {
					_fnClearTable( settings );
		
					var data = _fnAjaxDataSrc( settings, json );
					for ( var i=0, ien=data.length ; i<ien ; i++ ) {
						_fnAddData( settings, data[i] );
					}
		
					_fnReDraw( settings, holdPosition );
					_fnProcessingDisplay( settings, false );
				} );
			}
		};
		
		
		/**
		 * Get the JSON response from the last Ajax request that DataTables made to the
		 * server. Note that this returns the JSON from the first table in the current
		 * context.
		 *
		 * @return {object} JSON received from the server.
		 */
		_api_register( 'ajax.json()', function () {
			var ctx = this.context;
		
			if ( ctx.length > 0 ) {
				return ctx[0].json;
			}
		
			// else return undefined;
		} );
		
		
		/**
		 * Get the data submitted in the last Ajax request
		 */
		_api_register( 'ajax.params()', function () {
			var ctx = this.context;
		
			if ( ctx.length > 0 ) {
				return ctx[0].oAjaxData;
			}
		
			// else return undefined;
		} );
		
		
		/**
		 * Reload tables from the Ajax data source. Note that this function will
		 * automatically re-draw the table when the remote data has been loaded.
		 *
		 * @param {boolean} [reset=true] Reset (default) or hold the current paging
		 *   position. A full re-sort and re-filter is performed when this method is
		 *   called, which is why the pagination reset is the default action.
		 * @returns {DataTables.Api} this
		 */
		_api_register( 'ajax.reload()', function ( callback, resetPaging ) {
			return this.iterator( 'table', function (settings) {
				__reload( settings, resetPaging===false, callback );
			} );
		} );
		
		
		/**
		 * Get the current Ajax URL. Note that this returns the URL from the first
		 * table in the current context.
		 *
		 * @return {string} Current Ajax source URL
		 *//**
		 * Set the Ajax URL. Note that this will set the URL for all tables in the
		 * current context.
		 *
		 * @param {string} url URL to set.
		 * @returns {DataTables.Api} this
		 */
		_api_register( 'ajax.url()', function ( url ) {
			var ctx = this.context;
		
			if ( url === undefined ) {
				// get
				if ( ctx.length === 0 ) {
					return undefined;
				}
				ctx = ctx[0];
		
				return ctx.ajax ?
					$.isPlainObject( ctx.ajax ) ?
						ctx.ajax.url :
						ctx.ajax :
					ctx.sAjaxSource;
			}
		
			// set
			return this.iterator( 'table', function ( settings ) {
				if ( $.isPlainObject( settings.ajax ) ) {
					settings.ajax.url = url;
				}
				else {
					settings.ajax = url;
				}
				// No need to consider sAjaxSource here since DataTables gives priority
				// to `ajax` over `sAjaxSource`. So setting `ajax` here, renders any
				// value of `sAjaxSource` redundant.
			} );
		} );
		
		
		/**
		 * Load data from the newly set Ajax URL. Note that this method is only
		 * available when `ajax.url()` is used to set a URL. Additionally, this method
		 * has the same effect as calling `ajax.reload()` but is provided for
		 * convenience when setting a new URL. Like `ajax.reload()` it will
		 * automatically redraw the table once the remote data has been loaded.
		 *
		 * @returns {DataTables.Api} this
		 */
		_api_register( 'ajax.url().load()', function ( callback, resetPaging ) {
			// Same as a reload, but makes sense to present it for easy access after a
			// url change
			return this.iterator( 'table', function ( ctx ) {
				__reload( ctx, resetPaging===false, callback );
			} );
		} );
		
		
		
		
		var _selector_run = function ( type, selector, selectFn, settings, opts )
		{
			var
				out = [], res,
				a, i, ien, j, jen,
				selectorType = typeof selector;
		
			// Can't just check for isArray here, as an API or jQuery instance might be
			// given with their array like look
			if ( ! selector || selectorType === 'string' || selectorType === 'function' || selector.length === undefined ) {
				selector = [ selector ];
			}
		
			for ( i=0, ien=selector.length ; i<ien ; i++ ) {
				a = selector[i] && selector[i].split ?
					selector[i].split(',') :
					[ selector[i] ];
		
				for ( j=0, jen=a.length ; j<jen ; j++ ) {
					res = selectFn( typeof a[j] === 'string' ? $.trim(a[j]) : a[j] );
		
					if ( res && res.length ) {
						out = out.concat( res );
					}
				}
			}
		
			// selector extensions
			var ext = _ext.selector[ type ];
			if ( ext.length ) {
				for ( i=0, ien=ext.length ; i<ien ; i++ ) {
					out = ext[i]( settings, opts, out );
				}
			}
		
			return _unique( out );
		};
		
		
		var _selector_opts = function ( opts )
		{
			if ( ! opts ) {
				opts = {};
			}
		
			// Backwards compatibility for 1.9- which used the terminology filter rather
			// than search
			if ( opts.filter && opts.search === undefined ) {
				opts.search = opts.filter;
			}
		
			return $.extend( {
				search: 'none',
				order: 'current',
				page: 'all'
			}, opts );
		};
		
		
		var _selector_first = function ( inst )
		{
			// Reduce the API instance to the first item found
			for ( var i=0, ien=inst.length ; i<ien ; i++ ) {
				if ( inst[i].length > 0 ) {
					// Assign the first element to the first item in the instance
					// and truncate the instance and context
					inst[0] = inst[i];
					inst[0].length = 1;
					inst.length = 1;
					inst.context = [ inst.context[i] ];
		
					return inst;
				}
			}
		
			// Not found - return an empty instance
			inst.length = 0;
			return inst;
		};
		
		
		var _selector_row_indexes = function ( settings, opts )
		{
			var
				i, ien, tmp, a=[],
				displayFiltered = settings.aiDisplay,
				displayMaster = settings.aiDisplayMaster;
		
			var
				search = opts.search,  // none, applied, removed
				order  = opts.order,   // applied, current, index (original - compatibility with 1.9)
				page   = opts.page;    // all, current
		
			if ( _fnDataSource( settings ) == 'ssp' ) {
				// In server-side processing mode, most options are irrelevant since
				// rows not shown don't exist and the index order is the applied order
				// Removed is a special case - for consistency just return an empty
				// array
				return search === 'removed' ?
					[] :
					_range( 0, displayMaster.length );
			}
			else if ( page == 'current' ) {
				// Current page implies that order=current and fitler=applied, since it is
				// fairly senseless otherwise, regardless of what order and search actually
				// are
				for ( i=settings._iDisplayStart, ien=settings.fnDisplayEnd() ; i<ien ; i++ ) {
					a.push( displayFiltered[i] );
				}
			}
			else if ( order == 'current' || order == 'applied' ) {
				a = search == 'none' ?
					displayMaster.slice() :                      // no search
					search == 'applied' ?
						displayFiltered.slice() :                // applied search
						$.map( displayMaster, function (el, i) { // removed search
							return $.inArray( el, displayFiltered ) === -1 ? el : null;
						} );
			}
			else if ( order == 'index' || order == 'original' ) {
				for ( i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
					if ( search == 'none' ) {
						a.push( i );
					}
					else { // applied | removed
						tmp = $.inArray( i, displayFiltered );
		
						if ((tmp === -1 && search == 'removed') ||
							(tmp >= 0   && search == 'applied') )
						{
							a.push( i );
						}
					}
				}
			}
		
			return a;
		};
		
		
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * Rows
		 *
		 * {}          - no selector - use all available rows
		 * {integer}   - row aoData index
		 * {node}      - TR node
		 * {string}    - jQuery selector to apply to the TR elements
		 * {array}     - jQuery array of nodes, or simply an array of TR nodes
		 *
		 */
		
		
		var __row_selector = function ( settings, selector, opts )
		{
			var run = function ( sel ) {
				var selInt = _intVal( sel );
				var i, ien;
		
				// Short cut - selector is a number and no options provided (default is
				// all records, so no need to check if the index is in there, since it
				// must be - dev error if the index doesn't exist).
				if ( selInt !== null && ! opts ) {
					return [ selInt ];
				}
		
				var rows = _selector_row_indexes( settings, opts );
		
				if ( selInt !== null && $.inArray( selInt, rows ) !== -1 ) {
					// Selector - integer
					return [ selInt ];
				}
				else if ( ! sel ) {
					// Selector - none
					return rows;
				}
		
				// Selector - function
				if ( typeof sel === 'function' ) {
					return $.map( rows, function (idx) {
						var row = settings.aoData[ idx ];
						return sel( idx, row._aData, row.nTr ) ? idx : null;
					} );
				}
		
				// Get nodes in the order from the `rows` array with null values removed
				var nodes = _removeEmpty(
					_pluck_order( settings.aoData, rows, 'nTr' )
				);
		
				// Selector - node
				if ( sel.nodeName ) {
					if ( sel._DT_RowIndex !== undefined ) {
						return [ sel._DT_RowIndex ]; // Property added by DT for fast lookup
					}
					else if ( sel._DT_CellIndex ) {
						return [ sel._DT_CellIndex.row ];
					}
					else {
						var host = $(sel).closest('*[data-dt-row]');
						return host.length ?
							[ host.data('dt-row') ] :
							[];
					}
				}
		
				// ID selector. Want to always be able to select rows by id, regardless
				// of if the tr element has been created or not, so can't rely upon
				// jQuery here - hence a custom implementation. This does not match
				// Sizzle's fast selector or HTML4 - in HTML5 the ID can be anything,
				// but to select it using a CSS selector engine (like Sizzle or
				// querySelect) it would need to need to be escaped for some characters.
				// DataTables simplifies this for row selectors since you can select
				// only a row. A # indicates an id any anything that follows is the id -
				// unescaped.
				if ( typeof sel === 'string' && sel.charAt(0) === '#' ) {
					// get row index from id
					var rowObj = settings.aIds[ sel.replace( /^#/, '' ) ];
					if ( rowObj !== undefined ) {
						return [ rowObj.idx ];
					}
		
					// need to fall through to jQuery in case there is DOM id that
					// matches
				}
		
				// Selector - jQuery selector string, array of nodes or jQuery object/
				// As jQuery's .filter() allows jQuery objects to be passed in filter,
				// it also allows arrays, so this will cope with all three options
				return $(nodes)
					.filter( sel )
					.map( function () {
						return this._DT_RowIndex;
					} )
					.toArray();
			};
		
			return _selector_run( 'row', selector, run, settings, opts );
		};
		
		
		_api_register( 'rows()', function ( selector, opts ) {
			// argument shifting
			if ( selector === undefined ) {
				selector = '';
			}
			else if ( $.isPlainObject( selector ) ) {
				opts = selector;
				selector = '';
			}
		
			opts = _selector_opts( opts );
		
			var inst = this.iterator( 'table', function ( settings ) {
				return __row_selector( settings, selector, opts );
			}, 1 );
		
			// Want argument shifting here and in __row_selector?
			inst.selector.rows = selector;
			inst.selector.opts = opts;
		
			return inst;
		} );
		
		_api_register( 'rows().nodes()', function () {
			return this.iterator( 'row', function ( settings, row ) {
				return settings.aoData[ row ].nTr || undefined;
			}, 1 );
		} );
		
		_api_register( 'rows().data()', function () {
			return this.iterator( true, 'rows', function ( settings, rows ) {
				return _pluck_order( settings.aoData, rows, '_aData' );
			}, 1 );
		} );
		
		_api_registerPlural( 'rows().cache()', 'row().cache()', function ( type ) {
			return this.iterator( 'row', function ( settings, row ) {
				var r = settings.aoData[ row ];
				return type === 'search' ? r._aFilterData : r._aSortData;
			}, 1 );
		} );
		
		_api_registerPlural( 'rows().invalidate()', 'row().invalidate()', function ( src ) {
			return this.iterator( 'row', function ( settings, row ) {
				_fnInvalidate( settings, row, src );
			} );
		} );
		
		_api_registerPlural( 'rows().indexes()', 'row().index()', function () {
			return this.iterator( 'row', function ( settings, row ) {
				return row;
			}, 1 );
		} );
		
		_api_registerPlural( 'rows().ids()', 'row().id()', function ( hash ) {
			var a = [];
			var context = this.context;
		
			// `iterator` will drop undefined values, but in this case we want them
			for ( var i=0, ien=context.length ; i<ien ; i++ ) {
				for ( var j=0, jen=this[i].length ; j<jen ; j++ ) {
					var id = context[i].rowIdFn( context[i].aoData[ this[i][j] ]._aData );
					a.push( (hash === true ? '#' : '' )+ id );
				}
			}
		
			return new _Api( context, a );
		} );
		
		_api_registerPlural( 'rows().remove()', 'row().remove()', function () {
			var that = this;
		
			this.iterator( 'row', function ( settings, row, thatIdx ) {
				var data = settings.aoData;
				var rowData = data[ row ];
				var i, ien, j, jen;
				var loopRow, loopCells;
		
				data.splice( row, 1 );
		
				// Update the cached indexes
				for ( i=0, ien=data.length ; i<ien ; i++ ) {
					loopRow = data[i];
					loopCells = loopRow.anCells;
		
					// Rows
					if ( loopRow.nTr !== null ) {
						loopRow.nTr._DT_RowIndex = i;
					}
		
					// Cells
					if ( loopCells !== null ) {
						for ( j=0, jen=loopCells.length ; j<jen ; j++ ) {
							loopCells[j]._DT_CellIndex.row = i;
						}
					}
				}
		
				// Delete from the display arrays
				_fnDeleteIndex( settings.aiDisplayMaster, row );
				_fnDeleteIndex( settings.aiDisplay, row );
				_fnDeleteIndex( that[ thatIdx ], row, false ); // maintain local indexes
		
				// Check for an 'overflow' they case for displaying the table
				_fnLengthOverflow( settings );
		
				// Remove the row's ID reference if there is one
				var id = settings.rowIdFn( rowData._aData );
				if ( id !== undefined ) {
					delete settings.aIds[ id ];
				}
			} );
		
			this.iterator( 'table', function ( settings ) {
				for ( var i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
					settings.aoData[i].idx = i;
				}
			} );
		
			return this;
		} );
		
		
		_api_register( 'rows.add()', function ( rows ) {
			var newRows = this.iterator( 'table', function ( settings ) {
					var row, i, ien;
					var out = [];
		
					for ( i=0, ien=rows.length ; i<ien ; i++ ) {
						row = rows[i];
		
						if ( row.nodeName && row.nodeName.toUpperCase() === 'TR' ) {
							out.push( _fnAddTr( settings, row )[0] );
						}
						else {
							out.push( _fnAddData( settings, row ) );
						}
					}
		
					return out;
				}, 1 );
		
			// Return an Api.rows() extended instance, so rows().nodes() etc can be used
			var modRows = this.rows( -1 );
			modRows.pop();
			$.merge( modRows, newRows );
		
			return modRows;
		} );
		
		
		
		
		
		/**
		 *
		 */
		_api_register( 'row()', function ( selector, opts ) {
			return _selector_first( this.rows( selector, opts ) );
		} );
		
		
		_api_register( 'row().data()', function ( data ) {
			var ctx = this.context;
		
			if ( data === undefined ) {
				// Get
				return ctx.length && this.length ?
					ctx[0].aoData[ this[0] ]._aData :
					undefined;
			}
		
			// Set
			ctx[0].aoData[ this[0] ]._aData = data;
		
			// Automatically invalidate
			_fnInvalidate( ctx[0], this[0], 'data' );
		
			return this;
		} );
		
		
		_api_register( 'row().node()', function () {
			var ctx = this.context;
		
			return ctx.length && this.length ?
				ctx[0].aoData[ this[0] ].nTr || null :
				null;
		} );
		
		
		_api_register( 'row.add()', function ( row ) {
			// Allow a jQuery object to be passed in - only a single row is added from
			// it though - the first element in the set
			if ( row instanceof $ && row.length ) {
				row = row[0];
			}
		
			var rows = this.iterator( 'table', function ( settings ) {
				if ( row.nodeName && row.nodeName.toUpperCase() === 'TR' ) {
					return _fnAddTr( settings, row )[0];
				}
				return _fnAddData( settings, row );
			} );
		
			// Return an Api.rows() extended instance, with the newly added row selected
			return this.row( rows[0] );
		} );
		
		
		
		var __details_add = function ( ctx, row, data, klass )
		{
			// Convert to array of TR elements
			var rows = [];
			var addRow = function ( r, k ) {
				// Recursion to allow for arrays of jQuery objects
				if ( $.isArray( r ) || r instanceof $ ) {
					for ( var i=0, ien=r.length ; i<ien ; i++ ) {
						addRow( r[i], k );
					}
					return;
				}
		
				// If we get a TR element, then just add it directly - up to the dev
				// to add the correct number of columns etc
				if ( r.nodeName && r.nodeName.toLowerCase() === 'tr' ) {
					rows.push( r );
				}
				else {
					// Otherwise create a row with a wrapper
					var created = $('<tr><td/></tr>').addClass( k );
					$('td', created)
						.addClass( k )
						.html( r )
						[0].colSpan = _fnVisbleColumns( ctx );
		
					rows.push( created[0] );
				}
			};
		
			addRow( data, klass );
		
			if ( row._details ) {
				row._details.remove();
			}
		
			row._details = $(rows);
		
			// If the children were already shown, that state should be retained
			if ( row._detailsShow ) {
				row._details.insertAfter( row.nTr );
			}
		};
		
		
		var __details_remove = function ( api, idx )
		{
			var ctx = api.context;
		
			if ( ctx.length ) {
				var row = ctx[0].aoData[ idx !== undefined ? idx : api[0] ];
		
				if ( row && row._details ) {
					row._details.remove();
		
					row._detailsShow = undefined;
					row._details = undefined;
				}
			}
		};
		
		
		var __details_display = function ( api, show ) {
			var ctx = api.context;
		
			if ( ctx.length && api.length ) {
				var row = ctx[0].aoData[ api[0] ];
		
				if ( row._details ) {
					row._detailsShow = show;
		
					if ( show ) {
						row._details.insertAfter( row.nTr );
					}
					else {
						row._details.detach();
					}
		
					__details_events( ctx[0] );
				}
			}
		};
		
		
		var __details_events = function ( settings )
		{
			var api = new _Api( settings );
			var namespace = '.dt.DT_details';
			var drawEvent = 'draw'+namespace;
			var colvisEvent = 'column-visibility'+namespace;
			var destroyEvent = 'destroy'+namespace;
			var data = settings.aoData;
		
			api.off( drawEvent +' '+ colvisEvent +' '+ destroyEvent );
		
			if ( _pluck( data, '_details' ).length > 0 ) {
				// On each draw, insert the required elements into the document
				api.on( drawEvent, function ( e, ctx ) {
					if ( settings !== ctx ) {
						return;
					}
		
					api.rows( {page:'current'} ).eq(0).each( function (idx) {
						// Internal data grab
						var row = data[ idx ];
		
						if ( row._detailsShow ) {
							row._details.insertAfter( row.nTr );
						}
					} );
				} );
		
				// Column visibility change - update the colspan
				api.on( colvisEvent, function ( e, ctx, idx, vis ) {
					if ( settings !== ctx ) {
						return;
					}
		
					// Update the colspan for the details rows (note, only if it already has
					// a colspan)
					var row, visible = _fnVisbleColumns( ctx );
		
					for ( var i=0, ien=data.length ; i<ien ; i++ ) {
						row = data[i];
		
						if ( row._details ) {
							row._details.children('td[colspan]').attr('colspan', visible );
						}
					}
				} );
		
				// Table destroyed - nuke any child rows
				api.on( destroyEvent, function ( e, ctx ) {
					if ( settings !== ctx ) {
						return;
					}
		
					for ( var i=0, ien=data.length ; i<ien ; i++ ) {
						if ( data[i]._details ) {
							__details_remove( api, i );
						}
					}
				} );
			}
		};
		
		// Strings for the method names to help minification
		var _emp = '';
		var _child_obj = _emp+'row().child';
		var _child_mth = _child_obj+'()';
		
		// data can be:
		//  tr
		//  string
		//  jQuery or array of any of the above
		_api_register( _child_mth, function ( data, klass ) {
			var ctx = this.context;
		
			if ( data === undefined ) {
				// get
				return ctx.length && this.length ?
					ctx[0].aoData[ this[0] ]._details :
					undefined;
			}
			else if ( data === true ) {
				// show
				this.child.show();
			}
			else if ( data === false ) {
				// remove
				__details_remove( this );
			}
			else if ( ctx.length && this.length ) {
				// set
				__details_add( ctx[0], ctx[0].aoData[ this[0] ], data, klass );
			}
		
			return this;
		} );
		
		
		_api_register( [
			_child_obj+'.show()',
			_child_mth+'.show()' // only when `child()` was called with parameters (without
		], function ( show ) {   // it returns an object and this method is not executed)
			__details_display( this, true );
			return this;
		} );
		
		
		_api_register( [
			_child_obj+'.hide()',
			_child_mth+'.hide()' // only when `child()` was called with parameters (without
		], function () {         // it returns an object and this method is not executed)
			__details_display( this, false );
			return this;
		} );
		
		
		_api_register( [
			_child_obj+'.remove()',
			_child_mth+'.remove()' // only when `child()` was called with parameters (without
		], function () {           // it returns an object and this method is not executed)
			__details_remove( this );
			return this;
		} );
		
		
		_api_register( _child_obj+'.isShown()', function () {
			var ctx = this.context;
		
			if ( ctx.length && this.length ) {
				// _detailsShown as false or undefined will fall through to return false
				return ctx[0].aoData[ this[0] ]._detailsShow || false;
			}
			return false;
		} );
		
		
		
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * Columns
		 *
		 * {integer}           - column index (>=0 count from left, <0 count from right)
		 * "{integer}:visIdx"  - visible column index (i.e. translate to column index)  (>=0 count from left, <0 count from right)
		 * "{integer}:visible" - alias for {integer}:visIdx  (>=0 count from left, <0 count from right)
		 * "{string}:name"     - column name
		 * "{string}"          - jQuery selector on column header nodes
		 *
		 */
		
		// can be an array of these items, comma separated list, or an array of comma
		// separated lists
		
		var __re_column_selector = /^(.+):(name|visIdx|visible)$/;
		
		
		// r1 and r2 are redundant - but it means that the parameters match for the
		// iterator callback in columns().data()
		var __columnData = function ( settings, column, r1, r2, rows ) {
			var a = [];
			for ( var row=0, ien=rows.length ; row<ien ; row++ ) {
				a.push( _fnGetCellData( settings, rows[row], column ) );
			}
			return a;
		};
		
		
		var __column_selector = function ( settings, selector, opts )
		{
			var
				columns = settings.aoColumns,
				names = _pluck( columns, 'sName' ),
				nodes = _pluck( columns, 'nTh' );
		
			var run = function ( s ) {
				var selInt = _intVal( s );
		
				// Selector - all
				if ( s === '' ) {
					return _range( columns.length );
				}
		
				// Selector - index
				if ( selInt !== null ) {
					return [ selInt >= 0 ?
						selInt : // Count from left
						columns.length + selInt // Count from right (+ because its a negative value)
					];
				}
		
				// Selector = function
				if ( typeof s === 'function' ) {
					var rows = _selector_row_indexes( settings, opts );
		
					return $.map( columns, function (col, idx) {
						return s(
								idx,
								__columnData( settings, idx, 0, 0, rows ),
								nodes[ idx ]
							) ? idx : null;
					} );
				}
		
				// jQuery or string selector
				var match = typeof s === 'string' ?
					s.match( __re_column_selector ) :
					'';
		
				if ( match ) {
					switch( match[2] ) {
						case 'visIdx':
						case 'visible':
							var idx = parseInt( match[1], 10 );
							// Visible index given, convert to column index
							if ( idx < 0 ) {
								// Counting from the right
								var visColumns = $.map( columns, function (col,i) {
									return col.bVisible ? i : null;
								} );
								return [ visColumns[ visColumns.length + idx ] ];
							}
							// Counting from the left
							return [ _fnVisibleToColumnIndex( settings, idx ) ];
		
						case 'name':
							// match by name. `names` is column index complete and in order
							return $.map( names, function (name, i) {
								return name === match[1] ? i : null;
							} );
		
						default:
							return [];
					}
				}
		
				// Cell in the table body
				if ( s.nodeName && s._DT_CellIndex ) {
					return [ s._DT_CellIndex.column ];
				}
		
				// jQuery selector on the TH elements for the columns
				var jqResult = $( nodes )
					.filter( s )
					.map( function () {
						return $.inArray( this, nodes ); // `nodes` is column index complete and in order
					} )
					.toArray();
		
				if ( jqResult.length || ! s.nodeName ) {
					return jqResult;
				}
		
				// Otherwise a node which might have a `dt-column` data attribute, or be
				// a child or such an element
				var host = $(s).closest('*[data-dt-column]');
				return host.length ?
					[ host.data('dt-column') ] :
					[];
			};
		
			return _selector_run( 'column', selector, run, settings, opts );
		};
		
		
		var __setColumnVis = function ( settings, column, vis ) {
			var
				cols = settings.aoColumns,
				col  = cols[ column ],
				data = settings.aoData,
				row, cells, i, ien, tr;
		
			// Get
			if ( vis === undefined ) {
				return col.bVisible;
			}
		
			// Set
			// No change
			if ( col.bVisible === vis ) {
				return;
			}
		
			if ( vis ) {
				// Insert column
				// Need to decide if we should use appendChild or insertBefore
				var insertBefore = $.inArray( true, _pluck(cols, 'bVisible'), column+1 );
		
				for ( i=0, ien=data.length ; i<ien ; i++ ) {
					tr = data[i].nTr;
					cells = data[i].anCells;
		
					if ( tr ) {
						// insertBefore can act like appendChild if 2nd arg is null
						tr.insertBefore( cells[ column ], cells[ insertBefore ] || null );
					}
				}
			}
			else {
				// Remove column
				$( _pluck( settings.aoData, 'anCells', column ) ).detach();
			}
		
			// Common actions
			col.bVisible = vis;
			_fnDrawHead( settings, settings.aoHeader );
			_fnDrawHead( settings, settings.aoFooter );
		
			_fnSaveState( settings );
		};
		
		
		_api_register( 'columns()', function ( selector, opts ) {
			// argument shifting
			if ( selector === undefined ) {
				selector = '';
			}
			else if ( $.isPlainObject( selector ) ) {
				opts = selector;
				selector = '';
			}
		
			opts = _selector_opts( opts );
		
			var inst = this.iterator( 'table', function ( settings ) {
				return __column_selector( settings, selector, opts );
			}, 1 );
		
			// Want argument shifting here and in _row_selector?
			inst.selector.cols = selector;
			inst.selector.opts = opts;
		
			return inst;
		} );
		
		_api_registerPlural( 'columns().header()', 'column().header()', function ( selector, opts ) {
			return this.iterator( 'column', function ( settings, column ) {
				return settings.aoColumns[column].nTh;
			}, 1 );
		} );
		
		_api_registerPlural( 'columns().footer()', 'column().footer()', function ( selector, opts ) {
			return this.iterator( 'column', function ( settings, column ) {
				return settings.aoColumns[column].nTf;
			}, 1 );
		} );
		
		_api_registerPlural( 'columns().data()', 'column().data()', function () {
			return this.iterator( 'column-rows', __columnData, 1 );
		} );
		
		_api_registerPlural( 'columns().dataSrc()', 'column().dataSrc()', function () {
			return this.iterator( 'column', function ( settings, column ) {
				return settings.aoColumns[column].mData;
			}, 1 );
		} );
		
		_api_registerPlural( 'columns().cache()', 'column().cache()', function ( type ) {
			return this.iterator( 'column-rows', function ( settings, column, i, j, rows ) {
				return _pluck_order( settings.aoData, rows,
					type === 'search' ? '_aFilterData' : '_aSortData', column
				);
			}, 1 );
		} );
		
		_api_registerPlural( 'columns().nodes()', 'column().nodes()', function () {
			return this.iterator( 'column-rows', function ( settings, column, i, j, rows ) {
				return _pluck_order( settings.aoData, rows, 'anCells', column ) ;
			}, 1 );
		} );
		
		_api_registerPlural( 'columns().visible()', 'column().visible()', function ( vis, calc ) {
			var ret = this.iterator( 'column', function ( settings, column ) {
				if ( vis === undefined ) {
					return settings.aoColumns[ column ].bVisible;
				} // else
				__setColumnVis( settings, column, vis );
			} );
		
			// Group the column visibility changes
			if ( vis !== undefined ) {
				// Second loop once the first is done for events
				this.iterator( 'column', function ( settings, column ) {
					_fnCallbackFire( settings, null, 'column-visibility', [settings, column, vis, calc] );
				} );
		
				if ( calc === undefined || calc ) {
					this.columns.adjust();
				}
			}
		
			return ret;
		} );
		
		_api_registerPlural( 'columns().indexes()', 'column().index()', function ( type ) {
			return this.iterator( 'column', function ( settings, column ) {
				return type === 'visible' ?
					_fnColumnIndexToVisible( settings, column ) :
					column;
			}, 1 );
		} );
		
		_api_register( 'columns.adjust()', function () {
			return this.iterator( 'table', function ( settings ) {
				_fnAdjustColumnSizing( settings );
			}, 1 );
		} );
		
		_api_register( 'column.index()', function ( type, idx ) {
			if ( this.context.length !== 0 ) {
				var ctx = this.context[0];
		
				if ( type === 'fromVisible' || type === 'toData' ) {
					return _fnVisibleToColumnIndex( ctx, idx );
				}
				else if ( type === 'fromData' || type === 'toVisible' ) {
					return _fnColumnIndexToVisible( ctx, idx );
				}
			}
		} );
		
		_api_register( 'column()', function ( selector, opts ) {
			return _selector_first( this.columns( selector, opts ) );
		} );
		
		
		
		var __cell_selector = function ( settings, selector, opts )
		{
			var data = settings.aoData;
			var rows = _selector_row_indexes( settings, opts );
			var cells = _removeEmpty( _pluck_order( data, rows, 'anCells' ) );
			var allCells = $( [].concat.apply([], cells) );
			var row;
			var columns = settings.aoColumns.length;
			var a, i, ien, j, o, host;
		
			var run = function ( s ) {
				var fnSelector = typeof s === 'function';
		
				if ( s === null || s === undefined || fnSelector ) {
					// All cells and function selectors
					a = [];
		
					for ( i=0, ien=rows.length ; i<ien ; i++ ) {
						row = rows[i];
		
						for ( j=0 ; j<columns ; j++ ) {
							o = {
								row: row,
								column: j
							};
		
							if ( fnSelector ) {
								// Selector - function
								host = data[ row ];
		
								if ( s( o, _fnGetCellData(settings, row, j), host.anCells ? host.anCells[j] : null ) ) {
									a.push( o );
								}
							}
							else {
								// Selector - all
								a.push( o );
							}
						}
					}
		
					return a;
				}
				
				// Selector - index
				if ( $.isPlainObject( s ) ) {
					return [s];
				}
		
				// Selector - jQuery filtered cells
				var jqResult = allCells
					.filter( s )
					.map( function (i, el) {
						return { // use a new object, in case someone changes the values
							row:    el._DT_CellIndex.row,
							column: el._DT_CellIndex.column
		 				};
					} )
					.toArray();
		
				if ( jqResult.length || ! s.nodeName ) {
					return jqResult;
				}
		
				// Otherwise the selector is a node, and there is one last option - the
				// element might be a child of an element which has dt-row and dt-column
				// data attributes
				host = $(s).closest('*[data-dt-row]');
				return host.length ?
					[ {
						row: host.data('dt-row'),
						column: host.data('dt-column')
					} ] :
					[];
			};
		
			return _selector_run( 'cell', selector, run, settings, opts );
		};
		
		
		
		
		_api_register( 'cells()', function ( rowSelector, columnSelector, opts ) {
			// Argument shifting
			if ( $.isPlainObject( rowSelector ) ) {
				// Indexes
				if ( rowSelector.row === undefined ) {
					// Selector options in first parameter
					opts = rowSelector;
					rowSelector = null;
				}
				else {
					// Cell index objects in first parameter
					opts = columnSelector;
					columnSelector = null;
				}
			}
			if ( $.isPlainObject( columnSelector ) ) {
				opts = columnSelector;
				columnSelector = null;
			}
		
			// Cell selector
			if ( columnSelector === null || columnSelector === undefined ) {
				return this.iterator( 'table', function ( settings ) {
					return __cell_selector( settings, rowSelector, _selector_opts( opts ) );
				} );
			}
		
			// Row + column selector
			var columns = this.columns( columnSelector, opts );
			var rows = this.rows( rowSelector, opts );
			var a, i, ien, j, jen;
		
			var cells = this.iterator( 'table', function ( settings, idx ) {
				a = [];
		
				for ( i=0, ien=rows[idx].length ; i<ien ; i++ ) {
					for ( j=0, jen=columns[idx].length ; j<jen ; j++ ) {
						a.push( {
							row:    rows[idx][i],
							column: columns[idx][j]
						} );
					}
				}
		
				return a;
			}, 1 );
		
			$.extend( cells.selector, {
				cols: columnSelector,
				rows: rowSelector,
				opts: opts
			} );
		
			return cells;
		} );
		
		
		_api_registerPlural( 'cells().nodes()', 'cell().node()', function () {
			return this.iterator( 'cell', function ( settings, row, column ) {
				var data = settings.aoData[ row ];
		
				return data && data.anCells ?
					data.anCells[ column ] :
					undefined;
			}, 1 );
		} );
		
		
		_api_register( 'cells().data()', function () {
			return this.iterator( 'cell', function ( settings, row, column ) {
				return _fnGetCellData( settings, row, column );
			}, 1 );
		} );
		
		
		_api_registerPlural( 'cells().cache()', 'cell().cache()', function ( type ) {
			type = type === 'search' ? '_aFilterData' : '_aSortData';
		
			return this.iterator( 'cell', function ( settings, row, column ) {
				return settings.aoData[ row ][ type ][ column ];
			}, 1 );
		} );
		
		
		_api_registerPlural( 'cells().render()', 'cell().render()', function ( type ) {
			return this.iterator( 'cell', function ( settings, row, column ) {
				return _fnGetCellData( settings, row, column, type );
			}, 1 );
		} );
		
		
		_api_registerPlural( 'cells().indexes()', 'cell().index()', function () {
			return this.iterator( 'cell', function ( settings, row, column ) {
				return {
					row: row,
					column: column,
					columnVisible: _fnColumnIndexToVisible( settings, column )
				};
			}, 1 );
		} );
		
		
		_api_registerPlural( 'cells().invalidate()', 'cell().invalidate()', function ( src ) {
			return this.iterator( 'cell', function ( settings, row, column ) {
				_fnInvalidate( settings, row, src, column );
			} );
		} );
		
		
		
		_api_register( 'cell()', function ( rowSelector, columnSelector, opts ) {
			return _selector_first( this.cells( rowSelector, columnSelector, opts ) );
		} );
		
		
		_api_register( 'cell().data()', function ( data ) {
			var ctx = this.context;
			var cell = this[0];
		
			if ( data === undefined ) {
				// Get
				return ctx.length && cell.length ?
					_fnGetCellData( ctx[0], cell[0].row, cell[0].column ) :
					undefined;
			}
		
			// Set
			_fnSetCellData( ctx[0], cell[0].row, cell[0].column, data );
			_fnInvalidate( ctx[0], cell[0].row, 'data', cell[0].column );
		
			return this;
		} );
		
		
		
		/**
		 * Get current ordering (sorting) that has been applied to the table.
		 *
		 * @returns {array} 2D array containing the sorting information for the first
		 *   table in the current context. Each element in the parent array represents
		 *   a column being sorted upon (i.e. multi-sorting with two columns would have
		 *   2 inner arrays). The inner arrays may have 2 or 3 elements. The first is
		 *   the column index that the sorting condition applies to, the second is the
		 *   direction of the sort (`desc` or `asc`) and, optionally, the third is the
		 *   index of the sorting order from the `column.sorting` initialisation array.
		 *//**
		 * Set the ordering for the table.
		 *
		 * @param {integer} order Column index to sort upon.
		 * @param {string} direction Direction of the sort to be applied (`asc` or `desc`)
		 * @returns {DataTables.Api} this
		 *//**
		 * Set the ordering for the table.
		 *
		 * @param {array} order 1D array of sorting information to be applied.
		 * @param {array} [...] Optional additional sorting conditions
		 * @returns {DataTables.Api} this
		 *//**
		 * Set the ordering for the table.
		 *
		 * @param {array} order 2D array of sorting information to be applied.
		 * @returns {DataTables.Api} this
		 */
		_api_register( 'order()', function ( order, dir ) {
			var ctx = this.context;
		
			if ( order === undefined ) {
				// get
				return ctx.length !== 0 ?
					ctx[0].aaSorting :
					undefined;
			}
		
			// set
			if ( typeof order === 'number' ) {
				// Simple column / direction passed in
				order = [ [ order, dir ] ];
			}
			else if ( order.length && ! $.isArray( order[0] ) ) {
				// Arguments passed in (list of 1D arrays)
				order = Array.prototype.slice.call( arguments );
			}
			// otherwise a 2D array was passed in
		
			return this.iterator( 'table', function ( settings ) {
				settings.aaSorting = order.slice();
			} );
		} );
		
		
		/**
		 * Attach a sort listener to an element for a given column
		 *
		 * @param {node|jQuery|string} node Identifier for the element(s) to attach the
		 *   listener to. This can take the form of a single DOM node, a jQuery
		 *   collection of nodes or a jQuery selector which will identify the node(s).
		 * @param {integer} column the column that a click on this node will sort on
		 * @param {function} [callback] callback function when sort is run
		 * @returns {DataTables.Api} this
		 */
		_api_register( 'order.listener()', function ( node, column, callback ) {
			return this.iterator( 'table', function ( settings ) {
				_fnSortAttachListener( settings, node, column, callback );
			} );
		} );
		
		
		_api_register( 'order.fixed()', function ( set ) {
			if ( ! set ) {
				var ctx = this.context;
				var fixed = ctx.length ?
					ctx[0].aaSortingFixed :
					undefined;
		
				return $.isArray( fixed ) ?
					{ pre: fixed } :
					fixed;
			}
		
			return this.iterator( 'table', function ( settings ) {
				settings.aaSortingFixed = $.extend( true, {}, set );
			} );
		} );
		
		
		// Order by the selected column(s)
		_api_register( [
			'columns().order()',
			'column().order()'
		], function ( dir ) {
			var that = this;
		
			return this.iterator( 'table', function ( settings, i ) {
				var sort = [];
		
				$.each( that[i], function (j, col) {
					sort.push( [ col, dir ] );
				} );
		
				settings.aaSorting = sort;
			} );
		} );
		
		
		
		_api_register( 'search()', function ( input, regex, smart, caseInsen ) {
			var ctx = this.context;
		
			if ( input === undefined ) {
				// get
				return ctx.length !== 0 ?
					ctx[0].oPreviousSearch.sSearch :
					undefined;
			}
		
			// set
			return this.iterator( 'table', function ( settings ) {
				if ( ! settings.oFeatures.bFilter ) {
					return;
				}
		
				_fnFilterComplete( settings, $.extend( {}, settings.oPreviousSearch, {
					"sSearch": input+"",
					"bRegex":  regex === null ? false : regex,
					"bSmart":  smart === null ? true  : smart,
					"bCaseInsensitive": caseInsen === null ? true : caseInsen
				} ), 1 );
			} );
		} );
		
		
		_api_registerPlural(
			'columns().search()',
			'column().search()',
			function ( input, regex, smart, caseInsen ) {
				return this.iterator( 'column', function ( settings, column ) {
					var preSearch = settings.aoPreSearchCols;
		
					if ( input === undefined ) {
						// get
						return preSearch[ column ].sSearch;
					}
		
					// set
					if ( ! settings.oFeatures.bFilter ) {
						return;
					}
		
					$.extend( preSearch[ column ], {
						"sSearch": input+"",
						"bRegex":  regex === null ? false : regex,
						"bSmart":  smart === null ? true  : smart,
						"bCaseInsensitive": caseInsen === null ? true : caseInsen
					} );
		
					_fnFilterComplete( settings, settings.oPreviousSearch, 1 );
				} );
			}
		);
		
		/*
		 * State API methods
		 */
		
		_api_register( 'state()', function () {
			return this.context.length ?
				this.context[0].oSavedState :
				null;
		} );
		
		
		_api_register( 'state.clear()', function () {
			return this.iterator( 'table', function ( settings ) {
				// Save an empty object
				settings.fnStateSaveCallback.call( settings.oInstance, settings, {} );
			} );
		} );
		
		
		_api_register( 'state.loaded()', function () {
			return this.context.length ?
				this.context[0].oLoadedState :
				null;
		} );
		
		
		_api_register( 'state.save()', function () {
			return this.iterator( 'table', function ( settings ) {
				_fnSaveState( settings );
			} );
		} );
		
		
		
		/**
		 * Provide a common method for plug-ins to check the version of DataTables being
		 * used, in order to ensure compatibility.
		 *
		 *  @param {string} version Version string to check for, in the format "X.Y.Z".
		 *    Note that the formats "X" and "X.Y" are also acceptable.
		 *  @returns {boolean} true if this version of DataTables is greater or equal to
		 *    the required version, or false if this version of DataTales is not
		 *    suitable
		 *  @static
		 *  @dtopt API-Static
		 *
		 *  @example
		 *    alert( $.fn.dataTable.versionCheck( '1.9.0' ) );
		 */
		DataTable.versionCheck = DataTable.fnVersionCheck = function( version )
		{
			var aThis = DataTable.version.split('.');
			var aThat = version.split('.');
			var iThis, iThat;
		
			for ( var i=0, iLen=aThat.length ; i<iLen ; i++ ) {
				iThis = parseInt( aThis[i], 10 ) || 0;
				iThat = parseInt( aThat[i], 10 ) || 0;
		
				// Parts are the same, keep comparing
				if (iThis === iThat) {
					continue;
				}
		
				// Parts are different, return immediately
				return iThis > iThat;
			}
		
			return true;
		};
		
		
		/**
		 * Check if a `<table>` node is a DataTable table already or not.
		 *
		 *  @param {node|jquery|string} table Table node, jQuery object or jQuery
		 *      selector for the table to test. Note that if more than more than one
		 *      table is passed on, only the first will be checked
		 *  @returns {boolean} true the table given is a DataTable, or false otherwise
		 *  @static
		 *  @dtopt API-Static
		 *
		 *  @example
		 *    if ( ! $.fn.DataTable.isDataTable( '#example' ) ) {
		 *      $('#example').dataTable();
		 *    }
		 */
		DataTable.isDataTable = DataTable.fnIsDataTable = function ( table )
		{
			var t = $(table).get(0);
			var is = false;
		
			$.each( DataTable.settings, function (i, o) {
				var head = o.nScrollHead ? $('table', o.nScrollHead)[0] : null;
				var foot = o.nScrollFoot ? $('table', o.nScrollFoot)[0] : null;
		
				if ( o.nTable === t || head === t || foot === t ) {
					is = true;
				}
			} );
		
			return is;
		};
		
		
		/**
		 * Get all DataTable tables that have been initialised - optionally you can
		 * select to get only currently visible tables.
		 *
		 *  @param {boolean} [visible=false] Flag to indicate if you want all (default)
		 *    or visible tables only.
		 *  @returns {array} Array of `table` nodes (not DataTable instances) which are
		 *    DataTables
		 *  @static
		 *  @dtopt API-Static
		 *
		 *  @example
		 *    $.each( $.fn.dataTable.tables(true), function () {
		 *      $(table).DataTable().columns.adjust();
		 *    } );
		 */
		DataTable.tables = DataTable.fnTables = function ( visible )
		{
			var api = false;
		
			if ( $.isPlainObject( visible ) ) {
				api = visible.api;
				visible = visible.visible;
			}
		
			var a = $.map( DataTable.settings, function (o) {
				if ( !visible || (visible && $(o.nTable).is(':visible')) ) {
					return o.nTable;
				}
			} );
		
			return api ?
				new _Api( a ) :
				a;
		};
		
		
		/**
		 * Convert from camel case parameters to Hungarian notation. This is made public
		 * for the extensions to provide the same ability as DataTables core to accept
		 * either the 1.9 style Hungarian notation, or the 1.10+ style camelCase
		 * parameters.
		 *
		 *  @param {object} src The model object which holds all parameters that can be
		 *    mapped.
		 *  @param {object} user The object to convert from camel case to Hungarian.
		 *  @param {boolean} force When set to `true`, properties which already have a
		 *    Hungarian value in the `user` object will be overwritten. Otherwise they
		 *    won't be.
		 */
		DataTable.camelToHungarian = _fnCamelToHungarian;
		
		
		
		/**
		 *
		 */
		_api_register( '$()', function ( selector, opts ) {
			var
				rows   = this.rows( opts ).nodes(), // Get all rows
				jqRows = $(rows);
		
			return $( [].concat(
				jqRows.filter( selector ).toArray(),
				jqRows.find( selector ).toArray()
			) );
		} );
		
		
		// jQuery functions to operate on the tables
		$.each( [ 'on', 'one', 'off' ], function (i, key) {
			_api_register( key+'()', function ( /* event, handler */ ) {
				var args = Array.prototype.slice.call(arguments);
		
				// Add the `dt` namespace automatically if it isn't already present
				if ( ! args[0].match(/\.dt\b/) ) {
					args[0] += '.dt';
				}
		
				var inst = $( this.tables().nodes() );
				inst[key].apply( inst, args );
				return this;
			} );
		} );
		
		
		_api_register( 'clear()', function () {
			return this.iterator( 'table', function ( settings ) {
				_fnClearTable( settings );
			} );
		} );
		
		
		_api_register( 'settings()', function () {
			return new _Api( this.context, this.context );
		} );
		
		
		_api_register( 'init()', function () {
			var ctx = this.context;
			return ctx.length ? ctx[0].oInit : null;
		} );
		
		
		_api_register( 'data()', function () {
			return this.iterator( 'table', function ( settings ) {
				return _pluck( settings.aoData, '_aData' );
			} ).flatten();
		} );
		
		
		_api_register( 'destroy()', function ( remove ) {
			remove = remove || false;
		
			return this.iterator( 'table', function ( settings ) {
				var orig      = settings.nTableWrapper.parentNode;
				var classes   = settings.oClasses;
				var table     = settings.nTable;
				var tbody     = settings.nTBody;
				var thead     = settings.nTHead;
				var tfoot     = settings.nTFoot;
				var jqTable   = $(table);
				var jqTbody   = $(tbody);
				var jqWrapper = $(settings.nTableWrapper);
				var rows      = $.map( settings.aoData, function (r) { return r.nTr; } );
				var i, ien;
		
				// Flag to note that the table is currently being destroyed - no action
				// should be taken
				settings.bDestroying = true;
		
				// Fire off the destroy callbacks for plug-ins etc
				_fnCallbackFire( settings, "aoDestroyCallback", "destroy", [settings] );
		
				// If not being removed from the document, make all columns visible
				if ( ! remove ) {
					new _Api( settings ).columns().visible( true );
				}
		
				// Blitz all `DT` namespaced events (these are internal events, the
				// lowercase, `dt` events are user subscribed and they are responsible
				// for removing them
				jqWrapper.unbind('.DT').find(':not(tbody *)').unbind('.DT');
				$(window).unbind('.DT-'+settings.sInstance);
		
				// When scrolling we had to break the table up - restore it
				if ( table != thead.parentNode ) {
					jqTable.children('thead').detach();
					jqTable.append( thead );
				}
		
				if ( tfoot && table != tfoot.parentNode ) {
					jqTable.children('tfoot').detach();
					jqTable.append( tfoot );
				}
		
				settings.aaSorting = [];
				settings.aaSortingFixed = [];
				_fnSortingClasses( settings );
		
				$( rows ).removeClass( settings.asStripeClasses.join(' ') );
		
				$('th, td', thead).removeClass( classes.sSortable+' '+
					classes.sSortableAsc+' '+classes.sSortableDesc+' '+classes.sSortableNone
				);
		
				if ( settings.bJUI ) {
					$('th span.'+classes.sSortIcon+ ', td span.'+classes.sSortIcon, thead).detach();
					$('th, td', thead).each( function () {
						var wrapper = $('div.'+classes.sSortJUIWrapper, this);
						$(this).append( wrapper.contents() );
						wrapper.detach();
					} );
				}
		
				// Add the TR elements back into the table in their original order
				jqTbody.children().detach();
				jqTbody.append( rows );
		
				// Remove the DataTables generated nodes, events and classes
				var removedMethod = remove ? 'remove' : 'detach';
				jqTable[ removedMethod ]();
				jqWrapper[ removedMethod ]();
		
				// If we need to reattach the table to the document
				if ( ! remove && orig ) {
					// insertBefore acts like appendChild if !arg[1]
					orig.insertBefore( table, settings.nTableReinsertBefore );
		
					// Restore the width of the original table - was read from the style property,
					// so we can restore directly to that
					jqTable
						.css( 'width', settings.sDestroyWidth )
						.removeClass( classes.sTable );
		
					// If the were originally stripe classes - then we add them back here.
					// Note this is not fool proof (for example if not all rows had stripe
					// classes - but it's a good effort without getting carried away
					ien = settings.asDestroyStripes.length;
		
					if ( ien ) {
						jqTbody.children().each( function (i) {
							$(this).addClass( settings.asDestroyStripes[i % ien] );
						} );
					}
				}
		
				/* Remove the settings object from the settings array */
				var idx = $.inArray( settings, DataTable.settings );
				if ( idx !== -1 ) {
					DataTable.settings.splice( idx, 1 );
				}
			} );
		} );
		
		
		// Add the `every()` method for rows, columns and cells in a compact form
		$.each( [ 'column', 'row', 'cell' ], function ( i, type ) {
			_api_register( type+'s().every()', function ( fn ) {
				var opts = this.selector.opts;
				var api = this;
		
				return this.iterator( type, function ( settings, arg1, arg2, arg3, arg4 ) {
					// Rows and columns:
					//  arg1 - index
					//  arg2 - table counter
					//  arg3 - loop counter
					//  arg4 - undefined
					// Cells:
					//  arg1 - row index
					//  arg2 - column index
					//  arg3 - table counter
					//  arg4 - loop counter
					fn.call(
						api[ type ](
							arg1,
							type==='cell' ? arg2 : opts,
							type==='cell' ? opts : undefined
						),
						arg1, arg2, arg3, arg4
					);
				} );
			} );
		} );
		
		
		// i18n method for extensions to be able to use the language object from the
		// DataTable
		_api_register( 'i18n()', function ( token, def, plural ) {
			var ctx = this.context[0];
			var resolved = _fnGetObjectDataFn( token )( ctx.oLanguage );
		
			if ( resolved === undefined ) {
				resolved = def;
			}
		
			if ( plural !== undefined && $.isPlainObject( resolved ) ) {
				resolved = resolved[ plural ] !== undefined ?
					resolved[ plural ] :
					resolved._;
			}
		
			return resolved.replace( '%d', plural ); // nb: plural might be undefined,
		} );

		/**
		 * Version string for plug-ins to check compatibility. Allowed format is
		 * `a.b.c-d` where: a:int, b:int, c:int, d:string(dev|beta|alpha). `d` is used
		 * only for non-release builds. See http://semver.org/ for more information.
		 *  @member
		 *  @type string
		 *  @default Version number
		 */
		DataTable.version = "1.10.12";

		/**
		 * Private data store, containing all of the settings objects that are
		 * created for the tables on a given page.
		 *
		 * Note that the `DataTable.settings` object is aliased to
		 * `jQuery.fn.dataTableExt` through which it may be accessed and
		 * manipulated, or `jQuery.fn.dataTable.settings`.
		 *  @member
		 *  @type array
		 *  @default []
		 *  @private
		 */
		DataTable.settings = [];

		/**
		 * Object models container, for the various models that DataTables has
		 * available to it. These models define the objects that are used to hold
		 * the active state and configuration of the table.
		 *  @namespace
		 */
		DataTable.models = {};
		
		
		
		/**
		 * Template object for the way in which DataTables holds information about
		 * search information for the global filter and individual column filters.
		 *  @namespace
		 */
		DataTable.models.oSearch = {
			/**
			 * Flag to indicate if the filtering should be case insensitive or not
			 *  @type boolean
			 *  @default true
			 */
			"bCaseInsensitive": true,
		
			/**
			 * Applied search term
			 *  @type string
			 *  @default <i>Empty string</i>
			 */
			"sSearch": "",
		
			/**
			 * Flag to indicate if the search term should be interpreted as a
			 * regular expression (true) or not (false) and therefore and special
			 * regex characters escaped.
			 *  @type boolean
			 *  @default false
			 */
			"bRegex": false,
		
			/**
			 * Flag to indicate if DataTables is to use its smart filtering or not.
			 *  @type boolean
			 *  @default true
			 */
			"bSmart": true
		};
		
		
		
		
		/**
		 * Template object for the way in which DataTables holds information about
		 * each individual row. This is the object format used for the settings
		 * aoData array.
		 *  @namespace
		 */
		DataTable.models.oRow = {
			/**
			 * TR element for the row
			 *  @type node
			 *  @default null
			 */
			"nTr": null,
		
			/**
			 * Array of TD elements for each row. This is null until the row has been
			 * created.
			 *  @type array nodes
			 *  @default []
			 */
			"anCells": null,
		
			/**
			 * Data object from the original data source for the row. This is either
			 * an array if using the traditional form of DataTables, or an object if
			 * using mData options. The exact type will depend on the passed in
			 * data from the data source, or will be an array if using DOM a data
			 * source.
			 *  @type array|object
			 *  @default []
			 */
			"_aData": [],
		
			/**
			 * Sorting data cache - this array is ostensibly the same length as the
			 * number of columns (although each index is generated only as it is
			 * needed), and holds the data that is used for sorting each column in the
			 * row. We do this cache generation at the start of the sort in order that
			 * the formatting of the sort data need be done only once for each cell
			 * per sort. This array should not be read from or written to by anything
			 * other than the master sorting methods.
			 *  @type array
			 *  @default null
			 *  @private
			 */
			"_aSortData": null,
		
			/**
			 * Per cell filtering data cache. As per the sort data cache, used to
			 * increase the performance of the filtering in DataTables
			 *  @type array
			 *  @default null
			 *  @private
			 */
			"_aFilterData": null,
		
			/**
			 * Filtering data cache. This is the same as the cell filtering cache, but
			 * in this case a string rather than an array. This is easily computed with
			 * a join on `_aFilterData`, but is provided as a cache so the join isn't
			 * needed on every search (memory traded for performance)
			 *  @type array
			 *  @default null
			 *  @private
			 */
			"_sFilterRow": null,
		
			/**
			 * Cache of the class name that DataTables has applied to the row, so we
			 * can quickly look at this variable rather than needing to do a DOM check
			 * on className for the nTr property.
			 *  @type string
			 *  @default <i>Empty string</i>
			 *  @private
			 */
			"_sRowStripe": "",
		
			/**
			 * Denote if the original data source was from the DOM, or the data source
			 * object. This is used for invalidating data, so DataTables can
			 * automatically read data from the original source, unless uninstructed
			 * otherwise.
			 *  @type string
			 *  @default null
			 *  @private
			 */
			"src": null,
		
			/**
			 * Index in the aoData array. This saves an indexOf lookup when we have the
			 * object, but want to know the index
			 *  @type integer
			 *  @default -1
			 *  @private
			 */
			"idx": -1
		};
		
		
		/**
		 * Template object for the column information object in DataTables. This object
		 * is held in the settings aoColumns array and contains all the information that
		 * DataTables needs about each individual column.
		 *
		 * Note that this object is related to {@link DataTable.defaults.column}
		 * but this one is the internal data store for DataTables's cache of columns.
		 * It should NOT be manipulated outside of DataTables. Any configuration should
		 * be done through the initialisation options.
		 *  @namespace
		 */
		DataTable.models.oColumn = {
			/**
			 * Column index. This could be worked out on-the-fly with $.inArray, but it
			 * is faster to just hold it as a variable
			 *  @type integer
			 *  @default null
			 */
			"idx": null,
		
			/**
			 * A list of the columns that sorting should occur on when this column
			 * is sorted. That this property is an array allows multi-column sorting
			 * to be defined for a column (for example first name / last name columns
			 * would benefit from this). The values are integers pointing to the
			 * columns to be sorted on (typically it will be a single integer pointing
			 * at itself, but that doesn't need to be the case).
			 *  @type array
			 */
			"aDataSort": null,
		
			/**
			 * Define the sorting directions that are applied to the column, in sequence
			 * as the column is repeatedly sorted upon - i.e. the first value is used
			 * as the sorting direction when the column if first sorted (clicked on).
			 * Sort it again (click again) and it will move on to the next index.
			 * Repeat until loop.
			 *  @type array
			 */
			"asSorting": null,
		
			/**
			 * Flag to indicate if the column is searchable, and thus should be included
			 * in the filtering or not.
			 *  @type boolean
			 */
			"bSearchable": null,
		
			/**
			 * Flag to indicate if the column is sortable or not.
			 *  @type boolean
			 */
			"bSortable": null,
		
			/**
			 * Flag to indicate if the column is currently visible in the table or not
			 *  @type boolean
			 */
			"bVisible": null,
		
			/**
			 * Store for manual type assignment using the `column.type` option. This
			 * is held in store so we can manipulate the column's `sType` property.
			 *  @type string
			 *  @default null
			 *  @private
			 */
			"_sManualType": null,
		
			/**
			 * Flag to indicate if HTML5 data attributes should be used as the data
			 * source for filtering or sorting. True is either are.
			 *  @type boolean
			 *  @default false
			 *  @private
			 */
			"_bAttrSrc": false,
		
			/**
			 * Developer definable function that is called whenever a cell is created (Ajax source,
			 * etc) or processed for input (DOM source). This can be used as a compliment to mRender
			 * allowing you to modify the DOM element (add background colour for example) when the
			 * element is available.
			 *  @type function
			 *  @param {element} nTd The TD node that has been created
			 *  @param {*} sData The Data for the cell
			 *  @param {array|object} oData The data for the whole row
			 *  @param {int} iRow The row index for the aoData data store
			 *  @default null
			 */
			"fnCreatedCell": null,
		
			/**
			 * Function to get data from a cell in a column. You should <b>never</b>
			 * access data directly through _aData internally in DataTables - always use
			 * the method attached to this property. It allows mData to function as
			 * required. This function is automatically assigned by the column
			 * initialisation method
			 *  @type function
			 *  @param {array|object} oData The data array/object for the array
			 *    (i.e. aoData[]._aData)
			 *  @param {string} sSpecific The specific data type you want to get -
			 *    'display', 'type' 'filter' 'sort'
			 *  @returns {*} The data for the cell from the given row's data
			 *  @default null
			 */
			"fnGetData": null,
		
			/**
			 * Function to set data for a cell in the column. You should <b>never</b>
			 * set the data directly to _aData internally in DataTables - always use
			 * this method. It allows mData to function as required. This function
			 * is automatically assigned by the column initialisation method
			 *  @type function
			 *  @param {array|object} oData The data array/object for the array
			 *    (i.e. aoData[]._aData)
			 *  @param {*} sValue Value to set
			 *  @default null
			 */
			"fnSetData": null,
		
			/**
			 * Property to read the value for the cells in the column from the data
			 * source array / object. If null, then the default content is used, if a
			 * function is given then the return from the function is used.
			 *  @type function|int|string|null
			 *  @default null
			 */
			"mData": null,
		
			/**
			 * Partner property to mData which is used (only when defined) to get
			 * the data - i.e. it is basically the same as mData, but without the
			 * 'set' option, and also the data fed to it is the result from mData.
			 * This is the rendering method to match the data method of mData.
			 *  @type function|int|string|null
			 *  @default null
			 */
			"mRender": null,
		
			/**
			 * Unique header TH/TD element for this column - this is what the sorting
			 * listener is attached to (if sorting is enabled.)
			 *  @type node
			 *  @default null
			 */
			"nTh": null,
		
			/**
			 * Unique footer TH/TD element for this column (if there is one). Not used
			 * in DataTables as such, but can be used for plug-ins to reference the
			 * footer for each column.
			 *  @type node
			 *  @default null
			 */
			"nTf": null,
		
			/**
			 * The class to apply to all TD elements in the table's TBODY for the column
			 *  @type string
			 *  @default null
			 */
			"sClass": null,
		
			/**
			 * When DataTables calculates the column widths to assign to each column,
			 * it finds the longest string in each column and then constructs a
			 * temporary table and reads the widths from that. The problem with this
			 * is that "mmm" is much wider then "iiii", but the latter is a longer
			 * string - thus the calculation can go wrong (doing it properly and putting
			 * it into an DOM object and measuring that is horribly(!) slow). Thus as
			 * a "work around" we provide this option. It will append its value to the
			 * text that is found to be the longest string for the column - i.e. padding.
			 *  @type string
			 */
			"sContentPadding": null,
		
			/**
			 * Allows a default value to be given for a column's data, and will be used
			 * whenever a null data source is encountered (this can be because mData
			 * is set to null, or because the data source itself is null).
			 *  @type string
			 *  @default null
			 */
			"sDefaultContent": null,
		
			/**
			 * Name for the column, allowing reference to the column by name as well as
			 * by index (needs a lookup to work by name).
			 *  @type string
			 */
			"sName": null,
		
			/**
			 * Custom sorting data type - defines which of the available plug-ins in
			 * afnSortData the custom sorting will use - if any is defined.
			 *  @type string
			 *  @default std
			 */
			"sSortDataType": 'std',
		
			/**
			 * Class to be applied to the header element when sorting on this column
			 *  @type string
			 *  @default null
			 */
			"sSortingClass": null,
		
			/**
			 * Class to be applied to the header element when sorting on this column -
			 * when jQuery UI theming is used.
			 *  @type string
			 *  @default null
			 */
			"sSortingClassJUI": null,
		
			/**
			 * Title of the column - what is seen in the TH element (nTh).
			 *  @type string
			 */
			"sTitle": null,
		
			/**
			 * Column sorting and filtering type
			 *  @type string
			 *  @default null
			 */
			"sType": null,
		
			/**
			 * Width of the column
			 *  @type string
			 *  @default null
			 */
			"sWidth": null,
		
			/**
			 * Width of the column when it was first "encountered"
			 *  @type string
			 *  @default null
			 */
			"sWidthOrig": null
		};
		
		
		/*
		 * Developer note: The properties of the object below are given in Hungarian
		 * notation, that was used as the interface for DataTables prior to v1.10, however
		 * from v1.10 onwards the primary interface is camel case. In order to avoid
		 * breaking backwards compatibility utterly with this change, the Hungarian
		 * version is still, internally the primary interface, but is is not documented
		 * - hence the @name tags in each doc comment. This allows a Javascript function
		 * to create a map from Hungarian notation to camel case (going the other direction
		 * would require each property to be listed, which would at around 3K to the size
		 * of DataTables, while this method is about a 0.5K hit.
		 *
		 * Ultimately this does pave the way for Hungarian notation to be dropped
		 * completely, but that is a massive amount of work and will break current
		 * installs (therefore is on-hold until v2).
		 */
		
		/**
		 * Initialisation options that can be given to DataTables at initialisation
		 * time.
		 *  @namespace
		 */
		DataTable.defaults = {
			/**
			 * An array of data to use for the table, passed in at initialisation which
			 * will be used in preference to any data which is already in the DOM. This is
			 * particularly useful for constructing tables purely in Javascript, for
			 * example with a custom Ajax call.
			 *  @type array
			 *  @default null
			 *
			 *  @dtopt Option
			 *  @name DataTable.defaults.data
			 *
			 *  @example
			 *    // Using a 2D array data source
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "data": [
			 *          ['Trident', 'Internet Explorer 4.0', 'Win 95+', 4, 'X'],
			 *          ['Trident', 'Internet Explorer 5.0', 'Win 95+', 5, 'C'],
			 *        ],
			 *        "columns": [
			 *          { "title": "Engine" },
			 *          { "title": "Browser" },
			 *          { "title": "Platform" },
			 *          { "title": "Version" },
			 *          { "title": "Grade" }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using an array of objects as a data source (`data`)
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "data": [
			 *          {
			 *            "engine":   "Trident",
			 *            "browser":  "Internet Explorer 4.0",
			 *            "platform": "Win 95+",
			 *            "version":  4,
			 *            "grade":    "X"
			 *          },
			 *          {
			 *            "engine":   "Trident",
			 *            "browser":  "Internet Explorer 5.0",
			 *            "platform": "Win 95+",
			 *            "version":  5,
			 *            "grade":    "C"
			 *          }
			 *        ],
			 *        "columns": [
			 *          { "title": "Engine",   "data": "engine" },
			 *          { "title": "Browser",  "data": "browser" },
			 *          { "title": "Platform", "data": "platform" },
			 *          { "title": "Version",  "data": "version" },
			 *          { "title": "Grade",    "data": "grade" }
			 *        ]
			 *      } );
			 *    } );
			 */
			"aaData": null,
		
		
			/**
			 * If ordering is enabled, then DataTables will perform a first pass sort on
			 * initialisation. You can define which column(s) the sort is performed
			 * upon, and the sorting direction, with this variable. The `sorting` array
			 * should contain an array for each column to be sorted initially containing
			 * the column's index and a direction string ('asc' or 'desc').
			 *  @type array
			 *  @default [[0,'asc']]
			 *
			 *  @dtopt Option
			 *  @name DataTable.defaults.order
			 *
			 *  @example
			 *    // Sort by 3rd column first, and then 4th column
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "order": [[2,'asc'], [3,'desc']]
			 *      } );
			 *    } );
			 *
			 *    // No initial sorting
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "order": []
			 *      } );
			 *    } );
			 */
			"aaSorting": [[0,'asc']],
		
		
			/**
			 * This parameter is basically identical to the `sorting` parameter, but
			 * cannot be overridden by user interaction with the table. What this means
			 * is that you could have a column (visible or hidden) which the sorting
			 * will always be forced on first - any sorting after that (from the user)
			 * will then be performed as required. This can be useful for grouping rows
			 * together.
			 *  @type array
			 *  @default null
			 *
			 *  @dtopt Option
			 *  @name DataTable.defaults.orderFixed
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "orderFixed": [[0,'asc']]
			 *      } );
			 *    } )
			 */
			"aaSortingFixed": [],
		
		
			/**
			 * DataTables can be instructed to load data to display in the table from a
			 * Ajax source. This option defines how that Ajax call is made and where to.
			 *
			 * The `ajax` property has three different modes of operation, depending on
			 * how it is defined. These are:
			 *
			 * * `string` - Set the URL from where the data should be loaded from.
			 * * `object` - Define properties for `jQuery.ajax`.
			 * * `function` - Custom data get function
			 *
			 * `string`
			 * --------
			 *
			 * As a string, the `ajax` property simply defines the URL from which
			 * DataTables will load data.
			 *
			 * `object`
			 * --------
			 *
			 * As an object, the parameters in the object are passed to
			 * [jQuery.ajax](http://api.jquery.com/jQuery.ajax/) allowing fine control
			 * of the Ajax request. DataTables has a number of default parameters which
			 * you can override using this option. Please refer to the jQuery
			 * documentation for a full description of the options available, although
			 * the following parameters provide additional options in DataTables or
			 * require special consideration:
			 *
			 * * `data` - As with jQuery, `data` can be provided as an object, but it
			 *   can also be used as a function to manipulate the data DataTables sends
			 *   to the server. The function takes a single parameter, an object of
			 *   parameters with the values that DataTables has readied for sending. An
			 *   object may be returned which will be merged into the DataTables
			 *   defaults, or you can add the items to the object that was passed in and
			 *   not return anything from the function. This supersedes `fnServerParams`
			 *   from DataTables 1.9-.
			 *
			 * * `dataSrc` - By default DataTables will look for the property `data` (or
			 *   `aaData` for compatibility with DataTables 1.9-) when obtaining data
			 *   from an Ajax source or for server-side processing - this parameter
			 *   allows that property to be changed. You can use Javascript dotted
			 *   object notation to get a data source for multiple levels of nesting, or
			 *   it my be used as a function. As a function it takes a single parameter,
			 *   the JSON returned from the server, which can be manipulated as
			 *   required, with the returned value being that used by DataTables as the
			 *   data source for the table. This supersedes `sAjaxDataProp` from
			 *   DataTables 1.9-.
			 *
			 * * `success` - Should not be overridden it is used internally in
			 *   DataTables. To manipulate / transform the data returned by the server
			 *   use `ajax.dataSrc`, or use `ajax` as a function (see below).
			 *
			 * `function`
			 * ----------
			 *
			 * As a function, making the Ajax call is left up to yourself allowing
			 * complete control of the Ajax request. Indeed, if desired, a method other
			 * than Ajax could be used to obtain the required data, such as Web storage
			 * or an AIR database.
			 *
			 * The function is given four parameters and no return is required. The
			 * parameters are:
			 *
			 * 1. _object_ - Data to send to the server
			 * 2. _function_ - Callback function that must be executed when the required
			 *    data has been obtained. That data should be passed into the callback
			 *    as the only parameter
			 * 3. _object_ - DataTables settings object for the table
			 *
			 * Note that this supersedes `fnServerData` from DataTables 1.9-.
			 *
			 *  @type string|object|function
			 *  @default null
			 *
			 *  @dtopt Option
			 *  @name DataTable.defaults.ajax
			 *  @since 1.10.0
			 *
			 * @example
			 *   // Get JSON data from a file via Ajax.
			 *   // Note DataTables expects data in the form `{ data: [ ...data... ] }` by default).
			 *   $('#example').dataTable( {
			 *     "ajax": "data.json"
			 *   } );
			 *
			 * @example
			 *   // Get JSON data from a file via Ajax, using `dataSrc` to change
			 *   // `data` to `tableData` (i.e. `{ tableData: [ ...data... ] }`)
			 *   $('#example').dataTable( {
			 *     "ajax": {
			 *       "url": "data.json",
			 *       "dataSrc": "tableData"
			 *     }
			 *   } );
			 *
			 * @example
			 *   // Get JSON data from a file via Ajax, using `dataSrc` to read data
			 *   // from a plain array rather than an array in an object
			 *   $('#example').dataTable( {
			 *     "ajax": {
			 *       "url": "data.json",
			 *       "dataSrc": ""
			 *     }
			 *   } );
			 *
			 * @example
			 *   // Manipulate the data returned from the server - add a link to data
			 *   // (note this can, should, be done using `render` for the column - this
			 *   // is just a simple example of how the data can be manipulated).
			 *   $('#example').dataTable( {
			 *     "ajax": {
			 *       "url": "data.json",
			 *       "dataSrc": function ( json ) {
			 *         for ( var i=0, ien=json.length ; i<ien ; i++ ) {
			 *           json[i][0] = '<a href="/message/'+json[i][0]+'>View message</a>';
			 *         }
			 *         return json;
			 *       }
			 *     }
			 *   } );
			 *
			 * @example
			 *   // Add data to the request
			 *   $('#example').dataTable( {
			 *     "ajax": {
			 *       "url": "data.json",
			 *       "data": function ( d ) {
			 *         return {
			 *           "extra_search": $('#extra').val()
			 *         };
			 *       }
			 *     }
			 *   } );
			 *
			 * @example
			 *   // Send request as POST
			 *   $('#example').dataTable( {
			 *     "ajax": {
			 *       "url": "data.json",
			 *       "type": "POST"
			 *     }
			 *   } );
			 *
			 * @example
			 *   // Get the data from localStorage (could interface with a form for
			 *   // adding, editing and removing rows).
			 *   $('#example').dataTable( {
			 *     "ajax": function (data, callback, settings) {
			 *       callback(
			 *         JSON.parse( localStorage.getItem('dataTablesData') )
			 *       );
			 *     }
			 *   } );
			 */
			"ajax": null,
		
		
			/**
			 * This parameter allows you to readily specify the entries in the length drop
			 * down menu that DataTables shows when pagination is enabled. It can be
			 * either a 1D array of options which will be used for both the displayed
			 * option and the value, or a 2D array which will use the array in the first
			 * position as the value, and the array in the second position as the
			 * displayed options (useful for language strings such as 'All').
			 *
			 * Note that the `pageLength` property will be automatically set to the
			 * first value given in this array, unless `pageLength` is also provided.
			 *  @type array
			 *  @default [ 10, 25, 50, 100 ]
			 *
			 *  @dtopt Option
			 *  @name DataTable.defaults.lengthMenu
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
			 *      } );
			 *    } );
			 */
			"aLengthMenu": [ 10, 25, 50, 100 ],
		
		
			/**
			 * The `columns` option in the initialisation parameter allows you to define
			 * details about the way individual columns behave. For a full list of
			 * column options that can be set, please see
			 * {@link DataTable.defaults.column}. Note that if you use `columns` to
			 * define your columns, you must have an entry in the array for every single
			 * column that you have in your table (these can be null if you don't which
			 * to specify any options).
			 *  @member
			 *
			 *  @name DataTable.defaults.column
			 */
			"aoColumns": null,
		
			/**
			 * Very similar to `columns`, `columnDefs` allows you to target a specific
			 * column, multiple columns, or all columns, using the `targets` property of
			 * each object in the array. This allows great flexibility when creating
			 * tables, as the `columnDefs` arrays can be of any length, targeting the
			 * columns you specifically want. `columnDefs` may use any of the column
			 * options available: {@link DataTable.defaults.column}, but it _must_
			 * have `targets` defined in each object in the array. Values in the `targets`
			 * array may be:
			 *   <ul>
			 *     <li>a string - class name will be matched on the TH for the column</li>
			 *     <li>0 or a positive integer - column index counting from the left</li>
			 *     <li>a negative integer - column index counting from the right</li>
			 *     <li>the string "_all" - all columns (i.e. assign a default)</li>
			 *   </ul>
			 *  @member
			 *
			 *  @name DataTable.defaults.columnDefs
			 */
			"aoColumnDefs": null,
		
		
			/**
			 * Basically the same as `search`, this parameter defines the individual column
			 * filtering state at initialisation time. The array must be of the same size
			 * as the number of columns, and each element be an object with the parameters
			 * `search` and `escapeRegex` (the latter is optional). 'null' is also
			 * accepted and the default will be used.
			 *  @type array
			 *  @default []
			 *
			 *  @dtopt Option
			 *  @name DataTable.defaults.searchCols
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "searchCols": [
			 *          null,
			 *          { "search": "My filter" },
			 *          null,
			 *          { "search": "^[0-9]", "escapeRegex": false }
			 *        ]
			 *      } );
			 *    } )
			 */
			"aoSearchCols": [],
		
		
			/**
			 * An array of CSS classes that should be applied to displayed rows. This
			 * array may be of any length, and DataTables will apply each class
			 * sequentially, looping when required.
			 *  @type array
			 *  @default null <i>Will take the values determined by the `oClasses.stripe*`
			 *    options</i>
			 *
			 *  @dtopt Option
			 *  @name DataTable.defaults.stripeClasses
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "stripeClasses": [ 'strip1', 'strip2', 'strip3' ]
			 *      } );
			 *    } )
			 */
			"asStripeClasses": null,
		
		
			/**
			 * Enable or disable automatic column width calculation. This can be disabled
			 * as an optimisation (it takes some time to calculate the widths) if the
			 * tables widths are passed in using `columns`.
			 *  @type boolean
			 *  @default true
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.autoWidth
			 *
			 *  @example
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "autoWidth": false
			 *      } );
			 *    } );
			 */
			"bAutoWidth": true,
		
		
			/**
			 * Deferred rendering can provide DataTables with a huge speed boost when you
			 * are using an Ajax or JS data source for the table. This option, when set to
			 * true, will cause DataTables to defer the creation of the table elements for
			 * each row until they are needed for a draw - saving a significant amount of
			 * time.
			 *  @type boolean
			 *  @default false
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.deferRender
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "ajax": "sources/arrays.txt",
			 *        "deferRender": true
			 *      } );
			 *    } );
			 */
			"bDeferRender": false,
		
		
			/**
			 * Replace a DataTable which matches the given selector and replace it with
			 * one which has the properties of the new initialisation object passed. If no
			 * table matches the selector, then the new DataTable will be constructed as
			 * per normal.
			 *  @type boolean
			 *  @default false
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.destroy
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "srollY": "200px",
			 *        "paginate": false
			 *      } );
			 *
			 *      // Some time later....
			 *      $('#example').dataTable( {
			 *        "filter": false,
			 *        "destroy": true
			 *      } );
			 *    } );
			 */
			"bDestroy": false,
		
		
			/**
			 * Enable or disable filtering of data. Filtering in DataTables is "smart" in
			 * that it allows the end user to input multiple words (space separated) and
			 * will match a row containing those words, even if not in the order that was
			 * specified (this allow matching across multiple columns). Note that if you
			 * wish to use filtering in DataTables this must remain 'true' - to remove the
			 * default filtering input box and retain filtering abilities, please use
			 * {@link DataTable.defaults.dom}.
			 *  @type boolean
			 *  @default true
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.searching
			 *
			 *  @example
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "searching": false
			 *      } );
			 *    } );
			 */
			"bFilter": true,
		
		
			/**
			 * Enable or disable the table information display. This shows information
			 * about the data that is currently visible on the page, including information
			 * about filtered data if that action is being performed.
			 *  @type boolean
			 *  @default true
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.info
			 *
			 *  @example
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "info": false
			 *      } );
			 *    } );
			 */
			"bInfo": true,
		
		
			/**
			 * Enable jQuery UI ThemeRoller support (required as ThemeRoller requires some
			 * slightly different and additional mark-up from what DataTables has
			 * traditionally used).
			 *  @type boolean
			 *  @default false
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.jQueryUI
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "jQueryUI": true
			 *      } );
			 *    } );
			 */
			"bJQueryUI": false,
		
		
			/**
			 * Allows the end user to select the size of a formatted page from a select
			 * menu (sizes are 10, 25, 50 and 100). Requires pagination (`paginate`).
			 *  @type boolean
			 *  @default true
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.lengthChange
			 *
			 *  @example
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "lengthChange": false
			 *      } );
			 *    } );
			 */
			"bLengthChange": true,
		
		
			/**
			 * Enable or disable pagination.
			 *  @type boolean
			 *  @default true
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.paging
			 *
			 *  @example
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "paging": false
			 *      } );
			 *    } );
			 */
			"bPaginate": true,
		
		
			/**
			 * Enable or disable the display of a 'processing' indicator when the table is
			 * being processed (e.g. a sort). This is particularly useful for tables with
			 * large amounts of data where it can take a noticeable amount of time to sort
			 * the entries.
			 *  @type boolean
			 *  @default false
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.processing
			 *
			 *  @example
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "processing": true
			 *      } );
			 *    } );
			 */
			"bProcessing": false,
		
		
			/**
			 * Retrieve the DataTables object for the given selector. Note that if the
			 * table has already been initialised, this parameter will cause DataTables
			 * to simply return the object that has already been set up - it will not take
			 * account of any changes you might have made to the initialisation object
			 * passed to DataTables (setting this parameter to true is an acknowledgement
			 * that you understand this). `destroy` can be used to reinitialise a table if
			 * you need.
			 *  @type boolean
			 *  @default false
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.retrieve
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      initTable();
			 *      tableActions();
			 *    } );
			 *
			 *    function initTable ()
			 *    {
			 *      return $('#example').dataTable( {
			 *        "scrollY": "200px",
			 *        "paginate": false,
			 *        "retrieve": true
			 *      } );
			 *    }
			 *
			 *    function tableActions ()
			 *    {
			 *      var table = initTable();
			 *      // perform API operations with oTable
			 *    }
			 */
			"bRetrieve": false,
		
		
			/**
			 * When vertical (y) scrolling is enabled, DataTables will force the height of
			 * the table's viewport to the given height at all times (useful for layout).
			 * However, this can look odd when filtering data down to a small data set,
			 * and the footer is left "floating" further down. This parameter (when
			 * enabled) will cause DataTables to collapse the table's viewport down when
			 * the result set will fit within the given Y height.
			 *  @type boolean
			 *  @default false
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.scrollCollapse
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "scrollY": "200",
			 *        "scrollCollapse": true
			 *      } );
			 *    } );
			 */
			"bScrollCollapse": false,
		
		
			/**
			 * Configure DataTables to use server-side processing. Note that the
			 * `ajax` parameter must also be given in order to give DataTables a
			 * source to obtain the required data for each draw.
			 *  @type boolean
			 *  @default false
			 *
			 *  @dtopt Features
			 *  @dtopt Server-side
			 *  @name DataTable.defaults.serverSide
			 *
			 *  @example
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "serverSide": true,
			 *        "ajax": "xhr.php"
			 *      } );
			 *    } );
			 */
			"bServerSide": false,
		
		
			/**
			 * Enable or disable sorting of columns. Sorting of individual columns can be
			 * disabled by the `sortable` option for each column.
			 *  @type boolean
			 *  @default true
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.ordering
			 *
			 *  @example
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "ordering": false
			 *      } );
			 *    } );
			 */
			"bSort": true,
		
		
			/**
			 * Enable or display DataTables' ability to sort multiple columns at the
			 * same time (activated by shift-click by the user).
			 *  @type boolean
			 *  @default true
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.orderMulti
			 *
			 *  @example
			 *    // Disable multiple column sorting ability
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "orderMulti": false
			 *      } );
			 *    } );
			 */
			"bSortMulti": true,
		
		
			/**
			 * Allows control over whether DataTables should use the top (true) unique
			 * cell that is found for a single column, or the bottom (false - default).
			 * This is useful when using complex headers.
			 *  @type boolean
			 *  @default false
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.orderCellsTop
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "orderCellsTop": true
			 *      } );
			 *    } );
			 */
			"bSortCellsTop": false,
		
		
			/**
			 * Enable or disable the addition of the classes `sorting\_1`, `sorting\_2` and
			 * `sorting\_3` to the columns which are currently being sorted on. This is
			 * presented as a feature switch as it can increase processing time (while
			 * classes are removed and added) so for large data sets you might want to
			 * turn this off.
			 *  @type boolean
			 *  @default true
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.orderClasses
			 *
			 *  @example
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "orderClasses": false
			 *      } );
			 *    } );
			 */
			"bSortClasses": true,
		
		
			/**
			 * Enable or disable state saving. When enabled HTML5 `localStorage` will be
			 * used to save table display information such as pagination information,
			 * display length, filtering and sorting. As such when the end user reloads
			 * the page the display display will match what thy had previously set up.
			 *
			 * Due to the use of `localStorage` the default state saving is not supported
			 * in IE6 or 7. If state saving is required in those browsers, use
			 * `stateSaveCallback` to provide a storage solution such as cookies.
			 *  @type boolean
			 *  @default false
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.stateSave
			 *
			 *  @example
			 *    $(document).ready( function () {
			 *      $('#example').dataTable( {
			 *        "stateSave": true
			 *      } );
			 *    } );
			 */
			"bStateSave": false,
		
		
			/**
			 * This function is called when a TR element is created (and all TD child
			 * elements have been inserted), or registered if using a DOM source, allowing
			 * manipulation of the TR element (adding classes etc).
			 *  @type function
			 *  @param {node} row "TR" element for the current row
			 *  @param {array} data Raw data array for this row
			 *  @param {int} dataIndex The index of this row in the internal aoData array
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.createdRow
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "createdRow": function( row, data, dataIndex ) {
			 *          // Bold the grade for all 'A' grade browsers
			 *          if ( data[4] == "A" )
			 *          {
			 *            $('td:eq(4)', row).html( '<b>A</b>' );
			 *          }
			 *        }
			 *      } );
			 *    } );
			 */
			"fnCreatedRow": null,
		
		
			/**
			 * This function is called on every 'draw' event, and allows you to
			 * dynamically modify any aspect you want about the created DOM.
			 *  @type function
			 *  @param {object} settings DataTables settings object
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.drawCallback
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "drawCallback": function( settings ) {
			 *          alert( 'DataTables has redrawn the table' );
			 *        }
			 *      } );
			 *    } );
			 */
			"fnDrawCallback": null,
		
		
			/**
			 * Identical to fnHeaderCallback() but for the table footer this function
			 * allows you to modify the table footer on every 'draw' event.
			 *  @type function
			 *  @param {node} foot "TR" element for the footer
			 *  @param {array} data Full table data (as derived from the original HTML)
			 *  @param {int} start Index for the current display starting point in the
			 *    display array
			 *  @param {int} end Index for the current display ending point in the
			 *    display array
			 *  @param {array int} display Index array to translate the visual position
			 *    to the full data array
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.footerCallback
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "footerCallback": function( tfoot, data, start, end, display ) {
			 *          tfoot.getElementsByTagName('th')[0].innerHTML = "Starting index is "+start;
			 *        }
			 *      } );
			 *    } )
			 */
			"fnFooterCallback": null,
		
		
			/**
			 * When rendering large numbers in the information element for the table
			 * (i.e. "Showing 1 to 10 of 57 entries") DataTables will render large numbers
			 * to have a comma separator for the 'thousands' units (e.g. 1 million is
			 * rendered as "1,000,000") to help readability for the end user. This
			 * function will override the default method DataTables uses.
			 *  @type function
			 *  @member
			 *  @param {int} toFormat number to be formatted
			 *  @returns {string} formatted string for DataTables to show the number
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.formatNumber
			 *
			 *  @example
			 *    // Format a number using a single quote for the separator (note that
			 *    // this can also be done with the language.thousands option)
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "formatNumber": function ( toFormat ) {
			 *          return toFormat.toString().replace(
			 *            /\B(?=(\d{3})+(?!\d))/g, "'"
			 *          );
			 *        };
			 *      } );
			 *    } );
			 */
			"fnFormatNumber": function ( toFormat ) {
				return toFormat.toString().replace(
					/\B(?=(\d{3})+(?!\d))/g,
					this.oLanguage.sThousands
				);
			},
		
		
			/**
			 * This function is called on every 'draw' event, and allows you to
			 * dynamically modify the header row. This can be used to calculate and
			 * display useful information about the table.
			 *  @type function
			 *  @param {node} head "TR" element for the header
			 *  @param {array} data Full table data (as derived from the original HTML)
			 *  @param {int} start Index for the current display starting point in the
			 *    display array
			 *  @param {int} end Index for the current display ending point in the
			 *    display array
			 *  @param {array int} display Index array to translate the visual position
			 *    to the full data array
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.headerCallback
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "fheaderCallback": function( head, data, start, end, display ) {
			 *          head.getElementsByTagName('th')[0].innerHTML = "Displaying "+(end-start)+" records";
			 *        }
			 *      } );
			 *    } )
			 */
			"fnHeaderCallback": null,
		
		
			/**
			 * The information element can be used to convey information about the current
			 * state of the table. Although the internationalisation options presented by
			 * DataTables are quite capable of dealing with most customisations, there may
			 * be times where you wish to customise the string further. This callback
			 * allows you to do exactly that.
			 *  @type function
			 *  @param {object} oSettings DataTables settings object
			 *  @param {int} start Starting position in data for the draw
			 *  @param {int} end End position in data for the draw
			 *  @param {int} max Total number of rows in the table (regardless of
			 *    filtering)
			 *  @param {int} total Total number of rows in the data set, after filtering
			 *  @param {string} pre The string that DataTables has formatted using it's
			 *    own rules
			 *  @returns {string} The string to be displayed in the information element.
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.infoCallback
			 *
			 *  @example
			 *    $('#example').dataTable( {
			 *      "infoCallback": function( settings, start, end, max, total, pre ) {
			 *        return start +" to "+ end;
			 *      }
			 *    } );
			 */
			"fnInfoCallback": null,
		
		
			/**
			 * Called when the table has been initialised. Normally DataTables will
			 * initialise sequentially and there will be no need for this function,
			 * however, this does not hold true when using external language information
			 * since that is obtained using an async XHR call.
			 *  @type function
			 *  @param {object} settings DataTables settings object
			 *  @param {object} json The JSON object request from the server - only
			 *    present if client-side Ajax sourced data is used
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.initComplete
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "initComplete": function(settings, json) {
			 *          alert( 'DataTables has finished its initialisation.' );
			 *        }
			 *      } );
			 *    } )
			 */
			"fnInitComplete": null,
		
		
			/**
			 * Called at the very start of each table draw and can be used to cancel the
			 * draw by returning false, any other return (including undefined) results in
			 * the full draw occurring).
			 *  @type function
			 *  @param {object} settings DataTables settings object
			 *  @returns {boolean} False will cancel the draw, anything else (including no
			 *    return) will allow it to complete.
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.preDrawCallback
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "preDrawCallback": function( settings ) {
			 *          if ( $('#test').val() == 1 ) {
			 *            return false;
			 *          }
			 *        }
			 *      } );
			 *    } );
			 */
			"fnPreDrawCallback": null,
		
		
			/**
			 * This function allows you to 'post process' each row after it have been
			 * generated for each table draw, but before it is rendered on screen. This
			 * function might be used for setting the row class name etc.
			 *  @type function
			 *  @param {node} row "TR" element for the current row
			 *  @param {array} data Raw data array for this row
			 *  @param {int} displayIndex The display index for the current table draw
			 *  @param {int} displayIndexFull The index of the data in the full list of
			 *    rows (after filtering)
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.rowCallback
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "rowCallback": function( row, data, displayIndex, displayIndexFull ) {
			 *          // Bold the grade for all 'A' grade browsers
			 *          if ( data[4] == "A" ) {
			 *            $('td:eq(4)', row).html( '<b>A</b>' );
			 *          }
			 *        }
			 *      } );
			 *    } );
			 */
			"fnRowCallback": null,
		
		
			/**
			 * __Deprecated__ The functionality provided by this parameter has now been
			 * superseded by that provided through `ajax`, which should be used instead.
			 *
			 * This parameter allows you to override the default function which obtains
			 * the data from the server so something more suitable for your application.
			 * For example you could use POST data, or pull information from a Gears or
			 * AIR database.
			 *  @type function
			 *  @member
			 *  @param {string} source HTTP source to obtain the data from (`ajax`)
			 *  @param {array} data A key/value pair object containing the data to send
			 *    to the server
			 *  @param {function} callback to be called on completion of the data get
			 *    process that will draw the data on the page.
			 *  @param {object} settings DataTables settings object
			 *
			 *  @dtopt Callbacks
			 *  @dtopt Server-side
			 *  @name DataTable.defaults.serverData
			 *
			 *  @deprecated 1.10. Please use `ajax` for this functionality now.
			 */
			"fnServerData": null,
		
		
			/**
			 * __Deprecated__ The functionality provided by this parameter has now been
			 * superseded by that provided through `ajax`, which should be used instead.
			 *
			 *  It is often useful to send extra data to the server when making an Ajax
			 * request - for example custom filtering information, and this callback
			 * function makes it trivial to send extra information to the server. The
			 * passed in parameter is the data set that has been constructed by
			 * DataTables, and you can add to this or modify it as you require.
			 *  @type function
			 *  @param {array} data Data array (array of objects which are name/value
			 *    pairs) that has been constructed by DataTables and will be sent to the
			 *    server. In the case of Ajax sourced data with server-side processing
			 *    this will be an empty array, for server-side processing there will be a
			 *    significant number of parameters!
			 *  @returns {undefined} Ensure that you modify the data array passed in,
			 *    as this is passed by reference.
			 *
			 *  @dtopt Callbacks
			 *  @dtopt Server-side
			 *  @name DataTable.defaults.serverParams
			 *
			 *  @deprecated 1.10. Please use `ajax` for this functionality now.
			 */
			"fnServerParams": null,
		
		
			/**
			 * Load the table state. With this function you can define from where, and how, the
			 * state of a table is loaded. By default DataTables will load from `localStorage`
			 * but you might wish to use a server-side database or cookies.
			 *  @type function
			 *  @member
			 *  @param {object} settings DataTables settings object
			 *  @return {object} The DataTables state object to be loaded
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.stateLoadCallback
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "stateSave": true,
			 *        "stateLoadCallback": function (settings) {
			 *          var o;
			 *
			 *          // Send an Ajax request to the server to get the data. Note that
			 *          // this is a synchronous request.
			 *          $.ajax( {
			 *            "url": "/state_load",
			 *            "async": false,
			 *            "dataType": "json",
			 *            "success": function (json) {
			 *              o = json;
			 *            }
			 *          } );
			 *
			 *          return o;
			 *        }
			 *      } );
			 *    } );
			 */
			"fnStateLoadCallback": function ( settings ) {
				try {
					return JSON.parse(
						(settings.iStateDuration === -1 ? sessionStorage : localStorage).getItem(
							'DataTables_'+settings.sInstance+'_'+location.pathname
						)
					);
				} catch (e) {}
			},
		
		
			/**
			 * Callback which allows modification of the saved state prior to loading that state.
			 * This callback is called when the table is loading state from the stored data, but
			 * prior to the settings object being modified by the saved state. Note that for
			 * plug-in authors, you should use the `stateLoadParams` event to load parameters for
			 * a plug-in.
			 *  @type function
			 *  @param {object} settings DataTables settings object
			 *  @param {object} data The state object that is to be loaded
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.stateLoadParams
			 *
			 *  @example
			 *    // Remove a saved filter, so filtering is never loaded
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "stateSave": true,
			 *        "stateLoadParams": function (settings, data) {
			 *          data.oSearch.sSearch = "";
			 *        }
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Disallow state loading by returning false
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "stateSave": true,
			 *        "stateLoadParams": function (settings, data) {
			 *          return false;
			 *        }
			 *      } );
			 *    } );
			 */
			"fnStateLoadParams": null,
		
		
			/**
			 * Callback that is called when the state has been loaded from the state saving method
			 * and the DataTables settings object has been modified as a result of the loaded state.
			 *  @type function
			 *  @param {object} settings DataTables settings object
			 *  @param {object} data The state object that was loaded
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.stateLoaded
			 *
			 *  @example
			 *    // Show an alert with the filtering value that was saved
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "stateSave": true,
			 *        "stateLoaded": function (settings, data) {
			 *          alert( 'Saved filter was: '+data.oSearch.sSearch );
			 *        }
			 *      } );
			 *    } );
			 */
			"fnStateLoaded": null,
		
		
			/**
			 * Save the table state. This function allows you to define where and how the state
			 * information for the table is stored By default DataTables will use `localStorage`
			 * but you might wish to use a server-side database or cookies.
			 *  @type function
			 *  @member
			 *  @param {object} settings DataTables settings object
			 *  @param {object} data The state object to be saved
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.stateSaveCallback
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "stateSave": true,
			 *        "stateSaveCallback": function (settings, data) {
			 *          // Send an Ajax request to the server with the state object
			 *          $.ajax( {
			 *            "url": "/state_save",
			 *            "data": data,
			 *            "dataType": "json",
			 *            "method": "POST"
			 *            "success": function () {}
			 *          } );
			 *        }
			 *      } );
			 *    } );
			 */
			"fnStateSaveCallback": function ( settings, data ) {
				try {
					(settings.iStateDuration === -1 ? sessionStorage : localStorage).setItem(
						'DataTables_'+settings.sInstance+'_'+location.pathname,
						JSON.stringify( data )
					);
				} catch (e) {}
			},
		
		
			/**
			 * Callback which allows modification of the state to be saved. Called when the table
			 * has changed state a new state save is required. This method allows modification of
			 * the state saving object prior to actually doing the save, including addition or
			 * other state properties or modification. Note that for plug-in authors, you should
			 * use the `stateSaveParams` event to save parameters for a plug-in.
			 *  @type function
			 *  @param {object} settings DataTables settings object
			 *  @param {object} data The state object to be saved
			 *
			 *  @dtopt Callbacks
			 *  @name DataTable.defaults.stateSaveParams
			 *
			 *  @example
			 *    // Remove a saved filter, so filtering is never saved
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "stateSave": true,
			 *        "stateSaveParams": function (settings, data) {
			 *          data.oSearch.sSearch = "";
			 *        }
			 *      } );
			 *    } );
			 */
			"fnStateSaveParams": null,
		
		
			/**
			 * Duration for which the saved state information is considered valid. After this period
			 * has elapsed the state will be returned to the default.
			 * Value is given in seconds.
			 *  @type int
			 *  @default 7200 <i>(2 hours)</i>
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.stateDuration
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "stateDuration": 60*60*24; // 1 day
			 *      } );
			 *    } )
			 */
			"iStateDuration": 7200,
		
		
			/**
			 * When enabled DataTables will not make a request to the server for the first
			 * page draw - rather it will use the data already on the page (no sorting etc
			 * will be applied to it), thus saving on an XHR at load time. `deferLoading`
			 * is used to indicate that deferred loading is required, but it is also used
			 * to tell DataTables how many records there are in the full table (allowing
			 * the information element and pagination to be displayed correctly). In the case
			 * where a filtering is applied to the table on initial load, this can be
			 * indicated by giving the parameter as an array, where the first element is
			 * the number of records available after filtering and the second element is the
			 * number of records without filtering (allowing the table information element
			 * to be shown correctly).
			 *  @type int | array
			 *  @default null
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.deferLoading
			 *
			 *  @example
			 *    // 57 records available in the table, no filtering applied
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "serverSide": true,
			 *        "ajax": "scripts/server_processing.php",
			 *        "deferLoading": 57
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // 57 records after filtering, 100 without filtering (an initial filter applied)
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "serverSide": true,
			 *        "ajax": "scripts/server_processing.php",
			 *        "deferLoading": [ 57, 100 ],
			 *        "search": {
			 *          "search": "my_filter"
			 *        }
			 *      } );
			 *    } );
			 */
			"iDeferLoading": null,
		
		
			/**
			 * Number of rows to display on a single page when using pagination. If
			 * feature enabled (`lengthChange`) then the end user will be able to override
			 * this to a custom setting using a pop-up menu.
			 *  @type int
			 *  @default 10
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.pageLength
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "pageLength": 50
			 *      } );
			 *    } )
			 */
			"iDisplayLength": 10,
		
		
			/**
			 * Define the starting point for data display when using DataTables with
			 * pagination. Note that this parameter is the number of records, rather than
			 * the page number, so if you have 10 records per page and want to start on
			 * the third page, it should be "20".
			 *  @type int
			 *  @default 0
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.displayStart
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "displayStart": 20
			 *      } );
			 *    } )
			 */
			"iDisplayStart": 0,
		
		
			/**
			 * By default DataTables allows keyboard navigation of the table (sorting, paging,
			 * and filtering) by adding a `tabindex` attribute to the required elements. This
			 * allows you to tab through the controls and press the enter key to activate them.
			 * The tabindex is default 0, meaning that the tab follows the flow of the document.
			 * You can overrule this using this parameter if you wish. Use a value of -1 to
			 * disable built-in keyboard navigation.
			 *  @type int
			 *  @default 0
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.tabIndex
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "tabIndex": 1
			 *      } );
			 *    } );
			 */
			"iTabIndex": 0,
		
		
			/**
			 * Classes that DataTables assigns to the various components and features
			 * that it adds to the HTML table. This allows classes to be configured
			 * during initialisation in addition to through the static
			 * {@link DataTable.ext.oStdClasses} object).
			 *  @namespace
			 *  @name DataTable.defaults.classes
			 */
			"oClasses": {},
		
		
			/**
			 * All strings that DataTables uses in the user interface that it creates
			 * are defined in this object, allowing you to modified them individually or
			 * completely replace them all as required.
			 *  @namespace
			 *  @name DataTable.defaults.language
			 */
			"oLanguage": {
				/**
				 * Strings that are used for WAI-ARIA labels and controls only (these are not
				 * actually visible on the page, but will be read by screenreaders, and thus
				 * must be internationalised as well).
				 *  @namespace
				 *  @name DataTable.defaults.language.aria
				 */
				"oAria": {
					/**
					 * ARIA label that is added to the table headers when the column may be
					 * sorted ascending by activing the column (click or return when focused).
					 * Note that the column header is prefixed to this string.
					 *  @type string
					 *  @default : activate to sort column ascending
					 *
					 *  @dtopt Language
					 *  @name DataTable.defaults.language.aria.sortAscending
					 *
					 *  @example
					 *    $(document).ready( function() {
					 *      $('#example').dataTable( {
					 *        "language": {
					 *          "aria": {
					 *            "sortAscending": " - click/return to sort ascending"
					 *          }
					 *        }
					 *      } );
					 *    } );
					 */
					"sSortAscending": ": activate to sort column ascending",
		
					/**
					 * ARIA label that is added to the table headers when the column may be
					 * sorted descending by activing the column (click or return when focused).
					 * Note that the column header is prefixed to this string.
					 *  @type string
					 *  @default : activate to sort column ascending
					 *
					 *  @dtopt Language
					 *  @name DataTable.defaults.language.aria.sortDescending
					 *
					 *  @example
					 *    $(document).ready( function() {
					 *      $('#example').dataTable( {
					 *        "language": {
					 *          "aria": {
					 *            "sortDescending": " - click/return to sort descending"
					 *          }
					 *        }
					 *      } );
					 *    } );
					 */
					"sSortDescending": ": activate to sort column descending"
				},
		
				/**
				 * Pagination string used by DataTables for the built-in pagination
				 * control types.
				 *  @namespace
				 *  @name DataTable.defaults.language.paginate
				 */
				"oPaginate": {
					/**
					 * Text to use when using the 'full_numbers' type of pagination for the
					 * button to take the user to the first page.
					 *  @type string
					 *  @default First
					 *
					 *  @dtopt Language
					 *  @name DataTable.defaults.language.paginate.first
					 *
					 *  @example
					 *    $(document).ready( function() {
					 *      $('#example').dataTable( {
					 *        "language": {
					 *          "paginate": {
					 *            "first": "First page"
					 *          }
					 *        }
					 *      } );
					 *    } );
					 */
					"sFirst": "First",
		
		
					/**
					 * Text to use when using the 'full_numbers' type of pagination for the
					 * button to take the user to the last page.
					 *  @type string
					 *  @default Last
					 *
					 *  @dtopt Language
					 *  @name DataTable.defaults.language.paginate.last
					 *
					 *  @example
					 *    $(document).ready( function() {
					 *      $('#example').dataTable( {
					 *        "language": {
					 *          "paginate": {
					 *            "last": "Last page"
					 *          }
					 *        }
					 *      } );
					 *    } );
					 */
					"sLast": "Last",
		
		
					/**
					 * Text to use for the 'next' pagination button (to take the user to the
					 * next page).
					 *  @type string
					 *  @default Next
					 *
					 *  @dtopt Language
					 *  @name DataTable.defaults.language.paginate.next
					 *
					 *  @example
					 *    $(document).ready( function() {
					 *      $('#example').dataTable( {
					 *        "language": {
					 *          "paginate": {
					 *            "next": "Next page"
					 *          }
					 *        }
					 *      } );
					 *    } );
					 */
					"sNext": "Next",
		
		
					/**
					 * Text to use for the 'previous' pagination button (to take the user to
					 * the previous page).
					 *  @type string
					 *  @default Previous
					 *
					 *  @dtopt Language
					 *  @name DataTable.defaults.language.paginate.previous
					 *
					 *  @example
					 *    $(document).ready( function() {
					 *      $('#example').dataTable( {
					 *        "language": {
					 *          "paginate": {
					 *            "previous": "Previous page"
					 *          }
					 *        }
					 *      } );
					 *    } );
					 */
					"sPrevious": "Previous"
				},
		
				/**
				 * This string is shown in preference to `zeroRecords` when the table is
				 * empty of data (regardless of filtering). Note that this is an optional
				 * parameter - if it is not given, the value of `zeroRecords` will be used
				 * instead (either the default or given value).
				 *  @type string
				 *  @default No data available in table
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.emptyTable
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "emptyTable": "No data available in table"
				 *        }
				 *      } );
				 *    } );
				 */
				"sEmptyTable": "No data available in table",
		
		
				/**
				 * This string gives information to the end user about the information
				 * that is current on display on the page. The following tokens can be
				 * used in the string and will be dynamically replaced as the table
				 * display updates. This tokens can be placed anywhere in the string, or
				 * removed as needed by the language requires:
				 *
				 * * `\_START\_` - Display index of the first record on the current page
				 * * `\_END\_` - Display index of the last record on the current page
				 * * `\_TOTAL\_` - Number of records in the table after filtering
				 * * `\_MAX\_` - Number of records in the table without filtering
				 * * `\_PAGE\_` - Current page number
				 * * `\_PAGES\_` - Total number of pages of data in the table
				 *
				 *  @type string
				 *  @default Showing _START_ to _END_ of _TOTAL_ entries
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.info
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "info": "Showing page _PAGE_ of _PAGES_"
				 *        }
				 *      } );
				 *    } );
				 */
				"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
		
		
				/**
				 * Display information string for when the table is empty. Typically the
				 * format of this string should match `info`.
				 *  @type string
				 *  @default Showing 0 to 0 of 0 entries
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.infoEmpty
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "infoEmpty": "No entries to show"
				 *        }
				 *      } );
				 *    } );
				 */
				"sInfoEmpty": "Showing 0 to 0 of 0 entries",
		
		
				/**
				 * When a user filters the information in a table, this string is appended
				 * to the information (`info`) to give an idea of how strong the filtering
				 * is. The variable _MAX_ is dynamically updated.
				 *  @type string
				 *  @default (filtered from _MAX_ total entries)
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.infoFiltered
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "infoFiltered": " - filtering from _MAX_ records"
				 *        }
				 *      } );
				 *    } );
				 */
				"sInfoFiltered": "(filtered from _MAX_ total entries)",
		
		
				/**
				 * If can be useful to append extra information to the info string at times,
				 * and this variable does exactly that. This information will be appended to
				 * the `info` (`infoEmpty` and `infoFiltered` in whatever combination they are
				 * being used) at all times.
				 *  @type string
				 *  @default <i>Empty string</i>
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.infoPostFix
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "infoPostFix": "All records shown are derived from real information."
				 *        }
				 *      } );
				 *    } );
				 */
				"sInfoPostFix": "",
		
		
				/**
				 * This decimal place operator is a little different from the other
				 * language options since DataTables doesn't output floating point
				 * numbers, so it won't ever use this for display of a number. Rather,
				 * what this parameter does is modify the sort methods of the table so
				 * that numbers which are in a format which has a character other than
				 * a period (`.`) as a decimal place will be sorted numerically.
				 *
				 * Note that numbers with different decimal places cannot be shown in
				 * the same table and still be sortable, the table must be consistent.
				 * However, multiple different tables on the page can use different
				 * decimal place characters.
				 *  @type string
				 *  @default 
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.decimal
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "decimal": ","
				 *          "thousands": "."
				 *        }
				 *      } );
				 *    } );
				 */
				"sDecimal": "",
		
		
				/**
				 * DataTables has a build in number formatter (`formatNumber`) which is
				 * used to format large numbers that are used in the table information.
				 * By default a comma is used, but this can be trivially changed to any
				 * character you wish with this parameter.
				 *  @type string
				 *  @default ,
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.thousands
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "thousands": "'"
				 *        }
				 *      } );
				 *    } );
				 */
				"sThousands": ",",
		
		
				/**
				 * Detail the action that will be taken when the drop down menu for the
				 * pagination length option is changed. The '_MENU_' variable is replaced
				 * with a default select list of 10, 25, 50 and 100, and can be replaced
				 * with a custom select box if required.
				 *  @type string
				 *  @default Show _MENU_ entries
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.lengthMenu
				 *
				 *  @example
				 *    // Language change only
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "lengthMenu": "Display _MENU_ records"
				 *        }
				 *      } );
				 *    } );
				 *
				 *  @example
				 *    // Language and options change
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "lengthMenu": 'Display <select>'+
				 *            '<option value="10">10</option>'+
				 *            '<option value="20">20</option>'+
				 *            '<option value="30">30</option>'+
				 *            '<option value="40">40</option>'+
				 *            '<option value="50">50</option>'+
				 *            '<option value="-1">All</option>'+
				 *            '</select> records'
				 *        }
				 *      } );
				 *    } );
				 */
				"sLengthMenu": "Show _MENU_ entries",
		
		
				/**
				 * When using Ajax sourced data and during the first draw when DataTables is
				 * gathering the data, this message is shown in an empty row in the table to
				 * indicate to the end user the the data is being loaded. Note that this
				 * parameter is not used when loading data by server-side processing, just
				 * Ajax sourced data with client-side processing.
				 *  @type string
				 *  @default Loading...
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.loadingRecords
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "loadingRecords": "Please wait - loading..."
				 *        }
				 *      } );
				 *    } );
				 */
				"sLoadingRecords": "Loading...",
		
		
				/**
				 * Text which is displayed when the table is processing a user action
				 * (usually a sort command or similar).
				 *  @type string
				 *  @default Processing...
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.processing
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "processing": "DataTables is currently busy"
				 *        }
				 *      } );
				 *    } );
				 */
				"sProcessing": "Processing...",
		
		
				/**
				 * Details the actions that will be taken when the user types into the
				 * filtering input text box. The variable "_INPUT_", if used in the string,
				 * is replaced with the HTML text box for the filtering input allowing
				 * control over where it appears in the string. If "_INPUT_" is not given
				 * then the input box is appended to the string automatically.
				 *  @type string
				 *  @default Search:
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.search
				 *
				 *  @example
				 *    // Input text box will be appended at the end automatically
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "search": "Filter records:"
				 *        }
				 *      } );
				 *    } );
				 *
				 *  @example
				 *    // Specify where the filter should appear
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "search": "Apply filter _INPUT_ to table"
				 *        }
				 *      } );
				 *    } );
				 */
				"sSearch": "Search:",
		
		
				/**
				 * Assign a `placeholder` attribute to the search `input` element
				 *  @type string
				 *  @default 
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.searchPlaceholder
				 */
				"sSearchPlaceholder": "",
		
		
				/**
				 * All of the language information can be stored in a file on the
				 * server-side, which DataTables will look up if this parameter is passed.
				 * It must store the URL of the language file, which is in a JSON format,
				 * and the object has the same properties as the oLanguage object in the
				 * initialiser object (i.e. the above parameters). Please refer to one of
				 * the example language files to see how this works in action.
				 *  @type string
				 *  @default <i>Empty string - i.e. disabled</i>
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.url
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "url": "http://www.sprymedia.co.uk/dataTables/lang.txt"
				 *        }
				 *      } );
				 *    } );
				 */
				"sUrl": "",
		
		
				/**
				 * Text shown inside the table records when the is no information to be
				 * displayed after filtering. `emptyTable` is shown when there is simply no
				 * information in the table at all (regardless of filtering).
				 *  @type string
				 *  @default No matching records found
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.zeroRecords
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "zeroRecords": "No records to display"
				 *        }
				 *      } );
				 *    } );
				 */
				"sZeroRecords": "No matching records found"
			},
		
		
			/**
			 * This parameter allows you to have define the global filtering state at
			 * initialisation time. As an object the `search` parameter must be
			 * defined, but all other parameters are optional. When `regex` is true,
			 * the search string will be treated as a regular expression, when false
			 * (default) it will be treated as a straight string. When `smart`
			 * DataTables will use it's smart filtering methods (to word match at
			 * any point in the data), when false this will not be done.
			 *  @namespace
			 *  @extends DataTable.models.oSearch
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.search
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "search": {"search": "Initial search"}
			 *      } );
			 *    } )
			 */
			"oSearch": $.extend( {}, DataTable.models.oSearch ),
		
		
			/**
			 * __Deprecated__ The functionality provided by this parameter has now been
			 * superseded by that provided through `ajax`, which should be used instead.
			 *
			 * By default DataTables will look for the property `data` (or `aaData` for
			 * compatibility with DataTables 1.9-) when obtaining data from an Ajax
			 * source or for server-side processing - this parameter allows that
			 * property to be changed. You can use Javascript dotted object notation to
			 * get a data source for multiple levels of nesting.
			 *  @type string
			 *  @default data
			 *
			 *  @dtopt Options
			 *  @dtopt Server-side
			 *  @name DataTable.defaults.ajaxDataProp
			 *
			 *  @deprecated 1.10. Please use `ajax` for this functionality now.
			 */
			"sAjaxDataProp": "data",
		
		
			/**
			 * __Deprecated__ The functionality provided by this parameter has now been
			 * superseded by that provided through `ajax`, which should be used instead.
			 *
			 * You can instruct DataTables to load data from an external
			 * source using this parameter (use aData if you want to pass data in you
			 * already have). Simply provide a url a JSON object can be obtained from.
			 *  @type string
			 *  @default null
			 *
			 *  @dtopt Options
			 *  @dtopt Server-side
			 *  @name DataTable.defaults.ajaxSource
			 *
			 *  @deprecated 1.10. Please use `ajax` for this functionality now.
			 */
			"sAjaxSource": null,
		
		
			/**
			 * This initialisation variable allows you to specify exactly where in the
			 * DOM you want DataTables to inject the various controls it adds to the page
			 * (for example you might want the pagination controls at the top of the
			 * table). DIV elements (with or without a custom class) can also be added to
			 * aid styling. The follow syntax is used:
			 *   <ul>
			 *     <li>The following options are allowed:
			 *       <ul>
			 *         <li>'l' - Length changing</li>
			 *         <li>'f' - Filtering input</li>
			 *         <li>'t' - The table!</li>
			 *         <li>'i' - Information</li>
			 *         <li>'p' - Pagination</li>
			 *         <li>'r' - pRocessing</li>
			 *       </ul>
			 *     </li>
			 *     <li>The following constants are allowed:
			 *       <ul>
			 *         <li>'H' - jQueryUI theme "header" classes ('fg-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix')</li>
			 *         <li>'F' - jQueryUI theme "footer" classes ('fg-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix')</li>
			 *       </ul>
			 *     </li>
			 *     <li>The following syntax is expected:
			 *       <ul>
			 *         <li>'&lt;' and '&gt;' - div elements</li>
			 *         <li>'&lt;"class" and '&gt;' - div with a class</li>
			 *         <li>'&lt;"#id" and '&gt;' - div with an ID</li>
			 *       </ul>
			 *     </li>
			 *     <li>Examples:
			 *       <ul>
			 *         <li>'&lt;"wrapper"flipt&gt;'</li>
			 *         <li>'&lt;lf&lt;t&gt;ip&gt;'</li>
			 *       </ul>
			 *     </li>
			 *   </ul>
			 *  @type string
			 *  @default lfrtip <i>(when `jQueryUI` is false)</i> <b>or</b>
			 *    <"H"lfr>t<"F"ip> <i>(when `jQueryUI` is true)</i>
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.dom
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "dom": '&lt;"top"i&gt;rt&lt;"bottom"flp&gt;&lt;"clear"&gt;'
			 *      } );
			 *    } );
			 */
			"sDom": "lfrtip",
		
		
			/**
			 * Search delay option. This will throttle full table searches that use the
			 * DataTables provided search input element (it does not effect calls to
			 * `dt-api search()`, providing a delay before the search is made.
			 *  @type integer
			 *  @default 0
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.searchDelay
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "searchDelay": 200
			 *      } );
			 *    } )
			 */
			"searchDelay": null,
		
		
			/**
			 * DataTables features four different built-in options for the buttons to
			 * display for pagination control:
			 *
			 * * `simple` - 'Previous' and 'Next' buttons only
			 * * 'simple_numbers` - 'Previous' and 'Next' buttons, plus page numbers
			 * * `full` - 'First', 'Previous', 'Next' and 'Last' buttons
			 * * `full_numbers` - 'First', 'Previous', 'Next' and 'Last' buttons, plus
			 *   page numbers
			 *  
			 * Further methods can be added using {@link DataTable.ext.oPagination}.
			 *  @type string
			 *  @default simple_numbers
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.pagingType
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "pagingType": "full_numbers"
			 *      } );
			 *    } )
			 */
			"sPaginationType": "simple_numbers",
		
		
			/**
			 * Enable horizontal scrolling. When a table is too wide to fit into a
			 * certain layout, or you have a large number of columns in the table, you
			 * can enable x-scrolling to show the table in a viewport, which can be
			 * scrolled. This property can be `true` which will allow the table to
			 * scroll horizontally when needed, or any CSS unit, or a number (in which
			 * case it will be treated as a pixel measurement). Setting as simply `true`
			 * is recommended.
			 *  @type boolean|string
			 *  @default <i>blank string - i.e. disabled</i>
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.scrollX
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "scrollX": true,
			 *        "scrollCollapse": true
			 *      } );
			 *    } );
			 */
			"sScrollX": "",
		
		
			/**
			 * This property can be used to force a DataTable to use more width than it
			 * might otherwise do when x-scrolling is enabled. For example if you have a
			 * table which requires to be well spaced, this parameter is useful for
			 * "over-sizing" the table, and thus forcing scrolling. This property can by
			 * any CSS unit, or a number (in which case it will be treated as a pixel
			 * measurement).
			 *  @type string
			 *  @default <i>blank string - i.e. disabled</i>
			 *
			 *  @dtopt Options
			 *  @name DataTable.defaults.scrollXInner
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "scrollX": "100%",
			 *        "scrollXInner": "110%"
			 *      } );
			 *    } );
			 */
			"sScrollXInner": "",
		
		
			/**
			 * Enable vertical scrolling. Vertical scrolling will constrain the DataTable
			 * to the given height, and enable scrolling for any data which overflows the
			 * current viewport. This can be used as an alternative to paging to display
			 * a lot of data in a small area (although paging and scrolling can both be
			 * enabled at the same time). This property can be any CSS unit, or a number
			 * (in which case it will be treated as a pixel measurement).
			 *  @type string
			 *  @default <i>blank string - i.e. disabled</i>
			 *
			 *  @dtopt Features
			 *  @name DataTable.defaults.scrollY
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "scrollY": "200px",
			 *        "paginate": false
			 *      } );
			 *    } );
			 */
			"sScrollY": "",
		
		
			/**
			 * __Deprecated__ The functionality provided by this parameter has now been
			 * superseded by that provided through `ajax`, which should be used instead.
			 *
			 * Set the HTTP method that is used to make the Ajax call for server-side
			 * processing or Ajax sourced data.
			 *  @type string
			 *  @default GET
			 *
			 *  @dtopt Options
			 *  @dtopt Server-side
			 *  @name DataTable.defaults.serverMethod
			 *
			 *  @deprecated 1.10. Please use `ajax` for this functionality now.
			 */
			"sServerMethod": "GET",
		
		
			/**
			 * DataTables makes use of renderers when displaying HTML elements for
			 * a table. These renderers can be added or modified by plug-ins to
			 * generate suitable mark-up for a site. For example the Bootstrap
			 * integration plug-in for DataTables uses a paging button renderer to
			 * display pagination buttons in the mark-up required by Bootstrap.
			 *
			 * For further information about the renderers available see
			 * DataTable.ext.renderer
			 *  @type string|object
			 *  @default null
			 *
			 *  @name DataTable.defaults.renderer
			 *
			 */
			"renderer": null,
		
		
			/**
			 * Set the data property name that DataTables should use to get a row's id
			 * to set as the `id` property in the node.
			 *  @type string
			 *  @default DT_RowId
			 *
			 *  @name DataTable.defaults.rowId
			 */
			"rowId": "DT_RowId"
		};
		
		_fnHungarianMap( DataTable.defaults );
		
		
		
		/*
		 * Developer note - See note in model.defaults.js about the use of Hungarian
		 * notation and camel case.
		 */
		
		/**
		 * Column options that can be given to DataTables at initialisation time.
		 *  @namespace
		 */
		DataTable.defaults.column = {
			/**
			 * Define which column(s) an order will occur on for this column. This
			 * allows a column's ordering to take multiple columns into account when
			 * doing a sort or use the data from a different column. For example first
			 * name / last name columns make sense to do a multi-column sort over the
			 * two columns.
			 *  @type array|int
			 *  @default null <i>Takes the value of the column index automatically</i>
			 *
			 *  @name DataTable.defaults.column.orderData
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          { "orderData": [ 0, 1 ], "targets": [ 0 ] },
			 *          { "orderData": [ 1, 0 ], "targets": [ 1 ] },
			 *          { "orderData": 2, "targets": [ 2 ] }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          { "orderData": [ 0, 1 ] },
			 *          { "orderData": [ 1, 0 ] },
			 *          { "orderData": 2 },
			 *          null,
			 *          null
			 *        ]
			 *      } );
			 *    } );
			 */
			"aDataSort": null,
			"iDataSort": -1,
		
		
			/**
			 * You can control the default ordering direction, and even alter the
			 * behaviour of the sort handler (i.e. only allow ascending ordering etc)
			 * using this parameter.
			 *  @type array
			 *  @default [ 'asc', 'desc' ]
			 *
			 *  @name DataTable.defaults.column.orderSequence
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          { "orderSequence": [ "asc" ], "targets": [ 1 ] },
			 *          { "orderSequence": [ "desc", "asc", "asc" ], "targets": [ 2 ] },
			 *          { "orderSequence": [ "desc" ], "targets": [ 3 ] }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          null,
			 *          { "orderSequence": [ "asc" ] },
			 *          { "orderSequence": [ "desc", "asc", "asc" ] },
			 *          { "orderSequence": [ "desc" ] },
			 *          null
			 *        ]
			 *      } );
			 *    } );
			 */
			"asSorting": [ 'asc', 'desc' ],
		
		
			/**
			 * Enable or disable filtering on the data in this column.
			 *  @type boolean
			 *  @default true
			 *
			 *  @name DataTable.defaults.column.searchable
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          { "searchable": false, "targets": [ 0 ] }
			 *        ] } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          { "searchable": false },
			 *          null,
			 *          null,
			 *          null,
			 *          null
			 *        ] } );
			 *    } );
			 */
			"bSearchable": true,
		
		
			/**
			 * Enable or disable ordering on this column.
			 *  @type boolean
			 *  @default true
			 *
			 *  @name DataTable.defaults.column.orderable
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          { "orderable": false, "targets": [ 0 ] }
			 *        ] } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          { "orderable": false },
			 *          null,
			 *          null,
			 *          null,
			 *          null
			 *        ] } );
			 *    } );
			 */
			"bSortable": true,
		
		
			/**
			 * Enable or disable the display of this column.
			 *  @type boolean
			 *  @default true
			 *
			 *  @name DataTable.defaults.column.visible
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          { "visible": false, "targets": [ 0 ] }
			 *        ] } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          { "visible": false },
			 *          null,
			 *          null,
			 *          null,
			 *          null
			 *        ] } );
			 *    } );
			 */
			"bVisible": true,
		
		
			/**
			 * Developer definable function that is called whenever a cell is created (Ajax source,
			 * etc) or processed for input (DOM source). This can be used as a compliment to mRender
			 * allowing you to modify the DOM element (add background colour for example) when the
			 * element is available.
			 *  @type function
			 *  @param {element} td The TD node that has been created
			 *  @param {*} cellData The Data for the cell
			 *  @param {array|object} rowData The data for the whole row
			 *  @param {int} row The row index for the aoData data store
			 *  @param {int} col The column index for aoColumns
			 *
			 *  @name DataTable.defaults.column.createdCell
			 *  @dtopt Columns
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [ {
			 *          "targets": [3],
			 *          "createdCell": function (td, cellData, rowData, row, col) {
			 *            if ( cellData == "1.7" ) {
			 *              $(td).css('color', 'blue')
			 *            }
			 *          }
			 *        } ]
			 *      });
			 *    } );
			 */
			"fnCreatedCell": null,
		
		
			/**
			 * This parameter has been replaced by `data` in DataTables to ensure naming
			 * consistency. `dataProp` can still be used, as there is backwards
			 * compatibility in DataTables for this option, but it is strongly
			 * recommended that you use `data` in preference to `dataProp`.
			 *  @name DataTable.defaults.column.dataProp
			 */
		
		
			/**
			 * This property can be used to read data from any data source property,
			 * including deeply nested objects / properties. `data` can be given in a
			 * number of different ways which effect its behaviour:
			 *
			 * * `integer` - treated as an array index for the data source. This is the
			 *   default that DataTables uses (incrementally increased for each column).
			 * * `string` - read an object property from the data source. There are
			 *   three 'special' options that can be used in the string to alter how
			 *   DataTables reads the data from the source object:
			 *    * `.` - Dotted Javascript notation. Just as you use a `.` in
			 *      Javascript to read from nested objects, so to can the options
			 *      specified in `data`. For example: `browser.version` or
			 *      `browser.name`. If your object parameter name contains a period, use
			 *      `\\` to escape it - i.e. `first\\.name`.
			 *    * `[]` - Array notation. DataTables can automatically combine data
			 *      from and array source, joining the data with the characters provided
			 *      between the two brackets. For example: `name[, ]` would provide a
			 *      comma-space separated list from the source array. If no characters
			 *      are provided between the brackets, the original array source is
			 *      returned.
			 *    * `()` - Function notation. Adding `()` to the end of a parameter will
			 *      execute a function of the name given. For example: `browser()` for a
			 *      simple function on the data source, `browser.version()` for a
			 *      function in a nested property or even `browser().version` to get an
			 *      object property if the function called returns an object. Note that
			 *      function notation is recommended for use in `render` rather than
			 *      `data` as it is much simpler to use as a renderer.
			 * * `null` - use the original data source for the row rather than plucking
			 *   data directly from it. This action has effects on two other
			 *   initialisation options:
			 *    * `defaultContent` - When null is given as the `data` option and
			 *      `defaultContent` is specified for the column, the value defined by
			 *      `defaultContent` will be used for the cell.
			 *    * `render` - When null is used for the `data` option and the `render`
			 *      option is specified for the column, the whole data source for the
			 *      row is used for the renderer.
			 * * `function` - the function given will be executed whenever DataTables
			 *   needs to set or get the data for a cell in the column. The function
			 *   takes three parameters:
			 *    * Parameters:
			 *      * `{array|object}` The data source for the row
			 *      * `{string}` The type call data requested - this will be 'set' when
			 *        setting data or 'filter', 'display', 'type', 'sort' or undefined
			 *        when gathering data. Note that when `undefined` is given for the
			 *        type DataTables expects to get the raw data for the object back<
			 *      * `{*}` Data to set when the second parameter is 'set'.
			 *    * Return:
			 *      * The return value from the function is not required when 'set' is
			 *        the type of call, but otherwise the return is what will be used
			 *        for the data requested.
			 *
			 * Note that `data` is a getter and setter option. If you just require
			 * formatting of data for output, you will likely want to use `render` which
			 * is simply a getter and thus simpler to use.
			 *
			 * Note that prior to DataTables 1.9.2 `data` was called `mDataProp`. The
			 * name change reflects the flexibility of this property and is consistent
			 * with the naming of mRender. If 'mDataProp' is given, then it will still
			 * be used by DataTables, as it automatically maps the old name to the new
			 * if required.
			 *
			 *  @type string|int|function|null
			 *  @default null <i>Use automatically calculated column index</i>
			 *
			 *  @name DataTable.defaults.column.data
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Read table data from objects
			 *    // JSON structure for each row:
			 *    //   {
			 *    //      "engine": {value},
			 *    //      "browser": {value},
			 *    //      "platform": {value},
			 *    //      "version": {value},
			 *    //      "grade": {value}
			 *    //   }
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "ajaxSource": "sources/objects.txt",
			 *        "columns": [
			 *          { "data": "engine" },
			 *          { "data": "browser" },
			 *          { "data": "platform" },
			 *          { "data": "version" },
			 *          { "data": "grade" }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Read information from deeply nested objects
			 *    // JSON structure for each row:
			 *    //   {
			 *    //      "engine": {value},
			 *    //      "browser": {value},
			 *    //      "platform": {
			 *    //         "inner": {value}
			 *    //      },
			 *    //      "details": [
			 *    //         {value}, {value}
			 *    //      ]
			 *    //   }
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "ajaxSource": "sources/deep.txt",
			 *        "columns": [
			 *          { "data": "engine" },
			 *          { "data": "browser" },
			 *          { "data": "platform.inner" },
			 *          { "data": "platform.details.0" },
			 *          { "data": "platform.details.1" }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using `data` as a function to provide different information for
			 *    // sorting, filtering and display. In this case, currency (price)
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [ {
			 *          "targets": [ 0 ],
			 *          "data": function ( source, type, val ) {
			 *            if (type === 'set') {
			 *              source.price = val;
			 *              // Store the computed dislay and filter values for efficiency
			 *              source.price_display = val=="" ? "" : "$"+numberFormat(val);
			 *              source.price_filter  = val=="" ? "" : "$"+numberFormat(val)+" "+val;
			 *              return;
			 *            }
			 *            else if (type === 'display') {
			 *              return source.price_display;
			 *            }
			 *            else if (type === 'filter') {
			 *              return source.price_filter;
			 *            }
			 *            // 'sort', 'type' and undefined all just use the integer
			 *            return source.price;
			 *          }
			 *        } ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using default content
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [ {
			 *          "targets": [ 0 ],
			 *          "data": null,
			 *          "defaultContent": "Click to edit"
			 *        } ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using array notation - outputting a list from an array
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [ {
			 *          "targets": [ 0 ],
			 *          "data": "name[, ]"
			 *        } ]
			 *      } );
			 *    } );
			 *
			 */
			"mData": null,
		
		
			/**
			 * This property is the rendering partner to `data` and it is suggested that
			 * when you want to manipulate data for display (including filtering,
			 * sorting etc) without altering the underlying data for the table, use this
			 * property. `render` can be considered to be the the read only companion to
			 * `data` which is read / write (then as such more complex). Like `data`
			 * this option can be given in a number of different ways to effect its
			 * behaviour:
			 *
			 * * `integer` - treated as an array index for the data source. This is the
			 *   default that DataTables uses (incrementally increased for each column).
			 * * `string` - read an object property from the data source. There are
			 *   three 'special' options that can be used in the string to alter how
			 *   DataTables reads the data from the source object:
			 *    * `.` - Dotted Javascript notation. Just as you use a `.` in
			 *      Javascript to read from nested objects, so to can the options
			 *      specified in `data`. For example: `browser.version` or
			 *      `browser.name`. If your object parameter name contains a period, use
			 *      `\\` to escape it - i.e. `first\\.name`.
			 *    * `[]` - Array notation. DataTables can automatically combine data
			 *      from and array source, joining the data with the characters provided
			 *      between the two brackets. For example: `name[, ]` would provide a
			 *      comma-space separated list from the source array. If no characters
			 *      are provided between the brackets, the original array source is
			 *      returned.
			 *    * `()` - Function notation. Adding `()` to the end of a parameter will
			 *      execute a function of the name given. For example: `browser()` for a
			 *      simple function on the data source, `browser.version()` for a
			 *      function in a nested property or even `browser().version` to get an
			 *      object property if the function called returns an object.
			 * * `object` - use different data for the different data types requested by
			 *   DataTables ('filter', 'display', 'type' or 'sort'). The property names
			 *   of the object is the data type the property refers to and the value can
			 *   defined using an integer, string or function using the same rules as
			 *   `render` normally does. Note that an `_` option _must_ be specified.
			 *   This is the default value to use if you haven't specified a value for
			 *   the data type requested by DataTables.
			 * * `function` - the function given will be executed whenever DataTables
			 *   needs to set or get the data for a cell in the column. The function
			 *   takes three parameters:
			 *    * Parameters:
			 *      * {array|object} The data source for the row (based on `data`)
			 *      * {string} The type call data requested - this will be 'filter',
			 *        'display', 'type' or 'sort'.
			 *      * {array|object} The full data source for the row (not based on
			 *        `data`)
			 *    * Return:
			 *      * The return value from the function is what will be used for the
			 *        data requested.
			 *
			 *  @type string|int|function|object|null
			 *  @default null Use the data source value.
			 *
			 *  @name DataTable.defaults.column.render
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Create a comma separated list from an array of objects
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "ajaxSource": "sources/deep.txt",
			 *        "columns": [
			 *          { "data": "engine" },
			 *          { "data": "browser" },
			 *          {
			 *            "data": "platform",
			 *            "render": "[, ].name"
			 *          }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Execute a function to obtain data
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [ {
			 *          "targets": [ 0 ],
			 *          "data": null, // Use the full data source object for the renderer's source
			 *          "render": "browserName()"
			 *        } ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // As an object, extracting different data for the different types
			 *    // This would be used with a data source such as:
			 *    //   { "phone": 5552368, "phone_filter": "5552368 555-2368", "phone_display": "555-2368" }
			 *    // Here the `phone` integer is used for sorting and type detection, while `phone_filter`
			 *    // (which has both forms) is used for filtering for if a user inputs either format, while
			 *    // the formatted phone number is the one that is shown in the table.
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [ {
			 *          "targets": [ 0 ],
			 *          "data": null, // Use the full data source object for the renderer's source
			 *          "render": {
			 *            "_": "phone",
			 *            "filter": "phone_filter",
			 *            "display": "phone_display"
			 *          }
			 *        } ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Use as a function to create a link from the data source
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [ {
			 *          "targets": [ 0 ],
			 *          "data": "download_link",
			 *          "render": function ( data, type, full ) {
			 *            return '<a href="'+data+'">Download</a>';
			 *          }
			 *        } ]
			 *      } );
			 *    } );
			 */
			"mRender": null,
		
		
			/**
			 * Change the cell type created for the column - either TD cells or TH cells. This
			 * can be useful as TH cells have semantic meaning in the table body, allowing them
			 * to act as a header for a row (you may wish to add scope='row' to the TH elements).
			 *  @type string
			 *  @default td
			 *
			 *  @name DataTable.defaults.column.cellType
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Make the first column use TH cells
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [ {
			 *          "targets": [ 0 ],
			 *          "cellType": "th"
			 *        } ]
			 *      } );
			 *    } );
			 */
			"sCellType": "td",
		
		
			/**
			 * Class to give to each cell in this column.
			 *  @type string
			 *  @default <i>Empty string</i>
			 *
			 *  @name DataTable.defaults.column.class
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          { "class": "my_class", "targets": [ 0 ] }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          { "class": "my_class" },
			 *          null,
			 *          null,
			 *          null,
			 *          null
			 *        ]
			 *      } );
			 *    } );
			 */
			"sClass": "",
		
			/**
			 * When DataTables calculates the column widths to assign to each column,
			 * it finds the longest string in each column and then constructs a
			 * temporary table and reads the widths from that. The problem with this
			 * is that "mmm" is much wider then "iiii", but the latter is a longer
			 * string - thus the calculation can go wrong (doing it properly and putting
			 * it into an DOM object and measuring that is horribly(!) slow). Thus as
			 * a "work around" we provide this option. It will append its value to the
			 * text that is found to be the longest string for the column - i.e. padding.
			 * Generally you shouldn't need this!
			 *  @type string
			 *  @default <i>Empty string<i>
			 *
			 *  @name DataTable.defaults.column.contentPadding
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          null,
			 *          null,
			 *          null,
			 *          {
			 *            "contentPadding": "mmm"
			 *          }
			 *        ]
			 *      } );
			 *    } );
			 */
			"sContentPadding": "",
		
		
			/**
			 * Allows a default value to be given for a column's data, and will be used
			 * whenever a null data source is encountered (this can be because `data`
			 * is set to null, or because the data source itself is null).
			 *  @type string
			 *  @default null
			 *
			 *  @name DataTable.defaults.column.defaultContent
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          {
			 *            "data": null,
			 *            "defaultContent": "Edit",
			 *            "targets": [ -1 ]
			 *          }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          null,
			 *          null,
			 *          null,
			 *          {
			 *            "data": null,
			 *            "defaultContent": "Edit"
			 *          }
			 *        ]
			 *      } );
			 *    } );
			 */
			"sDefaultContent": null,
		
		
			/**
			 * This parameter is only used in DataTables' server-side processing. It can
			 * be exceptionally useful to know what columns are being displayed on the
			 * client side, and to map these to database fields. When defined, the names
			 * also allow DataTables to reorder information from the server if it comes
			 * back in an unexpected order (i.e. if you switch your columns around on the
			 * client-side, your server-side code does not also need updating).
			 *  @type string
			 *  @default <i>Empty string</i>
			 *
			 *  @name DataTable.defaults.column.name
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          { "name": "engine", "targets": [ 0 ] },
			 *          { "name": "browser", "targets": [ 1 ] },
			 *          { "name": "platform", "targets": [ 2 ] },
			 *          { "name": "version", "targets": [ 3 ] },
			 *          { "name": "grade", "targets": [ 4 ] }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          { "name": "engine" },
			 *          { "name": "browser" },
			 *          { "name": "platform" },
			 *          { "name": "version" },
			 *          { "name": "grade" }
			 *        ]
			 *      } );
			 *    } );
			 */
			"sName": "",
		
		
			/**
			 * Defines a data source type for the ordering which can be used to read
			 * real-time information from the table (updating the internally cached
			 * version) prior to ordering. This allows ordering to occur on user
			 * editable elements such as form inputs.
			 *  @type string
			 *  @default std
			 *
			 *  @name DataTable.defaults.column.orderDataType
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          { "orderDataType": "dom-text", "targets": [ 2, 3 ] },
			 *          { "type": "numeric", "targets": [ 3 ] },
			 *          { "orderDataType": "dom-select", "targets": [ 4 ] },
			 *          { "orderDataType": "dom-checkbox", "targets": [ 5 ] }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          null,
			 *          null,
			 *          { "orderDataType": "dom-text" },
			 *          { "orderDataType": "dom-text", "type": "numeric" },
			 *          { "orderDataType": "dom-select" },
			 *          { "orderDataType": "dom-checkbox" }
			 *        ]
			 *      } );
			 *    } );
			 */
			"sSortDataType": "std",
		
		
			/**
			 * The title of this column.
			 *  @type string
			 *  @default null <i>Derived from the 'TH' value for this column in the
			 *    original HTML table.</i>
			 *
			 *  @name DataTable.defaults.column.title
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          { "title": "My column title", "targets": [ 0 ] }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          { "title": "My column title" },
			 *          null,
			 *          null,
			 *          null,
			 *          null
			 *        ]
			 *      } );
			 *    } );
			 */
			"sTitle": null,
		
		
			/**
			 * The type allows you to specify how the data for this column will be
			 * ordered. Four types (string, numeric, date and html (which will strip
			 * HTML tags before ordering)) are currently available. Note that only date
			 * formats understood by Javascript's Date() object will be accepted as type
			 * date. For example: "Mar 26, 2008 5:03 PM". May take the values: 'string',
			 * 'numeric', 'date' or 'html' (by default). Further types can be adding
			 * through plug-ins.
			 *  @type string
			 *  @default null <i>Auto-detected from raw data</i>
			 *
			 *  @name DataTable.defaults.column.type
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          { "type": "html", "targets": [ 0 ] }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          { "type": "html" },
			 *          null,
			 *          null,
			 *          null,
			 *          null
			 *        ]
			 *      } );
			 *    } );
			 */
			"sType": null,
		
		
			/**
			 * Defining the width of the column, this parameter may take any CSS value
			 * (3em, 20px etc). DataTables applies 'smart' widths to columns which have not
			 * been given a specific width through this interface ensuring that the table
			 * remains readable.
			 *  @type string
			 *  @default null <i>Automatic</i>
			 *
			 *  @name DataTable.defaults.column.width
			 *  @dtopt Columns
			 *
			 *  @example
			 *    // Using `columnDefs`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columnDefs": [
			 *          { "width": "20%", "targets": [ 0 ] }
			 *        ]
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Using `columns`
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "columns": [
			 *          { "width": "20%" },
			 *          null,
			 *          null,
			 *          null,
			 *          null
			 *        ]
			 *      } );
			 *    } );
			 */
			"sWidth": null
		};
		
		_fnHungarianMap( DataTable.defaults.column );
		
		
		
		/**
		 * DataTables settings object - this holds all the information needed for a
		 * given table, including configuration, data and current application of the
		 * table options. DataTables does not have a single instance for each DataTable
		 * with the settings attached to that instance, but rather instances of the
		 * DataTable "class" are created on-the-fly as needed (typically by a
		 * $().dataTable() call) and the settings object is then applied to that
		 * instance.
		 *
		 * Note that this object is related to {@link DataTable.defaults} but this
		 * one is the internal data store for DataTables's cache of columns. It should
		 * NOT be manipulated outside of DataTables. Any configuration should be done
		 * through the initialisation options.
		 *  @namespace
		 *  @todo Really should attach the settings object to individual instances so we
		 *    don't need to create new instances on each $().dataTable() call (if the
		 *    table already exists). It would also save passing oSettings around and
		 *    into every single function. However, this is a very significant
		 *    architecture change for DataTables and will almost certainly break
		 *    backwards compatibility with older installations. This is something that
		 *    will be done in 2.0.
		 */
		DataTable.models.oSettings = {
			/**
			 * Primary features of DataTables and their enablement state.
			 *  @namespace
			 */
			"oFeatures": {
		
				/**
				 * Flag to say if DataTables should automatically try to calculate the
				 * optimum table and columns widths (true) or not (false).
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bAutoWidth": null,
		
				/**
				 * Delay the creation of TR and TD elements until they are actually
				 * needed by a driven page draw. This can give a significant speed
				 * increase for Ajax source and Javascript source data, but makes no
				 * difference at all fro DOM and server-side processing tables.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bDeferRender": null,
		
				/**
				 * Enable filtering on the table or not. Note that if this is disabled
				 * then there is no filtering at all on the table, including fnFilter.
				 * To just remove the filtering input use sDom and remove the 'f' option.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bFilter": null,
		
				/**
				 * Table information element (the 'Showing x of y records' div) enable
				 * flag.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bInfo": null,
		
				/**
				 * Present a user control allowing the end user to change the page size
				 * when pagination is enabled.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bLengthChange": null,
		
				/**
				 * Pagination enabled or not. Note that if this is disabled then length
				 * changing must also be disabled.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bPaginate": null,
		
				/**
				 * Processing indicator enable flag whenever DataTables is enacting a
				 * user request - typically an Ajax request for server-side processing.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bProcessing": null,
		
				/**
				 * Server-side processing enabled flag - when enabled DataTables will
				 * get all data from the server for every draw - there is no filtering,
				 * sorting or paging done on the client-side.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bServerSide": null,
		
				/**
				 * Sorting enablement flag.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bSort": null,
		
				/**
				 * Multi-column sorting
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bSortMulti": null,
		
				/**
				 * Apply a class to the columns which are being sorted to provide a
				 * visual highlight or not. This can slow things down when enabled since
				 * there is a lot of DOM interaction.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bSortClasses": null,
		
				/**
				 * State saving enablement flag.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bStateSave": null
			},
		
		
			/**
			 * Scrolling settings for a table.
			 *  @namespace
			 */
			"oScroll": {
				/**
				 * When the table is shorter in height than sScrollY, collapse the
				 * table container down to the height of the table (when true).
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type boolean
				 */
				"bCollapse": null,
		
				/**
				 * Width of the scrollbar for the web-browser's platform. Calculated
				 * during table initialisation.
				 *  @type int
				 *  @default 0
				 */
				"iBarWidth": 0,
		
				/**
				 * Viewport width for horizontal scrolling. Horizontal scrolling is
				 * disabled if an empty string.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type string
				 */
				"sX": null,
		
				/**
				 * Width to expand the table to when using x-scrolling. Typically you
				 * should not need to use this.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type string
				 *  @deprecated
				 */
				"sXInner": null,
		
				/**
				 * Viewport height for vertical scrolling. Vertical scrolling is disabled
				 * if an empty string.
				 * Note that this parameter will be set by the initialisation routine. To
				 * set a default use {@link DataTable.defaults}.
				 *  @type string
				 */
				"sY": null
			},
		
			/**
			 * Language information for the table.
			 *  @namespace
			 *  @extends DataTable.defaults.oLanguage
			 */
			"oLanguage": {
				/**
				 * Information callback function. See
				 * {@link DataTable.defaults.fnInfoCallback}
				 *  @type function
				 *  @default null
				 */
				"fnInfoCallback": null
			},
		
			/**
			 * Browser support parameters
			 *  @namespace
			 */
			"oBrowser": {
				/**
				 * Indicate if the browser incorrectly calculates width:100% inside a
				 * scrolling element (IE6/7)
				 *  @type boolean
				 *  @default false
				 */
				"bScrollOversize": false,
		
				/**
				 * Determine if the vertical scrollbar is on the right or left of the
				 * scrolling container - needed for rtl language layout, although not
				 * all browsers move the scrollbar (Safari).
				 *  @type boolean
				 *  @default false
				 */
				"bScrollbarLeft": false,
		
				/**
				 * Flag for if `getBoundingClientRect` is fully supported or not
				 *  @type boolean
				 *  @default false
				 */
				"bBounding": false,
		
				/**
				 * Browser scrollbar width
				 *  @type integer
				 *  @default 0
				 */
				"barWidth": 0
			},
		
		
			"ajax": null,
		
		
			/**
			 * Array referencing the nodes which are used for the features. The
			 * parameters of this object match what is allowed by sDom - i.e.
			 *   <ul>
			 *     <li>'l' - Length changing</li>
			 *     <li>'f' - Filtering input</li>
			 *     <li>'t' - The table!</li>
			 *     <li>'i' - Information</li>
			 *     <li>'p' - Pagination</li>
			 *     <li>'r' - pRocessing</li>
			 *   </ul>
			 *  @type array
			 *  @default []
			 */
			"aanFeatures": [],
		
			/**
			 * Store data information - see {@link DataTable.models.oRow} for detailed
			 * information.
			 *  @type array
			 *  @default []
			 */
			"aoData": [],
		
			/**
			 * Array of indexes which are in the current display (after filtering etc)
			 *  @type array
			 *  @default []
			 */
			"aiDisplay": [],
		
			/**
			 * Array of indexes for display - no filtering
			 *  @type array
			 *  @default []
			 */
			"aiDisplayMaster": [],
		
			/**
			 * Map of row ids to data indexes
			 *  @type object
			 *  @default {}
			 */
			"aIds": {},
		
			/**
			 * Store information about each column that is in use
			 *  @type array
			 *  @default []
			 */
			"aoColumns": [],
		
			/**
			 * Store information about the table's header
			 *  @type array
			 *  @default []
			 */
			"aoHeader": [],
		
			/**
			 * Store information about the table's footer
			 *  @type array
			 *  @default []
			 */
			"aoFooter": [],
		
			/**
			 * Store the applied global search information in case we want to force a
			 * research or compare the old search to a new one.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @namespace
			 *  @extends DataTable.models.oSearch
			 */
			"oPreviousSearch": {},
		
			/**
			 * Store the applied search for each column - see
			 * {@link DataTable.models.oSearch} for the format that is used for the
			 * filtering information for each column.
			 *  @type array
			 *  @default []
			 */
			"aoPreSearchCols": [],
		
			/**
			 * Sorting that is applied to the table. Note that the inner arrays are
			 * used in the following manner:
			 * <ul>
			 *   <li>Index 0 - column number</li>
			 *   <li>Index 1 - current sorting direction</li>
			 * </ul>
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type array
			 *  @todo These inner arrays should really be objects
			 */
			"aaSorting": null,
		
			/**
			 * Sorting that is always applied to the table (i.e. prefixed in front of
			 * aaSorting).
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type array
			 *  @default []
			 */
			"aaSortingFixed": [],
		
			/**
			 * Classes to use for the striping of a table.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type array
			 *  @default []
			 */
			"asStripeClasses": null,
		
			/**
			 * If restoring a table - we should restore its striping classes as well
			 *  @type array
			 *  @default []
			 */
			"asDestroyStripes": [],
		
			/**
			 * If restoring a table - we should restore its width
			 *  @type int
			 *  @default 0
			 */
			"sDestroyWidth": 0,
		
			/**
			 * Callback functions array for every time a row is inserted (i.e. on a draw).
			 *  @type array
			 *  @default []
			 */
			"aoRowCallback": [],
		
			/**
			 * Callback functions for the header on each draw.
			 *  @type array
			 *  @default []
			 */
			"aoHeaderCallback": [],
		
			/**
			 * Callback function for the footer on each draw.
			 *  @type array
			 *  @default []
			 */
			"aoFooterCallback": [],
		
			/**
			 * Array of callback functions for draw callback functions
			 *  @type array
			 *  @default []
			 */
			"aoDrawCallback": [],
		
			/**
			 * Array of callback functions for row created function
			 *  @type array
			 *  @default []
			 */
			"aoRowCreatedCallback": [],
		
			/**
			 * Callback functions for just before the table is redrawn. A return of
			 * false will be used to cancel the draw.
			 *  @type array
			 *  @default []
			 */
			"aoPreDrawCallback": [],
		
			/**
			 * Callback functions for when the table has been initialised.
			 *  @type array
			 *  @default []
			 */
			"aoInitComplete": [],
		
		
			/**
			 * Callbacks for modifying the settings to be stored for state saving, prior to
			 * saving state.
			 *  @type array
			 *  @default []
			 */
			"aoStateSaveParams": [],
		
			/**
			 * Callbacks for modifying the settings that have been stored for state saving
			 * prior to using the stored values to restore the state.
			 *  @type array
			 *  @default []
			 */
			"aoStateLoadParams": [],
		
			/**
			 * Callbacks for operating on the settings object once the saved state has been
			 * loaded
			 *  @type array
			 *  @default []
			 */
			"aoStateLoaded": [],
		
			/**
			 * Cache the table ID for quick access
			 *  @type string
			 *  @default <i>Empty string</i>
			 */
			"sTableId": "",
		
			/**
			 * The TABLE node for the main table
			 *  @type node
			 *  @default null
			 */
			"nTable": null,
		
			/**
			 * Permanent ref to the thead element
			 *  @type node
			 *  @default null
			 */
			"nTHead": null,
		
			/**
			 * Permanent ref to the tfoot element - if it exists
			 *  @type node
			 *  @default null
			 */
			"nTFoot": null,
		
			/**
			 * Permanent ref to the tbody element
			 *  @type node
			 *  @default null
			 */
			"nTBody": null,
		
			/**
			 * Cache the wrapper node (contains all DataTables controlled elements)
			 *  @type node
			 *  @default null
			 */
			"nTableWrapper": null,
		
			/**
			 * Indicate if when using server-side processing the loading of data
			 * should be deferred until the second draw.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 *  @default false
			 */
			"bDeferLoading": false,
		
			/**
			 * Indicate if all required information has been read in
			 *  @type boolean
			 *  @default false
			 */
			"bInitialised": false,
		
			/**
			 * Information about open rows. Each object in the array has the parameters
			 * 'nTr' and 'nParent'
			 *  @type array
			 *  @default []
			 */
			"aoOpenRows": [],
		
			/**
			 * Dictate the positioning of DataTables' control elements - see
			 * {@link DataTable.model.oInit.sDom}.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type string
			 *  @default null
			 */
			"sDom": null,
		
			/**
			 * Search delay (in mS)
			 *  @type integer
			 *  @default null
			 */
			"searchDelay": null,
		
			/**
			 * Which type of pagination should be used.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type string
			 *  @default two_button
			 */
			"sPaginationType": "two_button",
		
			/**
			 * The state duration (for `stateSave`) in seconds.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type int
			 *  @default 0
			 */
			"iStateDuration": 0,
		
			/**
			 * Array of callback functions for state saving. Each array element is an
			 * object with the following parameters:
			 *   <ul>
			 *     <li>function:fn - function to call. Takes two parameters, oSettings
			 *       and the JSON string to save that has been thus far created. Returns
			 *       a JSON string to be inserted into a json object
			 *       (i.e. '"param": [ 0, 1, 2]')</li>
			 *     <li>string:sName - name of callback</li>
			 *   </ul>
			 *  @type array
			 *  @default []
			 */
			"aoStateSave": [],
		
			/**
			 * Array of callback functions for state loading. Each array element is an
			 * object with the following parameters:
			 *   <ul>
			 *     <li>function:fn - function to call. Takes two parameters, oSettings
			 *       and the object stored. May return false to cancel state loading</li>
			 *     <li>string:sName - name of callback</li>
			 *   </ul>
			 *  @type array
			 *  @default []
			 */
			"aoStateLoad": [],
		
			/**
			 * State that was saved. Useful for back reference
			 *  @type object
			 *  @default null
			 */
			"oSavedState": null,
		
			/**
			 * State that was loaded. Useful for back reference
			 *  @type object
			 *  @default null
			 */
			"oLoadedState": null,
		
			/**
			 * Source url for AJAX data for the table.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type string
			 *  @default null
			 */
			"sAjaxSource": null,
		
			/**
			 * Property from a given object from which to read the table data from. This
			 * can be an empty string (when not server-side processing), in which case
			 * it is  assumed an an array is given directly.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type string
			 */
			"sAjaxDataProp": null,
		
			/**
			 * Note if draw should be blocked while getting data
			 *  @type boolean
			 *  @default true
			 */
			"bAjaxDataGet": true,
		
			/**
			 * The last jQuery XHR object that was used for server-side data gathering.
			 * This can be used for working with the XHR information in one of the
			 * callbacks
			 *  @type object
			 *  @default null
			 */
			"jqXHR": null,
		
			/**
			 * JSON returned from the server in the last Ajax request
			 *  @type object
			 *  @default undefined
			 */
			"json": undefined,
		
			/**
			 * Data submitted as part of the last Ajax request
			 *  @type object
			 *  @default undefined
			 */
			"oAjaxData": undefined,
		
			/**
			 * Function to get the server-side data.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type function
			 */
			"fnServerData": null,
		
			/**
			 * Functions which are called prior to sending an Ajax request so extra
			 * parameters can easily be sent to the server
			 *  @type array
			 *  @default []
			 */
			"aoServerParams": [],
		
			/**
			 * Send the XHR HTTP method - GET or POST (could be PUT or DELETE if
			 * required).
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type string
			 */
			"sServerMethod": null,
		
			/**
			 * Format numbers for display.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type function
			 */
			"fnFormatNumber": null,
		
			/**
			 * List of options that can be used for the user selectable length menu.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type array
			 *  @default []
			 */
			"aLengthMenu": null,
		
			/**
			 * Counter for the draws that the table does. Also used as a tracker for
			 * server-side processing
			 *  @type int
			 *  @default 0
			 */
			"iDraw": 0,
		
			/**
			 * Indicate if a redraw is being done - useful for Ajax
			 *  @type boolean
			 *  @default false
			 */
			"bDrawing": false,
		
			/**
			 * Draw index (iDraw) of the last error when parsing the returned data
			 *  @type int
			 *  @default -1
			 */
			"iDrawError": -1,
		
			/**
			 * Paging display length
			 *  @type int
			 *  @default 10
			 */
			"_iDisplayLength": 10,
		
			/**
			 * Paging start point - aiDisplay index
			 *  @type int
			 *  @default 0
			 */
			"_iDisplayStart": 0,
		
			/**
			 * Server-side processing - number of records in the result set
			 * (i.e. before filtering), Use fnRecordsTotal rather than
			 * this property to get the value of the number of records, regardless of
			 * the server-side processing setting.
			 *  @type int
			 *  @default 0
			 *  @private
			 */
			"_iRecordsTotal": 0,
		
			/**
			 * Server-side processing - number of records in the current display set
			 * (i.e. after filtering). Use fnRecordsDisplay rather than
			 * this property to get the value of the number of records, regardless of
			 * the server-side processing setting.
			 *  @type boolean
			 *  @default 0
			 *  @private
			 */
			"_iRecordsDisplay": 0,
		
			/**
			 * Flag to indicate if jQuery UI marking and classes should be used.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bJUI": null,
		
			/**
			 * The classes to use for the table
			 *  @type object
			 *  @default {}
			 */
			"oClasses": {},
		
			/**
			 * Flag attached to the settings object so you can check in the draw
			 * callback if filtering has been done in the draw. Deprecated in favour of
			 * events.
			 *  @type boolean
			 *  @default false
			 *  @deprecated
			 */
			"bFiltered": false,
		
			/**
			 * Flag attached to the settings object so you can check in the draw
			 * callback if sorting has been done in the draw. Deprecated in favour of
			 * events.
			 *  @type boolean
			 *  @default false
			 *  @deprecated
			 */
			"bSorted": false,
		
			/**
			 * Indicate that if multiple rows are in the header and there is more than
			 * one unique cell per column, if the top one (true) or bottom one (false)
			 * should be used for sorting / title by DataTables.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bSortCellsTop": null,
		
			/**
			 * Initialisation object that is used for the table
			 *  @type object
			 *  @default null
			 */
			"oInit": null,
		
			/**
			 * Destroy callback functions - for plug-ins to attach themselves to the
			 * destroy so they can clean up markup and events.
			 *  @type array
			 *  @default []
			 */
			"aoDestroyCallback": [],
		
		
			/**
			 * Get the number of records in the current record set, before filtering
			 *  @type function
			 */
			"fnRecordsTotal": function ()
			{
				return _fnDataSource( this ) == 'ssp' ?
					this._iRecordsTotal * 1 :
					this.aiDisplayMaster.length;
			},
		
			/**
			 * Get the number of records in the current record set, after filtering
			 *  @type function
			 */
			"fnRecordsDisplay": function ()
			{
				return _fnDataSource( this ) == 'ssp' ?
					this._iRecordsDisplay * 1 :
					this.aiDisplay.length;
			},
		
			/**
			 * Get the display end point - aiDisplay index
			 *  @type function
			 */
			"fnDisplayEnd": function ()
			{
				var
					len      = this._iDisplayLength,
					start    = this._iDisplayStart,
					calc     = start + len,
					records  = this.aiDisplay.length,
					features = this.oFeatures,
					paginate = features.bPaginate;
		
				if ( features.bServerSide ) {
					return paginate === false || len === -1 ?
						start + records :
						Math.min( start+len, this._iRecordsDisplay );
				}
				else {
					return ! paginate || calc>records || len===-1 ?
						records :
						calc;
				}
			},
		
			/**
			 * The DataTables object for this table
			 *  @type object
			 *  @default null
			 */
			"oInstance": null,
		
			/**
			 * Unique identifier for each instance of the DataTables object. If there
			 * is an ID on the table node, then it takes that value, otherwise an
			 * incrementing internal counter is used.
			 *  @type string
			 *  @default null
			 */
			"sInstance": null,
		
			/**
			 * tabindex attribute value that is added to DataTables control elements, allowing
			 * keyboard navigation of the table and its controls.
			 */
			"iTabIndex": 0,
		
			/**
			 * DIV container for the footer scrolling table if scrolling
			 */
			"nScrollHead": null,
		
			/**
			 * DIV container for the footer scrolling table if scrolling
			 */
			"nScrollFoot": null,
		
			/**
			 * Last applied sort
			 *  @type array
			 *  @default []
			 */
			"aLastSort": [],
		
			/**
			 * Stored plug-in instances
			 *  @type object
			 *  @default {}
			 */
			"oPlugins": {},
		
			/**
			 * Function used to get a row's id from the row's data
			 *  @type function
			 *  @default null
			 */
			"rowIdFn": null,
		
			/**
			 * Data location where to store a row's id
			 *  @type string
			 *  @default null
			 */
			"rowId": null
		};

		/**
		 * Extension object for DataTables that is used to provide all extension
		 * options.
		 *
		 * Note that the `DataTable.ext` object is available through
		 * `jQuery.fn.dataTable.ext` where it may be accessed and manipulated. It is
		 * also aliased to `jQuery.fn.dataTableExt` for historic reasons.
		 *  @namespace
		 *  @extends DataTable.models.ext
		 */
		
		
		/**
		 * DataTables extensions
		 * 
		 * This namespace acts as a collection area for plug-ins that can be used to
		 * extend DataTables capabilities. Indeed many of the build in methods
		 * use this method to provide their own capabilities (sorting methods for
		 * example).
		 *
		 * Note that this namespace is aliased to `jQuery.fn.dataTableExt` for legacy
		 * reasons
		 *
		 *  @namespace
		 */
		DataTable.ext = _ext = {
			/**
			 * Buttons. For use with the Buttons extension for DataTables. This is
			 * defined here so other extensions can define buttons regardless of load
			 * order. It is _not_ used by DataTables core.
			 *
			 *  @type object
			 *  @default {}
			 */
			buttons: {},
		
		
			/**
			 * Element class names
			 *
			 *  @type object
			 *  @default {}
			 */
			classes: {},
		
		
			/**
			 * DataTables build type (expanded by the download builder)
			 *
			 *  @type string
			 */
			builder: "-source-",
		
		
			/**
			 * Error reporting.
			 * 
			 * How should DataTables report an error. Can take the value 'alert',
			 * 'throw', 'none' or a function.
			 *
			 *  @type string|function
			 *  @default alert
			 */
			errMode: "alert",
		
		
			/**
			 * Feature plug-ins.
			 * 
			 * This is an array of objects which describe the feature plug-ins that are
			 * available to DataTables. These feature plug-ins are then available for
			 * use through the `dom` initialisation option.
			 * 
			 * Each feature plug-in is described by an object which must have the
			 * following properties:
			 * 
			 * * `fnInit` - function that is used to initialise the plug-in,
			 * * `cFeature` - a character so the feature can be enabled by the `dom`
			 *   instillation option. This is case sensitive.
			 *
			 * The `fnInit` function has the following input parameters:
			 *
			 * 1. `{object}` DataTables settings object: see
			 *    {@link DataTable.models.oSettings}
			 *
			 * And the following return is expected:
			 * 
			 * * {node|null} The element which contains your feature. Note that the
			 *   return may also be void if your plug-in does not require to inject any
			 *   DOM elements into DataTables control (`dom`) - for example this might
			 *   be useful when developing a plug-in which allows table control via
			 *   keyboard entry
			 *
			 *  @type array
			 *
			 *  @example
			 *    $.fn.dataTable.ext.features.push( {
			 *      "fnInit": function( oSettings ) {
			 *        return new TableTools( { "oDTSettings": oSettings } );
			 *      },
			 *      "cFeature": "T"
			 *    } );
			 */
			feature: [],
		
		
			/**
			 * Row searching.
			 * 
			 * This method of searching is complimentary to the default type based
			 * searching, and a lot more comprehensive as it allows you complete control
			 * over the searching logic. Each element in this array is a function
			 * (parameters described below) that is called for every row in the table,
			 * and your logic decides if it should be included in the searching data set
			 * or not.
			 *
			 * Searching functions have the following input parameters:
			 *
			 * 1. `{object}` DataTables settings object: see
			 *    {@link DataTable.models.oSettings}
			 * 2. `{array|object}` Data for the row to be processed (same as the
			 *    original format that was passed in as the data source, or an array
			 *    from a DOM data source
			 * 3. `{int}` Row index ({@link DataTable.models.oSettings.aoData}), which
			 *    can be useful to retrieve the `TR` element if you need DOM interaction.
			 *
			 * And the following return is expected:
			 *
			 * * {boolean} Include the row in the searched result set (true) or not
			 *   (false)
			 *
			 * Note that as with the main search ability in DataTables, technically this
			 * is "filtering", since it is subtractive. However, for consistency in
			 * naming we call it searching here.
			 *
			 *  @type array
			 *  @default []
			 *
			 *  @example
			 *    // The following example shows custom search being applied to the
			 *    // fourth column (i.e. the data[3] index) based on two input values
			 *    // from the end-user, matching the data in a certain range.
			 *    $.fn.dataTable.ext.search.push(
			 *      function( settings, data, dataIndex ) {
			 *        var min = document.getElementById('min').value * 1;
			 *        var max = document.getElementById('max').value * 1;
			 *        var version = data[3] == "-" ? 0 : data[3]*1;
			 *
			 *        if ( min == "" && max == "" ) {
			 *          return true;
			 *        }
			 *        else if ( min == "" && version < max ) {
			 *          return true;
			 *        }
			 *        else if ( min < version && "" == max ) {
			 *          return true;
			 *        }
			 *        else if ( min < version && version < max ) {
			 *          return true;
			 *        }
			 *        return false;
			 *      }
			 *    );
			 */
			search: [],
		
		
			/**
			 * Selector extensions
			 *
			 * The `selector` option can be used to extend the options available for the
			 * selector modifier options (`selector-modifier` object data type) that
			 * each of the three built in selector types offer (row, column and cell +
			 * their plural counterparts). For example the Select extension uses this
			 * mechanism to provide an option to select only rows, columns and cells
			 * that have been marked as selected by the end user (`{selected: true}`),
			 * which can be used in conjunction with the existing built in selector
			 * options.
			 *
			 * Each property is an array to which functions can be pushed. The functions
			 * take three attributes:
			 *
			 * * Settings object for the host table
			 * * Options object (`selector-modifier` object type)
			 * * Array of selected item indexes
			 *
			 * The return is an array of the resulting item indexes after the custom
			 * selector has been applied.
			 *
			 *  @type object
			 */
			selector: {
				cell: [],
				column: [],
				row: []
			},
		
		
			/**
			 * Internal functions, exposed for used in plug-ins.
			 * 
			 * Please note that you should not need to use the internal methods for
			 * anything other than a plug-in (and even then, try to avoid if possible).
			 * The internal function may change between releases.
			 *
			 *  @type object
			 *  @default {}
			 */
			internal: {},
		
		
			/**
			 * Legacy configuration options. Enable and disable legacy options that
			 * are available in DataTables.
			 *
			 *  @type object
			 */
			legacy: {
				/**
				 * Enable / disable DataTables 1.9 compatible server-side processing
				 * requests
				 *
				 *  @type boolean
				 *  @default null
				 */
				ajax: null
			},
		
		
			/**
			 * Pagination plug-in methods.
			 * 
			 * Each entry in this object is a function and defines which buttons should
			 * be shown by the pagination rendering method that is used for the table:
			 * {@link DataTable.ext.renderer.pageButton}. The renderer addresses how the
			 * buttons are displayed in the document, while the functions here tell it
			 * what buttons to display. This is done by returning an array of button
			 * descriptions (what each button will do).
			 *
			 * Pagination types (the four built in options and any additional plug-in
			 * options defined here) can be used through the `paginationType`
			 * initialisation parameter.
			 *
			 * The functions defined take two parameters:
			 *
			 * 1. `{int} page` The current page index
			 * 2. `{int} pages` The number of pages in the table
			 *
			 * Each function is expected to return an array where each element of the
			 * array can be one of:
			 *
			 * * `first` - Jump to first page when activated
			 * * `last` - Jump to last page when activated
			 * * `previous` - Show previous page when activated
			 * * `next` - Show next page when activated
			 * * `{int}` - Show page of the index given
			 * * `{array}` - A nested array containing the above elements to add a
			 *   containing 'DIV' element (might be useful for styling).
			 *
			 * Note that DataTables v1.9- used this object slightly differently whereby
			 * an object with two functions would be defined for each plug-in. That
			 * ability is still supported by DataTables 1.10+ to provide backwards
			 * compatibility, but this option of use is now decremented and no longer
			 * documented in DataTables 1.10+.
			 *
			 *  @type object
			 *  @default {}
			 *
			 *  @example
			 *    // Show previous, next and current page buttons only
			 *    $.fn.dataTableExt.oPagination.current = function ( page, pages ) {
			 *      return [ 'previous', page, 'next' ];
			 *    };
			 */
			pager: {},
		
		
			renderer: {
				pageButton: {},
				header: {}
			},
		
		
			/**
			 * Ordering plug-ins - custom data source
			 * 
			 * The extension options for ordering of data available here is complimentary
			 * to the default type based ordering that DataTables typically uses. It
			 * allows much greater control over the the data that is being used to
			 * order a column, but is necessarily therefore more complex.
			 * 
			 * This type of ordering is useful if you want to do ordering based on data
			 * live from the DOM (for example the contents of an 'input' element) rather
			 * than just the static string that DataTables knows of.
			 * 
			 * The way these plug-ins work is that you create an array of the values you
			 * wish to be ordering for the column in question and then return that
			 * array. The data in the array much be in the index order of the rows in
			 * the table (not the currently ordering order!). Which order data gathering
			 * function is run here depends on the `dt-init columns.orderDataType`
			 * parameter that is used for the column (if any).
			 *
			 * The functions defined take two parameters:
			 *
			 * 1. `{object}` DataTables settings object: see
			 *    {@link DataTable.models.oSettings}
			 * 2. `{int}` Target column index
			 *
			 * Each function is expected to return an array:
			 *
			 * * `{array}` Data for the column to be ordering upon
			 *
			 *  @type array
			 *
			 *  @example
			 *    // Ordering using `input` node values
			 *    $.fn.dataTable.ext.order['dom-text'] = function  ( settings, col )
			 *    {
			 *      return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
			 *        return $('input', td).val();
			 *      } );
			 *    }
			 */
			order: {},
		
		
			/**
			 * Type based plug-ins.
			 *
			 * Each column in DataTables has a type assigned to it, either by automatic
			 * detection or by direct assignment using the `type` option for the column.
			 * The type of a column will effect how it is ordering and search (plug-ins
			 * can also make use of the column type if required).
			 *
			 * @namespace
			 */
			type: {
				/**
				 * Type detection functions.
				 *
				 * The functions defined in this object are used to automatically detect
				 * a column's type, making initialisation of DataTables super easy, even
				 * when complex data is in the table.
				 *
				 * The functions defined take two parameters:
				 *
			     *  1. `{*}` Data from the column cell to be analysed
			     *  2. `{settings}` DataTables settings object. This can be used to
			     *     perform context specific type detection - for example detection
			     *     based on language settings such as using a comma for a decimal
			     *     place. Generally speaking the options from the settings will not
			     *     be required
				 *
				 * Each function is expected to return:
				 *
				 * * `{string|null}` Data type detected, or null if unknown (and thus
				 *   pass it on to the other type detection functions.
				 *
				 *  @type array
				 *
				 *  @example
				 *    // Currency type detection plug-in:
				 *    $.fn.dataTable.ext.type.detect.push(
				 *      function ( data, settings ) {
				 *        // Check the numeric part
				 *        if ( ! $.isNumeric( data.substring(1) ) ) {
				 *          return null;
				 *        }
				 *
				 *        // Check prefixed by currency
				 *        if ( data.charAt(0) == '$' || data.charAt(0) == '&pound;' ) {
				 *          return 'currency';
				 *        }
				 *        return null;
				 *      }
				 *    );
				 */
				detect: [],
		
		
				/**
				 * Type based search formatting.
				 *
				 * The type based searching functions can be used to pre-format the
				 * data to be search on. For example, it can be used to strip HTML
				 * tags or to de-format telephone numbers for numeric only searching.
				 *
				 * Note that is a search is not defined for a column of a given type,
				 * no search formatting will be performed.
				 * 
				 * Pre-processing of searching data plug-ins - When you assign the sType
				 * for a column (or have it automatically detected for you by DataTables
				 * or a type detection plug-in), you will typically be using this for
				 * custom sorting, but it can also be used to provide custom searching
				 * by allowing you to pre-processing the data and returning the data in
				 * the format that should be searched upon. This is done by adding
				 * functions this object with a parameter name which matches the sType
				 * for that target column. This is the corollary of <i>afnSortData</i>
				 * for searching data.
				 *
				 * The functions defined take a single parameter:
				 *
			     *  1. `{*}` Data from the column cell to be prepared for searching
				 *
				 * Each function is expected to return:
				 *
				 * * `{string|null}` Formatted string that will be used for the searching.
				 *
				 *  @type object
				 *  @default {}
				 *
				 *  @example
				 *    $.fn.dataTable.ext.type.search['title-numeric'] = function ( d ) {
				 *      return d.replace(/\n/g," ").replace( /<.*?>/g, "" );
				 *    }
				 */
				search: {},
		
		
				/**
				 * Type based ordering.
				 *
				 * The column type tells DataTables what ordering to apply to the table
				 * when a column is sorted upon. The order for each type that is defined,
				 * is defined by the functions available in this object.
				 *
				 * Each ordering option can be described by three properties added to
				 * this object:
				 *
				 * * `{type}-pre` - Pre-formatting function
				 * * `{type}-asc` - Ascending order function
				 * * `{type}-desc` - Descending order function
				 *
				 * All three can be used together, only `{type}-pre` or only
				 * `{type}-asc` and `{type}-desc` together. It is generally recommended
				 * that only `{type}-pre` is used, as this provides the optimal
				 * implementation in terms of speed, although the others are provided
				 * for compatibility with existing Javascript sort functions.
				 *
				 * `{type}-pre`: Functions defined take a single parameter:
				 *
			     *  1. `{*}` Data from the column cell to be prepared for ordering
				 *
				 * And return:
				 *
				 * * `{*}` Data to be sorted upon
				 *
				 * `{type}-asc` and `{type}-desc`: Functions are typical Javascript sort
				 * functions, taking two parameters:
				 *
			     *  1. `{*}` Data to compare to the second parameter
			     *  2. `{*}` Data to compare to the first parameter
				 *
				 * And returning:
				 *
				 * * `{*}` Ordering match: <0 if first parameter should be sorted lower
				 *   than the second parameter, ===0 if the two parameters are equal and
				 *   >0 if the first parameter should be sorted height than the second
				 *   parameter.
				 * 
				 *  @type object
				 *  @default {}
				 *
				 *  @example
				 *    // Numeric ordering of formatted numbers with a pre-formatter
				 *    $.extend( $.fn.dataTable.ext.type.order, {
				 *      "string-pre": function(x) {
				 *        a = (a === "-" || a === "") ? 0 : a.replace( /[^\d\-\.]/g, "" );
				 *        return parseFloat( a );
				 *      }
				 *    } );
				 *
				 *  @example
				 *    // Case-sensitive string ordering, with no pre-formatting method
				 *    $.extend( $.fn.dataTable.ext.order, {
				 *      "string-case-asc": function(x,y) {
				 *        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				 *      },
				 *      "string-case-desc": function(x,y) {
				 *        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
				 *      }
				 *    } );
				 */
				order: {}
			},
		
			/**
			 * Unique DataTables instance counter
			 *
			 * @type int
			 * @private
			 */
			_unique: 0,
		
		
			//
			// Depreciated
			// The following properties are retained for backwards compatiblity only.
			// The should not be used in new projects and will be removed in a future
			// version
			//
		
			/**
			 * Version check function.
			 *  @type function
			 *  @depreciated Since 1.10
			 */
			fnVersionCheck: DataTable.fnVersionCheck,
		
		
			/**
			 * Index for what 'this' index API functions should use
			 *  @type int
			 *  @deprecated Since v1.10
			 */
			iApiIndex: 0,
		
		
			/**
			 * jQuery UI class container
			 *  @type object
			 *  @deprecated Since v1.10
			 */
			oJUIClasses: {},
		
		
			/**
			 * Software version
			 *  @type string
			 *  @deprecated Since v1.10
			 */
			sVersion: DataTable.version
		};
		
		
		//
		// Backwards compatibility. Alias to pre 1.10 Hungarian notation counter parts
		//
		$.extend( _ext, {
			afnFiltering: _ext.search,
			aTypes:       _ext.type.detect,
			ofnSearch:    _ext.type.search,
			oSort:        _ext.type.order,
			afnSortData:  _ext.order,
			aoFeatures:   _ext.feature,
			oApi:         _ext.internal,
			oStdClasses:  _ext.classes,
			oPagination:  _ext.pager
		} );
		
		
		$.extend( DataTable.ext.classes, {
			"sTable": "dataTable",
			"sNoFooter": "no-footer",
		
			/* Paging buttons */
			"sPageButton": "paginate_button",
			"sPageButtonActive": "current",
			"sPageButtonDisabled": "disabled",
		
			/* Striping classes */
			"sStripeOdd": "odd",
			"sStripeEven": "even",
		
			/* Empty row */
			"sRowEmpty": "dataTables_empty",
		
			/* Features */
			"sWrapper": "dataTables_wrapper",
			"sFilter": "dataTables_filter",
			"sInfo": "dataTables_info",
			"sPaging": "dataTables_paginate paging_", /* Note that the type is postfixed */
			"sLength": "dataTables_length",
			"sProcessing": "dataTables_processing",
		
			/* Sorting */
			"sSortAsc": "sorting_asc",
			"sSortDesc": "sorting_desc",
			"sSortable": "sorting", /* Sortable in both directions */
			"sSortableAsc": "sorting_asc_disabled",
			"sSortableDesc": "sorting_desc_disabled",
			"sSortableNone": "sorting_disabled",
			"sSortColumn": "sorting_", /* Note that an int is postfixed for the sorting order */
		
			/* Filtering */
			"sFilterInput": "",
		
			/* Page length */
			"sLengthSelect": "",
		
			/* Scrolling */
			"sScrollWrapper": "dataTables_scroll",
			"sScrollHead": "dataTables_scrollHead",
			"sScrollHeadInner": "dataTables_scrollHeadInner",
			"sScrollBody": "dataTables_scrollBody",
			"sScrollFoot": "dataTables_scrollFoot",
			"sScrollFootInner": "dataTables_scrollFootInner",
		
			/* Misc */
			"sHeaderTH": "",
			"sFooterTH": "",
		
			// Deprecated
			"sSortJUIAsc": "",
			"sSortJUIDesc": "",
			"sSortJUI": "",
			"sSortJUIAscAllowed": "",
			"sSortJUIDescAllowed": "",
			"sSortJUIWrapper": "",
			"sSortIcon": "",
			"sJUIHeader": "",
			"sJUIFooter": ""
		} );
		
		
		(function() {
		
		// Reused strings for better compression. Closure compiler appears to have a
		// weird edge case where it is trying to expand strings rather than use the
		// variable version. This results in about 200 bytes being added, for very
		// little preference benefit since it this run on script load only.
		var _empty = '';
		_empty = '';
		
		var _stateDefault = _empty + 'ui-state-default';
		var _sortIcon     = _empty + 'css_right ui-icon ui-icon-';
		var _headerFooter = _empty + 'fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix';
		
		$.extend( DataTable.ext.oJUIClasses, DataTable.ext.classes, {
			/* Full numbers paging buttons */
			"sPageButton":         "fg-button ui-button "+_stateDefault,
			"sPageButtonActive":   "ui-state-disabled",
			"sPageButtonDisabled": "ui-state-disabled",
		
			/* Features */
			"sPaging": "dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi "+
				"ui-buttonset-multi paging_", /* Note that the type is postfixed */
		
			/* Sorting */
			"sSortAsc":            _stateDefault+" sorting_asc",
			"sSortDesc":           _stateDefault+" sorting_desc",
			"sSortable":           _stateDefault+" sorting",
			"sSortableAsc":        _stateDefault+" sorting_asc_disabled",
			"sSortableDesc":       _stateDefault+" sorting_desc_disabled",
			"sSortableNone":       _stateDefault+" sorting_disabled",
			"sSortJUIAsc":         _sortIcon+"triangle-1-n",
			"sSortJUIDesc":        _sortIcon+"triangle-1-s",
			"sSortJUI":            _sortIcon+"carat-2-n-s",
			"sSortJUIAscAllowed":  _sortIcon+"carat-1-n",
			"sSortJUIDescAllowed": _sortIcon+"carat-1-s",
			"sSortJUIWrapper":     "DataTables_sort_wrapper",
			"sSortIcon":           "DataTables_sort_icon",
		
			/* Scrolling */
			"sScrollHead": "dataTables_scrollHead "+_stateDefault,
			"sScrollFoot": "dataTables_scrollFoot "+_stateDefault,
		
			/* Misc */
			"sHeaderTH":  _stateDefault,
			"sFooterTH":  _stateDefault,
			"sJUIHeader": _headerFooter+" ui-corner-tl ui-corner-tr",
			"sJUIFooter": _headerFooter+" ui-corner-bl ui-corner-br"
		} );
		
		}());
		
		
		
		var extPagination = DataTable.ext.pager;
		
		function _numbers ( page, pages ) {
			var
				numbers = [],
				buttons = extPagination.numbers_length,
				half = Math.floor( buttons / 2 ),
				i = 1;
		
			if ( pages <= buttons ) {
				numbers = _range( 0, pages );
			}
			else if ( page <= half ) {
				numbers = _range( 0, buttons-2 );
				numbers.push( 'ellipsis' );
				numbers.push( pages-1 );
			}
			else if ( page >= pages - 1 - half ) {
				numbers = _range( pages-(buttons-2), pages );
				numbers.splice( 0, 0, 'ellipsis' ); // no unshift in ie6
				numbers.splice( 0, 0, 0 );
			}
			else {
				numbers = _range( page-half+2, page+half-1 );
				numbers.push( 'ellipsis' );
				numbers.push( pages-1 );
				numbers.splice( 0, 0, 'ellipsis' );
				numbers.splice( 0, 0, 0 );
			}
		
			numbers.DT_el = 'span';
			return numbers;
		}
		
		
		$.extend( extPagination, {
			simple: function ( page, pages ) {
				return [ 'previous', 'next' ];
			},
		
			full: function ( page, pages ) {
				return [  'first', 'previous', 'next', 'last' ];
			},
		
			numbers: function ( page, pages ) {
				return [ _numbers(page, pages) ];
			},
		
			simple_numbers: function ( page, pages ) {
				return [ 'previous', _numbers(page, pages), 'next' ];
			},
		
			full_numbers: function ( page, pages ) {
				return [ 'first', 'previous', _numbers(page, pages), 'next', 'last' ];
			},
		
			// For testing and plug-ins to use
			_numbers: _numbers,
		
			// Number of number buttons (including ellipsis) to show. _Must be odd!_
			numbers_length: 7
		} );
		
		
		$.extend( true, DataTable.ext.renderer, {
			pageButton: {
				_: function ( settings, host, idx, buttons, page, pages ) {
					var classes = settings.oClasses;
					var lang = settings.oLanguage.oPaginate;
					var aria = settings.oLanguage.oAria.paginate || {};
					var btnDisplay, btnClass, counter=0;
		
					var attach = function( container, buttons ) {
						var i, ien, node, button;
						var clickHandler = function ( e ) {
							_fnPageChange( settings, e.data.action, true );
						};
		
						for ( i=0, ien=buttons.length ; i<ien ; i++ ) {
							button = buttons[i];
		
							if ( $.isArray( button ) ) {
								var inner = $( '<'+(button.DT_el || 'div')+'/>' )
									.appendTo( container );
								attach( inner, button );
							}
							else {
								btnDisplay = null;
								btnClass = '';
		
								switch ( button ) {
									case 'ellipsis':
										container.append('<span class="ellipsis">&#x2026;</span>');
										break;
		
									case 'first':
										btnDisplay = lang.sFirst;
										btnClass = button + (page > 0 ?
											'' : ' '+classes.sPageButtonDisabled);
										break;
		
									case 'previous':
										btnDisplay = lang.sPrevious;
										btnClass = button + (page > 0 ?
											'' : ' '+classes.sPageButtonDisabled);
										break;
		
									case 'next':
										btnDisplay = lang.sNext;
										btnClass = button + (page < pages-1 ?
											'' : ' '+classes.sPageButtonDisabled);
										break;
		
									case 'last':
										btnDisplay = lang.sLast;
										btnClass = button + (page < pages-1 ?
											'' : ' '+classes.sPageButtonDisabled);
										break;
		
									default:
										btnDisplay = button + 1;
										btnClass = page === button ?
											classes.sPageButtonActive : '';
										break;
								}
		
								if ( btnDisplay !== null ) {
									node = $('<a>', {
											'class': classes.sPageButton+' '+btnClass,
											'aria-controls': settings.sTableId,
											'aria-label': aria[ button ],
											'data-dt-idx': counter,
											'tabindex': settings.iTabIndex,
											'id': idx === 0 && typeof button === 'string' ?
												settings.sTableId +'_'+ button :
												null
										} )
										.html( btnDisplay )
										.appendTo( container );
		
									_fnBindAction(
										node, {action: button}, clickHandler
									);
		
									counter++;
								}
							}
						}
					};
		
					// IE9 throws an 'unknown error' if document.activeElement is used
					// inside an iframe or frame. Try / catch the error. Not good for
					// accessibility, but neither are frames.
					var activeEl;
		
					try {
						// Because this approach is destroying and recreating the paging
						// elements, focus is lost on the select button which is bad for
						// accessibility. So we want to restore focus once the draw has
						// completed
						activeEl = $(host).find(document.activeElement).data('dt-idx');
					}
					catch (e) {}
		
					attach( $(host).empty(), buttons );
		
					if ( activeEl ) {
						$(host).find( '[data-dt-idx='+activeEl+']' ).focus();
					}
				}
			}
		} );
		
		
		
		// Built in type detection. See model.ext.aTypes for information about
		// what is required from this methods.
		$.extend( DataTable.ext.type.detect, [
			// Plain numbers - first since V8 detects some plain numbers as dates
			// e.g. Date.parse('55') (but not all, e.g. Date.parse('22')...).
			function ( d, settings )
			{
				var decimal = settings.oLanguage.sDecimal;
				return _isNumber( d, decimal ) ? 'num'+decimal : null;
			},
		
			// Dates (only those recognised by the browser's Date.parse)
			function ( d, settings )
			{
				// V8 will remove any unknown characters at the start and end of the
				// expression, leading to false matches such as `$245.12` or `10%` being
				// a valid date. See forum thread 18941 for detail.
				if ( d && !(d instanceof Date) && ( ! _re_date_start.test(d) || ! _re_date_end.test(d) ) ) {
					return null;
				}
				var parsed = Date.parse(d);
				return (parsed !== null && !isNaN(parsed)) || _empty(d) ? 'date' : null;
			},
		
			// Formatted numbers
			function ( d, settings )
			{
				var decimal = settings.oLanguage.sDecimal;
				return _isNumber( d, decimal, true ) ? 'num-fmt'+decimal : null;
			},
		
			// HTML numeric
			function ( d, settings )
			{
				var decimal = settings.oLanguage.sDecimal;
				return _htmlNumeric( d, decimal ) ? 'html-num'+decimal : null;
			},
		
			// HTML numeric, formatted
			function ( d, settings )
			{
				var decimal = settings.oLanguage.sDecimal;
				return _htmlNumeric( d, decimal, true ) ? 'html-num-fmt'+decimal : null;
			},
		
			// HTML (this is strict checking - there must be html)
			function ( d, settings )
			{
				return _empty( d ) || (typeof d === 'string' && d.indexOf('<') !== -1) ?
					'html' : null;
			}
		] );
		
		
		
		// Filter formatting functions. See model.ext.ofnSearch for information about
		// what is required from these methods.
		// 
		// Note that additional search methods are added for the html numbers and
		// html formatted numbers by `_addNumericSort()` when we know what the decimal
		// place is
		
		
		$.extend( DataTable.ext.type.search, {
			html: function ( data ) {
				return _empty(data) ?
					data :
					typeof data === 'string' ?
						data
							.replace( _re_new_lines, " " )
							.replace( _re_html, "" ) :
						'';
			},
		
			string: function ( data ) {
				return _empty(data) ?
					data :
					typeof data === 'string' ?
						data.replace( _re_new_lines, " " ) :
						data;
			}
		} );
		
		
		
		var __numericReplace = function ( d, decimalPlace, re1, re2 ) {
			if ( d !== 0 && (!d || d === '-') ) {
				return -Infinity;
			}
		
			// If a decimal place other than `.` is used, it needs to be given to the
			// function so we can detect it and replace with a `.` which is the only
			// decimal place Javascript recognises - it is not locale aware.
			if ( decimalPlace ) {
				d = _numToDecimal( d, decimalPlace );
			}
		
			if ( d.replace ) {
				if ( re1 ) {
					d = d.replace( re1, '' );
				}
		
				if ( re2 ) {
					d = d.replace( re2, '' );
				}
			}
		
			return d * 1;
		};
		
		
		// Add the numeric 'deformatting' functions for sorting and search. This is done
		// in a function to provide an easy ability for the language options to add
		// additional methods if a non-period decimal place is used.
		function _addNumericSort ( decimalPlace ) {
			$.each(
				{
					// Plain numbers
					"num": function ( d ) {
						return __numericReplace( d, decimalPlace );
					},
		
					// Formatted numbers
					"num-fmt": function ( d ) {
						return __numericReplace( d, decimalPlace, _re_formatted_numeric );
					},
		
					// HTML numeric
					"html-num": function ( d ) {
						return __numericReplace( d, decimalPlace, _re_html );
					},
		
					// HTML numeric, formatted
					"html-num-fmt": function ( d ) {
						return __numericReplace( d, decimalPlace, _re_html, _re_formatted_numeric );
					}
				},
				function ( key, fn ) {
					// Add the ordering method
					_ext.type.order[ key+decimalPlace+'-pre' ] = fn;
		
					// For HTML types add a search formatter that will strip the HTML
					if ( key.match(/^html\-/) ) {
						_ext.type.search[ key+decimalPlace ] = _ext.type.search.html;
					}
				}
			);
		}
		
		
		// Default sort methods
		$.extend( _ext.type.order, {
			// Dates
			"date-pre": function ( d ) {
				return Date.parse( d ) || 0;
			},
		
			// html
			"html-pre": function ( a ) {
				return _empty(a) ?
					'' :
					a.replace ?
						a.replace( /<.*?>/g, "" ).toLowerCase() :
						a+'';
			},
		
			// string
			"string-pre": function ( a ) {
				// This is a little complex, but faster than always calling toString,
				// http://jsperf.com/tostring-v-check
				return _empty(a) ?
					'' :
					typeof a === 'string' ?
						a.toLowerCase() :
						! a.toString ?
							'' :
							a.toString();
			},
		
			// string-asc and -desc are retained only for compatibility with the old
			// sort methods
			"string-asc": function ( x, y ) {
				return ((x < y) ? -1 : ((x > y) ? 1 : 0));
			},
		
			"string-desc": function ( x, y ) {
				return ((x < y) ? 1 : ((x > y) ? -1 : 0));
			}
		} );
		
		
		// Numeric sorting types - order doesn't matter here
		_addNumericSort( '' );
		
		
		$.extend( true, DataTable.ext.renderer, {
			header: {
				_: function ( settings, cell, column, classes ) {
					// No additional mark-up required
					// Attach a sort listener to update on sort - note that using the
					// `DT` namespace will allow the event to be removed automatically
					// on destroy, while the `dt` namespaced event is the one we are
					// listening for
					$(settings.nTable).on( 'order.dt.DT', function ( e, ctx, sorting, columns ) {
						if ( settings !== ctx ) { // need to check this this is the host
							return;               // table, not a nested one
						}
		
						var colIdx = column.idx;
		
						cell
							.removeClass(
								column.sSortingClass +' '+
								classes.sSortAsc +' '+
								classes.sSortDesc
							)
							.addClass( columns[ colIdx ] == 'asc' ?
								classes.sSortAsc : columns[ colIdx ] == 'desc' ?
									classes.sSortDesc :
									column.sSortingClass
							);
					} );
				},
		
				jqueryui: function ( settings, cell, column, classes ) {
					$('<div/>')
						.addClass( classes.sSortJUIWrapper )
						.append( cell.contents() )
						.append( $('<span/>')
							.addClass( classes.sSortIcon+' '+column.sSortingClassJUI )
						)
						.appendTo( cell );
		
					// Attach a sort listener to update on sort
					$(settings.nTable).on( 'order.dt.DT', function ( e, ctx, sorting, columns ) {
						if ( settings !== ctx ) {
							return;
						}
		
						var colIdx = column.idx;
		
						cell
							.removeClass( classes.sSortAsc +" "+classes.sSortDesc )
							.addClass( columns[ colIdx ] == 'asc' ?
								classes.sSortAsc : columns[ colIdx ] == 'desc' ?
									classes.sSortDesc :
									column.sSortingClass
							);
		
						cell
							.find( 'span.'+classes.sSortIcon )
							.removeClass(
								classes.sSortJUIAsc +" "+
								classes.sSortJUIDesc +" "+
								classes.sSortJUI +" "+
								classes.sSortJUIAscAllowed +" "+
								classes.sSortJUIDescAllowed
							)
							.addClass( columns[ colIdx ] == 'asc' ?
								classes.sSortJUIAsc : columns[ colIdx ] == 'desc' ?
									classes.sSortJUIDesc :
									column.sSortingClassJUI
							);
					} );
				}
			}
		} );
		
		/*
		 * Public helper functions. These aren't used internally by DataTables, or
		 * called by any of the options passed into DataTables, but they can be used
		 * externally by developers working with DataTables. They are helper functions
		 * to make working with DataTables a little bit easier.
		 */
		
		var __htmlEscapeEntities = function ( d ) {
			return typeof d === 'string' ?
				d.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;') :
				d;
		};
		
		/**
		 * Helpers for `columns.render`.
		 *
		 * The options defined here can be used with the `columns.render` initialisation
		 * option to provide a display renderer. The following functions are defined:
		 *
		 * * `number` - Will format numeric data (defined by `columns.data`) for
		 *   display, retaining the original unformatted data for sorting and filtering.
		 *   It takes 5 parameters:
		 *   * `string` - Thousands grouping separator
		 *   * `string` - Decimal point indicator
		 *   * `integer` - Number of decimal points to show
		 *   * `string` (optional) - Prefix.
		 *   * `string` (optional) - Postfix (/suffix).
		 * * `text` - Escape HTML to help prevent XSS attacks. It has no optional
		 *   parameters.
		 *
		 * @example
		 *   // Column definition using the number renderer
		 *   {
		 *     data: "salary",
		 *     render: $.fn.dataTable.render.number( '\'', '.', 0, '$' )
		 *   }
		 *
		 * @namespace
		 */
		DataTable.render = {
			number: function ( thousands, decimal, precision, prefix, postfix ) {
				return {
					display: function ( d ) {
						if ( typeof d !== 'number' && typeof d !== 'string' ) {
							return d;
						}
		
						var negative = d < 0 ? '-' : '';
						var flo = parseFloat( d );
		
						// If NaN then there isn't much formatting that we can do - just
						// return immediately, escaping any HTML (this was supposed to
						// be a number after all)
						if ( isNaN( flo ) ) {
							return __htmlEscapeEntities( d );
						}
		
						d = Math.abs( flo );
		
						var intPart = parseInt( d, 10 );
						var floatPart = precision ?
							decimal+(d - intPart).toFixed( precision ).substring( 2 ):
							'';
		
						return negative + (prefix||'') +
							intPart.toString().replace(
								/\B(?=(\d{3})+(?!\d))/g, thousands
							) +
							floatPart +
							(postfix||'');
					}
				};
			},
		
			text: function () {
				return {
					display: __htmlEscapeEntities
				};
			}
		};
		
		
		/*
		 * This is really a good bit rubbish this method of exposing the internal methods
		 * publicly... - To be fixed in 2.0 using methods on the prototype
		 */
		
		
		/**
		 * Create a wrapper function for exporting an internal functions to an external API.
		 *  @param {string} fn API function name
		 *  @returns {function} wrapped function
		 *  @memberof DataTable#internal
		 */
		function _fnExternApiFunc (fn)
		{
			return function() {
				var args = [_fnSettingsFromNode( this[DataTable.ext.iApiIndex] )].concat(
					Array.prototype.slice.call(arguments)
				);
				return DataTable.ext.internal[fn].apply( this, args );
			};
		}
		
		
		/**
		 * Reference to internal functions for use by plug-in developers. Note that
		 * these methods are references to internal functions and are considered to be
		 * private. If you use these methods, be aware that they are liable to change
		 * between versions.
		 *  @namespace
		 */
		$.extend( DataTable.ext.internal, {
			_fnExternApiFunc: _fnExternApiFunc,
			_fnBuildAjax: _fnBuildAjax,
			_fnAjaxUpdate: _fnAjaxUpdate,
			_fnAjaxParameters: _fnAjaxParameters,
			_fnAjaxUpdateDraw: _fnAjaxUpdateDraw,
			_fnAjaxDataSrc: _fnAjaxDataSrc,
			_fnAddColumn: _fnAddColumn,
			_fnColumnOptions: _fnColumnOptions,
			_fnAdjustColumnSizing: _fnAdjustColumnSizing,
			_fnVisibleToColumnIndex: _fnVisibleToColumnIndex,
			_fnColumnIndexToVisible: _fnColumnIndexToVisible,
			_fnVisbleColumns: _fnVisbleColumns,
			_fnGetColumns: _fnGetColumns,
			_fnColumnTypes: _fnColumnTypes,
			_fnApplyColumnDefs: _fnApplyColumnDefs,
			_fnHungarianMap: _fnHungarianMap,
			_fnCamelToHungarian: _fnCamelToHungarian,
			_fnLanguageCompat: _fnLanguageCompat,
			_fnBrowserDetect: _fnBrowserDetect,
			_fnAddData: _fnAddData,
			_fnAddTr: _fnAddTr,
			_fnNodeToDataIndex: _fnNodeToDataIndex,
			_fnNodeToColumnIndex: _fnNodeToColumnIndex,
			_fnGetCellData: _fnGetCellData,
			_fnSetCellData: _fnSetCellData,
			_fnSplitObjNotation: _fnSplitObjNotation,
			_fnGetObjectDataFn: _fnGetObjectDataFn,
			_fnSetObjectDataFn: _fnSetObjectDataFn,
			_fnGetDataMaster: _fnGetDataMaster,
			_fnClearTable: _fnClearTable,
			_fnDeleteIndex: _fnDeleteIndex,
			_fnInvalidate: _fnInvalidate,
			_fnGetRowElements: _fnGetRowElements,
			_fnCreateTr: _fnCreateTr,
			_fnBuildHead: _fnBuildHead,
			_fnDrawHead: _fnDrawHead,
			_fnDraw: _fnDraw,
			_fnReDraw: _fnReDraw,
			_fnAddOptionsHtml: _fnAddOptionsHtml,
			_fnDetectHeader: _fnDetectHeader,
			_fnGetUniqueThs: _fnGetUniqueThs,
			_fnFeatureHtmlFilter: _fnFeatureHtmlFilter,
			_fnFilterComplete: _fnFilterComplete,
			_fnFilterCustom: _fnFilterCustom,
			_fnFilterColumn: _fnFilterColumn,
			_fnFilter: _fnFilter,
			_fnFilterCreateSearch: _fnFilterCreateSearch,
			_fnEscapeRegex: _fnEscapeRegex,
			_fnFilterData: _fnFilterData,
			_fnFeatureHtmlInfo: _fnFeatureHtmlInfo,
			_fnUpdateInfo: _fnUpdateInfo,
			_fnInfoMacros: _fnInfoMacros,
			_fnInitialise: _fnInitialise,
			_fnInitComplete: _fnInitComplete,
			_fnLengthChange: _fnLengthChange,
			_fnFeatureHtmlLength: _fnFeatureHtmlLength,
			_fnFeatureHtmlPaginate: _fnFeatureHtmlPaginate,
			_fnPageChange: _fnPageChange,
			_fnFeatureHtmlProcessing: _fnFeatureHtmlProcessing,
			_fnProcessingDisplay: _fnProcessingDisplay,
			_fnFeatureHtmlTable: _fnFeatureHtmlTable,
			_fnScrollDraw: _fnScrollDraw,
			_fnApplyToChildren: _fnApplyToChildren,
			_fnCalculateColumnWidths: _fnCalculateColumnWidths,
			_fnThrottle: _fnThrottle,
			_fnConvertToWidth: _fnConvertToWidth,
			_fnGetWidestNode: _fnGetWidestNode,
			_fnGetMaxLenString: _fnGetMaxLenString,
			_fnStringToCss: _fnStringToCss,
			_fnSortFlatten: _fnSortFlatten,
			_fnSort: _fnSort,
			_fnSortAria: _fnSortAria,
			_fnSortListener: _fnSortListener,
			_fnSortAttachListener: _fnSortAttachListener,
			_fnSortingClasses: _fnSortingClasses,
			_fnSortData: _fnSortData,
			_fnSaveState: _fnSaveState,
			_fnLoadState: _fnLoadState,
			_fnSettingsFromNode: _fnSettingsFromNode,
			_fnLog: _fnLog,
			_fnMap: _fnMap,
			_fnBindAction: _fnBindAction,
			_fnCallbackReg: _fnCallbackReg,
			_fnCallbackFire: _fnCallbackFire,
			_fnLengthOverflow: _fnLengthOverflow,
			_fnRenderer: _fnRenderer,
			_fnDataSource: _fnDataSource,
			_fnRowAttributes: _fnRowAttributes,
			_fnCalculateEnd: function () {} // Used by a lot of plug-ins, but redundant
			                                // in 1.10, so this dead-end function is
			                                // added to prevent errors
		} );
		

		// jQuery access
		$.fn.dataTable = DataTable;

		// Provide access to the host jQuery object (circular reference)
		DataTable.$ = $;

		// Legacy aliases
		$.fn.dataTableSettings = DataTable.settings;
		$.fn.dataTableExt = DataTable.ext;

		// With a capital `D` we return a DataTables API instance rather than a
		// jQuery object
		$.fn.DataTable = function ( opts ) {
			return $(this).dataTable( opts ).api();
		};

		// All properties that are available to $.fn.dataTable should also be
		// available on $.fn.DataTable
		$.each( DataTable, function ( prop, val ) {
			$.fn.DataTable[ prop ] = val;
		} );


		// Information about events fired by DataTables - for documentation.
		/**
		 * Draw event, fired whenever the table is redrawn on the page, at the same
		 * point as fnDrawCallback. This may be useful for binding events or
		 * performing calculations when the table is altered at all.
		 *  @name DataTable#draw.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
		 */

		/**
		 * Search event, fired when the searching applied to the table (using the
		 * built-in global search, or column filters) is altered.
		 *  @name DataTable#search.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
		 */

		/**
		 * Page change event, fired when the paging of the table is altered.
		 *  @name DataTable#page.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
		 */

		/**
		 * Order event, fired when the ordering applied to the table is altered.
		 *  @name DataTable#order.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
		 */

		/**
		 * DataTables initialisation complete event, fired when the table is fully
		 * drawn, including Ajax data loaded, if Ajax data is required.
		 *  @name DataTable#init.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} oSettings DataTables settings object
		 *  @param {object} json The JSON object request from the server - only
		 *    present if client-side Ajax sourced data is used</li></ol>
		 */

		/**
		 * State save event, fired when the table has changed state a new state save
		 * is required. This event allows modification of the state saving object
		 * prior to actually doing the save, including addition or other state
		 * properties (for plug-ins) or modification of a DataTables core property.
		 *  @name DataTable#stateSaveParams.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} oSettings DataTables settings object
		 *  @param {object} json The state information to be saved
		 */

		/**
		 * State load event, fired when the table is loading state from the stored
		 * data, but prior to the settings object being modified by the saved state
		 * - allowing modification of the saved state is required or loading of
		 * state for a plug-in.
		 *  @name DataTable#stateLoadParams.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} oSettings DataTables settings object
		 *  @param {object} json The saved state information
		 */

		/**
		 * State loaded event, fired when state has been loaded from stored data and
		 * the settings object has been modified by the loaded data.
		 *  @name DataTable#stateLoaded.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} oSettings DataTables settings object
		 *  @param {object} json The saved state information
		 */

		/**
		 * Processing event, fired when DataTables is doing some kind of processing
		 * (be it, order, searcg or anything else). It can be used to indicate to
		 * the end user that there is something happening, or that something has
		 * finished.
		 *  @name DataTable#processing.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} oSettings DataTables settings object
		 *  @param {boolean} bShow Flag for if DataTables is doing processing or not
		 */

		/**
		 * Ajax (XHR) event, fired whenever an Ajax request is completed from a
		 * request to made to the server for new data. This event is called before
		 * DataTables processed the returned data, so it can also be used to pre-
		 * process the data returned from the server, if needed.
		 *
		 * Note that this trigger is called in `fnServerData`, if you override
		 * `fnServerData` and which to use this event, you need to trigger it in you
		 * success function.
		 *  @name DataTable#xhr.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
		 *  @param {object} json JSON returned from the server
		 *
		 *  @example
		 *     // Use a custom property returned from the server in another DOM element
		 *     $('#table').dataTable().on('xhr.dt', function (e, settings, json) {
		 *       $('#status').html( json.status );
		 *     } );
		 *
		 *  @example
		 *     // Pre-process the data returned from the server
		 *     $('#table').dataTable().on('xhr.dt', function (e, settings, json) {
		 *       for ( var i=0, ien=json.aaData.length ; i<ien ; i++ ) {
		 *         json.aaData[i].sum = json.aaData[i].one + json.aaData[i].two;
		 *       }
		 *       // Note no return - manipulate the data directly in the JSON object.
		 *     } );
		 */

		/**
		 * Destroy event, fired when the DataTable is destroyed by calling fnDestroy
		 * or passing the bDestroy:true parameter in the initialisation object. This
		 * can be used to remove bound events, added DOM nodes, etc.
		 *  @name DataTable#destroy.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
		 */

		/**
		 * Page length change event, fired when number of records to show on each
		 * page (the length) is changed.
		 *  @name DataTable#length.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
		 *  @param {integer} len New length
		 */

		/**
		 * Column sizing has changed.
		 *  @name DataTable#column-sizing.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
		 */

		/**
		 * Column visibility has changed.
		 *  @name DataTable#column-visibility.dt
		 *  @event
		 *  @param {event} e jQuery event object
		 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
		 *  @param {int} column Column index
		 *  @param {bool} vis `false` if column now hidden, or `true` if visible
		 */

		return $.fn.dataTable;
	}));


/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(52);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(4)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../css-loader/index.js!./jquery.dataTables.css", function() {
				var newContent = require("!!./../../../css-loader/index.js!./jquery.dataTables.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
	 * Toastr
	 * Version 2.0.1
	 * Copyright 2012 John Papa and Hans Fjällemark.
	 * All Rights Reserved.
	 * Use, reproduction, distribution, and modification of this code is subject to the terms and
	 * conditions of the MIT license, available at http://www.opensource.org/licenses/mit-license.php
	 *
	 * Author: John Papa and Hans Fjällemark
	 * Project: https://github.com/CodeSeven/toastr
	 */
	;(function (define) {
	    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1)], __WEBPACK_AMD_DEFINE_RESULT__ = function ($) {
	        return function () {
	            var version = '2.0.1';
	            var $container;
	            var listener;
	            var toastId = 0;
	            var toastType = {
	                error: 'error',
	                info: 'info',
	                success: 'success',
	                warning: 'warning'
	            };

	            var toastr = {
	                clear: clear,
	                error: error,
	                getContainer: getContainer,
	                info: info,
	                options: {},
	                subscribe: subscribe,
	                success: success,
	                version: version,
	                warning: warning
	            };

	            return toastr;

	            //#region Accessible Methods
	            function error(message, title, optionsOverride) {
	                return notify({
	                    type: toastType.error,
	                    iconClass: getOptions().iconClasses.error,
	                    message: message,
	                    optionsOverride: optionsOverride,
	                    title: title
	                });
	            }

	            function info(message, title, optionsOverride) {
	                return notify({
	                    type: toastType.info,
	                    iconClass: getOptions().iconClasses.info,
	                    message: message,
	                    optionsOverride: optionsOverride,
	                    title: title
	                });
	            }

	            function subscribe(callback) {
	                listener = callback;
	            }

	            function success(message, title, optionsOverride) {
	                return notify({
	                    type: toastType.success,
	                    iconClass: getOptions().iconClasses.success,
	                    message: message,
	                    optionsOverride: optionsOverride,
	                    title: title
	                });
	            }

	            function warning(message, title, optionsOverride) {
	                return notify({
	                    type: toastType.warning,
	                    iconClass: getOptions().iconClasses.warning,
	                    message: message,
	                    optionsOverride: optionsOverride,
	                    title: title
	                });
	            }

	            function clear($toastElement) {
	                var options = getOptions();
	                if (!$container) {
	                    getContainer(options);
	                }
	                if ($toastElement && $(':focus', $toastElement).length === 0) {
	                    $toastElement[options.hideMethod]({
	                        duration: options.hideDuration,
	                        easing: options.hideEasing,
	                        complete: function () {
	                            removeToast($toastElement);
	                        }
	                    });
	                    return;
	                }
	                if ($container.children().length) {
	                    $container[options.hideMethod]({
	                        duration: options.hideDuration,
	                        easing: options.hideEasing,
	                        complete: function () {
	                            $container.remove();
	                        }
	                    });
	                }
	            }
	            //#endregion

	            //#region Internal Methods

	            function getDefaults() {
	                return {
	                    tapToDismiss: true,
	                    toastClass: 'toast',
	                    containerId: 'toast-container',
	                    debug: false,

	                    showMethod: 'fadeIn', //fadeIn, slideDown, and show are built into jQuery
	                    showDuration: 300,
	                    showEasing: 'swing', //swing and linear are built into jQuery
	                    onShown: undefined,
	                    hideMethod: 'fadeOut',
	                    hideDuration: 1000,
	                    hideEasing: 'swing',
	                    onHidden: undefined,

	                    extendedTimeOut: 1000,
	                    iconClasses: {
	                        error: 'toast-error',
	                        info: 'toast-info',
	                        success: 'toast-success',
	                        warning: 'toast-warning'
	                    },
	                    iconClass: 'toast-info',
	                    positionClass: 'toast-top-right',
	                    timeOut: 5000, // Set timeOut and extendedTimeout to 0 to make it sticky
	                    titleClass: 'toast-title',
	                    messageClass: 'toast-message',
	                    target: 'body',
	                    closeHtml: '<button>&times;</button>',
	                    newestOnTop: true
	                };
	            }

	            function publish(args) {
	                if (!listener) {
	                    return;
	                }
	                listener(args);
	            }

	            function notify(map) {
	                var options = getOptions(),
	                    iconClass = map.iconClass || options.iconClass;

	                if (typeof map.optionsOverride !== 'undefined') {
	                    options = $.extend(options, map.optionsOverride);
	                    iconClass = map.optionsOverride.iconClass || iconClass;
	                }

	                toastId++;

	                $container = getContainer(options);
	                var intervalId = null,
	                    $toastElement = $('<div/>'),
	                    $titleElement = $('<div/>'),
	                    $messageElement = $('<div/>'),
	                    $closeElement = $(options.closeHtml),
	                    response = {
	                    toastId: toastId,
	                    state: 'visible',
	                    startTime: new Date(),
	                    options: options,
	                    map: map
	                };

	                if (map.iconClass) {
	                    $toastElement.addClass(options.toastClass).addClass(iconClass);
	                }

	                if (map.title) {
	                    $titleElement.append(map.title).addClass(options.titleClass);
	                    $toastElement.append($titleElement);
	                }

	                if (map.message) {
	                    $messageElement.append(map.message).addClass(options.messageClass);
	                    $toastElement.append($messageElement);
	                }

	                if (options.closeButton) {
	                    $closeElement.addClass('toast-close-button');
	                    $toastElement.prepend($closeElement);
	                }

	                $toastElement.hide();
	                if (options.newestOnTop) {
	                    $container.prepend($toastElement);
	                } else {
	                    $container.append($toastElement);
	                }

	                $toastElement[options.showMethod]({ duration: options.showDuration, easing: options.showEasing, complete: options.onShown });
	                if (options.timeOut > 0) {
	                    intervalId = setTimeout(hideToast, options.timeOut);
	                }

	                $toastElement.hover(stickAround, delayedhideToast);
	                if (!options.onclick && options.tapToDismiss) {
	                    $toastElement.click(hideToast);
	                }
	                if (options.closeButton && $closeElement) {
	                    $closeElement.click(function (event) {
	                        if (event.stopPropagation) {
	                            event.stopPropagation();
	                        } else if (event.cancelBubble !== undefined && event.cancelBubble !== true) {
	                            event.cancelBubble = true;
	                        }
	                        hideToast(true);
	                    });
	                }

	                if (options.onclick) {
	                    $toastElement.click(function () {
	                        options.onclick();
	                        hideToast();
	                    });
	                }

	                publish(response);

	                if (options.debug && console) {
	                    console.log(response);
	                }

	                return $toastElement;

	                function hideToast(override) {
	                    if ($(':focus', $toastElement).length && !override) {
	                        return;
	                    }
	                    return $toastElement[options.hideMethod]({
	                        duration: options.hideDuration,
	                        easing: options.hideEasing,
	                        complete: function () {
	                            removeToast($toastElement);
	                            if (options.onHidden) {
	                                options.onHidden();
	                            }
	                            response.state = 'hidden';
	                            response.endTime = new Date(), publish(response);
	                        }
	                    });
	                }

	                function delayedhideToast() {
	                    if (options.timeOut > 0 || options.extendedTimeOut > 0) {
	                        intervalId = setTimeout(hideToast, options.extendedTimeOut);
	                    }
	                }

	                function stickAround() {
	                    clearTimeout(intervalId);
	                    $toastElement.stop(true, true)[options.showMethod]({ duration: options.showDuration, easing: options.showEasing });
	                }
	            }
	            function getContainer(options) {
	                if (!options) {
	                    options = getOptions();
	                }
	                $container = $('#' + options.containerId);
	                if ($container.length) {
	                    return $container;
	                }
	                $container = $('<div/>').attr('id', options.containerId).addClass(options.positionClass);
	                $container.appendTo($(options.target));
	                return $container;
	            }

	            function getOptions() {
	                return $.extend({}, getDefaults(), toastr.options);
	            }

	            function removeToast($toastElement) {
	                if (!$container) {
	                    $container = getContainer();
	                }
	                if ($toastElement.is(':visible')) {
	                    return;
	                }
	                $toastElement.remove();
	                $toastElement = null;
	                if ($container.children().length === 0) {
	                    $container.remove();
	                }
	            }
	            //#endregion
	        }();
	    }.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	})(__webpack_require__(103));

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	var toastr = __webpack_require__(14);
	__webpack_require__(35);
	var c = __webpack_require__(7);

	module.exports = function (selector, updateUrl, urlAll, urlSelected, createRowCallback, mediator) {
	    var that = this;

	    this.container = $(selector);
	    this.allList = $($('.droppable-tables-group tbody')[0], that.container);
	    this.selectedList = $($('.droppable-tables-group tbody')[1], that.container);
	    this.page = 1;
	    this.loadBlock = false;
	    this.search = new (__webpack_require__(47))(that.container);
	    this.previousResult = null;

	    this.search.on('refresh', function (phrase) {
	        that.resetPageNum();
	        that.clearAllList();
	        that.page.page = 1;
	        that.loadAllDataByPage(that.page, phrase, function (isEnd) {
	            that.loadBlock = isEnd;
	        });
	    });

	    this.clearAllList = function () {
	        $('tr:not(.empty-placeholder)', that.allList).remove();
	    };

	    this.resetPageNum = function () {
	        that.page = 1;
	    };

	    // Sortable rows
	    $('.droppable-tables-group tbody').multisortable({
	        items: "tr:not(.empty-placeholder)",
	        selectedClass: "selected",
	        click: function (e) {},
	        stop: function (e, ui) {
	            var currentResult = that.serializeResultList();
	            if (JSON.stringify(that.previousResult) != JSON.stringify(currentResult)) {
	                console.log("I've been sorted.");
	                that.previousResult = currentResult;
	                that.updateServerData(currentResult);
	            } else {
	                console.log('not changed');
	            }
	        },
	        remove: function (event, ui) {
	            setTimeout(function () {
	                var length = $('tr', event.target).length;
	                if (length <= 1) {
	                    $('.empty-placeholder', event.target).show();
	                }
	            }, 1);
	        },
	        update: function (event, ui) {
	            $('.empty-placeholder', event.target).hide();
	        }
	    });

	    this.allList.sortable({
	        'connectWith': this.selectedList
	    });
	    this.selectedList.sortable({
	        'connectWith': this.allList
	    });

	    this.updateServerData = function (data) {
	        $.ajax({
	            method: "PUT",
	            url: updateUrl,
	            contentType: "application/json",
	            data: JSON.stringify(data)
	        }).done(function (result) {
	            that.deselectSelectedElements();
	            mediator.publish('showNotification', 'Updated!');
	        }).error(function (err) {
	            that.deselectSelectedElements();

	            if (err.responseJSON.errorCode == c.SALE_POINT_ATTACH_TO_ZONE_ERROR || err.responseJSON.errorCode == c.SALE_POINT_ATTACH_TO_RETAIL_CHAIN_ERROR) {
	                that.moveFromSelectedToAllByIds(err.responseJSON.errorData.ids);
	            }

	            mediator.publish('serverError', err.responseJSON);
	        });
	    };

	    this.moveFromSelectedToAllByIds = function (ids) {
	        $.each(ids, function (k, v) {
	            $('tr[data-id=' + v + ']').addClass('removed');
	        });
	        $.each(ids, function (k, v) {
	            $('tr[data-id=' + v + ']').prependTo(that.allList);
	            setTimeout(function () {
	                $('tr[data-id=' + v + ']').removeClass('removed');
	            }, 2000);
	        });

	        $('.empty-placeholder', that.allList).hide();

	        var length = $('tr', that.selectedList).length;
	        if (length <= 1) {
	            $('.empty-placeholder', that.selectedList).show();
	        }

	        that.previousResult = that.serializeResultList();
	    };

	    this.deselectSelectedElements = function () {
	        $('tr.selected:not(.empty-placeholder)', that.selectedList).each(function (k, v) {
	            $(this).removeClass('selected');
	        });

	        $('tr.selected:not(.empty-placeholder)', that.allList).each(function (k, v) {
	            $(this).removeClass('selected');
	        });
	    };

	    this.getSelectedElements = function () {
	        return $('tr.selected:not(.empty-placeholder)', that.selectedList);
	    };

	    this.serializeResultList = function () {
	        var result = [];
	        $('tr:not(.empty-placeholder)', that.selectedList).each(function (k, v) {
	            result.push({ id: $(this).data('id'), order: k });
	        });
	        return result;
	    };

	    this.createRow = createRowCallback || function (v) {
	        return $('<tr />', { 'data-id': v.id }).append($('<td />', { text: v.id })).append($('<td />', { text: v.name })).append($('<td />', {
	            html: '<a class="btn btn-xs btn-white" href="/sale-points/edit/' + v.id + '" target="_blank"><i class="entypo-eye"></i>show</a>',
	            class: 'nrw'
	        }));
	    };

	    $.ajax({
	        method: "GET",
	        url: urlSelected,
	        contentType: "application/json"
	    }).done(function (result) {
	        if (result.items.length) {
	            $('.empty-placeholder', that.selectedList).hide();
	            $.each(result.items, function (k, v) {
	                that.selectedList.append(that.createRow(v));
	            });
	            that.previousResult = that.serializeResultList();
	        } else {
	            $('.empty-placeholder', that.selectedList).show();
	        }

	        that.previousResult = that.serializeResultList();
	    }).error(function (err) {
	        mediator.publish('serverError', err.responseJSON);
	    });

	    this.loadAllDataByPage = function (pageNum, search, cb) {
	        $.ajax({
	            method: "GET",
	            url: urlAll(pageNum, search),
	            contentType: "application/json"
	        }).done(function (result) {
	            if (result.items.length) {
	                $('.empty-placeholder', that.allList).hide();
	                $.each(result.items, function (k, v) {
	                    that.allList.append(that.createRow(v));
	                });
	            } else if (that.page == 1) {
	                $('.empty-placeholder', that.allList).show();
	            }
	            if (cb) cb(result.items.length ? false : true);
	        }).error(function (err) {
	            mediator.publish('serverError', err.responseJSON);
	        });
	    };

	    that.loadAllDataByPage(that.page);

	    $('.droppable-tables-group-wrapper').scroll(function () {
	        if ($(this).scrollTop() + $(this).height() > $('table', this).height() && !that.loadBlock) {
	            that.loadBlock = true;
	            that.page++;
	            that.loadAllDataByPage(that.page, that.search.getCurrentValue(), function (isEnd) {
	                that.loadBlock = isEnd;
	            });
	        }
	    });
	};

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	var Mediator = __webpack_require__(8);

	module.exports = function (selector, url) {
	    new Mediator().installTo(this);

	    var that = this;

	    var options = {};

	    if (url) {
	        var ajax = {
	            url: url,
	            dataType: 'json',
	            quietMillis: 250,
	            data: function (term, page) {
	                // page is the one-based page number tracked by Select2
	                return {
	                    "options[searchPhrase]": term, //search term
	                    "options[pageNum]": page, // page number
	                    "options[pageTotal]": 30 // page number
	                };
	            },
	            results: function (data, page) {
	                var results = [];
	                $.each(data.items, function (index, item) {
	                    results.push({
	                        id: item.id,
	                        text: item.name
	                    });
	                });

	                var more = page * data.pagination.items_per_page_requested < data.pagination.total_items; // whether or not there are more results available

	                // notice we return the value of more so Select2 knows if more results can be loaded
	                return { results: results, more: more };
	            }
	        };

	        options.ajax = ajax;
	        options.initSelection = function (element, callback) {
	            var elementText = $(element).attr('data-init-text');
	            callback({ "text": elementText, "id": elementText });
	        };
	        options.minimumInputLength = 0;
	        options.cache = true;
	    } else {
	        options.minimumResultsForSearch = -1;
	    }

	    $(selector).select2(options).on('change', function (e) {
	        that.publish('change', e.val);
	    });
	};

/***/ },
/* 17 */,
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	var baseController = __webpack_require__(5);
	var notifications = __webpack_require__(9);
	__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"imports?this=>window!webpack-raphael\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
	__webpack_require__(78);
	var MultiSelect = __webpack_require__(49);
	var Select = __webpack_require__(16);
	__webpack_require__(36);
	__webpack_require__(37);
	__webpack_require__(93);

	module.exports = function (currentRoute) {
	    var that = this;
	    baseController.call(that, currentRoute);
	    notifications(that.mediator);

	    this.init = function () {
	        var container = $('.report-chart-container');

	        var handleDetalizationState = function (v) {
	            $('.picker', container).hide();
	            switch (v) {
	                case '1':
	                    $('.day-picker', container).show();
	                    break;
	                case '2':
	                    $('.week-picker', container).show();
	                    break;
	                case '3':
	                    $('.month-picker', container).show();
	                    break;
	            }
	        };

	        new Select($('[name="detalization"]', container)).on('change', handleDetalizationState);
	        new Select($('[name="dimension"]', container));

	        new MultiSelect($('[name="product_filter"]', container), '/api/v1/products');
	        new MultiSelect($('[name="retail_chain_filter"]', container), '/api/v1/retail-chains');
	        new MultiSelect($('[name="zone_filter"]', container), '/api/v1/zones');
	        new MultiSelect($('[name="sale_point_filter"]', container), '/api/v1/sale-points');
	        new MultiSelect($('[name="manager_filter"]', container));

	        new Morris.Line({
	            // ID of the element in which to draw the chart.
	            element: 'main-chart',
	            // Chart data records -- each entry in this array corresponds to a point on
	            // the chart.
	            data: [{ year: '2008', value: 20 }, { year: '2009', value: 10 }, { year: '2010', value: 5 }, { year: '2011', value: 5 }, { year: '2012', value: 20 }],
	            // The name of the data record attribute that contains x-values.
	            xkey: 'year',
	            // A list of names of data record attributes that contain y-values.
	            ykeys: ['value'],
	            // Labels for the ykeys -- will be displayed when you hover over the
	            // chart.
	            labels: ['Value']
	        });

	        $('.btn-clear-select', container).on('click', function () {
	            $('[name="' + $(this).attr('for') + '"]', container).select2('val', '');
	        });

	        $('.day-picker', container).dateRangePicker();
	        $('.week-picker', container).dateRangePicker({
	            batchMode: 'week-range',
	            showShortcuts: false,
	            showWeekNumbers: true
	        });
	        $('.month-picker', container).dateRangePicker({
	            batchMode: 'month-range',
	            showShortcuts: false,
	            showWeekNumbers: true
	        });

	        handleDetalizationState($('[name="detalization"]', container).val());
	    };
	};

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	var baseController = __webpack_require__(5);

	module.exports = function (currentRoute) {
	    var that = this;
	    baseController.call(that, currentRoute);

	    this.init = function () {
	        __webpack_require__(84);
	        jQuery.extend(window, __webpack_require__(80));
	    };
	};

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	var baseController = __webpack_require__(5);
	var crudMixin = __webpack_require__(10);
	var DatatableModule = __webpack_require__(42);
	var Form = __webpack_require__(11);
	var DroppableTables = __webpack_require__(15);
	var notifications = __webpack_require__(9);

	module.exports = function (currentRoute) {
	    var that = this;
	    baseController.call(that, currentRoute);

	    crudMixin.call(that, { 'all-table-id': 'all-retail-chains-table' });
	    notifications(that.mediator);

	    this.all = function () {
	        var datatable = new DatatableModule("all-priority-lists-table");
	    };

	    this.add = function () {
	        var form = new Form(null, '#priority-list-general-form', '/priority-lists', '/api/v1/priority-lists', 'create', that.mediator);
	    };

	    this.edit = function (id) {
	        var form = new Form(id, '#priority-list-general-form', '/priority-lists', '/api/v1/priority-lists', 'update', that.mediator);

	        new DroppableTables('#products', "/api/v1/priority-lists/" + id + "/attach-products", function (pageNum, search) {
	            return "/api/v1/products?options[notPriorityListId]=" + id + "&options[pageNum]=" + (pageNum ? pageNum : 1) + "&options[perPage]=10" + (search ? '&options[searchPhrase]=' + search : '');
	        }, "/api/v1/products?options[priorityListId]=" + id, function (v) {
	            return $('<tr />', { 'data-id': v.id }).append($('<td />', { text: v.id })).append($('<td />', { text: v.name })).append($('<td />', {
	                html: '<a class="btn btn-xs btn-white" href="/products/edit/' + v.id + '" target="_blank"><i class="entypo-eye"></i>show</a>',
	                class: 'nrw'
	            }));
	        }, that.mediator);
	    };

	    this.init = function () {
	        that.route();
	    };
	};

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	var baseController = __webpack_require__(5);
	var crudMixin = __webpack_require__(10);
	var DatatableModule = __webpack_require__(43);
	var FileUploader = __webpack_require__(26);
	var notifications = __webpack_require__(9);
	var Form = __webpack_require__(11);

	module.exports = function (currentRoute) {
	    var that = this;
	    baseController.call(that, currentRoute);
	    crudMixin.call(that, { 'all-table-id': 'all-products-table' });
	    notifications(that.mediator);

	    this.all = function () {
	        new DatatableModule("all-products-table");
	    };

	    this.initPhotoUploader = function (form) {
	        var fileuploader = new FileUploader('#product-general-form', '#photo', '/products/photo', 'photo');

	        fileuploader.on('success', function (data) {
	            form.addField('photo_id', data.id);
	            $('.img-thumbnail', '#product-general-form').attr('src', data.thumbnail_url);
	        });

	        fileuploader.on('error', function (e) {
	            if (e.message && e.message.photo) {
	                that.mediator.publish('showNotification', e.message.photo, null, false);
	            }
	        });

	        fileuploader.on('progress', function (percentage) {});
	    };

	    this.add = function () {
	        var form = new Form(null, '#product-general-form', '/products', '/products', 'create', that.mediator);
	        that.initPhotoUploader(form);
	    };

	    this.edit = function (id) {
	        var form = new Form(id, '#product-general-form', '/products', '/products', 'update', that.mediator);
	        that.initPhotoUploader(form);
	    };

	    this.init = function () {
	        that.route();
	    };
	};

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	var baseController = __webpack_require__(5);
	var crudMixin = __webpack_require__(10);
	var Form = __webpack_require__(11);
	var DroppableTables = __webpack_require__(15);
	var notifications = __webpack_require__(9);
	var FileUploader = __webpack_require__(26);
	var AssignTable = __webpack_require__(39);

	module.exports = function (currentRoute) {
	    var that = this;
	    baseController.call(that, currentRoute);
	    crudMixin.call(that, { 'all-table-id': 'all-retail-chains-table' });
	    notifications(that.mediator);

	    this.initPhotoUploader = function (form) {
	        var fileuploader = new FileUploader('#retail-chain-general-form', '#logo', '/retail-chains/logo', 'logo');

	        fileuploader.on('success', function (data) {
	            form.addField('logo_id', data.id);
	            $('.img-thumbnail', '#retail-chain-general-form').attr('src', data.thumbnail_url);
	        });

	        fileuploader.on('error', function (e) {
	            if (e.message && e.message.photo) {
	                that.mediator.publish('showNotification', e.message.photo, null, false);
	            }
	        });

	        fileuploader.on('progress', function (percentage) {});
	    };

	    this.add = function () {
	        var form = new Form(null, '#retail-chain-general-form', '/retail-chains', '/api/v1/retail-chains', 'create', that.mediator);

	        that.initPhotoUploader(form);
	    };

	    this.edit = function (id) {
	        var form = new Form(id, '#retail-chain-general-form', '/retail-chains', '/api/v1/retail-chains', 'update', that.mediator);

	        new DroppableTables('#sale-points', "/api/v1/retail-chains/" + id + "/attach-sale-points", function (pageNum, search) {
	            return "/api/v1/sale-points?options[emptyRetailChain]=1&options[pageNum]=" + (pageNum ? pageNum : 1) + "&options[perPage]=10" + (search ? '&options[searchPhrase]=' + search : '');
	        }, "/api/v1/sale-points?options[retailChainId]=" + id, null, that.mediator);

	        that.initPhotoUploader(form);
	        new AssignTable('.table-assign', id, that.mediator);
	    };

	    this.init = function () {
	        that.route();
	    };
	};

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	var baseController = __webpack_require__(5);
	var crudMixin = __webpack_require__(10);
	var Form = __webpack_require__(48);
	var DatatableModule = __webpack_require__(44);
	var notifications = __webpack_require__(9);

	module.exports = function (currentRoute) {
	    var that = this;
	    baseController.call(that, currentRoute);
	    crudMixin.call(that, { 'all-table-id': 'all-sale-points-table' });
	    notifications(that.mediator);

	    this.all = function () {
	        new DatatableModule("all-sale-points-table");
	    };

	    this.add = function () {
	        new Form(null, 'create', that.mediator);
	    };

	    this.edit = function (id) {
	        new Form(id, 'update', that.mediator);
	    };

	    this.import = function () {
	        console.log(55555555555);
	    };

	    this.init = function () {
	        that.route();
	    };
	};

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	var baseController = __webpack_require__(5);
	var crudMixin = __webpack_require__(10);
	var DroppableTables = __webpack_require__(15);
	var notifications = __webpack_require__(9);
	var DatatableModule = __webpack_require__(45);
	var AssignTable = __webpack_require__(40);
	var Form = __webpack_require__(11);

	module.exports = function (currentRoute) {
	    var that = this;
	    baseController.call(that, currentRoute);
	    crudMixin.call(that, { 'all-table-id': 'all-zones-table' });
	    notifications(that.mediator);

	    this.all = function () {
	        var datatable = new DatatableModule("all-zones-table");
	    };

	    this.add = function () {
	        new Form(null, '#zone-general-form', '/zones', '/api/v1/zones', 'create', that.mediator);
	    };

	    this.edit = function (id) {
	        new Form(id, '#zone-general-form', '/zones', '/api/v1/zones', 'update', that.mediator);
	        new DroppableTables('#sale-points', "/api/v1/zones/" + id + "/attach-sale-points", function (pageNum, search) {
	            return "/api/v1/sale-points?options[emptyZone]=1&options[pageNum]=" + (pageNum ? pageNum : 1) + "&options[perPage]=10" + (search ? '&options[searchPhrase]=' + search : '');
	        }, "/api/v1/sale-points?options[zoneId]=" + id, function (v) {
	            return $('<tr />', { 'data-id': v.id }).append($('<td />', { text: v.id })).append($('<td />', { text: v.name })).append($('<td />', {
	                html: '<a class="btn btn-xs btn-white" href="/sale-points/edit/' + v.id + '" target="_blank"><i class="entypo-eye"></i>show</a>',
	                class: 'nrw'
	            }));
	        }, that.mediator);
	        new AssignTable('.table-assign', id, that.mediator);
	    };

	    this.init = function () {
	        that.route();
	    };
	};

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	var Mediator = __webpack_require__(8);
	var Select = __webpack_require__(16);
	var c = __webpack_require__(7);

	module.exports = function (containerSelector, id, mediator) {
	    new Mediator().installTo(this);

	    var that = this;
	    this.container = $(containerSelector);

	    new Select(that.firstSelectSelector, that.firstSource);
	    new Select(that.secondSelectSelector, that.secondSource);

	    this.serializeInputFields = function () {
	        return $('.input-fields input', that.container).serializeArray();
	    };

	    this.clearForm = function () {
	        $('.input-fields input', that.container).select2("val", "");
	    };

	    this.showEmpty = function () {
	        $('.empty', that.container).show();
	    };

	    this.hideEmpty = function () {
	        $('.empty', that.container).hide();
	    };

	    this.attachRow = function (v) {
	        that.hideEmpty();
	        var row = $('<tr \>').append($('<td \>', {
	            html: v[that.firstObjectName].name + ' <a href="' + that.firstSource + '/' + v[that.firstObjectName].id + '" target="_blank">[show]</a>',
	            class: 'item'
	        })).append($('<td \>', {
	            html: v[that.secondObjectName].name + ' <a href="' + that.secondSource + '/' + v[that.secondObjectName].id + '" target="_blank">[show]</a>',
	            class: 'item'
	        })).append($('<td \>', { html: '<button class="btn btn-danger remove-button"><i class="entypo-trash"></i> remove</button>' }).on('click', function () {
	            that.removeAssign(v.id, function () {
	                row.remove();
	                if ($('tbody tr', that.container).length == 1) {
	                    that.showEmpty();
	                }
	            });
	        }));
	        $('tbody', that.container).append(row);
	    };

	    this.addAssign = function (scb) {
	        var values = that.serializeInputFields();

	        if (!values[0].value.length) {
	            mediator.publish('showNotification', that.firstParamName + ' parameter is required.', null, false);
	            return;
	        }

	        if (!values[1].value.length) {
	            mediator.publish('showNotification', that.secondParamName + ' parameter is required.', null, false);
	            return;
	        }

	        var data = {};
	        data[that.firstParamIdName] = values[0].value;
	        data[that.secondParamIdName] = values[1].value;

	        $.ajax({
	            method: "POST",
	            url: that.assignmentSource + "/" + id + "/priority-list-assigns",
	            contentType: "application/json",
	            data: JSON.stringify(data)
	        }).done(function (result) {
	            mediator.publish('showNotification', 'Assign was created with success.');
	            that.clearForm();
	            that.attachRow(result);
	        }).error(function (err) {
	            mediator.publish('serverError', err.responseJSON);
	        });
	    };

	    this.removeAssign = function (assignId, scb) {

	        if (!confirm('Are you sure?')) {
	            return;
	        }

	        $.ajax({
	            method: "DELETE",
	            url: that.assignmentSource + "/priority-list-assigns/" + assignId,
	            contentType: "application/json"
	        }).done(function (result) {
	            scb();
	            mediator.publish('showNotification', 'Removed with success');
	        }).error(function (err) {

	            if (err.responseJSON.errorCode == c.ROUTE_NOT_FOUND) {
	                scb();
	            }

	            mediator.publish('serverError', err.responseJSON);
	        });
	    };

	    $('.assign-button', that.container).on('click', function () {
	        that.addAssign();
	    });

	    $.ajax({
	        method: "GET",
	        url: that.assignmentSource + "/" + id + "/priority-list-assigns",
	        contentType: "application/json"
	    }).done(function (result) {
	        if (result.length) {
	            $.each(result, function (k, v) {
	                that.attachRow(v);
	            });
	        } else {}
	    }).error(function (err) {});
	};

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	var Mediator = __webpack_require__(8);

	module.exports = function (containerSelector, uploadInputSelector, url, name) {
	    new Mediator().installTo(this);

	    var that = this;
	    this.container = $(containerSelector);

	    this.error = null;

	    $(that.container).on('change', uploadInputSelector, function () {
	        if (this.files && this.files[0]) {
	            var file = this.files[0];
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                var error = e.target.error;
	                if (error != null) {
	                    switch (error.code) {
	                        case error.ENCODING_ERR:
	                            error.message = 'Ошибка парсинга кодировки файла.';
	                            break;

	                        case error.NOT_FOUND_ERR:
	                            error.message = 'Файл не найден.';
	                            break;

	                        case error.NOT_READABLE_ERR:
	                            error.message = 'Файл не может быть прочитан.';
	                            break;

	                        default:
	                            error.message = 'Не изестная ошибка файла.';
	                    }
	                    that.publish('error', error);
	                } else {
	                    that.upload(e.target.result, file);
	                }
	            };
	            reader.readAsBinaryString(file);
	        }
	    });

	    this.upload = function (data) {

	        that.publish('beforeUpload');

	        var xhr = new XMLHttpRequest();

	        xhr.upload.addEventListener("progress", function (event) {
	            var percentage = event.loaded * 100 / event.total;
	            that.publish('progress', Math.floor(percentage));
	        }, false);

	        xhr.onreadystatechange = function () {

	            if (this.readyState == 4) {
	                if (this.status == 200) {
	                    that.publish('success', JSON.parse(this.response));
	                } else {
	                    that.publish('error', { message: JSON.parse(this.responseText)['errorData'] });
	                }
	                that.publish('afterUpload');
	            }
	        };

	        xhr.open("POST", url);

	        // Составляем заголовки и тело запроса к серверу,  в котором и отправим файл.
	        var boundary = "xxxxxxxxx";
	        // Устанавливаем заголовки.
	        xhr.setRequestHeader('Content-type', 'multipart/form-data; boundary="' + boundary + '"');
	        xhr.setRequestHeader('Cache-Control', 'no-cache');

	        // Формируем тело запроса.
	        var body = "--" + boundary + "\r\n";
	        body += "Content-Disposition: form-data; name='" + name + "'; filename='" + name + "'\r\n";
	        body += "Content-Type: application/octet-stream\r\n\r\n";
	        body += data + "\r\n";
	        body += "--" + boundary + "--";

	        // Пилюля от слабоумия для Chrome, который гад портит файлы в процессе загрузки.
	        if (!XMLHttpRequest.prototype.sendAsBinary) {
	            XMLHttpRequest.prototype.sendAsBinary = function (datastr) {
	                function byteValue(x) {
	                    return x.charCodeAt(0) & 0xff;
	                }
	                var ords = Array.prototype.map.call(datastr, byteValue);
	                var ui8a = new Uint8Array(ords);
	                this.send(ui8a.buffer);
	            };
	        }

	        if (xhr.sendAsBinary) {
	            xhr.sendAsBinary(body);
	        } else {
	            xhr.send(body);
	        }
	    };
	};

/***/ },
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */
/***/ function(module, exports) {

	/**
	 * Debounce and throttle function's decorator plugin 1.0.5
	 *
	 * Copyright (c) 2009 Filatov Dmitry (alpha@zforms.ru)
	 * Dual licensed under the MIT and GPL licenses:
	 * http://www.opensource.org/licenses/mit-license.php
	 * http://www.gnu.org/licenses/gpl.html
	 *
	 */

	(function ($) {

		$.extend({

			debounce: function (fn, timeout, invokeAsap, ctx) {

				if (arguments.length == 3 && typeof invokeAsap != 'boolean') {
					ctx = invokeAsap;
					invokeAsap = false;
				}

				var timer;

				return function () {

					var args = arguments;
					ctx = ctx || this;

					invokeAsap && !timer && fn.apply(ctx, args);

					clearTimeout(timer);

					timer = setTimeout(function () {
						!invokeAsap && fn.apply(ctx, args);
						timer = null;
					}, timeout);
				};
			},

			throttle: function (fn, timeout, ctx) {

				var timer, args, needInvoke;

				return function () {

					args = arguments;
					needInvoke = true;
					ctx = ctx || this;

					if (!timer) {
						(function () {
							if (needInvoke) {
								fn.apply(ctx, args);
								needInvoke = false;
								timer = setTimeout(arguments.callee, timeout);
							} else {
								timer = null;
							}
						})();
					}
				};
			}

		});
	})(jQuery);

/***/ },
/* 35 */
/***/ function(module, exports) {

	/**
	 * jquery.multisortable.js - v0.2
	 * https://github.com/shvetsgroup/jquery.multisortable
	 *
	 * Author: Ethan Atlakson, Jay Hayes, Gabriel Such, Alexander Shvets
	 * Last Revision 3/16/2012
	 * multi-selectable, multi-sortable jQuery plugin
	 */

	!function ($) {

	    $.fn.multiselectable = function (options) {
	        if (!options) {
	            options = {};
	        }
	        options = $.extend({}, $.fn.multiselectable.defaults, options);

	        function mouseDown(e) {
	            var item = $(this),
	                parent = item.parent(),
	                myIndex = item.index();

	            var prev = parent.find('.multiselectable-previous');
	            // If no previous selection found, start selecting from first selected item.
	            prev = prev.length ? prev : $(parent.find('.' + options.selectedClass)[0]).addClass('multiselectable-previous');
	            var prevIndex = prev.index();

	            if (e.ctrlKey || e.metaKey) {
	                if (item.hasClass(options.selectedClass)) {
	                    item.removeClass(options.selectedClass).removeClass('multiselectable-previous');
	                    if (item.not('.child').length) {
	                        item.nextUntil(':not(.child)').removeClass(options.selectedClass);
	                    }
	                } else {
	                    parent.find('.multiselectable-previous').removeClass('multiselectable-previous');
	                    item.addClass(options.selectedClass).addClass('multiselectable-previous');
	                    if (item.not('.child').length) {
	                        item.nextUntil(':not(.child)').addClass(options.selectedClass);
	                    }
	                }
	            }

	            if (e.shiftKey) {
	                var last_shift_range = parent.find('.multiselectable-shift');
	                last_shift_range.removeClass(options.selectedClass).removeClass('multiselectable-shift');

	                var shift_range;
	                if (prevIndex < myIndex) {
	                    shift_range = item.prevUntil('.multiselectable-previous').add(prev).add(item);
	                } else if (prevIndex > myIndex) {
	                    shift_range = item.nextUntil('.multiselectable-previous').add(prev).add(item);
	                }
	                shift_range.addClass(options.selectedClass).addClass('multiselectable-shift');
	            } else {
	                parent.find('.multiselectable-shift').removeClass('multiselectable-shift');
	            }

	            if (!e.ctrlKey && !e.metaKey && !e.shiftKey) {
	                parent.find('.multiselectable-previous').removeClass('multiselectable-previous');
	                if (!item.hasClass(options.selectedClass)) {
	                    parent.find('.' + options.selectedClass).removeClass(options.selectedClass);
	                    item.addClass(options.selectedClass).addClass('multiselectable-previous');
	                    if (item.not('.child').length) {
	                        item.nextUntil(':not(.child)').addClass(options.selectedClass);
	                    }
	                }
	            }

	            options.mousedown(e, item);
	        }

	        function click(e) {
	            if ($(this).is('.ui-draggable-dragging')) {
	                return;
	            }

	            var item = $(this),
	                parent = item.parent();

	            // If item wasn't draged and is not multiselected, it should reset selection for other items.
	            if (!e.ctrlKey && !e.metaKey && !e.shiftKey) {
	                parent.find('.multiselectable-previous').removeClass('multiselectable-previous');
	                parent.find('.' + options.selectedClass).removeClass(options.selectedClass);
	                item.addClass(options.selectedClass).addClass('multiselectable-previous');
	                if (item.not('.child').length) {
	                    item.nextUntil(':not(.child)').addClass(options.selectedClass);
	                }
	            }

	            options.click(e, item);
	        }

	        return this.each(function () {
	            var list = $(this);

	            if (!list.data('multiselectable')) {
	                list.data('multiselectable', true).delegate(options.items, 'mousedown', mouseDown).delegate(options.items, 'click', click).disableSelection();
	            }
	        });
	    };

	    $.fn.multiselectable.defaults = {
	        click: function (event, elem) {},
	        mousedown: function (event, elem) {},
	        selectedClass: 'selected',
	        items: 'li'
	    };

	    $.fn.multisortable = function (options) {
	        if (!options) {
	            options = {};
	        }
	        var settings = $.extend({}, $.fn.multisortable.defaults, options);

	        function regroup(item, list) {
	            if (list.find('.' + settings.selectedClass).length > 0) {
	                var myIndex = item.data('i');

	                var itemsBefore = list.find('.' + settings.selectedClass).filter(function () {
	                    return $(this).data('i') < myIndex;
	                }).css({
	                    position: '',
	                    width: '',
	                    left: '',
	                    top: '',
	                    zIndex: ''
	                });

	                item.before(itemsBefore);

	                var itemsAfter = list.find('.' + settings.selectedClass).filter(function () {
	                    return $(this).data('i') > myIndex;
	                }).css({
	                    position: '',
	                    width: '',
	                    left: '',
	                    top: '',
	                    zIndex: ''
	                });

	                item.after(itemsAfter);

	                setTimeout(function () {
	                    itemsAfter.add(itemsBefore).addClass(settings.selectedClass);
	                }, 0);
	            }
	        }

	        return this.each(function () {
	            var list = $(this);

	            //enable multi-selection
	            list.multiselectable({
	                selectedClass: settings.selectedClass,
	                click: settings.click,
	                items: settings.items,
	                mousedown: settings.mousedown
	            });

	            //enable sorting
	            options.cancel = settings.items + ':not(.' + settings.selectedClass + ')';
	            options.placeholder = settings.placeholder;
	            options.start = function (event, ui) {
	                if (ui.item.hasClass(settings.selectedClass)) {
	                    var parent = ui.item.parent();

	                    //assign indexes to all selected items
	                    parent.find('.' + settings.selectedClass).each(function (i) {
	                        $(this).data('i', i);
	                    });

	                    // adjust placeholder size to be size of items
	                    var height = parent.find('.' + settings.selectedClass).length * ui.item.outerHeight();
	                    ui.placeholder.height(height);
	                }

	                settings.start(event, ui);
	            };

	            options.stop = function (event, ui) {
	                regroup(ui.item, ui.item.parent());
	                settings.stop(event, ui);
	            };

	            options.sort = function (event, ui) {
	                var parent = ui.item.parent(),
	                    myIndex = ui.item.data('i'),
	                    top = parseInt(ui.item.css('top').replace('px', '')),
	                    left = parseInt(ui.item.css('left').replace('px', ''));

	                // fix to keep compatibility using prototype.js and jquery together
	                $.fn.reverse = Array.prototype._reverse || Array.prototype.reverse;

	                var height = 0;
	                $('.' + settings.selectedClass, parent).filter(function () {
	                    return $(this).data('i') < myIndex;
	                }).reverse().each(function () {
	                    height += $(this).outerHeight();
	                    $(this).css({
	                        left: left,
	                        top: top - height,
	                        position: 'absolute',
	                        zIndex: 1000,
	                        width: ui.item.width()
	                    });
	                });

	                height = ui.item.outerHeight();
	                $('.' + settings.selectedClass, parent).filter(function () {
	                    return $(this).data('i') > myIndex;
	                }).each(function () {
	                    var item = $(this);
	                    item.css({
	                        left: left,
	                        top: top + height,
	                        position: 'absolute',
	                        zIndex: 1000,
	                        width: ui.item.width()
	                    });

	                    height += item.outerHeight();
	                });

	                settings.sort(event, ui);
	            };

	            options.receive = function (event, ui) {
	                regroup(ui.item, ui.sender);
	                settings.receive(event, ui);
	            };

	            list.sortable(options).disableSelection();
	        });
	    };

	    $.fn.multisortable.defaults = {
	        start: function (event, ui) {},
	        stop: function (event, ui) {},
	        sort: function (event, ui) {},
	        receive: function (event, ui) {},
	        click: function (event, elem) {},
	        mousedown: function (event, elem) {},
	        selectedClass: 'selected',
	        placeholder: 'placeholder',
	        items: 'li'
	    };
	}(jQuery);

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(module) {//! moment.js
	//! version : 2.13.0
	//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
	//! license : MIT
	//! momentjs.com
	;(function(global,factory){ true?module.exports=factory():typeof define==='function'&&define.amd?define(factory):global.moment=factory();})(this,function(){'use strict';var hookCallback;function utils_hooks__hooks(){return hookCallback.apply(null,arguments);} // This is done to register the method called with moment()
	// without creating circular dependencies.
	function setHookCallback(callback){hookCallback=callback;}function isArray(input){return input instanceof Array||Object.prototype.toString.call(input)==='[object Array]';}function isDate(input){return input instanceof Date||Object.prototype.toString.call(input)==='[object Date]';}function map(arr,fn){var res=[],i;for(i=0;i<arr.length;++i){res.push(fn(arr[i],i));}return res;}function hasOwnProp(a,b){return Object.prototype.hasOwnProperty.call(a,b);}function extend(a,b){for(var i in b){if(hasOwnProp(b,i)){a[i]=b[i];}}if(hasOwnProp(b,'toString')){a.toString=b.toString;}if(hasOwnProp(b,'valueOf')){a.valueOf=b.valueOf;}return a;}function create_utc__createUTC(input,format,locale,strict){return createLocalOrUTC(input,format,locale,strict,true).utc();}function defaultParsingFlags(){ // We need to deep clone this object.
	return {empty:false,unusedTokens:[],unusedInput:[],overflow:-2,charsLeftOver:0,nullInput:false,invalidMonth:null,invalidFormat:false,userInvalidated:false,iso:false,parsedDateParts:[],meridiem:null};}function getParsingFlags(m){if(m._pf==null){m._pf=defaultParsingFlags();}return m._pf;}var some;if(Array.prototype.some){some=Array.prototype.some;}else {some=function(fun){var t=Object(this);var len=t.length>>>0;for(var i=0;i<len;i++){if(i in t&&fun.call(this,t[i],i,t)){return true;}}return false;};}function valid__isValid(m){if(m._isValid==null){var flags=getParsingFlags(m);var parsedParts=some.call(flags.parsedDateParts,function(i){return i!=null;});m._isValid=!isNaN(m._d.getTime())&&flags.overflow<0&&!flags.empty&&!flags.invalidMonth&&!flags.invalidWeekday&&!flags.nullInput&&!flags.invalidFormat&&!flags.userInvalidated&&(!flags.meridiem||flags.meridiem&&parsedParts);if(m._strict){m._isValid=m._isValid&&flags.charsLeftOver===0&&flags.unusedTokens.length===0&&flags.bigHour===undefined;}}return m._isValid;}function valid__createInvalid(flags){var m=create_utc__createUTC(NaN);if(flags!=null){extend(getParsingFlags(m),flags);}else {getParsingFlags(m).userInvalidated=true;}return m;}function isUndefined(input){return input===void 0;} // Plugins that add properties should also add the key here (null value),
	// so we can properly clone ourselves.
	var momentProperties=utils_hooks__hooks.momentProperties=[];function copyConfig(to,from){var i,prop,val;if(!isUndefined(from._isAMomentObject)){to._isAMomentObject=from._isAMomentObject;}if(!isUndefined(from._i)){to._i=from._i;}if(!isUndefined(from._f)){to._f=from._f;}if(!isUndefined(from._l)){to._l=from._l;}if(!isUndefined(from._strict)){to._strict=from._strict;}if(!isUndefined(from._tzm)){to._tzm=from._tzm;}if(!isUndefined(from._isUTC)){to._isUTC=from._isUTC;}if(!isUndefined(from._offset)){to._offset=from._offset;}if(!isUndefined(from._pf)){to._pf=getParsingFlags(from);}if(!isUndefined(from._locale)){to._locale=from._locale;}if(momentProperties.length>0){for(i in momentProperties){prop=momentProperties[i];val=from[prop];if(!isUndefined(val)){to[prop]=val;}}}return to;}var updateInProgress=false; // Moment prototype object
	function Moment(config){copyConfig(this,config);this._d=new Date(config._d!=null?config._d.getTime():NaN); // Prevent infinite loop in case updateOffset creates new moment
	// objects.
	if(updateInProgress===false){updateInProgress=true;utils_hooks__hooks.updateOffset(this);updateInProgress=false;}}function isMoment(obj){return obj instanceof Moment||obj!=null&&obj._isAMomentObject!=null;}function absFloor(number){if(number<0){return Math.ceil(number);}else {return Math.floor(number);}}function toInt(argumentForCoercion){var coercedNumber=+argumentForCoercion,value=0;if(coercedNumber!==0&&isFinite(coercedNumber)){value=absFloor(coercedNumber);}return value;} // compare two arrays, return the number of differences
	function compareArrays(array1,array2,dontConvert){var len=Math.min(array1.length,array2.length),lengthDiff=Math.abs(array1.length-array2.length),diffs=0,i;for(i=0;i<len;i++){if(dontConvert&&array1[i]!==array2[i]||!dontConvert&&toInt(array1[i])!==toInt(array2[i])){diffs++;}}return diffs+lengthDiff;}function warn(msg){if(utils_hooks__hooks.suppressDeprecationWarnings===false&&typeof console!=='undefined'&&console.warn){console.warn('Deprecation warning: '+msg);}}function deprecate(msg,fn){var firstTime=true;return extend(function(){if(utils_hooks__hooks.deprecationHandler!=null){utils_hooks__hooks.deprecationHandler(null,msg);}if(firstTime){warn(msg+'\nArguments: '+Array.prototype.slice.call(arguments).join(', ')+'\n'+new Error().stack);firstTime=false;}return fn.apply(this,arguments);},fn);}var deprecations={};function deprecateSimple(name,msg){if(utils_hooks__hooks.deprecationHandler!=null){utils_hooks__hooks.deprecationHandler(name,msg);}if(!deprecations[name]){warn(msg);deprecations[name]=true;}}utils_hooks__hooks.suppressDeprecationWarnings=false;utils_hooks__hooks.deprecationHandler=null;function isFunction(input){return input instanceof Function||Object.prototype.toString.call(input)==='[object Function]';}function isObject(input){return Object.prototype.toString.call(input)==='[object Object]';}function locale_set__set(config){var prop,i;for(i in config){prop=config[i];if(isFunction(prop)){this[i]=prop;}else {this['_'+i]=prop;}}this._config=config; // Lenient ordinal parsing accepts just a number in addition to
	// number + (possibly) stuff coming from _ordinalParseLenient.
	this._ordinalParseLenient=new RegExp(this._ordinalParse.source+'|'+/\d{1,2}/.source);}function mergeConfigs(parentConfig,childConfig){var res=extend({},parentConfig),prop;for(prop in childConfig){if(hasOwnProp(childConfig,prop)){if(isObject(parentConfig[prop])&&isObject(childConfig[prop])){res[prop]={};extend(res[prop],parentConfig[prop]);extend(res[prop],childConfig[prop]);}else if(childConfig[prop]!=null){res[prop]=childConfig[prop];}else {delete res[prop];}}}return res;}function Locale(config){if(config!=null){this.set(config);}}var keys;if(Object.keys){keys=Object.keys;}else {keys=function(obj){var i,res=[];for(i in obj){if(hasOwnProp(obj,i)){res.push(i);}}return res;};} // internal storage for locale config files
	var locales={};var globalLocale;function normalizeLocale(key){return key?key.toLowerCase().replace('_','-'):key;} // pick the locale from the array
	// try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
	// substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
	function chooseLocale(names){var i=0,j,next,locale,split;while(i<names.length){split=normalizeLocale(names[i]).split('-');j=split.length;next=normalizeLocale(names[i+1]);next=next?next.split('-'):null;while(j>0){locale=loadLocale(split.slice(0,j).join('-'));if(locale){return locale;}if(next&&next.length>=j&&compareArrays(split,next,true)>=j-1){ //the next array item is better than a shallower substring of this one
	break;}j--;}i++;}return null;}function loadLocale(name){var oldLocale=null; // TODO: Find a better way to register and load all the locales in Node
	if(!locales[name]&&typeof module!=='undefined'&&module&&module.exports){try{oldLocale=globalLocale._abbr;!(function webpackMissingModule() { var e = new Error("Cannot find module \"./locale\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()); // because defineLocale currently also sets the global locale, we
	// want to undo that for lazy loaded locales
	locale_locales__getSetGlobalLocale(oldLocale);}catch(e){}}return locales[name];} // This function will load locale and then set the global locale.  If
	// no arguments are passed in, it will simply return the current global
	// locale key.
	function locale_locales__getSetGlobalLocale(key,values){var data;if(key){if(isUndefined(values)){data=locale_locales__getLocale(key);}else {data=defineLocale(key,values);}if(data){ // moment.duration._locale = moment._locale = data;
	globalLocale=data;}}return globalLocale._abbr;}function defineLocale(name,config){if(config!==null){config.abbr=name;if(locales[name]!=null){deprecateSimple('defineLocaleOverride','use moment.updateLocale(localeName, config) to change '+'an existing locale. moment.defineLocale(localeName, '+'config) should only be used for creating a new locale');config=mergeConfigs(locales[name]._config,config);}else if(config.parentLocale!=null){if(locales[config.parentLocale]!=null){config=mergeConfigs(locales[config.parentLocale]._config,config);}else { // treat as if there is no base config
	deprecateSimple('parentLocaleUndefined','specified parentLocale is not defined yet');}}locales[name]=new Locale(config); // backwards compat for now: also set the locale
	locale_locales__getSetGlobalLocale(name);return locales[name];}else { // useful for testing
	delete locales[name];return null;}}function updateLocale(name,config){if(config!=null){var locale;if(locales[name]!=null){config=mergeConfigs(locales[name]._config,config);}locale=new Locale(config);locale.parentLocale=locales[name];locales[name]=locale; // backwards compat for now: also set the locale
	locale_locales__getSetGlobalLocale(name);}else { // pass null for config to unupdate, useful for tests
	if(locales[name]!=null){if(locales[name].parentLocale!=null){locales[name]=locales[name].parentLocale;}else if(locales[name]!=null){delete locales[name];}}}return locales[name];} // returns locale data
	function locale_locales__getLocale(key){var locale;if(key&&key._locale&&key._locale._abbr){key=key._locale._abbr;}if(!key){return globalLocale;}if(!isArray(key)){ //short-circuit everything else
	locale=loadLocale(key);if(locale){return locale;}key=[key];}return chooseLocale(key);}function locale_locales__listLocales(){return keys(locales);}var aliases={};function addUnitAlias(unit,shorthand){var lowerCase=unit.toLowerCase();aliases[lowerCase]=aliases[lowerCase+'s']=aliases[shorthand]=unit;}function normalizeUnits(units){return typeof units==='string'?aliases[units]||aliases[units.toLowerCase()]:undefined;}function normalizeObjectUnits(inputObject){var normalizedInput={},normalizedProp,prop;for(prop in inputObject){if(hasOwnProp(inputObject,prop)){normalizedProp=normalizeUnits(prop);if(normalizedProp){normalizedInput[normalizedProp]=inputObject[prop];}}}return normalizedInput;}function makeGetSet(unit,keepTime){return function(value){if(value!=null){get_set__set(this,unit,value);utils_hooks__hooks.updateOffset(this,keepTime);return this;}else {return get_set__get(this,unit);}};}function get_set__get(mom,unit){return mom.isValid()?mom._d['get'+(mom._isUTC?'UTC':'')+unit]():NaN;}function get_set__set(mom,unit,value){if(mom.isValid()){mom._d['set'+(mom._isUTC?'UTC':'')+unit](value);}} // MOMENTS
	function getSet(units,value){var unit;if(typeof units==='object'){for(unit in units){this.set(unit,units[unit]);}}else {units=normalizeUnits(units);if(isFunction(this[units])){return this[units](value);}}return this;}function zeroFill(number,targetLength,forceSign){var absNumber=''+Math.abs(number),zerosToFill=targetLength-absNumber.length,sign=number>=0;return (sign?forceSign?'+':'':'-')+Math.pow(10,Math.max(0,zerosToFill)).toString().substr(1)+absNumber;}var formattingTokens=/(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;var localFormattingTokens=/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;var formatFunctions={};var formatTokenFunctions={}; // token:    'M'
	// padded:   ['MM', 2]
	// ordinal:  'Mo'
	// callback: function () { this.month() + 1 }
	function addFormatToken(token,padded,ordinal,callback){var func=callback;if(typeof callback==='string'){func=function(){return this[callback]();};}if(token){formatTokenFunctions[token]=func;}if(padded){formatTokenFunctions[padded[0]]=function(){return zeroFill(func.apply(this,arguments),padded[1],padded[2]);};}if(ordinal){formatTokenFunctions[ordinal]=function(){return this.localeData().ordinal(func.apply(this,arguments),token);};}}function removeFormattingTokens(input){if(input.match(/\[[\s\S]/)){return input.replace(/^\[|\]$/g,'');}return input.replace(/\\/g,'');}function makeFormatFunction(format){var array=format.match(formattingTokens),i,length;for(i=0,length=array.length;i<length;i++){if(formatTokenFunctions[array[i]]){array[i]=formatTokenFunctions[array[i]];}else {array[i]=removeFormattingTokens(array[i]);}}return function(mom){var output='',i;for(i=0;i<length;i++){output+=array[i] instanceof Function?array[i].call(mom,format):array[i];}return output;};} // format date using native date object
	function formatMoment(m,format){if(!m.isValid()){return m.localeData().invalidDate();}format=expandFormat(format,m.localeData());formatFunctions[format]=formatFunctions[format]||makeFormatFunction(format);return formatFunctions[format](m);}function expandFormat(format,locale){var i=5;function replaceLongDateFormatTokens(input){return locale.longDateFormat(input)||input;}localFormattingTokens.lastIndex=0;while(i>=0&&localFormattingTokens.test(format)){format=format.replace(localFormattingTokens,replaceLongDateFormatTokens);localFormattingTokens.lastIndex=0;i-=1;}return format;}var match1=/\d/; //       0 - 9
	var match2=/\d\d/; //      00 - 99
	var match3=/\d{3}/; //     000 - 999
	var match4=/\d{4}/; //    0000 - 9999
	var match6=/[+-]?\d{6}/; // -999999 - 999999
	var match1to2=/\d\d?/; //       0 - 99
	var match3to4=/\d\d\d\d?/; //     999 - 9999
	var match5to6=/\d\d\d\d\d\d?/; //   99999 - 999999
	var match1to3=/\d{1,3}/; //       0 - 999
	var match1to4=/\d{1,4}/; //       0 - 9999
	var match1to6=/[+-]?\d{1,6}/; // -999999 - 999999
	var matchUnsigned=/\d+/; //       0 - inf
	var matchSigned=/[+-]?\d+/; //    -inf - inf
	var matchOffset=/Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
	var matchShortOffset=/Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z
	var matchTimestamp=/[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123
	// any word (or two) characters or numbers including two/three word month in arabic.
	// includes scottish gaelic two word and hyphenated months
	var matchWord=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i;var regexes={};function addRegexToken(token,regex,strictRegex){regexes[token]=isFunction(regex)?regex:function(isStrict,localeData){return isStrict&&strictRegex?strictRegex:regex;};}function getParseRegexForToken(token,config){if(!hasOwnProp(regexes,token)){return new RegExp(unescapeFormat(token));}return regexes[token](config._strict,config._locale);} // Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
	function unescapeFormat(s){return regexEscape(s.replace('\\','').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(matched,p1,p2,p3,p4){return p1||p2||p3||p4;}));}function regexEscape(s){return s.replace(/[-\/\\^$*+?.()|[\]{}]/g,'\\$&');}var tokens={};function addParseToken(token,callback){var i,func=callback;if(typeof token==='string'){token=[token];}if(typeof callback==='number'){func=function(input,array){array[callback]=toInt(input);};}for(i=0;i<token.length;i++){tokens[token[i]]=func;}}function addWeekParseToken(token,callback){addParseToken(token,function(input,array,config,token){config._w=config._w||{};callback(input,config._w,config,token);});}function addTimeToArrayFromToken(token,input,config){if(input!=null&&hasOwnProp(tokens,token)){tokens[token](input,config._a,config,token);}}var YEAR=0;var MONTH=1;var DATE=2;var HOUR=3;var MINUTE=4;var SECOND=5;var MILLISECOND=6;var WEEK=7;var WEEKDAY=8;var indexOf;if(Array.prototype.indexOf){indexOf=Array.prototype.indexOf;}else {indexOf=function(o){ // I know
	var i;for(i=0;i<this.length;++i){if(this[i]===o){return i;}}return -1;};}function daysInMonth(year,month){return new Date(Date.UTC(year,month+1,0)).getUTCDate();} // FORMATTING
	addFormatToken('M',['MM',2],'Mo',function(){return this.month()+1;});addFormatToken('MMM',0,0,function(format){return this.localeData().monthsShort(this,format);});addFormatToken('MMMM',0,0,function(format){return this.localeData().months(this,format);}); // ALIASES
	addUnitAlias('month','M'); // PARSING
	addRegexToken('M',match1to2);addRegexToken('MM',match1to2,match2);addRegexToken('MMM',function(isStrict,locale){return locale.monthsShortRegex(isStrict);});addRegexToken('MMMM',function(isStrict,locale){return locale.monthsRegex(isStrict);});addParseToken(['M','MM'],function(input,array){array[MONTH]=toInt(input)-1;});addParseToken(['MMM','MMMM'],function(input,array,config,token){var month=config._locale.monthsParse(input,token,config._strict); // if we didn't find a month name, mark the date as invalid.
	if(month!=null){array[MONTH]=month;}else {getParsingFlags(config).invalidMonth=input;}}); // LOCALES
	var MONTHS_IN_FORMAT=/D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/;var defaultLocaleMonths='January_February_March_April_May_June_July_August_September_October_November_December'.split('_');function localeMonths(m,format){return isArray(this._months)?this._months[m.month()]:this._months[MONTHS_IN_FORMAT.test(format)?'format':'standalone'][m.month()];}var defaultLocaleMonthsShort='Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');function localeMonthsShort(m,format){return isArray(this._monthsShort)?this._monthsShort[m.month()]:this._monthsShort[MONTHS_IN_FORMAT.test(format)?'format':'standalone'][m.month()];}function units_month__handleStrictParse(monthName,format,strict){var i,ii,mom,llc=monthName.toLocaleLowerCase();if(!this._monthsParse){ // this is not used
	this._monthsParse=[];this._longMonthsParse=[];this._shortMonthsParse=[];for(i=0;i<12;++i){mom=create_utc__createUTC([2000,i]);this._shortMonthsParse[i]=this.monthsShort(mom,'').toLocaleLowerCase();this._longMonthsParse[i]=this.months(mom,'').toLocaleLowerCase();}}if(strict){if(format==='MMM'){ii=indexOf.call(this._shortMonthsParse,llc);return ii!==-1?ii:null;}else {ii=indexOf.call(this._longMonthsParse,llc);return ii!==-1?ii:null;}}else {if(format==='MMM'){ii=indexOf.call(this._shortMonthsParse,llc);if(ii!==-1){return ii;}ii=indexOf.call(this._longMonthsParse,llc);return ii!==-1?ii:null;}else {ii=indexOf.call(this._longMonthsParse,llc);if(ii!==-1){return ii;}ii=indexOf.call(this._shortMonthsParse,llc);return ii!==-1?ii:null;}}}function localeMonthsParse(monthName,format,strict){var i,mom,regex;if(this._monthsParseExact){return units_month__handleStrictParse.call(this,monthName,format,strict);}if(!this._monthsParse){this._monthsParse=[];this._longMonthsParse=[];this._shortMonthsParse=[];} // TODO: add sorting
	// Sorting makes sure if one month (or abbr) is a prefix of another
	// see sorting in computeMonthsParse
	for(i=0;i<12;i++){ // make the regex if we don't have it already
	mom=create_utc__createUTC([2000,i]);if(strict&&!this._longMonthsParse[i]){this._longMonthsParse[i]=new RegExp('^'+this.months(mom,'').replace('.','')+'$','i');this._shortMonthsParse[i]=new RegExp('^'+this.monthsShort(mom,'').replace('.','')+'$','i');}if(!strict&&!this._monthsParse[i]){regex='^'+this.months(mom,'')+'|^'+this.monthsShort(mom,'');this._monthsParse[i]=new RegExp(regex.replace('.',''),'i');} // test the regex
	if(strict&&format==='MMMM'&&this._longMonthsParse[i].test(monthName)){return i;}else if(strict&&format==='MMM'&&this._shortMonthsParse[i].test(monthName)){return i;}else if(!strict&&this._monthsParse[i].test(monthName)){return i;}}} // MOMENTS
	function setMonth(mom,value){var dayOfMonth;if(!mom.isValid()){ // No op
	return mom;}if(typeof value==='string'){if(/^\d+$/.test(value)){value=toInt(value);}else {value=mom.localeData().monthsParse(value); // TODO: Another silent failure?
	if(typeof value!=='number'){return mom;}}}dayOfMonth=Math.min(mom.date(),daysInMonth(mom.year(),value));mom._d['set'+(mom._isUTC?'UTC':'')+'Month'](value,dayOfMonth);return mom;}function getSetMonth(value){if(value!=null){setMonth(this,value);utils_hooks__hooks.updateOffset(this,true);return this;}else {return get_set__get(this,'Month');}}function getDaysInMonth(){return daysInMonth(this.year(),this.month());}var defaultMonthsShortRegex=matchWord;function monthsShortRegex(isStrict){if(this._monthsParseExact){if(!hasOwnProp(this,'_monthsRegex')){computeMonthsParse.call(this);}if(isStrict){return this._monthsShortStrictRegex;}else {return this._monthsShortRegex;}}else {return this._monthsShortStrictRegex&&isStrict?this._monthsShortStrictRegex:this._monthsShortRegex;}}var defaultMonthsRegex=matchWord;function monthsRegex(isStrict){if(this._monthsParseExact){if(!hasOwnProp(this,'_monthsRegex')){computeMonthsParse.call(this);}if(isStrict){return this._monthsStrictRegex;}else {return this._monthsRegex;}}else {return this._monthsStrictRegex&&isStrict?this._monthsStrictRegex:this._monthsRegex;}}function computeMonthsParse(){function cmpLenRev(a,b){return b.length-a.length;}var shortPieces=[],longPieces=[],mixedPieces=[],i,mom;for(i=0;i<12;i++){ // make the regex if we don't have it already
	mom=create_utc__createUTC([2000,i]);shortPieces.push(this.monthsShort(mom,''));longPieces.push(this.months(mom,''));mixedPieces.push(this.months(mom,''));mixedPieces.push(this.monthsShort(mom,''));} // Sorting makes sure if one month (or abbr) is a prefix of another it
	// will match the longer piece.
	shortPieces.sort(cmpLenRev);longPieces.sort(cmpLenRev);mixedPieces.sort(cmpLenRev);for(i=0;i<12;i++){shortPieces[i]=regexEscape(shortPieces[i]);longPieces[i]=regexEscape(longPieces[i]);mixedPieces[i]=regexEscape(mixedPieces[i]);}this._monthsRegex=new RegExp('^('+mixedPieces.join('|')+')','i');this._monthsShortRegex=this._monthsRegex;this._monthsStrictRegex=new RegExp('^('+longPieces.join('|')+')','i');this._monthsShortStrictRegex=new RegExp('^('+shortPieces.join('|')+')','i');}function checkOverflow(m){var overflow;var a=m._a;if(a&&getParsingFlags(m).overflow===-2){overflow=a[MONTH]<0||a[MONTH]>11?MONTH:a[DATE]<1||a[DATE]>daysInMonth(a[YEAR],a[MONTH])?DATE:a[HOUR]<0||a[HOUR]>24||a[HOUR]===24&&(a[MINUTE]!==0||a[SECOND]!==0||a[MILLISECOND]!==0)?HOUR:a[MINUTE]<0||a[MINUTE]>59?MINUTE:a[SECOND]<0||a[SECOND]>59?SECOND:a[MILLISECOND]<0||a[MILLISECOND]>999?MILLISECOND:-1;if(getParsingFlags(m)._overflowDayOfYear&&(overflow<YEAR||overflow>DATE)){overflow=DATE;}if(getParsingFlags(m)._overflowWeeks&&overflow===-1){overflow=WEEK;}if(getParsingFlags(m)._overflowWeekday&&overflow===-1){overflow=WEEKDAY;}getParsingFlags(m).overflow=overflow;}return m;} // iso 8601 regex
	// 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
	var extendedIsoRegex=/^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/;var basicIsoRegex=/^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/;var tzRegex=/Z|[+-]\d\d(?::?\d\d)?/;var isoDates=[['YYYYYY-MM-DD',/[+-]\d{6}-\d\d-\d\d/],['YYYY-MM-DD',/\d{4}-\d\d-\d\d/],['GGGG-[W]WW-E',/\d{4}-W\d\d-\d/],['GGGG-[W]WW',/\d{4}-W\d\d/,false],['YYYY-DDD',/\d{4}-\d{3}/],['YYYY-MM',/\d{4}-\d\d/,false],['YYYYYYMMDD',/[+-]\d{10}/],['YYYYMMDD',/\d{8}/], // YYYYMM is NOT allowed by the standard
	['GGGG[W]WWE',/\d{4}W\d{3}/],['GGGG[W]WW',/\d{4}W\d{2}/,false],['YYYYDDD',/\d{7}/]]; // iso time formats and regexes
	var isoTimes=[['HH:mm:ss.SSSS',/\d\d:\d\d:\d\d\.\d+/],['HH:mm:ss,SSSS',/\d\d:\d\d:\d\d,\d+/],['HH:mm:ss',/\d\d:\d\d:\d\d/],['HH:mm',/\d\d:\d\d/],['HHmmss.SSSS',/\d\d\d\d\d\d\.\d+/],['HHmmss,SSSS',/\d\d\d\d\d\d,\d+/],['HHmmss',/\d\d\d\d\d\d/],['HHmm',/\d\d\d\d/],['HH',/\d\d/]];var aspNetJsonRegex=/^\/?Date\((\-?\d+)/i; // date from iso format
	function configFromISO(config){var i,l,string=config._i,match=extendedIsoRegex.exec(string)||basicIsoRegex.exec(string),allowTime,dateFormat,timeFormat,tzFormat;if(match){getParsingFlags(config).iso=true;for(i=0,l=isoDates.length;i<l;i++){if(isoDates[i][1].exec(match[1])){dateFormat=isoDates[i][0];allowTime=isoDates[i][2]!==false;break;}}if(dateFormat==null){config._isValid=false;return;}if(match[3]){for(i=0,l=isoTimes.length;i<l;i++){if(isoTimes[i][1].exec(match[3])){ // match[2] should be 'T' or space
	timeFormat=(match[2]||' ')+isoTimes[i][0];break;}}if(timeFormat==null){config._isValid=false;return;}}if(!allowTime&&timeFormat!=null){config._isValid=false;return;}if(match[4]){if(tzRegex.exec(match[4])){tzFormat='Z';}else {config._isValid=false;return;}}config._f=dateFormat+(timeFormat||'')+(tzFormat||'');configFromStringAndFormat(config);}else {config._isValid=false;}} // date from iso format or fallback
	function configFromString(config){var matched=aspNetJsonRegex.exec(config._i);if(matched!==null){config._d=new Date(+matched[1]);return;}configFromISO(config);if(config._isValid===false){delete config._isValid;utils_hooks__hooks.createFromInputFallback(config);}}utils_hooks__hooks.createFromInputFallback=deprecate('moment construction falls back to js Date. This is '+'discouraged and will be removed in upcoming major '+'release. Please refer to '+'https://github.com/moment/moment/issues/1407 for more info.',function(config){config._d=new Date(config._i+(config._useUTC?' UTC':''));});function createDate(y,m,d,h,M,s,ms){ //can't just apply() to create a date:
	//http://stackoverflow.com/questions/181348/instantiating-a-javascript-object-by-calling-prototype-constructor-apply
	var date=new Date(y,m,d,h,M,s,ms); //the date constructor remaps years 0-99 to 1900-1999
	if(y<100&&y>=0&&isFinite(date.getFullYear())){date.setFullYear(y);}return date;}function createUTCDate(y){var date=new Date(Date.UTC.apply(null,arguments)); //the Date.UTC function remaps years 0-99 to 1900-1999
	if(y<100&&y>=0&&isFinite(date.getUTCFullYear())){date.setUTCFullYear(y);}return date;} // FORMATTING
	addFormatToken('Y',0,0,function(){var y=this.year();return y<=9999?''+y:'+'+y;});addFormatToken(0,['YY',2],0,function(){return this.year()%100;});addFormatToken(0,['YYYY',4],0,'year');addFormatToken(0,['YYYYY',5],0,'year');addFormatToken(0,['YYYYYY',6,true],0,'year'); // ALIASES
	addUnitAlias('year','y'); // PARSING
	addRegexToken('Y',matchSigned);addRegexToken('YY',match1to2,match2);addRegexToken('YYYY',match1to4,match4);addRegexToken('YYYYY',match1to6,match6);addRegexToken('YYYYYY',match1to6,match6);addParseToken(['YYYYY','YYYYYY'],YEAR);addParseToken('YYYY',function(input,array){array[YEAR]=input.length===2?utils_hooks__hooks.parseTwoDigitYear(input):toInt(input);});addParseToken('YY',function(input,array){array[YEAR]=utils_hooks__hooks.parseTwoDigitYear(input);});addParseToken('Y',function(input,array){array[YEAR]=parseInt(input,10);}); // HELPERS
	function daysInYear(year){return isLeapYear(year)?366:365;}function isLeapYear(year){return year%4===0&&year%100!==0||year%400===0;} // HOOKS
	utils_hooks__hooks.parseTwoDigitYear=function(input){return toInt(input)+(toInt(input)>68?1900:2000);}; // MOMENTS
	var getSetYear=makeGetSet('FullYear',true);function getIsLeapYear(){return isLeapYear(this.year());} // start-of-first-week - start-of-year
	function firstWeekOffset(year,dow,doy){var  // first-week day -- which january is always in the first week (4 for iso, 1 for other)
	fwd=7+dow-doy, // first-week day local weekday -- which local weekday is fwd
	fwdlw=(7+createUTCDate(year,0,fwd).getUTCDay()-dow)%7;return -fwdlw+fwd-1;} //http://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
	function dayOfYearFromWeeks(year,week,weekday,dow,doy){var localWeekday=(7+weekday-dow)%7,weekOffset=firstWeekOffset(year,dow,doy),dayOfYear=1+7*(week-1)+localWeekday+weekOffset,resYear,resDayOfYear;if(dayOfYear<=0){resYear=year-1;resDayOfYear=daysInYear(resYear)+dayOfYear;}else if(dayOfYear>daysInYear(year)){resYear=year+1;resDayOfYear=dayOfYear-daysInYear(year);}else {resYear=year;resDayOfYear=dayOfYear;}return {year:resYear,dayOfYear:resDayOfYear};}function weekOfYear(mom,dow,doy){var weekOffset=firstWeekOffset(mom.year(),dow,doy),week=Math.floor((mom.dayOfYear()-weekOffset-1)/7)+1,resWeek,resYear;if(week<1){resYear=mom.year()-1;resWeek=week+weeksInYear(resYear,dow,doy);}else if(week>weeksInYear(mom.year(),dow,doy)){resWeek=week-weeksInYear(mom.year(),dow,doy);resYear=mom.year()+1;}else {resYear=mom.year();resWeek=week;}return {week:resWeek,year:resYear};}function weeksInYear(year,dow,doy){var weekOffset=firstWeekOffset(year,dow,doy),weekOffsetNext=firstWeekOffset(year+1,dow,doy);return (daysInYear(year)-weekOffset+weekOffsetNext)/7;} // Pick the first defined of two or three arguments.
	function defaults(a,b,c){if(a!=null){return a;}if(b!=null){return b;}return c;}function currentDateArray(config){ // hooks is actually the exported moment object
	var nowValue=new Date(utils_hooks__hooks.now());if(config._useUTC){return [nowValue.getUTCFullYear(),nowValue.getUTCMonth(),nowValue.getUTCDate()];}return [nowValue.getFullYear(),nowValue.getMonth(),nowValue.getDate()];} // convert an array to a date.
	// the array should mirror the parameters below
	// note: all values past the year are optional and will default to the lowest possible value.
	// [year, month, day , hour, minute, second, millisecond]
	function configFromArray(config){var i,date,input=[],currentDate,yearToUse;if(config._d){return;}currentDate=currentDateArray(config); //compute day of the year from weeks and weekdays
	if(config._w&&config._a[DATE]==null&&config._a[MONTH]==null){dayOfYearFromWeekInfo(config);} //if the day of the year is set, figure out what it is
	if(config._dayOfYear){yearToUse=defaults(config._a[YEAR],currentDate[YEAR]);if(config._dayOfYear>daysInYear(yearToUse)){getParsingFlags(config)._overflowDayOfYear=true;}date=createUTCDate(yearToUse,0,config._dayOfYear);config._a[MONTH]=date.getUTCMonth();config._a[DATE]=date.getUTCDate();} // Default to current date.
	// * if no year, month, day of month are given, default to today
	// * if day of month is given, default month and year
	// * if month is given, default only year
	// * if year is given, don't default anything
	for(i=0;i<3&&config._a[i]==null;++i){config._a[i]=input[i]=currentDate[i];} // Zero out whatever was not defaulted, including time
	for(;i<7;i++){config._a[i]=input[i]=config._a[i]==null?i===2?1:0:config._a[i];} // Check for 24:00:00.000
	if(config._a[HOUR]===24&&config._a[MINUTE]===0&&config._a[SECOND]===0&&config._a[MILLISECOND]===0){config._nextDay=true;config._a[HOUR]=0;}config._d=(config._useUTC?createUTCDate:createDate).apply(null,input); // Apply timezone offset from input. The actual utcOffset can be changed
	// with parseZone.
	if(config._tzm!=null){config._d.setUTCMinutes(config._d.getUTCMinutes()-config._tzm);}if(config._nextDay){config._a[HOUR]=24;}}function dayOfYearFromWeekInfo(config){var w,weekYear,week,weekday,dow,doy,temp,weekdayOverflow;w=config._w;if(w.GG!=null||w.W!=null||w.E!=null){dow=1;doy=4; // TODO: We need to take the current isoWeekYear, but that depends on
	// how we interpret now (local, utc, fixed offset). So create
	// a now version of current config (take local/utc/offset flags, and
	// create now).
	weekYear=defaults(w.GG,config._a[YEAR],weekOfYear(local__createLocal(),1,4).year);week=defaults(w.W,1);weekday=defaults(w.E,1);if(weekday<1||weekday>7){weekdayOverflow=true;}}else {dow=config._locale._week.dow;doy=config._locale._week.doy;weekYear=defaults(w.gg,config._a[YEAR],weekOfYear(local__createLocal(),dow,doy).year);week=defaults(w.w,1);if(w.d!=null){ // weekday -- low day numbers are considered next week
	weekday=w.d;if(weekday<0||weekday>6){weekdayOverflow=true;}}else if(w.e!=null){ // local weekday -- counting starts from begining of week
	weekday=w.e+dow;if(w.e<0||w.e>6){weekdayOverflow=true;}}else { // default to begining of week
	weekday=dow;}}if(week<1||week>weeksInYear(weekYear,dow,doy)){getParsingFlags(config)._overflowWeeks=true;}else if(weekdayOverflow!=null){getParsingFlags(config)._overflowWeekday=true;}else {temp=dayOfYearFromWeeks(weekYear,week,weekday,dow,doy);config._a[YEAR]=temp.year;config._dayOfYear=temp.dayOfYear;}} // constant that refers to the ISO standard
	utils_hooks__hooks.ISO_8601=function(){}; // date from string and format string
	function configFromStringAndFormat(config){ // TODO: Move this to another part of the creation flow to prevent circular deps
	if(config._f===utils_hooks__hooks.ISO_8601){configFromISO(config);return;}config._a=[];getParsingFlags(config).empty=true; // This array is used to make a Date, either with `new Date` or `Date.UTC`
	var string=''+config._i,i,parsedInput,tokens,token,skipped,stringLength=string.length,totalParsedInputLength=0;tokens=expandFormat(config._f,config._locale).match(formattingTokens)||[];for(i=0;i<tokens.length;i++){token=tokens[i];parsedInput=(string.match(getParseRegexForToken(token,config))||[])[0]; // console.log('token', token, 'parsedInput', parsedInput,
	//         'regex', getParseRegexForToken(token, config));
	if(parsedInput){skipped=string.substr(0,string.indexOf(parsedInput));if(skipped.length>0){getParsingFlags(config).unusedInput.push(skipped);}string=string.slice(string.indexOf(parsedInput)+parsedInput.length);totalParsedInputLength+=parsedInput.length;} // don't parse if it's not a known token
	if(formatTokenFunctions[token]){if(parsedInput){getParsingFlags(config).empty=false;}else {getParsingFlags(config).unusedTokens.push(token);}addTimeToArrayFromToken(token,parsedInput,config);}else if(config._strict&&!parsedInput){getParsingFlags(config).unusedTokens.push(token);}} // add remaining unparsed input length to the string
	getParsingFlags(config).charsLeftOver=stringLength-totalParsedInputLength;if(string.length>0){getParsingFlags(config).unusedInput.push(string);} // clear _12h flag if hour is <= 12
	if(getParsingFlags(config).bigHour===true&&config._a[HOUR]<=12&&config._a[HOUR]>0){getParsingFlags(config).bigHour=undefined;}getParsingFlags(config).parsedDateParts=config._a.slice(0);getParsingFlags(config).meridiem=config._meridiem; // handle meridiem
	config._a[HOUR]=meridiemFixWrap(config._locale,config._a[HOUR],config._meridiem);configFromArray(config);checkOverflow(config);}function meridiemFixWrap(locale,hour,meridiem){var isPm;if(meridiem==null){ // nothing to do
	return hour;}if(locale.meridiemHour!=null){return locale.meridiemHour(hour,meridiem);}else if(locale.isPM!=null){ // Fallback
	isPm=locale.isPM(meridiem);if(isPm&&hour<12){hour+=12;}if(!isPm&&hour===12){hour=0;}return hour;}else { // this is not supposed to happen
	return hour;}} // date from string and array of format strings
	function configFromStringAndArray(config){var tempConfig,bestMoment,scoreToBeat,i,currentScore;if(config._f.length===0){getParsingFlags(config).invalidFormat=true;config._d=new Date(NaN);return;}for(i=0;i<config._f.length;i++){currentScore=0;tempConfig=copyConfig({},config);if(config._useUTC!=null){tempConfig._useUTC=config._useUTC;}tempConfig._f=config._f[i];configFromStringAndFormat(tempConfig);if(!valid__isValid(tempConfig)){continue;} // if there is any input that was not parsed add a penalty for that format
	currentScore+=getParsingFlags(tempConfig).charsLeftOver; //or tokens
	currentScore+=getParsingFlags(tempConfig).unusedTokens.length*10;getParsingFlags(tempConfig).score=currentScore;if(scoreToBeat==null||currentScore<scoreToBeat){scoreToBeat=currentScore;bestMoment=tempConfig;}}extend(config,bestMoment||tempConfig);}function configFromObject(config){if(config._d){return;}var i=normalizeObjectUnits(config._i);config._a=map([i.year,i.month,i.day||i.date,i.hour,i.minute,i.second,i.millisecond],function(obj){return obj&&parseInt(obj,10);});configFromArray(config);}function createFromConfig(config){var res=new Moment(checkOverflow(prepareConfig(config)));if(res._nextDay){ // Adding is smart enough around DST
	res.add(1,'d');res._nextDay=undefined;}return res;}function prepareConfig(config){var input=config._i,format=config._f;config._locale=config._locale||locale_locales__getLocale(config._l);if(input===null||format===undefined&&input===''){return valid__createInvalid({nullInput:true});}if(typeof input==='string'){config._i=input=config._locale.preparse(input);}if(isMoment(input)){return new Moment(checkOverflow(input));}else if(isArray(format)){configFromStringAndArray(config);}else if(format){configFromStringAndFormat(config);}else if(isDate(input)){config._d=input;}else {configFromInput(config);}if(!valid__isValid(config)){config._d=null;}return config;}function configFromInput(config){var input=config._i;if(input===undefined){config._d=new Date(utils_hooks__hooks.now());}else if(isDate(input)){config._d=new Date(input.valueOf());}else if(typeof input==='string'){configFromString(config);}else if(isArray(input)){config._a=map(input.slice(0),function(obj){return parseInt(obj,10);});configFromArray(config);}else if(typeof input==='object'){configFromObject(config);}else if(typeof input==='number'){ // from milliseconds
	config._d=new Date(input);}else {utils_hooks__hooks.createFromInputFallback(config);}}function createLocalOrUTC(input,format,locale,strict,isUTC){var c={};if(typeof locale==='boolean'){strict=locale;locale=undefined;} // object construction must be done this way.
	// https://github.com/moment/moment/issues/1423
	c._isAMomentObject=true;c._useUTC=c._isUTC=isUTC;c._l=locale;c._i=input;c._f=format;c._strict=strict;return createFromConfig(c);}function local__createLocal(input,format,locale,strict){return createLocalOrUTC(input,format,locale,strict,false);}var prototypeMin=deprecate('moment().min is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548',function(){var other=local__createLocal.apply(null,arguments);if(this.isValid()&&other.isValid()){return other<this?this:other;}else {return valid__createInvalid();}});var prototypeMax=deprecate('moment().max is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548',function(){var other=local__createLocal.apply(null,arguments);if(this.isValid()&&other.isValid()){return other>this?this:other;}else {return valid__createInvalid();}}); // Pick a moment m from moments so that m[fn](other) is true for all
	// other. This relies on the function fn to be transitive.
	//
	// moments should either be an array of moment objects or an array, whose
	// first element is an array of moment objects.
	function pickBy(fn,moments){var res,i;if(moments.length===1&&isArray(moments[0])){moments=moments[0];}if(!moments.length){return local__createLocal();}res=moments[0];for(i=1;i<moments.length;++i){if(!moments[i].isValid()||moments[i][fn](res)){res=moments[i];}}return res;} // TODO: Use [].sort instead?
	function min(){var args=[].slice.call(arguments,0);return pickBy('isBefore',args);}function max(){var args=[].slice.call(arguments,0);return pickBy('isAfter',args);}var now=function(){return Date.now?Date.now():+new Date();};function Duration(duration){var normalizedInput=normalizeObjectUnits(duration),years=normalizedInput.year||0,quarters=normalizedInput.quarter||0,months=normalizedInput.month||0,weeks=normalizedInput.week||0,days=normalizedInput.day||0,hours=normalizedInput.hour||0,minutes=normalizedInput.minute||0,seconds=normalizedInput.second||0,milliseconds=normalizedInput.millisecond||0; // representation for dateAddRemove
	this._milliseconds=+milliseconds+seconds*1e3+ // 1000
	minutes*6e4+ // 1000 * 60
	hours*1000*60*60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
	// Because of dateAddRemove treats 24 hours as different from a
	// day when working around DST, we need to store them separately
	this._days=+days+weeks*7; // It is impossible translate months into days without knowing
	// which months you are are talking about, so we have to store
	// it separately.
	this._months=+months+quarters*3+years*12;this._data={};this._locale=locale_locales__getLocale();this._bubble();}function isDuration(obj){return obj instanceof Duration;} // FORMATTING
	function offset(token,separator){addFormatToken(token,0,0,function(){var offset=this.utcOffset();var sign='+';if(offset<0){offset=-offset;sign='-';}return sign+zeroFill(~ ~(offset/60),2)+separator+zeroFill(~ ~offset%60,2);});}offset('Z',':');offset('ZZ',''); // PARSING
	addRegexToken('Z',matchShortOffset);addRegexToken('ZZ',matchShortOffset);addParseToken(['Z','ZZ'],function(input,array,config){config._useUTC=true;config._tzm=offsetFromString(matchShortOffset,input);}); // HELPERS
	// timezone chunker
	// '+10:00' > ['10',  '00']
	// '-1530'  > ['-15', '30']
	var chunkOffset=/([\+\-]|\d\d)/gi;function offsetFromString(matcher,string){var matches=(string||'').match(matcher)||[];var chunk=matches[matches.length-1]||[];var parts=(chunk+'').match(chunkOffset)||['-',0,0];var minutes=+(parts[1]*60)+toInt(parts[2]);return parts[0]==='+'?minutes:-minutes;} // Return a moment from input, that is local/utc/zone equivalent to model.
	function cloneWithOffset(input,model){var res,diff;if(model._isUTC){res=model.clone();diff=(isMoment(input)||isDate(input)?input.valueOf():local__createLocal(input).valueOf())-res.valueOf(); // Use low-level api, because this fn is low-level api.
	res._d.setTime(res._d.valueOf()+diff);utils_hooks__hooks.updateOffset(res,false);return res;}else {return local__createLocal(input).local();}}function getDateOffset(m){ // On Firefox.24 Date#getTimezoneOffset returns a floating point.
	// https://github.com/moment/moment/pull/1871
	return -Math.round(m._d.getTimezoneOffset()/15)*15;} // HOOKS
	// This function will be called whenever a moment is mutated.
	// It is intended to keep the offset in sync with the timezone.
	utils_hooks__hooks.updateOffset=function(){}; // MOMENTS
	// keepLocalTime = true means only change the timezone, without
	// affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
	// 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
	// +0200, so we adjust the time as needed, to be valid.
	//
	// Keeping the time actually adds/subtracts (one hour)
	// from the actual represented time. That is why we call updateOffset
	// a second time. In case it wants us to change the offset again
	// _changeInProgress == true case, then we have to adjust, because
	// there is no such time in the given timezone.
	function getSetOffset(input,keepLocalTime){var offset=this._offset||0,localAdjust;if(!this.isValid()){return input!=null?this:NaN;}if(input!=null){if(typeof input==='string'){input=offsetFromString(matchShortOffset,input);}else if(Math.abs(input)<16){input=input*60;}if(!this._isUTC&&keepLocalTime){localAdjust=getDateOffset(this);}this._offset=input;this._isUTC=true;if(localAdjust!=null){this.add(localAdjust,'m');}if(offset!==input){if(!keepLocalTime||this._changeInProgress){add_subtract__addSubtract(this,create__createDuration(input-offset,'m'),1,false);}else if(!this._changeInProgress){this._changeInProgress=true;utils_hooks__hooks.updateOffset(this,true);this._changeInProgress=null;}}return this;}else {return this._isUTC?offset:getDateOffset(this);}}function getSetZone(input,keepLocalTime){if(input!=null){if(typeof input!=='string'){input=-input;}this.utcOffset(input,keepLocalTime);return this;}else {return -this.utcOffset();}}function setOffsetToUTC(keepLocalTime){return this.utcOffset(0,keepLocalTime);}function setOffsetToLocal(keepLocalTime){if(this._isUTC){this.utcOffset(0,keepLocalTime);this._isUTC=false;if(keepLocalTime){this.subtract(getDateOffset(this),'m');}}return this;}function setOffsetToParsedOffset(){if(this._tzm){this.utcOffset(this._tzm);}else if(typeof this._i==='string'){this.utcOffset(offsetFromString(matchOffset,this._i));}return this;}function hasAlignedHourOffset(input){if(!this.isValid()){return false;}input=input?local__createLocal(input).utcOffset():0;return (this.utcOffset()-input)%60===0;}function isDaylightSavingTime(){return this.utcOffset()>this.clone().month(0).utcOffset()||this.utcOffset()>this.clone().month(5).utcOffset();}function isDaylightSavingTimeShifted(){if(!isUndefined(this._isDSTShifted)){return this._isDSTShifted;}var c={};copyConfig(c,this);c=prepareConfig(c);if(c._a){var other=c._isUTC?create_utc__createUTC(c._a):local__createLocal(c._a);this._isDSTShifted=this.isValid()&&compareArrays(c._a,other.toArray())>0;}else {this._isDSTShifted=false;}return this._isDSTShifted;}function isLocal(){return this.isValid()?!this._isUTC:false;}function isUtcOffset(){return this.isValid()?this._isUTC:false;}function isUtc(){return this.isValid()?this._isUTC&&this._offset===0:false;} // ASP.NET json date format regex
	var aspNetRegex=/^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?\d*)?$/; // from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
	// somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
	// and further modified to allow for strings containing both week and day
	var isoRegex=/^(-)?P(?:(-?[0-9,.]*)Y)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)W)?(?:(-?[0-9,.]*)D)?(?:T(?:(-?[0-9,.]*)H)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)S)?)?$/;function create__createDuration(input,key){var duration=input, // matching against regexp is expensive, do it on demand
	match=null,sign,ret,diffRes;if(isDuration(input)){duration={ms:input._milliseconds,d:input._days,M:input._months};}else if(typeof input==='number'){duration={};if(key){duration[key]=input;}else {duration.milliseconds=input;}}else if(!!(match=aspNetRegex.exec(input))){sign=match[1]==='-'?-1:1;duration={y:0,d:toInt(match[DATE])*sign,h:toInt(match[HOUR])*sign,m:toInt(match[MINUTE])*sign,s:toInt(match[SECOND])*sign,ms:toInt(match[MILLISECOND])*sign};}else if(!!(match=isoRegex.exec(input))){sign=match[1]==='-'?-1:1;duration={y:parseIso(match[2],sign),M:parseIso(match[3],sign),w:parseIso(match[4],sign),d:parseIso(match[5],sign),h:parseIso(match[6],sign),m:parseIso(match[7],sign),s:parseIso(match[8],sign)};}else if(duration==null){ // checks for null or undefined
	duration={};}else if(typeof duration==='object'&&('from' in duration||'to' in duration)){diffRes=momentsDifference(local__createLocal(duration.from),local__createLocal(duration.to));duration={};duration.ms=diffRes.milliseconds;duration.M=diffRes.months;}ret=new Duration(duration);if(isDuration(input)&&hasOwnProp(input,'_locale')){ret._locale=input._locale;}return ret;}create__createDuration.fn=Duration.prototype;function parseIso(inp,sign){ // We'd normally use ~~inp for this, but unfortunately it also
	// converts floats to ints.
	// inp may be undefined, so careful calling replace on it.
	var res=inp&&parseFloat(inp.replace(',','.')); // apply sign while we're at it
	return (isNaN(res)?0:res)*sign;}function positiveMomentsDifference(base,other){var res={milliseconds:0,months:0};res.months=other.month()-base.month()+(other.year()-base.year())*12;if(base.clone().add(res.months,'M').isAfter(other)){--res.months;}res.milliseconds=+other-+base.clone().add(res.months,'M');return res;}function momentsDifference(base,other){var res;if(!(base.isValid()&&other.isValid())){return {milliseconds:0,months:0};}other=cloneWithOffset(other,base);if(base.isBefore(other)){res=positiveMomentsDifference(base,other);}else {res=positiveMomentsDifference(other,base);res.milliseconds=-res.milliseconds;res.months=-res.months;}return res;}function absRound(number){if(number<0){return Math.round(-1*number)*-1;}else {return Math.round(number);}} // TODO: remove 'name' arg after deprecation is removed
	function createAdder(direction,name){return function(val,period){var dur,tmp; //invert the arguments, but complain about it
	if(period!==null&&!isNaN(+period)){deprecateSimple(name,'moment().'+name+'(period, number) is deprecated. Please use moment().'+name+'(number, period).');tmp=val;val=period;period=tmp;}val=typeof val==='string'?+val:val;dur=create__createDuration(val,period);add_subtract__addSubtract(this,dur,direction);return this;};}function add_subtract__addSubtract(mom,duration,isAdding,updateOffset){var milliseconds=duration._milliseconds,days=absRound(duration._days),months=absRound(duration._months);if(!mom.isValid()){ // No op
	return;}updateOffset=updateOffset==null?true:updateOffset;if(milliseconds){mom._d.setTime(mom._d.valueOf()+milliseconds*isAdding);}if(days){get_set__set(mom,'Date',get_set__get(mom,'Date')+days*isAdding);}if(months){setMonth(mom,get_set__get(mom,'Month')+months*isAdding);}if(updateOffset){utils_hooks__hooks.updateOffset(mom,days||months);}}var add_subtract__add=createAdder(1,'add');var add_subtract__subtract=createAdder(-1,'subtract');function moment_calendar__calendar(time,formats){ // We want to compare the start of today, vs this.
	// Getting start-of-today depends on whether we're local/utc/offset or not.
	var now=time||local__createLocal(),sod=cloneWithOffset(now,this).startOf('day'),diff=this.diff(sod,'days',true),format=diff<-6?'sameElse':diff<-1?'lastWeek':diff<0?'lastDay':diff<1?'sameDay':diff<2?'nextDay':diff<7?'nextWeek':'sameElse';var output=formats&&(isFunction(formats[format])?formats[format]():formats[format]);return this.format(output||this.localeData().calendar(format,this,local__createLocal(now)));}function clone(){return new Moment(this);}function isAfter(input,units){var localInput=isMoment(input)?input:local__createLocal(input);if(!(this.isValid()&&localInput.isValid())){return false;}units=normalizeUnits(!isUndefined(units)?units:'millisecond');if(units==='millisecond'){return this.valueOf()>localInput.valueOf();}else {return localInput.valueOf()<this.clone().startOf(units).valueOf();}}function isBefore(input,units){var localInput=isMoment(input)?input:local__createLocal(input);if(!(this.isValid()&&localInput.isValid())){return false;}units=normalizeUnits(!isUndefined(units)?units:'millisecond');if(units==='millisecond'){return this.valueOf()<localInput.valueOf();}else {return this.clone().endOf(units).valueOf()<localInput.valueOf();}}function isBetween(from,to,units,inclusivity){inclusivity=inclusivity||'()';return (inclusivity[0]==='('?this.isAfter(from,units):!this.isBefore(from,units))&&(inclusivity[1]===')'?this.isBefore(to,units):!this.isAfter(to,units));}function isSame(input,units){var localInput=isMoment(input)?input:local__createLocal(input),inputMs;if(!(this.isValid()&&localInput.isValid())){return false;}units=normalizeUnits(units||'millisecond');if(units==='millisecond'){return this.valueOf()===localInput.valueOf();}else {inputMs=localInput.valueOf();return this.clone().startOf(units).valueOf()<=inputMs&&inputMs<=this.clone().endOf(units).valueOf();}}function isSameOrAfter(input,units){return this.isSame(input,units)||this.isAfter(input,units);}function isSameOrBefore(input,units){return this.isSame(input,units)||this.isBefore(input,units);}function diff(input,units,asFloat){var that,zoneDelta,delta,output;if(!this.isValid()){return NaN;}that=cloneWithOffset(input,this);if(!that.isValid()){return NaN;}zoneDelta=(that.utcOffset()-this.utcOffset())*6e4;units=normalizeUnits(units);if(units==='year'||units==='month'||units==='quarter'){output=monthDiff(this,that);if(units==='quarter'){output=output/3;}else if(units==='year'){output=output/12;}}else {delta=this-that;output=units==='second'?delta/1e3: // 1000
	units==='minute'?delta/6e4: // 1000 * 60
	units==='hour'?delta/36e5: // 1000 * 60 * 60
	units==='day'?(delta-zoneDelta)/864e5: // 1000 * 60 * 60 * 24, negate dst
	units==='week'?(delta-zoneDelta)/6048e5: // 1000 * 60 * 60 * 24 * 7, negate dst
	delta;}return asFloat?output:absFloor(output);}function monthDiff(a,b){ // difference in months
	var wholeMonthDiff=(b.year()-a.year())*12+(b.month()-a.month()), // b is in (anchor - 1 month, anchor + 1 month)
	anchor=a.clone().add(wholeMonthDiff,'months'),anchor2,adjust;if(b-anchor<0){anchor2=a.clone().add(wholeMonthDiff-1,'months'); // linear across the month
	adjust=(b-anchor)/(anchor-anchor2);}else {anchor2=a.clone().add(wholeMonthDiff+1,'months'); // linear across the month
	adjust=(b-anchor)/(anchor2-anchor);} //check for negative zero, return zero if negative zero
	return -(wholeMonthDiff+adjust)||0;}utils_hooks__hooks.defaultFormat='YYYY-MM-DDTHH:mm:ssZ';utils_hooks__hooks.defaultFormatUtc='YYYY-MM-DDTHH:mm:ss[Z]';function toString(){return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');}function moment_format__toISOString(){var m=this.clone().utc();if(0<m.year()&&m.year()<=9999){if(isFunction(Date.prototype.toISOString)){ // native implementation is ~50x faster, use it when we can
	return this.toDate().toISOString();}else {return formatMoment(m,'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');}}else {return formatMoment(m,'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');}}function format(inputString){if(!inputString){inputString=this.isUtc()?utils_hooks__hooks.defaultFormatUtc:utils_hooks__hooks.defaultFormat;}var output=formatMoment(this,inputString);return this.localeData().postformat(output);}function from(time,withoutSuffix){if(this.isValid()&&(isMoment(time)&&time.isValid()||local__createLocal(time).isValid())){return create__createDuration({to:this,from:time}).locale(this.locale()).humanize(!withoutSuffix);}else {return this.localeData().invalidDate();}}function fromNow(withoutSuffix){return this.from(local__createLocal(),withoutSuffix);}function to(time,withoutSuffix){if(this.isValid()&&(isMoment(time)&&time.isValid()||local__createLocal(time).isValid())){return create__createDuration({from:this,to:time}).locale(this.locale()).humanize(!withoutSuffix);}else {return this.localeData().invalidDate();}}function toNow(withoutSuffix){return this.to(local__createLocal(),withoutSuffix);} // If passed a locale key, it will set the locale for this
	// instance.  Otherwise, it will return the locale configuration
	// variables for this instance.
	function locale(key){var newLocaleData;if(key===undefined){return this._locale._abbr;}else {newLocaleData=locale_locales__getLocale(key);if(newLocaleData!=null){this._locale=newLocaleData;}return this;}}var lang=deprecate('moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',function(key){if(key===undefined){return this.localeData();}else {return this.locale(key);}});function localeData(){return this._locale;}function startOf(units){units=normalizeUnits(units); // the following switch intentionally omits break keywords
	// to utilize falling through the cases.
	switch(units){case 'year':this.month(0); /* falls through */case 'quarter':case 'month':this.date(1); /* falls through */case 'week':case 'isoWeek':case 'day':case 'date':this.hours(0); /* falls through */case 'hour':this.minutes(0); /* falls through */case 'minute':this.seconds(0); /* falls through */case 'second':this.milliseconds(0);} // weeks are a special case
	if(units==='week'){this.weekday(0);}if(units==='isoWeek'){this.isoWeekday(1);} // quarters are also special
	if(units==='quarter'){this.month(Math.floor(this.month()/3)*3);}return this;}function endOf(units){units=normalizeUnits(units);if(units===undefined||units==='millisecond'){return this;} // 'date' is an alias for 'day', so it should be considered as such.
	if(units==='date'){units='day';}return this.startOf(units).add(1,units==='isoWeek'?'week':units).subtract(1,'ms');}function to_type__valueOf(){return this._d.valueOf()-(this._offset||0)*60000;}function unix(){return Math.floor(this.valueOf()/1000);}function toDate(){return this._offset?new Date(this.valueOf()):this._d;}function toArray(){var m=this;return [m.year(),m.month(),m.date(),m.hour(),m.minute(),m.second(),m.millisecond()];}function toObject(){var m=this;return {years:m.year(),months:m.month(),date:m.date(),hours:m.hours(),minutes:m.minutes(),seconds:m.seconds(),milliseconds:m.milliseconds()};}function toJSON(){ // new Date(NaN).toJSON() === null
	return this.isValid()?this.toISOString():null;}function moment_valid__isValid(){return valid__isValid(this);}function parsingFlags(){return extend({},getParsingFlags(this));}function invalidAt(){return getParsingFlags(this).overflow;}function creationData(){return {input:this._i,format:this._f,locale:this._locale,isUTC:this._isUTC,strict:this._strict};} // FORMATTING
	addFormatToken(0,['gg',2],0,function(){return this.weekYear()%100;});addFormatToken(0,['GG',2],0,function(){return this.isoWeekYear()%100;});function addWeekYearFormatToken(token,getter){addFormatToken(0,[token,token.length],0,getter);}addWeekYearFormatToken('gggg','weekYear');addWeekYearFormatToken('ggggg','weekYear');addWeekYearFormatToken('GGGG','isoWeekYear');addWeekYearFormatToken('GGGGG','isoWeekYear'); // ALIASES
	addUnitAlias('weekYear','gg');addUnitAlias('isoWeekYear','GG'); // PARSING
	addRegexToken('G',matchSigned);addRegexToken('g',matchSigned);addRegexToken('GG',match1to2,match2);addRegexToken('gg',match1to2,match2);addRegexToken('GGGG',match1to4,match4);addRegexToken('gggg',match1to4,match4);addRegexToken('GGGGG',match1to6,match6);addRegexToken('ggggg',match1to6,match6);addWeekParseToken(['gggg','ggggg','GGGG','GGGGG'],function(input,week,config,token){week[token.substr(0,2)]=toInt(input);});addWeekParseToken(['gg','GG'],function(input,week,config,token){week[token]=utils_hooks__hooks.parseTwoDigitYear(input);}); // MOMENTS
	function getSetWeekYear(input){return getSetWeekYearHelper.call(this,input,this.week(),this.weekday(),this.localeData()._week.dow,this.localeData()._week.doy);}function getSetISOWeekYear(input){return getSetWeekYearHelper.call(this,input,this.isoWeek(),this.isoWeekday(),1,4);}function getISOWeeksInYear(){return weeksInYear(this.year(),1,4);}function getWeeksInYear(){var weekInfo=this.localeData()._week;return weeksInYear(this.year(),weekInfo.dow,weekInfo.doy);}function getSetWeekYearHelper(input,week,weekday,dow,doy){var weeksTarget;if(input==null){return weekOfYear(this,dow,doy).year;}else {weeksTarget=weeksInYear(input,dow,doy);if(week>weeksTarget){week=weeksTarget;}return setWeekAll.call(this,input,week,weekday,dow,doy);}}function setWeekAll(weekYear,week,weekday,dow,doy){var dayOfYearData=dayOfYearFromWeeks(weekYear,week,weekday,dow,doy),date=createUTCDate(dayOfYearData.year,0,dayOfYearData.dayOfYear);this.year(date.getUTCFullYear());this.month(date.getUTCMonth());this.date(date.getUTCDate());return this;} // FORMATTING
	addFormatToken('Q',0,'Qo','quarter'); // ALIASES
	addUnitAlias('quarter','Q'); // PARSING
	addRegexToken('Q',match1);addParseToken('Q',function(input,array){array[MONTH]=(toInt(input)-1)*3;}); // MOMENTS
	function getSetQuarter(input){return input==null?Math.ceil((this.month()+1)/3):this.month((input-1)*3+this.month()%3);} // FORMATTING
	addFormatToken('w',['ww',2],'wo','week');addFormatToken('W',['WW',2],'Wo','isoWeek'); // ALIASES
	addUnitAlias('week','w');addUnitAlias('isoWeek','W'); // PARSING
	addRegexToken('w',match1to2);addRegexToken('ww',match1to2,match2);addRegexToken('W',match1to2);addRegexToken('WW',match1to2,match2);addWeekParseToken(['w','ww','W','WW'],function(input,week,config,token){week[token.substr(0,1)]=toInt(input);}); // HELPERS
	// LOCALES
	function localeWeek(mom){return weekOfYear(mom,this._week.dow,this._week.doy).week;}var defaultLocaleWeek={dow:0, // Sunday is the first day of the week.
	doy:6 // The week that contains Jan 1st is the first week of the year.
	};function localeFirstDayOfWeek(){return this._week.dow;}function localeFirstDayOfYear(){return this._week.doy;} // MOMENTS
	function getSetWeek(input){var week=this.localeData().week(this);return input==null?week:this.add((input-week)*7,'d');}function getSetISOWeek(input){var week=weekOfYear(this,1,4).week;return input==null?week:this.add((input-week)*7,'d');} // FORMATTING
	addFormatToken('D',['DD',2],'Do','date'); // ALIASES
	addUnitAlias('date','D'); // PARSING
	addRegexToken('D',match1to2);addRegexToken('DD',match1to2,match2);addRegexToken('Do',function(isStrict,locale){return isStrict?locale._ordinalParse:locale._ordinalParseLenient;});addParseToken(['D','DD'],DATE);addParseToken('Do',function(input,array){array[DATE]=toInt(input.match(match1to2)[0],10);}); // MOMENTS
	var getSetDayOfMonth=makeGetSet('Date',true); // FORMATTING
	addFormatToken('d',0,'do','day');addFormatToken('dd',0,0,function(format){return this.localeData().weekdaysMin(this,format);});addFormatToken('ddd',0,0,function(format){return this.localeData().weekdaysShort(this,format);});addFormatToken('dddd',0,0,function(format){return this.localeData().weekdays(this,format);});addFormatToken('e',0,0,'weekday');addFormatToken('E',0,0,'isoWeekday'); // ALIASES
	addUnitAlias('day','d');addUnitAlias('weekday','e');addUnitAlias('isoWeekday','E'); // PARSING
	addRegexToken('d',match1to2);addRegexToken('e',match1to2);addRegexToken('E',match1to2);addRegexToken('dd',function(isStrict,locale){return locale.weekdaysMinRegex(isStrict);});addRegexToken('ddd',function(isStrict,locale){return locale.weekdaysShortRegex(isStrict);});addRegexToken('dddd',function(isStrict,locale){return locale.weekdaysRegex(isStrict);});addWeekParseToken(['dd','ddd','dddd'],function(input,week,config,token){var weekday=config._locale.weekdaysParse(input,token,config._strict); // if we didn't get a weekday name, mark the date as invalid
	if(weekday!=null){week.d=weekday;}else {getParsingFlags(config).invalidWeekday=input;}});addWeekParseToken(['d','e','E'],function(input,week,config,token){week[token]=toInt(input);}); // HELPERS
	function parseWeekday(input,locale){if(typeof input!=='string'){return input;}if(!isNaN(input)){return parseInt(input,10);}input=locale.weekdaysParse(input);if(typeof input==='number'){return input;}return null;} // LOCALES
	var defaultLocaleWeekdays='Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');function localeWeekdays(m,format){return isArray(this._weekdays)?this._weekdays[m.day()]:this._weekdays[this._weekdays.isFormat.test(format)?'format':'standalone'][m.day()];}var defaultLocaleWeekdaysShort='Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');function localeWeekdaysShort(m){return this._weekdaysShort[m.day()];}var defaultLocaleWeekdaysMin='Su_Mo_Tu_We_Th_Fr_Sa'.split('_');function localeWeekdaysMin(m){return this._weekdaysMin[m.day()];}function day_of_week__handleStrictParse(weekdayName,format,strict){var i,ii,mom,llc=weekdayName.toLocaleLowerCase();if(!this._weekdaysParse){this._weekdaysParse=[];this._shortWeekdaysParse=[];this._minWeekdaysParse=[];for(i=0;i<7;++i){mom=create_utc__createUTC([2000,1]).day(i);this._minWeekdaysParse[i]=this.weekdaysMin(mom,'').toLocaleLowerCase();this._shortWeekdaysParse[i]=this.weekdaysShort(mom,'').toLocaleLowerCase();this._weekdaysParse[i]=this.weekdays(mom,'').toLocaleLowerCase();}}if(strict){if(format==='dddd'){ii=indexOf.call(this._weekdaysParse,llc);return ii!==-1?ii:null;}else if(format==='ddd'){ii=indexOf.call(this._shortWeekdaysParse,llc);return ii!==-1?ii:null;}else {ii=indexOf.call(this._minWeekdaysParse,llc);return ii!==-1?ii:null;}}else {if(format==='dddd'){ii=indexOf.call(this._weekdaysParse,llc);if(ii!==-1){return ii;}ii=indexOf.call(this._shortWeekdaysParse,llc);if(ii!==-1){return ii;}ii=indexOf.call(this._minWeekdaysParse,llc);return ii!==-1?ii:null;}else if(format==='ddd'){ii=indexOf.call(this._shortWeekdaysParse,llc);if(ii!==-1){return ii;}ii=indexOf.call(this._weekdaysParse,llc);if(ii!==-1){return ii;}ii=indexOf.call(this._minWeekdaysParse,llc);return ii!==-1?ii:null;}else {ii=indexOf.call(this._minWeekdaysParse,llc);if(ii!==-1){return ii;}ii=indexOf.call(this._weekdaysParse,llc);if(ii!==-1){return ii;}ii=indexOf.call(this._shortWeekdaysParse,llc);return ii!==-1?ii:null;}}}function localeWeekdaysParse(weekdayName,format,strict){var i,mom,regex;if(this._weekdaysParseExact){return day_of_week__handleStrictParse.call(this,weekdayName,format,strict);}if(!this._weekdaysParse){this._weekdaysParse=[];this._minWeekdaysParse=[];this._shortWeekdaysParse=[];this._fullWeekdaysParse=[];}for(i=0;i<7;i++){ // make the regex if we don't have it already
	mom=create_utc__createUTC([2000,1]).day(i);if(strict&&!this._fullWeekdaysParse[i]){this._fullWeekdaysParse[i]=new RegExp('^'+this.weekdays(mom,'').replace('.','\.?')+'$','i');this._shortWeekdaysParse[i]=new RegExp('^'+this.weekdaysShort(mom,'').replace('.','\.?')+'$','i');this._minWeekdaysParse[i]=new RegExp('^'+this.weekdaysMin(mom,'').replace('.','\.?')+'$','i');}if(!this._weekdaysParse[i]){regex='^'+this.weekdays(mom,'')+'|^'+this.weekdaysShort(mom,'')+'|^'+this.weekdaysMin(mom,'');this._weekdaysParse[i]=new RegExp(regex.replace('.',''),'i');} // test the regex
	if(strict&&format==='dddd'&&this._fullWeekdaysParse[i].test(weekdayName)){return i;}else if(strict&&format==='ddd'&&this._shortWeekdaysParse[i].test(weekdayName)){return i;}else if(strict&&format==='dd'&&this._minWeekdaysParse[i].test(weekdayName)){return i;}else if(!strict&&this._weekdaysParse[i].test(weekdayName)){return i;}}} // MOMENTS
	function getSetDayOfWeek(input){if(!this.isValid()){return input!=null?this:NaN;}var day=this._isUTC?this._d.getUTCDay():this._d.getDay();if(input!=null){input=parseWeekday(input,this.localeData());return this.add(input-day,'d');}else {return day;}}function getSetLocaleDayOfWeek(input){if(!this.isValid()){return input!=null?this:NaN;}var weekday=(this.day()+7-this.localeData()._week.dow)%7;return input==null?weekday:this.add(input-weekday,'d');}function getSetISODayOfWeek(input){if(!this.isValid()){return input!=null?this:NaN;} // behaves the same as moment#day except
	// as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
	// as a setter, sunday should belong to the previous week.
	return input==null?this.day()||7:this.day(this.day()%7?input:input-7);}var defaultWeekdaysRegex=matchWord;function weekdaysRegex(isStrict){if(this._weekdaysParseExact){if(!hasOwnProp(this,'_weekdaysRegex')){computeWeekdaysParse.call(this);}if(isStrict){return this._weekdaysStrictRegex;}else {return this._weekdaysRegex;}}else {return this._weekdaysStrictRegex&&isStrict?this._weekdaysStrictRegex:this._weekdaysRegex;}}var defaultWeekdaysShortRegex=matchWord;function weekdaysShortRegex(isStrict){if(this._weekdaysParseExact){if(!hasOwnProp(this,'_weekdaysRegex')){computeWeekdaysParse.call(this);}if(isStrict){return this._weekdaysShortStrictRegex;}else {return this._weekdaysShortRegex;}}else {return this._weekdaysShortStrictRegex&&isStrict?this._weekdaysShortStrictRegex:this._weekdaysShortRegex;}}var defaultWeekdaysMinRegex=matchWord;function weekdaysMinRegex(isStrict){if(this._weekdaysParseExact){if(!hasOwnProp(this,'_weekdaysRegex')){computeWeekdaysParse.call(this);}if(isStrict){return this._weekdaysMinStrictRegex;}else {return this._weekdaysMinRegex;}}else {return this._weekdaysMinStrictRegex&&isStrict?this._weekdaysMinStrictRegex:this._weekdaysMinRegex;}}function computeWeekdaysParse(){function cmpLenRev(a,b){return b.length-a.length;}var minPieces=[],shortPieces=[],longPieces=[],mixedPieces=[],i,mom,minp,shortp,longp;for(i=0;i<7;i++){ // make the regex if we don't have it already
	mom=create_utc__createUTC([2000,1]).day(i);minp=this.weekdaysMin(mom,'');shortp=this.weekdaysShort(mom,'');longp=this.weekdays(mom,'');minPieces.push(minp);shortPieces.push(shortp);longPieces.push(longp);mixedPieces.push(minp);mixedPieces.push(shortp);mixedPieces.push(longp);} // Sorting makes sure if one weekday (or abbr) is a prefix of another it
	// will match the longer piece.
	minPieces.sort(cmpLenRev);shortPieces.sort(cmpLenRev);longPieces.sort(cmpLenRev);mixedPieces.sort(cmpLenRev);for(i=0;i<7;i++){shortPieces[i]=regexEscape(shortPieces[i]);longPieces[i]=regexEscape(longPieces[i]);mixedPieces[i]=regexEscape(mixedPieces[i]);}this._weekdaysRegex=new RegExp('^('+mixedPieces.join('|')+')','i');this._weekdaysShortRegex=this._weekdaysRegex;this._weekdaysMinRegex=this._weekdaysRegex;this._weekdaysStrictRegex=new RegExp('^('+longPieces.join('|')+')','i');this._weekdaysShortStrictRegex=new RegExp('^('+shortPieces.join('|')+')','i');this._weekdaysMinStrictRegex=new RegExp('^('+minPieces.join('|')+')','i');} // FORMATTING
	addFormatToken('DDD',['DDDD',3],'DDDo','dayOfYear'); // ALIASES
	addUnitAlias('dayOfYear','DDD'); // PARSING
	addRegexToken('DDD',match1to3);addRegexToken('DDDD',match3);addParseToken(['DDD','DDDD'],function(input,array,config){config._dayOfYear=toInt(input);}); // HELPERS
	// MOMENTS
	function getSetDayOfYear(input){var dayOfYear=Math.round((this.clone().startOf('day')-this.clone().startOf('year'))/864e5)+1;return input==null?dayOfYear:this.add(input-dayOfYear,'d');} // FORMATTING
	function hFormat(){return this.hours()%12||12;}function kFormat(){return this.hours()||24;}addFormatToken('H',['HH',2],0,'hour');addFormatToken('h',['hh',2],0,hFormat);addFormatToken('k',['kk',2],0,kFormat);addFormatToken('hmm',0,0,function(){return ''+hFormat.apply(this)+zeroFill(this.minutes(),2);});addFormatToken('hmmss',0,0,function(){return ''+hFormat.apply(this)+zeroFill(this.minutes(),2)+zeroFill(this.seconds(),2);});addFormatToken('Hmm',0,0,function(){return ''+this.hours()+zeroFill(this.minutes(),2);});addFormatToken('Hmmss',0,0,function(){return ''+this.hours()+zeroFill(this.minutes(),2)+zeroFill(this.seconds(),2);});function meridiem(token,lowercase){addFormatToken(token,0,0,function(){return this.localeData().meridiem(this.hours(),this.minutes(),lowercase);});}meridiem('a',true);meridiem('A',false); // ALIASES
	addUnitAlias('hour','h'); // PARSING
	function matchMeridiem(isStrict,locale){return locale._meridiemParse;}addRegexToken('a',matchMeridiem);addRegexToken('A',matchMeridiem);addRegexToken('H',match1to2);addRegexToken('h',match1to2);addRegexToken('HH',match1to2,match2);addRegexToken('hh',match1to2,match2);addRegexToken('hmm',match3to4);addRegexToken('hmmss',match5to6);addRegexToken('Hmm',match3to4);addRegexToken('Hmmss',match5to6);addParseToken(['H','HH'],HOUR);addParseToken(['a','A'],function(input,array,config){config._isPm=config._locale.isPM(input);config._meridiem=input;});addParseToken(['h','hh'],function(input,array,config){array[HOUR]=toInt(input);getParsingFlags(config).bigHour=true;});addParseToken('hmm',function(input,array,config){var pos=input.length-2;array[HOUR]=toInt(input.substr(0,pos));array[MINUTE]=toInt(input.substr(pos));getParsingFlags(config).bigHour=true;});addParseToken('hmmss',function(input,array,config){var pos1=input.length-4;var pos2=input.length-2;array[HOUR]=toInt(input.substr(0,pos1));array[MINUTE]=toInt(input.substr(pos1,2));array[SECOND]=toInt(input.substr(pos2));getParsingFlags(config).bigHour=true;});addParseToken('Hmm',function(input,array,config){var pos=input.length-2;array[HOUR]=toInt(input.substr(0,pos));array[MINUTE]=toInt(input.substr(pos));});addParseToken('Hmmss',function(input,array,config){var pos1=input.length-4;var pos2=input.length-2;array[HOUR]=toInt(input.substr(0,pos1));array[MINUTE]=toInt(input.substr(pos1,2));array[SECOND]=toInt(input.substr(pos2));}); // LOCALES
	function localeIsPM(input){ // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
	// Using charAt should be more compatible.
	return (input+'').toLowerCase().charAt(0)==='p';}var defaultLocaleMeridiemParse=/[ap]\.?m?\.?/i;function localeMeridiem(hours,minutes,isLower){if(hours>11){return isLower?'pm':'PM';}else {return isLower?'am':'AM';}} // MOMENTS
	// Setting the hour should keep the time, because the user explicitly
	// specified which hour he wants. So trying to maintain the same hour (in
	// a new timezone) makes sense. Adding/subtracting hours does not follow
	// this rule.
	var getSetHour=makeGetSet('Hours',true); // FORMATTING
	addFormatToken('m',['mm',2],0,'minute'); // ALIASES
	addUnitAlias('minute','m'); // PARSING
	addRegexToken('m',match1to2);addRegexToken('mm',match1to2,match2);addParseToken(['m','mm'],MINUTE); // MOMENTS
	var getSetMinute=makeGetSet('Minutes',false); // FORMATTING
	addFormatToken('s',['ss',2],0,'second'); // ALIASES
	addUnitAlias('second','s'); // PARSING
	addRegexToken('s',match1to2);addRegexToken('ss',match1to2,match2);addParseToken(['s','ss'],SECOND); // MOMENTS
	var getSetSecond=makeGetSet('Seconds',false); // FORMATTING
	addFormatToken('S',0,0,function(){return ~ ~(this.millisecond()/100);});addFormatToken(0,['SS',2],0,function(){return ~ ~(this.millisecond()/10);});addFormatToken(0,['SSS',3],0,'millisecond');addFormatToken(0,['SSSS',4],0,function(){return this.millisecond()*10;});addFormatToken(0,['SSSSS',5],0,function(){return this.millisecond()*100;});addFormatToken(0,['SSSSSS',6],0,function(){return this.millisecond()*1000;});addFormatToken(0,['SSSSSSS',7],0,function(){return this.millisecond()*10000;});addFormatToken(0,['SSSSSSSS',8],0,function(){return this.millisecond()*100000;});addFormatToken(0,['SSSSSSSSS',9],0,function(){return this.millisecond()*1000000;}); // ALIASES
	addUnitAlias('millisecond','ms'); // PARSING
	addRegexToken('S',match1to3,match1);addRegexToken('SS',match1to3,match2);addRegexToken('SSS',match1to3,match3);var token;for(token='SSSS';token.length<=9;token+='S'){addRegexToken(token,matchUnsigned);}function parseMs(input,array){array[MILLISECOND]=toInt(('0.'+input)*1000);}for(token='S';token.length<=9;token+='S'){addParseToken(token,parseMs);} // MOMENTS
	var getSetMillisecond=makeGetSet('Milliseconds',false); // FORMATTING
	addFormatToken('z',0,0,'zoneAbbr');addFormatToken('zz',0,0,'zoneName'); // MOMENTS
	function getZoneAbbr(){return this._isUTC?'UTC':'';}function getZoneName(){return this._isUTC?'Coordinated Universal Time':'';}var momentPrototype__proto=Moment.prototype;momentPrototype__proto.add=add_subtract__add;momentPrototype__proto.calendar=moment_calendar__calendar;momentPrototype__proto.clone=clone;momentPrototype__proto.diff=diff;momentPrototype__proto.endOf=endOf;momentPrototype__proto.format=format;momentPrototype__proto.from=from;momentPrototype__proto.fromNow=fromNow;momentPrototype__proto.to=to;momentPrototype__proto.toNow=toNow;momentPrototype__proto.get=getSet;momentPrototype__proto.invalidAt=invalidAt;momentPrototype__proto.isAfter=isAfter;momentPrototype__proto.isBefore=isBefore;momentPrototype__proto.isBetween=isBetween;momentPrototype__proto.isSame=isSame;momentPrototype__proto.isSameOrAfter=isSameOrAfter;momentPrototype__proto.isSameOrBefore=isSameOrBefore;momentPrototype__proto.isValid=moment_valid__isValid;momentPrototype__proto.lang=lang;momentPrototype__proto.locale=locale;momentPrototype__proto.localeData=localeData;momentPrototype__proto.max=prototypeMax;momentPrototype__proto.min=prototypeMin;momentPrototype__proto.parsingFlags=parsingFlags;momentPrototype__proto.set=getSet;momentPrototype__proto.startOf=startOf;momentPrototype__proto.subtract=add_subtract__subtract;momentPrototype__proto.toArray=toArray;momentPrototype__proto.toObject=toObject;momentPrototype__proto.toDate=toDate;momentPrototype__proto.toISOString=moment_format__toISOString;momentPrototype__proto.toJSON=toJSON;momentPrototype__proto.toString=toString;momentPrototype__proto.unix=unix;momentPrototype__proto.valueOf=to_type__valueOf;momentPrototype__proto.creationData=creationData; // Year
	momentPrototype__proto.year=getSetYear;momentPrototype__proto.isLeapYear=getIsLeapYear; // Week Year
	momentPrototype__proto.weekYear=getSetWeekYear;momentPrototype__proto.isoWeekYear=getSetISOWeekYear; // Quarter
	momentPrototype__proto.quarter=momentPrototype__proto.quarters=getSetQuarter; // Month
	momentPrototype__proto.month=getSetMonth;momentPrototype__proto.daysInMonth=getDaysInMonth; // Week
	momentPrototype__proto.week=momentPrototype__proto.weeks=getSetWeek;momentPrototype__proto.isoWeek=momentPrototype__proto.isoWeeks=getSetISOWeek;momentPrototype__proto.weeksInYear=getWeeksInYear;momentPrototype__proto.isoWeeksInYear=getISOWeeksInYear; // Day
	momentPrototype__proto.date=getSetDayOfMonth;momentPrototype__proto.day=momentPrototype__proto.days=getSetDayOfWeek;momentPrototype__proto.weekday=getSetLocaleDayOfWeek;momentPrototype__proto.isoWeekday=getSetISODayOfWeek;momentPrototype__proto.dayOfYear=getSetDayOfYear; // Hour
	momentPrototype__proto.hour=momentPrototype__proto.hours=getSetHour; // Minute
	momentPrototype__proto.minute=momentPrototype__proto.minutes=getSetMinute; // Second
	momentPrototype__proto.second=momentPrototype__proto.seconds=getSetSecond; // Millisecond
	momentPrototype__proto.millisecond=momentPrototype__proto.milliseconds=getSetMillisecond; // Offset
	momentPrototype__proto.utcOffset=getSetOffset;momentPrototype__proto.utc=setOffsetToUTC;momentPrototype__proto.local=setOffsetToLocal;momentPrototype__proto.parseZone=setOffsetToParsedOffset;momentPrototype__proto.hasAlignedHourOffset=hasAlignedHourOffset;momentPrototype__proto.isDST=isDaylightSavingTime;momentPrototype__proto.isDSTShifted=isDaylightSavingTimeShifted;momentPrototype__proto.isLocal=isLocal;momentPrototype__proto.isUtcOffset=isUtcOffset;momentPrototype__proto.isUtc=isUtc;momentPrototype__proto.isUTC=isUtc; // Timezone
	momentPrototype__proto.zoneAbbr=getZoneAbbr;momentPrototype__proto.zoneName=getZoneName; // Deprecations
	momentPrototype__proto.dates=deprecate('dates accessor is deprecated. Use date instead.',getSetDayOfMonth);momentPrototype__proto.months=deprecate('months accessor is deprecated. Use month instead',getSetMonth);momentPrototype__proto.years=deprecate('years accessor is deprecated. Use year instead',getSetYear);momentPrototype__proto.zone=deprecate('moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779',getSetZone);var momentPrototype=momentPrototype__proto;function moment__createUnix(input){return local__createLocal(input*1000);}function moment__createInZone(){return local__createLocal.apply(null,arguments).parseZone();}var defaultCalendar={sameDay:'[Today at] LT',nextDay:'[Tomorrow at] LT',nextWeek:'dddd [at] LT',lastDay:'[Yesterday at] LT',lastWeek:'[Last] dddd [at] LT',sameElse:'L'};function locale_calendar__calendar(key,mom,now){var output=this._calendar[key];return isFunction(output)?output.call(mom,now):output;}var defaultLongDateFormat={LTS:'h:mm:ss A',LT:'h:mm A',L:'MM/DD/YYYY',LL:'MMMM D, YYYY',LLL:'MMMM D, YYYY h:mm A',LLLL:'dddd, MMMM D, YYYY h:mm A'};function longDateFormat(key){var format=this._longDateFormat[key],formatUpper=this._longDateFormat[key.toUpperCase()];if(format||!formatUpper){return format;}this._longDateFormat[key]=formatUpper.replace(/MMMM|MM|DD|dddd/g,function(val){return val.slice(1);});return this._longDateFormat[key];}var defaultInvalidDate='Invalid date';function invalidDate(){return this._invalidDate;}var defaultOrdinal='%d';var defaultOrdinalParse=/\d{1,2}/;function ordinal(number){return this._ordinal.replace('%d',number);}function preParsePostFormat(string){return string;}var defaultRelativeTime={future:'in %s',past:'%s ago',s:'a few seconds',m:'a minute',mm:'%d minutes',h:'an hour',hh:'%d hours',d:'a day',dd:'%d days',M:'a month',MM:'%d months',y:'a year',yy:'%d years'};function relative__relativeTime(number,withoutSuffix,string,isFuture){var output=this._relativeTime[string];return isFunction(output)?output(number,withoutSuffix,string,isFuture):output.replace(/%d/i,number);}function pastFuture(diff,output){var format=this._relativeTime[diff>0?'future':'past'];return isFunction(format)?format(output):format.replace(/%s/i,output);}var prototype__proto=Locale.prototype;prototype__proto._calendar=defaultCalendar;prototype__proto.calendar=locale_calendar__calendar;prototype__proto._longDateFormat=defaultLongDateFormat;prototype__proto.longDateFormat=longDateFormat;prototype__proto._invalidDate=defaultInvalidDate;prototype__proto.invalidDate=invalidDate;prototype__proto._ordinal=defaultOrdinal;prototype__proto.ordinal=ordinal;prototype__proto._ordinalParse=defaultOrdinalParse;prototype__proto.preparse=preParsePostFormat;prototype__proto.postformat=preParsePostFormat;prototype__proto._relativeTime=defaultRelativeTime;prototype__proto.relativeTime=relative__relativeTime;prototype__proto.pastFuture=pastFuture;prototype__proto.set=locale_set__set; // Month
	prototype__proto.months=localeMonths;prototype__proto._months=defaultLocaleMonths;prototype__proto.monthsShort=localeMonthsShort;prototype__proto._monthsShort=defaultLocaleMonthsShort;prototype__proto.monthsParse=localeMonthsParse;prototype__proto._monthsRegex=defaultMonthsRegex;prototype__proto.monthsRegex=monthsRegex;prototype__proto._monthsShortRegex=defaultMonthsShortRegex;prototype__proto.monthsShortRegex=monthsShortRegex; // Week
	prototype__proto.week=localeWeek;prototype__proto._week=defaultLocaleWeek;prototype__proto.firstDayOfYear=localeFirstDayOfYear;prototype__proto.firstDayOfWeek=localeFirstDayOfWeek; // Day of Week
	prototype__proto.weekdays=localeWeekdays;prototype__proto._weekdays=defaultLocaleWeekdays;prototype__proto.weekdaysMin=localeWeekdaysMin;prototype__proto._weekdaysMin=defaultLocaleWeekdaysMin;prototype__proto.weekdaysShort=localeWeekdaysShort;prototype__proto._weekdaysShort=defaultLocaleWeekdaysShort;prototype__proto.weekdaysParse=localeWeekdaysParse;prototype__proto._weekdaysRegex=defaultWeekdaysRegex;prototype__proto.weekdaysRegex=weekdaysRegex;prototype__proto._weekdaysShortRegex=defaultWeekdaysShortRegex;prototype__proto.weekdaysShortRegex=weekdaysShortRegex;prototype__proto._weekdaysMinRegex=defaultWeekdaysMinRegex;prototype__proto.weekdaysMinRegex=weekdaysMinRegex; // Hours
	prototype__proto.isPM=localeIsPM;prototype__proto._meridiemParse=defaultLocaleMeridiemParse;prototype__proto.meridiem=localeMeridiem;function lists__get(format,index,field,setter){var locale=locale_locales__getLocale();var utc=create_utc__createUTC().set(setter,index);return locale[field](utc,format);}function listMonthsImpl(format,index,field){if(typeof format==='number'){index=format;format=undefined;}format=format||'';if(index!=null){return lists__get(format,index,field,'month');}var i;var out=[];for(i=0;i<12;i++){out[i]=lists__get(format,i,field,'month');}return out;} // ()
	// (5)
	// (fmt, 5)
	// (fmt)
	// (true)
	// (true, 5)
	// (true, fmt, 5)
	// (true, fmt)
	function listWeekdaysImpl(localeSorted,format,index,field){if(typeof localeSorted==='boolean'){if(typeof format==='number'){index=format;format=undefined;}format=format||'';}else {format=localeSorted;index=format;localeSorted=false;if(typeof format==='number'){index=format;format=undefined;}format=format||'';}var locale=locale_locales__getLocale(),shift=localeSorted?locale._week.dow:0;if(index!=null){return lists__get(format,(index+shift)%7,field,'day');}var i;var out=[];for(i=0;i<7;i++){out[i]=lists__get(format,(i+shift)%7,field,'day');}return out;}function lists__listMonths(format,index){return listMonthsImpl(format,index,'months');}function lists__listMonthsShort(format,index){return listMonthsImpl(format,index,'monthsShort');}function lists__listWeekdays(localeSorted,format,index){return listWeekdaysImpl(localeSorted,format,index,'weekdays');}function lists__listWeekdaysShort(localeSorted,format,index){return listWeekdaysImpl(localeSorted,format,index,'weekdaysShort');}function lists__listWeekdaysMin(localeSorted,format,index){return listWeekdaysImpl(localeSorted,format,index,'weekdaysMin');}locale_locales__getSetGlobalLocale('en',{ordinalParse:/\d{1,2}(th|st|nd|rd)/,ordinal:function(number){var b=number%10,output=toInt(number%100/10)===1?'th':b===1?'st':b===2?'nd':b===3?'rd':'th';return number+output;}}); // Side effect imports
	utils_hooks__hooks.lang=deprecate('moment.lang is deprecated. Use moment.locale instead.',locale_locales__getSetGlobalLocale);utils_hooks__hooks.langData=deprecate('moment.langData is deprecated. Use moment.localeData instead.',locale_locales__getLocale);var mathAbs=Math.abs;function duration_abs__abs(){var data=this._data;this._milliseconds=mathAbs(this._milliseconds);this._days=mathAbs(this._days);this._months=mathAbs(this._months);data.milliseconds=mathAbs(data.milliseconds);data.seconds=mathAbs(data.seconds);data.minutes=mathAbs(data.minutes);data.hours=mathAbs(data.hours);data.months=mathAbs(data.months);data.years=mathAbs(data.years);return this;}function duration_add_subtract__addSubtract(duration,input,value,direction){var other=create__createDuration(input,value);duration._milliseconds+=direction*other._milliseconds;duration._days+=direction*other._days;duration._months+=direction*other._months;return duration._bubble();} // supports only 2.0-style add(1, 's') or add(duration)
	function duration_add_subtract__add(input,value){return duration_add_subtract__addSubtract(this,input,value,1);} // supports only 2.0-style subtract(1, 's') or subtract(duration)
	function duration_add_subtract__subtract(input,value){return duration_add_subtract__addSubtract(this,input,value,-1);}function absCeil(number){if(number<0){return Math.floor(number);}else {return Math.ceil(number);}}function bubble(){var milliseconds=this._milliseconds;var days=this._days;var months=this._months;var data=this._data;var seconds,minutes,hours,years,monthsFromDays; // if we have a mix of positive and negative values, bubble down first
	// check: https://github.com/moment/moment/issues/2166
	if(!(milliseconds>=0&&days>=0&&months>=0||milliseconds<=0&&days<=0&&months<=0)){milliseconds+=absCeil(monthsToDays(months)+days)*864e5;days=0;months=0;} // The following code bubbles up values, see the tests for
	// examples of what that means.
	data.milliseconds=milliseconds%1000;seconds=absFloor(milliseconds/1000);data.seconds=seconds%60;minutes=absFloor(seconds/60);data.minutes=minutes%60;hours=absFloor(minutes/60);data.hours=hours%24;days+=absFloor(hours/24); // convert days to months
	monthsFromDays=absFloor(daysToMonths(days));months+=monthsFromDays;days-=absCeil(monthsToDays(monthsFromDays)); // 12 months -> 1 year
	years=absFloor(months/12);months%=12;data.days=days;data.months=months;data.years=years;return this;}function daysToMonths(days){ // 400 years have 146097 days (taking into account leap year rules)
	// 400 years have 12 months === 4800
	return days*4800/146097;}function monthsToDays(months){ // the reverse of daysToMonths
	return months*146097/4800;}function as(units){var days;var months;var milliseconds=this._milliseconds;units=normalizeUnits(units);if(units==='month'||units==='year'){days=this._days+milliseconds/864e5;months=this._months+daysToMonths(days);return units==='month'?months:months/12;}else { // handle milliseconds separately because of floating point math errors (issue #1867)
	days=this._days+Math.round(monthsToDays(this._months));switch(units){case 'week':return days/7+milliseconds/6048e5;case 'day':return days+milliseconds/864e5;case 'hour':return days*24+milliseconds/36e5;case 'minute':return days*1440+milliseconds/6e4;case 'second':return days*86400+milliseconds/1000; // Math.floor prevents floating point math errors here
	case 'millisecond':return Math.floor(days*864e5)+milliseconds;default:throw new Error('Unknown unit '+units);}}} // TODO: Use this.as('ms')?
	function duration_as__valueOf(){return this._milliseconds+this._days*864e5+this._months%12*2592e6+toInt(this._months/12)*31536e6;}function makeAs(alias){return function(){return this.as(alias);};}var asMilliseconds=makeAs('ms');var asSeconds=makeAs('s');var asMinutes=makeAs('m');var asHours=makeAs('h');var asDays=makeAs('d');var asWeeks=makeAs('w');var asMonths=makeAs('M');var asYears=makeAs('y');function duration_get__get(units){units=normalizeUnits(units);return this[units+'s']();}function makeGetter(name){return function(){return this._data[name];};}var milliseconds=makeGetter('milliseconds');var seconds=makeGetter('seconds');var minutes=makeGetter('minutes');var hours=makeGetter('hours');var days=makeGetter('days');var months=makeGetter('months');var years=makeGetter('years');function weeks(){return absFloor(this.days()/7);}var round=Math.round;var thresholds={s:45, // seconds to minute
	m:45, // minutes to hour
	h:22, // hours to day
	d:26, // days to month
	M:11 // months to year
	}; // helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
	function substituteTimeAgo(string,number,withoutSuffix,isFuture,locale){return locale.relativeTime(number||1,!!withoutSuffix,string,isFuture);}function duration_humanize__relativeTime(posNegDuration,withoutSuffix,locale){var duration=create__createDuration(posNegDuration).abs();var seconds=round(duration.as('s'));var minutes=round(duration.as('m'));var hours=round(duration.as('h'));var days=round(duration.as('d'));var months=round(duration.as('M'));var years=round(duration.as('y'));var a=seconds<thresholds.s&&['s',seconds]||minutes<=1&&['m']||minutes<thresholds.m&&['mm',minutes]||hours<=1&&['h']||hours<thresholds.h&&['hh',hours]||days<=1&&['d']||days<thresholds.d&&['dd',days]||months<=1&&['M']||months<thresholds.M&&['MM',months]||years<=1&&['y']||['yy',years];a[2]=withoutSuffix;a[3]=+posNegDuration>0;a[4]=locale;return substituteTimeAgo.apply(null,a);} // This function allows you to set a threshold for relative time strings
	function duration_humanize__getSetRelativeTimeThreshold(threshold,limit){if(thresholds[threshold]===undefined){return false;}if(limit===undefined){return thresholds[threshold];}thresholds[threshold]=limit;return true;}function humanize(withSuffix){var locale=this.localeData();var output=duration_humanize__relativeTime(this,!withSuffix,locale);if(withSuffix){output=locale.pastFuture(+this,output);}return locale.postformat(output);}var iso_string__abs=Math.abs;function iso_string__toISOString(){ // for ISO strings we do not use the normal bubbling rules:
	//  * milliseconds bubble up until they become hours
	//  * days do not bubble at all
	//  * months bubble up until they become years
	// This is because there is no context-free conversion between hours and days
	// (think of clock changes)
	// and also not between days and months (28-31 days per month)
	var seconds=iso_string__abs(this._milliseconds)/1000;var days=iso_string__abs(this._days);var months=iso_string__abs(this._months);var minutes,hours,years; // 3600 seconds -> 60 minutes -> 1 hour
	minutes=absFloor(seconds/60);hours=absFloor(minutes/60);seconds%=60;minutes%=60; // 12 months -> 1 year
	years=absFloor(months/12);months%=12; // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
	var Y=years;var M=months;var D=days;var h=hours;var m=minutes;var s=seconds;var total=this.asSeconds();if(!total){ // this is the same as C#'s (Noda) and python (isodate)...
	// but not other JS (goog.date)
	return 'P0D';}return (total<0?'-':'')+'P'+(Y?Y+'Y':'')+(M?M+'M':'')+(D?D+'D':'')+(h||m||s?'T':'')+(h?h+'H':'')+(m?m+'M':'')+(s?s+'S':'');}var duration_prototype__proto=Duration.prototype;duration_prototype__proto.abs=duration_abs__abs;duration_prototype__proto.add=duration_add_subtract__add;duration_prototype__proto.subtract=duration_add_subtract__subtract;duration_prototype__proto.as=as;duration_prototype__proto.asMilliseconds=asMilliseconds;duration_prototype__proto.asSeconds=asSeconds;duration_prototype__proto.asMinutes=asMinutes;duration_prototype__proto.asHours=asHours;duration_prototype__proto.asDays=asDays;duration_prototype__proto.asWeeks=asWeeks;duration_prototype__proto.asMonths=asMonths;duration_prototype__proto.asYears=asYears;duration_prototype__proto.valueOf=duration_as__valueOf;duration_prototype__proto._bubble=bubble;duration_prototype__proto.get=duration_get__get;duration_prototype__proto.milliseconds=milliseconds;duration_prototype__proto.seconds=seconds;duration_prototype__proto.minutes=minutes;duration_prototype__proto.hours=hours;duration_prototype__proto.days=days;duration_prototype__proto.weeks=weeks;duration_prototype__proto.months=months;duration_prototype__proto.years=years;duration_prototype__proto.humanize=humanize;duration_prototype__proto.toISOString=iso_string__toISOString;duration_prototype__proto.toString=iso_string__toISOString;duration_prototype__proto.toJSON=iso_string__toISOString;duration_prototype__proto.locale=locale;duration_prototype__proto.localeData=localeData; // Deprecations
	duration_prototype__proto.toIsoString=deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)',iso_string__toISOString);duration_prototype__proto.lang=lang; // Side effect imports
	// FORMATTING
	addFormatToken('X',0,0,'unix');addFormatToken('x',0,0,'valueOf'); // PARSING
	addRegexToken('x',matchSigned);addRegexToken('X',matchTimestamp);addParseToken('X',function(input,array,config){config._d=new Date(parseFloat(input,10)*1000);});addParseToken('x',function(input,array,config){config._d=new Date(toInt(input));}); // Side effect imports
	utils_hooks__hooks.version='2.13.0';setHookCallback(local__createLocal);utils_hooks__hooks.fn=momentPrototype;utils_hooks__hooks.min=min;utils_hooks__hooks.max=max;utils_hooks__hooks.now=now;utils_hooks__hooks.utc=create_utc__createUTC;utils_hooks__hooks.unix=moment__createUnix;utils_hooks__hooks.months=lists__listMonths;utils_hooks__hooks.isDate=isDate;utils_hooks__hooks.locale=locale_locales__getSetGlobalLocale;utils_hooks__hooks.invalid=valid__createInvalid;utils_hooks__hooks.duration=create__createDuration;utils_hooks__hooks.isMoment=isMoment;utils_hooks__hooks.weekdays=lists__listWeekdays;utils_hooks__hooks.parseZone=moment__createInZone;utils_hooks__hooks.localeData=locale_locales__getLocale;utils_hooks__hooks.isDuration=isDuration;utils_hooks__hooks.monthsShort=lists__listMonthsShort;utils_hooks__hooks.weekdaysMin=lists__listWeekdaysMin;utils_hooks__hooks.defineLocale=defineLocale;utils_hooks__hooks.updateLocale=updateLocale;utils_hooks__hooks.locales=locale_locales__listLocales;utils_hooks__hooks.weekdaysShort=lists__listWeekdaysShort;utils_hooks__hooks.normalizeUnits=normalizeUnits;utils_hooks__hooks.relativeTimeThreshold=duration_humanize__getSetRelativeTimeThreshold;utils_hooks__hooks.prototype=momentPrototype;var _moment=utils_hooks__hooks;return _moment;});
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(104)(module)))

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/**
	 * jquery-date-range-picker
	 * @version v0.4.0
	 * @link https://github.com/longbill/jquery-date-range-picker
	 * @license MIT
	 */
	!function (e) {
	   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1), !(function webpackMissingModule() { var e = new Error("Cannot find module \"moment\""); e.code = 'MODULE_NOT_FOUND'; throw e; }())], __WEBPACK_AMD_DEFINE_FACTORY__ = (e), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : "object" == typeof exports && "undefined" != typeof module ? module.exports = e(require("jquery"), require("moment")) : e(jQuery, moment);
	}(function (e, t) {
	  "use strict";
	  e.dateRangePickerLanguages = { "default": { selected: "Selected:", day: "Day", days: "Days", apply: "Close", "week-1": "mo", "week-2": "tu", "week-3": "we", "week-4": "th", "week-5": "fr", "week-6": "sa", "week-7": "su", "week-number": "W", "month-name": ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"], shortcuts: "Shortcuts", "custom-values": "Custom Values", past: "Past", following: "Following", previous: "Previous", "prev-week": "Week", "prev-month": "Month", "prev-year": "Year", next: "Next", "next-week": "Week", "next-month": "Month", "next-year": "Year", "less-than": "Date range should not be more than %d days", "more-than": "Date range should not be less than %d days", "default-more": "Please select a date range longer than %d days", "default-single": "Please select a date", "default-less": "Please select a date range less than %d days", "default-range": "Please select a date range between %d and %d days", "default-default": "Please select a date range", time: "Time", hour: "Hour", minute: "Minute" }, az: { selected: "Seçildi:", day: " gün", days: " gün", apply: "tətbiq", "week-1": "1", "week-2": "2", "week-3": "3", "week-4": "4", "week-5": "5", "week-6": "6", "week-7": "7", "month-name": ["yanvar", "fevral", "mart", "aprel", "may", "iyun", "iyul", "avqust", "sentyabr", "oktyabr", "noyabr", "dekabr"], shortcuts: "Qısayollar", past: "Keçmiş", following: "Növbəti", previous: "&nbsp;&nbsp;&nbsp;", "prev-week": "Öncəki həftə", "prev-month": "Öncəki ay", "prev-year": "Öncəki il", next: "&nbsp;&nbsp;&nbsp;", "next-week": "Növbəti həftə", "next-month": "Növbəti ay", "next-year": "Növbəti il", "less-than": "Tarix aralığı %d gündən çox olmamalıdır", "more-than": "Tarix aralığı %d gündən az olmamalıdır", "default-more": "%d gündən çox bir tarix seçin", "default-single": "Tarix seçin", "default-less": "%d gündən az bir tarix seçin", "default-range": "%d və %d gün aralığında tarixlər seçin", "default-default": "Tarix aralığı seçin" }, cn: { selected: "已选择:", day: "天", days: "天", apply: "确定", "week-1": "一", "week-2": "二", "week-3": "三", "week-4": "四", "week-5": "五", "week-6": "六", "week-7": "日", "week-number": "周", "month-name": ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"], shortcuts: "快捷选择", past: "过去", following: "将来", previous: "&nbsp;&nbsp;&nbsp;", "prev-week": "上周", "prev-month": "上个月", "prev-year": "去年", next: "&nbsp;&nbsp;&nbsp;", "next-week": "下周", "next-month": "下个月", "next-year": "明年", "less-than": "所选日期范围不能大于%d天", "more-than": "所选日期范围不能小于%d天", "default-more": "请选择大于%d天的日期范围", "default-less": "请选择小于%d天的日期范围", "default-range": "请选择%d天到%d天的日期范围", "default-single": "请选择一个日期", "default-default": "请选择一个日期范围", time: "时间", hour: "小时", minute: "分钟" }, cz: { selected: "Vybráno:", day: "Den", days: "Dny", apply: "Zavřít", "week-1": "po", "week-2": "út", "week-3": "st", "week-4": "čt", "week-5": "pá", "week-6": "so", "week-7": "ne", "month-name": ["leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec"], shortcuts: "Zkratky", past: "po", following: "následující", previous: "předchozí", "prev-week": "týden", "prev-month": "měsíc", "prev-year": "rok", next: "další", "next-week": "týden", "next-month": "měsíc", "next-year": "rok", "less-than": "Rozsah data by neměl být větší než %d dnů", "more-than": "Rozsah data by neměl být menší než %d dnů", "default-more": "Prosím zvolte rozsah data větší než %d dnů", "default-single": "Prosím zvolte datum", "default-less": "Prosím zvolte rozsah data menší než %d dnů", "default-range": "Prosím zvolte rozsah data mezi %d a %d dny", "default-default": "Prosím zvolte rozsah data" }, de: { selected: "Auswahl:", day: "Tag", days: "Tage", apply: "Schließen", "week-1": "mo", "week-2": "di", "week-3": "mi", "week-4": "do", "week-5": "fr", "week-6": "sa", "week-7": "so", "month-name": ["januar", "februar", "märz", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "dezember"], shortcuts: "Schnellwahl", past: "Vorherige", following: "Folgende", previous: "Vorherige", "prev-week": "Woche", "prev-month": "Monat", "prev-year": "Jahr", next: "Nächste", "next-week": "Woche", "next-month": "Monat", "next-year": "Jahr", "less-than": "Datumsbereich darf nicht größer sein als %d Tage", "more-than": "Datumsbereich darf nicht kleiner sein als %d Tage", "default-more": "Bitte mindestens %d Tage auswählen", "default-single": "Bitte ein Datum auswählen", "default-less": "Bitte weniger als %d Tage auswählen", "default-range": "Bitte einen Datumsbereich zwischen %d und %d Tagen auswählen", "default-default": "Bitte ein Start- und Enddatum auswählen", Time: "Zeit", hour: "Stunde", minute: "Minute" }, es: { selected: "Seleccionado:", day: "Día", days: "Días", apply: "Cerrar", "week-1": "lu", "week-2": "ma", "week-3": "mi", "week-4": "ju", "week-5": "vi", "week-6": "sa", "week-7": "do", "month-name": ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"], shortcuts: "Accesos directos", past: "Pasado", following: "Siguiente", previous: "Anterior", "prev-week": "Semana", "prev-month": "Mes", "prev-year": "Año", next: "Siguiente", "next-week": "Semana", "next-month": "Mes", "next-year": "Año", "less-than": "El rango no debería ser mayor de %d días", "more-than": "El rango no debería ser menor de %d días", "default-more": "Por favor selecciona un rango mayor a %d días", "default-single": "Por favor selecciona un día", "default-less": "Por favor selecciona un rango menor a %d días", "default-range": "Por favor selecciona un rango entre %d y %d días", "default-default": "Por favor selecciona un rango de fechas." }, fr: { selected: "Sélection:", day: "Jour", days: "Jours", apply: "Fermer", "week-1": "lu", "week-2": "ma", "week-3": "me", "week-4": "je", "week-5": "ve", "week-6": "sa", "week-7": "di", "month-name": ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"], shortcuts: "Raccourcis", past: "Passé", following: "Suivant", previous: "Précédent", "prev-week": "Semaine", "prev-month": "Mois", "prev-year": "Année", next: "Suivant", "next-week": "Semaine", "next-month": "Mois", "next-year": "Année", "less-than": "L'intervalle ne doit pas être supérieure à %d jours", "more-than": "L'intervalle ne doit pas être inférieure à %d jours", "default-more": "Merci de choisir une intervalle supérieure à %d jours", "default-single": "Merci de choisir une date", "default-less": "Merci de choisir une intervalle inférieure %d jours", "default-range": "Merci de choisir une intervalle comprise entre %d et %d jours", "default-default": "Merci de choisir une date" }, hu: { selected: "Kiválasztva:", day: "Nap", days: "Nap", apply: "Ok", "week-1": "h", "week-2": "k", "week-3": "sz", "week-4": "cs", "week-5": "p", "week-6": "sz", "week-7": "v", "month-name": ["január", "február", "március", "április", "május", "június", "július", "augusztus", "szeptember", "október", "november", "december"], shortcuts: "Gyorsválasztó", past: "Múlt", following: "Következő", previous: "Előző", "prev-week": "Hét", "prev-month": "Hónap", "prev-year": "Év", next: "Következő", "next-week": "Hét", "next-month": "Hónap", "next-year": "Év", "less-than": "A kiválasztás nem lehet több %d napnál", "more-than": "A kiválasztás nem lehet több %d napnál", "default-more": "Válassz ki egy időszakot ami hosszabb mint %d nap", "default-single": "Válassz egy napot", "default-less": "Válassz ki egy időszakot ami rövidebb mint %d nap", "default-range": "Válassz ki egy %d - %d nap hosszú időszakot", "default-default": "Válassz ki egy időszakot" }, it: { selected: "Selezionati:", day: "Giorno", days: "Giorni", apply: "Chiudi", "week-1": "lu", "week-2": "ma", "week-3": "me", "week-4": "gi", "week-5": "ve", "week-6": "sa", "week-7": "do", "month-name": ["gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre"], shortcuts: "Scorciatoie", past: "Scorso", following: "Successivo", previous: "Precedente", "prev-week": "Settimana", "prev-month": "Mese", "prev-year": "Anno", next: "Prossimo", "next-week": "Settimana", "next-month": "Mese", "next-year": "Anno", "less-than": "L'intervallo non dev'essere maggiore di %d giorni", "more-than": "L'intervallo non dev'essere minore di %d giorni", "default-more": "Seleziona un intervallo maggiore di %d giorni", "default-single": "Seleziona una data", "default-less": "Seleziona un intervallo minore di %d giorni", "default-range": "Seleziona un intervallo compreso tra i %d e i %d giorni", "default-default": "Seleziona un intervallo di date" }, ko: { selected: "기간:", day: "일", days: "일간", apply: "닫기", "week-1": "월", "week-2": "화", "week-3": "수", "week-4": "목", "week-5": "금", "week-6": "토", "week-7": "일", "week-number": "주", "month-name": ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"], shortcuts: "단축키들", past: "지난(오늘기준)", following: "이후(오늘기준)", previous: "이전", "prev-week": "1주", "prev-month": "1달", "prev-year": "1년", next: "다음", "next-week": "1주", "next-month": "1달", "next-year": "1년", "less-than": "날짜 범위는 %d 일보다 많을 수 없습니다", "more-than": "날짜 범위는 %d 일보다 작을 수 없습니다", "default-more": "날짜 범위를 %d 일보다 길게 선택해 주세요", "default-single": "날짜를 선택해 주세요", "default-less": "%d 일보다 작은 날짜를 선택해 주세요", "default-range": "%d와 %d 일 사이의 날짜 범위를 선택해 주세요", "default-default": "날짜 범위를 선택해 주세요", time: "시각", hour: "시", minute: "분" }, no: { selected: "Valgt:", day: "Dag", days: "Dager", apply: "Lukk", "week-1": "ma", "week-2": "ti", "week-3": "on", "week-4": "to", "week-5": "fr", "week-6": "lø", "week-7": "sø", "month-name": ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"], shortcuts: "Snarveier", "custom-values": "Egendefinerte Verdier", past: "Over", following: "Følger", previous: "Forrige", "prev-week": "Uke", "prev-month": "Måned", "prev-year": "År", next: "Neste", "next-week": "Uke", "next-month": "Måned", "next-year": "År", "less-than": "Datoperioden skal ikkje være lengre enn %d dager", "more-than": "Datoperioden skal ikkje være kortere enn %d dager", "default-more": "Vennligst velg ein datoperiode lengre enn %d dager", "default-single": "Vennligst velg ein dato", "default-less": "Vennligst velg ein datoperiode mindre enn %d dager", "default-range": "Vennligst velg ein datoperiode mellom %d og %d dager", "default-default": "Vennligst velg ein datoperiode", time: "Tid", hour: "Time", minute: "Minutter" }, nl: { selected: "Geselecteerd:", day: "Dag", days: "Dagen", apply: "Ok", "week-1": "ma", "week-2": "di", "week-3": "wo", "week-4": "do", "week-5": "vr", "week-6": "za", "week-7": "zo", "month-name": ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "october", "november", "december"], shortcuts: "Snelkoppelingen", "custom-values": "Aangepaste waarden", past: "Verleden", following: "Komend", previous: "Vorige", "prev-week": "Week", "prev-month": "Maand", "prev-year": "Jaar", next: "Volgende", "next-week": "Week", "next-month": "Maand", "next-year": "Jaar", "less-than": "Interval moet langer dan %d dagen zijn", "more-than": "Interval mag niet minder dan %d dagen zijn", "default-more": "Selecteer een interval langer dan %dagen", "default-single": "Selecteer een datum", "default-less": "Selecteer een interval minder dan %d dagen", "default-range": "Selecteer een interval tussen %d en %d dagen", "default-default": "Selecteer een interval", time: "Tijd", hour: "Uur", minute: "Minuut" }, ru: { selected: "Выбрано:", day: "День", days: "Дней", apply: "Закрыть", "week-1": "пн", "week-2": "вт", "week-3": "ср", "week-4": "чт", "week-5": "пт", "week-6": "сб", "week-7": "вс", "month-name": ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"], shortcuts: "Быстрый выбор", past: "Прошедшие", following: "Следующие", previous: "&nbsp;&nbsp;&nbsp;", "prev-week": "Неделя", "prev-month": "Месяц", "prev-year": "Год", next: "&nbsp;&nbsp;&nbsp;", "next-week": "Неделя", "next-month": "Месяц", "next-year": "Год", "less-than": "Диапазон не может быть больше %d дней", "more-than": "Диапазон не может быть меньше %d дней", "default-more": "Пожалуйста выберите диапазон больше %d дней", "default-single": "Пожалуйста выберите дату", "default-less": "Пожалуйста выберите диапазон меньше %d дней", "default-range": "Пожалуйста выберите диапазон между %d и %d днями", "default-default": "Пожалуйста выберите диапазон" }, pl: { selected: "Wybrany:", day: "Dzień", days: "Dni", apply: "Zamknij", "week-1": "pon", "week-2": "wt", "week-3": "śr", "week-4": "czw", "week-5": "pt", "week-6": "so", "week-7": "nd", "month-name": ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"], shortcuts: "Skróty", "custom-values": "Niestandardowe wartości", past: "Przeszłe", following: "Następne", previous: "Poprzednie", "prev-week": "tydzień", "prev-month": "miesiąc", "prev-year": "rok", next: "Następny", "next-week": "tydzień", "next-month": "miesiąc", "next-year": "rok", "less-than": "Okres nie powinien być dłuższy niż %d dni", "more-than": "Okres nie powinien być krótszy niż  %d ni", "default-more": "Wybierz okres dłuższy niż %d dni", "default-single": "Wybierz datę", "default-less": "Wybierz okres krótszy niż %d dni", "default-range": "Wybierz okres trwający od %d do %d dni", "default-default": "Wybierz okres", time: "Czas", hour: "Godzina", minute: "Minuta" }, se: { selected: "Vald:", day: "dag", days: "dagar", apply: "godkänn", "week-1": "ma", "week-2": "ti", "week-3": "on", "week-4": "to", "week-5": "fr", "week-6": "lö", "week-7": "sö", "month-name": ["januari", "februari", "mars", "april", "maj", "juni", "juli", "augusti", "september", "oktober", "november", "december"], shortcuts: "genvägar", "custom-values": "Anpassade värden", past: "över", following: "följande", previous: "förra", "prev-week": "vecka", "prev-month": "månad", "prev-year": "år", next: "nästa", "next-week": "vecka", "next-month": "måned", "next-year": "år", "less-than": "Datumintervall bör inte vara mindre än %d dagar", "more-than": "Datumintervall bör inte vara mer än %d dagar", "default-more": "Välj ett datumintervall längre än %d dagar", "default-single": "Välj ett datum", "default-less": "Välj ett datumintervall mindre än %d dagar", "default-range": "Välj ett datumintervall mellan %d och %d dagar", "default-default": "Välj ett datumintervall", time: "tid", hour: "timme", minute: "minut" }, pt: { selected: "Selecionado:", day: "Dia", days: "Dias", apply: "Fechar", "week-1": "seg", "week-2": "ter", "week-3": "qua", "week-4": "qui", "week-5": "sex", "week-6": "sab", "week-7": "dom", "week-number": "N", "month-name": ["janeiro", "fevereiro", "março", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"], shortcuts: "Atalhos", "custom-values": "Valores Personalizados", past: "Passado", following: "Seguinte", previous: "Anterior", "prev-week": "Semana", "prev-month": "Mês", "prev-year": "Ano", next: "Próximo", "next-week": "Próxima Semana", "next-month": "Próximo Mês", "next-year": "Próximo Ano", "less-than": "O período selecionado não deve ser maior que %d dias", "more-than": "O período selecionado não deve ser menor que %d dias", "default-more": "Selecione um período superior a %d dias", "default-single": "Selecione uma data", "default-less": "Selecione um período inferior a %d dias", "default-range": "Selecione um período de %d a %d dias", "default-default": "Selecione um período", time: "Tempo", hour: "Hora", minute: "Minuto" } }, e.fn.dateRangePicker = function (a) {
	    function n(t, a) {
	      return a.contains(t.target) || t.target == a || void 0 != a.childNodes && e.inArray(t.target, a.childNodes) >= 0;
	    }function r() {
	      function r(t) {
	        var n = e(t).parents("table").hasClass("month2"),
	            r = n ? a.month2 : a.month1;r = F(r), !a.singleMonth && !a.singleDate && !n && N(r, a.month2) >= 0 || _(r) || (Y(r, n ? "month2" : "month1"), W());
	      }function i(e) {
	        var t = F(a.month1),
	            n = F(a.month2);_(n) || !a.singleDate && N(t, n) >= 0 || (Y(t, "month1"), Y(n, "month2"), S());
	      }function o(t) {
	        var n = e(t).parents("table").hasClass("month2"),
	            r = n ? a.month2 : a.month1;r = H(r), n && N(r, a.month1) <= 0 || _(r) || (Y(r, n ? "month2" : "month1"), W());
	      }function d(e) {
	        var t = H(a.month1),
	            n = H(a.month2);_(t) || !a.singleDate && N(n, t) <= 0 || (Y(n, "month2"), Y(t, "month1"), S());
	      }var l = this;if (e(this).data("date-picker-opened")) return void A();e(this).data("date-picker-opened", !0), re = q().hide(), re.append('<div class="date-range-length-tip"></div>'), re.delegate(".day", "mouseleave", function () {
	        re.find(".date-range-length-tip").hide(), a.singleDate && x();
	      }), e(a.container).append(re), a.inline ? re.addClass("inline-wrapper") : s(), a.alwaysOpen && re.find(".apply-btn").hide();var m = ae();if (ne(m), a.time.enabled) if (a.startDate && a.endDate || a.start && a.end) P(t(a.start || a.startDate).toDate(), "time1"), P(t(a.end || a.endDate).toDate(), "time2");else {
	        var c = a.defaultEndTime ? a.defaultEndTime : m;P(m, "time1"), P(c, "time2");
	      }var h = "";h = te(a.singleDate ? "default-single" : a.minDays && a.maxDays ? "default-range" : a.minDays ? "default-more" : a.maxDays ? "default-less" : "default-default"), re.find(".default-top").html(h.replace(/\%d/, a.minDays).replace(/\%d/, a.maxDays)), a.singleMonth ? re.addClass("single-month") : re.addClass("two-months"), setTimeout(function () {
	        u(), oe = !0;
	      }, 0), re.click(function (e) {
	        e.stopPropagation();
	      }), e(document).bind("click.datepicker", function (e) {
	        n(e, l[0]) || re.is(":visible") && A();
	      }), re.find(".next").click(function () {
	        a.stickyMonths ? i(this) : r(this);
	      }), re.find(".prev").click(function () {
	        a.stickyMonths ? d(this) : o(this);
	      }), re.delegate(".day", "click", function (t) {
	        k(e(this));
	      }), re.delegate(".day", "mouseenter", function (t) {
	        D(e(this));
	      }), re.delegate(".week-number", "click", function (t) {
	        y(e(this));
	      }), re.attr("unselectable", "on").css("user-select", "none").bind("selectstart", function (e) {
	        return e.preventDefault(), !1;
	      }), re.find(".apply-btn").click(function () {
	        A();var t = V(new Date(a.start)) + a.separator + V(new Date(a.end));e(l).trigger("datepicker-apply", { value: t, date1: new Date(a.start), date2: new Date(a.end) });
	      }), re.find("[custom]").click(function () {
	        var t = e(this).attr("custom");a.start = !1, a.end = !1, re.find(".day.checked").removeClass("checked"), a.setValue.call(le, t), z(), C(!0), S(), a.autoClose && A();
	      }), re.find("[shortcut]").click(function () {
	        var t,
	            n = e(this).attr("shortcut"),
	            r = new Date(),
	            s = !1;if (-1 != n.indexOf("day")) {
	          var i = parseInt(n.split(",", 2)[1], 10);s = new Date(new Date().getTime() + 864e5 * i), r = new Date(r.getTime() + 864e5 * (i > 0 ? 1 : -1));
	        } else if (-1 != n.indexOf("week")) {
	          t = -1 != n.indexOf("prev,") ? -1 : 1;var o;for (o = 1 == t ? "monday" == a.startOfWeek ? 1 : 0 : "monday" == a.startOfWeek ? 0 : 6, r = new Date(r.getTime() - 864e5); r.getDay() != o;) r = new Date(r.getTime() + 864e5 * t);s = new Date(r.getTime() + 864e5 * t * 6);
	        } else if (-1 != n.indexOf("month")) t = -1 != n.indexOf("prev,") ? -1 : 1, s = 1 == t ? F(r) : H(r), s.setDate(1), r = F(s), r.setDate(1), r = new Date(r.getTime() - 864e5);else if (-1 != n.indexOf("year")) t = -1 != n.indexOf("prev,") ? -1 : 1, s = new Date(), s.setFullYear(r.getFullYear() + t), s.setMonth(0), s.setDate(1), r.setFullYear(r.getFullYear() + t), r.setMonth(11), r.setDate(31);else if ("custom" == n) {
	          var d = e(this).html();if (a.customShortcuts && a.customShortcuts.length > 0) for (var l = 0; l < a.customShortcuts.length; l++) {
	            var u = a.customShortcuts[l];if (u.name == d) {
	              var m = [];if (m = u.dates.call(), m && 2 == m.length && (s = m[0], r = m[1]), m && 1 == m.length) {
	                var c = m[0];Y(c, "month1"), Y(F(c), "month2"), W();
	              }break;
	            }
	          }
	        }s && r && (T(s, r), z());
	      }), re.find(".time1 input[type=range]").bind("change touchmove mousemove", function (t) {
	        var a = t.target,
	            n = "hour" == a.name ? e(a).val().replace(/^(\d{1})$/, "0$1") : void 0,
	            r = "minute" == a.name ? e(a).val().replace(/^(\d{1})$/, "0$1") : void 0;p("time1", n, r);
	      }), re.find(".time2 input[type=range]").bind("change touchmove mousemove", function (t) {
	        var a = t.target,
	            n = "hour" == a.name ? e(a).val().replace(/^(\d{1})$/, "0$1") : void 0,
	            r = "minute" == a.name ? e(a).val().replace(/^(\d{1})$/, "0$1") : void 0;p("time2", n, r);
	      });
	    }function s() {
	      if (!a.inline) {
	        var t = e(de).offset();if ("relative" == e(a.container).css("position")) {
	          var n = e(a.container).offset();re.css({ top: t.top - n.top + e(de).outerHeight() + 4, left: t.left - n.left });
	        } else t.left < 460 ? re.css({ top: t.top + e(de).outerHeight() + parseInt(e("body").css("border-top") || 0, 10), left: t.left }) : re.css({ top: t.top + e(de).outerHeight() + parseInt(e("body").css("border-top") || 0, 10), left: t.left + e(de).width() - re.width() - 16 });
	      }
	    }function i() {
	      return re;
	    }function o(t) {
	      s(), B(), d(), a.customOpenAnimation ? a.customOpenAnimation.call(re.get(0), function () {
	        e(de).trigger("datepicker-opened", { relatedTarget: re });
	      }) : re.slideDown(t, function () {
	        e(de).trigger("datepicker-opened", { relatedTarget: re });
	      }), e(de).trigger("datepicker-open", { relatedTarget: re }), W(), u();
	    }function d() {
	      var e = a.getValue.call(le),
	          n = e ? e.split(a.separator) : "";if (n && (1 == n.length && a.singleDate || n.length >= 2)) {
	        var r = a.format;r.match(/Do/) && (r = r.replace(/Do/, "D"), n[0] = n[0].replace(/(\d+)(th|nd|st)/, "$1"), n.length >= 2 && (n[1] = n[1].replace(/(\d+)(th|nd|st)/, "$1"))), oe = !1, n.length >= 2 ? T(l(n[0], r, t.locale(a.language)), l(n[1], r, t.locale(a.language))) : 1 == n.length && a.singleDate && j(l(n[0], r, t.locale(a.language))), oe = !0;
	      }
	    }function l(e, a, n) {
	      return t(e, a, n).isValid() ? t(e, a, n).toDate() : t().toDate();
	    }function u() {
	      var e = re.find(".gap").css("margin-left");e && (e = parseInt(e));var t = re.find(".month1").width(),
	          a = re.find(".gap").width() + (e ? 2 * e : 0),
	          n = re.find(".month2").width();re.find(".month-wrapper").width(t + a + n);
	    }function m(e, a) {
	      re.find("." + e + " input[type=range].hour-range").val(t(a).hours()), re.find("." + e + " input[type=range].minute-range").val(t(a).minutes()), p(e, t(a).format("HH"), t(a).format("mm"));
	    }function c(e, n) {
	      a[e] = parseInt(t(parseInt(n)).startOf("day").add(t(a[e + "Time"]).format("HH"), "h").add(t(a[e + "Time"]).format("mm"), "m").valueOf());
	    }function h() {
	      m("time1", a.start), m("time2", a.end);
	    }function p(e, n, r) {
	      function s(e, t) {
	        var s = t.format("HH"),
	            i = t.format("mm");a[e] = t.startOf("day").add(n || s, "h").add(r || i, "m").valueOf();
	      }switch (n && re.find("." + e + " .hour-val").text(n), r && re.find("." + e + " .minute-val").text(r), e) {case "time1":
	          a.start && s("start", t(a.start)), s("startTime", t(a.startTime || t().valueOf()));break;case "time2":
	          a.end && s("end", t(a.end)), s("endTime", t(a.endTime || t().valueOf()));}z(), C(), S();
	    }function f() {
	      a.start = !1, a.end = !1, re.find(".day.checked").removeClass("checked"), re.find(".day.last-date-selected").removeClass("last-date-selected"), re.find(".day.first-date-selected").removeClass("first-date-selected"), a.setValue.call(le, ""), z(), C(), S();
	    }function v(e) {
	      var n = e;return "week-range" === a.batchMode ? n = "monday" === a.startOfWeek ? t(parseInt(e)).startOf("isoweek").valueOf() : t(parseInt(e)).startOf("week").valueOf() : "month-range" === a.batchMode && (n = t(parseInt(e)).startOf("month").valueOf()), n;
	    }function g(e) {
	      var n = e;return "week-range" === a.batchMode ? n = "monday" === a.startOfWeek ? t(parseInt(e)).endOf("isoweek").valueOf() : t(parseInt(e)).endOf("week").valueOf() : "month-range" === a.batchMode && (n = t(parseInt(e)).endOf("month").valueOf()), n;
	    }function k(n) {
	      if (!n.hasClass("invalid")) {
	        var r = n.attr("time");if (n.addClass("checked"), a.singleDate ? (a.start = r, a.end = !1) : "week" === a.batchMode ? "monday" === a.startOfWeek ? (a.start = t(parseInt(r)).startOf("isoweek").valueOf(), a.end = t(parseInt(r)).endOf("isoweek").valueOf()) : (a.end = t(parseInt(r)).endOf("week").valueOf(), a.start = t(parseInt(r)).startOf("week").valueOf()) : "workweek" === a.batchMode ? (a.start = t(parseInt(r)).day(1).valueOf(), a.end = t(parseInt(r)).day(5).valueOf()) : "weekend" === a.batchMode ? (a.start = t(parseInt(r)).day(6).valueOf(), a.end = t(parseInt(r)).day(7).valueOf()) : "month" === a.batchMode ? (a.start = t(parseInt(r)).startOf("month").valueOf(), a.end = t(parseInt(r)).endOf("month").valueOf()) : a.start && a.end || !a.start && !a.end ? (a.start = v(r), a.end = !1) : a.start && (a.end = g(r), a.time.enabled && c("end", a.end)), a.time.enabled && (a.start && c("start", a.start), a.end && c("end", a.end)), !a.singleDate && a.start && a.end && a.start > a.end) {
	          var s = a.end;a.end = g(a.start), a.start = v(s), a.time.enabled && a.swapTime && h();
	        }a.start = parseInt(a.start), a.end = parseInt(a.end), x(), a.start && !a.end && (e(de).trigger("datepicker-first-date-selected", { date1: new Date(a.start) }), D(n)), b(r), z(), C(), S(), M();
	      }
	    }function y(e) {
	      var n,
	          r,
	          s = parseInt(e.attr("data-start-time"), 10);a.startWeek ? (re.find(".week-number-selected").removeClass("week-number-selected"), n = new Date(s < a.startWeek ? s : a.startWeek), r = new Date(s < a.startWeek ? a.startWeek : s), a.startWeek = !1, a.start = t(n).day("monday" == a.startOfWeek ? 1 : 0).valueOf(), a.end = t(r).day("monday" == a.startOfWeek ? 7 : 6).valueOf()) : (a.startWeek = s, e.addClass("week-number-selected"), n = new Date(s), a.start = t(n).day("monday" == a.startOfWeek ? 1 : 0).valueOf(), a.end = t(n).day("monday" == a.startOfWeek ? 7 : 6).valueOf()), b(), z(), C(), S(), M();
	    }function w(e) {
	      if (e = parseInt(e, 10), a.startDate && L(e, a.startDate) < 0) return !1;if (a.endDate && L(e, a.endDate) > 0) return !1;if (a.start && !a.end && !a.singleDate) {
	        if (a.maxDays > 0 && O(e, a.start) > a.maxDays) return !1;if (a.minDays > 0 && O(e, a.start) < a.minDays) return !1;if (a.selectForward && e < a.start) return !1;if (a.selectBackward && e > a.start) return !1;if (a.beforeShowDay && "function" == typeof a.beforeShowDay) {
	          for (var t = !0, n = e; O(n, a.start) > 1;) {
	            var r = a.beforeShowDay(new Date(n));if (!r[0]) {
	              t = !1;break;
	            }if (Math.abs(n - a.start) < 864e5) break;n > a.start && (n -= 864e5), n < a.start && (n += 864e5);
	          }if (!t) return !1;
	        }
	      }return !0;
	    }function b() {
	      return re.find(".day.invalid.tmp").removeClass("tmp invalid").addClass("valid"), a.start && !a.end && re.find(".day.toMonth.valid").each(function () {
	        var t = parseInt(e(this).attr("time"), 10);w(t) ? e(this).addClass("valid tmp").removeClass("invalid") : e(this).addClass("invalid tmp").removeClass("valid");
	      }), !0;
	    }function D(t) {
	      var n = parseInt(t.attr("time")),
	          r = "";if (t.hasClass("has-tooltip") && t.attr("data-tooltip")) r = '<span style="white-space:nowrap">' + t.attr("data-tooltip") + "</span>";else if (!t.hasClass("invalid")) if (a.singleDate) re.find(".day.hovering").removeClass("hovering"), t.addClass("hovering");else if (re.find(".day").each(function () {
	        var t = parseInt(e(this).attr("time"));a.start, a.end;t == n ? e(this).addClass("hovering") : e(this).removeClass("hovering"), a.start && !a.end && (a.start < t && n >= t || a.start > t && t >= n) ? e(this).addClass("hovering") : e(this).removeClass("hovering");
	      }), a.start && !a.end) {
	        var s = O(n, a.start);a.hoveringTooltip && ("function" == typeof a.hoveringTooltip ? r = a.hoveringTooltip(s, a.start, n) : a.hoveringTooltip === !0 && s > 1 && (r = s + " " + te("days")));
	      }if (r) {
	        var i = t.offset(),
	            o = re.offset(),
	            d = i.left - o.left,
	            l = i.top - o.top;d += t.width() / 2;var u = re.find(".date-range-length-tip"),
	            m = u.css({ visibility: "hidden", display: "none" }).html(r).width(),
	            c = u.height();d -= m / 2, l -= c, setTimeout(function () {
	          u.css({ left: d, top: l, display: "block", visibility: "visible" });
	        }, 10);
	      } else re.find(".date-range-length-tip").hide();
	    }function x() {
	      re.find(".day.hovering").removeClass("hovering"), re.find(".date-range-length-tip").hide();
	    }function M() {
	      a.singleDate === !0 ? oe && a.start && a.autoClose && A() : oe && a.start && a.end && a.autoClose && A();
	    }function z() {
	      var e = Math.ceil((a.end - a.start) / 864e5) + 1;a.singleDate ? a.start && !a.end ? re.find(".drp_top-bar").removeClass("error").addClass("normal") : re.find(".drp_top-bar").removeClass("error").removeClass("normal") : a.maxDays && e > a.maxDays ? (a.start = !1, a.end = !1, re.find(".day").removeClass("checked"), re.find(".drp_top-bar").removeClass("normal").addClass("error").find(".error-top").html(te("less-than").replace("%d", a.maxDays))) : a.minDays && e < a.minDays ? (a.start = !1, a.end = !1, re.find(".day").removeClass("checked"), re.find(".drp_top-bar").removeClass("normal").addClass("error").find(".error-top").html(te("more-than").replace("%d", a.minDays))) : a.start || a.end ? re.find(".drp_top-bar").removeClass("error").addClass("normal") : re.find(".drp_top-bar").removeClass("error").removeClass("normal"), a.singleDate && a.start && !a.end || !a.singleDate && a.start && a.end ? re.find(".apply-btn").removeClass("disabled") : re.find(".apply-btn").addClass("disabled"), a.batchMode && (a.start && a.startDate && L(a.start, a.startDate) < 0 || a.end && a.endDate && L(a.end, a.endDate) > 0) && (a.start = !1, a.end = !1, re.find(".day").removeClass("checked"));
	    }function C(t, n) {
	      re.find(".start-day").html("..."), re.find(".end-day").html("..."), re.find(".selected-days").hide(), a.start && re.find(".start-day").html(V(new Date(parseInt(a.start)))), a.end && re.find(".end-day").html(V(new Date(parseInt(a.end))));var r;a.start && a.singleDate ? (re.find(".apply-btn").removeClass("disabled"), r = V(new Date(a.start)), a.setValue.call(le, r, V(new Date(a.start)), V(new Date(a.end))), oe && !n && e(de).trigger("datepicker-change", { value: r, date1: new Date(a.start) })) : a.start && a.end ? (re.find(".selected-days").show().find(".selected-days-num").html(O(a.end, a.start)), re.find(".apply-btn").removeClass("disabled"), r = V(new Date(a.start)) + a.separator + V(new Date(a.end)), a.setValue.call(le, r, V(new Date(a.start)), V(new Date(a.end))), oe && !n && e(de).trigger("datepicker-change", { value: r, date1: new Date(a.start), date2: new Date(a.end) })) : t ? re.find(".apply-btn").removeClass("disabled") : re.find(".apply-btn").addClass("disabled");
	    }function O(e, t) {
	      return Math.abs(Q(e) - Q(t)) + 1;
	    }function T(e, t, n) {
	      if (e.getTime() > t.getTime()) {
	        var r = t;t = e, e = r, r = null;
	      }var s = !0;return a.startDate && L(e, a.startDate) < 0 && (s = !1), a.endDate && L(t, a.endDate) > 0 && (s = !1), s ? (a.start = e.getTime(), a.end = t.getTime(), a.time.enabled && (m("time1", e), m("time2", t)), (a.stickyMonths || L(e, t) > 0 && 0 === N(e, t)) && (a.lookBehind ? e = H(t) : t = F(e)), a.stickyMonths && N(t, a.endDate) > 0 && (e = H(e), t = H(t)), a.stickyMonths || 0 === N(e, t) && (a.lookBehind ? e = H(t) : t = F(e)), Y(e, "month1"), Y(t, "month2"), W(), z(), C(!1, n), void M()) : (Y(a.startDate, "month1"), Y(F(a.startDate), "month2"), void W());
	    }function j(e) {
	      var t = !0;if (a.startDate && L(e, a.startDate) < 0 && (t = !1), a.endDate && L(e, a.endDate) > 0 && (t = !1), !t) return void Y(a.startDate, "month1");if (a.start = e.getTime(), a.time.enabled && m("time1", e), Y(e, "month1"), a.singleMonth !== !0) {
	        var n = F(e);Y(n, "month2");
	      }W(), C(), M();
	    }function S() {
	      (a.start || a.end) && (re.find(".day").each(function () {
	        var n = parseInt(e(this).attr("time")),
	            r = a.start,
	            s = a.end;a.time.enabled && (n = t(n).startOf("day").valueOf(), r = t(r || t().valueOf()).startOf("day").valueOf(), s = t(s || t().valueOf()).startOf("day").valueOf()), a.start && a.end && s >= n && n >= r || a.start && !a.end && t(r).format("YYYY-MM-DD") == t(n).format("YYYY-MM-DD") ? e(this).addClass("checked") : e(this).removeClass("checked"), a.start && t(r).format("YYYY-MM-DD") == t(n).format("YYYY-MM-DD") ? e(this).addClass("first-date-selected") : e(this).removeClass("first-date-selected"), a.end && t(s).format("YYYY-MM-DD") == t(n).format("YYYY-MM-DD") ? e(this).addClass("last-date-selected") : e(this).removeClass("last-date-selected");
	      }), re.find(".week-number").each(function () {
	        e(this).attr("data-start-time") == a.startWeek && e(this).addClass("week-number-selected");
	      }));
	    }function Y(e, n) {
	      e = t(e).toDate();var r = I(e.getMonth());re.find("." + n + " .month-name").html(r + " " + e.getFullYear()), re.find("." + n + " tbody").html(Z(e)), a[n] = e, b();
	    }function P(e, t) {
	      re.find("." + t).append(R()), m(t, e);
	    }function I(e) {
	      return te("month-name")[e];
	    }function V(e) {
	      return t(e).format(a.format);
	    }function W() {
	      S();var e = parseInt(t(a.month1).format("YYYYMM")),
	          n = parseInt(t(a.month2).format("YYYYMM")),
	          r = Math.abs(e - n),
	          s = r > 1 && 89 != r;s ? re.addClass("has-gap").removeClass("no-gap").find(".gap").css("visibility", "visible") : re.removeClass("has-gap").addClass("no-gap").find(".gap").css("visibility", "hidden");var i = re.find("table.month1").height(),
	          o = re.find("table.month2").height();re.find(".gap").height(Math.max(i, o) + 10);
	    }function A() {
	      if (!a.alwaysOpen) {
	        var t = function () {
	          e(de).data("date-picker-opened", !1), e(de).trigger("datepicker-closed", { relatedTarget: re });
	        };a.customCloseAnimation ? a.customCloseAnimation.call(re.get(0), t) : e(re).slideUp(a.duration, t), e(de).trigger("datepicker-close", { relatedTarget: re });
	      }
	    }function B() {
	      Y(a.month1, "month1"), Y(a.month2, "month2");
	    }function N(e, a) {
	      var n = parseInt(t(e).format("YYYYMM")) - parseInt(t(a).format("YYYYMM"));return n > 0 ? 1 : 0 === n ? 0 : -1;
	    }function L(e, a) {
	      var n = parseInt(t(e).format("YYYYMMDD")) - parseInt(t(a).format("YYYYMMDD"));return n > 0 ? 1 : 0 === n ? 0 : -1;
	    }function F(e) {
	      return t(e).add(1, "months").toDate();
	    }function H(e) {
	      return t(e).add(-1, "months").toDate();
	    }function R() {
	      return "<div><span>" + te("Time") + ': <span class="hour-val">00</span>:<span class="minute-val">00</span></span></div><div class="hour"><label>' + te("Hour") + ': <input type="range" class="hour-range" name="hour" min="0" max="23"></label></div><div class="minute"><label>' + te("Minute") + ': <input type="range" class="minute-range" name="minute" min="0" max="59"></label></div>';
	    }function q() {
	      var t = '<div class="date-picker-wrapper';a.extraClass && (t += " " + a.extraClass + " "), a.singleDate && (t += " single-date "), a.showShortcuts || (t += " no-shortcuts "), a.showTopbar || (t += " no-topbar "), a.customTopBar && (t += " custom-topbar "), t += '">', a.showTopbar && (t += '<div class="drp_top-bar">', a.customTopBar ? ("function" == typeof a.customTopBar && (a.customTopBar = a.customTopBar()), t += '<div class="custom-top">' + a.customTopBar + "</div>") : (t += '<div class="normal-top"><span style="color:#333">' + te("selected") + ' </span> <b class="start-day">...</b>', a.singleDate || (t += ' <span class="separator-day">' + a.separator + '</span> <b class="end-day">...</b> <i class="selected-days">(<span class="selected-days-num">3</span> ' + te("days") + ")</i>"), t += "</div>", t += '<div class="error-top">error</div><div class="default-top">default</div>'), t += '<input type="button" class="apply-btn disabled' + $() + '" value="' + te("apply") + '" />', t += "</div>");var n = a.showWeekNumbers ? 6 : 5;if (t += '<div class="month-wrapper"><table class="month1" cellspacing="0" border="0" cellpadding="0"><thead><tr class="caption"><th style="width:27px;"><span class="prev">&lt;</span></th><th colspan="' + n + '" class="month-name"></th><th style="width:27px;">' + (a.singleDate || !a.stickyMonths ? '<span class="next">&gt;</span>' : "") + '</th></tr><tr class="week-name">' + E() + "</thead><tbody></tbody></table>", G() && (t += '<div class="gap">' + J() + '</div><table class="month2" cellspacing="0" border="0" cellpadding="0"><thead><tr class="caption"><th style="width:27px;">' + (a.stickyMonths ? "" : '<span class="prev">&lt;</span>') + '</th><th colspan="' + n + '" class="month-name"></th><th style="width:27px;"><span class="next">&gt;</span></th></tr><tr class="week-name">' + E() + "</thead><tbody></tbody></table>"), t += '<div style="clear:both;height:0;font-size:0;"></div><div class="time"><div class="time1"></div>', a.singleDate || (t += '<div class="time2"></div>'), t += '</div><div style="clear:both;height:0;font-size:0;"></div></div>', t += '<div class="footer">', a.showShortcuts) {
	        t += '<div class="shortcuts"><b>' + te("shortcuts") + "</b>";var r = a.shortcuts;if (r) {
	          var s;if (r["prev-days"] && r["prev-days"].length > 0) {
	            t += '&nbsp;<span class="prev-days">' + te("past");for (var i = 0; i < r["prev-days"].length; i++) s = r["prev-days"][i], s += te(r["prev-days"][i] > 1 ? "days" : "day"), t += ' <a href="javascript:;" shortcut="day,-' + r["prev-days"][i] + '">' + s + "</a>";t += "</span>";
	          }if (r["next-days"] && r["next-days"].length > 0) {
	            t += '&nbsp;<span class="next-days">' + te("following");for (var i = 0; i < r["next-days"].length; i++) s = r["next-days"][i], s += te(r["next-days"][i] > 1 ? "days" : "day"), t += ' <a href="javascript:;" shortcut="day,' + r["next-days"][i] + '">' + s + "</a>";t += "</span>";
	          }if (r.prev && r.prev.length > 0) {
	            t += '&nbsp;<span class="prev-buttons">' + te("previous");for (var i = 0; i < r.prev.length; i++) s = te("prev-" + r.prev[i]), t += ' <a href="javascript:;" shortcut="prev,' + r.prev[i] + '">' + s + "</a>";t += "</span>";
	          }if (r.next && r.next.length > 0) {
	            t += '&nbsp;<span class="next-buttons">' + te("next");for (var i = 0; i < r.next.length; i++) s = te("next-" + r.next[i]), t += ' <a href="javascript:;" shortcut="next,' + r.next[i] + '">' + s + "</a>";t += "</span>";
	          }
	        }if (a.customShortcuts) for (var i = 0; i < a.customShortcuts.length; i++) {
	          var o = a.customShortcuts[i];t += '&nbsp;<span class="custom-shortcut"><a href="javascript:;" shortcut="custom">' + o.name + "</a></span>";
	        }t += "</div>";
	      }if (a.showCustomValues && (t += '<div class="customValues"><b>' + (a.customValueLabel || te("custom-values")) + "</b>", a.customValues)) for (var i = 0; i < a.customValues.length; i++) {
	        var d = a.customValues[i];t += '&nbsp;<span class="custom-value"><a href="javascript:;" custom="' + d.value + '">' + d.name + "</a></span>";
	      }return t += "</div></div>", e(t);
	    }function $() {
	      var e = "";return a.autoClose === !0 && (e += " hide"), "" !== a.applyBtnClass && (e += " " + a.applyBtnClass), e;
	    }function E() {
	      var e = a.showWeekNumbers ? "<th>" + te("week-number") + "</th>" : "";return "monday" == a.startOfWeek ? e + "<th>" + te("week-1") + "</th><th>" + te("week-2") + "</th><th>" + te("week-3") + "</th><th>" + te("week-4") + "</th><th>" + te("week-5") + "</th><th>" + te("week-6") + "</th><th>" + te("week-7") + "</th>" : e + "<th>" + te("week-7") + "</th><th>" + te("week-1") + "</th><th>" + te("week-2") + "</th><th>" + te("week-3") + "</th><th>" + te("week-4") + "</th><th>" + te("week-5") + "</th><th>" + te("week-6") + "</th>";
	    }function _(e) {
	      return e = t(e), a.startDate && e.endOf("month").isBefore(a.startDate) ? !0 : !(!a.endDate || !e.startOf("month").isAfter(a.endDate));
	    }function J() {
	      for (var e = ['<div class="gap-top-mask"></div><div class="gap-bottom-mask"></div><div class="gap-lines">'], t = 0; 20 > t; t++) e.push('<div class="gap-line"><div class="gap-1"></div><div class="gap-2"></div><div class="gap-3"></div></div>');return e.push("</div>"), e.join("");
	    }function G() {
	      return !a.singleMonth;
	    }function K(e, t, a) {
	      var n = jQuery.extend(!0, {}, e);jQuery.each(t, function (e, t) {
	        var r = t(a);for (var s in r) n.hasOwnProperty(s) ? n[s] += r[s] : n[s] = r[s];
	      });var r = "";for (var s in n) n.hasOwnProperty(s) && (r += s + '="' + n[s] + '" ');return r;
	    }function Q(e) {
	      return Math.floor(U(e) / 864e5);
	    }function U(e) {
	      return t.isMoment(e) && (e = e.toDate().getTime()), "object" == typeof e && e.getTime && (e = e.getTime()), "string" != typeof e || e.match(/\d{13}/) || (e = t(e, a.format).toDate().getTime()), e = parseInt(e, 10) - 60 * new Date().getTimezoneOffset() * 1e3;
	    }function Z(e) {
	      var n = [];e.setDate(1);var r = (new Date(e.getTime() - 864e5), new Date()),
	          s = e.getDay();0 === s && "monday" === a.startOfWeek && (s = 7);var i, o;if (s > 0) for (var d = s; d > 0; d--) {
	        var l = new Date(e.getTime() - 864e5 * d);o = w(l.getTime()), a.startDate && L(l, a.startDate) < 0 && (o = !1), a.endDate && L(l, a.endDate) > 0 && (o = !1), n.push({ date: l, type: "lastMonth", day: l.getDate(), time: l.getTime(), valid: o });
	      }for (var u = e.getMonth(), d = 0; 40 > d; d++) i = t(e).add(d, "days").toDate(), o = w(i.getTime()), a.startDate && L(i, a.startDate) < 0 && (o = !1), a.endDate && L(i, a.endDate) > 0 && (o = !1), n.push({ date: i, type: i.getMonth() == u ? "toMonth" : "nextMonth", day: i.getDate(), time: i.getTime(), valid: o });for (var m = [], c = 0; 6 > c && "nextMonth" != n[7 * c].type; c++) {
	        m.push("<tr>");for (var l = 0; 7 > l; l++) {
	          var h = "monday" == a.startOfWeek ? l + 1 : l;i = n[7 * c + h];var p = t(i.time).format("L") == t(r).format("L");if (i.extraClass = "", i.tooltip = "", i.valid && a.beforeShowDay && "function" == typeof a.beforeShowDay) {
	            var f = a.beforeShowDay(t(i.time).toDate());i.valid = f[0], i.extraClass = f[1] || "", i.tooltip = f[2] || "", "" !== i.tooltip && (i.extraClass += " has-tooltip ");
	          }var v = { time: i.time, "data-tooltip": i.tooltip, "class": "day " + i.type + " " + i.extraClass + " " + (i.valid ? "valid" : "invalid") + " " + (p ? "real-today" : "") };0 === l && a.showWeekNumbers && m.push('<td><div class="week-number" data-start-time="' + i.time + '">' + a.getWeekNumber(i.date) + "</div></td>"), m.push("<td " + K({}, a.dayTdAttrs, i) + "><div " + K(v, a.dayDivAttrs, i) + ">" + X(i.time, i.day) + "</div></td>");
	        }m.push("</tr>");
	      }return m.join("");
	    }function X(e, t) {
	      return a.showDateFilter && "function" == typeof a.showDateFilter ? a.showDateFilter(e, t) : t;
	    }function ee() {
	      if ("auto" == a.language) {
	        var t = navigator.language ? navigator.language : navigator.browserLanguage;if (!t) return e.dateRangePickerLanguages["default"];t = t.toLowerCase();for (var n in e.dateRangePickerLanguages) if (-1 !== t.indexOf(n)) return e.dateRangePickerLanguages[n];return e.dateRangePickerLanguages["default"];
	      }return a.language && a.language in e.dateRangePickerLanguages ? e.dateRangePickerLanguages[a.language] : e.dateRangePickerLanguages["default"];
	    }function te(t) {
	      var a = t.toLowerCase(),
	          n = t in ie ? ie[t] : a in ie ? ie[a] : null,
	          r = e.dateRangePickerLanguages["default"];return null == n && (n = t in r ? r[t] : a in r ? r[a] : ""), n;
	    }function ae() {
	      var e = a.defaultTime ? a.defaultTime : new Date();return a.lookBehind ? (a.startDate && N(e, a.startDate) < 0 && (e = F(t(a.startDate).toDate())), a.endDate && N(e, a.endDate) > 0 && (e = t(a.endDate).toDate())) : (a.startDate && N(e, a.startDate) < 0 && (e = t(a.startDate).toDate()), a.endDate && N(F(e), a.endDate) > 0 && (e = H(t(a.endDate).toDate()))), a.singleDate && (a.startDate && N(e, a.startDate) < 0 && (e = t(a.startDate).toDate()), a.endDate && N(e, a.endDate) > 0 && (e = t(a.endDate).toDate())), e;
	    }function ne(e) {
	      e || (e = ae()), a.lookBehind ? (Y(H(e), "month1"), Y(e, "month2")) : (Y(e, "month1"), Y(F(e), "month2")), a.singleDate && Y(e, "month1"), S(), W();
	    }a || (a = {}), a = e.extend(!0, { autoClose: !1, format: "YYYY-MM-DD", separator: " to ", language: "auto", startOfWeek: "sunday", getValue: function () {
	        return e(this).val();
	      }, setValue: function (t) {
	        e(this).attr("readonly") || e(this).is(":disabled") || t == e(this).val() || e(this).val(t);
	      }, startDate: !1, endDate: !1, time: { enabled: !1 }, minDays: 0, maxDays: 0, showShortcuts: !1, shortcuts: {}, customShortcuts: [], inline: !1, container: "body", alwaysOpen: !1, singleDate: !1, lookBehind: !1, batchMode: !1, duration: 200, stickyMonths: !1, dayDivAttrs: [], dayTdAttrs: [], selectForward: !1, selectBackward: !1, applyBtnClass: "", singleMonth: "auto", hoveringTooltip: function (e, t, a) {
	        return e > 1 ? e + " " + te("days") : "";
	      }, showTopbar: !0, swapTime: !1, showWeekNumbers: !1, getWeekNumber: function (e) {
	        return t(e).format("w");
	      }, customOpenAnimation: null, customCloseAnimation: null }, a), a.start = !1, a.end = !1, a.startWeek = !1, a.isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints, a.isTouchDevice && (a.hoveringTooltip = !1), "auto" == a.singleMonth && (a.singleMonth = e(window).width() < 480), a.singleMonth && (a.stickyMonths = !1), a.showTopbar || (a.autoClose = !0), a.startDate && "string" == typeof a.startDate && (a.startDate = t(a.startDate, a.format).toDate()), a.endDate && "string" == typeof a.endDate && (a.endDate = t(a.endDate, a.format).toDate());var re,
	        se,
	        ie = ee(),
	        oe = !1,
	        de = this,
	        le = e(de).get(0);return e(this).unbind(".datepicker").bind("click.datepicker", function (e) {
	      var t = re.is(":visible");t || o(a.duration);
	    }).bind("change.datepicker", function (e) {
	      d();
	    }).bind("keyup.datepicker", function () {
	      try {
	        clearTimeout(se);
	      } catch (e) {}se = setTimeout(function () {
	        d();
	      }, 2e3);
	    }), r.call(this), a.alwaysOpen && o(0), e(this).data("dateRangePicker", { setDateRange: function (e, n, r) {
	        "string" == typeof e && "string" == typeof n && (e = t(e, a.format).toDate(), n = t(n, a.format).toDate()), T(e, n, r);
	      }, clear: f, close: A, open: o, redraw: B, getDatePicker: i, resetMonthsView: ne, destroy: function () {
	        e(de).unbind(".datepicker"), e(de).data("dateRangePicker", ""), e(de).data("date-picker-opened", null), re.remove(), e(window).unbind("resize.datepicker", s), e(document).unbind("click.datepicker", A);
	      } }), e(window).bind("resize.datepicker", s), this;
	  };
	});

/***/ },
/* 38 */,
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	var baseAssignTable = __webpack_require__(25);

	module.exports = function (containerSelector, id, mediator) {

	    this.firstSelectSelector = '[name=zone_id]';
	    this.secondSelectSelector = '[name=priority_list_id]';
	    this.firstSource = '/api/v1/zones';
	    this.secondSource = '/api/v1/priority-lists';
	    this.assignmentSource = '/api/v1/retail-chains';
	    this.firstParamName = 'Zone';
	    this.secondParamName = 'Priority list';
	    this.firstParamIdName = 'zone_id';
	    this.secondParamIdName = 'priority_list_id';
	    this.firstObjectName = 'zone';
	    this.secondObjectName = 'priority_list';

	    baseAssignTable.call(this, containerSelector, id, mediator);
	};

/***/ },
/* 40 */
/***/ function(module, exports, __webpack_require__) {

	var baseAssignTable = __webpack_require__(25);

	module.exports = function (containerSelector, id, mediator) {

	    this.firstSelectSelector = '[name=retail_chain_id]';
	    this.secondSelectSelector = '[name=priority_list_id]';
	    this.firstSource = '/api/v1/retail-chains';
	    this.secondSource = '/api/v1/priority-lists';
	    this.assignmentSource = '/api/v1/zones';
	    this.firstParamName = 'Retail chain';
	    this.secondParamName = 'Priority list';
	    this.firstParamIdName = 'retail_chain_id';
	    this.secondParamIdName = 'priority_list_id';
	    this.firstObjectName = 'retail_chain';
	    this.secondObjectName = 'priority_list';

	    baseAssignTable.call(this, containerSelector, id, mediator);
	};

/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(1);
	var loadGoogleMapsAPI = __webpack_require__(85).default;

	module.exports = function (selector, addressSelector, defaultCenter, mediator) {

	    var that = this;
	    this.mapsInstance = null;
	    defaultCenter = defaultCenter || {
	        lat: 55.33948, lng: 9.55373
	    };

	    addressSelector = addressSelector || '[name=address]';

	    this.container = $(selector);
	    this.inputs = {
	        lng: $('[name=lng]', that.container),
	        ltd: $('[name=ltd]', that.container)
	    };
	    this.map = null;
	    this.marker = null;
	    this.addressInput = $(addressSelector)[0];

	    this.isInitialValuesSet = function () {
	        return that.inputs.lng.val().length && that.inputs.ltd.val().length;
	    };

	    this.getCurrentCoordinates = function () {
	        if (that.inputs.lng.val().length && that.inputs.ltd.val().length) {
	            return {
	                lat: parseFloat(that.inputs.ltd.val()),
	                lng: parseFloat(that.inputs.lng.val())
	            };
	        } else {
	            return defaultCenter;
	        }
	    };

	    this.setCoordinates = function (coordinates) {
	        that.inputs.lng.val(coordinates.lng);
	        that.inputs.ltd.val(coordinates.lat);

	        if (that.marker) {
	            that.marker.setVisible(true);
	            that.marker.setPosition(coordinates);
	        }
	    };

	    this.resetCoordinates = function () {
	        that.inputs.lng.val('');
	        that.inputs.ltd.val('');

	        if (that.marker) {
	            that.marker.setVisible(false);
	        }
	    };

	    this.getAddressInput = function () {
	        return that.addressInput;
	    };

	    $(that.addressInput).on('keyup keypress', function (e) {
	        var keyCode = e.keyCode || e.which;
	        if (keyCode === 13) {
	            e.preventDefault();
	            return false;
	        }
	    });

	    loadGoogleMapsAPI({
	        key: 'AIzaSyDyXX_hQFlAwLw7duFDdkPQFDfgxsQpWH4',
	        v: '3.23',
	        libraries: ['places']
	    }).then(function (maps) {
	        that.mapsInstance = maps;
	        that.map = new that.mapsInstance.Map($('.map-container', that.container)[0], {
	            center: that.getCurrentCoordinates(),
	            zoom: 13
	        });

	        that.createMarker(that.getCurrentCoordinates());

	        if (that.isInitialValuesSet()) {
	            that.setCoordinates(that.getCurrentCoordinates());
	        } else {
	            that.map.setZoom(8);
	        }

	        var autocomplete = new that.mapsInstance.places.Autocomplete(that.getAddressInput());
	        autocomplete.bindTo('bounds', that.map);

	        autocomplete.addListener('place_changed', function () {
	            var place = autocomplete.getPlace();
	            if (!place.geometry) {
	                that.resetCoordinates();
	                return;
	            }

	            // If the place has a geometry, then present it on a map.
	            if (place.geometry.viewport) {
	                that.map.fitBounds(place.geometry.viewport);
	            } else {
	                that.map.setCenter(place.geometry.location);
	                that.map.setZoom(17); // Why 17? Because it looks good.
	            }

	            that.setCoordinates({
	                lng: parseFloat(place.geometry.location.lng().toFixed(5)),
	                lat: parseFloat(place.geometry.location.lat().toFixed(5))
	            });
	        });
	    });

	    that.createMarker = function (position) {
	        var marker = new that.mapsInstance.Marker({
	            map: that.map,
	            anchorPoint: new that.mapsInstance.Point(0, -29),
	            draggable: true
	        });
	        marker.setVisible(false);
	        marker.addListener('dragend', function (event) {
	            that.setCoordinates({
	                lng: parseFloat(event.latLng.lng().toFixed(5)),
	                lat: parseFloat(event.latLng.lat().toFixed(5))
	            });
	        });

	        that.marker = marker;
	        return marker;
	    };
	};

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(1);
	__webpack_require__(12);
	__webpack_require__(13);

	module.exports = function (sourceTableId) {
	    var table = $('#' + sourceTableId);

	    var datatable = table.DataTable({
	        "columns": [null, {
	            "orderable": false
	        }, {
	            "orderable": false
	        }],
	        "serverSide": true,
	        "ajax": function (data, callback) {
	            console.log(data);

	            var fields = {
	                0: 'id',
	                1: 'name'
	            };
	            var pageInfo = table.DataTable().page.info();

	            console.log(pageInfo);

	            var orderBy = {
	                field: fields[data.order[0].column],
	                direction: data.order[0].dir
	            };

	            var orderbyQuery = orderBy.field ? "&options[field]=" + orderBy.field + "&options[direction]=" + orderBy.direction : "";
	            var searchQuery = data.search.value.length ? "&options[searchPhrase]=" + data.search.value : '';

	            $.ajax({
	                method: "GET",
	                url: "/api/v1/priority-lists?options[perPage]=" + pageInfo.length + "&options[pageNum]=" + (pageInfo.page + 1) + orderbyQuery + searchQuery,
	                contentType: "application/json"
	            }).done(function (result) {
	                callback({
	                    data: result.items.map(function (v) {
	                        return [v.id, v.name, '<a href="/priority-lists/edit/' + v.id + '" class="btn btn-xs btn-black"><i class="entypo-pencil"></i> edit</a>'];
	                    }),
	                    recordsTotal: result.pagination.total_items,
	                    recordsFiltered: result.pagination.total_items
	                });
	            }).error(function (err) {});
	        },
	        "initComplete": function () {
	            $('select').select2({
	                minimumResultsForSearch: -1
	            });
	        }
	    });
	};

/***/ },
/* 43 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(1);
	__webpack_require__(12);
	__webpack_require__(13);

	module.exports = function (sourceTableId) {
	    var table = $('#' + sourceTableId);

	    var datatable = table.DataTable({
	        "columns": [null, null, null, {
	            "orderable": false
	        }, {
	            "orderable": false
	        }],
	        "serverSide": true,
	        "ajax": function (data, callback) {
	            console.log(data);

	            var fields = {
	                0: 'id',
	                1: 'sku',
	                2: 'name'
	            };
	            var pageInfo = table.DataTable().page.info();

	            console.log(pageInfo);

	            var orderBy = {
	                field: fields[data.order[0].column],
	                direction: data.order[0].dir
	            };

	            var orderbyQuery = orderBy.field ? "&options[field]=" + orderBy.field + "&options[direction]=" + orderBy.direction : "";
	            var searchQuery = data.search.value.length ? "&options[searchPhrase]=" + data.search.value : '';

	            $.ajax({
	                method: "GET",
	                url: "/api/v1/products?options[perPage]=" + pageInfo.length + "&options[pageNum]=" + (pageInfo.page + 1) + orderbyQuery + searchQuery,
	                contentType: "application/json"
	            }).done(function (result) {
	                callback({
	                    data: result.items.map(function (v) {
	                        return [v.id, v.sku, v.name, v.photo ? '<img src="' + v.photo.thumbnail_url + '" class="img-thumbnail" style="width: 100px; height: 100px;">' : 'no photo', '<a href="/products/edit/' + v.id + '" class="btn btn-xs btn-black"><i class="entypo-pencil"></i> edit</a>'];
	                    }),
	                    recordsTotal: result.pagination.total_items,
	                    recordsFiltered: result.pagination.total_items
	                });
	            }).error(function (err) {});
	        },
	        "initComplete": function () {
	            $('select').select2({
	                minimumResultsForSearch: -1
	            });
	        }
	    });
	};

/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(1);
	__webpack_require__(12);
	__webpack_require__(13);

	module.exports = function (sourceTableId) {
	    var table = $('#' + sourceTableId);

	    var datatable = table.DataTable({
	        "columns": [null, null, null, {
	            "orderable": false
	        }, {
	            "orderable": false
	        }],
	        "serverSide": true,
	        "ajax": function (data, callback) {
	            console.log(data);

	            var fields = {
	                0: 'id',
	                1: 'name',
	                2: 'address'
	            };
	            var pageInfo = table.DataTable().page.info();

	            console.log(pageInfo);

	            var orderBy = {
	                field: fields[data.order[0].column],
	                direction: data.order[0].dir
	            };

	            var orderbyQuery = orderBy.field ? "&options[field]=" + orderBy.field + "&options[direction]=" + orderBy.direction : "";
	            var searchQuery = data.search.value.length ? "&options[searchPhrase]=" + data.search.value : '';

	            $.ajax({
	                method: "GET",
	                url: "/api/v1/sale-points?options[perPage]=" + pageInfo.length + "&options[withRetailChain]=1&options[withZone]=1&options[pageNum]=" + (pageInfo.page + 1) + orderbyQuery + searchQuery,
	                contentType: "application/json"
	            }).done(function (result) {
	                callback({
	                    data: result.items.map(function (v) {
	                        return [v.id, v.name, v.address, (v.retail_chain ? v.retail_chain.name : '-') + '/' + (v.zone ? v.zone.name : '-'), '<a href="/sale-points/edit/' + v.id + '" class="btn btn-xs btn-black"><i class="entypo-pencil"></i> edit</a>'];
	                    }),
	                    recordsTotal: result.pagination.total_items,
	                    recordsFiltered: result.pagination.total_items
	                });
	            }).error(function (err) {});
	        },
	        "initComplete": function () {
	            $('select').select2({
	                minimumResultsForSearch: -1
	            });
	        }
	    });
	};

/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(1);
	__webpack_require__(12);
	__webpack_require__(13);

	module.exports = function (sourceTableId) {
	    var table = $('#' + sourceTableId);

	    var datatable = table.DataTable({
	        "columns": [null, {
	            "orderable": false
	        }, {
	            "orderable": false
	        }],
	        "serverSide": true,
	        "ajax": function (data, callback) {
	            console.log(data);

	            var fields = {
	                0: 'id',
	                1: 'name'
	            };
	            var pageInfo = table.DataTable().page.info();

	            console.log(pageInfo);

	            var orderBy = {
	                field: fields[data.order[0].column],
	                direction: data.order[0].dir
	            };

	            var orderbyQuery = orderBy.field ? "&options[field]=" + orderBy.field + "&options[direction]=" + orderBy.direction : "";
	            var searchQuery = data.search.value.length ? "&options[searchPhrase]=" + data.search.value : '';

	            $.ajax({
	                method: "GET",
	                url: "/api/v1/zones?options[perPage]=" + pageInfo.length + "&options[pageNum]=" + (pageInfo.page + 1) + orderbyQuery + searchQuery,
	                contentType: "application/json"
	            }).done(function (result) {
	                callback({
	                    data: result.items.map(function (v) {
	                        return [v.id, v.name, '<a href="/zones/edit/' + v.id + '" class="btn btn-xs btn-black"><i class="entypo-pencil"></i> edit</a>'];
	                    }),
	                    recordsTotal: result.pagination.total_items,
	                    recordsFiltered: result.pagination.total_items
	                });
	            }).error(function (err) {});
	        },
	        "initComplete": function () {
	            $('select').select2({
	                minimumResultsForSearch: -1
	            });
	        }
	    });
	};

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(1);
	__webpack_require__(12);
	__webpack_require__(13);

	module.exports = function (sourceTableId) {
	    var table = $('#' + sourceTableId);

	    var datatable = table.DataTable({
	        "columns": [null, null, {
	            "orderable": false
	        }, {
	            "orderable": false
	        }],
	        "serverSide": true,
	        "ajax": function (data, callback) {
	            console.log(data);

	            var fields = {
	                0: 'id',
	                1: 'name'
	            };
	            var pageInfo = table.DataTable().page.info();

	            console.log(pageInfo);

	            var orderBy = {
	                field: fields[data.order[0].column],
	                direction: data.order[0].dir
	            };

	            var orderbyQuery = orderBy.field ? "&options[field]=" + orderBy.field + "&options[direction]=" + orderBy.direction : "";
	            var searchQuery = data.search.value.length ? "&options[searchPhrase]=" + data.search.value : '';

	            $.ajax({
	                method: "GET",
	                url: "/api/v1/retail-chains?options[perPage]=" + pageInfo.length + "&options[pageNum]=" + (pageInfo.page + 1) + orderbyQuery + searchQuery,
	                contentType: "application/json"
	            }).done(function (result) {
	                callback({
	                    data: result.items.map(function (v) {
	                        return [v.id, v.name, v.logo ? '<img src="' + v.logo.thumbnailUrl + '" class="img-thumbnail" style="width: 100px; height: 100px;">' : '', '<a href="/retail-chains/edit/' + v.id + '" class="btn btn-xs btn-black"><i class="entypo-eye"></i> edit</a>'];
	                    }),
	                    recordsTotal: result.pagination.total_items,
	                    recordsFiltered: result.pagination.total_items
	                });
	            }).error(function (err) {});
	        },
	        "initComplete": function () {
	            $('select').select2({
	                minimumResultsForSearch: -1
	            });
	        }
	    });
	};

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	var Mediator = __webpack_require__(8);
	__webpack_require__(34);

	module.exports = function (container) {
	    var that = this;
	    new Mediator().installTo(this);

	    console.log(this);

	    var currentValue = null;
	    $('.search-phrase-input', container).on('keyup', $.debounce(function () {
	        currentValue = this.value;
	        that.publish('refresh', currentValue);
	    }, 200));

	    this.getCurrentValue = function () {
	        return currentValue;
	    };
	};

/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	var baseForm = __webpack_require__(11);
	var Select = __webpack_require__(16);
	var c = __webpack_require__(7);
	var CooridnatesPicker = __webpack_require__(41);

	module.exports = function (id, mode, mediator) {
	    baseForm.call(this, id, '#sale-point-general-form', '/sale-points', '/api/v1/sale-points', mode, mediator);

	    new Select('[name=retail_chain_id]', '/api/v1/retail-chains');
	    new Select('[name=zone_id]', '/api/v1/zones');
	    new CooridnatesPicker('.coordinates-picker');

	    this.setHandleErrorCallback(function (body) {
	        if (body.errorCode == c.VALIDATION_ERROR && (body.errorData.ltd || body.errorData.lng)) {
	            mediator.publish('showNotification', 'Sale point coordinates are required', null, false);
	        }
	    });
	};

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	var Mediator = __webpack_require__(8);

	module.exports = function (selector, url) {
	    new Mediator().installTo(this);

	    var that = this;

	    $(selector).select2({
	        minimumInputLength: 0,
	        multiple: true,
	        allowClear: true,
	        closeOnSelect: false,
	        ajax: {
	            url: url,
	            dataType: 'json',
	            quietMillis: 250,
	            data: function (term, page) {
	                // page is the one-based page number tracked by Select2
	                return {
	                    "options[searchPhrase]": term, //search term
	                    "options[pageNum]": page, // page number
	                    "options[pageTotal]": 30 // page number
	                };
	            },
	            results: function (data, page) {
	                var results = [];
	                $.each(data.items, function (index, item) {
	                    results.push({
	                        id: item.id,
	                        text: item.name
	                    });
	                });

	                var more = page * data.pagination.items_per_page_requested < data.pagination.total_items; // whether or not there are more results available

	                // notice we return the value of more so Select2 knows if more results can be loaded
	                return { results: results, more: more };
	            }
	        },
	        cache: true,
	        initSelection: function (element, callback) {
	            var elementText = $(element).attr('data-init-text');
	            callback({ "text": elementText, "id": elementText });
	        }
	    }).on('select2-selecting', function (val, choice) {});
	};

/***/ },
/* 50 */,
/* 51 */,
/* 52 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(3)();
	// imports


	// module
	exports.push([module.id, "/*\n * Table styles\n */\ntable.dataTable {\n  width: 100%;\n  margin: 0 auto;\n  clear: both;\n  border-collapse: separate;\n  border-spacing: 0;\n  /*\n   * Header and footer styles\n   */\n  /*\n   * Body styles\n   */\n}\ntable.dataTable thead th,\ntable.dataTable tfoot th {\n  font-weight: bold;\n}\ntable.dataTable thead th,\ntable.dataTable thead td {\n  padding: 10px 18px;\n  border-bottom: 1px solid #111;\n}\ntable.dataTable thead th:active,\ntable.dataTable thead td:active {\n  outline: none;\n}\ntable.dataTable tfoot th,\ntable.dataTable tfoot td {\n  padding: 10px 18px 6px 18px;\n  border-top: 1px solid #111;\n}\ntable.dataTable thead .sorting,\ntable.dataTable thead .sorting_asc,\ntable.dataTable thead .sorting_desc {\n  cursor: pointer;\n  *cursor: hand;\n}\ntable.dataTable thead .sorting,\ntable.dataTable thead .sorting_asc,\ntable.dataTable thead .sorting_desc,\ntable.dataTable thead .sorting_asc_disabled,\ntable.dataTable thead .sorting_desc_disabled {\n  background-repeat: no-repeat;\n  background-position: center right;\n}\ntable.dataTable thead .sorting {\n  background-image: url(" + __webpack_require__(66) + ");\n}\ntable.dataTable thead .sorting_asc {\n  background-image: url(" + __webpack_require__(64) + ");\n}\ntable.dataTable thead .sorting_desc {\n  background-image: url(" + __webpack_require__(67) + ");\n}\ntable.dataTable thead .sorting_asc_disabled {\n  background-image: url(" + __webpack_require__(65) + ");\n}\ntable.dataTable thead .sorting_desc_disabled {\n  background-image: url(" + __webpack_require__(68) + ");\n}\ntable.dataTable tbody tr {\n  background-color: #ffffff;\n}\ntable.dataTable tbody tr.selected {\n  background-color: #B0BED9;\n}\ntable.dataTable tbody th,\ntable.dataTable tbody td {\n  padding: 8px 10px;\n}\ntable.dataTable.row-border tbody th, table.dataTable.row-border tbody td, table.dataTable.display tbody th, table.dataTable.display tbody td {\n  border-top: 1px solid #ddd;\n}\ntable.dataTable.row-border tbody tr:first-child th,\ntable.dataTable.row-border tbody tr:first-child td, table.dataTable.display tbody tr:first-child th,\ntable.dataTable.display tbody tr:first-child td {\n  border-top: none;\n}\ntable.dataTable.cell-border tbody th, table.dataTable.cell-border tbody td {\n  border-top: 1px solid #ddd;\n  border-right: 1px solid #ddd;\n}\ntable.dataTable.cell-border tbody tr th:first-child,\ntable.dataTable.cell-border tbody tr td:first-child {\n  border-left: 1px solid #ddd;\n}\ntable.dataTable.cell-border tbody tr:first-child th,\ntable.dataTable.cell-border tbody tr:first-child td {\n  border-top: none;\n}\ntable.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {\n  background-color: #f9f9f9;\n}\ntable.dataTable.stripe tbody tr.odd.selected, table.dataTable.display tbody tr.odd.selected {\n  background-color: #acbad4;\n}\ntable.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {\n  background-color: #f6f6f6;\n}\ntable.dataTable.hover tbody tr:hover.selected, table.dataTable.display tbody tr:hover.selected {\n  background-color: #aab7d1;\n}\ntable.dataTable.order-column tbody tr > .sorting_1,\ntable.dataTable.order-column tbody tr > .sorting_2,\ntable.dataTable.order-column tbody tr > .sorting_3, table.dataTable.display tbody tr > .sorting_1,\ntable.dataTable.display tbody tr > .sorting_2,\ntable.dataTable.display tbody tr > .sorting_3 {\n  background-color: #fafafa;\n}\ntable.dataTable.order-column tbody tr.selected > .sorting_1,\ntable.dataTable.order-column tbody tr.selected > .sorting_2,\ntable.dataTable.order-column tbody tr.selected > .sorting_3, table.dataTable.display tbody tr.selected > .sorting_1,\ntable.dataTable.display tbody tr.selected > .sorting_2,\ntable.dataTable.display tbody tr.selected > .sorting_3 {\n  background-color: #acbad5;\n}\ntable.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1 {\n  background-color: #f1f1f1;\n}\ntable.dataTable.display tbody tr.odd > .sorting_2, table.dataTable.order-column.stripe tbody tr.odd > .sorting_2 {\n  background-color: #f3f3f3;\n}\ntable.dataTable.display tbody tr.odd > .sorting_3, table.dataTable.order-column.stripe tbody tr.odd > .sorting_3 {\n  background-color: whitesmoke;\n}\ntable.dataTable.display tbody tr.odd.selected > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd.selected > .sorting_1 {\n  background-color: #a6b4cd;\n}\ntable.dataTable.display tbody tr.odd.selected > .sorting_2, table.dataTable.order-column.stripe tbody tr.odd.selected > .sorting_2 {\n  background-color: #a8b5cf;\n}\ntable.dataTable.display tbody tr.odd.selected > .sorting_3, table.dataTable.order-column.stripe tbody tr.odd.selected > .sorting_3 {\n  background-color: #a9b7d1;\n}\ntable.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {\n  background-color: #fafafa;\n}\ntable.dataTable.display tbody tr.even > .sorting_2, table.dataTable.order-column.stripe tbody tr.even > .sorting_2 {\n  background-color: #fcfcfc;\n}\ntable.dataTable.display tbody tr.even > .sorting_3, table.dataTable.order-column.stripe tbody tr.even > .sorting_3 {\n  background-color: #fefefe;\n}\ntable.dataTable.display tbody tr.even.selected > .sorting_1, table.dataTable.order-column.stripe tbody tr.even.selected > .sorting_1 {\n  background-color: #acbad5;\n}\ntable.dataTable.display tbody tr.even.selected > .sorting_2, table.dataTable.order-column.stripe tbody tr.even.selected > .sorting_2 {\n  background-color: #aebcd6;\n}\ntable.dataTable.display tbody tr.even.selected > .sorting_3, table.dataTable.order-column.stripe tbody tr.even.selected > .sorting_3 {\n  background-color: #afbdd8;\n}\ntable.dataTable.display tbody tr:hover > .sorting_1, table.dataTable.order-column.hover tbody tr:hover > .sorting_1 {\n  background-color: #eaeaea;\n}\ntable.dataTable.display tbody tr:hover > .sorting_2, table.dataTable.order-column.hover tbody tr:hover > .sorting_2 {\n  background-color: #ececec;\n}\ntable.dataTable.display tbody tr:hover > .sorting_3, table.dataTable.order-column.hover tbody tr:hover > .sorting_3 {\n  background-color: #efefef;\n}\ntable.dataTable.display tbody tr:hover.selected > .sorting_1, table.dataTable.order-column.hover tbody tr:hover.selected > .sorting_1 {\n  background-color: #a2aec7;\n}\ntable.dataTable.display tbody tr:hover.selected > .sorting_2, table.dataTable.order-column.hover tbody tr:hover.selected > .sorting_2 {\n  background-color: #a3b0c9;\n}\ntable.dataTable.display tbody tr:hover.selected > .sorting_3, table.dataTable.order-column.hover tbody tr:hover.selected > .sorting_3 {\n  background-color: #a5b2cb;\n}\ntable.dataTable.no-footer {\n  border-bottom: 1px solid #111;\n}\ntable.dataTable.nowrap th, table.dataTable.nowrap td {\n  white-space: nowrap;\n}\ntable.dataTable.compact thead th,\ntable.dataTable.compact thead td {\n  padding: 4px 17px 4px 4px;\n}\ntable.dataTable.compact tfoot th,\ntable.dataTable.compact tfoot td {\n  padding: 4px;\n}\ntable.dataTable.compact tbody th,\ntable.dataTable.compact tbody td {\n  padding: 4px;\n}\ntable.dataTable th.dt-left,\ntable.dataTable td.dt-left {\n  text-align: left;\n}\ntable.dataTable th.dt-center,\ntable.dataTable td.dt-center,\ntable.dataTable td.dataTables_empty {\n  text-align: center;\n}\ntable.dataTable th.dt-right,\ntable.dataTable td.dt-right {\n  text-align: right;\n}\ntable.dataTable th.dt-justify,\ntable.dataTable td.dt-justify {\n  text-align: justify;\n}\ntable.dataTable th.dt-nowrap,\ntable.dataTable td.dt-nowrap {\n  white-space: nowrap;\n}\ntable.dataTable thead th.dt-head-left,\ntable.dataTable thead td.dt-head-left,\ntable.dataTable tfoot th.dt-head-left,\ntable.dataTable tfoot td.dt-head-left {\n  text-align: left;\n}\ntable.dataTable thead th.dt-head-center,\ntable.dataTable thead td.dt-head-center,\ntable.dataTable tfoot th.dt-head-center,\ntable.dataTable tfoot td.dt-head-center {\n  text-align: center;\n}\ntable.dataTable thead th.dt-head-right,\ntable.dataTable thead td.dt-head-right,\ntable.dataTable tfoot th.dt-head-right,\ntable.dataTable tfoot td.dt-head-right {\n  text-align: right;\n}\ntable.dataTable thead th.dt-head-justify,\ntable.dataTable thead td.dt-head-justify,\ntable.dataTable tfoot th.dt-head-justify,\ntable.dataTable tfoot td.dt-head-justify {\n  text-align: justify;\n}\ntable.dataTable thead th.dt-head-nowrap,\ntable.dataTable thead td.dt-head-nowrap,\ntable.dataTable tfoot th.dt-head-nowrap,\ntable.dataTable tfoot td.dt-head-nowrap {\n  white-space: nowrap;\n}\ntable.dataTable tbody th.dt-body-left,\ntable.dataTable tbody td.dt-body-left {\n  text-align: left;\n}\ntable.dataTable tbody th.dt-body-center,\ntable.dataTable tbody td.dt-body-center {\n  text-align: center;\n}\ntable.dataTable tbody th.dt-body-right,\ntable.dataTable tbody td.dt-body-right {\n  text-align: right;\n}\ntable.dataTable tbody th.dt-body-justify,\ntable.dataTable tbody td.dt-body-justify {\n  text-align: justify;\n}\ntable.dataTable tbody th.dt-body-nowrap,\ntable.dataTable tbody td.dt-body-nowrap {\n  white-space: nowrap;\n}\n\ntable.dataTable,\ntable.dataTable th,\ntable.dataTable td {\n  -webkit-box-sizing: content-box;\n  box-sizing: content-box;\n}\n\n/*\n * Control feature layout\n */\n.dataTables_wrapper {\n  position: relative;\n  clear: both;\n  *zoom: 1;\n  zoom: 1;\n}\n.dataTables_wrapper .dataTables_length {\n  float: left;\n}\n.dataTables_wrapper .dataTables_filter {\n  float: right;\n  text-align: right;\n}\n.dataTables_wrapper .dataTables_filter input {\n  margin-left: 0.5em;\n}\n.dataTables_wrapper .dataTables_info {\n  clear: both;\n  float: left;\n  padding-top: 0.755em;\n}\n.dataTables_wrapper .dataTables_paginate {\n  float: right;\n  text-align: right;\n  padding-top: 0.25em;\n}\n.dataTables_wrapper .dataTables_paginate .paginate_button {\n  box-sizing: border-box;\n  display: inline-block;\n  min-width: 1.5em;\n  padding: 0.5em 1em;\n  margin-left: 2px;\n  text-align: center;\n  text-decoration: none !important;\n  cursor: pointer;\n  *cursor: hand;\n  color: #333 !important;\n  border: 1px solid transparent;\n  border-radius: 2px;\n}\n.dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {\n  color: #333 !important;\n  border: 1px solid #979797;\n  background-color: white;\n  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, white), color-stop(100%, #dcdcdc));\n  /* Chrome,Safari4+ */\n  background: -webkit-linear-gradient(top, white 0%, #dcdcdc 100%);\n  /* Chrome10+,Safari5.1+ */\n  background: -moz-linear-gradient(top, white 0%, #dcdcdc 100%);\n  /* FF3.6+ */\n  background: -ms-linear-gradient(top, white 0%, #dcdcdc 100%);\n  /* IE10+ */\n  background: -o-linear-gradient(top, white 0%, #dcdcdc 100%);\n  /* Opera 11.10+ */\n  background: linear-gradient(to bottom, white 0%, #dcdcdc 100%);\n  /* W3C */\n}\n.dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {\n  cursor: default;\n  color: #666 !important;\n  border: 1px solid transparent;\n  background: transparent;\n  box-shadow: none;\n}\n.dataTables_wrapper .dataTables_paginate .paginate_button:hover {\n  color: white !important;\n  border: 1px solid #111;\n  background-color: #585858;\n  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #585858), color-stop(100%, #111));\n  /* Chrome,Safari4+ */\n  background: -webkit-linear-gradient(top, #585858 0%, #111 100%);\n  /* Chrome10+,Safari5.1+ */\n  background: -moz-linear-gradient(top, #585858 0%, #111 100%);\n  /* FF3.6+ */\n  background: -ms-linear-gradient(top, #585858 0%, #111 100%);\n  /* IE10+ */\n  background: -o-linear-gradient(top, #585858 0%, #111 100%);\n  /* Opera 11.10+ */\n  background: linear-gradient(to bottom, #585858 0%, #111 100%);\n  /* W3C */\n}\n.dataTables_wrapper .dataTables_paginate .paginate_button:active {\n  outline: none;\n  background-color: #2b2b2b;\n  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #2b2b2b), color-stop(100%, #0c0c0c));\n  /* Chrome,Safari4+ */\n  background: -webkit-linear-gradient(top, #2b2b2b 0%, #0c0c0c 100%);\n  /* Chrome10+,Safari5.1+ */\n  background: -moz-linear-gradient(top, #2b2b2b 0%, #0c0c0c 100%);\n  /* FF3.6+ */\n  background: -ms-linear-gradient(top, #2b2b2b 0%, #0c0c0c 100%);\n  /* IE10+ */\n  background: -o-linear-gradient(top, #2b2b2b 0%, #0c0c0c 100%);\n  /* Opera 11.10+ */\n  background: linear-gradient(to bottom, #2b2b2b 0%, #0c0c0c 100%);\n  /* W3C */\n  box-shadow: inset 0 0 3px #111;\n}\n.dataTables_wrapper .dataTables_paginate .ellipsis {\n  padding: 0 1em;\n}\n.dataTables_wrapper .dataTables_processing {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  width: 100%;\n  height: 40px;\n  margin-left: -50%;\n  margin-top: -25px;\n  padding-top: 20px;\n  text-align: center;\n  font-size: 1.2em;\n  background-color: white;\n  background: -webkit-gradient(linear, left top, right top, color-stop(0%, rgba(255, 255, 255, 0)), color-stop(25%, rgba(255, 255, 255, 0.9)), color-stop(75%, rgba(255, 255, 255, 0.9)), color-stop(100%, rgba(255, 255, 255, 0)));\n  background: -webkit-linear-gradient(left, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 0.9) 25%, rgba(255, 255, 255, 0.9) 75%, rgba(255, 255, 255, 0) 100%);\n  background: -moz-linear-gradient(left, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 0.9) 25%, rgba(255, 255, 255, 0.9) 75%, rgba(255, 255, 255, 0) 100%);\n  background: -ms-linear-gradient(left, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 0.9) 25%, rgba(255, 255, 255, 0.9) 75%, rgba(255, 255, 255, 0) 100%);\n  background: -o-linear-gradient(left, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 0.9) 25%, rgba(255, 255, 255, 0.9) 75%, rgba(255, 255, 255, 0) 100%);\n  background: linear-gradient(to right, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 0.9) 25%, rgba(255, 255, 255, 0.9) 75%, rgba(255, 255, 255, 0) 100%);\n}\n.dataTables_wrapper .dataTables_length,\n.dataTables_wrapper .dataTables_filter,\n.dataTables_wrapper .dataTables_info,\n.dataTables_wrapper .dataTables_processing,\n.dataTables_wrapper .dataTables_paginate {\n  color: #333;\n}\n.dataTables_wrapper .dataTables_scroll {\n  clear: both;\n}\n.dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody {\n  *margin-top: -1px;\n  -webkit-overflow-scrolling: touch;\n}\n.dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody th, .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody td {\n  vertical-align: middle;\n}\n.dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody th > div.dataTables_sizing,\n.dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody td > div.dataTables_sizing {\n  height: 0;\n  overflow: hidden;\n  margin: 0 !important;\n  padding: 0 !important;\n}\n.dataTables_wrapper.no-footer .dataTables_scrollBody {\n  border-bottom: 1px solid #111;\n}\n.dataTables_wrapper.no-footer div.dataTables_scrollHead table,\n.dataTables_wrapper.no-footer div.dataTables_scrollBody table {\n  border-bottom: none;\n}\n.dataTables_wrapper:after {\n  visibility: hidden;\n  display: block;\n  content: \"\";\n  clear: both;\n  height: 0;\n}\n\n@media screen and (max-width: 767px) {\n  .dataTables_wrapper .dataTables_info,\n  .dataTables_wrapper .dataTables_paginate {\n    float: none;\n    text-align: center;\n  }\n  .dataTables_wrapper .dataTables_paginate {\n    margin-top: 0.5em;\n  }\n}\n@media screen and (max-width: 640px) {\n  .dataTables_wrapper .dataTables_length,\n  .dataTables_wrapper .dataTables_filter {\n    float: none;\n    text-align: center;\n  }\n  .dataTables_wrapper .dataTables_filter {\n    margin-top: 0.5em;\n  }\n}\n", ""]);

	// exports


/***/ },
/* 53 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(3)();
	// imports


	// module
	exports.push([module.id, "input.parsley-success,\nselect.parsley-success,\ntextarea.parsley-success {\n  color: #468847;\n  background-color: #DFF0D8;\n  border: 1px solid #D6E9C6;\n}\n\ninput.parsley-error,\nselect.parsley-error,\ntextarea.parsley-error {\n  color: #B94A48;\n  background-color: #F2DEDE;\n  border: 1px solid #EED3D7;\n}\n\n.parsley-errors-list {\n  margin: 2px 0 3px;\n  padding: 0;\n  list-style-type: none;\n  font-size: 0.9em;\n  line-height: 0.9em;\n  opacity: 0;\n\n  transition: all .3s ease-in;\n  -o-transition: all .3s ease-in;\n  -moz-transition: all .3s ease-in;\n  -webkit-transition: all .3s ease-in;\n}\n\n.parsley-errors-list.filled {\n  opacity: 1;\n}\n", ""]);

	// exports


/***/ },
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(3)();
	// imports


	// module
	exports.push([module.id, ".date-picker,.date-picker-wrapper{font-size:12px;font-family:Arial,sans-serif}.date-picker{width:170px;height:25px;padding:0 0 0 10px;border:0;line-height:25px;font-weight:700;cursor:pointer;color:#303030;position:relative;z-index:2}.date-picker-wrapper{position:absolute;z-index:1;border:1px solid #bfbfbf;background-color:#efefef;padding:5px 12px;line-height:20px;color:#aaa;box-shadow:3px 3px 10px rgba(0,0,0,.5);box-sizing:initial}.date-picker-wrapper.inline-wrapper{position:relative;box-shadow:none;display:inline-block}.date-picker-wrapper .drp_top-bar .error-top,.date-picker-wrapper .drp_top-bar .normal-top,.date-picker-wrapper .month-wrapper table .day.lastMonth,.date-picker-wrapper .month-wrapper table .day.nextMonth,.date-picker-wrapper .selected-days{display:none}.date-picker-wrapper.single-date{width:auto}.date-picker-wrapper.no-shortcuts{padding-bottom:12px}.date-picker-wrapper.no-topbar{padding-top:12px}.date-picker-wrapper .footer{font-size:11px;padding-top:3px}.date-picker-wrapper b{color:#666;font-weight:700}.date-picker-wrapper a{color:#6bb4d6;text-decoration:underline}.date-picker-wrapper .month-name{text-transform:uppercase}.date-picker-wrapper .month-wrapper{border:1px solid #bfbfbf;border-radius:3px;background-color:#fff;padding:5px;cursor:default;position:relative}.date-picker-wrapper .month-wrapper table,.date-picker-wrapper .month-wrapper table.month2{width:190px;float:left}.date-picker-wrapper .month-wrapper table td,.date-picker-wrapper .month-wrapper table th{vertical-align:middle;text-align:center;line-height:14px;margin:0;padding:0}.date-picker-wrapper .month-wrapper table .day{padding:5px 0;line-height:1;font-size:12px;margin-bottom:1px;color:#ccc;cursor:default}.date-picker-wrapper .month-wrapper table div.day.lastMonth,.date-picker-wrapper .month-wrapper table div.day.nextMonth{color:#999;cursor:default}.date-picker-wrapper .month-wrapper table .day.checked{background-color:#9cdbf7}.date-picker-wrapper .month-wrapper table .week-name{height:20px;line-height:20px;font-weight:100;text-transform:uppercase}.date-picker-wrapper .month-wrapper table .day.has-tooltip{cursor:help!important}.date-picker-wrapper .time label{white-space:nowrap}.date-picker-wrapper .month-wrapper table .day.toMonth.valid{color:#333;cursor:pointer}.date-picker-wrapper .month-wrapper table .day.toMonth.hovering{background-color:#cdecfa}.date-picker-wrapper .month-wrapper table .day.real-today{background-color:#ffe684}.date-picker-wrapper .month-wrapper table .day.real-today.checked,.date-picker-wrapper .month-wrapper table .day.real-today.hovering{background-color:#70ccd5}.date-picker-wrapper table .caption{height:40px}.date-picker-wrapper table .caption .next,.date-picker-wrapper table .caption .prev{padding:0 5px;cursor:pointer}.date-picker-wrapper table .caption .next:hover,.date-picker-wrapper table .caption .prev:hover{background-color:#ccc;color:#fff}.date-picker-wrapper .gap{position:relative;z-index:1;width:15px;background-color:red;font-size:0;line-height:0;float:left;top:-5px;margin:0 10px -10px;visibility:hidden;height:0}.date-picker-wrapper .gap .gap-lines{height:100%;overflow:hidden}.date-picker-wrapper .gap .gap-line{height:15px;width:15px;position:relative}.date-picker-wrapper .gap .gap-line .gap-1{z-index:1;height:0;border-left:8px solid #fff;border-top:8px solid #eee;border-bottom:8px solid #eee}.date-picker-wrapper .gap .gap-line .gap-2{position:absolute;right:0;top:0;z-index:2;height:0;border-left:8px solid transparent;border-top:8px solid #fff}.date-picker-wrapper .gap .gap-line .gap-3{position:absolute;right:0;top:8px;z-index:2;height:0;border-left:8px solid transparent;border-bottom:8px solid #fff}.date-picker-wrapper .gap .gap-top-mask{width:6px;height:1px;position:absolute;top:-1px;left:1px;background-color:#eee;z-index:3}.date-picker-wrapper .gap .gap-bottom-mask{width:6px;height:1px;position:absolute;bottom:-1px;left:7px;background-color:#eee;z-index:3}.date-picker-wrapper .drp_top-bar{line-height:1.4;position:relative;padding:10px 40px 10px 0}.date-picker-wrapper .drp_top-bar .default-top{display:block}.date-picker-wrapper .drp_top-bar.error .default-top{display:none}.date-picker-wrapper .drp_top-bar.error .error-top{display:block;color:red}.date-picker-wrapper .drp_top-bar.normal .default-top{display:none}.date-picker-wrapper .drp_top-bar.normal .normal-top,.date-picker-wrapper.single-month .time{display:block}.date-picker-wrapper .drp_top-bar .apply-btn{position:absolute;right:0;top:6px;padding:3px 5px;margin:0;font-size:12px;border-radius:4px;cursor:pointer;border:1px solid #0076a3;background:#0095cd;background:-webkit-gradient(linear,left top,left bottom,from(#00adee),to(#0078a5));background:-moz-linear-gradient(top,#00adee,#0078a5);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#00adee', endColorstr='#0078a5');color:#fff;line-height:initial}.date-picker-wrapper .drp_top-bar .apply-btn.disabled{cursor:pointer;color:#606060;border:1px solid #b7b7b7;background:#fff;background:-webkit-gradient(linear,left top,left bottom,from(#fff),to(#ededed));background:-moz-linear-gradient(top,#fff,#ededed);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#ededed')}.date-picker-wrapper .time{position:relative}.date-picker-wrapper .hide,.date-picker-wrapper.two-months.no-gap .month1 .next,.date-picker-wrapper.two-months.no-gap .month2 .prev{display:none}.date-picker-wrapper .time input[type=range]{vertical-align:middle;width:129px;padding:0;margin:0;height:20px}.date-picker-wrapper .time1,.time2{width:180px;padding:0 5px;text-align:center}.date-picker-wrapper .time1{float:left}.date-picker-wrapper .time2{float:right}.date-picker-wrapper .hour,.minute{text-align:right}.date-picker-wrapper .first-date-selected,.date-picker-wrapper .last-date-selected{background-color:#49e!important;color:#fff!important}.date-picker-wrapper .date-range-length-tip{position:absolute;margin-top:-4px;margin-left:-8px;box-shadow:0 0 3px rgba(0,0,0,.3);display:none;background-color:#ff0;padding:0 6px;border-radius:2px;font-size:12px;line-height:16px;-webkit-filter:drop-shadow(0 0 3px rgba(0,0,0,.3));-moz-filter:drop-shadow(0 0 3px rgba(0,0,0,.3));-ms-filter:drop-shadow(0 0 3px rgba(0,0,0,.3));-o-filter:drop-shadow(0 0 3px rgba(0,0,0,.3));filter:drop-shadow(0 0 3px rgba(0, 0, 0, .3))}.date-picker-wrapper .date-range-length-tip:after{content:'';position:absolute;border-left:4px solid transparent;border-right:4px solid transparent;border-top:4px solid #ff0;left:50%;margin-left:-4px;bottom:-4px}.date-picker-wrapper .week-number{padding:5px 0;line-height:1;font-size:12px;margin-bottom:1px;color:#999;cursor:pointer}.date-picker-wrapper .week-number.week-number-selected{color:#49E;font-weight:700}", ""]);

	// exports


/***/ },
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "9326ad44ae4bebdedd141e7a53c2a730.png";

/***/ },
/* 65 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "d7dc10c78f23615d328581aebcd805eb.png";

/***/ },
/* 66 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "9a6486086d09bb38cf66a57cc559ade3.png";

/***/ },
/* 67 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "1fc418e33fd5a687290258b23fac4e98.png";

/***/ },
/* 68 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "bda51e15154a18257b4f955a222fd66f.png";

/***/ },
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */
/***/ function(module, exports) {

	/*** IMPORTS FROM imports-loader ***/
	(function() {

	/* @license
	morris.js v0.5.0
	Copyright 2014 Olly Smith All rights reserved.
	Licensed under the BSD-2-Clause License.
	*/

	(function () {
	  var $,
	      Morris,
	      minutesSpecHelper,
	      secondsSpecHelper,
	      __slice = [].slice,
	      __bind = function (fn, me) {
	    return function () {
	      return fn.apply(me, arguments);
	    };
	  },
	      __hasProp = {}.hasOwnProperty,
	      __extends = function (child, parent) {
	    for (var key in parent) {
	      if (__hasProp.call(parent, key)) child[key] = parent[key];
	    }function ctor() {
	      this.constructor = child;
	    }ctor.prototype = parent.prototype;child.prototype = new ctor();child.__super__ = parent.prototype;return child;
	  },
	      __indexOf = [].indexOf || function (item) {
	    for (var i = 0, l = this.length; i < l; i++) {
	      if (i in this && this[i] === item) return i;
	    }return -1;
	  };

	  Morris = window.Morris = {};

	  $ = jQuery;

	  Morris.EventEmitter = function () {
	    function EventEmitter() {}

	    EventEmitter.prototype.on = function (name, handler) {
	      if (this.handlers == null) {
	        this.handlers = {};
	      }
	      if (this.handlers[name] == null) {
	        this.handlers[name] = [];
	      }
	      this.handlers[name].push(handler);
	      return this;
	    };

	    EventEmitter.prototype.fire = function () {
	      var args, handler, name, _i, _len, _ref, _results;
	      name = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
	      if (this.handlers != null && this.handlers[name] != null) {
	        _ref = this.handlers[name];
	        _results = [];
	        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	          handler = _ref[_i];
	          _results.push(handler.apply(null, args));
	        }
	        return _results;
	      }
	    };

	    return EventEmitter;
	  }();

	  Morris.commas = function (num) {
	    var absnum, intnum, ret, strabsnum;
	    if (num != null) {
	      ret = num < 0 ? "-" : "";
	      absnum = Math.abs(num);
	      intnum = Math.floor(absnum).toFixed(0);
	      ret += intnum.replace(/(?=(?:\d{3})+$)(?!^)/g, ',');
	      strabsnum = absnum.toString();
	      if (strabsnum.length > intnum.length) {
	        ret += strabsnum.slice(intnum.length);
	      }
	      return ret;
	    } else {
	      return '-';
	    }
	  };

	  Morris.pad2 = function (number) {
	    return (number < 10 ? '0' : '') + number;
	  };

	  Morris.Grid = function (_super) {
	    __extends(Grid, _super);

	    function Grid(options) {
	      this.resizeHandler = __bind(this.resizeHandler, this);
	      var _this = this;
	      if (typeof options.element === 'string') {
	        this.el = $(document.getElementById(options.element));
	      } else {
	        this.el = $(options.element);
	      }
	      if (this.el == null || this.el.length === 0) {
	        throw new Error("Graph container element not found");
	      }
	      if (this.el.css('position') === 'static') {
	        this.el.css('position', 'relative');
	      }
	      this.options = $.extend({}, this.gridDefaults, this.defaults || {}, options);
	      if (typeof this.options.units === 'string') {
	        this.options.postUnits = options.units;
	      }
	      this.raphael = new Raphael(this.el[0]);
	      this.elementWidth = null;
	      this.elementHeight = null;
	      this.dirty = false;
	      this.selectFrom = null;
	      if (this.init) {
	        this.init();
	      }
	      this.setData(this.options.data);
	      this.el.bind('mousemove', function (evt) {
	        var left, offset, right, width, x;
	        offset = _this.el.offset();
	        x = evt.pageX - offset.left;
	        if (_this.selectFrom) {
	          left = _this.data[_this.hitTest(Math.min(x, _this.selectFrom))]._x;
	          right = _this.data[_this.hitTest(Math.max(x, _this.selectFrom))]._x;
	          width = right - left;
	          return _this.selectionRect.attr({
	            x: left,
	            width: width
	          });
	        } else {
	          return _this.fire('hovermove', x, evt.pageY - offset.top);
	        }
	      });
	      this.el.bind('mouseleave', function (evt) {
	        if (_this.selectFrom) {
	          _this.selectionRect.hide();
	          _this.selectFrom = null;
	        }
	        return _this.fire('hoverout');
	      });
	      this.el.bind('touchstart touchmove touchend', function (evt) {
	        var offset, touch;
	        touch = evt.originalEvent.touches[0] || evt.originalEvent.changedTouches[0];
	        offset = _this.el.offset();
	        return _this.fire('hovermove', touch.pageX - offset.left, touch.pageY - offset.top);
	      });
	      this.el.bind('click', function (evt) {
	        var offset;
	        offset = _this.el.offset();
	        return _this.fire('gridclick', evt.pageX - offset.left, evt.pageY - offset.top);
	      });
	      if (this.options.rangeSelect) {
	        this.selectionRect = this.raphael.rect(0, 0, 0, this.el.innerHeight()).attr({
	          fill: this.options.rangeSelectColor,
	          stroke: false
	        }).toBack().hide();
	        this.el.bind('mousedown', function (evt) {
	          var offset;
	          offset = _this.el.offset();
	          return _this.startRange(evt.pageX - offset.left);
	        });
	        this.el.bind('mouseup', function (evt) {
	          var offset;
	          offset = _this.el.offset();
	          _this.endRange(evt.pageX - offset.left);
	          return _this.fire('hovermove', evt.pageX - offset.left, evt.pageY - offset.top);
	        });
	      }
	      if (this.options.resize) {
	        $(window).bind('resize', function (evt) {
	          if (_this.timeoutId != null) {
	            window.clearTimeout(_this.timeoutId);
	          }
	          return _this.timeoutId = window.setTimeout(_this.resizeHandler, 100);
	        });
	      }
	      this.el.css('-webkit-tap-highlight-color', 'rgba(0,0,0,0)');
	      if (this.postInit) {
	        this.postInit();
	      }
	    }

	    Grid.prototype.gridDefaults = {
	      dateFormat: null,
	      axes: true,
	      grid: true,
	      gridLineColor: '#aaa',
	      gridStrokeWidth: 0.5,
	      gridTextColor: '#888',
	      gridTextSize: 12,
	      gridTextFamily: 'sans-serif',
	      gridTextWeight: 'normal',
	      hideHover: false,
	      yLabelFormat: null,
	      xLabelAngle: 0,
	      numLines: 5,
	      padding: 25,
	      parseTime: true,
	      postUnits: '',
	      preUnits: '',
	      ymax: 'auto',
	      ymin: 'auto 0',
	      goals: [],
	      goalStrokeWidth: 1.0,
	      goalLineColors: ['#666633', '#999966', '#cc6666', '#663333'],
	      events: [],
	      eventStrokeWidth: 1.0,
	      eventLineColors: ['#005a04', '#ccffbb', '#3a5f0b', '#005502'],
	      rangeSelect: null,
	      rangeSelectColor: '#eef',
	      resize: false
	    };

	    Grid.prototype.setData = function (data, redraw) {
	      var e, idx, index, maxGoal, minGoal, ret, row, step, total, y, ykey, ymax, ymin, yval, _ref;
	      if (redraw == null) {
	        redraw = true;
	      }
	      this.options.data = data;
	      if (data == null || data.length === 0) {
	        this.data = [];
	        this.raphael.clear();
	        if (this.hover != null) {
	          this.hover.hide();
	        }
	        return;
	      }
	      ymax = this.cumulative ? 0 : null;
	      ymin = this.cumulative ? 0 : null;
	      if (this.options.goals.length > 0) {
	        minGoal = Math.min.apply(Math, this.options.goals);
	        maxGoal = Math.max.apply(Math, this.options.goals);
	        ymin = ymin != null ? Math.min(ymin, minGoal) : minGoal;
	        ymax = ymax != null ? Math.max(ymax, maxGoal) : maxGoal;
	      }
	      this.data = function () {
	        var _i, _len, _results;
	        _results = [];
	        for (index = _i = 0, _len = data.length; _i < _len; index = ++_i) {
	          row = data[index];
	          ret = {
	            src: row
	          };
	          ret.label = row[this.options.xkey];
	          if (this.options.parseTime) {
	            ret.x = Morris.parseDate(ret.label);
	            if (this.options.dateFormat) {
	              ret.label = this.options.dateFormat(ret.x);
	            } else if (typeof ret.label === 'number') {
	              ret.label = new Date(ret.label).toString();
	            }
	          } else {
	            ret.x = index;
	            if (this.options.xLabelFormat) {
	              ret.label = this.options.xLabelFormat(ret);
	            }
	          }
	          total = 0;
	          ret.y = function () {
	            var _j, _len1, _ref, _results1;
	            _ref = this.options.ykeys;
	            _results1 = [];
	            for (idx = _j = 0, _len1 = _ref.length; _j < _len1; idx = ++_j) {
	              ykey = _ref[idx];
	              yval = row[ykey];
	              if (typeof yval === 'string') {
	                yval = parseFloat(yval);
	              }
	              if (yval != null && typeof yval !== 'number') {
	                yval = null;
	              }
	              if (yval != null) {
	                if (this.cumulative) {
	                  total += yval;
	                } else {
	                  if (ymax != null) {
	                    ymax = Math.max(yval, ymax);
	                    ymin = Math.min(yval, ymin);
	                  } else {
	                    ymax = ymin = yval;
	                  }
	                }
	              }
	              if (this.cumulative && total != null) {
	                ymax = Math.max(total, ymax);
	                ymin = Math.min(total, ymin);
	              }
	              _results1.push(yval);
	            }
	            return _results1;
	          }.call(this);
	          _results.push(ret);
	        }
	        return _results;
	      }.call(this);
	      if (this.options.parseTime) {
	        this.data = this.data.sort(function (a, b) {
	          return (a.x > b.x) - (b.x > a.x);
	        });
	      }
	      this.xmin = this.data[0].x;
	      this.xmax = this.data[this.data.length - 1].x;
	      this.events = [];
	      if (this.options.events.length > 0) {
	        if (this.options.parseTime) {
	          this.events = function () {
	            var _i, _len, _ref, _results;
	            _ref = this.options.events;
	            _results = [];
	            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	              e = _ref[_i];
	              _results.push(Morris.parseDate(e));
	            }
	            return _results;
	          }.call(this);
	        } else {
	          this.events = this.options.events;
	        }
	        this.xmax = Math.max(this.xmax, Math.max.apply(Math, this.events));
	        this.xmin = Math.min(this.xmin, Math.min.apply(Math, this.events));
	      }
	      if (this.xmin === this.xmax) {
	        this.xmin -= 1;
	        this.xmax += 1;
	      }
	      this.ymin = this.yboundary('min', ymin);
	      this.ymax = this.yboundary('max', ymax);
	      if (this.ymin === this.ymax) {
	        if (ymin) {
	          this.ymin -= 1;
	        }
	        this.ymax += 1;
	      }
	      if ((_ref = this.options.axes) === true || _ref === 'both' || _ref === 'y' || this.options.grid === true) {
	        if (this.options.ymax === this.gridDefaults.ymax && this.options.ymin === this.gridDefaults.ymin) {
	          this.grid = this.autoGridLines(this.ymin, this.ymax, this.options.numLines);
	          this.ymin = Math.min(this.ymin, this.grid[0]);
	          this.ymax = Math.max(this.ymax, this.grid[this.grid.length - 1]);
	        } else {
	          step = (this.ymax - this.ymin) / (this.options.numLines - 1);
	          this.grid = function () {
	            var _i, _ref1, _ref2, _results;
	            _results = [];
	            for (y = _i = _ref1 = this.ymin, _ref2 = this.ymax; step > 0 ? _i <= _ref2 : _i >= _ref2; y = _i += step) {
	              _results.push(y);
	            }
	            return _results;
	          }.call(this);
	        }
	      }
	      this.dirty = true;
	      if (redraw) {
	        return this.redraw();
	      }
	    };

	    Grid.prototype.yboundary = function (boundaryType, currentValue) {
	      var boundaryOption, suggestedValue;
	      boundaryOption = this.options["y" + boundaryType];
	      if (typeof boundaryOption === 'string') {
	        if (boundaryOption.slice(0, 4) === 'auto') {
	          if (boundaryOption.length > 5) {
	            suggestedValue = parseInt(boundaryOption.slice(5), 10);
	            if (currentValue == null) {
	              return suggestedValue;
	            }
	            return Math[boundaryType](currentValue, suggestedValue);
	          } else {
	            if (currentValue != null) {
	              return currentValue;
	            } else {
	              return 0;
	            }
	          }
	        } else {
	          return parseInt(boundaryOption, 10);
	        }
	      } else {
	        return boundaryOption;
	      }
	    };

	    Grid.prototype.autoGridLines = function (ymin, ymax, nlines) {
	      var gmax, gmin, grid, smag, span, step, unit, y, ymag;
	      span = ymax - ymin;
	      ymag = Math.floor(Math.log(span) / Math.log(10));
	      unit = Math.pow(10, ymag);
	      gmin = Math.floor(ymin / unit) * unit;
	      gmax = Math.ceil(ymax / unit) * unit;
	      step = (gmax - gmin) / (nlines - 1);
	      if (unit === 1 && step > 1 && Math.ceil(step) !== step) {
	        step = Math.ceil(step);
	        gmax = gmin + step * (nlines - 1);
	      }
	      if (gmin < 0 && gmax > 0) {
	        gmin = Math.floor(ymin / step) * step;
	        gmax = Math.ceil(ymax / step) * step;
	      }
	      if (step < 1) {
	        smag = Math.floor(Math.log(step) / Math.log(10));
	        grid = function () {
	          var _i, _results;
	          _results = [];
	          for (y = _i = gmin; step > 0 ? _i <= gmax : _i >= gmax; y = _i += step) {
	            _results.push(parseFloat(y.toFixed(1 - smag)));
	          }
	          return _results;
	        }();
	      } else {
	        grid = function () {
	          var _i, _results;
	          _results = [];
	          for (y = _i = gmin; step > 0 ? _i <= gmax : _i >= gmax; y = _i += step) {
	            _results.push(y);
	          }
	          return _results;
	        }();
	      }
	      return grid;
	    };

	    Grid.prototype._calc = function () {
	      var bottomOffsets, gridLine, h, i, w, yLabelWidths, _ref, _ref1;
	      w = this.el.width();
	      h = this.el.height();
	      if (this.elementWidth !== w || this.elementHeight !== h || this.dirty) {
	        this.elementWidth = w;
	        this.elementHeight = h;
	        this.dirty = false;
	        this.left = this.options.padding;
	        this.right = this.elementWidth - this.options.padding;
	        this.top = this.options.padding;
	        this.bottom = this.elementHeight - this.options.padding;
	        if ((_ref = this.options.axes) === true || _ref === 'both' || _ref === 'y') {
	          yLabelWidths = function () {
	            var _i, _len, _ref1, _results;
	            _ref1 = this.grid;
	            _results = [];
	            for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
	              gridLine = _ref1[_i];
	              _results.push(this.measureText(this.yAxisFormat(gridLine)).width);
	            }
	            return _results;
	          }.call(this);
	          this.left += Math.max.apply(Math, yLabelWidths);
	        }
	        if ((_ref1 = this.options.axes) === true || _ref1 === 'both' || _ref1 === 'x') {
	          bottomOffsets = function () {
	            var _i, _ref2, _results;
	            _results = [];
	            for (i = _i = 0, _ref2 = this.data.length; 0 <= _ref2 ? _i < _ref2 : _i > _ref2; i = 0 <= _ref2 ? ++_i : --_i) {
	              _results.push(this.measureText(this.data[i].text, -this.options.xLabelAngle).height);
	            }
	            return _results;
	          }.call(this);
	          this.bottom -= Math.max.apply(Math, bottomOffsets);
	        }
	        this.width = Math.max(1, this.right - this.left);
	        this.height = Math.max(1, this.bottom - this.top);
	        this.dx = this.width / (this.xmax - this.xmin);
	        this.dy = this.height / (this.ymax - this.ymin);
	        if (this.calc) {
	          return this.calc();
	        }
	      }
	    };

	    Grid.prototype.transY = function (y) {
	      return this.bottom - (y - this.ymin) * this.dy;
	    };

	    Grid.prototype.transX = function (x) {
	      if (this.data.length === 1) {
	        return (this.left + this.right) / 2;
	      } else {
	        return this.left + (x - this.xmin) * this.dx;
	      }
	    };

	    Grid.prototype.redraw = function () {
	      this.raphael.clear();
	      this._calc();
	      this.drawGrid();
	      this.drawGoals();
	      this.drawEvents();
	      if (this.draw) {
	        return this.draw();
	      }
	    };

	    Grid.prototype.measureText = function (text, angle) {
	      var ret, tt;
	      if (angle == null) {
	        angle = 0;
	      }
	      tt = this.raphael.text(100, 100, text).attr('font-size', this.options.gridTextSize).attr('font-family', this.options.gridTextFamily).attr('font-weight', this.options.gridTextWeight).rotate(angle);
	      ret = tt.getBBox();
	      tt.remove();
	      return ret;
	    };

	    Grid.prototype.yAxisFormat = function (label) {
	      return this.yLabelFormat(label);
	    };

	    Grid.prototype.yLabelFormat = function (label) {
	      if (typeof this.options.yLabelFormat === 'function') {
	        return this.options.yLabelFormat(label);
	      } else {
	        return "" + this.options.preUnits + Morris.commas(label) + this.options.postUnits;
	      }
	    };

	    Grid.prototype.drawGrid = function () {
	      var lineY, y, _i, _len, _ref, _ref1, _ref2, _results;
	      if (this.options.grid === false && (_ref = this.options.axes) !== true && _ref !== 'both' && _ref !== 'y') {
	        return;
	      }
	      _ref1 = this.grid;
	      _results = [];
	      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
	        lineY = _ref1[_i];
	        y = this.transY(lineY);
	        if ((_ref2 = this.options.axes) === true || _ref2 === 'both' || _ref2 === 'y') {
	          this.drawYAxisLabel(this.left - this.options.padding / 2, y, this.yAxisFormat(lineY));
	        }
	        if (this.options.grid) {
	          _results.push(this.drawGridLine("M" + this.left + "," + y + "H" + (this.left + this.width)));
	        } else {
	          _results.push(void 0);
	        }
	      }
	      return _results;
	    };

	    Grid.prototype.drawGoals = function () {
	      var color, goal, i, _i, _len, _ref, _results;
	      _ref = this.options.goals;
	      _results = [];
	      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
	        goal = _ref[i];
	        color = this.options.goalLineColors[i % this.options.goalLineColors.length];
	        _results.push(this.drawGoal(goal, color));
	      }
	      return _results;
	    };

	    Grid.prototype.drawEvents = function () {
	      var color, event, i, _i, _len, _ref, _results;
	      _ref = this.events;
	      _results = [];
	      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
	        event = _ref[i];
	        color = this.options.eventLineColors[i % this.options.eventLineColors.length];
	        _results.push(this.drawEvent(event, color));
	      }
	      return _results;
	    };

	    Grid.prototype.drawGoal = function (goal, color) {
	      return this.raphael.path("M" + this.left + "," + this.transY(goal) + "H" + this.right).attr('stroke', color).attr('stroke-width', this.options.goalStrokeWidth);
	    };

	    Grid.prototype.drawEvent = function (event, color) {
	      return this.raphael.path("M" + this.transX(event) + "," + this.bottom + "V" + this.top).attr('stroke', color).attr('stroke-width', this.options.eventStrokeWidth);
	    };

	    Grid.prototype.drawYAxisLabel = function (xPos, yPos, text) {
	      return this.raphael.text(xPos, yPos, text).attr('font-size', this.options.gridTextSize).attr('font-family', this.options.gridTextFamily).attr('font-weight', this.options.gridTextWeight).attr('fill', this.options.gridTextColor).attr('text-anchor', 'end');
	    };

	    Grid.prototype.drawGridLine = function (path) {
	      return this.raphael.path(path).attr('stroke', this.options.gridLineColor).attr('stroke-width', this.options.gridStrokeWidth);
	    };

	    Grid.prototype.startRange = function (x) {
	      this.hover.hide();
	      this.selectFrom = x;
	      return this.selectionRect.attr({
	        x: x,
	        width: 0
	      }).show();
	    };

	    Grid.prototype.endRange = function (x) {
	      var end, start;
	      if (this.selectFrom) {
	        start = Math.min(this.selectFrom, x);
	        end = Math.max(this.selectFrom, x);
	        this.options.rangeSelect.call(this.el, {
	          start: this.data[this.hitTest(start)].x,
	          end: this.data[this.hitTest(end)].x
	        });
	        return this.selectFrom = null;
	      }
	    };

	    Grid.prototype.resizeHandler = function () {
	      this.timeoutId = null;
	      this.raphael.setSize(this.el.width(), this.el.height());
	      return this.redraw();
	    };

	    return Grid;
	  }(Morris.EventEmitter);

	  Morris.parseDate = function (date) {
	    var isecs, m, msecs, n, o, offsetmins, p, q, r, ret, secs;
	    if (typeof date === 'number') {
	      return date;
	    }
	    m = date.match(/^(\d+) Q(\d)$/);
	    n = date.match(/^(\d+)-(\d+)$/);
	    o = date.match(/^(\d+)-(\d+)-(\d+)$/);
	    p = date.match(/^(\d+) W(\d+)$/);
	    q = date.match(/^(\d+)-(\d+)-(\d+)[ T](\d+):(\d+)(Z|([+-])(\d\d):?(\d\d))?$/);
	    r = date.match(/^(\d+)-(\d+)-(\d+)[ T](\d+):(\d+):(\d+(\.\d+)?)(Z|([+-])(\d\d):?(\d\d))?$/);
	    if (m) {
	      return new Date(parseInt(m[1], 10), parseInt(m[2], 10) * 3 - 1, 1).getTime();
	    } else if (n) {
	      return new Date(parseInt(n[1], 10), parseInt(n[2], 10) - 1, 1).getTime();
	    } else if (o) {
	      return new Date(parseInt(o[1], 10), parseInt(o[2], 10) - 1, parseInt(o[3], 10)).getTime();
	    } else if (p) {
	      ret = new Date(parseInt(p[1], 10), 0, 1);
	      if (ret.getDay() !== 4) {
	        ret.setMonth(0, 1 + (4 - ret.getDay() + 7) % 7);
	      }
	      return ret.getTime() + parseInt(p[2], 10) * 604800000;
	    } else if (q) {
	      if (!q[6]) {
	        return new Date(parseInt(q[1], 10), parseInt(q[2], 10) - 1, parseInt(q[3], 10), parseInt(q[4], 10), parseInt(q[5], 10)).getTime();
	      } else {
	        offsetmins = 0;
	        if (q[6] !== 'Z') {
	          offsetmins = parseInt(q[8], 10) * 60 + parseInt(q[9], 10);
	          if (q[7] === '+') {
	            offsetmins = 0 - offsetmins;
	          }
	        }
	        return Date.UTC(parseInt(q[1], 10), parseInt(q[2], 10) - 1, parseInt(q[3], 10), parseInt(q[4], 10), parseInt(q[5], 10) + offsetmins);
	      }
	    } else if (r) {
	      secs = parseFloat(r[6]);
	      isecs = Math.floor(secs);
	      msecs = Math.round((secs - isecs) * 1000);
	      if (!r[8]) {
	        return new Date(parseInt(r[1], 10), parseInt(r[2], 10) - 1, parseInt(r[3], 10), parseInt(r[4], 10), parseInt(r[5], 10), isecs, msecs).getTime();
	      } else {
	        offsetmins = 0;
	        if (r[8] !== 'Z') {
	          offsetmins = parseInt(r[10], 10) * 60 + parseInt(r[11], 10);
	          if (r[9] === '+') {
	            offsetmins = 0 - offsetmins;
	          }
	        }
	        return Date.UTC(parseInt(r[1], 10), parseInt(r[2], 10) - 1, parseInt(r[3], 10), parseInt(r[4], 10), parseInt(r[5], 10) + offsetmins, isecs, msecs);
	      }
	    } else {
	      return new Date(parseInt(date, 10), 0, 1).getTime();
	    }
	  };

	  Morris.Hover = function () {
	    Hover.defaults = {
	      "class": 'morris-hover morris-default-style'
	    };

	    function Hover(options) {
	      if (options == null) {
	        options = {};
	      }
	      this.options = $.extend({}, Morris.Hover.defaults, options);
	      this.el = $("<div class='" + this.options["class"] + "'></div>");
	      this.el.hide();
	      this.options.parent.append(this.el);
	    }

	    Hover.prototype.update = function (html, x, y) {
	      if (!html) {
	        return this.hide();
	      } else {
	        this.html(html);
	        this.show();
	        return this.moveTo(x, y);
	      }
	    };

	    Hover.prototype.html = function (content) {
	      return this.el.html(content);
	    };

	    Hover.prototype.moveTo = function (x, y) {
	      var hoverHeight, hoverWidth, left, parentHeight, parentWidth, top;
	      parentWidth = this.options.parent.innerWidth();
	      parentHeight = this.options.parent.innerHeight();
	      hoverWidth = this.el.outerWidth();
	      hoverHeight = this.el.outerHeight();
	      left = Math.min(Math.max(0, x - hoverWidth / 2), parentWidth - hoverWidth);
	      if (y != null) {
	        top = y - hoverHeight - 10;
	        if (top < 0) {
	          top = y + 10;
	          if (top + hoverHeight > parentHeight) {
	            top = parentHeight / 2 - hoverHeight / 2;
	          }
	        }
	      } else {
	        top = parentHeight / 2 - hoverHeight / 2;
	      }
	      return this.el.css({
	        left: left + "px",
	        top: parseInt(top) + "px"
	      });
	    };

	    Hover.prototype.show = function () {
	      return this.el.show();
	    };

	    Hover.prototype.hide = function () {
	      return this.el.hide();
	    };

	    return Hover;
	  }();

	  Morris.Line = function (_super) {
	    __extends(Line, _super);

	    function Line(options) {
	      this.hilight = __bind(this.hilight, this);
	      this.onHoverOut = __bind(this.onHoverOut, this);
	      this.onHoverMove = __bind(this.onHoverMove, this);
	      this.onGridClick = __bind(this.onGridClick, this);
	      if (!(this instanceof Morris.Line)) {
	        return new Morris.Line(options);
	      }
	      Line.__super__.constructor.call(this, options);
	    }

	    Line.prototype.init = function () {
	      if (this.options.hideHover !== 'always') {
	        this.hover = new Morris.Hover({
	          parent: this.el
	        });
	        this.on('hovermove', this.onHoverMove);
	        this.on('hoverout', this.onHoverOut);
	        return this.on('gridclick', this.onGridClick);
	      }
	    };

	    Line.prototype.defaults = {
	      lineWidth: 3,
	      pointSize: 4,
	      lineColors: ['#0b62a4', '#7A92A3', '#4da74d', '#afd8f8', '#edc240', '#cb4b4b', '#9440ed'],
	      pointStrokeWidths: [1],
	      pointStrokeColors: ['#ffffff'],
	      pointFillColors: [],
	      smooth: true,
	      xLabels: 'auto',
	      xLabelFormat: null,
	      xLabelMargin: 24,
	      hideHover: false
	    };

	    Line.prototype.calc = function () {
	      this.calcPoints();
	      return this.generatePaths();
	    };

	    Line.prototype.calcPoints = function () {
	      var row, y, _i, _len, _ref, _results;
	      _ref = this.data;
	      _results = [];
	      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	        row = _ref[_i];
	        row._x = this.transX(row.x);
	        row._y = function () {
	          var _j, _len1, _ref1, _results1;
	          _ref1 = row.y;
	          _results1 = [];
	          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
	            y = _ref1[_j];
	            if (y != null) {
	              _results1.push(this.transY(y));
	            } else {
	              _results1.push(y);
	            }
	          }
	          return _results1;
	        }.call(this);
	        _results.push(row._ymax = Math.min.apply(Math, [this.bottom].concat(function () {
	          var _j, _len1, _ref1, _results1;
	          _ref1 = row._y;
	          _results1 = [];
	          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
	            y = _ref1[_j];
	            if (y != null) {
	              _results1.push(y);
	            }
	          }
	          return _results1;
	        }())));
	      }
	      return _results;
	    };

	    Line.prototype.hitTest = function (x) {
	      var index, r, _i, _len, _ref;
	      if (this.data.length === 0) {
	        return null;
	      }
	      _ref = this.data.slice(1);
	      for (index = _i = 0, _len = _ref.length; _i < _len; index = ++_i) {
	        r = _ref[index];
	        if (x < (r._x + this.data[index]._x) / 2) {
	          break;
	        }
	      }
	      return index;
	    };

	    Line.prototype.onGridClick = function (x, y) {
	      var index;
	      index = this.hitTest(x);
	      return this.fire('click', index, this.data[index].src, x, y);
	    };

	    Line.prototype.onHoverMove = function (x, y) {
	      var index;
	      index = this.hitTest(x);
	      return this.displayHoverForRow(index);
	    };

	    Line.prototype.onHoverOut = function () {
	      if (this.options.hideHover !== false) {
	        return this.displayHoverForRow(null);
	      }
	    };

	    Line.prototype.displayHoverForRow = function (index) {
	      var _ref;
	      if (index != null) {
	        (_ref = this.hover).update.apply(_ref, this.hoverContentForRow(index));
	        return this.hilight(index);
	      } else {
	        this.hover.hide();
	        return this.hilight();
	      }
	    };

	    Line.prototype.hoverContentForRow = function (index) {
	      var content, j, row, y, _i, _len, _ref;
	      row = this.data[index];
	      content = "<div class='morris-hover-row-label'>" + row.label + "</div>";
	      _ref = row.y;
	      for (j = _i = 0, _len = _ref.length; _i < _len; j = ++_i) {
	        y = _ref[j];
	        content += "<div class='morris-hover-point' style='color: " + this.colorFor(row, j, 'label') + "'>\n  " + this.options.labels[j] + ":\n  " + this.yLabelFormat(y) + "\n</div>";
	      }
	      if (typeof this.options.hoverCallback === 'function') {
	        content = this.options.hoverCallback(index, this.options, content, row.src);
	      }
	      return [content, row._x, row._ymax];
	    };

	    Line.prototype.generatePaths = function () {
	      var coords, i, r, smooth;
	      return this.paths = function () {
	        var _i, _ref, _ref1, _results;
	        _results = [];
	        for (i = _i = 0, _ref = this.options.ykeys.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
	          smooth = typeof this.options.smooth === "boolean" ? this.options.smooth : (_ref1 = this.options.ykeys[i], __indexOf.call(this.options.smooth, _ref1) >= 0);
	          coords = function () {
	            var _j, _len, _ref2, _results1;
	            _ref2 = this.data;
	            _results1 = [];
	            for (_j = 0, _len = _ref2.length; _j < _len; _j++) {
	              r = _ref2[_j];
	              if (r._y[i] !== void 0) {
	                _results1.push({
	                  x: r._x,
	                  y: r._y[i]
	                });
	              }
	            }
	            return _results1;
	          }.call(this);
	          if (coords.length > 1) {
	            _results.push(Morris.Line.createPath(coords, smooth, this.bottom));
	          } else {
	            _results.push(null);
	          }
	        }
	        return _results;
	      }.call(this);
	    };

	    Line.prototype.draw = function () {
	      var _ref;
	      if ((_ref = this.options.axes) === true || _ref === 'both' || _ref === 'x') {
	        this.drawXAxis();
	      }
	      this.drawSeries();
	      if (this.options.hideHover === false) {
	        return this.displayHoverForRow(this.data.length - 1);
	      }
	    };

	    Line.prototype.drawXAxis = function () {
	      var drawLabel,
	          l,
	          labels,
	          prevAngleMargin,
	          prevLabelMargin,
	          row,
	          ypos,
	          _i,
	          _len,
	          _results,
	          _this = this;
	      ypos = this.bottom + this.options.padding / 2;
	      prevLabelMargin = null;
	      prevAngleMargin = null;
	      drawLabel = function (labelText, xpos) {
	        var label, labelBox, margin, offset, textBox;
	        label = _this.drawXAxisLabel(_this.transX(xpos), ypos, labelText);
	        textBox = label.getBBox();
	        label.transform("r" + -_this.options.xLabelAngle);
	        labelBox = label.getBBox();
	        label.transform("t0," + labelBox.height / 2 + "...");
	        if (_this.options.xLabelAngle !== 0) {
	          offset = -0.5 * textBox.width * Math.cos(_this.options.xLabelAngle * Math.PI / 180.0);
	          label.transform("t" + offset + ",0...");
	        }
	        labelBox = label.getBBox();
	        if ((prevLabelMargin == null || prevLabelMargin >= labelBox.x + labelBox.width || prevAngleMargin != null && prevAngleMargin >= labelBox.x) && labelBox.x >= 0 && labelBox.x + labelBox.width < _this.el.width()) {
	          if (_this.options.xLabelAngle !== 0) {
	            margin = 1.25 * _this.options.gridTextSize / Math.sin(_this.options.xLabelAngle * Math.PI / 180.0);
	            prevAngleMargin = labelBox.x - margin;
	          }
	          return prevLabelMargin = labelBox.x - _this.options.xLabelMargin;
	        } else {
	          return label.remove();
	        }
	      };
	      if (this.options.parseTime) {
	        if (this.data.length === 1 && this.options.xLabels === 'auto') {
	          labels = [[this.data[0].label, this.data[0].x]];
	        } else {
	          labels = Morris.labelSeries(this.xmin, this.xmax, this.width, this.options.xLabels, this.options.xLabelFormat);
	        }
	      } else {
	        labels = function () {
	          var _i, _len, _ref, _results;
	          _ref = this.data;
	          _results = [];
	          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	            row = _ref[_i];
	            _results.push([row.label, row.x]);
	          }
	          return _results;
	        }.call(this);
	      }
	      labels.reverse();
	      _results = [];
	      for (_i = 0, _len = labels.length; _i < _len; _i++) {
	        l = labels[_i];
	        _results.push(drawLabel(l[0], l[1]));
	      }
	      return _results;
	    };

	    Line.prototype.drawSeries = function () {
	      var i, _i, _j, _ref, _ref1, _results;
	      this.seriesPoints = [];
	      for (i = _i = _ref = this.options.ykeys.length - 1; _ref <= 0 ? _i <= 0 : _i >= 0; i = _ref <= 0 ? ++_i : --_i) {
	        this._drawLineFor(i);
	      }
	      _results = [];
	      for (i = _j = _ref1 = this.options.ykeys.length - 1; _ref1 <= 0 ? _j <= 0 : _j >= 0; i = _ref1 <= 0 ? ++_j : --_j) {
	        _results.push(this._drawPointFor(i));
	      }
	      return _results;
	    };

	    Line.prototype._drawPointFor = function (index) {
	      var circle, row, _i, _len, _ref, _results;
	      this.seriesPoints[index] = [];
	      _ref = this.data;
	      _results = [];
	      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	        row = _ref[_i];
	        circle = null;
	        if (row._y[index] != null) {
	          circle = this.drawLinePoint(row._x, row._y[index], this.colorFor(row, index, 'point'), index);
	        }
	        _results.push(this.seriesPoints[index].push(circle));
	      }
	      return _results;
	    };

	    Line.prototype._drawLineFor = function (index) {
	      var path;
	      path = this.paths[index];
	      if (path !== null) {
	        return this.drawLinePath(path, this.colorFor(null, index, 'line'), index);
	      }
	    };

	    Line.createPath = function (coords, smooth, bottom) {
	      var coord, g, grads, i, ix, lg, path, prevCoord, x1, x2, y1, y2, _i, _len;
	      path = "";
	      if (smooth) {
	        grads = Morris.Line.gradients(coords);
	      }
	      prevCoord = {
	        y: null
	      };
	      for (i = _i = 0, _len = coords.length; _i < _len; i = ++_i) {
	        coord = coords[i];
	        if (coord.y != null) {
	          if (prevCoord.y != null) {
	            if (smooth) {
	              g = grads[i];
	              lg = grads[i - 1];
	              ix = (coord.x - prevCoord.x) / 4;
	              x1 = prevCoord.x + ix;
	              y1 = Math.min(bottom, prevCoord.y + ix * lg);
	              x2 = coord.x - ix;
	              y2 = Math.min(bottom, coord.y - ix * g);
	              path += "C" + x1 + "," + y1 + "," + x2 + "," + y2 + "," + coord.x + "," + coord.y;
	            } else {
	              path += "L" + coord.x + "," + coord.y;
	            }
	          } else {
	            if (!smooth || grads[i] != null) {
	              path += "M" + coord.x + "," + coord.y;
	            }
	          }
	        }
	        prevCoord = coord;
	      }
	      return path;
	    };

	    Line.gradients = function (coords) {
	      var coord, grad, i, nextCoord, prevCoord, _i, _len, _results;
	      grad = function (a, b) {
	        return (a.y - b.y) / (a.x - b.x);
	      };
	      _results = [];
	      for (i = _i = 0, _len = coords.length; _i < _len; i = ++_i) {
	        coord = coords[i];
	        if (coord.y != null) {
	          nextCoord = coords[i + 1] || {
	            y: null
	          };
	          prevCoord = coords[i - 1] || {
	            y: null
	          };
	          if (prevCoord.y != null && nextCoord.y != null) {
	            _results.push(grad(prevCoord, nextCoord));
	          } else if (prevCoord.y != null) {
	            _results.push(grad(prevCoord, coord));
	          } else if (nextCoord.y != null) {
	            _results.push(grad(coord, nextCoord));
	          } else {
	            _results.push(null);
	          }
	        } else {
	          _results.push(null);
	        }
	      }
	      return _results;
	    };

	    Line.prototype.hilight = function (index) {
	      var i, _i, _j, _ref, _ref1;
	      if (this.prevHilight !== null && this.prevHilight !== index) {
	        for (i = _i = 0, _ref = this.seriesPoints.length - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
	          if (this.seriesPoints[i][this.prevHilight]) {
	            this.seriesPoints[i][this.prevHilight].animate(this.pointShrinkSeries(i));
	          }
	        }
	      }
	      if (index !== null && this.prevHilight !== index) {
	        for (i = _j = 0, _ref1 = this.seriesPoints.length - 1; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; i = 0 <= _ref1 ? ++_j : --_j) {
	          if (this.seriesPoints[i][index]) {
	            this.seriesPoints[i][index].animate(this.pointGrowSeries(i));
	          }
	        }
	      }
	      return this.prevHilight = index;
	    };

	    Line.prototype.colorFor = function (row, sidx, type) {
	      if (typeof this.options.lineColors === 'function') {
	        return this.options.lineColors.call(this, row, sidx, type);
	      } else if (type === 'point') {
	        return this.options.pointFillColors[sidx % this.options.pointFillColors.length] || this.options.lineColors[sidx % this.options.lineColors.length];
	      } else {
	        return this.options.lineColors[sidx % this.options.lineColors.length];
	      }
	    };

	    Line.prototype.drawXAxisLabel = function (xPos, yPos, text) {
	      return this.raphael.text(xPos, yPos, text).attr('font-size', this.options.gridTextSize).attr('font-family', this.options.gridTextFamily).attr('font-weight', this.options.gridTextWeight).attr('fill', this.options.gridTextColor);
	    };

	    Line.prototype.drawLinePath = function (path, lineColor, lineIndex) {
	      return this.raphael.path(path).attr('stroke', lineColor).attr('stroke-width', this.lineWidthForSeries(lineIndex));
	    };

	    Line.prototype.drawLinePoint = function (xPos, yPos, pointColor, lineIndex) {
	      return this.raphael.circle(xPos, yPos, this.pointSizeForSeries(lineIndex)).attr('fill', pointColor).attr('stroke-width', this.pointStrokeWidthForSeries(lineIndex)).attr('stroke', this.pointStrokeColorForSeries(lineIndex));
	    };

	    Line.prototype.pointStrokeWidthForSeries = function (index) {
	      return this.options.pointStrokeWidths[index % this.options.pointStrokeWidths.length];
	    };

	    Line.prototype.pointStrokeColorForSeries = function (index) {
	      return this.options.pointStrokeColors[index % this.options.pointStrokeColors.length];
	    };

	    Line.prototype.lineWidthForSeries = function (index) {
	      if (this.options.lineWidth instanceof Array) {
	        return this.options.lineWidth[index % this.options.lineWidth.length];
	      } else {
	        return this.options.lineWidth;
	      }
	    };

	    Line.prototype.pointSizeForSeries = function (index) {
	      if (this.options.pointSize instanceof Array) {
	        return this.options.pointSize[index % this.options.pointSize.length];
	      } else {
	        return this.options.pointSize;
	      }
	    };

	    Line.prototype.pointGrowSeries = function (index) {
	      return Raphael.animation({
	        r: this.pointSizeForSeries(index) + 3
	      }, 25, 'linear');
	    };

	    Line.prototype.pointShrinkSeries = function (index) {
	      return Raphael.animation({
	        r: this.pointSizeForSeries(index)
	      }, 25, 'linear');
	    };

	    return Line;
	  }(Morris.Grid);

	  Morris.labelSeries = function (dmin, dmax, pxwidth, specName, xLabelFormat) {
	    var d, d0, ddensity, name, ret, s, spec, t, _i, _len, _ref;
	    ddensity = 200 * (dmax - dmin) / pxwidth;
	    d0 = new Date(dmin);
	    spec = Morris.LABEL_SPECS[specName];
	    if (spec === void 0) {
	      _ref = Morris.AUTO_LABEL_ORDER;
	      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	        name = _ref[_i];
	        s = Morris.LABEL_SPECS[name];
	        if (ddensity >= s.span) {
	          spec = s;
	          break;
	        }
	      }
	    }
	    if (spec === void 0) {
	      spec = Morris.LABEL_SPECS["second"];
	    }
	    if (xLabelFormat) {
	      spec = $.extend({}, spec, {
	        fmt: xLabelFormat
	      });
	    }
	    d = spec.start(d0);
	    ret = [];
	    while ((t = d.getTime()) <= dmax) {
	      if (t >= dmin) {
	        ret.push([spec.fmt(d), t]);
	      }
	      spec.incr(d);
	    }
	    return ret;
	  };

	  minutesSpecHelper = function (interval) {
	    return {
	      span: interval * 60 * 1000,
	      start: function (d) {
	        return new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours());
	      },
	      fmt: function (d) {
	        return "" + Morris.pad2(d.getHours()) + ":" + Morris.pad2(d.getMinutes());
	      },
	      incr: function (d) {
	        return d.setUTCMinutes(d.getUTCMinutes() + interval);
	      }
	    };
	  };

	  secondsSpecHelper = function (interval) {
	    return {
	      span: interval * 1000,
	      start: function (d) {
	        return new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes());
	      },
	      fmt: function (d) {
	        return "" + Morris.pad2(d.getHours()) + ":" + Morris.pad2(d.getMinutes()) + ":" + Morris.pad2(d.getSeconds());
	      },
	      incr: function (d) {
	        return d.setUTCSeconds(d.getUTCSeconds() + interval);
	      }
	    };
	  };

	  Morris.LABEL_SPECS = {
	    "decade": {
	      span: 172800000000,
	      start: function (d) {
	        return new Date(d.getFullYear() - d.getFullYear() % 10, 0, 1);
	      },
	      fmt: function (d) {
	        return "" + d.getFullYear();
	      },
	      incr: function (d) {
	        return d.setFullYear(d.getFullYear() + 10);
	      }
	    },
	    "year": {
	      span: 17280000000,
	      start: function (d) {
	        return new Date(d.getFullYear(), 0, 1);
	      },
	      fmt: function (d) {
	        return "" + d.getFullYear();
	      },
	      incr: function (d) {
	        return d.setFullYear(d.getFullYear() + 1);
	      }
	    },
	    "month": {
	      span: 2419200000,
	      start: function (d) {
	        return new Date(d.getFullYear(), d.getMonth(), 1);
	      },
	      fmt: function (d) {
	        return "" + d.getFullYear() + "-" + Morris.pad2(d.getMonth() + 1);
	      },
	      incr: function (d) {
	        return d.setMonth(d.getMonth() + 1);
	      }
	    },
	    "week": {
	      span: 604800000,
	      start: function (d) {
	        return new Date(d.getFullYear(), d.getMonth(), d.getDate());
	      },
	      fmt: function (d) {
	        return "" + d.getFullYear() + "-" + Morris.pad2(d.getMonth() + 1) + "-" + Morris.pad2(d.getDate());
	      },
	      incr: function (d) {
	        return d.setDate(d.getDate() + 7);
	      }
	    },
	    "day": {
	      span: 86400000,
	      start: function (d) {
	        return new Date(d.getFullYear(), d.getMonth(), d.getDate());
	      },
	      fmt: function (d) {
	        return "" + d.getFullYear() + "-" + Morris.pad2(d.getMonth() + 1) + "-" + Morris.pad2(d.getDate());
	      },
	      incr: function (d) {
	        return d.setDate(d.getDate() + 1);
	      }
	    },
	    "hour": minutesSpecHelper(60),
	    "30min": minutesSpecHelper(30),
	    "15min": minutesSpecHelper(15),
	    "10min": minutesSpecHelper(10),
	    "5min": minutesSpecHelper(5),
	    "minute": minutesSpecHelper(1),
	    "30sec": secondsSpecHelper(30),
	    "15sec": secondsSpecHelper(15),
	    "10sec": secondsSpecHelper(10),
	    "5sec": secondsSpecHelper(5),
	    "second": secondsSpecHelper(1)
	  };

	  Morris.AUTO_LABEL_ORDER = ["decade", "year", "month", "week", "day", "hour", "30min", "15min", "10min", "5min", "minute", "30sec", "15sec", "10sec", "5sec", "second"];

	  Morris.Area = function (_super) {
	    var areaDefaults;

	    __extends(Area, _super);

	    areaDefaults = {
	      fillOpacity: 'auto',
	      behaveLikeLine: false
	    };

	    function Area(options) {
	      var areaOptions;
	      if (!(this instanceof Morris.Area)) {
	        return new Morris.Area(options);
	      }
	      areaOptions = $.extend({}, areaDefaults, options);
	      this.cumulative = !areaOptions.behaveLikeLine;
	      if (areaOptions.fillOpacity === 'auto') {
	        areaOptions.fillOpacity = areaOptions.behaveLikeLine ? .8 : 1;
	      }
	      Area.__super__.constructor.call(this, areaOptions);
	    }

	    Area.prototype.calcPoints = function () {
	      var row, total, y, _i, _len, _ref, _results;
	      _ref = this.data;
	      _results = [];
	      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	        row = _ref[_i];
	        row._x = this.transX(row.x);
	        total = 0;
	        row._y = function () {
	          var _j, _len1, _ref1, _results1;
	          _ref1 = row.y;
	          _results1 = [];
	          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
	            y = _ref1[_j];
	            if (this.options.behaveLikeLine) {
	              _results1.push(this.transY(y));
	            } else {
	              total += y || 0;
	              _results1.push(this.transY(total));
	            }
	          }
	          return _results1;
	        }.call(this);
	        _results.push(row._ymax = Math.max.apply(Math, row._y));
	      }
	      return _results;
	    };

	    Area.prototype.drawSeries = function () {
	      var i, range, _i, _j, _k, _len, _ref, _ref1, _results, _results1, _results2;
	      this.seriesPoints = [];
	      if (this.options.behaveLikeLine) {
	        range = function () {
	          _results = [];
	          for (var _i = 0, _ref = this.options.ykeys.length - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; 0 <= _ref ? _i++ : _i--) {
	            _results.push(_i);
	          }
	          return _results;
	        }.apply(this);
	      } else {
	        range = function () {
	          _results1 = [];
	          for (var _j = _ref1 = this.options.ykeys.length - 1; _ref1 <= 0 ? _j <= 0 : _j >= 0; _ref1 <= 0 ? _j++ : _j--) {
	            _results1.push(_j);
	          }
	          return _results1;
	        }.apply(this);
	      }
	      _results2 = [];
	      for (_k = 0, _len = range.length; _k < _len; _k++) {
	        i = range[_k];
	        this._drawFillFor(i);
	        this._drawLineFor(i);
	        _results2.push(this._drawPointFor(i));
	      }
	      return _results2;
	    };

	    Area.prototype._drawFillFor = function (index) {
	      var path;
	      path = this.paths[index];
	      if (path !== null) {
	        path = path + ("L" + this.transX(this.xmax) + "," + this.bottom + "L" + this.transX(this.xmin) + "," + this.bottom + "Z");
	        return this.drawFilledPath(path, this.fillForSeries(index));
	      }
	    };

	    Area.prototype.fillForSeries = function (i) {
	      var color;
	      color = Raphael.rgb2hsl(this.colorFor(this.data[i], i, 'line'));
	      return Raphael.hsl(color.h, this.options.behaveLikeLine ? color.s * 0.9 : color.s * 0.75, Math.min(0.98, this.options.behaveLikeLine ? color.l * 1.2 : color.l * 1.25));
	    };

	    Area.prototype.drawFilledPath = function (path, fill) {
	      return this.raphael.path(path).attr('fill', fill).attr('fill-opacity', this.options.fillOpacity).attr('stroke', 'none');
	    };

	    return Area;
	  }(Morris.Line);

	  Morris.Bar = function (_super) {
	    __extends(Bar, _super);

	    function Bar(options) {
	      this.onHoverOut = __bind(this.onHoverOut, this);
	      this.onHoverMove = __bind(this.onHoverMove, this);
	      this.onGridClick = __bind(this.onGridClick, this);
	      if (!(this instanceof Morris.Bar)) {
	        return new Morris.Bar(options);
	      }
	      Bar.__super__.constructor.call(this, $.extend({}, options, {
	        parseTime: false
	      }));
	    }

	    Bar.prototype.init = function () {
	      this.cumulative = this.options.stacked;
	      if (this.options.hideHover !== 'always') {
	        this.hover = new Morris.Hover({
	          parent: this.el
	        });
	        this.on('hovermove', this.onHoverMove);
	        this.on('hoverout', this.onHoverOut);
	        return this.on('gridclick', this.onGridClick);
	      }
	    };

	    Bar.prototype.defaults = {
	      barSizeRatio: 0.75,
	      barGap: 3,
	      barColors: ['#0b62a4', '#7a92a3', '#4da74d', '#afd8f8', '#edc240', '#cb4b4b', '#9440ed'],
	      barOpacity: 1.0,
	      barRadius: [0, 0, 0, 0],
	      xLabelMargin: 50
	    };

	    Bar.prototype.calc = function () {
	      var _ref;
	      this.calcBars();
	      if (this.options.hideHover === false) {
	        return (_ref = this.hover).update.apply(_ref, this.hoverContentForRow(this.data.length - 1));
	      }
	    };

	    Bar.prototype.calcBars = function () {
	      var idx, row, y, _i, _len, _ref, _results;
	      _ref = this.data;
	      _results = [];
	      for (idx = _i = 0, _len = _ref.length; _i < _len; idx = ++_i) {
	        row = _ref[idx];
	        row._x = this.left + this.width * (idx + 0.5) / this.data.length;
	        _results.push(row._y = function () {
	          var _j, _len1, _ref1, _results1;
	          _ref1 = row.y;
	          _results1 = [];
	          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
	            y = _ref1[_j];
	            if (y != null) {
	              _results1.push(this.transY(y));
	            } else {
	              _results1.push(null);
	            }
	          }
	          return _results1;
	        }.call(this));
	      }
	      return _results;
	    };

	    Bar.prototype.draw = function () {
	      var _ref;
	      if ((_ref = this.options.axes) === true || _ref === 'both' || _ref === 'x') {
	        this.drawXAxis();
	      }
	      return this.drawSeries();
	    };

	    Bar.prototype.drawXAxis = function () {
	      var i, label, labelBox, margin, offset, prevAngleMargin, prevLabelMargin, row, textBox, ypos, _i, _ref, _results;
	      ypos = this.bottom + (this.options.xAxisLabelTopPadding || this.options.padding / 2);
	      prevLabelMargin = null;
	      prevAngleMargin = null;
	      _results = [];
	      for (i = _i = 0, _ref = this.data.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
	        row = this.data[this.data.length - 1 - i];
	        label = this.drawXAxisLabel(row._x, ypos, row.label);
	        textBox = label.getBBox();
	        label.transform("r" + -this.options.xLabelAngle);
	        labelBox = label.getBBox();
	        label.transform("t0," + labelBox.height / 2 + "...");
	        if (this.options.xLabelAngle !== 0) {
	          offset = -0.5 * textBox.width * Math.cos(this.options.xLabelAngle * Math.PI / 180.0);
	          label.transform("t" + offset + ",0...");
	        }
	        if ((prevLabelMargin == null || prevLabelMargin >= labelBox.x + labelBox.width || prevAngleMargin != null && prevAngleMargin >= labelBox.x) && labelBox.x >= 0 && labelBox.x + labelBox.width < this.el.width()) {
	          if (this.options.xLabelAngle !== 0) {
	            margin = 1.25 * this.options.gridTextSize / Math.sin(this.options.xLabelAngle * Math.PI / 180.0);
	            prevAngleMargin = labelBox.x - margin;
	          }
	          _results.push(prevLabelMargin = labelBox.x - this.options.xLabelMargin);
	        } else {
	          _results.push(label.remove());
	        }
	      }
	      return _results;
	    };

	    Bar.prototype.drawSeries = function () {
	      var barWidth, bottom, groupWidth, idx, lastTop, left, leftPadding, numBars, row, sidx, size, spaceLeft, top, ypos, zeroPos;
	      groupWidth = this.width / this.options.data.length;
	      numBars = this.options.stacked ? 1 : this.options.ykeys.length;
	      barWidth = (groupWidth * this.options.barSizeRatio - this.options.barGap * (numBars - 1)) / numBars;
	      if (this.options.barSize) {
	        barWidth = Math.min(barWidth, this.options.barSize);
	      }
	      spaceLeft = groupWidth - barWidth * numBars - this.options.barGap * (numBars - 1);
	      leftPadding = spaceLeft / 2;
	      zeroPos = this.ymin <= 0 && this.ymax >= 0 ? this.transY(0) : null;
	      return this.bars = function () {
	        var _i, _len, _ref, _results;
	        _ref = this.data;
	        _results = [];
	        for (idx = _i = 0, _len = _ref.length; _i < _len; idx = ++_i) {
	          row = _ref[idx];
	          lastTop = 0;
	          _results.push(function () {
	            var _j, _len1, _ref1, _results1;
	            _ref1 = row._y;
	            _results1 = [];
	            for (sidx = _j = 0, _len1 = _ref1.length; _j < _len1; sidx = ++_j) {
	              ypos = _ref1[sidx];
	              if (ypos !== null) {
	                if (zeroPos) {
	                  top = Math.min(ypos, zeroPos);
	                  bottom = Math.max(ypos, zeroPos);
	                } else {
	                  top = ypos;
	                  bottom = this.bottom;
	                }
	                left = this.left + idx * groupWidth + leftPadding;
	                if (!this.options.stacked) {
	                  left += sidx * (barWidth + this.options.barGap);
	                }
	                size = bottom - top;
	                if (this.options.verticalGridCondition && this.options.verticalGridCondition(row.x)) {
	                  this.drawBar(this.left + idx * groupWidth, this.top, groupWidth, Math.abs(this.top - this.bottom), this.options.verticalGridColor, this.options.verticalGridOpacity, this.options.barRadius);
	                }
	                if (this.options.stacked) {
	                  top -= lastTop;
	                }
	                this.drawBar(left, top, barWidth, size, this.colorFor(row, sidx, 'bar'), this.options.barOpacity, this.options.barRadius);
	                _results1.push(lastTop += size);
	              } else {
	                _results1.push(null);
	              }
	            }
	            return _results1;
	          }.call(this));
	        }
	        return _results;
	      }.call(this);
	    };

	    Bar.prototype.colorFor = function (row, sidx, type) {
	      var r, s;
	      if (typeof this.options.barColors === 'function') {
	        r = {
	          x: row.x,
	          y: row.y[sidx],
	          label: row.label
	        };
	        s = {
	          index: sidx,
	          key: this.options.ykeys[sidx],
	          label: this.options.labels[sidx]
	        };
	        return this.options.barColors.call(this, r, s, type);
	      } else {
	        return this.options.barColors[sidx % this.options.barColors.length];
	      }
	    };

	    Bar.prototype.hitTest = function (x) {
	      if (this.data.length === 0) {
	        return null;
	      }
	      x = Math.max(Math.min(x, this.right), this.left);
	      return Math.min(this.data.length - 1, Math.floor((x - this.left) / (this.width / this.data.length)));
	    };

	    Bar.prototype.onGridClick = function (x, y) {
	      var index;
	      index = this.hitTest(x);
	      return this.fire('click', index, this.data[index].src, x, y);
	    };

	    Bar.prototype.onHoverMove = function (x, y) {
	      var index, _ref;
	      index = this.hitTest(x);
	      return (_ref = this.hover).update.apply(_ref, this.hoverContentForRow(index));
	    };

	    Bar.prototype.onHoverOut = function () {
	      if (this.options.hideHover !== false) {
	        return this.hover.hide();
	      }
	    };

	    Bar.prototype.hoverContentForRow = function (index) {
	      var content, j, row, x, y, _i, _len, _ref;
	      row = this.data[index];
	      content = "<div class='morris-hover-row-label'>" + row.label + "</div>";
	      _ref = row.y;
	      for (j = _i = 0, _len = _ref.length; _i < _len; j = ++_i) {
	        y = _ref[j];
	        content += "<div class='morris-hover-point' style='color: " + this.colorFor(row, j, 'label') + "'>\n  " + this.options.labels[j] + ":\n  " + this.yLabelFormat(y) + "\n</div>";
	      }
	      if (typeof this.options.hoverCallback === 'function') {
	        content = this.options.hoverCallback(index, this.options, content, row.src);
	      }
	      x = this.left + (index + 0.5) * this.width / this.data.length;
	      return [content, x];
	    };

	    Bar.prototype.drawXAxisLabel = function (xPos, yPos, text) {
	      var label;
	      return label = this.raphael.text(xPos, yPos, text).attr('font-size', this.options.gridTextSize).attr('font-family', this.options.gridTextFamily).attr('font-weight', this.options.gridTextWeight).attr('fill', this.options.gridTextColor);
	    };

	    Bar.prototype.drawBar = function (xPos, yPos, width, height, barColor, opacity, radiusArray) {
	      var maxRadius, path;
	      maxRadius = Math.max.apply(Math, radiusArray);
	      if (maxRadius === 0 || maxRadius > height) {
	        path = this.raphael.rect(xPos, yPos, width, height);
	      } else {
	        path = this.raphael.path(this.roundedRect(xPos, yPos, width, height, radiusArray));
	      }
	      return path.attr('fill', barColor).attr('fill-opacity', opacity).attr('stroke', 'none');
	    };

	    Bar.prototype.roundedRect = function (x, y, w, h, r) {
	      if (r == null) {
	        r = [0, 0, 0, 0];
	      }
	      return ["M", x, r[0] + y, "Q", x, y, x + r[0], y, "L", x + w - r[1], y, "Q", x + w, y, x + w, y + r[1], "L", x + w, y + h - r[2], "Q", x + w, y + h, x + w - r[2], y + h, "L", x + r[3], y + h, "Q", x, y + h, x, y + h - r[3], "Z"];
	    };

	    return Bar;
	  }(Morris.Grid);

	  Morris.Donut = function (_super) {
	    __extends(Donut, _super);

	    Donut.prototype.defaults = {
	      colors: ['#0B62A4', '#3980B5', '#679DC6', '#95BBD7', '#B0CCE1', '#095791', '#095085', '#083E67', '#052C48', '#042135'],
	      backgroundColor: '#FFFFFF',
	      labelColor: '#000000',
	      formatter: Morris.commas,
	      resize: false
	    };

	    function Donut(options) {
	      this.resizeHandler = __bind(this.resizeHandler, this);
	      this.select = __bind(this.select, this);
	      this.click = __bind(this.click, this);
	      var _this = this;
	      if (!(this instanceof Morris.Donut)) {
	        return new Morris.Donut(options);
	      }
	      this.options = $.extend({}, this.defaults, options);
	      if (typeof options.element === 'string') {
	        this.el = $(document.getElementById(options.element));
	      } else {
	        this.el = $(options.element);
	      }
	      if (this.el === null || this.el.length === 0) {
	        throw new Error("Graph placeholder not found.");
	      }
	      if (options.data === void 0 || options.data.length === 0) {
	        return;
	      }
	      this.raphael = new Raphael(this.el[0]);
	      if (this.options.resize) {
	        $(window).bind('resize', function (evt) {
	          if (_this.timeoutId != null) {
	            window.clearTimeout(_this.timeoutId);
	          }
	          return _this.timeoutId = window.setTimeout(_this.resizeHandler, 100);
	        });
	      }
	      this.setData(options.data);
	    }

	    Donut.prototype.redraw = function () {
	      var C, cx, cy, i, idx, last, max_value, min, next, seg, total, value, w, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _results;
	      this.raphael.clear();
	      cx = this.el.width() / 2;
	      cy = this.el.height() / 2;
	      w = (Math.min(cx, cy) - 10) / 3;
	      total = 0;
	      _ref = this.values;
	      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	        value = _ref[_i];
	        total += value;
	      }
	      min = 5 / (2 * w);
	      C = 1.9999 * Math.PI - min * this.data.length;
	      last = 0;
	      idx = 0;
	      this.segments = [];
	      _ref1 = this.values;
	      for (i = _j = 0, _len1 = _ref1.length; _j < _len1; i = ++_j) {
	        value = _ref1[i];
	        next = last + min + C * (value / total);
	        seg = new Morris.DonutSegment(cx, cy, w * 2, w, last, next, this.data[i].color || this.options.colors[idx % this.options.colors.length], this.options.backgroundColor, idx, this.raphael);
	        seg.render();
	        this.segments.push(seg);
	        seg.on('hover', this.select);
	        seg.on('click', this.click);
	        last = next;
	        idx += 1;
	      }
	      this.text1 = this.drawEmptyDonutLabel(cx, cy - 10, this.options.labelColor, 15, 800);
	      this.text2 = this.drawEmptyDonutLabel(cx, cy + 10, this.options.labelColor, 14);
	      max_value = Math.max.apply(Math, this.values);
	      idx = 0;
	      _ref2 = this.values;
	      _results = [];
	      for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
	        value = _ref2[_k];
	        if (value === max_value) {
	          this.select(idx);
	          break;
	        }
	        _results.push(idx += 1);
	      }
	      return _results;
	    };

	    Donut.prototype.setData = function (data) {
	      var row;
	      this.data = data;
	      this.values = function () {
	        var _i, _len, _ref, _results;
	        _ref = this.data;
	        _results = [];
	        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	          row = _ref[_i];
	          _results.push(parseFloat(row.value));
	        }
	        return _results;
	      }.call(this);
	      return this.redraw();
	    };

	    Donut.prototype.click = function (idx) {
	      return this.fire('click', idx, this.data[idx]);
	    };

	    Donut.prototype.select = function (idx) {
	      var row, s, segment, _i, _len, _ref;
	      _ref = this.segments;
	      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	        s = _ref[_i];
	        s.deselect();
	      }
	      segment = this.segments[idx];
	      segment.select();
	      row = this.data[idx];
	      return this.setLabels(row.label, this.options.formatter(row.value, row));
	    };

	    Donut.prototype.setLabels = function (label1, label2) {
	      var inner, maxHeightBottom, maxHeightTop, maxWidth, text1bbox, text1scale, text2bbox, text2scale;
	      inner = (Math.min(this.el.width() / 2, this.el.height() / 2) - 10) * 2 / 3;
	      maxWidth = 1.8 * inner;
	      maxHeightTop = inner / 2;
	      maxHeightBottom = inner / 3;
	      this.text1.attr({
	        text: label1,
	        transform: ''
	      });
	      text1bbox = this.text1.getBBox();
	      text1scale = Math.min(maxWidth / text1bbox.width, maxHeightTop / text1bbox.height);
	      this.text1.attr({
	        transform: "S" + text1scale + "," + text1scale + "," + (text1bbox.x + text1bbox.width / 2) + "," + (text1bbox.y + text1bbox.height)
	      });
	      this.text2.attr({
	        text: label2,
	        transform: ''
	      });
	      text2bbox = this.text2.getBBox();
	      text2scale = Math.min(maxWidth / text2bbox.width, maxHeightBottom / text2bbox.height);
	      return this.text2.attr({
	        transform: "S" + text2scale + "," + text2scale + "," + (text2bbox.x + text2bbox.width / 2) + "," + text2bbox.y
	      });
	    };

	    Donut.prototype.drawEmptyDonutLabel = function (xPos, yPos, color, fontSize, fontWeight) {
	      var text;
	      text = this.raphael.text(xPos, yPos, '').attr('font-size', fontSize).attr('fill', color);
	      if (fontWeight != null) {
	        text.attr('font-weight', fontWeight);
	      }
	      return text;
	    };

	    Donut.prototype.resizeHandler = function () {
	      this.timeoutId = null;
	      this.raphael.setSize(this.el.width(), this.el.height());
	      return this.redraw();
	    };

	    return Donut;
	  }(Morris.EventEmitter);

	  Morris.DonutSegment = function (_super) {
	    __extends(DonutSegment, _super);

	    function DonutSegment(cx, cy, inner, outer, p0, p1, color, backgroundColor, index, raphael) {
	      this.cx = cx;
	      this.cy = cy;
	      this.inner = inner;
	      this.outer = outer;
	      this.color = color;
	      this.backgroundColor = backgroundColor;
	      this.index = index;
	      this.raphael = raphael;
	      this.deselect = __bind(this.deselect, this);
	      this.select = __bind(this.select, this);
	      this.sin_p0 = Math.sin(p0);
	      this.cos_p0 = Math.cos(p0);
	      this.sin_p1 = Math.sin(p1);
	      this.cos_p1 = Math.cos(p1);
	      this.is_long = p1 - p0 > Math.PI ? 1 : 0;
	      this.path = this.calcSegment(this.inner + 3, this.inner + this.outer - 5);
	      this.selectedPath = this.calcSegment(this.inner + 3, this.inner + this.outer);
	      this.hilight = this.calcArc(this.inner);
	    }

	    DonutSegment.prototype.calcArcPoints = function (r) {
	      return [this.cx + r * this.sin_p0, this.cy + r * this.cos_p0, this.cx + r * this.sin_p1, this.cy + r * this.cos_p1];
	    };

	    DonutSegment.prototype.calcSegment = function (r1, r2) {
	      var ix0, ix1, iy0, iy1, ox0, ox1, oy0, oy1, _ref, _ref1;
	      _ref = this.calcArcPoints(r1), ix0 = _ref[0], iy0 = _ref[1], ix1 = _ref[2], iy1 = _ref[3];
	      _ref1 = this.calcArcPoints(r2), ox0 = _ref1[0], oy0 = _ref1[1], ox1 = _ref1[2], oy1 = _ref1[3];
	      return "M" + ix0 + "," + iy0 + ("A" + r1 + "," + r1 + ",0," + this.is_long + ",0," + ix1 + "," + iy1) + ("L" + ox1 + "," + oy1) + ("A" + r2 + "," + r2 + ",0," + this.is_long + ",1," + ox0 + "," + oy0) + "Z";
	    };

	    DonutSegment.prototype.calcArc = function (r) {
	      var ix0, ix1, iy0, iy1, _ref;
	      _ref = this.calcArcPoints(r), ix0 = _ref[0], iy0 = _ref[1], ix1 = _ref[2], iy1 = _ref[3];
	      return "M" + ix0 + "," + iy0 + ("A" + r + "," + r + ",0," + this.is_long + ",0," + ix1 + "," + iy1);
	    };

	    DonutSegment.prototype.render = function () {
	      var _this = this;
	      this.arc = this.drawDonutArc(this.hilight, this.color);
	      return this.seg = this.drawDonutSegment(this.path, this.color, this.backgroundColor, function () {
	        return _this.fire('hover', _this.index);
	      }, function () {
	        return _this.fire('click', _this.index);
	      });
	    };

	    DonutSegment.prototype.drawDonutArc = function (path, color) {
	      return this.raphael.path(path).attr({
	        stroke: color,
	        'stroke-width': 2,
	        opacity: 0
	      });
	    };

	    DonutSegment.prototype.drawDonutSegment = function (path, fillColor, strokeColor, hoverFunction, clickFunction) {
	      return this.raphael.path(path).attr({
	        fill: fillColor,
	        stroke: strokeColor,
	        'stroke-width': 3
	      }).hover(hoverFunction).click(clickFunction);
	    };

	    DonutSegment.prototype.select = function () {
	      if (!this.selected) {
	        this.seg.animate({
	          path: this.selectedPath
	        }, 150, '<>');
	        this.arc.animate({
	          opacity: 1
	        }, 150, '<>');
	        return this.selected = true;
	      }
	    };

	    DonutSegment.prototype.deselect = function () {
	      if (this.selected) {
	        this.seg.animate({
	          path: this.path
	        }, 150, '<>');
	        this.arc.animate({
	          opacity: 0
	        }, 150, '<>');
	        return this.selected = false;
	      }
	    };

	    return DonutSegment;
	  }(Morris.EventEmitter);
	}).call(this);
	}.call(window));

/***/ },
/* 79 */,
/* 80 */
/***/ function(module, exports) {

	/*** IMPORTS FROM imports-loader ***/
	(function() {

	/**
	 *	Neon Login Script
	 *
	 *	Developed by Arlind Nushi - www.laborator.co
	 */

	var neonLogin = neonLogin || {};

	;(function ($, window, undefined) {
		"use strict";

		$(document).ready(function () {
			neonLogin.$container = $("#form_login");

			// Login Form & Validation
			neonLogin.$container.validate({
				rules: {
					username: {
						required: true
					},

					password: {
						required: true
					}

				},

				highlight: function (element) {
					$(element).closest('.input-group').addClass('validate-has-error');
				},

				unhighlight: function (element) {
					$(element).closest('.input-group').removeClass('validate-has-error');
				},

				submitHandler: function (ev) {
					/* 
	    	Updated on v1.1.4
	    	Login form now processes the login data, here is the file: data/sample-login-form.php
	    */

					$(".login-page").addClass('logging-in'); // This will hide the login form and init the progress bar

					// Hide Errors
					$(".form-login-error").slideUp('fast');

					// We will wait till the transition ends				
					setTimeout(function () {
						var random_pct = 25 + Math.round(Math.random() * 30);

						// The form data are subbmitted, we can forward the progress to 70%
						neonLogin.setPercentage(40 + random_pct);

						// Send data to the server
						$.ajax({
							url: '/api/v1/login',
							method: 'POST',
							data: JSON.stringify({
								login: $("input#username").val(),
								password: $("input#password").val()
							}),
							contentType: "application/json",
							error: function () {
								alert("An error occoured!");
							},
							success: function (response) {
								var status = response.status;

								neonLogin.setPercentage(100);

								setTimeout(function () {
									if (!status) {
										$(".login-page").removeClass('logging-in');
										neonLogin.resetProgressBar(true);
									} else {
										setTimeout(function () {
											var redirect_url = '/';

											if (response.redirectUrl && response.redirectUrl.length) {
												redirect_url = response.redirectUrl;
											}

											window.location.href = redirect_url;
										}, 400);
									}
								}, 1000);
							}
						});
					}, 650);
				}
			});

			// Lockscreen & Validation
			var is_lockscreen = $(".login-page").hasClass('is-lockscreen');

			if (is_lockscreen) {
				neonLogin.$container = $("#form_lockscreen");
				neonLogin.$ls_thumb = neonLogin.$container.find('.lockscreen-thumb');

				neonLogin.$container.validate({
					rules: {

						password: {
							required: true
						}

					},

					highlight: function (element) {
						$(element).closest('.input-group').addClass('validate-has-error');
					},

					unhighlight: function (element) {
						$(element).closest('.input-group').removeClass('validate-has-error');
					},

					submitHandler: function (ev) {
						/* 
	     	Demo Purpose Only 
	     	
	     	Here you can handle the page login, currently it does not process anything, just fills the loader.
	     */

						$(".login-page").addClass('logging-in-lockscreen'); // This will hide the login form and init the progress bar

						// We will wait till the transition ends				
						setTimeout(function () {
							var random_pct = 25 + Math.round(Math.random() * 30);

							neonLogin.setPercentage(random_pct, function () {
								// Just an example, this is phase 1
								// Do some stuff...

								// After 0.77s second we will execute the next phase
								setTimeout(function () {
									neonLogin.setPercentage(100, function () {
										// Just an example, this is phase 2
										// Do some other stuff...

										// Redirect to the page
										setTimeout("window.location.href = '../../'", 600);
									}, 2);
								}, 820);
							});
						}, 650);
					}
				});
			}

			// Login Form Setup
			neonLogin.$body = $(".login-page");
			neonLogin.$login_progressbar_indicator = $(".login-progressbar-indicator h3");
			neonLogin.$login_progressbar = neonLogin.$body.find(".login-progressbar div");

			neonLogin.$login_progressbar_indicator.html('0%');

			if (neonLogin.$body.hasClass('login-form-fall')) {
				var focus_set = false;

				setTimeout(function () {
					neonLogin.$body.addClass('login-form-fall-init');

					setTimeout(function () {
						if (!focus_set) {
							neonLogin.$container.find('input:first').focus();
							focus_set = true;
						}
					}, 550);
				}, 0);
			} else {
				neonLogin.$container.find('input:first').focus();
			}

			// Focus Class
			neonLogin.$container.find('.form-control').each(function (i, el) {
				var $this = $(el),
				    $group = $this.closest('.input-group');

				$this.prev('.input-group-addon').click(function () {
					$this.focus();
				});

				$this.on({
					focus: function () {
						$group.addClass('focused');
					},

					blur: function () {
						$group.removeClass('focused');
					}
				});
			});

			// Functions
			$.extend(neonLogin, {
				setPercentage: function (pct, callback) {
					pct = parseInt(pct / 100 * 100, 10) + '%';

					// Lockscreen
					if (is_lockscreen) {
						neonLogin.$lockscreen_progress_indicator.html(pct);

						var o = {
							pct: currentProgress
						};

						TweenMax.to(o, .7, {
							pct: parseInt(pct, 10),
							roundProps: ["pct"],
							ease: Sine.easeOut,
							onUpdate: function () {
								neonLogin.$lockscreen_progress_indicator.html(o.pct + '%');
								drawProgress(parseInt(o.pct, 10) / 100);
							},
							onComplete: callback
						});
						return;
					}

					// Normal Login
					neonLogin.$login_progressbar_indicator.html(pct);
					neonLogin.$login_progressbar.width(pct);

					var o = {
						pct: parseInt(neonLogin.$login_progressbar.width() / neonLogin.$login_progressbar.parent().width() * 100, 10)
					};

					TweenMax.to(o, .7, {
						pct: parseInt(pct, 10),
						roundProps: ["pct"],
						ease: Sine.easeOut,
						onUpdate: function () {
							neonLogin.$login_progressbar_indicator.html(o.pct + '%');
						},
						onComplete: callback
					});
				},

				resetProgressBar: function (display_errors) {
					TweenMax.set(neonLogin.$container, { css: { opacity: 0 } });

					setTimeout(function () {
						TweenMax.to(neonLogin.$container, .6, { css: { opacity: 1 }, onComplete: function () {
								neonLogin.$container.attr('style', '');
							} });

						neonLogin.$login_progressbar_indicator.html('0%');
						neonLogin.$login_progressbar.width(0);

						if (display_errors) {
							var $errors_container = $(".form-login-error");

							$errors_container.show();
							var height = $errors_container.outerHeight();

							$errors_container.css({
								height: 0
							});

							TweenMax.to($errors_container, .45, { css: { height: height }, onComplete: function () {
									$errors_container.css({ height: 'auto' });
								} });

							// Reset password fields
							neonLogin.$container.find('input[type="password"]').val('');
						}
					}, 800);
				}
			});

			// Lockscreen Create Canvas
			if (is_lockscreen) {
				neonLogin.$lockscreen_progress_canvas = $('<canvas></canvas>');
				neonLogin.$lockscreen_progress_indicator = neonLogin.$container.find('.lockscreen-progress-indicator');

				neonLogin.$lockscreen_progress_canvas.appendTo(neonLogin.$ls_thumb);

				var line_width = 3,
				    thumb_size = neonLogin.$ls_thumb.width() + line_width;

				neonLogin.$lockscreen_progress_canvas.attr({
					width: thumb_size,
					height: thumb_size
				}).css({
					top: -line_width / 2,
					left: -line_width / 2
				});

				neonLogin.lockscreen_progress_canvas = neonLogin.$lockscreen_progress_canvas.get(0);

				// Create Progress Circle
				var bg = neonLogin.lockscreen_progress_canvas,
				    ctx = ctx = bg.getContext('2d'),
				    imd = null,
				    circ = Math.PI * 2,
				    quart = Math.PI / 2,
				    currentProgress = 0;

				ctx.beginPath();
				ctx.strokeStyle = '#eb7067';
				ctx.lineCap = 'square';
				ctx.closePath();
				ctx.fill();
				ctx.lineWidth = line_width;

				imd = ctx.getImageData(0, 0, thumb_size, thumb_size);

				var drawProgress = function (current) {
					ctx.putImageData(imd, 0, 0);
					ctx.beginPath();
					ctx.arc(thumb_size / 2, thumb_size / 2, 70, -quart, circ * current - quart, false);
					ctx.stroke();

					currentProgress = current * 100;
				};

				drawProgress(0 / 100);

				neonLogin.$lockscreen_progress_indicator.html('0%');

				ctx.restore();
			}
		});
	})(jQuery, window);
	}.call(window));

/***/ },
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
	 * jQuery Validation Plugin v1.15.0
	 *
	 * http://jqueryvalidation.org/
	 *
	 * Copyright (c) 2016 Jörn Zaefferer
	 * Released under the MIT license
	 */
	(function( factory ) {
		if ( true ) {
			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		} else if (typeof module === "object" && module.exports) {
			module.exports = factory( require( "jquery" ) );
		} else {
			factory( jQuery );
		}
	}(function( $ ) {

	$.extend( $.fn, {

		// http://jqueryvalidation.org/validate/
		validate: function( options ) {

			// If nothing is selected, return nothing; can't chain anyway
			if ( !this.length ) {
				if ( options && options.debug && window.console ) {
					console.warn( "Nothing selected, can't validate, returning nothing." );
				}
				return;
			}

			// Check if a validator for this form was already created
			var validator = $.data( this[ 0 ], "validator" );
			if ( validator ) {
				return validator;
			}

			// Add novalidate tag if HTML5.
			this.attr( "novalidate", "novalidate" );

			validator = new $.validator( options, this[ 0 ] );
			$.data( this[ 0 ], "validator", validator );

			if ( validator.settings.onsubmit ) {

				this.on( "click.validate", ":submit", function( event ) {
					if ( validator.settings.submitHandler ) {
						validator.submitButton = event.target;
					}

					// Allow suppressing validation by adding a cancel class to the submit button
					if ( $( this ).hasClass( "cancel" ) ) {
						validator.cancelSubmit = true;
					}

					// Allow suppressing validation by adding the html5 formnovalidate attribute to the submit button
					if ( $( this ).attr( "formnovalidate" ) !== undefined ) {
						validator.cancelSubmit = true;
					}
				} );

				// Validate the form on submit
				this.on( "submit.validate", function( event ) {
					if ( validator.settings.debug ) {

						// Prevent form submit to be able to see console output
						event.preventDefault();
					}
					function handle() {
						var hidden, result;
						if ( validator.settings.submitHandler ) {
							if ( validator.submitButton ) {

								// Insert a hidden input as a replacement for the missing submit button
								hidden = $( "<input type='hidden'/>" )
									.attr( "name", validator.submitButton.name )
									.val( $( validator.submitButton ).val() )
									.appendTo( validator.currentForm );
							}
							result = validator.settings.submitHandler.call( validator, validator.currentForm, event );
							if ( validator.submitButton ) {

								// And clean up afterwards; thanks to no-block-scope, hidden can be referenced
								hidden.remove();
							}
							if ( result !== undefined ) {
								return result;
							}
							return false;
						}
						return true;
					}

					// Prevent submit for invalid forms or custom submit handlers
					if ( validator.cancelSubmit ) {
						validator.cancelSubmit = false;
						return handle();
					}
					if ( validator.form() ) {
						if ( validator.pendingRequest ) {
							validator.formSubmitted = true;
							return false;
						}
						return handle();
					} else {
						validator.focusInvalid();
						return false;
					}
				} );
			}

			return validator;
		},

		// http://jqueryvalidation.org/valid/
		valid: function() {
			var valid, validator, errorList;

			if ( $( this[ 0 ] ).is( "form" ) ) {
				valid = this.validate().form();
			} else {
				errorList = [];
				valid = true;
				validator = $( this[ 0 ].form ).validate();
				this.each( function() {
					valid = validator.element( this ) && valid;
					if ( !valid ) {
						errorList = errorList.concat( validator.errorList );
					}
				} );
				validator.errorList = errorList;
			}
			return valid;
		},

		// http://jqueryvalidation.org/rules/
		rules: function( command, argument ) {

			// If nothing is selected, return nothing; can't chain anyway
			if ( !this.length ) {
				return;
			}

			var element = this[ 0 ],
				settings, staticRules, existingRules, data, param, filtered;

			if ( command ) {
				settings = $.data( element.form, "validator" ).settings;
				staticRules = settings.rules;
				existingRules = $.validator.staticRules( element );
				switch ( command ) {
				case "add":
					$.extend( existingRules, $.validator.normalizeRule( argument ) );

					// Remove messages from rules, but allow them to be set separately
					delete existingRules.messages;
					staticRules[ element.name ] = existingRules;
					if ( argument.messages ) {
						settings.messages[ element.name ] = $.extend( settings.messages[ element.name ], argument.messages );
					}
					break;
				case "remove":
					if ( !argument ) {
						delete staticRules[ element.name ];
						return existingRules;
					}
					filtered = {};
					$.each( argument.split( /\s/ ), function( index, method ) {
						filtered[ method ] = existingRules[ method ];
						delete existingRules[ method ];
						if ( method === "required" ) {
							$( element ).removeAttr( "aria-required" );
						}
					} );
					return filtered;
				}
			}

			data = $.validator.normalizeRules(
			$.extend(
				{},
				$.validator.classRules( element ),
				$.validator.attributeRules( element ),
				$.validator.dataRules( element ),
				$.validator.staticRules( element )
			), element );

			// Make sure required is at front
			if ( data.required ) {
				param = data.required;
				delete data.required;
				data = $.extend( { required: param }, data );
				$( element ).attr( "aria-required", "true" );
			}

			// Make sure remote is at back
			if ( data.remote ) {
				param = data.remote;
				delete data.remote;
				data = $.extend( data, { remote: param } );
			}

			return data;
		}
	} );

	// Custom selectors
	$.extend( $.expr[ ":" ], {

		// http://jqueryvalidation.org/blank-selector/
		blank: function( a ) {
			return !$.trim( "" + $( a ).val() );
		},

		// http://jqueryvalidation.org/filled-selector/
		filled: function( a ) {
			var val = $( a ).val();
			return val !== null && !!$.trim( "" + val );
		},

		// http://jqueryvalidation.org/unchecked-selector/
		unchecked: function( a ) {
			return !$( a ).prop( "checked" );
		}
	} );

	// Constructor for validator
	$.validator = function( options, form ) {
		this.settings = $.extend( true, {}, $.validator.defaults, options );
		this.currentForm = form;
		this.init();
	};

	// http://jqueryvalidation.org/jQuery.validator.format/
	$.validator.format = function( source, params ) {
		if ( arguments.length === 1 ) {
			return function() {
				var args = $.makeArray( arguments );
				args.unshift( source );
				return $.validator.format.apply( this, args );
			};
		}
		if ( params === undefined ) {
			return source;
		}
		if ( arguments.length > 2 && params.constructor !== Array  ) {
			params = $.makeArray( arguments ).slice( 1 );
		}
		if ( params.constructor !== Array ) {
			params = [ params ];
		}
		$.each( params, function( i, n ) {
			source = source.replace( new RegExp( "\\{" + i + "\\}", "g" ), function() {
				return n;
			} );
		} );
		return source;
	};

	$.extend( $.validator, {

		defaults: {
			messages: {},
			groups: {},
			rules: {},
			errorClass: "error",
			pendingClass: "pending",
			validClass: "valid",
			errorElement: "label",
			focusCleanup: false,
			focusInvalid: true,
			errorContainer: $( [] ),
			errorLabelContainer: $( [] ),
			onsubmit: true,
			ignore: ":hidden",
			ignoreTitle: false,
			onfocusin: function( element ) {
				this.lastActive = element;

				// Hide error label and remove error class on focus if enabled
				if ( this.settings.focusCleanup ) {
					if ( this.settings.unhighlight ) {
						this.settings.unhighlight.call( this, element, this.settings.errorClass, this.settings.validClass );
					}
					this.hideThese( this.errorsFor( element ) );
				}
			},
			onfocusout: function( element ) {
				if ( !this.checkable( element ) && ( element.name in this.submitted || !this.optional( element ) ) ) {
					this.element( element );
				}
			},
			onkeyup: function( element, event ) {

				// Avoid revalidate the field when pressing one of the following keys
				// Shift       => 16
				// Ctrl        => 17
				// Alt         => 18
				// Caps lock   => 20
				// End         => 35
				// Home        => 36
				// Left arrow  => 37
				// Up arrow    => 38
				// Right arrow => 39
				// Down arrow  => 40
				// Insert      => 45
				// Num lock    => 144
				// AltGr key   => 225
				var excludedKeys = [
					16, 17, 18, 20, 35, 36, 37,
					38, 39, 40, 45, 144, 225
				];

				if ( event.which === 9 && this.elementValue( element ) === "" || $.inArray( event.keyCode, excludedKeys ) !== -1 ) {
					return;
				} else if ( element.name in this.submitted || element.name in this.invalid ) {
					this.element( element );
				}
			},
			onclick: function( element ) {

				// Click on selects, radiobuttons and checkboxes
				if ( element.name in this.submitted ) {
					this.element( element );

				// Or option elements, check parent select in that case
				} else if ( element.parentNode.name in this.submitted ) {
					this.element( element.parentNode );
				}
			},
			highlight: function( element, errorClass, validClass ) {
				if ( element.type === "radio" ) {
					this.findByName( element.name ).addClass( errorClass ).removeClass( validClass );
				} else {
					$( element ).addClass( errorClass ).removeClass( validClass );
				}
			},
			unhighlight: function( element, errorClass, validClass ) {
				if ( element.type === "radio" ) {
					this.findByName( element.name ).removeClass( errorClass ).addClass( validClass );
				} else {
					$( element ).removeClass( errorClass ).addClass( validClass );
				}
			}
		},

		// http://jqueryvalidation.org/jQuery.validator.setDefaults/
		setDefaults: function( settings ) {
			$.extend( $.validator.defaults, settings );
		},

		messages: {
			required: "This field is required.",
			remote: "Please fix this field.",
			email: "Please enter a valid email address.",
			url: "Please enter a valid URL.",
			date: "Please enter a valid date.",
			dateISO: "Please enter a valid date ( ISO ).",
			number: "Please enter a valid number.",
			digits: "Please enter only digits.",
			equalTo: "Please enter the same value again.",
			maxlength: $.validator.format( "Please enter no more than {0} characters." ),
			minlength: $.validator.format( "Please enter at least {0} characters." ),
			rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
			range: $.validator.format( "Please enter a value between {0} and {1}." ),
			max: $.validator.format( "Please enter a value less than or equal to {0}." ),
			min: $.validator.format( "Please enter a value greater than or equal to {0}." ),
			step: $.validator.format( "Please enter a multiple of {0}." )
		},

		autoCreateRanges: false,

		prototype: {

			init: function() {
				this.labelContainer = $( this.settings.errorLabelContainer );
				this.errorContext = this.labelContainer.length && this.labelContainer || $( this.currentForm );
				this.containers = $( this.settings.errorContainer ).add( this.settings.errorLabelContainer );
				this.submitted = {};
				this.valueCache = {};
				this.pendingRequest = 0;
				this.pending = {};
				this.invalid = {};
				this.reset();

				var groups = ( this.groups = {} ),
					rules;
				$.each( this.settings.groups, function( key, value ) {
					if ( typeof value === "string" ) {
						value = value.split( /\s/ );
					}
					$.each( value, function( index, name ) {
						groups[ name ] = key;
					} );
				} );
				rules = this.settings.rules;
				$.each( rules, function( key, value ) {
					rules[ key ] = $.validator.normalizeRule( value );
				} );

				function delegate( event ) {
					var validator = $.data( this.form, "validator" ),
						eventType = "on" + event.type.replace( /^validate/, "" ),
						settings = validator.settings;
					if ( settings[ eventType ] && !$( this ).is( settings.ignore ) ) {
						settings[ eventType ].call( validator, this, event );
					}
				}

				$( this.currentForm )
					.on( "focusin.validate focusout.validate keyup.validate",
						":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], " +
						"[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], " +
						"[type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], " +
						"[type='radio'], [type='checkbox'], [contenteditable]", delegate )

					// Support: Chrome, oldIE
					// "select" is provided as event.target when clicking a option
					.on( "click.validate", "select, option, [type='radio'], [type='checkbox']", delegate );

				if ( this.settings.invalidHandler ) {
					$( this.currentForm ).on( "invalid-form.validate", this.settings.invalidHandler );
				}

				// Add aria-required to any Static/Data/Class required fields before first validation
				// Screen readers require this attribute to be present before the initial submission http://www.w3.org/TR/WCAG-TECHS/ARIA2.html
				$( this.currentForm ).find( "[required], [data-rule-required], .required" ).attr( "aria-required", "true" );
			},

			// http://jqueryvalidation.org/Validator.form/
			form: function() {
				this.checkForm();
				$.extend( this.submitted, this.errorMap );
				this.invalid = $.extend( {}, this.errorMap );
				if ( !this.valid() ) {
					$( this.currentForm ).triggerHandler( "invalid-form", [ this ] );
				}
				this.showErrors();
				return this.valid();
			},

			checkForm: function() {
				this.prepareForm();
				for ( var i = 0, elements = ( this.currentElements = this.elements() ); elements[ i ]; i++ ) {
					this.check( elements[ i ] );
				}
				return this.valid();
			},

			// http://jqueryvalidation.org/Validator.element/
			element: function( element ) {
				var cleanElement = this.clean( element ),
					checkElement = this.validationTargetFor( cleanElement ),
					v = this,
					result = true,
					rs, group;

				if ( checkElement === undefined ) {
					delete this.invalid[ cleanElement.name ];
				} else {
					this.prepareElement( checkElement );
					this.currentElements = $( checkElement );

					// If this element is grouped, then validate all group elements already
					// containing a value
					group = this.groups[ checkElement.name ];
					if ( group ) {
						$.each( this.groups, function( name, testgroup ) {
							if ( testgroup === group && name !== checkElement.name ) {
								cleanElement = v.validationTargetFor( v.clean( v.findByName( name ) ) );
								if ( cleanElement && cleanElement.name in v.invalid ) {
									v.currentElements.push( cleanElement );
									result = result && v.check( cleanElement );
								}
							}
						} );
					}

					rs = this.check( checkElement ) !== false;
					result = result && rs;
					if ( rs ) {
						this.invalid[ checkElement.name ] = false;
					} else {
						this.invalid[ checkElement.name ] = true;
					}

					if ( !this.numberOfInvalids() ) {

						// Hide error containers on last error
						this.toHide = this.toHide.add( this.containers );
					}
					this.showErrors();

					// Add aria-invalid status for screen readers
					$( element ).attr( "aria-invalid", !rs );
				}

				return result;
			},

			// http://jqueryvalidation.org/Validator.showErrors/
			showErrors: function( errors ) {
				if ( errors ) {
					var validator = this;

					// Add items to error list and map
					$.extend( this.errorMap, errors );
					this.errorList = $.map( this.errorMap, function( message, name ) {
						return {
							message: message,
							element: validator.findByName( name )[ 0 ]
						};
					} );

					// Remove items from success list
					this.successList = $.grep( this.successList, function( element ) {
						return !( element.name in errors );
					} );
				}
				if ( this.settings.showErrors ) {
					this.settings.showErrors.call( this, this.errorMap, this.errorList );
				} else {
					this.defaultShowErrors();
				}
			},

			// http://jqueryvalidation.org/Validator.resetForm/
			resetForm: function() {
				if ( $.fn.resetForm ) {
					$( this.currentForm ).resetForm();
				}
				this.invalid = {};
				this.submitted = {};
				this.prepareForm();
				this.hideErrors();
				var elements = this.elements()
					.removeData( "previousValue" )
					.removeAttr( "aria-invalid" );

				this.resetElements( elements );
			},

			resetElements: function( elements ) {
				var i;

				if ( this.settings.unhighlight ) {
					for ( i = 0; elements[ i ]; i++ ) {
						this.settings.unhighlight.call( this, elements[ i ],
							this.settings.errorClass, "" );
						this.findByName( elements[ i ].name ).removeClass( this.settings.validClass );
					}
				} else {
					elements
						.removeClass( this.settings.errorClass )
						.removeClass( this.settings.validClass );
				}
			},

			numberOfInvalids: function() {
				return this.objectLength( this.invalid );
			},

			objectLength: function( obj ) {
				/* jshint unused: false */
				var count = 0,
					i;
				for ( i in obj ) {
					if ( obj[ i ] ) {
						count++;
					}
				}
				return count;
			},

			hideErrors: function() {
				this.hideThese( this.toHide );
			},

			hideThese: function( errors ) {
				errors.not( this.containers ).text( "" );
				this.addWrapper( errors ).hide();
			},

			valid: function() {
				return this.size() === 0;
			},

			size: function() {
				return this.errorList.length;
			},

			focusInvalid: function() {
				if ( this.settings.focusInvalid ) {
					try {
						$( this.findLastActive() || this.errorList.length && this.errorList[ 0 ].element || [] )
						.filter( ":visible" )
						.focus()

						// Manually trigger focusin event; without it, focusin handler isn't called, findLastActive won't have anything to find
						.trigger( "focusin" );
					} catch ( e ) {

						// Ignore IE throwing errors when focusing hidden elements
					}
				}
			},

			findLastActive: function() {
				var lastActive = this.lastActive;
				return lastActive && $.grep( this.errorList, function( n ) {
					return n.element.name === lastActive.name;
				} ).length === 1 && lastActive;
			},

			elements: function() {
				var validator = this,
					rulesCache = {};

				// Select all valid inputs inside the form (no submit or reset buttons)
				return $( this.currentForm )
				.find( "input, select, textarea, [contenteditable]" )
				.not( ":submit, :reset, :image, :disabled" )
				.not( this.settings.ignore )
				.filter( function() {
					var name = this.name || $( this ).attr( "name" ); // For contenteditable
					if ( !name && validator.settings.debug && window.console ) {
						console.error( "%o has no name assigned", this );
					}

					// Set form expando on contenteditable
					if ( this.hasAttribute( "contenteditable" ) ) {
						this.form = $( this ).closest( "form" )[ 0 ];
					}

					// Select only the first element for each name, and only those with rules specified
					if ( name in rulesCache || !validator.objectLength( $( this ).rules() ) ) {
						return false;
					}

					rulesCache[ name ] = true;
					return true;
				} );
			},

			clean: function( selector ) {
				return $( selector )[ 0 ];
			},

			errors: function() {
				var errorClass = this.settings.errorClass.split( " " ).join( "." );
				return $( this.settings.errorElement + "." + errorClass, this.errorContext );
			},

			resetInternals: function() {
				this.successList = [];
				this.errorList = [];
				this.errorMap = {};
				this.toShow = $( [] );
				this.toHide = $( [] );
			},

			reset: function() {
				this.resetInternals();
				this.currentElements = $( [] );
			},

			prepareForm: function() {
				this.reset();
				this.toHide = this.errors().add( this.containers );
			},

			prepareElement: function( element ) {
				this.reset();
				this.toHide = this.errorsFor( element );
			},

			elementValue: function( element ) {
				var $element = $( element ),
					type = element.type,
					val, idx;

				if ( type === "radio" || type === "checkbox" ) {
					return this.findByName( element.name ).filter( ":checked" ).val();
				} else if ( type === "number" && typeof element.validity !== "undefined" ) {
					return element.validity.badInput ? "NaN" : $element.val();
				}

				if ( element.hasAttribute( "contenteditable" ) ) {
					val = $element.text();
				} else {
					val = $element.val();
				}

				if ( type === "file" ) {

					// Modern browser (chrome & safari)
					if ( val.substr( 0, 12 ) === "C:\\fakepath\\" ) {
						return val.substr( 12 );
					}

					// Legacy browsers
					// Unix-based path
					idx = val.lastIndexOf( "/" );
					if ( idx >= 0 ) {
						return val.substr( idx + 1 );
					}

					// Windows-based path
					idx = val.lastIndexOf( "\\" );
					if ( idx >= 0 ) {
						return val.substr( idx + 1 );
					}

					// Just the file name
					return val;
				}

				if ( typeof val === "string" ) {
					return val.replace( /\r/g, "" );
				}
				return val;
			},

			check: function( element ) {
				element = this.validationTargetFor( this.clean( element ) );

				var rules = $( element ).rules(),
					rulesCount = $.map( rules, function( n, i ) {
						return i;
					} ).length,
					dependencyMismatch = false,
					val = this.elementValue( element ),
					result, method, rule;

				// If a normalizer is defined for this element, then
				// call it to retreive the changed value instead
				// of using the real one.
				// Note that `this` in the normalizer is `element`.
				if ( typeof rules.normalizer === "function" ) {
					val = rules.normalizer.call( element, val );

					if ( typeof val !== "string" ) {
						throw new TypeError( "The normalizer should return a string value." );
					}

					// Delete the normalizer from rules to avoid treating
					// it as a pre-defined method.
					delete rules.normalizer;
				}

				for ( method in rules ) {
					rule = { method: method, parameters: rules[ method ] };
					try {
						result = $.validator.methods[ method ].call( this, val, element, rule.parameters );

						// If a method indicates that the field is optional and therefore valid,
						// don't mark it as valid when there are no other rules
						if ( result === "dependency-mismatch" && rulesCount === 1 ) {
							dependencyMismatch = true;
							continue;
						}
						dependencyMismatch = false;

						if ( result === "pending" ) {
							this.toHide = this.toHide.not( this.errorsFor( element ) );
							return;
						}

						if ( !result ) {
							this.formatAndAdd( element, rule );
							return false;
						}
					} catch ( e ) {
						if ( this.settings.debug && window.console ) {
							console.log( "Exception occurred when checking element " + element.id + ", check the '" + rule.method + "' method.", e );
						}
						if ( e instanceof TypeError ) {
							e.message += ".  Exception occurred when checking element " + element.id + ", check the '" + rule.method + "' method.";
						}

						throw e;
					}
				}
				if ( dependencyMismatch ) {
					return;
				}
				if ( this.objectLength( rules ) ) {
					this.successList.push( element );
				}
				return true;
			},

			// Return the custom message for the given element and validation method
			// specified in the element's HTML5 data attribute
			// return the generic message if present and no method specific message is present
			customDataMessage: function( element, method ) {
				return $( element ).data( "msg" + method.charAt( 0 ).toUpperCase() +
					method.substring( 1 ).toLowerCase() ) || $( element ).data( "msg" );
			},

			// Return the custom message for the given element name and validation method
			customMessage: function( name, method ) {
				var m = this.settings.messages[ name ];
				return m && ( m.constructor === String ? m : m[ method ] );
			},

			// Return the first defined argument, allowing empty strings
			findDefined: function() {
				for ( var i = 0; i < arguments.length; i++ ) {
					if ( arguments[ i ] !== undefined ) {
						return arguments[ i ];
					}
				}
				return undefined;
			},

			defaultMessage: function( element, rule ) {
				var message = this.findDefined(
						this.customMessage( element.name, rule.method ),
						this.customDataMessage( element, rule.method ),

						// 'title' is never undefined, so handle empty string as undefined
						!this.settings.ignoreTitle && element.title || undefined,
						$.validator.messages[ rule.method ],
						"<strong>Warning: No message defined for " + element.name + "</strong>"
					),
					theregex = /\$?\{(\d+)\}/g;
				if ( typeof message === "function" ) {
					message = message.call( this, rule.parameters, element );
				} else if ( theregex.test( message ) ) {
					message = $.validator.format( message.replace( theregex, "{$1}" ), rule.parameters );
				}

				return message;
			},

			formatAndAdd: function( element, rule ) {
				var message = this.defaultMessage( element, rule );

				this.errorList.push( {
					message: message,
					element: element,
					method: rule.method
				} );

				this.errorMap[ element.name ] = message;
				this.submitted[ element.name ] = message;
			},

			addWrapper: function( toToggle ) {
				if ( this.settings.wrapper ) {
					toToggle = toToggle.add( toToggle.parent( this.settings.wrapper ) );
				}
				return toToggle;
			},

			defaultShowErrors: function() {
				var i, elements, error;
				for ( i = 0; this.errorList[ i ]; i++ ) {
					error = this.errorList[ i ];
					if ( this.settings.highlight ) {
						this.settings.highlight.call( this, error.element, this.settings.errorClass, this.settings.validClass );
					}
					this.showLabel( error.element, error.message );
				}
				if ( this.errorList.length ) {
					this.toShow = this.toShow.add( this.containers );
				}
				if ( this.settings.success ) {
					for ( i = 0; this.successList[ i ]; i++ ) {
						this.showLabel( this.successList[ i ] );
					}
				}
				if ( this.settings.unhighlight ) {
					for ( i = 0, elements = this.validElements(); elements[ i ]; i++ ) {
						this.settings.unhighlight.call( this, elements[ i ], this.settings.errorClass, this.settings.validClass );
					}
				}
				this.toHide = this.toHide.not( this.toShow );
				this.hideErrors();
				this.addWrapper( this.toShow ).show();
			},

			validElements: function() {
				return this.currentElements.not( this.invalidElements() );
			},

			invalidElements: function() {
				return $( this.errorList ).map( function() {
					return this.element;
				} );
			},

			showLabel: function( element, message ) {
				var place, group, errorID, v,
					error = this.errorsFor( element ),
					elementID = this.idOrName( element ),
					describedBy = $( element ).attr( "aria-describedby" );

				if ( error.length ) {

					// Refresh error/success class
					error.removeClass( this.settings.validClass ).addClass( this.settings.errorClass );

					// Replace message on existing label
					error.html( message );
				} else {

					// Create error element
					error = $( "<" + this.settings.errorElement + ">" )
						.attr( "id", elementID + "-error" )
						.addClass( this.settings.errorClass )
						.html( message || "" );

					// Maintain reference to the element to be placed into the DOM
					place = error;
					if ( this.settings.wrapper ) {

						// Make sure the element is visible, even in IE
						// actually showing the wrapped element is handled elsewhere
						place = error.hide().show().wrap( "<" + this.settings.wrapper + "/>" ).parent();
					}
					if ( this.labelContainer.length ) {
						this.labelContainer.append( place );
					} else if ( this.settings.errorPlacement ) {
						this.settings.errorPlacement( place, $( element ) );
					} else {
						place.insertAfter( element );
					}

					// Link error back to the element
					if ( error.is( "label" ) ) {

						// If the error is a label, then associate using 'for'
						error.attr( "for", elementID );

						// If the element is not a child of an associated label, then it's necessary
						// to explicitly apply aria-describedby
					} else if ( error.parents( "label[for='" + this.escapeCssMeta( elementID ) + "']" ).length === 0 ) {
						errorID = error.attr( "id" );

						// Respect existing non-error aria-describedby
						if ( !describedBy ) {
							describedBy = errorID;
						} else if ( !describedBy.match( new RegExp( "\\b" + this.escapeCssMeta( errorID ) + "\\b" ) ) ) {

							// Add to end of list if not already present
							describedBy += " " + errorID;
						}
						$( element ).attr( "aria-describedby", describedBy );

						// If this element is grouped, then assign to all elements in the same group
						group = this.groups[ element.name ];
						if ( group ) {
							v = this;
							$.each( v.groups, function( name, testgroup ) {
								if ( testgroup === group ) {
									$( "[name='" + v.escapeCssMeta( name ) + "']", v.currentForm )
										.attr( "aria-describedby", error.attr( "id" ) );
								}
							} );
						}
					}
				}
				if ( !message && this.settings.success ) {
					error.text( "" );
					if ( typeof this.settings.success === "string" ) {
						error.addClass( this.settings.success );
					} else {
						this.settings.success( error, element );
					}
				}
				this.toShow = this.toShow.add( error );
			},

			errorsFor: function( element ) {
				var name = this.escapeCssMeta( this.idOrName( element ) ),
					describer = $( element ).attr( "aria-describedby" ),
					selector = "label[for='" + name + "'], label[for='" + name + "'] *";

				// 'aria-describedby' should directly reference the error element
				if ( describer ) {
					selector = selector + ", #" + this.escapeCssMeta( describer )
						.replace( /\s+/g, ", #" );
				}

				return this
					.errors()
					.filter( selector );
			},

			// See https://api.jquery.com/category/selectors/, for CSS
			// meta-characters that should be escaped in order to be used with JQuery
			// as a literal part of a name/id or any selector.
			escapeCssMeta: function( string ) {
				return string.replace( /([\\!"#$%&'()*+,./:;<=>?@\[\]^`{|}~])/g, "\\$1" );
			},

			idOrName: function( element ) {
				return this.groups[ element.name ] || ( this.checkable( element ) ? element.name : element.id || element.name );
			},

			validationTargetFor: function( element ) {

				// If radio/checkbox, validate first element in group instead
				if ( this.checkable( element ) ) {
					element = this.findByName( element.name );
				}

				// Always apply ignore filter
				return $( element ).not( this.settings.ignore )[ 0 ];
			},

			checkable: function( element ) {
				return ( /radio|checkbox/i ).test( element.type );
			},

			findByName: function( name ) {
				return $( this.currentForm ).find( "[name='" + this.escapeCssMeta( name ) + "']" );
			},

			getLength: function( value, element ) {
				switch ( element.nodeName.toLowerCase() ) {
				case "select":
					return $( "option:selected", element ).length;
				case "input":
					if ( this.checkable( element ) ) {
						return this.findByName( element.name ).filter( ":checked" ).length;
					}
				}
				return value.length;
			},

			depend: function( param, element ) {
				return this.dependTypes[ typeof param ] ? this.dependTypes[ typeof param ]( param, element ) : true;
			},

			dependTypes: {
				"boolean": function( param ) {
					return param;
				},
				"string": function( param, element ) {
					return !!$( param, element.form ).length;
				},
				"function": function( param, element ) {
					return param( element );
				}
			},

			optional: function( element ) {
				var val = this.elementValue( element );
				return !$.validator.methods.required.call( this, val, element ) && "dependency-mismatch";
			},

			startRequest: function( element ) {
				if ( !this.pending[ element.name ] ) {
					this.pendingRequest++;
					$( element ).addClass( this.settings.pendingClass );
					this.pending[ element.name ] = true;
				}
			},

			stopRequest: function( element, valid ) {
				this.pendingRequest--;

				// Sometimes synchronization fails, make sure pendingRequest is never < 0
				if ( this.pendingRequest < 0 ) {
					this.pendingRequest = 0;
				}
				delete this.pending[ element.name ];
				$( element ).removeClass( this.settings.pendingClass );
				if ( valid && this.pendingRequest === 0 && this.formSubmitted && this.form() ) {
					$( this.currentForm ).submit();
					this.formSubmitted = false;
				} else if ( !valid && this.pendingRequest === 0 && this.formSubmitted ) {
					$( this.currentForm ).triggerHandler( "invalid-form", [ this ] );
					this.formSubmitted = false;
				}
			},

			previousValue: function( element, method ) {
				return $.data( element, "previousValue" ) || $.data( element, "previousValue", {
					old: null,
					valid: true,
					message: this.defaultMessage( element, { method: method } )
				} );
			},

			// Cleans up all forms and elements, removes validator-specific events
			destroy: function() {
				this.resetForm();

				$( this.currentForm )
					.off( ".validate" )
					.removeData( "validator" )
					.find( ".validate-equalTo-blur" )
						.off( ".validate-equalTo" )
						.removeClass( "validate-equalTo-blur" );
			}

		},

		classRuleSettings: {
			required: { required: true },
			email: { email: true },
			url: { url: true },
			date: { date: true },
			dateISO: { dateISO: true },
			number: { number: true },
			digits: { digits: true },
			creditcard: { creditcard: true }
		},

		addClassRules: function( className, rules ) {
			if ( className.constructor === String ) {
				this.classRuleSettings[ className ] = rules;
			} else {
				$.extend( this.classRuleSettings, className );
			}
		},

		classRules: function( element ) {
			var rules = {},
				classes = $( element ).attr( "class" );

			if ( classes ) {
				$.each( classes.split( " " ), function() {
					if ( this in $.validator.classRuleSettings ) {
						$.extend( rules, $.validator.classRuleSettings[ this ] );
					}
				} );
			}
			return rules;
		},

		normalizeAttributeRule: function( rules, type, method, value ) {

			// Convert the value to a number for number inputs, and for text for backwards compability
			// allows type="date" and others to be compared as strings
			if ( /min|max|step/.test( method ) && ( type === null || /number|range|text/.test( type ) ) ) {
				value = Number( value );

				// Support Opera Mini, which returns NaN for undefined minlength
				if ( isNaN( value ) ) {
					value = undefined;
				}
			}

			if ( value || value === 0 ) {
				rules[ method ] = value;
			} else if ( type === method && type !== "range" ) {

				// Exception: the jquery validate 'range' method
				// does not test for the html5 'range' type
				rules[ method ] = true;
			}
		},

		attributeRules: function( element ) {
			var rules = {},
				$element = $( element ),
				type = element.getAttribute( "type" ),
				method, value;

			for ( method in $.validator.methods ) {

				// Support for <input required> in both html5 and older browsers
				if ( method === "required" ) {
					value = element.getAttribute( method );

					// Some browsers return an empty string for the required attribute
					// and non-HTML5 browsers might have required="" markup
					if ( value === "" ) {
						value = true;
					}

					// Force non-HTML5 browsers to return bool
					value = !!value;
				} else {
					value = $element.attr( method );
				}

				this.normalizeAttributeRule( rules, type, method, value );
			}

			// 'maxlength' may be returned as -1, 2147483647 ( IE ) and 524288 ( safari ) for text inputs
			if ( rules.maxlength && /-1|2147483647|524288/.test( rules.maxlength ) ) {
				delete rules.maxlength;
			}

			return rules;
		},

		dataRules: function( element ) {
			var rules = {},
				$element = $( element ),
				type = element.getAttribute( "type" ),
				method, value;

			for ( method in $.validator.methods ) {
				value = $element.data( "rule" + method.charAt( 0 ).toUpperCase() + method.substring( 1 ).toLowerCase() );
				this.normalizeAttributeRule( rules, type, method, value );
			}
			return rules;
		},

		staticRules: function( element ) {
			var rules = {},
				validator = $.data( element.form, "validator" );

			if ( validator.settings.rules ) {
				rules = $.validator.normalizeRule( validator.settings.rules[ element.name ] ) || {};
			}
			return rules;
		},

		normalizeRules: function( rules, element ) {

			// Handle dependency check
			$.each( rules, function( prop, val ) {

				// Ignore rule when param is explicitly false, eg. required:false
				if ( val === false ) {
					delete rules[ prop ];
					return;
				}
				if ( val.param || val.depends ) {
					var keepRule = true;
					switch ( typeof val.depends ) {
					case "string":
						keepRule = !!$( val.depends, element.form ).length;
						break;
					case "function":
						keepRule = val.depends.call( element, element );
						break;
					}
					if ( keepRule ) {
						rules[ prop ] = val.param !== undefined ? val.param : true;
					} else {
						$.data( element.form, "validator" ).resetElements( $( element ) );
						delete rules[ prop ];
					}
				}
			} );

			// Evaluate parameters
			$.each( rules, function( rule, parameter ) {
				rules[ rule ] = $.isFunction( parameter ) && rule !== "normalizer" ? parameter( element ) : parameter;
			} );

			// Clean number parameters
			$.each( [ "minlength", "maxlength" ], function() {
				if ( rules[ this ] ) {
					rules[ this ] = Number( rules[ this ] );
				}
			} );
			$.each( [ "rangelength", "range" ], function() {
				var parts;
				if ( rules[ this ] ) {
					if ( $.isArray( rules[ this ] ) ) {
						rules[ this ] = [ Number( rules[ this ][ 0 ] ), Number( rules[ this ][ 1 ] ) ];
					} else if ( typeof rules[ this ] === "string" ) {
						parts = rules[ this ].replace( /[\[\]]/g, "" ).split( /[\s,]+/ );
						rules[ this ] = [ Number( parts[ 0 ] ), Number( parts[ 1 ] ) ];
					}
				}
			} );

			if ( $.validator.autoCreateRanges ) {

				// Auto-create ranges
				if ( rules.min != null && rules.max != null ) {
					rules.range = [ rules.min, rules.max ];
					delete rules.min;
					delete rules.max;
				}
				if ( rules.minlength != null && rules.maxlength != null ) {
					rules.rangelength = [ rules.minlength, rules.maxlength ];
					delete rules.minlength;
					delete rules.maxlength;
				}
			}

			return rules;
		},

		// Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
		normalizeRule: function( data ) {
			if ( typeof data === "string" ) {
				var transformed = {};
				$.each( data.split( /\s/ ), function() {
					transformed[ this ] = true;
				} );
				data = transformed;
			}
			return data;
		},

		// http://jqueryvalidation.org/jQuery.validator.addMethod/
		addMethod: function( name, method, message ) {
			$.validator.methods[ name ] = method;
			$.validator.messages[ name ] = message !== undefined ? message : $.validator.messages[ name ];
			if ( method.length < 3 ) {
				$.validator.addClassRules( name, $.validator.normalizeRule( name ) );
			}
		},

		// http://jqueryvalidation.org/jQuery.validator.methods/
		methods: {

			// http://jqueryvalidation.org/required-method/
			required: function( value, element, param ) {

				// Check if dependency is met
				if ( !this.depend( param, element ) ) {
					return "dependency-mismatch";
				}
				if ( element.nodeName.toLowerCase() === "select" ) {

					// Could be an array for select-multiple or a string, both are fine this way
					var val = $( element ).val();
					return val && val.length > 0;
				}
				if ( this.checkable( element ) ) {
					return this.getLength( value, element ) > 0;
				}
				return value.length > 0;
			},

			// http://jqueryvalidation.org/email-method/
			email: function( value, element ) {

				// From https://html.spec.whatwg.org/multipage/forms.html#valid-e-mail-address
				// Retrieved 2014-01-14
				// If you have a problem with this implementation, report a bug against the above spec
				// Or use custom methods to implement your own email validation
				return this.optional( element ) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test( value );
			},

			// http://jqueryvalidation.org/url-method/
			url: function( value, element ) {

				// Copyright (c) 2010-2013 Diego Perini, MIT licensed
				// https://gist.github.com/dperini/729294
				// see also https://mathiasbynens.be/demo/url-regex
				// modified to allow protocol-relative URLs
				return this.optional( element ) || /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test( value );
			},

			// http://jqueryvalidation.org/date-method/
			date: function( value, element ) {
				return this.optional( element ) || !/Invalid|NaN/.test( new Date( value ).toString() );
			},

			// http://jqueryvalidation.org/dateISO-method/
			dateISO: function( value, element ) {
				return this.optional( element ) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test( value );
			},

			// http://jqueryvalidation.org/number-method/
			number: function( value, element ) {
				return this.optional( element ) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test( value );
			},

			// http://jqueryvalidation.org/digits-method/
			digits: function( value, element ) {
				return this.optional( element ) || /^\d+$/.test( value );
			},

			// http://jqueryvalidation.org/minlength-method/
			minlength: function( value, element, param ) {
				var length = $.isArray( value ) ? value.length : this.getLength( value, element );
				return this.optional( element ) || length >= param;
			},

			// http://jqueryvalidation.org/maxlength-method/
			maxlength: function( value, element, param ) {
				var length = $.isArray( value ) ? value.length : this.getLength( value, element );
				return this.optional( element ) || length <= param;
			},

			// http://jqueryvalidation.org/rangelength-method/
			rangelength: function( value, element, param ) {
				var length = $.isArray( value ) ? value.length : this.getLength( value, element );
				return this.optional( element ) || ( length >= param[ 0 ] && length <= param[ 1 ] );
			},

			// http://jqueryvalidation.org/min-method/
			min: function( value, element, param ) {
				return this.optional( element ) || value >= param;
			},

			// http://jqueryvalidation.org/max-method/
			max: function( value, element, param ) {
				return this.optional( element ) || value <= param;
			},

			// http://jqueryvalidation.org/range-method/
			range: function( value, element, param ) {
				return this.optional( element ) || ( value >= param[ 0 ] && value <= param[ 1 ] );
			},

			// http://jqueryvalidation.org/step-method/
			step: function( value, element, param ) {
				var type = $( element ).attr( "type" ),
					errorMessage = "Step attribute on input type " + type + " is not supported.",
					supportedTypes = [ "text", "number", "range" ],
					re = new RegExp( "\\b" + type + "\\b" ),
					notSupported = type && !re.test( supportedTypes.join() );

				// Works only for text, number and range input types
				// TODO find a way to support input types date, datetime, datetime-local, month, time and week
				if ( notSupported ) {
					throw new Error( errorMessage );
				}
				return this.optional( element ) || ( value % param === 0 );
			},

			// http://jqueryvalidation.org/equalTo-method/
			equalTo: function( value, element, param ) {

				// Bind to the blur event of the target in order to revalidate whenever the target field is updated
				var target = $( param );
				if ( this.settings.onfocusout && target.not( ".validate-equalTo-blur" ).length ) {
					target.addClass( "validate-equalTo-blur" ).on( "blur.validate-equalTo", function() {
						$( element ).valid();
					} );
				}
				return value === target.val();
			},

			// http://jqueryvalidation.org/remote-method/
			remote: function( value, element, param, method ) {
				if ( this.optional( element ) ) {
					return "dependency-mismatch";
				}

				method = typeof method === "string" && method || "remote";

				var previous = this.previousValue( element, method ),
					validator, data, optionDataString;

				if ( !this.settings.messages[ element.name ] ) {
					this.settings.messages[ element.name ] = {};
				}
				previous.originalMessage = previous.originalMessage || this.settings.messages[ element.name ][ method ];
				this.settings.messages[ element.name ][ method ] = previous.message;

				param = typeof param === "string" && { url: param } || param;
				optionDataString = $.param( $.extend( { data: value }, param.data ) );
				if ( previous.old === optionDataString ) {
					return previous.valid;
				}

				previous.old = optionDataString;
				validator = this;
				this.startRequest( element );
				data = {};
				data[ element.name ] = value;
				$.ajax( $.extend( true, {
					mode: "abort",
					port: "validate" + element.name,
					dataType: "json",
					data: data,
					context: validator.currentForm,
					success: function( response ) {
						var valid = response === true || response === "true",
							errors, message, submitted;

						validator.settings.messages[ element.name ][ method ] = previous.originalMessage;
						if ( valid ) {
							submitted = validator.formSubmitted;
							validator.resetInternals();
							validator.toHide = validator.errorsFor( element );
							validator.formSubmitted = submitted;
							validator.successList.push( element );
							validator.invalid[ element.name ] = false;
							validator.showErrors();
						} else {
							errors = {};
							message = response || validator.defaultMessage( element, { method: method, parameters: value } );
							errors[ element.name ] = previous.message = message;
							validator.invalid[ element.name ] = true;
							validator.showErrors( errors );
						}
						previous.valid = valid;
						validator.stopRequest( element, valid );
					}
				}, param ) );
				return "pending";
			}
		}

	} );

	// Ajax mode: abort
	// usage: $.ajax({ mode: "abort"[, port: "uniqueport"]});
	// if mode:"abort" is used, the previous request on that port (port can be undefined) is aborted via XMLHttpRequest.abort()

	var pendingRequests = {},
		ajax;

	// Use a prefilter if available (1.5+)
	if ( $.ajaxPrefilter ) {
		$.ajaxPrefilter( function( settings, _, xhr ) {
			var port = settings.port;
			if ( settings.mode === "abort" ) {
				if ( pendingRequests[ port ] ) {
					pendingRequests[ port ].abort();
				}
				pendingRequests[ port ] = xhr;
			}
		} );
	} else {

		// Proxy ajax
		ajax = $.ajax;
		$.ajax = function( settings ) {
			var mode = ( "mode" in settings ? settings : $.ajaxSettings ).mode,
				port = ( "port" in settings ? settings : $.ajaxSettings ).port;
			if ( mode === "abort" ) {
				if ( pendingRequests[ port ] ) {
					pendingRequests[ port ].abort();
				}
				pendingRequests[ port ] = ajax.apply( this, arguments );
				return pendingRequests[ port ];
			}
			return ajax.apply( this, arguments );
		};
	}

	}));

/***/ },
/* 85 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	exports.default = function () {
	  var _ref = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

	  var client = _ref.client;
	  var key = _ref.key;
	  var language = _ref.language;
	  var _ref$libraries = _ref.libraries;
	  var libraries = _ref$libraries === undefined ? [] : _ref$libraries;
	  var _ref$timeout = _ref.timeout;
	  var timeout = _ref$timeout === undefined ? 10000 : _ref$timeout;
	  var v = _ref.v;

	  var callbackName = '__googleMapsApiOnLoadCallback';

	  return new Promise(function (resolve, reject) {

	    // Exit if not running inside a browser.
	    if (typeof window === 'undefined') {
	      return reject(new Error('Can only load the Google Maps API in the browser'));
	    }

	    // Prepare the `script` tag to be inserted into the page.
	    var scriptElement = document.createElement('script');
	    var params = ['callback=' + callbackName];
	    if (client) params.push('client=' + client);
	    if (key) params.push('key=' + key);
	    if (language) params.push('language=' + language);
	    libraries = [].concat(libraries); // Ensure that `libraries` is an array
	    if (libraries.length) params.push('libraries=' + libraries.join(','));
	    if (v) params.push('v=' + v);
	    scriptElement.src = 'https://maps.googleapis.com/maps/api/js?' + params.join('&');

	    // Timeout if necessary.
	    var timeoutId = null;
	    if (timeout) {
	      timeoutId = setTimeout(function () {
	        window[callbackName] = function () {}; // Set the on load callback to a no-op.
	        reject(new Error('Could not load the Google Maps API'));
	      }, timeout);
	    }

	    // Hook up the on load callback.
	    window[callbackName] = function () {
	      if (timeoutId !== null) {
	        clearTimeout(timeoutId);
	      }
	      resolve(window.google.maps);
	      delete window[callbackName];
	    };

	    // Insert the `script` tag.
	    document.body.appendChild(scriptElement);
	  });
	};

/***/ },
/* 86 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {/*!
	* Parsley.js
	* Version 2.3.13 - built Tue, May 31st 2016, 8:55 am
	* http://parsleyjs.org
	* Guillaume Potier - <guillaume@wisembly.com>
	* Marc-Andre Lafortune - <petroselinum@marc-andre.ca>
	* MIT Licensed
	*/

	// The source code below is generated by babel as
	// Parsley is written in ECMAScript 6
	//
	var _slice = Array.prototype.slice;

	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

	(function (global, factory) {
	   true ? module.exports = factory(__webpack_require__(1)) : typeof define === 'function' && define.amd ? define(['jquery'], factory) : global.parsley = factory(global.jQuery);
	})(this, function ($) {
	  'use strict';

	  var globalID = 1;
	  var pastWarnings = {};

	  var ParsleyUtils__ParsleyUtils = {
	    // Parsley DOM-API
	    // returns object from dom attributes and values
	    attr: function attr($element, namespace, obj) {
	      var i;
	      var attribute;
	      var attributes;
	      var regex = new RegExp('^' + namespace, 'i');

	      if ('undefined' === typeof obj) obj = {};else {
	        // Clear all own properties. This won't affect prototype's values
	        for (i in obj) {
	          if (obj.hasOwnProperty(i)) delete obj[i];
	        }
	      }

	      if ('undefined' === typeof $element || 'undefined' === typeof $element[0]) return obj;

	      attributes = $element[0].attributes;
	      for (i = attributes.length; i--;) {
	        attribute = attributes[i];

	        if (attribute && attribute.specified && regex.test(attribute.name)) {
	          obj[this.camelize(attribute.name.slice(namespace.length))] = this.deserializeValue(attribute.value);
	        }
	      }

	      return obj;
	    },

	    checkAttr: function checkAttr($element, namespace, _checkAttr) {
	      return $element.is('[' + namespace + _checkAttr + ']');
	    },

	    setAttr: function setAttr($element, namespace, attr, value) {
	      $element[0].setAttribute(this.dasherize(namespace + attr), String(value));
	    },

	    generateID: function generateID() {
	      return '' + globalID++;
	    },

	    /** Third party functions **/
	    // Zepto deserialize function
	    deserializeValue: function deserializeValue(value) {
	      var num;

	      try {
	        return value ? value == "true" || (value == "false" ? false : value == "null" ? null : !isNaN(num = Number(value)) ? num : /^[\[\{]/.test(value) ? $.parseJSON(value) : value) : value;
	      } catch (e) {
	        return value;
	      }
	    },

	    // Zepto camelize function
	    camelize: function camelize(str) {
	      return str.replace(/-+(.)?/g, function (match, chr) {
	        return chr ? chr.toUpperCase() : '';
	      });
	    },

	    // Zepto dasherize function
	    dasherize: function dasherize(str) {
	      return str.replace(/::/g, '/').replace(/([A-Z]+)([A-Z][a-z])/g, '$1_$2').replace(/([a-z\d])([A-Z])/g, '$1_$2').replace(/_/g, '-').toLowerCase();
	    },

	    warn: function warn() {
	      var _window$console;

	      if (window.console && 'function' === typeof window.console.warn) (_window$console = window.console).warn.apply(_window$console, arguments);
	    },

	    warnOnce: function warnOnce(msg) {
	      if (!pastWarnings[msg]) {
	        pastWarnings[msg] = true;
	        this.warn.apply(this, arguments);
	      }
	    },

	    _resetWarnings: function _resetWarnings() {
	      pastWarnings = {};
	    },

	    trimString: function trimString(string) {
	      return string.replace(/^\s+|\s+$/g, '');
	    },

	    namespaceEvents: function namespaceEvents(events, namespace) {
	      events = this.trimString(events || '').split(/\s+/);
	      if (!events[0]) return '';
	      return $.map(events, function (evt) {
	        return evt + '.' + namespace;
	      }).join(' ');
	    },

	    // Object.create polyfill, see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create#Polyfill
	    objectCreate: Object.create || (function () {
	      var Object = function Object() {};
	      return function (prototype) {
	        if (arguments.length > 1) {
	          throw Error('Second argument not supported');
	        }
	        if (typeof prototype != 'object') {
	          throw TypeError('Argument must be an object');
	        }
	        Object.prototype = prototype;
	        var result = new Object();
	        Object.prototype = null;
	        return result;
	      };
	    })()
	  };

	  var ParsleyUtils__default = ParsleyUtils__ParsleyUtils;

	  // All these options could be overriden and specified directly in DOM using
	  // `data-parsley-` default DOM-API
	  // eg: `inputs` can be set in DOM using `data-parsley-inputs="input, textarea"`
	  // eg: `data-parsley-stop-on-first-failing-constraint="false"`

	  var ParsleyDefaults = {
	    // ### General

	    // Default data-namespace for DOM API
	    namespace: 'data-parsley-',

	    // Supported inputs by default
	    inputs: 'input, textarea, select',

	    // Excluded inputs by default
	    excluded: 'input[type=button], input[type=submit], input[type=reset], input[type=hidden]',

	    // Stop validating field on highest priority failing constraint
	    priorityEnabled: true,

	    // ### Field only

	    // identifier used to group together inputs (e.g. radio buttons...)
	    multiple: null,

	    // identifier (or array of identifiers) used to validate only a select group of inputs
	    group: null,

	    // ### UI
	    // Enable\Disable error messages
	    uiEnabled: true,

	    // Key events threshold before validation
	    validationThreshold: 3,

	    // Focused field on form validation error. 'first'|'last'|'none'
	    focus: 'first',

	    // event(s) that will trigger validation before first failure. eg: `input`...
	    trigger: false,

	    // event(s) that will trigger validation after first failure.
	    triggerAfterFailure: 'input',

	    // Class that would be added on every failing validation Parsley field
	    errorClass: 'parsley-error',

	    // Same for success validation
	    successClass: 'parsley-success',

	    // Return the `$element` that will receive these above success or error classes
	    // Could also be (and given directly from DOM) a valid selector like `'#div'`
	    classHandler: function classHandler(ParsleyField) {},

	    // Return the `$element` where errors will be appended
	    // Could also be (and given directly from DOM) a valid selector like `'#div'`
	    errorsContainer: function errorsContainer(ParsleyField) {},

	    // ul elem that would receive errors' list
	    errorsWrapper: '<ul class="parsley-errors-list"></ul>',

	    // li elem that would receive error message
	    errorTemplate: '<li></li>'
	  };

	  var ParsleyAbstract = function ParsleyAbstract() {
	    this.__id__ = ParsleyUtils__default.generateID();
	  };

	  ParsleyAbstract.prototype = {
	    asyncSupport: true, // Deprecated

	    _pipeAccordingToValidationResult: function _pipeAccordingToValidationResult() {
	      var _this = this;

	      var pipe = function pipe() {
	        var r = $.Deferred();
	        if (true !== _this.validationResult) r.reject();
	        return r.resolve().promise();
	      };
	      return [pipe, pipe];
	    },

	    actualizeOptions: function actualizeOptions() {
	      ParsleyUtils__default.attr(this.$element, this.options.namespace, this.domOptions);
	      if (this.parent && this.parent.actualizeOptions) this.parent.actualizeOptions();
	      return this;
	    },

	    _resetOptions: function _resetOptions(initOptions) {
	      this.domOptions = ParsleyUtils__default.objectCreate(this.parent.options);
	      this.options = ParsleyUtils__default.objectCreate(this.domOptions);
	      // Shallow copy of ownProperties of initOptions:
	      for (var i in initOptions) {
	        if (initOptions.hasOwnProperty(i)) this.options[i] = initOptions[i];
	      }
	      this.actualizeOptions();
	    },

	    _listeners: null,

	    // Register a callback for the given event name
	    // Callback is called with context as the first argument and the `this`
	    // The context is the current parsley instance, or window.Parsley if global
	    // A return value of `false` will interrupt the calls
	    on: function on(name, fn) {
	      this._listeners = this._listeners || {};
	      var queue = this._listeners[name] = this._listeners[name] || [];
	      queue.push(fn);

	      return this;
	    },

	    // Deprecated. Use `on` instead
	    subscribe: function subscribe(name, fn) {
	      $.listenTo(this, name.toLowerCase(), fn);
	    },

	    // Unregister a callback (or all if none is given) for the given event name
	    off: function off(name, fn) {
	      var queue = this._listeners && this._listeners[name];
	      if (queue) {
	        if (!fn) {
	          delete this._listeners[name];
	        } else {
	          for (var i = queue.length; i--;) if (queue[i] === fn) queue.splice(i, 1);
	        }
	      }
	      return this;
	    },

	    // Deprecated. Use `off`
	    unsubscribe: function unsubscribe(name, fn) {
	      $.unsubscribeTo(this, name.toLowerCase());
	    },

	    // Trigger an event of the given name
	    // A return value of `false` interrupts the callback chain
	    // Returns false if execution was interrupted
	    trigger: function trigger(name, target, extraArg) {
	      target = target || this;
	      var queue = this._listeners && this._listeners[name];
	      var result;
	      var parentResult;
	      if (queue) {
	        for (var i = queue.length; i--;) {
	          result = queue[i].call(target, target, extraArg);
	          if (result === false) return result;
	        }
	      }
	      if (this.parent) {
	        return this.parent.trigger(name, target, extraArg);
	      }
	      return true;
	    },

	    // Reset UI
	    reset: function reset() {
	      // Field case: just emit a reset event for UI
	      if ('ParsleyForm' !== this.__class__) {
	        this._resetUI();
	        return this._trigger('reset');
	      }

	      // Form case: emit a reset event for each field
	      for (var i = 0; i < this.fields.length; i++) this.fields[i].reset();

	      this._trigger('reset');
	    },

	    // Destroy Parsley instance (+ UI)
	    destroy: function destroy() {
	      // Field case: emit destroy event to clean UI and then destroy stored instance
	      this._destroyUI();
	      if ('ParsleyForm' !== this.__class__) {
	        this.$element.removeData('Parsley');
	        this.$element.removeData('ParsleyFieldMultiple');
	        this._trigger('destroy');

	        return;
	      }

	      // Form case: destroy all its fields and then destroy stored instance
	      for (var i = 0; i < this.fields.length; i++) this.fields[i].destroy();

	      this.$element.removeData('Parsley');
	      this._trigger('destroy');
	    },

	    asyncIsValid: function asyncIsValid(group, force) {
	      ParsleyUtils__default.warnOnce("asyncIsValid is deprecated; please use whenValid instead");
	      return this.whenValid({ group: group, force: force });
	    },

	    _findRelated: function _findRelated() {
	      return this.options.multiple ? this.parent.$element.find('[' + this.options.namespace + 'multiple="' + this.options.multiple + '"]') : this.$element;
	    }
	  };

	  var requirementConverters = {
	    string: function string(_string) {
	      return _string;
	    },
	    integer: function integer(string) {
	      if (isNaN(string)) throw 'Requirement is not an integer: "' + string + '"';
	      return parseInt(string, 10);
	    },
	    number: function number(string) {
	      if (isNaN(string)) throw 'Requirement is not a number: "' + string + '"';
	      return parseFloat(string);
	    },
	    reference: function reference(string) {
	      // Unused for now
	      var result = $(string);
	      if (result.length === 0) throw 'No such reference: "' + string + '"';
	      return result;
	    },
	    boolean: function boolean(string) {
	      return string !== 'false';
	    },
	    object: function object(string) {
	      return ParsleyUtils__default.deserializeValue(string);
	    },
	    regexp: function regexp(_regexp) {
	      var flags = '';

	      // Test if RegExp is literal, if not, nothing to be done, otherwise, we need to isolate flags and pattern
	      if (/^\/.*\/(?:[gimy]*)$/.test(_regexp)) {
	        // Replace the regexp literal string with the first match group: ([gimy]*)
	        // If no flag is present, this will be a blank string
	        flags = _regexp.replace(/.*\/([gimy]*)$/, '$1');
	        // Again, replace the regexp literal string with the first match group:
	        // everything excluding the opening and closing slashes and the flags
	        _regexp = _regexp.replace(new RegExp('^/(.*?)/' + flags + '$'), '$1');
	      } else {
	        // Anchor regexp:
	        _regexp = '^' + _regexp + '$';
	      }
	      return new RegExp(_regexp, flags);
	    }
	  };

	  var convertArrayRequirement = function convertArrayRequirement(string, length) {
	    var m = string.match(/^\s*\[(.*)\]\s*$/);
	    if (!m) throw 'Requirement is not an array: "' + string + '"';
	    var values = m[1].split(',').map(ParsleyUtils__default.trimString);
	    if (values.length !== length) throw 'Requirement has ' + values.length + ' values when ' + length + ' are needed';
	    return values;
	  };

	  var convertRequirement = function convertRequirement(requirementType, string) {
	    var converter = requirementConverters[requirementType || 'string'];
	    if (!converter) throw 'Unknown requirement specification: "' + requirementType + '"';
	    return converter(string);
	  };

	  var convertExtraOptionRequirement = function convertExtraOptionRequirement(requirementSpec, string, extraOptionReader) {
	    var main = null;
	    var extra = {};
	    for (var key in requirementSpec) {
	      if (key) {
	        var value = extraOptionReader(key);
	        if ('string' === typeof value) value = convertRequirement(requirementSpec[key], value);
	        extra[key] = value;
	      } else {
	        main = convertRequirement(requirementSpec[key], string);
	      }
	    }
	    return [main, extra];
	  };

	  // A Validator needs to implement the methods `validate` and `parseRequirements`

	  var ParsleyValidator = function ParsleyValidator(spec) {
	    $.extend(true, this, spec);
	  };

	  ParsleyValidator.prototype = {
	    // Returns `true` iff the given `value` is valid according the given requirements.
	    validate: function validate(value, requirementFirstArg) {
	      if (this.fn) {
	        // Legacy style validator

	        if (arguments.length > 3) // If more args then value, requirement, instance...
	          requirementFirstArg = [].slice.call(arguments, 1, -1); // Skip first arg (value) and last (instance), combining the rest
	        return this.fn.call(this, value, requirementFirstArg);
	      }

	      if ($.isArray(value)) {
	        if (!this.validateMultiple) throw 'Validator `' + this.name + '` does not handle multiple values';
	        return this.validateMultiple.apply(this, arguments);
	      } else {
	        if (this.validateNumber) {
	          if (isNaN(value)) return false;
	          arguments[0] = parseFloat(arguments[0]);
	          return this.validateNumber.apply(this, arguments);
	        }
	        if (this.validateString) {
	          return this.validateString.apply(this, arguments);
	        }
	        throw 'Validator `' + this.name + '` only handles multiple values';
	      }
	    },

	    // Parses `requirements` into an array of arguments,
	    // according to `this.requirementType`
	    parseRequirements: function parseRequirements(requirements, extraOptionReader) {
	      if ('string' !== typeof requirements) {
	        // Assume requirement already parsed
	        // but make sure we return an array
	        return $.isArray(requirements) ? requirements : [requirements];
	      }
	      var type = this.requirementType;
	      if ($.isArray(type)) {
	        var values = convertArrayRequirement(requirements, type.length);
	        for (var i = 0; i < values.length; i++) values[i] = convertRequirement(type[i], values[i]);
	        return values;
	      } else if ($.isPlainObject(type)) {
	        return convertExtraOptionRequirement(type, requirements, extraOptionReader);
	      } else {
	        return [convertRequirement(type, requirements)];
	      }
	    },
	    // Defaults:
	    requirementType: 'string',

	    priority: 2

	  };

	  var ParsleyValidatorRegistry = function ParsleyValidatorRegistry(validators, catalog) {
	    this.__class__ = 'ParsleyValidatorRegistry';

	    // Default Parsley locale is en
	    this.locale = 'en';

	    this.init(validators || {}, catalog || {});
	  };

	  var typeRegexes = {
	    email: /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i,

	    // Follow https://www.w3.org/TR/html5/infrastructure.html#floating-point-numbers
	    number: /^-?(\d*\.)?\d+(e[-+]?\d+)?$/i,

	    integer: /^-?\d+$/,

	    digits: /^\d+$/,

	    alphanum: /^\w+$/i,

	    url: new RegExp("^" +
	    // protocol identifier
	    "(?:(?:https?|ftp)://)?" + // ** mod: make scheme optional
	    // user:pass authentication
	    "(?:\\S+(?::\\S*)?@)?" + "(?:" +
	    // IP address exclusion
	    // private & local networks
	    // "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +   // ** mod: allow local networks
	    // "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +  // ** mod: allow local networks
	    // "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +  // ** mod: allow local networks
	    // IP address dotted notation octets
	    // excludes loopback network 0.0.0.0
	    // excludes reserved space >= 224.0.0.0
	    // excludes network & broacast addresses
	    // (first & last IP address of each class)
	    "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" + "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" + "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" + "|" +
	    // host name
	    '(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)' +
	    // domain name
	    '(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*' +
	    // TLD identifier
	    '(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))' + ")" +
	    // port number
	    "(?::\\d{2,5})?" +
	    // resource path
	    "(?:/\\S*)?" + "$", 'i')
	  };
	  typeRegexes.range = typeRegexes.number;

	  // See http://stackoverflow.com/a/10454560/8279
	  var decimalPlaces = function decimalPlaces(num) {
	    var match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
	    if (!match) {
	      return 0;
	    }
	    return Math.max(0,
	    // Number of digits right of decimal point.
	    (match[1] ? match[1].length : 0) - (
	    // Adjust for scientific notation.
	    match[2] ? +match[2] : 0));
	  };

	  ParsleyValidatorRegistry.prototype = {
	    init: function init(validators, catalog) {
	      this.catalog = catalog;
	      // Copy prototype's validators:
	      this.validators = $.extend({}, this.validators);

	      for (var name in validators) this.addValidator(name, validators[name].fn, validators[name].priority);

	      window.Parsley.trigger('parsley:validator:init');
	    },

	    // Set new messages locale if we have dictionary loaded in ParsleyConfig.i18n
	    setLocale: function setLocale(locale) {
	      if ('undefined' === typeof this.catalog[locale]) throw new Error(locale + ' is not available in the catalog');

	      this.locale = locale;

	      return this;
	    },

	    // Add a new messages catalog for a given locale. Set locale for this catalog if set === `true`
	    addCatalog: function addCatalog(locale, messages, set) {
	      if ('object' === typeof messages) this.catalog[locale] = messages;

	      if (true === set) return this.setLocale(locale);

	      return this;
	    },

	    // Add a specific message for a given constraint in a given locale
	    addMessage: function addMessage(locale, name, message) {
	      if ('undefined' === typeof this.catalog[locale]) this.catalog[locale] = {};

	      this.catalog[locale][name] = message;

	      return this;
	    },

	    // Add messages for a given locale
	    addMessages: function addMessages(locale, nameMessageObject) {
	      for (var name in nameMessageObject) this.addMessage(locale, name, nameMessageObject[name]);

	      return this;
	    },

	    // Add a new validator
	    //
	    //    addValidator('custom', {
	    //        requirementType: ['integer', 'integer'],
	    //        validateString: function(value, from, to) {},
	    //        priority: 22,
	    //        messages: {
	    //          en: "Hey, that's no good",
	    //          fr: "Aye aye, pas bon du tout",
	    //        }
	    //    })
	    //
	    // Old API was addValidator(name, function, priority)
	    //
	    addValidator: function addValidator(name, arg1, arg2) {
	      if (this.validators[name]) ParsleyUtils__default.warn('Validator "' + name + '" is already defined.');else if (ParsleyDefaults.hasOwnProperty(name)) {
	        ParsleyUtils__default.warn('"' + name + '" is a restricted keyword and is not a valid validator name.');
	        return;
	      }
	      return this._setValidator.apply(this, arguments);
	    },

	    updateValidator: function updateValidator(name, arg1, arg2) {
	      if (!this.validators[name]) {
	        ParsleyUtils__default.warn('Validator "' + name + '" is not already defined.');
	        return this.addValidator.apply(this, arguments);
	      }
	      return this._setValidator.apply(this, arguments);
	    },

	    removeValidator: function removeValidator(name) {
	      if (!this.validators[name]) ParsleyUtils__default.warn('Validator "' + name + '" is not defined.');

	      delete this.validators[name];

	      return this;
	    },

	    _setValidator: function _setValidator(name, validator, priority) {
	      if ('object' !== typeof validator) {
	        // Old style validator, with `fn` and `priority`
	        validator = {
	          fn: validator,
	          priority: priority
	        };
	      }
	      if (!validator.validate) {
	        validator = new ParsleyValidator(validator);
	      }
	      this.validators[name] = validator;

	      for (var locale in validator.messages || {}) this.addMessage(locale, name, validator.messages[locale]);

	      return this;
	    },

	    getErrorMessage: function getErrorMessage(constraint) {
	      var message;

	      // Type constraints are a bit different, we have to match their requirements too to find right error message
	      if ('type' === constraint.name) {
	        var typeMessages = this.catalog[this.locale][constraint.name] || {};
	        message = typeMessages[constraint.requirements];
	      } else message = this.formatMessage(this.catalog[this.locale][constraint.name], constraint.requirements);

	      return message || this.catalog[this.locale].defaultMessage || this.catalog.en.defaultMessage;
	    },

	    // Kind of light `sprintf()` implementation
	    formatMessage: function formatMessage(string, parameters) {
	      if ('object' === typeof parameters) {
	        for (var i in parameters) string = this.formatMessage(string, parameters[i]);

	        return string;
	      }

	      return 'string' === typeof string ? string.replace(/%s/i, parameters) : '';
	    },

	    // Here is the Parsley default validators list.
	    // A validator is an object with the following key values:
	    //  - priority: an integer
	    //  - requirement: 'string' (default), 'integer', 'number', 'regexp' or an Array of these
	    //  - validateString, validateMultiple, validateNumber: functions returning `true`, `false` or a promise
	    // Alternatively, a validator can be a function that returns such an object
	    //
	    validators: {
	      notblank: {
	        validateString: function validateString(value) {
	          return (/\S/.test(value)
	          );
	        },
	        priority: 2
	      },
	      required: {
	        validateMultiple: function validateMultiple(values) {
	          return values.length > 0;
	        },
	        validateString: function validateString(value) {
	          return (/\S/.test(value)
	          );
	        },
	        priority: 512
	      },
	      type: {
	        validateString: function validateString(value, type) {
	          var _ref = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

	          var _ref$step = _ref.step;
	          var step = _ref$step === undefined ? '1' : _ref$step;
	          var _ref$base = _ref.base;
	          var base = _ref$base === undefined ? 0 : _ref$base;

	          var regex = typeRegexes[type];
	          if (!regex) {
	            throw new Error('validator type `' + type + '` is not supported');
	          }
	          if (!regex.test(value)) return false;
	          if ('number' === type) {
	            if (!/^any$/i.test(step || '')) {
	              var nb = Number(value);
	              var decimals = Math.max(decimalPlaces(step), decimalPlaces(base));
	              if (decimalPlaces(nb) > decimals) // Value can't have too many decimals
	                return false;
	              // Be careful of rounding errors by using integers.
	              var toInt = function toInt(f) {
	                return Math.round(f * Math.pow(10, decimals));
	              };
	              if ((toInt(nb) - toInt(base)) % toInt(step) != 0) return false;
	            }
	          }
	          return true;
	        },
	        requirementType: {
	          '': 'string',
	          step: 'string',
	          base: 'number'
	        },
	        priority: 256
	      },
	      pattern: {
	        validateString: function validateString(value, regexp) {
	          return regexp.test(value);
	        },
	        requirementType: 'regexp',
	        priority: 64
	      },
	      minlength: {
	        validateString: function validateString(value, requirement) {
	          return value.length >= requirement;
	        },
	        requirementType: 'integer',
	        priority: 30
	      },
	      maxlength: {
	        validateString: function validateString(value, requirement) {
	          return value.length <= requirement;
	        },
	        requirementType: 'integer',
	        priority: 30
	      },
	      length: {
	        validateString: function validateString(value, min, max) {
	          return value.length >= min && value.length <= max;
	        },
	        requirementType: ['integer', 'integer'],
	        priority: 30
	      },
	      mincheck: {
	        validateMultiple: function validateMultiple(values, requirement) {
	          return values.length >= requirement;
	        },
	        requirementType: 'integer',
	        priority: 30
	      },
	      maxcheck: {
	        validateMultiple: function validateMultiple(values, requirement) {
	          return values.length <= requirement;
	        },
	        requirementType: 'integer',
	        priority: 30
	      },
	      check: {
	        validateMultiple: function validateMultiple(values, min, max) {
	          return values.length >= min && values.length <= max;
	        },
	        requirementType: ['integer', 'integer'],
	        priority: 30
	      },
	      min: {
	        validateNumber: function validateNumber(value, requirement) {
	          return value >= requirement;
	        },
	        requirementType: 'number',
	        priority: 30
	      },
	      max: {
	        validateNumber: function validateNumber(value, requirement) {
	          return value <= requirement;
	        },
	        requirementType: 'number',
	        priority: 30
	      },
	      range: {
	        validateNumber: function validateNumber(value, min, max) {
	          return value >= min && value <= max;
	        },
	        requirementType: ['number', 'number'],
	        priority: 30
	      },
	      equalto: {
	        validateString: function validateString(value, refOrValue) {
	          var $reference = $(refOrValue);
	          if ($reference.length) return value === $reference.val();else return value === refOrValue;
	        },
	        priority: 256
	      }
	    }
	  };

	  var ParsleyUI = {};

	  var diffResults = function diffResults(newResult, oldResult, deep) {
	    var added = [];
	    var kept = [];

	    for (var i = 0; i < newResult.length; i++) {
	      var found = false;

	      for (var j = 0; j < oldResult.length; j++) if (newResult[i].assert.name === oldResult[j].assert.name) {
	        found = true;
	        break;
	      }

	      if (found) kept.push(newResult[i]);else added.push(newResult[i]);
	    }

	    return {
	      kept: kept,
	      added: added,
	      removed: !deep ? diffResults(oldResult, newResult, true).added : []
	    };
	  };

	  ParsleyUI.Form = {

	    _actualizeTriggers: function _actualizeTriggers() {
	      var _this2 = this;

	      this.$element.on('submit.Parsley', function (evt) {
	        _this2.onSubmitValidate(evt);
	      });
	      this.$element.on('click.Parsley', 'input[type="submit"], button[type="submit"]', function (evt) {
	        _this2.onSubmitButton(evt);
	      });

	      // UI could be disabled
	      if (false === this.options.uiEnabled) return;

	      this.$element.attr('novalidate', '');
	    },

	    focus: function focus() {
	      this._focusedField = null;

	      if (true === this.validationResult || 'none' === this.options.focus) return null;

	      for (var i = 0; i < this.fields.length; i++) {
	        var field = this.fields[i];
	        if (true !== field.validationResult && field.validationResult.length > 0 && 'undefined' === typeof field.options.noFocus) {
	          this._focusedField = field.$element;
	          if ('first' === this.options.focus) break;
	        }
	      }

	      if (null === this._focusedField) return null;

	      return this._focusedField.focus();
	    },

	    _destroyUI: function _destroyUI() {
	      // Reset all event listeners
	      this.$element.off('.Parsley');
	    }

	  };

	  ParsleyUI.Field = {

	    _reflowUI: function _reflowUI() {
	      this._buildUI();

	      // If this field doesn't have an active UI don't bother doing something
	      if (!this._ui) return;

	      // Diff between two validation results
	      var diff = diffResults(this.validationResult, this._ui.lastValidationResult);

	      // Then store current validation result for next reflow
	      this._ui.lastValidationResult = this.validationResult;

	      // Handle valid / invalid / none field class
	      this._manageStatusClass();

	      // Add, remove, updated errors messages
	      this._manageErrorsMessages(diff);

	      // Triggers impl
	      this._actualizeTriggers();

	      // If field is not valid for the first time, bind keyup trigger to ease UX and quickly inform user
	      if ((diff.kept.length || diff.added.length) && !this._failedOnce) {
	        this._failedOnce = true;
	        this._actualizeTriggers();
	      }
	    },

	    // Returns an array of field's error message(s)
	    getErrorsMessages: function getErrorsMessages() {
	      // No error message, field is valid
	      if (true === this.validationResult) return [];

	      var messages = [];

	      for (var i = 0; i < this.validationResult.length; i++) messages.push(this.validationResult[i].errorMessage || this._getErrorMessage(this.validationResult[i].assert));

	      return messages;
	    },

	    // It's a goal of Parsley that this method is no longer required [#1073]
	    addError: function addError(name) {
	      var _ref2 = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

	      var message = _ref2.message;
	      var assert = _ref2.assert;
	      var _ref2$updateClass = _ref2.updateClass;
	      var updateClass = _ref2$updateClass === undefined ? true : _ref2$updateClass;

	      this._buildUI();
	      this._addError(name, { message: message, assert: assert });

	      if (updateClass) this._errorClass();
	    },

	    // It's a goal of Parsley that this method is no longer required [#1073]
	    updateError: function updateError(name) {
	      var _ref3 = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

	      var message = _ref3.message;
	      var assert = _ref3.assert;
	      var _ref3$updateClass = _ref3.updateClass;
	      var updateClass = _ref3$updateClass === undefined ? true : _ref3$updateClass;

	      this._buildUI();
	      this._updateError(name, { message: message, assert: assert });

	      if (updateClass) this._errorClass();
	    },

	    // It's a goal of Parsley that this method is no longer required [#1073]
	    removeError: function removeError(name) {
	      var _ref4 = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

	      var _ref4$updateClass = _ref4.updateClass;
	      var updateClass = _ref4$updateClass === undefined ? true : _ref4$updateClass;

	      this._buildUI();
	      this._removeError(name);

	      // edge case possible here: remove a standard Parsley error that is still failing in this.validationResult
	      // but highly improbable cuz' manually removing a well Parsley handled error makes no sense.
	      if (updateClass) this._manageStatusClass();
	    },

	    _manageStatusClass: function _manageStatusClass() {
	      if (this.hasConstraints() && this.needsValidation() && true === this.validationResult) this._successClass();else if (this.validationResult.length > 0) this._errorClass();else this._resetClass();
	    },

	    _manageErrorsMessages: function _manageErrorsMessages(diff) {
	      if ('undefined' !== typeof this.options.errorsMessagesDisabled) return;

	      // Case where we have errorMessage option that configure an unique field error message, regardless failing validators
	      if ('undefined' !== typeof this.options.errorMessage) {
	        if (diff.added.length || diff.kept.length) {
	          this._insertErrorWrapper();

	          if (0 === this._ui.$errorsWrapper.find('.parsley-custom-error-message').length) this._ui.$errorsWrapper.append($(this.options.errorTemplate).addClass('parsley-custom-error-message'));

	          return this._ui.$errorsWrapper.addClass('filled').find('.parsley-custom-error-message').html(this.options.errorMessage);
	        }

	        return this._ui.$errorsWrapper.removeClass('filled').find('.parsley-custom-error-message').remove();
	      }

	      // Show, hide, update failing constraints messages
	      for (var i = 0; i < diff.removed.length; i++) this._removeError(diff.removed[i].assert.name);

	      for (i = 0; i < diff.added.length; i++) this._addError(diff.added[i].assert.name, { message: diff.added[i].errorMessage, assert: diff.added[i].assert });

	      for (i = 0; i < diff.kept.length; i++) this._updateError(diff.kept[i].assert.name, { message: diff.kept[i].errorMessage, assert: diff.kept[i].assert });
	    },

	    _addError: function _addError(name, _ref5) {
	      var message = _ref5.message;
	      var assert = _ref5.assert;

	      this._insertErrorWrapper();
	      this._ui.$errorsWrapper.addClass('filled').append($(this.options.errorTemplate).addClass('parsley-' + name).html(message || this._getErrorMessage(assert)));
	    },

	    _updateError: function _updateError(name, _ref6) {
	      var message = _ref6.message;
	      var assert = _ref6.assert;

	      this._ui.$errorsWrapper.addClass('filled').find('.parsley-' + name).html(message || this._getErrorMessage(assert));
	    },

	    _removeError: function _removeError(name) {
	      this._ui.$errorsWrapper.removeClass('filled').find('.parsley-' + name).remove();
	    },

	    _getErrorMessage: function _getErrorMessage(constraint) {
	      var customConstraintErrorMessage = constraint.name + 'Message';

	      if ('undefined' !== typeof this.options[customConstraintErrorMessage]) return window.Parsley.formatMessage(this.options[customConstraintErrorMessage], constraint.requirements);

	      return window.Parsley.getErrorMessage(constraint);
	    },

	    _buildUI: function _buildUI() {
	      // UI could be already built or disabled
	      if (this._ui || false === this.options.uiEnabled) return;

	      var _ui = {};

	      // Give field its Parsley id in DOM
	      this.$element.attr(this.options.namespace + 'id', this.__id__);

	      /** Generate important UI elements and store them in this **/
	      // $errorClassHandler is the $element that woul have parsley-error and parsley-success classes
	      _ui.$errorClassHandler = this._manageClassHandler();

	      // $errorsWrapper is a div that would contain the various field errors, it will be appended into $errorsContainer
	      _ui.errorsWrapperId = 'parsley-id-' + (this.options.multiple ? 'multiple-' + this.options.multiple : this.__id__);
	      _ui.$errorsWrapper = $(this.options.errorsWrapper).attr('id', _ui.errorsWrapperId);

	      // ValidationResult UI storage to detect what have changed bwt two validations, and update DOM accordingly
	      _ui.lastValidationResult = [];
	      _ui.validationInformationVisible = false;

	      // Store it in this for later
	      this._ui = _ui;
	    },

	    // Determine which element will have `parsley-error` and `parsley-success` classes
	    _manageClassHandler: function _manageClassHandler() {
	      // An element selector could be passed through DOM with `data-parsley-class-handler=#foo`
	      if ('string' === typeof this.options.classHandler && $(this.options.classHandler).length) return $(this.options.classHandler);

	      // Class handled could also be determined by function given in Parsley options
	      var $handler = this.options.classHandler.call(this, this);

	      // If this function returned a valid existing DOM element, go for it
	      if ('undefined' !== typeof $handler && $handler.length) return $handler;

	      // Otherwise, if simple element (input, texatrea, select...) it will perfectly host the classes
	      if (!this.options.multiple || this.$element.is('select')) return this.$element;

	      // But if multiple element (radio, checkbox), that would be their parent
	      return this.$element.parent();
	    },

	    _insertErrorWrapper: function _insertErrorWrapper() {
	      var $errorsContainer;

	      // Nothing to do if already inserted
	      if (0 !== this._ui.$errorsWrapper.parent().length) return this._ui.$errorsWrapper.parent();

	      if ('string' === typeof this.options.errorsContainer) {
	        if ($(this.options.errorsContainer).length) return $(this.options.errorsContainer).append(this._ui.$errorsWrapper);else ParsleyUtils__default.warn('The errors container `' + this.options.errorsContainer + '` does not exist in DOM');
	      } else if ('function' === typeof this.options.errorsContainer) $errorsContainer = this.options.errorsContainer.call(this, this);

	      if ('undefined' !== typeof $errorsContainer && $errorsContainer.length) return $errorsContainer.append(this._ui.$errorsWrapper);

	      var $from = this.$element;
	      if (this.options.multiple) $from = $from.parent();
	      return $from.after(this._ui.$errorsWrapper);
	    },

	    _actualizeTriggers: function _actualizeTriggers() {
	      var _this3 = this;

	      var $toBind = this._findRelated();
	      var trigger;

	      // Remove Parsley events already bound on this field
	      $toBind.off('.Parsley');
	      if (this._failedOnce) $toBind.on(ParsleyUtils__default.namespaceEvents(this.options.triggerAfterFailure, 'Parsley'), function () {
	        _this3.validate();
	      });else if (trigger = ParsleyUtils__default.namespaceEvents(this.options.trigger, 'Parsley')) {
	        $toBind.on(trigger, function (event) {
	          _this3._eventValidate(event);
	        });
	      }
	    },

	    _eventValidate: function _eventValidate(event) {
	      // For keyup, keypress, keydown, input... events that could be a little bit obstrusive
	      // do not validate if val length < min threshold on first validation. Once field have been validated once and info
	      // about success or failure have been displayed, always validate with this trigger to reflect every yalidation change.
	      if (/key|input/.test(event.type)) if (!(this._ui && this._ui.validationInformationVisible) && this.getValue().length <= this.options.validationThreshold) return;

	      this.validate();
	    },

	    _resetUI: function _resetUI() {
	      // Reset all event listeners
	      this._failedOnce = false;
	      this._actualizeTriggers();

	      // Nothing to do if UI never initialized for this field
	      if ('undefined' === typeof this._ui) return;

	      // Reset all errors' li
	      this._ui.$errorsWrapper.removeClass('filled').children().remove();

	      // Reset validation class
	      this._resetClass();

	      // Reset validation flags and last validation result
	      this._ui.lastValidationResult = [];
	      this._ui.validationInformationVisible = false;
	    },

	    _destroyUI: function _destroyUI() {
	      this._resetUI();

	      if ('undefined' !== typeof this._ui) this._ui.$errorsWrapper.remove();

	      delete this._ui;
	    },

	    _successClass: function _successClass() {
	      this._ui.validationInformationVisible = true;
	      this._ui.$errorClassHandler.removeClass(this.options.errorClass).addClass(this.options.successClass);
	    },
	    _errorClass: function _errorClass() {
	      this._ui.validationInformationVisible = true;
	      this._ui.$errorClassHandler.removeClass(this.options.successClass).addClass(this.options.errorClass);
	    },
	    _resetClass: function _resetClass() {
	      this._ui.$errorClassHandler.removeClass(this.options.successClass).removeClass(this.options.errorClass);
	    }
	  };

	  var ParsleyForm = function ParsleyForm(element, domOptions, options) {
	    this.__class__ = 'ParsleyForm';

	    this.$element = $(element);
	    this.domOptions = domOptions;
	    this.options = options;
	    this.parent = window.Parsley;

	    this.fields = [];
	    this.validationResult = null;
	  };

	  var ParsleyForm__statusMapping = { pending: null, resolved: true, rejected: false };

	  ParsleyForm.prototype = {
	    onSubmitValidate: function onSubmitValidate(event) {
	      var _this4 = this;

	      // This is a Parsley generated submit event, do not validate, do not prevent, simply exit and keep normal behavior
	      if (true === event.parsley) return;

	      // If we didn't come here through a submit button, use the first one in the form
	      var $submitSource = this._$submitSource || this.$element.find('input[type="submit"], button[type="submit"]').first();
	      this._$submitSource = null;
	      this.$element.find('.parsley-synthetic-submit-button').prop('disabled', true);
	      if ($submitSource.is('[formnovalidate]')) return;

	      var promise = this.whenValidate({ event: event });

	      if ('resolved' === promise.state() && false !== this._trigger('submit')) {
	        // All good, let event go through. We make this distinction because browsers
	        // differ in their handling of `submit` being called from inside a submit event [#1047]
	      } else {
	          // Rejected or pending: cancel this submit
	          event.stopImmediatePropagation();
	          event.preventDefault();
	          if ('pending' === promise.state()) promise.done(function () {
	            _this4._submit($submitSource);
	          });
	        }
	    },

	    onSubmitButton: function onSubmitButton(event) {
	      this._$submitSource = $(event.target);
	    },
	    // internal
	    // _submit submits the form, this time without going through the validations.
	    // Care must be taken to "fake" the actual submit button being clicked.
	    _submit: function _submit($submitSource) {
	      if (false === this._trigger('submit')) return;
	      // Add submit button's data
	      if ($submitSource) {
	        var $synthetic = this.$element.find('.parsley-synthetic-submit-button').prop('disabled', false);
	        if (0 === $synthetic.length) $synthetic = $('<input class="parsley-synthetic-submit-button" type="hidden">').appendTo(this.$element);
	        $synthetic.attr({
	          name: $submitSource.attr('name'),
	          value: $submitSource.attr('value')
	        });
	      }

	      this.$element.trigger($.extend($.Event('submit'), { parsley: true }));
	    },

	    // Performs validation on fields while triggering events.
	    // @returns `true` if all validations succeeds, `false`
	    // if a failure is immediately detected, or `null`
	    // if dependant on a promise.
	    // Consider using `whenValidate` instead.
	    validate: function validate(options) {
	      if (arguments.length >= 1 && !$.isPlainObject(options)) {
	        ParsleyUtils__default.warnOnce('Calling validate on a parsley form without passing arguments as an object is deprecated.');

	        var _arguments = _slice.call(arguments);

	        var group = _arguments[0];
	        var force = _arguments[1];
	        var event = _arguments[2];

	        options = { group: group, force: force, event: event };
	      }
	      return ParsleyForm__statusMapping[this.whenValidate(options).state()];
	    },

	    whenValidate: function whenValidate() {
	      var _$$when$done$fail$always,
	          _this5 = this;

	      var _ref7 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

	      var group = _ref7.group;
	      var force = _ref7.force;
	      var event = _ref7.event;

	      this.submitEvent = event;
	      if (event) {
	        this.submitEvent = $.extend({}, event, { preventDefault: function preventDefault() {
	            ParsleyUtils__default.warnOnce("Using `this.submitEvent.preventDefault()` is deprecated; instead, call `this.validationResult = false`");
	            _this5.validationResult = false;
	          } });
	      }
	      this.validationResult = true;

	      // fire validate event to eventually modify things before very validation
	      this._trigger('validate');

	      // Refresh form DOM options and form's fields that could have changed
	      this._refreshFields();

	      var promises = this._withoutReactualizingFormOptions(function () {
	        return $.map(_this5.fields, function (field) {
	          return field.whenValidate({ force: force, group: group });
	        });
	      });

	      return (_$$when$done$fail$always = $.when.apply($, _toConsumableArray(promises)).done(function () {
	        _this5._trigger('success');
	      }).fail(function () {
	        _this5.validationResult = false;
	        _this5.focus();
	        _this5._trigger('error');
	      }).always(function () {
	        _this5._trigger('validated');
	      })).pipe.apply(_$$when$done$fail$always, _toConsumableArray(this._pipeAccordingToValidationResult()));
	    },

	    // Iterate over refreshed fields, and stop on first failure.
	    // Returns `true` if all fields are valid, `false` if a failure is detected
	    // or `null` if the result depends on an unresolved promise.
	    // Prefer using `whenValid` instead.
	    isValid: function isValid(options) {
	      if (arguments.length >= 1 && !$.isPlainObject(options)) {
	        ParsleyUtils__default.warnOnce('Calling isValid on a parsley form without passing arguments as an object is deprecated.');

	        var _arguments2 = _slice.call(arguments);

	        var group = _arguments2[0];
	        var force = _arguments2[1];

	        options = { group: group, force: force };
	      }
	      return ParsleyForm__statusMapping[this.whenValid(options).state()];
	    },

	    // Iterate over refreshed fields and validate them.
	    // Returns a promise.
	    // A validation that immediately fails will interrupt the validations.
	    whenValid: function whenValid() {
	      var _this6 = this;

	      var _ref8 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

	      var group = _ref8.group;
	      var force = _ref8.force;

	      this._refreshFields();

	      var promises = this._withoutReactualizingFormOptions(function () {
	        return $.map(_this6.fields, function (field) {
	          return field.whenValid({ group: group, force: force });
	        });
	      });
	      return $.when.apply($, _toConsumableArray(promises));
	    },

	    _refreshFields: function _refreshFields() {
	      return this.actualizeOptions()._bindFields();
	    },

	    _bindFields: function _bindFields() {
	      var _this7 = this;

	      var oldFields = this.fields;

	      this.fields = [];
	      this.fieldsMappedById = {};

	      this._withoutReactualizingFormOptions(function () {
	        _this7.$element.find(_this7.options.inputs).not(_this7.options.excluded).each(function (_, element) {
	          var fieldInstance = new window.Parsley.Factory(element, {}, _this7);

	          // Only add valid and not excluded `ParsleyField` and `ParsleyFieldMultiple` children
	          if (('ParsleyField' === fieldInstance.__class__ || 'ParsleyFieldMultiple' === fieldInstance.__class__) && true !== fieldInstance.options.excluded) if ('undefined' === typeof _this7.fieldsMappedById[fieldInstance.__class__ + '-' + fieldInstance.__id__]) {
	            _this7.fieldsMappedById[fieldInstance.__class__ + '-' + fieldInstance.__id__] = fieldInstance;
	            _this7.fields.push(fieldInstance);
	          }
	        });

	        $(oldFields).not(_this7.fields).each(function (_, field) {
	          field._trigger('reset');
	        });
	      });
	      return this;
	    },

	    // Internal only.
	    // Looping on a form's fields to do validation or similar
	    // will trigger reactualizing options on all of them, which
	    // in turn will reactualize the form's options.
	    // To avoid calling actualizeOptions so many times on the form
	    // for nothing, _withoutReactualizingFormOptions temporarily disables
	    // the method actualizeOptions on this form while `fn` is called.
	    _withoutReactualizingFormOptions: function _withoutReactualizingFormOptions(fn) {
	      var oldActualizeOptions = this.actualizeOptions;
	      this.actualizeOptions = function () {
	        return this;
	      };
	      var result = fn();
	      this.actualizeOptions = oldActualizeOptions;
	      return result;
	    },

	    // Internal only.
	    // Shortcut to trigger an event
	    // Returns true iff event is not interrupted and default not prevented.
	    _trigger: function _trigger(eventName) {
	      return this.trigger('form:' + eventName);
	    }

	  };

	  var ConstraintFactory = function ConstraintFactory(parsleyField, name, requirements, priority, isDomConstraint) {
	    if (!/ParsleyField/.test(parsleyField.__class__)) throw new Error('ParsleyField or ParsleyFieldMultiple instance expected');

	    var validatorSpec = window.Parsley._validatorRegistry.validators[name];
	    var validator = new ParsleyValidator(validatorSpec);

	    $.extend(this, {
	      validator: validator,
	      name: name,
	      requirements: requirements,
	      priority: priority || parsleyField.options[name + 'Priority'] || validator.priority,
	      isDomConstraint: true === isDomConstraint
	    });
	    this._parseRequirements(parsleyField.options);
	  };

	  var capitalize = function capitalize(str) {
	    var cap = str[0].toUpperCase();
	    return cap + str.slice(1);
	  };

	  ConstraintFactory.prototype = {
	    validate: function validate(value, instance) {
	      var _validator;

	      return (_validator = this.validator).validate.apply(_validator, [value].concat(_toConsumableArray(this.requirementList), [instance]));
	    },

	    _parseRequirements: function _parseRequirements(options) {
	      var _this8 = this;

	      this.requirementList = this.validator.parseRequirements(this.requirements, function (key) {
	        return options[_this8.name + capitalize(key)];
	      });
	    }
	  };

	  var ParsleyField = function ParsleyField(field, domOptions, options, parsleyFormInstance) {
	    this.__class__ = 'ParsleyField';

	    this.$element = $(field);

	    // Set parent if we have one
	    if ('undefined' !== typeof parsleyFormInstance) {
	      this.parent = parsleyFormInstance;
	    }

	    this.options = options;
	    this.domOptions = domOptions;

	    // Initialize some properties
	    this.constraints = [];
	    this.constraintsByName = {};
	    this.validationResult = true;

	    // Bind constraints
	    this._bindConstraints();
	  };

	  var parsley_field__statusMapping = { pending: null, resolved: true, rejected: false };

	  ParsleyField.prototype = {
	    // # Public API
	    // Validate field and trigger some events for mainly `ParsleyUI`
	    // @returns `true`, an array of the validators that failed, or
	    // `null` if validation is not finished. Prefer using whenValidate
	    validate: function validate(options) {
	      if (arguments.length >= 1 && !$.isPlainObject(options)) {
	        ParsleyUtils__default.warnOnce('Calling validate on a parsley field without passing arguments as an object is deprecated.');
	        options = { options: options };
	      }
	      var promise = this.whenValidate(options);
	      if (!promise) // If excluded with `group` option
	        return true;
	      switch (promise.state()) {
	        case 'pending':
	          return null;
	        case 'resolved':
	          return true;
	        case 'rejected':
	          return this.validationResult;
	      }
	    },

	    // Validate field and trigger some events for mainly `ParsleyUI`
	    // @returns a promise that succeeds only when all validations do
	    // or `undefined` if field is not in the given `group`.
	    whenValidate: function whenValidate() {
	      var _whenValid$always$done$fail$always,
	          _this9 = this;

	      var _ref9 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

	      var force = _ref9.force;
	      var group = _ref9.group;

	      // do not validate a field if not the same as given validation group
	      this.refreshConstraints();
	      if (group && !this._isInGroup(group)) return;

	      this.value = this.getValue();

	      // Field Validate event. `this.value` could be altered for custom needs
	      this._trigger('validate');

	      return (_whenValid$always$done$fail$always = this.whenValid({ force: force, value: this.value, _refreshed: true }).always(function () {
	        _this9._reflowUI();
	      }).done(function () {
	        _this9._trigger('success');
	      }).fail(function () {
	        _this9._trigger('error');
	      }).always(function () {
	        _this9._trigger('validated');
	      })).pipe.apply(_whenValid$always$done$fail$always, _toConsumableArray(this._pipeAccordingToValidationResult()));
	    },

	    hasConstraints: function hasConstraints() {
	      return 0 !== this.constraints.length;
	    },

	    // An empty optional field does not need validation
	    needsValidation: function needsValidation(value) {
	      if ('undefined' === typeof value) value = this.getValue();

	      // If a field is empty and not required, it is valid
	      // Except if `data-parsley-validate-if-empty` explicitely added, useful for some custom validators
	      if (!value.length && !this._isRequired() && 'undefined' === typeof this.options.validateIfEmpty) return false;

	      return true;
	    },

	    _isInGroup: function _isInGroup(group) {
	      if ($.isArray(this.options.group)) return -1 !== $.inArray(group, this.options.group);
	      return this.options.group === group;
	    },

	    // Just validate field. Do not trigger any event.
	    // Returns `true` iff all constraints pass, `false` if there are failures,
	    // or `null` if the result can not be determined yet (depends on a promise)
	    // See also `whenValid`.
	    isValid: function isValid(options) {
	      if (arguments.length >= 1 && !$.isPlainObject(options)) {
	        ParsleyUtils__default.warnOnce('Calling isValid on a parsley field without passing arguments as an object is deprecated.');

	        var _arguments3 = _slice.call(arguments);

	        var force = _arguments3[0];
	        var value = _arguments3[1];

	        options = { force: force, value: value };
	      }
	      var promise = this.whenValid(options);
	      if (!promise) // Excluded via `group`
	        return true;
	      return parsley_field__statusMapping[promise.state()];
	    },

	    // Just validate field. Do not trigger any event.
	    // @returns a promise that succeeds only when all validations do
	    // or `undefined` if the field is not in the given `group`.
	    // The argument `force` will force validation of empty fields.
	    // If a `value` is given, it will be validated instead of the value of the input.
	    whenValid: function whenValid() {
	      var _this10 = this;

	      var _ref10 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

	      var _ref10$force = _ref10.force;
	      var force = _ref10$force === undefined ? false : _ref10$force;
	      var value = _ref10.value;
	      var group = _ref10.group;
	      var _refreshed = _ref10._refreshed;

	      // Recompute options and rebind constraints to have latest changes
	      if (!_refreshed) this.refreshConstraints();
	      // do not validate a field if not the same as given validation group
	      if (group && !this._isInGroup(group)) return;

	      this.validationResult = true;

	      // A field without constraint is valid
	      if (!this.hasConstraints()) return $.when();

	      // Value could be passed as argument, needed to add more power to 'field:validate'
	      if ('undefined' === typeof value || null === value) value = this.getValue();

	      if (!this.needsValidation(value) && true !== force) return $.when();

	      var groupedConstraints = this._getGroupedConstraints();
	      var promises = [];
	      $.each(groupedConstraints, function (_, constraints) {
	        // Process one group of constraints at a time, we validate the constraints
	        // and combine the promises together.
	        var promise = $.when.apply($, _toConsumableArray($.map(constraints, function (constraint) {
	          return _this10._validateConstraint(value, constraint);
	        })));
	        promises.push(promise);
	        if (promise.state() === 'rejected') return false; // Interrupt processing if a group has already failed
	      });
	      return $.when.apply($, promises);
	    },

	    // @returns a promise
	    _validateConstraint: function _validateConstraint(value, constraint) {
	      var _this11 = this;

	      var result = constraint.validate(value, this);
	      // Map false to a failed promise
	      if (false === result) result = $.Deferred().reject();
	      // Make sure we return a promise and that we record failures
	      return $.when(result).fail(function (errorMessage) {
	        if (!(_this11.validationResult instanceof Array)) _this11.validationResult = [];
	        _this11.validationResult.push({
	          assert: constraint,
	          errorMessage: 'string' === typeof errorMessage && errorMessage
	        });
	      });
	    },

	    // @returns Parsley field computed value that could be overrided or configured in DOM
	    getValue: function getValue() {
	      var value;

	      // Value could be overriden in DOM or with explicit options
	      if ('function' === typeof this.options.value) value = this.options.value(this);else if ('undefined' !== typeof this.options.value) value = this.options.value;else value = this.$element.val();

	      // Handle wrong DOM or configurations
	      if ('undefined' === typeof value || null === value) return '';

	      return this._handleWhitespace(value);
	    },

	    // Actualize options that could have change since previous validation
	    // Re-bind accordingly constraints (could be some new, removed or updated)
	    refreshConstraints: function refreshConstraints() {
	      return this.actualizeOptions()._bindConstraints();
	    },

	    /**
	    * Add a new constraint to a field
	    *
	    * @param {String}   name
	    * @param {Mixed}    requirements      optional
	    * @param {Number}   priority          optional
	    * @param {Boolean}  isDomConstraint   optional
	    */
	    addConstraint: function addConstraint(name, requirements, priority, isDomConstraint) {

	      if (window.Parsley._validatorRegistry.validators[name]) {
	        var constraint = new ConstraintFactory(this, name, requirements, priority, isDomConstraint);

	        // if constraint already exist, delete it and push new version
	        if ('undefined' !== this.constraintsByName[constraint.name]) this.removeConstraint(constraint.name);

	        this.constraints.push(constraint);
	        this.constraintsByName[constraint.name] = constraint;
	      }

	      return this;
	    },

	    // Remove a constraint
	    removeConstraint: function removeConstraint(name) {
	      for (var i = 0; i < this.constraints.length; i++) if (name === this.constraints[i].name) {
	        this.constraints.splice(i, 1);
	        break;
	      }
	      delete this.constraintsByName[name];
	      return this;
	    },

	    // Update a constraint (Remove + re-add)
	    updateConstraint: function updateConstraint(name, parameters, priority) {
	      return this.removeConstraint(name).addConstraint(name, parameters, priority);
	    },

	    // # Internals

	    // Internal only.
	    // Bind constraints from config + options + DOM
	    _bindConstraints: function _bindConstraints() {
	      var constraints = [];
	      var constraintsByName = {};

	      // clean all existing DOM constraints to only keep javascript user constraints
	      for (var i = 0; i < this.constraints.length; i++) if (false === this.constraints[i].isDomConstraint) {
	        constraints.push(this.constraints[i]);
	        constraintsByName[this.constraints[i].name] = this.constraints[i];
	      }

	      this.constraints = constraints;
	      this.constraintsByName = constraintsByName;

	      // then re-add Parsley DOM-API constraints
	      for (var name in this.options) this.addConstraint(name, this.options[name], undefined, true);

	      // finally, bind special HTML5 constraints
	      return this._bindHtml5Constraints();
	    },

	    // Internal only.
	    // Bind specific HTML5 constraints to be HTML5 compliant
	    _bindHtml5Constraints: function _bindHtml5Constraints() {
	      // html5 required
	      if (this.$element.hasClass('required') || this.$element.attr('required')) this.addConstraint('required', true, undefined, true);

	      // html5 pattern
	      if ('string' === typeof this.$element.attr('pattern')) this.addConstraint('pattern', this.$element.attr('pattern'), undefined, true);

	      // range
	      if ('undefined' !== typeof this.$element.attr('min') && 'undefined' !== typeof this.$element.attr('max')) this.addConstraint('range', [this.$element.attr('min'), this.$element.attr('max')], undefined, true);

	      // HTML5 min
	      else if ('undefined' !== typeof this.$element.attr('min')) this.addConstraint('min', this.$element.attr('min'), undefined, true);

	        // HTML5 max
	        else if ('undefined' !== typeof this.$element.attr('max')) this.addConstraint('max', this.$element.attr('max'), undefined, true);

	      // length
	      if ('undefined' !== typeof this.$element.attr('minlength') && 'undefined' !== typeof this.$element.attr('maxlength')) this.addConstraint('length', [this.$element.attr('minlength'), this.$element.attr('maxlength')], undefined, true);

	      // HTML5 minlength
	      else if ('undefined' !== typeof this.$element.attr('minlength')) this.addConstraint('minlength', this.$element.attr('minlength'), undefined, true);

	        // HTML5 maxlength
	        else if ('undefined' !== typeof this.$element.attr('maxlength')) this.addConstraint('maxlength', this.$element.attr('maxlength'), undefined, true);

	      // html5 types
	      var type = this.$element.attr('type');

	      if ('undefined' === typeof type) return this;

	      // Small special case here for HTML5 number: integer validator if step attribute is undefined or an integer value, number otherwise
	      if ('number' === type) {
	        return this.addConstraint('type', ['number', {
	          step: this.$element.attr('step'),
	          base: this.$element.attr('min') || this.$element.attr('value')
	        }], undefined, true);
	        // Regular other HTML5 supported types
	      } else if (/^(email|url|range)$/i.test(type)) {
	          return this.addConstraint('type', type, undefined, true);
	        }
	      return this;
	    },

	    // Internal only.
	    // Field is required if have required constraint without `false` value
	    _isRequired: function _isRequired() {
	      if ('undefined' === typeof this.constraintsByName.required) return false;

	      return false !== this.constraintsByName.required.requirements;
	    },

	    // Internal only.
	    // Shortcut to trigger an event
	    _trigger: function _trigger(eventName) {
	      return this.trigger('field:' + eventName);
	    },

	    // Internal only
	    // Handles whitespace in a value
	    // Use `data-parsley-whitespace="squish"` to auto squish input value
	    // Use `data-parsley-whitespace="trim"` to auto trim input value
	    _handleWhitespace: function _handleWhitespace(value) {
	      if (true === this.options.trimValue) ParsleyUtils__default.warnOnce('data-parsley-trim-value="true" is deprecated, please use data-parsley-whitespace="trim"');

	      if ('squish' === this.options.whitespace) value = value.replace(/\s{2,}/g, ' ');

	      if ('trim' === this.options.whitespace || 'squish' === this.options.whitespace || true === this.options.trimValue) value = ParsleyUtils__default.trimString(value);

	      return value;
	    },

	    // Internal only.
	    // Returns the constraints, grouped by descending priority.
	    // The result is thus an array of arrays of constraints.
	    _getGroupedConstraints: function _getGroupedConstraints() {
	      if (false === this.options.priorityEnabled) return [this.constraints];

	      var groupedConstraints = [];
	      var index = {};

	      // Create array unique of priorities
	      for (var i = 0; i < this.constraints.length; i++) {
	        var p = this.constraints[i].priority;
	        if (!index[p]) groupedConstraints.push(index[p] = []);
	        index[p].push(this.constraints[i]);
	      }
	      // Sort them by priority DESC
	      groupedConstraints.sort(function (a, b) {
	        return b[0].priority - a[0].priority;
	      });

	      return groupedConstraints;
	    }

	  };

	  var parsley_field = ParsleyField;

	  var ParsleyMultiple = function ParsleyMultiple() {
	    this.__class__ = 'ParsleyFieldMultiple';
	  };

	  ParsleyMultiple.prototype = {
	    // Add new `$element` sibling for multiple field
	    addElement: function addElement($element) {
	      this.$elements.push($element);

	      return this;
	    },

	    // See `ParsleyField.refreshConstraints()`
	    refreshConstraints: function refreshConstraints() {
	      var fieldConstraints;

	      this.constraints = [];

	      // Select multiple special treatment
	      if (this.$element.is('select')) {
	        this.actualizeOptions()._bindConstraints();

	        return this;
	      }

	      // Gather all constraints for each input in the multiple group
	      for (var i = 0; i < this.$elements.length; i++) {

	        // Check if element have not been dynamically removed since last binding
	        if (!$('html').has(this.$elements[i]).length) {
	          this.$elements.splice(i, 1);
	          continue;
	        }

	        fieldConstraints = this.$elements[i].data('ParsleyFieldMultiple').refreshConstraints().constraints;

	        for (var j = 0; j < fieldConstraints.length; j++) this.addConstraint(fieldConstraints[j].name, fieldConstraints[j].requirements, fieldConstraints[j].priority, fieldConstraints[j].isDomConstraint);
	      }

	      return this;
	    },

	    // See `ParsleyField.getValue()`
	    getValue: function getValue() {
	      // Value could be overriden in DOM
	      if ('function' === typeof this.options.value) return this.options.value(this);else if ('undefined' !== typeof this.options.value) return this.options.value;

	      // Radio input case
	      if (this.$element.is('input[type=radio]')) return this._findRelated().filter(':checked').val() || '';

	      // checkbox input case
	      if (this.$element.is('input[type=checkbox]')) {
	        var values = [];

	        this._findRelated().filter(':checked').each(function () {
	          values.push($(this).val());
	        });

	        return values;
	      }

	      // Select multiple case
	      if (this.$element.is('select') && null === this.$element.val()) return [];

	      // Default case that should never happen
	      return this.$element.val();
	    },

	    _init: function _init() {
	      this.$elements = [this.$element];

	      return this;
	    }
	  };

	  var ParsleyFactory = function ParsleyFactory(element, options, parsleyFormInstance) {
	    this.$element = $(element);

	    // If the element has already been bound, returns its saved Parsley instance
	    var savedparsleyFormInstance = this.$element.data('Parsley');
	    if (savedparsleyFormInstance) {

	      // If the saved instance has been bound without a ParsleyForm parent and there is one given in this call, add it
	      if ('undefined' !== typeof parsleyFormInstance && savedparsleyFormInstance.parent === window.Parsley) {
	        savedparsleyFormInstance.parent = parsleyFormInstance;
	        savedparsleyFormInstance._resetOptions(savedparsleyFormInstance.options);
	      }

	      return savedparsleyFormInstance;
	    }

	    // Parsley must be instantiated with a DOM element or jQuery $element
	    if (!this.$element.length) throw new Error('You must bind Parsley on an existing element.');

	    if ('undefined' !== typeof parsleyFormInstance && 'ParsleyForm' !== parsleyFormInstance.__class__) throw new Error('Parent instance must be a ParsleyForm instance');

	    this.parent = parsleyFormInstance || window.Parsley;
	    return this.init(options);
	  };

	  ParsleyFactory.prototype = {
	    init: function init(options) {
	      this.__class__ = 'Parsley';
	      this.__version__ = '2.3.13';
	      this.__id__ = ParsleyUtils__default.generateID();

	      // Pre-compute options
	      this._resetOptions(options);

	      // A ParsleyForm instance is obviously a `<form>` element but also every node that is not an input and has the `data-parsley-validate` attribute
	      if (this.$element.is('form') || ParsleyUtils__default.checkAttr(this.$element, this.options.namespace, 'validate') && !this.$element.is(this.options.inputs)) return this.bind('parsleyForm');

	      // Every other element is bound as a `ParsleyField` or `ParsleyFieldMultiple`
	      return this.isMultiple() ? this.handleMultiple() : this.bind('parsleyField');
	    },

	    isMultiple: function isMultiple() {
	      return this.$element.is('input[type=radio], input[type=checkbox]') || this.$element.is('select') && 'undefined' !== typeof this.$element.attr('multiple');
	    },

	    // Multiples fields are a real nightmare :(
	    // Maybe some refactoring would be appreciated here...
	    handleMultiple: function handleMultiple() {
	      var _this12 = this;

	      var name;
	      var multiple;
	      var parsleyMultipleInstance;

	      // Handle multiple name
	      if (this.options.multiple) ; // We already have our 'multiple' identifier
	      else if ('undefined' !== typeof this.$element.attr('name') && this.$element.attr('name').length) this.options.multiple = name = this.$element.attr('name');else if ('undefined' !== typeof this.$element.attr('id') && this.$element.attr('id').length) this.options.multiple = this.$element.attr('id');

	      // Special select multiple input
	      if (this.$element.is('select') && 'undefined' !== typeof this.$element.attr('multiple')) {
	        this.options.multiple = this.options.multiple || this.__id__;
	        return this.bind('parsleyFieldMultiple');

	        // Else for radio / checkboxes, we need a `name` or `data-parsley-multiple` to properly bind it
	      } else if (!this.options.multiple) {
	          ParsleyUtils__default.warn('To be bound by Parsley, a radio, a checkbox and a multiple select input must have either a name or a multiple option.', this.$element);
	          return this;
	        }

	      // Remove special chars
	      this.options.multiple = this.options.multiple.replace(/(:|\.|\[|\]|\{|\}|\$)/g, '');

	      // Add proper `data-parsley-multiple` to siblings if we have a valid multiple name
	      if ('undefined' !== typeof name) {
	        $('input[name="' + name + '"]').each(function (i, input) {
	          if ($(input).is('input[type=radio], input[type=checkbox]')) $(input).attr(_this12.options.namespace + 'multiple', _this12.options.multiple);
	        });
	      }

	      // Check here if we don't already have a related multiple instance saved
	      var $previouslyRelated = this._findRelated();
	      for (var i = 0; i < $previouslyRelated.length; i++) {
	        parsleyMultipleInstance = $($previouslyRelated.get(i)).data('Parsley');
	        if ('undefined' !== typeof parsleyMultipleInstance) {

	          if (!this.$element.data('ParsleyFieldMultiple')) {
	            parsleyMultipleInstance.addElement(this.$element);
	          }

	          break;
	        }
	      }

	      // Create a secret ParsleyField instance for every multiple field. It will be stored in `data('ParsleyFieldMultiple')`
	      // And will be useful later to access classic `ParsleyField` stuff while being in a `ParsleyFieldMultiple` instance
	      this.bind('parsleyField', true);

	      return parsleyMultipleInstance || this.bind('parsleyFieldMultiple');
	    },

	    // Return proper `ParsleyForm`, `ParsleyField` or `ParsleyFieldMultiple`
	    bind: function bind(type, doNotStore) {
	      var parsleyInstance;

	      switch (type) {
	        case 'parsleyForm':
	          parsleyInstance = $.extend(new ParsleyForm(this.$element, this.domOptions, this.options), new ParsleyAbstract(), window.ParsleyExtend)._bindFields();
	          break;
	        case 'parsleyField':
	          parsleyInstance = $.extend(new parsley_field(this.$element, this.domOptions, this.options, this.parent), new ParsleyAbstract(), window.ParsleyExtend);
	          break;
	        case 'parsleyFieldMultiple':
	          parsleyInstance = $.extend(new parsley_field(this.$element, this.domOptions, this.options, this.parent), new ParsleyMultiple(), new ParsleyAbstract(), window.ParsleyExtend)._init();
	          break;
	        default:
	          throw new Error(type + 'is not a supported Parsley type');
	      }

	      if (this.options.multiple) ParsleyUtils__default.setAttr(this.$element, this.options.namespace, 'multiple', this.options.multiple);

	      if ('undefined' !== typeof doNotStore) {
	        this.$element.data('ParsleyFieldMultiple', parsleyInstance);

	        return parsleyInstance;
	      }

	      // Store the freshly bound instance in a DOM element for later access using jQuery `data()`
	      this.$element.data('Parsley', parsleyInstance);

	      // Tell the world we have a new ParsleyForm or ParsleyField instance!
	      parsleyInstance._actualizeTriggers();
	      parsleyInstance._trigger('init');

	      return parsleyInstance;
	    }
	  };

	  var vernums = $.fn.jquery.split('.');
	  if (parseInt(vernums[0]) <= 1 && parseInt(vernums[1]) < 8) {
	    throw "The loaded version of jQuery is too old. Please upgrade to 1.8.x or better.";
	  }
	  if (!vernums.forEach) {
	    ParsleyUtils__default.warn('Parsley requires ES5 to run properly. Please include https://github.com/es-shims/es5-shim');
	  }
	  // Inherit `on`, `off` & `trigger` to Parsley:
	  var Parsley = $.extend(new ParsleyAbstract(), {
	    $element: $(document),
	    actualizeOptions: null,
	    _resetOptions: null,
	    Factory: ParsleyFactory,
	    version: '2.3.13'
	  });

	  // Supplement ParsleyField and Form with ParsleyAbstract
	  // This way, the constructors will have access to those methods
	  $.extend(parsley_field.prototype, ParsleyUI.Field, ParsleyAbstract.prototype);
	  $.extend(ParsleyForm.prototype, ParsleyUI.Form, ParsleyAbstract.prototype);
	  // Inherit actualizeOptions and _resetOptions:
	  $.extend(ParsleyFactory.prototype, ParsleyAbstract.prototype);

	  // ### jQuery API
	  // `$('.elem').parsley(options)` or `$('.elem').psly(options)`
	  $.fn.parsley = $.fn.psly = function (options) {
	    if (this.length > 1) {
	      var instances = [];

	      this.each(function () {
	        instances.push($(this).parsley(options));
	      });

	      return instances;
	    }

	    // Return undefined if applied to non existing DOM element
	    if (!$(this).length) {
	      ParsleyUtils__default.warn('You must bind Parsley on an existing element.');

	      return;
	    }

	    return new ParsleyFactory(this, options);
	  };

	  // ### ParsleyField and ParsleyForm extension
	  // Ensure the extension is now defined if it wasn't previously
	  if ('undefined' === typeof window.ParsleyExtend) window.ParsleyExtend = {};

	  // ### Parsley config
	  // Inherit from ParsleyDefault, and copy over any existing values
	  Parsley.options = $.extend(ParsleyUtils__default.objectCreate(ParsleyDefaults), window.ParsleyConfig);
	  window.ParsleyConfig = Parsley.options; // Old way of accessing global options

	  // ### Globals
	  window.Parsley = window.psly = Parsley;
	  window.ParsleyUtils = ParsleyUtils__default;

	  // ### Define methods that forward to the registry, and deprecate all access except through window.Parsley
	  var registry = window.Parsley._validatorRegistry = new ParsleyValidatorRegistry(window.ParsleyConfig.validators, window.ParsleyConfig.i18n);
	  window.ParsleyValidator = {};
	  $.each('setLocale addCatalog addMessage addMessages getErrorMessage formatMessage addValidator updateValidator removeValidator'.split(' '), function (i, method) {
	    window.Parsley[method] = $.proxy(registry, method);
	    window.ParsleyValidator[method] = function () {
	      var _window$Parsley;

	      ParsleyUtils__default.warnOnce('Accessing the method \'' + method + '\' through ParsleyValidator is deprecated. Simply call \'window.Parsley.' + method + '(...)\'');
	      return (_window$Parsley = window.Parsley)[method].apply(_window$Parsley, arguments);
	    };
	  });

	  // ### ParsleyUI
	  // Deprecated global object
	  window.Parsley.UI = ParsleyUI;
	  window.ParsleyUI = {
	    removeError: function removeError(instance, name, doNotUpdateClass) {
	      var updateClass = true !== doNotUpdateClass;
	      ParsleyUtils__default.warnOnce('Accessing ParsleyUI is deprecated. Call \'removeError\' on the instance directly. Please comment in issue 1073 as to your need to call this method.');
	      return instance.removeError(name, { updateClass: updateClass });
	    },
	    getErrorsMessages: function getErrorsMessages(instance) {
	      ParsleyUtils__default.warnOnce('Accessing ParsleyUI is deprecated. Call \'getErrorsMessages\' on the instance directly.');
	      return instance.getErrorsMessages();
	    }
	  };
	  $.each('addError updateError'.split(' '), function (i, method) {
	    window.ParsleyUI[method] = function (instance, name, message, assert, doNotUpdateClass) {
	      var updateClass = true !== doNotUpdateClass;
	      ParsleyUtils__default.warnOnce('Accessing ParsleyUI is deprecated. Call \'' + method + '\' on the instance directly. Please comment in issue 1073 as to your need to call this method.');
	      return instance[method](name, { message: message, assert: assert, updateClass: updateClass });
	    };
	  });

	  // ### PARSLEY auto-binding
	  // Prevent it by setting `ParsleyConfig.autoBind` to `false`
	  if (false !== window.ParsleyConfig.autoBind) {
	    $(function () {
	      // Works only on `data-parsley-validate`.
	      if ($('[data-parsley-validate]').length) $('[data-parsley-validate]').parsley();
	    });
	  }

	  var o = $({});
	  var deprecated = function deprecated() {
	    ParsleyUtils__default.warnOnce("Parsley's pubsub module is deprecated; use the 'on' and 'off' methods on parsley instances or window.Parsley");
	  };

	  // Returns an event handler that calls `fn` with the arguments it expects
	  function adapt(fn, context) {
	    // Store to allow unbinding
	    if (!fn.parsleyAdaptedCallback) {
	      fn.parsleyAdaptedCallback = function () {
	        var args = Array.prototype.slice.call(arguments, 0);
	        args.unshift(this);
	        fn.apply(context || o, args);
	      };
	    }
	    return fn.parsleyAdaptedCallback;
	  }

	  var eventPrefix = 'parsley:';
	  // Converts 'parsley:form:validate' into 'form:validate'
	  function eventName(name) {
	    if (name.lastIndexOf(eventPrefix, 0) === 0) return name.substr(eventPrefix.length);
	    return name;
	  }

	  // $.listen is deprecated. Use Parsley.on instead.
	  $.listen = function (name, callback) {
	    var context;
	    deprecated();
	    if ('object' === typeof arguments[1] && 'function' === typeof arguments[2]) {
	      context = arguments[1];
	      callback = arguments[2];
	    }

	    if ('function' !== typeof callback) throw new Error('Wrong parameters');

	    window.Parsley.on(eventName(name), adapt(callback, context));
	  };

	  $.listenTo = function (instance, name, fn) {
	    deprecated();
	    if (!(instance instanceof parsley_field) && !(instance instanceof ParsleyForm)) throw new Error('Must give Parsley instance');

	    if ('string' !== typeof name || 'function' !== typeof fn) throw new Error('Wrong parameters');

	    instance.on(eventName(name), adapt(fn));
	  };

	  $.unsubscribe = function (name, fn) {
	    deprecated();
	    if ('string' !== typeof name || 'function' !== typeof fn) throw new Error('Wrong arguments');
	    window.Parsley.off(eventName(name), fn.parsleyAdaptedCallback);
	  };

	  $.unsubscribeTo = function (instance, name) {
	    deprecated();
	    if (!(instance instanceof parsley_field) && !(instance instanceof ParsleyForm)) throw new Error('Must give Parsley instance');
	    instance.off(eventName(name));
	  };

	  $.unsubscribeAll = function (name) {
	    deprecated();
	    window.Parsley.off(eventName(name));
	    $('form,input,textarea,select').each(function () {
	      var instance = $(this).data('Parsley');
	      if (instance) {
	        instance.off(eventName(name));
	      }
	    });
	  };

	  // $.emit is deprecated. Use jQuery events instead.
	  $.emit = function (name, instance) {
	    var _instance;

	    deprecated();
	    var instanceGiven = instance instanceof parsley_field || instance instanceof ParsleyForm;
	    var args = Array.prototype.slice.call(arguments, instanceGiven ? 2 : 1);
	    args.unshift(eventName(name));
	    if (!instanceGiven) {
	      instance = window.Parsley;
	    }
	    (_instance = instance).trigger.apply(_instance, _toConsumableArray(args));
	  };

	  var pubsub = {};

	  $.extend(true, Parsley, {
	    asyncValidators: {
	      'default': {
	        fn: function fn(xhr) {
	          // By default, only status 2xx are deemed successful.
	          // Note: we use status instead of state() because responses with status 200
	          // but invalid messages (e.g. an empty body for content type set to JSON) will
	          // result in state() === 'rejected'.
	          return xhr.status >= 200 && xhr.status < 300;
	        },
	        url: false
	      },
	      reverse: {
	        fn: function fn(xhr) {
	          // If reverse option is set, a failing ajax request is considered successful
	          return xhr.status < 200 || xhr.status >= 300;
	        },
	        url: false
	      }
	    },

	    addAsyncValidator: function addAsyncValidator(name, fn, url, options) {
	      Parsley.asyncValidators[name] = {
	        fn: fn,
	        url: url || false,
	        options: options || {}
	      };

	      return this;
	    }

	  });

	  Parsley.addValidator('remote', {
	    requirementType: {
	      '': 'string',
	      'validator': 'string',
	      'reverse': 'boolean',
	      'options': 'object'
	    },

	    validateString: function validateString(value, url, options, instance) {
	      var data = {};
	      var ajaxOptions;
	      var csr;
	      var validator = options.validator || (true === options.reverse ? 'reverse' : 'default');

	      if ('undefined' === typeof Parsley.asyncValidators[validator]) throw new Error('Calling an undefined async validator: `' + validator + '`');

	      url = Parsley.asyncValidators[validator].url || url;

	      // Fill current value
	      if (url.indexOf('{value}') > -1) {
	        url = url.replace('{value}', encodeURIComponent(value));
	      } else {
	        data[instance.$element.attr('name') || instance.$element.attr('id')] = value;
	      }

	      // Merge options passed in from the function with the ones in the attribute
	      var remoteOptions = $.extend(true, options.options || {}, Parsley.asyncValidators[validator].options);

	      // All `$.ajax(options)` could be overridden or extended directly from DOM in `data-parsley-remote-options`
	      ajaxOptions = $.extend(true, {}, {
	        url: url,
	        data: data,
	        type: 'GET'
	      }, remoteOptions);

	      // Generate store key based on ajax options
	      instance.trigger('field:ajaxoptions', instance, ajaxOptions);

	      csr = $.param(ajaxOptions);

	      // Initialise querry cache
	      if ('undefined' === typeof Parsley._remoteCache) Parsley._remoteCache = {};

	      // Try to retrieve stored xhr
	      var xhr = Parsley._remoteCache[csr] = Parsley._remoteCache[csr] || $.ajax(ajaxOptions);

	      var handleXhr = function handleXhr() {
	        var result = Parsley.asyncValidators[validator].fn.call(instance, xhr, url, options);
	        if (!result) // Map falsy results to rejected promise
	          result = $.Deferred().reject();
	        return $.when(result);
	      };

	      return xhr.then(handleXhr, handleXhr);
	    },

	    priority: -1
	  });

	  Parsley.on('form:submit', function () {
	    Parsley._remoteCache = {};
	  });

	  window.ParsleyExtend.addAsyncValidator = function () {
	    ParsleyUtils.warnOnce('Accessing the method `addAsyncValidator` through an instance is deprecated. Simply call `Parsley.addAsyncValidator(...)`');
	    return Parsley.addAsyncValidator.apply(Parsley, arguments);
	  };

	  // This is included with the Parsley library itself,
	  // thus there is no use in adding it to your project.
	  Parsley.addMessages('en', {
	    defaultMessage: "This value seems to be invalid.",
	    type: {
	      email: "This value should be a valid email.",
	      url: "This value should be a valid url.",
	      number: "This value should be a valid number.",
	      integer: "This value should be a valid integer.",
	      digits: "This value should be digits.",
	      alphanum: "This value should be alphanumeric."
	    },
	    notblank: "This value should not be blank.",
	    required: "This value is required.",
	    pattern: "This value seems to be invalid.",
	    min: "This value should be greater than or equal to %s.",
	    max: "This value should be lower than or equal to %s.",
	    range: "This value should be between %s and %s.",
	    minlength: "This value is too short. It should have %s characters or more.",
	    maxlength: "This value is too long. It should have %s characters or fewer.",
	    length: "This value length is invalid. It should be between %s and %s characters long.",
	    mincheck: "You must select at least %s choices.",
	    maxcheck: "You must select %s choices or fewer.",
	    check: "You must select between %s and %s choices.",
	    equalto: "This value should be the same."
	  });

	  Parsley.setLocale('en');

	  /**
	   * inputevent - Alleviate browser bugs for input events
	   * https://github.com/marcandre/inputevent
	   * @version v0.0.3 - (built Thu, Apr 14th 2016, 5:58 pm)
	   * @author Marc-Andre Lafortune <github@marc-andre.ca>
	   * @license MIT
	   */

	  function InputEvent() {
	    var _this13 = this;

	    var globals = window || global;

	    // Slightly odd way construct our object. This way methods are force bound.
	    // Used to test for duplicate library.
	    $.extend(this, {

	      // For browsers that do not support isTrusted, assumes event is native.
	      isNativeEvent: function isNativeEvent(evt) {
	        return evt.originalEvent && evt.originalEvent.isTrusted !== false;
	      },

	      fakeInputEvent: function fakeInputEvent(evt) {
	        if (_this13.isNativeEvent(evt)) {
	          $(evt.target).trigger('input');
	        }
	      },

	      misbehaves: function misbehaves(evt) {
	        if (_this13.isNativeEvent(evt)) {
	          _this13.behavesOk(evt);
	          $(document).on('change.inputevent', evt.data.selector, _this13.fakeInputEvent);
	          _this13.fakeInputEvent(evt);
	        }
	      },

	      behavesOk: function behavesOk(evt) {
	        if (_this13.isNativeEvent(evt)) {
	          $(document) // Simply unbinds the testing handler
	          .off('input.inputevent', evt.data.selector, _this13.behavesOk).off('change.inputevent', evt.data.selector, _this13.misbehaves);
	        }
	      },

	      // Bind the testing handlers
	      install: function install() {
	        if (globals.inputEventPatched) {
	          return;
	        }
	        globals.inputEventPatched = '0.0.3';
	        var _arr = ['select', 'input[type="checkbox"]', 'input[type="radio"]', 'input[type="file"]'];
	        for (var _i = 0; _i < _arr.length; _i++) {
	          var selector = _arr[_i];
	          $(document).on('input.inputevent', selector, { selector: selector }, _this13.behavesOk).on('change.inputevent', selector, { selector: selector }, _this13.misbehaves);
	        }
	      },

	      uninstall: function uninstall() {
	        delete globals.inputEventPatched;
	        $(document).off('.inputevent');
	      }

	    });
	  };

	  var inputevent = new InputEvent();

	  inputevent.install();

	  var parsley = Parsley;

	  return parsley;
	});
	//# sourceMappingURL=parsley.js.map

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 87 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(53);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(4)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../css-loader/index.js!./parsley.css", function() {
				var newContent = require("!!./../../css-loader/index.js!./parsley.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(59);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(4)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./daterangepicker.min.css", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./daterangepicker.min.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */
/***/ function(module, exports) {

	module.exports = function() { throw new Error("define cannot be used indirect"); };


/***/ },
/* 104 */
/***/ function(module, exports) {

	module.exports = function(module) {
		if(!module.webpackPolyfill) {
			module.deprecate = function() {};
			module.paths = [];
			// module.parent = undefined by default
			module.children = [];
			module.webpackPolyfill = 1;
		}
		return module;
	}


/***/ },
/* 105 */
/***/ function(module, exports, __webpack_require__) {

	var map = {
		"./controller": 5,
		"./controller.js": 5,
		"./dashboard-controller": 18,
		"./dashboard-controller.js": 18,
		"./login-controller": 19,
		"./login-controller.js": 19,
		"./priority-lists-controller": 20,
		"./priority-lists-controller.js": 20,
		"./products-controller": 21,
		"./products-controller.js": 21,
		"./retail-chains-controller": 22,
		"./retail-chains-controller.js": 22,
		"./sale-points-controller": 23,
		"./sale-points-controller.js": 23,
		"./zones-controller": 24,
		"./zones-controller.js": 24
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 105;


/***/ }
]);