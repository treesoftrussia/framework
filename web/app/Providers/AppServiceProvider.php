<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        //TODO created Validators in separates files
        Validator::extend('latitude', function ($attribute, $value, $parameters, $validator) {
            $pattern = '/^(\+|-)?(?:90(?:(?:\.0{1,7})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,7})?))$/';

            return preg_match($pattern, $value) === 1;
        });
        Validator::replacer('latitude', function ($message, $attribute, $rule, $parameters) {
            return trans('api.latitude_validation');
        });

        Validator::extend('longitude', function ($attribute, $value, $parameters, $validator) {
            $pattern = '/^(\+|-)?(?:180(?:(?:\.0{1,7})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,7})?))$/';

            return preg_match($pattern, $value) === 1;
        });
        Validator::replacer('longitude', function ($message, $attribute, $rule, $parameters) {
            return trans('api.longitude_validation');
        });

        Validator::extend('int_array', function ($attribute, $value, $parameters) {
            foreach ($value as $v) {
                if (!is_int($v)) {
                    return false;
                }
            }

            return true;
        });
        Validator::replacer('int_array', function ($message, $attribute, $rule, $parameters) {
            return trans('api.int_array_validation', ['attribute' => $attribute]);
        });

        Validator::replacer('required', function ($message, $attribute, $rule, $parameters) {
            switch ($attribute) {
                case 'coordinates.ltd':
                    return trans('api.coordinates_ltd_required_validation');
                case 'coordinates.lng':
                    return trans('api.coordinates_lng_required_validation');
            }

            return trans('api.int_array_validation', ['attribute' => $attribute]);
        });
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        //
    }
}
