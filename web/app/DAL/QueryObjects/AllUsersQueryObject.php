<?php
namespace App\DAL\QueryObjects;

use App\Core\Support\Options\UserListOptions;
use App\DAL\Models\SalePoint;
use App\DAL\Models\User;

class AllUsersQueryObject
{
    public function build(UserListOptions $options)
    {
        $query = User::query();

        if($options->getIdentificators())
        {
            $query->whereIn('id',$options->getIdentificators());
        }

        if ($options->getSortingOptions()) {
            $query = $query->orderBy($options->getSortingOptions()->getField(), $options->getSortingOptions()->getDirection());
        }
        
        if ($options->getSearchPhrase()) {
            $phrase = $options->getSearchPhrase();
            $query = $query->where(function ($query) use ($phrase) {
                $query
                    ->where('username', 'LIKE', '%' . $phrase . '%')
                    ->orWhere('email', 'LIKE', '%' . $phrase . '%')
                    ->orWhere('phone', 'LIKE', '%' . $phrase . '%')
                    ->orWhere('id', 'LIKE', '%' . $phrase . '%');
            });
        }

        return $query;
    }
}
