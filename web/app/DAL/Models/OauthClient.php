<?php
namespace App\DAL\Models;

use App\Libraries\Elegant\Elegant;

class OauthClient extends Elegant
{
    protected $table = 'oauth_clients';


    public function scopes()
    {
        return $this->belongsToMany(Scope::class, 'oauth_client_scopes', 'client_id', 'scope_id');
    }
}