<?php
namespace App\DAL\Models;

use App\Libraries\Elegant\Elegant;

class Scope extends Elegant
{
    protected $table = 'oauth_scopes';

    public $incrementing = false;


    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_oauth_scopes', 'oauth_scope_id', 'role_id');
    }


    public function oauthClients()
    {
        return $this->belongsToMany(OauthClient::class, 'oauth_client_scopes', 'scope_id', 'client_id');
    }
}