<?php namespace App\DAL\Models;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use App\Libraries\Elegant\Elegant;
use Illuminate\Auth\Authenticatable;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class User extends Elegant implements AuthenticatableContract
{
    use Authenticatable;

    protected $table = 'users';
    protected $primaryKey = 'id';
    
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    }
}