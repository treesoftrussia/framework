<?php
namespace App\DAL\Models;

use App\Libraries\Elegant\Elegant;

class Role extends Elegant
{
    protected $table = 'roles';
    
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_roles', 'role_id', 'user_id');
    }


    public function scopes()
    {
        return $this->belongsToMany(Scope::class, 'role_oauth_scopes', 'role_id', 'oauth_scope_id');
    }

}