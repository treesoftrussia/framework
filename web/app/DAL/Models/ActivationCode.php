<?php
namespace App\DAL\Models;

use App\Libraries\Elegant\Elegant;

class ActivationCode extends Elegant
{
    protected $table = 'activation_codes';

    protected $primaryKey = 'phone';

    public $incrementing = false;

    public $timestamps = false;
}