<?php namespace App\DAL\Models;

use App\Libraries\Elegant\Elegant;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class UserSocialNetwork extends Elegant
{
    protected $table = 'user_social_networks_data';
    protected $primaryKey = 'id';
    public $timestamps = false;
}