<?php namespace App\DAL\Models;

use App\Libraries\Elegant\Elegant;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestChild extends Elegant
{
    protected $table = 'test_children_t';
    protected $primaryKey = 'id';
    public $timestamps = false;
}