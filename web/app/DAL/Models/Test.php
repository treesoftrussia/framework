<?php namespace App\DAL\Models;

use App\Libraries\Elegant\Elegant;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class Test extends Elegant
{
    protected $table = 'tests_t';
    protected $primaryKey = 'id';
    public $timestamps = false;
}