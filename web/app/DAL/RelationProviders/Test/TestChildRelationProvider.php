<?php namespace App\DAL\RelationProviders\Test;

use Illuminate\Database\Eloquent\Collection;
use App\DAL\Models\TestChild;
use App\DAL\RelationProviders\AbstractRelationProvider;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestChildRelationProvider extends AbstractRelationProvider
{
    /**
     * @param $test
     * @return Collection
     */
    public function execute($test){
        return TestChild::whereTestId($test->id)->get();
    }

    /**
     * @param $models
     * @return Collection
     */
    public function executeEager($models){
        return TestChild::whereIn('test_id', array_pluck($models, 'id'))->get();
    }

}