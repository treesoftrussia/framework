<?php namespace App\DAL\RelationProviders;
use App\Libraries\Elegant\Collection;
use App\Libraries\Elegant\Elegant;
use RuntimeException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
abstract class AbstractRelationProvider
{
    const ONE_TO_MANY = 1;

    private $type;
    private $cache;
    private $eagerModels;

    private static $relationCache = [];

    public function __construct($relationType, $eagerModels = null)
    {
        $this->type = $relationType;
        $this->eagerModels = $eagerModels;
    }

    public static function get($relationType, $eagerModels = null){
        $class = get_called_class();

        $hash = sha1($class.$relationType.(!is_null($eagerModels) ? spl_object_hash($eagerModels) : ''));

        if(isset(self::$relationCache[$hash])){
            return self::$relationCache[$hash];
        }

        self::$relationCache[$hash] = new static($relationType, $eagerModels);

        return self::$relationCache[$hash];
    }

    /**
     * Provides relation for specified $model
     *
     * @param $model
     * @return mixed
     */
    public function provide($model)
    {
        if (isset($this->cache[$model->id])) {
            return new Collection($this->cache[$model->id]);
        } else {
            if (method_exists($this, 'executeEager') && !empty($this->eagerModels) && $this->type == self::ONE_TO_MANY) {

                $result = $this->executeEager($this->eagerModels);

                if(is_array($result)){
                    $this->cache = $result;

                    foreach($this->eagerModels as $eagerModel){
                        if(empty($this->cache[$eagerModel->id])){
                            $this->cache[$eagerModel->id] = [];
                        }
                    }

                    return  new Collection($this->cache[$model->id]);
                }

                throw new RuntimeException('Not supported related entity - '.get_class($result));

            } else {
                return $this->execute($model);
            }
        }
    }

    public abstract function execute($model);

}