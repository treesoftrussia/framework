<?php

namespace App\DAL\FlattenAdapters;

use App\Libraries\Adapter\AbstractAdapter;
use App\Core\Test\Entity\Test;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestFlattenAdapter extends AbstractAdapter
{
    /**
     * @param Test $test
     *
     * @return array
     */
    public function transform($test = null)
    {
        $santinizer = $this->sanitizer()->make();

        return skip_empty([
            'name' => $santinizer($test->getName(), 'string'),
            'description' => $santinizer($test->getDescription(), 'string'),
        ], [null]);
    }
}
