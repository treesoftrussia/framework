<?php namespace App\DAL\FlattenAdapters;

use App\Core\ClientManagement\Entity\Users\SocialNetworkProfile;
use App\Libraries\Adapter\AbstractAdapter;


class UserSocialNetworkFlattenAdapter extends AbstractAdapter
{
    
    /**
     * @param SocialNetworkProfile $profile
     * @return array
     */
    public function transform($profile = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return skip_empty([
            'internal_user_id' => $sanitizer($profile->getInternalId(), 'string'),
            'type' => $sanitizer($profile->getType(), 'string'),
            'user_id' => $sanitizer($profile->getUserId(), 'integer')
        ], [null]);
    }
}