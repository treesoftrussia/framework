<?php

namespace App\DAL\FlattenAdapters;

use App\Core\Test\Entity\TestChild;
use App\Libraries\Adapter\AbstractAdapter;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestChildFlattenAdapter extends AbstractAdapter
{
    public function transform($test = null)
    {
        $santinizer = $this->sanitizer()->make();

        return skip_empty([
            'id' => $santinizer($test->getId(), 'integer'),
            'title' => $santinizer($test->getTitle(), 'string'),
        ], [null]);
    }
}
