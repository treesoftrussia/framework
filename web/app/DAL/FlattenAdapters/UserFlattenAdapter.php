<?php namespace App\DAL\FlattenAdapters;

use App\Core\ClientManagement\Entity\Users\User;
use App\Libraries\Adapter\AbstractAdapter;


class UserFlattenAdapter extends AbstractAdapter
{
    /**
     * @param User $user
     * @return array
     */
    public function transform($user = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return skip_empty([
            'email' => $sanitizer($user->getEmail(), 'string'),
            'username' => $sanitizer($user->getUsername(), 'string'),
            'phone' => $sanitizer($user->getPhone(), 'string'),
            'password' => $sanitizer($user->getPasswordHash(), 'string')
        ], [null]);
    }
}