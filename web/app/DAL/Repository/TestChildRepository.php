<?php namespace App\DAL\Repository;

use App\Core\Test\Interfaces\TestChildRepositoryInterface;
use App\DAL\Adapters\TestChildAdapter;
use App\Core\Test\Entity\TestChild as TestChildEntity;
use App\DAL\FlattenAdapters\TestChildFlattenAdapter;
use App\DAL\Models\TestChild;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestChildRepository implements TestChildRepositoryInterface
{
    /**
     * @param \App\Core\Test\Entity\TestChild $testChild
     * @return \App\Core\Test\Entity\TestChild $testChild
     */
    public function create(TestChildEntity $testChild){

        $model = TestChild::create((new TestChildFlattenAdapter())->transform($testChild));

        /**
         * @var \App\Core\Test\Entity\TestChild
         */
        return (new TestChildAdapter())->transform($model);
    }
}