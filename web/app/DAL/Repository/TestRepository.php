<?php namespace App\DAL\Repository;

use Illuminate\Support\Collection;
use App\Core\Test\Interfaces\TestRepositoryInterface;
use App\DAL\Adapters\TestChildAdapter;
use App\DAL\Models\Test;
use App\DAL\Adapters\TestAdapter;
use App\Core\Test\Entity\Test as TestEntity;
use App\DAL\FlattenAdapters\TestFlattenAdapter;
use App\Core\Support\Container\Container;
use App\DAL\Models\TestChild;
use App\Core\Test\Interfaces\TestChildRepositoryInterface;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestRepository implements TestRepositoryInterface
{
    /**
     * @var Container $container
     */
    private $container;

    public function __construct(Container $container){
        $this->container = $container;
    }

    /**
     * @param $id
     * @return \App\Core\Test\Entity\Test;
     */
    public function get($id){

        $testEntry = Test::find($id);

        if($testEntry === null){
            return null;
        }

        /**
         * @var \App\Core\Test\Entity\Test
         */
        return (new TestAdapter())->transform($testEntry);
    }

    /**
     * @param TestEntity $test
     * @return TestEntity
     */
    public function create(TestEntity $test){

        $model = Test::create((new TestFlattenAdapter())->transform($test));

        $testChildRepository = $this->container->get(TestChildRepositoryInterface::class);

        $testEntity = (new TestAdapter())->transform($model);

        $children = new Collection();

        foreach($test->getChildren() as $child){
            $childEntity = $testChildRepository->create($child);
            $childEntity->setTestId($testEntity->getId());

            $children->push($childEntity);
        }

        $testEntity->setChildren($children);

        return $testEntity;
    }

    /**
     * @param int $id
     * @param TestEntity $test
     * @return null
     */
    public function update($id, TestEntity $test){
        return Test::refresh($id, (new TestFlattenAdapter())->transform($test));
    }

    /**
     * @param $id
     * @return boolean
     */
    public function delete($id)
    {
        Test::whereId($id)->delete();

        return null;
    }
    /**
     * @param $id
     * @return boolean
     */
    public function exists($id)
    {
        return Test::whereId($id)->exists();
    }
}