<?php

namespace App\DAL\Repository;

use App\Core\ClientManagement\Entity\Users\SocialNetworkProfile;
use App\Core\ClientManagement\Interfaces\UserRepositoryInterface;
use App\Core\Support\Options\UserListOptions;
use App\Core\Support\Options\UserOptions;
use App\DAL\Adapters\ActivationCodeAdapter;
use App\DAL\Adapters\RoleAdapter;
use App\DAL\Adapters\SocialNetworkProfileAdapter;
use App\DAL\Adapters\UserAdapter;
use App\DAL\FlattenAdapters\ActivationCodeFlattenAdapter;
use App\DAL\FlattenAdapters\UserFlattenAdapter;
use App\DAL\FlattenAdapters\UserSocialNetworkFlattenAdapter;
use App\DAL\Models\ActivationCode;
use App\DAL\Models\Role;
use App\DAL\Models\User;
use App\Core\ClientManagement\Entity\Users\User as UserEntity;
use App\Core\ClientManagement\Entity\Support\ActivationCode as ActivationCodeEntity;
use App\DAL\Models\UserSocialNetwork;
use App\DAL\QueryObjects\AllUsersQueryObject;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @param $id
     *
     * @return \App\Core\ClientManagement\Entity\Users\BaseUser|null
     */
    public function getUser($id)
    {
        $userEntry = User::where('id', $id)->with('roles')->first();
        $entity = new UserEntity();

        return is_null($userEntry) ? null : (new UserAdapter())->transform($userEntry, $entity);
    }

    public function delete($id)
    {
        User::where('id', $id)->delete();
    }
    

    /**
     * @param $login
     *
     * @return UserEntity|null
     */
    public function getUserByCredentials($login)
    {
        $userEntry = User::orWhere('email', $login)->orWhere('phone', $login)->orWhere('username', $login)->with('roles')->first();
        $entity = new UserEntity();

        return is_null($userEntry) ? null : (new UserAdapter())->transform($userEntry, $entity);
    }

    /**
     * @param UserEntity $user
     *
     * @return \App\Core\ClientManagement\Entity\Users\BaseUser
     */
    public function create(UserEntity $user)
    {
        $createdUser = User::create((new UserFlattenAdapter())->transform($user));

        $userEntry = User::find($createdUser->id);

        if ($userEntry) {
            $entity = new UserEntity();
            $userEntity = (new UserAdapter())->transform($userEntry, $entity);
            $userEntity->setPassword($user->getPassword());

            return $userEntity;
        } else {
            return false;
        }
    }

    /**
     * @param UserEntity $userEntity
     * @param array $roleIds
     *
     * @return UserEntity|bool
     */
    public function setRoles(UserEntity $userEntity, array $roleIds)
    {
        $roleEntries = Role::whereIn('machine_name', $roleIds)->get();
        if (!empty($roleEntries)) {
            $userEntry = User::find($userEntity->getId());
            $userEntry->roles()->attach(array_pluck($roleEntries, 'id'));

            $userEntity->setRoles((new RoleAdapter())->transformCollection($roleEntries));

            return $userEntity;
        } else {
            return false;
        }
    }

    public function update($id, UserEntity $user)
    {
        User::refresh($id, (new UserFlattenAdapter($user))->transform($user));

        return $this->getUser($id)->setPassword($user->getPassword());
    }
    
    public function checkOnExistUserByPhone($phone)
    {
        return User::wherePhone($phone)->first() ? true : false;
    }

    public function checkOnExistUserByEmail($email)
    {
        return User::whereEmail($email)->first() ? true : false;
    }

    public function getActivationCodeByPhone($phone)
    {
        $activationCodeEntry = ActivationCode::wherePhone($phone)->first();

        return is_null($activationCodeEntry) ? $activationCodeEntry : (new ActivationCodeAdapter())->transform($activationCodeEntry);
    }

    public function checkOnActivityCode($phone)
    {
        $activationCodeEntry = ActivationCode::where('phone', '=', $phone)
            ->where('expires_at', '>', time())
            ->first();

        return is_null($activationCodeEntry) ? false : true;
    }

    public function checkActivationCodeOnAvailabilityForRefresh($phone)
    {
        $activationCodeEntry = ActivationCode::where('phone', '=', $phone)
            ->where('refresh_at', '<', time())
            ->first();

        return is_null($activationCodeEntry) ? false : true;
    }

    public function deleteActivationCodeByPhone($phone)
    {
        ActivationCode::where('phone', '=', $phone)->delete();
    }

    public function checkOnActivityAndExistenceActivationCode($phone, $activationCode)
    {
        $activationCodeEntry = ActivationCode::where('phone', '=', $phone)
            ->where('expires_at', '>', time())
            ->where('activation_code', '=', $activationCode)
            ->first();

        return is_null($activationCodeEntry) ? false : true;
    }

    public function createOrUpdateActivationCode(ActivationCodeEntity $activationCodeEntity)
    {
        $activationCodeExist = ActivationCode::find($activationCodeEntity->getPhone());

        if (is_null($activationCodeExist)) {
            ActivationCode::create((new ActivationCodeFlattenAdapter())->transform($activationCodeEntity));
        } else {
            ActivationCode::refresh($activationCodeEntity->getPhone(), (new ActivationCodeFlattenAdapter())->transform($activationCodeEntity));
        }

        $activationCodeEntry = ActivationCode::find($activationCodeEntity->getPhone());

        if ($activationCodeEntry) {
            $activationCodeEntity = (new ActivationCodeAdapter())->transform($activationCodeEntry);

            return $activationCodeEntity;
        }

        return false;
    }

    /**
     * @param $phone
     * @param $passwordHash
     * @return mixed
     */
    public function updatePasswordByPhone($phone, $passwordHash)
    {
        return User::wherePhone($phone)->update(['password' => $passwordHash]);
    }
    

    
    public function getAll(UserListOptions $options)
    {
        $query = (new AllUsersQueryObject())->build($options);

        if ($options->getPaginationOptions()) {
            $query = $query->forPage($options->getPaginationOptions()->getPageNum(), $options->getPaginationOptions()->getPerPage());
        }

        $usersEntries = $query->get();

        return (new UserAdapter())->transformCollection($usersEntries);
    }

    public function getTotal(UserListOptions $options)
    {
        $total = (new AllUsersQueryObject())->build($options)->count();

        return $total;
    }

    public function getUserByEmail($email)
    {
        $userEntry = User::whereEmail($email)->first();

        $userEntity = new UserEntity();

        return is_null($userEntry) ? null : (new UserAdapter())->transform($userEntry, $userEntity);
    }

    public function getRoleIdByName($name)
    {
        return Role::where('machine_name', '=', $name)->value('id');
    }

    public function assignRoleForUser($userId, $roleId)
    {
        $user = User::find($userId);

        $exists = $user->roles->contains($roleId);
        if (!$exists) {
            $user->roles()->attach($roleId);
        }
    }

    /**
     * @param SocialNetworkProfile $socialNetworkProfile
     * @return SocialNetworkProfile
     */
    public function createSocialNetworkProfile(SocialNetworkProfile $socialNetworkProfile){
        $createdSocialNetworkProfileModel = UserSocialNetwork::create((new UserSocialNetworkFlattenAdapter())->transform($socialNetworkProfile));

        return (new SocialNetworkProfileAdapter())->transform(UserSocialNetwork::find($createdSocialNetworkProfileModel->id));
    }

    /**
     * @param UserEntity $user
     * @param SocialNetworkProfile $socialNetworkProfile
     * @param UserOptions|null $options
     */
    public function createOrUpdateIfExistsSocialNetworkEntry(UserEntity $user, SocialNetworkProfile $socialNetworkProfile, UserOptions $options = null){
        $context = $this;
        return DB::transaction(function () use ($context, $user, $socialNetworkProfile, $options) {
            $socialNetworkModel = UserSocialNetwork::whereType($socialNetworkProfile->getType())->whereInternalUserId($socialNetworkProfile->getInternalId())->sharedLock()->first();

            if(!$socialNetworkModel){
                $user = $context->create($user);
                if($options->getWithSocialNetworkEntry()){
                    $socialNetworkProfile->setUserId($user->getId());
                    $context->createSocialNetworkProfile($socialNetworkProfile);
                }
            } else {
                $user = $context->update($socialNetworkModel->user_id, $user);
            }

            return $user;
        });
    }
}
