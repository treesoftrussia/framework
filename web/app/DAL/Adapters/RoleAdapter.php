<?php

namespace App\DAL\Adapters;

use App\Libraries\Adapter\AbstractAdapter;
use App\Core\ClientManagement\Entity\Rights\Role as RoleEntity;

class RoleAdapter extends AbstractAdapter
{

    public function transform($model = null){
        $entity = new RoleEntity();

        $entity->setId($model->id)
            ->setMachineName($model->machine_name)
            ->setScopes((new ScopeAdapter())->transformCollection($model->scopes));

        return $entity;
    }
}