<?php

namespace App\DAL\Adapters;

use App\Libraries\Adapter\AbstractAdapter;
use App\Core\ClientManagement\Entity\Users\User as UserEntity;

class UserAdapter extends AbstractAdapter
{

    /**
     * @param null $model
     * @return UserEntity
     */
    public function transform($model = null){

        $entity = new UserEntity();

        $entity
            ->setId($model->id)
            ->setEmail($model->email)
            ->setUsername($model->username)
            ->setPhone($model->phone)
            ->setPasswordHash($model->password)
            ->setRoles((new RoleAdapter())->transformCollection($model->roles))
            ->setCreatedAt($model->created_at)
            ->setUpdatedAt($model->updated_at);

        return $entity;
    }
}