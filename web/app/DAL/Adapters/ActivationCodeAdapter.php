<?php

namespace App\DAL\Adapters;

use App\DAL\Models\ActivationCode;
use App\Libraries\Adapter\AbstractAdapter;
use App\Core\ClientManagement\Entity\Support\ActivationCode as ActivationCodeEntity;

class ActivationCodeAdapter extends AbstractAdapter
{
    /**
     * @param ActivationCode $model
     * @return ActivationCodeEntity
     */
    public function transform($model = null){
        $entity = new ActivationCodeEntity();
        $entity
            ->setPhone($model->phone)
            ->setActivationCode($model->activation_code)
            ->setExpiresAt($model->expires_at)
            ->setRefreshAt($model->refresh_at);
        return $entity;
    }
}