<?php

namespace App\DAL\Adapters;

use App\Core\ClientManagement\Entity\Users\SocialNetworkProfile;
use App\DAL\Models\UserSocialNetwork;
use App\Libraries\Adapter\AbstractAdapter;

class SocialNetworkProfileAdapter extends AbstractAdapter
{
    
    /**
     * @param UserSocialNetwork $model
     * @return SocialNetworkProfile
     */
    public function transform($model = null){

        $entity = new SocialNetworkProfile();

        $entity->setId($model->id)->setInternalId($model->user_internal_id)->setType($model->type)->setUserId($model->user_id);

        return $entity;
    }
}