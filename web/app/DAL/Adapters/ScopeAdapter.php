<?php

namespace App\DAL\Adapters;

use App\DAL\Models\Scope;
use App\Libraries\Adapter\AbstractAdapter;
use App\Core\ClientManagement\Entity\Rights\Scope as ScopeEntity;

class ScopeAdapter extends AbstractAdapter
{
    /**
     * @param Scope $model
     * @return ScopeEntity
     */
    public function transform($model = null){
        $entity = new ScopeEntity();

        $entity->setId($model->id)
            ->setDescription($model->description);

        return $entity;
    }
}