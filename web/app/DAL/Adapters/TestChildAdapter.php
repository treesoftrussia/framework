<?php

namespace App\DAL\Adapters;

use App\DAL\Models\TestChild;
use App\Libraries\Adapter\AbstractAdapter;
use App\Core\Test\Entity\TestChild as TestChildEntity;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestChildAdapter extends AbstractAdapter
{
    /**
     * @param TestChild $model
     *
     * @return TestChildEntity
     */
    public function transform($model = null)
    {
        $entity = new TestChildEntity();

        $entity->setId($model->id)->setTestId($model->test_id)->setTitle($model->title);

        return $entity;
    }
}
