<?php

namespace App\DAL\Adapters;

use App\DAL\RelationProviders\Test\TestChildRelationProvider;
use App\Libraries\Adapter\AbstractAdapter;
use App\DAL\Models\Test;
use App\Core\Test\Entity\Test as TestEntity;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestAdapter extends AbstractAdapter
{
    /**
     * @param Test $model
     *
     * @return TestEntity
     */
    public function transform($model = null)
    {
        $entity = new TestEntity();
        $entity
            ->setId($model->id)
            ->setName($model->name)
            ->setDescription($model->description)
            ->setChildren((new TestAdapter())->transformCollection((new TestChildRelationProvider(TestChildRelationProvider::ONE_TO_MANY))->provide($model)));

        return $entity;
    }
}
