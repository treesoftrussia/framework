<?php

namespace App\Support\Providers;

use Illuminate\Support\ServiceProvider;
use App\Libraries\Adapter\AbstractAdapter;
use App\Libraries\Adapter\SharedModifiers as AdapterModifiers;
use App\Libraries\Adapter\SharedSanitizers as AdapterSanitizers;
use App\Libraries\Transformer\AbstractTransformer;
use App\Libraries\Transformer\SharedModifiers as TransformerModifiers;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        /*
         * Register shared modifiers  for all adapters
         */
        AbstractAdapter::setSharedModifiersProvider(AbstractAdapter::M_MODIFIER, new AdapterModifiers());

        /*
         * Register shared sanitizers for all adapters
         */
        AbstractAdapter::setSharedModifiersProvider(AbstractAdapter::M_SANITIZER, new AdapterSanitizers());

        /*
         * Registers shared modifiers for all transformers
         */
        AbstractTransformer::setSharedModifiersProvider(new TransformerModifiers());
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        //
    }
}
