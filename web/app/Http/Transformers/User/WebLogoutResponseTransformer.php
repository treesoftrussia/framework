<?php namespace App\Http\Transformers\User;

use App\Core\ClientManagement\Entity\Support\WebLoginResponse;
use App\Core\ClientManagement\Entity\Support\WebLogoutResponse;
use App\Http\Transformers\BaseTransformer;


class WebLogoutResponseTransformer extends BaseTransformer
{
    /**
     * @param WebLogoutResponse $webLogin
     * @return array
     */
    public function transform(WebLogoutResponse $webLogin)
    {
        return $this->extract($webLogin, []);
    }
}