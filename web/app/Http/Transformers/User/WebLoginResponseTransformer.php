<?php namespace App\Http\Transformers\User;

use App\Core\ClientManagement\Entity\Support\WebLoginResponse;
use App\Http\Transformers\BaseTransformer;


class WebLoginResponseTransformer extends BaseTransformer
{
    /**
     * @param WebLoginResponse $webLogin
     * @return array
     */
    public function transform(WebLoginResponse $webLogin)
    {
        return $this->extract($webLogin, []);
    }
}