<?php namespace App\Http\Transformers\User;

use App\Core\ClientManagement\Entity\Support\Token;
use App\Http\Transformers\BaseTransformer;

class TokenEntityTransformer extends BaseTransformer
{
    /**
     * @param Token $token
     * @return array
     */
    public function transform(Token $token)
    {
        return $this->extract($token, [
            'map' => [
                'accessToken' => 'access_token',
                'expiresIn' => 'expires_in',
                'tokenType' => 'token_type'
            ]
        ]);
    }
}