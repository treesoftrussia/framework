<?php

namespace App\Http\Transformers;

use App\Core\Test\Entity\Test;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestTransformer extends BaseTransformer
{
    /**
     * @param Test $test
     *
     * @return array
     */
    public function transform(Test $test)
    {
        return $this->extract($test, ['ignore' => ['children.testId']]);
    }
}
