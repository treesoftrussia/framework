<?php

namespace App\Http\Transformers;

use App\Libraries\Transformer\AbstractTransformer;

/**
 * All reusable Transformer methods should be put into this class. This will make them available to every Transformer.
 *
 * Class BaseTransformer
 */
abstract class BaseTransformer extends AbstractTransformer
{
}
