<?php
namespace App\Http\Security\OAuth;

use League\OAuth2\Server\ResourceServer as LeagueResourceServer;
use League\OAuth2\Server\Exception\InvalidRequestException;

class ResourceServer extends LeagueResourceServer
{
    public function determineAccessToken($headerOnly = false)
    {
        if ($this->getRequest()->headers->get('Authorization') !== null) {
            $accessToken = $this->getTokenType()->determineAccessTokenInHeader($this->getRequest());
        } elseif ($headerOnly === false && (! $this->getTokenType() instanceof MAC)) {
            $accessToken = ($this->getRequest()->server->get('REQUEST_METHOD') === 'GET')
                ? $this->getRequest()->query->get($this->tokenKey)
                : $this->getRequest()->request->get($this->tokenKey);
        } else {
            $accessToken = $this->getRequest()->cookies->get('access_token');
        }


        if (empty($accessToken)) {
            throw new InvalidRequestException('access token');
        }

        return $accessToken;
    }
}