<?php
namespace App\Http\Security\OAuth;

use App\Core\ClientManagement\Services\UserService;
use App\Core\Support\SmsPushers\InvalidActivationCodeException;
use League\OAuth2\Server\Grant\PasswordGrant as BasePasswordGrant;
use League\OAuth2\Server\Exception;
use League\OAuth2\Server\Event;
use League\OAuth2\Server\Entity\ClientEntity;
use League\OAuth2\Server\Entity\SessionEntity;

use League\OAuth2\Server\Entity\AccessTokenEntity;
use League\OAuth2\Server\Util\SecureKey;
use League\OAuth2\Server\Entity\RefreshTokenEntity;

/**
 * Password grant class
 */
class ActivationCodeGrant extends BasePasswordGrant
{
    private $userService;

    /**
     * Grant identifier
     *
     * @var string
     */
    protected $identifier = 'activation_code';

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    /**
     * Complete the password grant
     *
     * @return array
     *
     * @throws
     */
    public function completeFlow()
    {
        // Get the required params
        $clientId = $this->server->getRequest()->request->get('client_id', $this->server->getRequest()->getUser());
        if (is_null($clientId)) {
            throw new Exception\InvalidRequestException('client_id');
        }


        $clientSecret = $this->server->getRequest()->request->get('client_secret',
            $this->server->getRequest()->getPassword());
        if (is_null($clientSecret)) {
            throw new Exception\InvalidRequestException('client_secret');
        }

        // Validate client ID and client secret
        $client = $this->server->getClientStorage()->get(
            $clientId,
            $clientSecret,
            null,
            $this->getIdentifier()
        );

        if (($client instanceof ClientEntity) === false) {
            $this->server->getEventEmitter()->emit(new Event\ClientAuthenticationFailedEvent($this->server->getRequest()));
            throw new Exception\InvalidClientException();
        }

        $phone = $this->server->getRequest()->request->get('phone', null);
        if (is_null($phone)) {
            throw new Exception\InvalidRequestException('phone');
        }

        $activationCode = $this->server->getRequest()->request->get('activation_code', null);
        if (is_null($activationCode)) {
            throw new Exception\InvalidRequestException('activation_code');
        }

        // Check if user's phone and activation_code are correct
        $activationCodeIsActive = $this->userService->checkOnActivityAndExistenceActivationCode($phone, $activationCode);
        if ($activationCodeIsActive) {
            $this->userService->updatePasswordByPhone($phone, $activationCode);
        } else {
            throw new InvalidActivationCodeException();
        }


        $user = $this->userService->getUserByCredentials($phone, $activationCode);

        if (is_null($user)) {
            $this->server->getEventEmitter()->emit(new Event\UserAuthenticationFailedEvent($this->server->getRequest()));
            throw new Exception\InvalidCredentialsException();
        }


        if (is_null($user)) {
            $this->server->getEventEmitter()->emit(new Event\UserAuthenticationFailedEvent($this->server->getRequest()));
            throw new Exception\InvalidCredentialsException();
        }
        // Set current user for scope storage
        $this->server->getScopeStorage()->setCurrentUser($user);

        // Set default scopes if was not defined in request
        $this->server->requireScopeParam(false);
        $this->server->setDefaultScope($user->getScopeIds());

        // Validate any scopes that are in the request
        $scopeParam = $this->server->getRequest()->request->get('scope', '');
        $scopes = $this->validateScopes($scopeParam, $client);

        // Create a new session
        $session = new SessionEntity($this->server);
        $session->setOwner('user', $user->getId());
        $session->associateClient($client);

        // Generate an access token
        $accessToken = new AccessTokenEntity($this->server);
        $accessToken->setId(SecureKey::generate());
        $accessToken->setExpireTime($this->getAccessTokenTTL() + time());

        // Associate scopes with the session and access token
        foreach ($scopes as $scope) {
            $session->associateScope($scope);
        }

        foreach ($session->getScopes() as $scope) {
            $accessToken->associateScope($scope);
        }

        $this->server->getTokenType()->setSession($session);
        $this->server->getTokenType()->setParam('access_token', $accessToken->getId());
        $this->server->getTokenType()->setParam('expires_in', $this->getAccessTokenTTL());

        // Associate a refresh token if set
        if ($this->server->hasGrantType('refresh_token')) {
            $refreshToken = new RefreshTokenEntity($this->server);
            $refreshToken->setId(SecureKey::generate());
            $refreshToken->setExpireTime($this->server->getGrantType('refresh_token')->getRefreshTokenTTL() + time());
            $this->server->getTokenType()->setParam('refresh_token', $refreshToken->getId());
        }

        // Save everything
        $session->save();
        $accessToken->setSession($session);
        $accessToken->save();

        if ($this->server->hasGrantType('refresh_token')) {
            $refreshToken->setAccessToken($accessToken);
            $refreshToken->save();
        }

        return $this->server->getTokenType()->generateResponse();
    }

}
