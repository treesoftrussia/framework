<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Config;

class LanguageDetector
{
    private $application;

    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->hasHeader('Accept-Language')) {
            $headerValue = $request->header('Accept-Language');

            $matchResult = preg_match('/([a-z]{2})/i', $headerValue, $matches);

            $supportedLocales = Config::get('app.supported_locales');
            if ($matchResult && $matchResult[1] && in_array(trim($matchResult[1]), $supportedLocales)) {
                $this->application->setLocale(trim($matchResult[1]));
            } else {
                $this->application->setLocale(Config::get('app.locale'));
            }
        }

        return $next($request);
    }
}
