<?php

Route::get('/oauth/access_token', ['uses' => 'API\SocialNetworkController@getAccessTokenByCode']);

Route::post('/oauth/access_token', function () {
    return Response::json(Authorizer::issueAccessToken());
});

Route::group(['prefix' => 'oauthtest'], function () {
    Route::get('/{id}', ['uses' => 'API\Test\OAuthTestController@index', 'middleware' => 'oauth:test_Get']);
    Route::post('/', ['uses' => 'API\Test\OAuthTestController@store', 'middleware' => 'oauth:test_Store']);
    Route::put('/{id}', ['uses' => 'API\Test\OAuthTestController@update', 'middleware' => 'oauth:test_Update']);
    Route::patch('/{id}', ['uses' => 'API\Test\OAuthTestController@patch', 'middleware' => 'oauth:test_Update']);
    Route::delete('/{id}', ['uses' => 'API\Test\OAuthTestController@delete', 'middleware' => 'oauth:test_Delete']);
});
