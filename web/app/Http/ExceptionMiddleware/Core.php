<?php

namespace App\Http\ExceptionMiddleware;

use App\Http\Support\PageBuilder\PageData;
use Closure;
use App\Libraries\Kangaroo\Error;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Core
{
    private $error;

    public function __construct(Error $error)
    {
        $this->error = $error;
    }

    /**
     * @param $data
     * @param Closure $next
     *
     * @return \Illuminate\Http\Response
     */
    public function handle($data, Closure $next)
    {
        if ($data->exception instanceof HttpException) {
            switch ($data->exception->getStatusCode()) {
                case 404:
                    $code = Error::ROUTE_NOT_FOUND;

                    if (!$data->request->isJson()) {
                        $pageData = new PageData();

                        return $this->error->write(view('admin.support.404', ['pageData' => $pageData]), $code, $data->exception->getStatusCode());
                    }

                    return $this->error->write('Requested resource not found.', $code, $data->exception->getStatusCode());
                case 405:
                    $code = Error::METHOD_NOT_ALLOWED;

                    return $this->error->write('Method not allowed.', $code, $data->exception->getStatusCode());
            }
        }

        return $next($data);
    }
}
