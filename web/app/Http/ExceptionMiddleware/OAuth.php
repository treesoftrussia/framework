<?php

namespace App\Http\ExceptionMiddleware;

use Closure;
use League\OAuth2\Server\Exception\OAuthException;
use App\Libraries\Kangaroo\Error;
use Illuminate\Http\Response;

class OAuth
{

    private $error;

    public function __construct(Error $error)
    {
        $this->error = $error;
    }

    /**
     * @param $data
     * @param Closure $next
     * @return Response
     */
    public function handle($data, Closure $next)
    {
        if($data->exception instanceof OAuthException){
            switch ($data->exception->errorType){
                case 'invalid_request':
                    if(!$data->request->isJson()){
                        return redirect('/login');
                    }
                    $code = Error::PERMISSION_DENIED;
                    return $this->error->write($data->exception->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
                case 'invalid_credentials':
                    $code = Error::INVALID_CREDENTIALS;
                    return $this->error->write($data->exception->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
                case 'invalid_client':
                    $code = Error::INVALID_CLIENT;
                    return $this->error->write($data->exception->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
                case 'invalid_scope':
                    $code = Error::VALIDATION_ERROR;
                    return $this->error->write($data->exception->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
                case 'unsupported_grant_type':
                    $code = Error::INVALID_GRANT_TYPE;
                    return $this->error->write($data->exception->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
                case 'access_denied':
                    if(!$data->request->isJson()){
                        return redirect('/login');
                    }
                    $code = Error::PERMISSION_DENIED;
                    return $this->error->write($data->exception->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
            }
        }
        return $next($data);
    }
    
}


