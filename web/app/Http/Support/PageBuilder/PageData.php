<?php

namespace App\Http\Support\PageBuilder;

use App\Libraries\Kangaroo\Exceptions\RuntimeException;

class PageData
{
    /**
     * State constants.
     */
    const UPDATE = 1;
    const CREATE = 2;

    /**
     * @var
     */
    private $entity;

    /**
     * @var
     */
    private $state;

    /**
     * @var
     */
    private $parsley;

    /**
     * @var
     */
    private $rulesBuilder;

    private $parsleyAttributes;

    private $user;

    private $entityName;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     *
     * @return PageData
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    public function setRuleBuilder($rulesBuilderClassName)
    {
        $this->rulesBuilder = new $rulesBuilderClassName($this->state == self::UPDATE ? 'PUT' : 'POST');

        $this->parsley = new ParsleyRuleCreator();
        $rules = $this->rulesBuilder->body();

        $this->parsleyAttributes = [];
        foreach ($rules as $name => $rule) {
            $this->parsleyAttributes[$name] = $this->parsley->convertRules($name, is_array($rule) ? $rule : explode('|', $rule));
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     *
     * @return PageData
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    public function getEntityName()
    {
        return $this->entityName;
    }

    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     *
     * @return PageData
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEditState()
    {
        return $this->state == self::UPDATE;
    }

    /**
     * @return bool
     */
    public function isUpdateState()
    {
        return $this->state == self::UPDATE;
    }

    /**
     * @param $name
     *
     * @return string
     */
    public function getInputValue($name)
    {
        if (!$this->isEditState()) {
            return '';
        }

        $getter = make_getter($name);

        $entity = $this->getEntity();
        if ($entity && method_exists($entity, $getter)) {
            return $entity->{$getter}();
        }
        throw new RuntimeException('Requested input value was not found.');
    }

    public function getParsleyAttributes($name)
    {
        $result = '';
        foreach ($this->parsleyAttributes[$name] as $key => $value) {
            $result .= ($key.'="'.$value.'"');
        }

        return !empty($result) ? ' '.$result : '';
    }
}
