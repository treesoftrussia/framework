<?php

namespace App\Http\Support\Validator\RuleBuilders;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait SortingRuleBuilderTrait
{
    public function sorting()
    {
        return [
            'direction' => 'string|in:ASC,DESC|required_with:field',
            'field' => 'string|max:255|required_with:direction',
        ];
    }
}
