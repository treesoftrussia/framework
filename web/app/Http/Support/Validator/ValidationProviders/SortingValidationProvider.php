<?php

namespace App\Http\Support\Validator\ValidationProviders;

use App\Libraries\Kangaroo\Exceptions\InvalidInputException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class SortingValidationProvider extends AbstractValidationProvider
{
    protected function getRules()
    {
        if (method_exists($this->rulesBuilder, 'sorting')) {
            return $this->container->call([$this->rulesBuilder, 'sorting']);
        }

        return [];
    }

    protected function getValidationData()
    {
        $query = $this->request->query();
        if (!empty($query['options'])) {
            return $query['options'];
        }

        return [];
    }

    protected function validationFailed($validator)
    {
        throw new InvalidInputException($this->formatErrors($validator));
    }
}
