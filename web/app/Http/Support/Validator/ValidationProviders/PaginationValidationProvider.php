<?php

namespace App\Http\Support\Validator\ValidationProviders;

use Illuminate\Http\Exception\HttpResponseException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class PaginationValidationProvider extends AbstractValidationProvider
{
    protected function getRules()
    {
        if (method_exists($this->rulesBuilder, 'pagination')) {
            return $this->container->call([$this->rulesBuilder, 'pagination']);
        }

        return [];
    }

    protected function getValidationData()
    {
        $query = $this->request->query();
        if (!empty($query['options'])) {
            return $query['options'];
        }

        return [];
    }

    protected function validationFailed($validator)
    {
        //TODO - add errors messages
        throw new HttpResponseException($this->response(
            $this->formatErrors($validator)
        ));
    }
}
