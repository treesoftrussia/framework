<?php

namespace App\Http\Support\Validator\ValidationProviders;

use Illuminate\Contracts\Container\Container;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Validator;
use App\Http\Requests\APIRequest;
use App\Http\Support\Validator\RuleBuilders\AbstractRuleBuilder;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
abstract class AbstractValidationProvider
{
    protected $container;
    protected $rulesBuilder;
    protected $request;

    public function __construct(Container $container, APIRequest $request, AbstractRuleBuilder $rulesBuilder)
    {
        $this->container = $container;
        $this->request = $request;
        $this->rulesBuilder = $rulesBuilder;
    }

    protected function getValidatorInstance()
    {
        $factory = $this->container->make('Illuminate\Validation\Factory');

        $rules = $this->getRules();

        return $factory->make(
            $this->getValidationData(), $rules, $this->request->messages(), $this->request->attributes()
        );
    }

    public function validate()
    {
        $validator = $this->getValidatorInstance();

        if (!$validator->passes()) {
            $this->validationFailed($validator);
        }
    }

    protected function response($errors)
    {
        return new JsonResponse($errors, 422);
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->getMessages();
    }

    abstract protected function validationFailed($validator);

    abstract protected function getRules();

    abstract protected function getValidationData();
}
