<?php

namespace App\Http\Support\Validator\ValidationProviders;

use App\Libraries\Kangaroo\Exceptions\InvalidInputException;

class FilesValidationProvider extends AbstractValidationProvider
{
    protected function getRules()
    {
        if (method_exists($this->rulesBuilder, 'files')) {
            return $this->container->call([$this->rulesBuilder, 'files']);
        }

        return [];
    }

    protected function getValidationData()
    {
        return $this->request->allFiles();
    }

    protected function validationFailed($validator)
    {
        throw new InvalidInputException($this->formatErrors($validator));
    }
}
