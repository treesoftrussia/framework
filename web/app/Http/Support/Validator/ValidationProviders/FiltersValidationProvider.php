<?php

namespace App\Http\Support\Validator\ValidationProviders;

use Illuminate\Http\Exception\HttpResponseException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class FiltersValidationProvider extends AbstractValidationProvider
{
    protected function getRules()
    {
        if (method_exists($this->rulesBuilder, 'filters')) {
            return $this->container->call([$this->rulesBuilder, 'filters']);
        }

        return [];
    }

    protected function getValidationData()
    {
        $query = $this->request->query();
        if (!empty($query['filters'])) {
            return $query['filters'];
        }

        return [];
    }

    protected function validationFailed($validator)
    {
        throw new HttpResponseException($this->response(
            $this->formatErrors($validator)
        ));
    }
}
