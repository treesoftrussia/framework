<?php

namespace App\Http\Support\Validator;

use App\Core\Location\Services\StateService;
use App\Libraries\Validator\StateExistenceInterface;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class StateExistence implements StateExistenceInterface
{
    /**
     * @var StateService
     */
    private $stateService;

    /**
     * @param StateService $stateService
     */
    public function __construct(StateService $stateService)
    {
        $this->stateService = $stateService;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function exists($id)
    {
        return $this->stateService->exists($id);
    }

    /**
     * @param $code
     *
     * @return bool
     */
    public function existsByCode($code)
    {
        return $this->stateService->existsByCode($code);
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function existsByName($name)
    {
        return $this->stateService->existsByName($name);
    }
}
