<?php

namespace App\Http\Support\Validator\RuleObjects;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class RuleObjectCollection extends AbstractRuleObject
{
    protected $baseRuleObject;

    public function __construct(AbstractRuleObject $baseRuleObject)
    {
        $this->baseRuleObject = $baseRuleObject;
    }

    protected function rules()
    {
        $originalRules = $this->baseRuleObject->rules();
        $rules = array();
        foreach ($originalRules as $originalRuleKey => $originalRule) {
            $rules['*.'.$originalRuleKey] = $originalRule;
        }

        return $rules;
    }
}
