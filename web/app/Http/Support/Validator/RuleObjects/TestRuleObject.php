<?php

namespace App\Http\Support\Validator\RuleObjects;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestRuleObject extends AbstractRuleObject
{
    protected function rules()
    {
        if ($this->fieldsRequired) {
            $rules['name'][] = 'required';
        }
        $rules['name'][] = 'string';
        $rules['name'][] = 'max:255';
        if ($this->fieldsRequired) {
            $rules['description'][] = 'required';
        }
        $rules['description'][] = 'string';
        $rules['description'][] = 'max:1000';

        return $rules;
    }
}
