<?php

namespace App\Http\Support\Request;

use App\Core\Support\Sorting\SortingOptions;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait SortingTrait
{
    /**
     * @var bool
     */
    protected $sorting = false;

    public function getSorting()
    {
        if (!$this->sorting) {
            return;
        } else {
            $requestOptions = $this->getOptionsData();

            return $this->buildSorting($requestOptions);
        }
    }

    protected function buildSorting(array $data)
    {
        if (empty($data['field'])) {
            return;
        }

        return $this->extract($data, new SortingOptions(), []);
    }
}
