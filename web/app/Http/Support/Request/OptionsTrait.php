<?php

namespace App\Http\Support\Request;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait OptionsTrait
{
    /**
     * @var bool
     */
    protected $options = false;

    public function getOptions()
    {
        if (!method_exists($this, 'createOptions')) {
            throw new RuntimeException('createOptionsEntity method not implemented.');
        }

        $options = $this->getOptionsData();

        //if(!empty($options)) {

            $optionsEntity = $this->extract($options, $this->container->call([$this, 'createOptions']),
                ['ignore' => ['paginationOptions', 'sortingOptions']]);

        if (method_exists($optionsEntity, 'setPaginationOptions')) {
            $optionsEntity->setPaginationOptions($this->getPagination());
        }

        if (method_exists($optionsEntity, 'setSortingOptions')) {
            $optionsEntity->setSortingOptions($this->getSorting());
        }

        return $optionsEntity;
        //};

        //return null;
    }
}
