<?php

namespace App\Http\Support\Request;

use App\Core\Support\Pagination\PaginationOptions;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait PaginationTrait
{
    /**
     * @var bool
     */
    protected $pagination = false;

    /**
     * @return null|PaginationOptions
     */
    public function getPagination()
    {
        if (!$this->pagination) {
            return;
        } else {
            $requestOptions = $this->getOptionsData();

            return $this->buildPagination($requestOptions);
        }
    }

    protected function buildPagination(array $data)
    {
        return $this->extract($data, new PaginationOptions(), []);
    }
}
