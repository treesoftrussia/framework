<?php

namespace App\Http\Requests;

use App\Core\ClientManagement\Entity\Users\User;
use App\Core\ClientManagement\Services\UserService;
use App\Http\Support\PageBuilder\PageData;
use Illuminate\Http\Request;

class WebRequest extends Request
{
    private $currentUser;

    private $pageData;

    public function __construct(UserService $userService)
    {

    }

    public function getPageData()
    {
        try {
            $this->pageData = new PageData();
            $this->pageData->setUser((new User())->setUserName("Test User"));
        } catch(\Exception $e){

        }

        return $this->pageData;
    }
}
