<?php

namespace App\Http\Requests\Test;

use App\Http\Requests\APIRequest;
use App\Http\Support\Validator\RuleBuilders\TestRuleBuilder;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestDestroyRequest extends APIRequest
{
    protected $rulesBuilder = TestRuleBuilder::class;
    protected $pagination = false;
    protected $routeValidation = true;
}
