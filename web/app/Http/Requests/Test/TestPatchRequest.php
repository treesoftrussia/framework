<?php

namespace App\Http\Requests\Test;

use App\Http\Requests\APIRequest;
use App\Core\Test\Entity\Test;
use App\Http\Support\Validator\RuleBuilders\TestRuleBuilder;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestPatchRequest extends APIRequest
{
    protected $rulesBuilder = TestRuleBuilder::class;
    protected $pagination = false;
    protected $routeValidation = true;

    public function createEntity()
    {
        return $this->extract($this->json()->all(), new Test());
    }
}
