<?php

namespace App\Http\Requests;

use App\Http\Support\Validator\ValidationProviders\BodyValidationProvider;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Support\Request\FiltersTrait;
use App\Http\Support\Request\OptionsTrait;
use App\Http\Support\Request\PaginationTrait;
use App\Http\Support\Request\SortingTrait;
use App\Http\Support\Validator\RuleBuilders\RuleBuilder;
use App\Http\Support\Validator\ValidationProviders\JsonBodyValidationProvider;
use App\Http\Support\Validator\ValidationProviders\FiltersValidationProvider;
use App\Http\Support\Validator\ValidationProviders\PaginationValidationProvider;
use App\Http\Support\Validator\ValidationProviders\RouteValidationProvider;
use App\Http\Support\Validator\ValidationProviders\SortingValidationProvider;
use App\Http\Support\Validator\ValidationProviders\OptionsValidationProvider;
use App\Libraries\Converter\Populator\Populator;
use RuntimeException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
abstract class APIRequest extends FormRequest
{
    use PaginationTrait;
    use SortingTrait;
    use OptionsTrait;
    use FiltersTrait;

    /**
     * @var RuleBuilder
     */
    protected $rulesBuilder = null;

    /**
     * @var bool
     */
    protected $routeValidation = true;

    /**
     * @var array
     */
    protected $validationProviders = array();

    /**
     * @var bool
     */
    protected $jsonBody = true;

    /**
     * Return entity built using request data.
     *
     * @return object
     */
    public function getEntity()
    {
        $method = $this->method();

        if ($method == 'GET' || $method == 'DELETE') {
            throw new RuntimeException('Entity creation through Request instance not possible with GET/DELETE methods.');
        }

        if (method_exists($this, 'createEntity')) {
            return call_user_func([$this, 'createEntity']);
        } else {
            throw new RuntimeException('CreateEntity method not implemented.');
        }
    }

    /**
     * Validate the class instance.
     */
    public function validate()
    {
        if ($this->rulesBuilder) {
            $this->rulesBuilder = new $this->rulesBuilder($this->method());

            if ($this->method() == 'PUT' && $this->route('id')) {
                $this->rulesBuilder->setId($this->route('id'));
            }

            $this->registerValidationProviders();

            foreach ($this->validationProviders as $provider) {
                $provider->validate();
            }
        }
    }

    /**
     * Extract input data to the entity object.
     *
     * @param $data
     * @param $entityToExtract
     * @param array $options
     *
     * @return object
     */
    protected function extract($data, $entityToExtract, $options = [])
    {
        $populator = new Populator($options);

        return $populator->populate($data, $entityToExtract);
    }

    /**
     * @return array
     */
    protected function getFiltersData()
    {
        return array_take($this->query->all(), 'filters', []);
    }

    /**
     * @return array
     */
    protected function getOptionsData()
    {
        return array_take($this->query->all(), 'options', []);
    }

    /**
     * @return RuleBuilder
     */
    public function getRulesBuilder()
    {
        return $this->rulesBuilder;
    }

    /**
     * Registers validation providers using for validation different parts of request.
     */
    public function registerValidationProviders()
    {
        if ($this->jsonBody) {
            $this->validationProviders[] = new JsonBodyValidationProvider($this->container, $this, $this->rulesBuilder);
        } else {
            $this->validationProviders[] = new BodyValidationProvider($this->container, $this, $this->rulesBuilder);
        }

        if ($this->routeValidation) {
            $this->validationProviders[] = new RouteValidationProvider($this->container, $this, $this->rulesBuilder);
        }

        if ($this->sorting) {
            $this->validationProviders[] = new SortingValidationProvider($this->container, $this, $this->rulesBuilder);
        }

        if ($this->pagination) {
            $this->validationProviders[] = new PaginationValidationProvider($this->container, $this, $this->rulesBuilder);
        }

        if ($this->filters) {
            $this->validationProviders[] = new FiltersValidationProvider($this->container, $this, $this->rulesBuilder);
        }

        if ($this->options) {
            $this->validationProviders[] = new OptionsValidationProvider($this->container, $this, $this->rulesBuilder);
        }
    }
}
