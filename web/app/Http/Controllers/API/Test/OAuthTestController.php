<?php namespace App\Http\Controllers\API\Test;

use App\Core\ClientManagement\Services\UserService;
use App\Core\Test\Services\TestService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Test\TestDestroyRequest;
use App\Http\Requests\Test\TestGetRequest;
use App\Http\Requests\Test\TestPatchRequest;
use App\Http\Requests\Test\TestUpdateRequest;
use App\Http\Transformers\TestTransformer;
use App\Libraries\Kangaroo\Resource\Facade as Resource;
use App\Http\Requests\Test\TestStoreRequest;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class OAuthTestController extends Controller
{

    /**
     * @param TestGetRequest $request
     * @param TestService $testService
     * @param UserService $userService
     * @return mixed
     */
    public function index(TestGetRequest $request, TestService $testService, UserService $userService)
    {
        $user = $userService->getUser();

        if ($user->hasScope('test_Get')) {
            return Resource::make($testService->get($request->route('id')), new TestTransformer());
        }

        return null;
    }


    /**
     * @param TestStoreRequest $request
     * @param TestService $testService
     * @param UserService $userService
     * @return null
     */
    public function store(TestStoreRequest $request, TestService $testService, UserService $userService)
    {
        $user = $userService->getUser();

        if ($user->hasScope('test_Store')) {
            $test = $testService->create($request->getEntity());
            return Resource::make($test, new TestTransformer(), false);
        }

        return null;
    }


    /**
     * @param TestUpdateRequest $request
     * @param TestService $testService
     * @param UserService $userService
     * @return null
     */
    public function update(TestUpdateRequest $request, TestService $testService, UserService $userService)
    {
        $user = $userService->getUser();

        if ($user->hasScope('test_Update')) {
            $test = $testService->update($request->route('id'), $request->getEntity());
            return Resource::make($test, new TestTransformer());
        }

        return null;
    }

    /**
     * @param TestPatchRequest $request
     * @param TestService $testService
     * @param UserService $userService
     * @return null
     * @throws \Symfony\Component\CssSelector\Exception\InternalErrorException
     */
    public function patch(TestPatchRequest $request, TestService $testService, UserService $userService)
    {
        $user = $userService->getUser();

        if ($user->hasScope('test_Update')) {
            $test = $testService->update($request->route('id'), $request->getEntity());
            return Resource::make($test, new TestTransformer());
        }

        return null;
    }

    /**
     * @param TestDestroyRequest $request
     * @param TestService $testService
     * @param UserService $userService
     * @return null
     */
    public function delete(TestDestroyRequest $request, TestService $testService, UserService $userService)
    {
        $user = $userService->getUser();

        if ($user->hasScope('test_Delete')) {
            $testService->delete($request->route('id'));

            return Resource::blank();
        }

        return null;
    }

}