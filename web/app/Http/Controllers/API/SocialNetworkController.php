<?php namespace App\Http\Controllers\API;

use App\Core\ClientManagement\Services\OAuthLoginViaSocialNetworkSupportService;
use App\Http\Controllers\Controller;
use App\Http\Requests\SocialNetwork\GetWechatAccessTokenRequest;
use App\Http\Transformers\User\TokenEntityTransformer;
use App\Libraries\Kangaroo\Resource\Facade as Resource;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class SocialNetworkController extends Controller
{
    /**
     * @param GetWechatAccessTokenRequest $request
     * @param OAuthLoginViaSocialNetworkSupportService $service
     * @return mixed
     */
    public function getAccessTokenByCode(GetWechatAccessTokenRequest $request, OAuthLoginViaSocialNetworkSupportService $service)
    {
        $tokenEntity = $service->getToken($request->get('code'), $request->get('type'));

        return Resource::make($tokenEntity, new TokenEntityTransformer());
    }
}