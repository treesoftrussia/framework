<?php

namespace App\Http\ErrorCodes;

use App\Http\ErrorCodes\ErrorCodesConstantDefinitions\AppErrorCodesConstantDefinitions;
use App\Http\ErrorCodes\ErrorCodesConstantDefinitions\CoreErrorCodesConstantDefinitions;
use App\Http\ErrorCodes\ErrorCodesConstantDefinitions\OAuthErrorCodesConstantDefinitions;
use App\Libraries\Kangaroo\BaseErrorCodes;

class ErrorCodes extends BaseErrorCodes implements CoreErrorCodesConstantDefinitions, OAuthErrorCodesConstantDefinitions, AppErrorCodesConstantDefinitions
{
}
