<?php

namespace App\Core\Test\Entity;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestChild
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $testId;

    /**
     * @var string
     */
    private $title;

    /**
     * @param int $testId
     *
     * @return string
     */
    public function setTestId($testId)
    {
        $this->testId = $testId;

        return $this;
    }

    /**
     * @return int
     */
    public function getTestId()
    {
        return $this->testId;
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
