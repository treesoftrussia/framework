<?php

namespace App\Core\Test\Interfaces;

use App\Core\Test\Entity\Test;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
interface TestRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return Test|null
     */
    public function get($id);

    /**
     * @param Test $entity
     *
     * @return Test|null
     */
    public function create(Test $entity);

    /**
     * @param int    $id
     * @param Entity $entity
     */
    public function update($id, Test $entity);

    /**
     * @param int $id
     *
     * @return bool
     */
    public function exists($id);

    /**
     * @param int $id
     */
    public function delete($id);
}
