<?php

namespace App\Core\Test\Interfaces;

use App\Core\Test\Entity\TestChild;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
interface TestChildRepositoryInterface
{
    /**
     * @param TestChild $entity
     *
     * @return TestChild|null
     */
    public function create(TestChild $entity);
}
