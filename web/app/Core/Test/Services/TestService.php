<?php

namespace App\Core\Test\Services;

use App\Core\Support\Service\AbstractService;
use App\Core\Test\Interfaces\TestRepositoryInterface;
use App\Core\Test\Entity\Test;
use App\Libraries\Kangaroo\Exceptions\ResourceNotFoundException;
use App\Libraries\Kangaroo\Exceptions\RuntimeException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestService extends AbstractService
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function get($id)
    {
        $repository = $this->container->get(TestRepositoryInterface::class);

        $testEntity = $repository->get($id);

        if (is_null($testEntity)) {
            throw new ResourceNotFoundException('Requested route was not found');
        }

        return $testEntity;
    }

    /**
     * @param Test $test
     *
     * @return mixed
     */
    public function create(Test $test)
    {
        $repository = $this->container->get(TestRepositoryInterface::class);

        $testEntity = $repository->create($test);

        return $testEntity;
    }

    /**
     * @param $id
     * @param Test $test
     *
     * @return mixed
     */
    public function update($id, Test $test)
    {
        $repository = $this->container->get(TestRepositoryInterface::class);

        $updated = $repository->update($id, $test);
        if (!$updated) {
            throw new RuntimeException('Resource Test Not Updated');
        }

        $testEntity = $repository->get($id);

        if (is_null($testEntity)) {
            throw new ResourceNotFoundException('Requested route was not found');
        }

        return $testEntity;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function exists($id)
    {
        $repository = $this->container->get(TestRepositoryInterface::class);

        return $repository->exists($id);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function delete($id)
    {
        $repository = $this->container->get(TestRepositoryInterface::class);

        return $repository->delete($id);
    }
}
