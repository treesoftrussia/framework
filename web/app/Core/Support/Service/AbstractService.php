<?php

namespace App\Core\Support\Service;

use App\Core\Support\Container\ContainerInterface;

/**
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 */
abstract class AbstractService
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->initialize();
    }

    /**
     * A hook allowing to initialize a service.
     */
    protected function initialize()
    {
        //
    }
}
