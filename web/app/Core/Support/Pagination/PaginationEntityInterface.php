<?php

namespace App\Core\Support\Pagination;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
interface PaginationEntityInterface
{
    public function getItems();

    public function getPagination();
}
