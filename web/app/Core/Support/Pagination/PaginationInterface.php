<?php

namespace App\Core\Support\Pagination;

interface PaginationInterface
{
    public function getTotalItems();

    public function getItemsOnCurrentPage();

    public function getItemsPerPageRequested();

    public function getCurrentPage();

    public function getTotalPages();
}
