<?php

namespace App\Core\Support\Pagination;

class Pagination implements PaginationInterface
{
    /**
     * @var
     */
    private $totalItems;

    /**
     * @var
     */
    private $itemsOnCurrentPage;

    /**
     * @var
     */
    private $itemsPerPageRequested;

    /**
     * @var
     */
    private $currentPage;

    /**
     * @var
     */
    private $totalPages;

    /**
     * @return mixed
     */
    public function getTotalItems()
    {
        return $this->totalItems;
    }

    /**
     * @param $totalItems
     *
     * @return $this
     */
    public function setTotalItems($totalItems)
    {
        $this->totalItems = $totalItems;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getItemsOnCurrentPage()
    {
        return $this->itemsOnCurrentPage;
    }

    /**
     * @param $total
     * @param $current
     * @param $itemsPerPage
     *
     * @return $this
     */
    public function setItemsOnCurrentPage($total, $current, $itemsPerPage, $totalPages)
    {
        if ($total <= $itemsPerPage && $current <= $totalPages) {
            $this->itemsOnCurrentPage = $total;

            return $this;
        }
        if ($current * $itemsPerPage <= $total) {
            $this->itemsOnCurrentPage = $itemsPerPage;

            return $this;
        }
        if ($total < $itemsPerPage * ($current - 1)) {
            $this->itemsOnCurrentPage = 0;

            return $this;
        }
        $this->itemsOnCurrentPage = $itemsPerPage - ($current * $itemsPerPage - $total);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getItemsPerPageRequested()
    {
        return $this->itemsPerPageRequested;
    }

    /**
     * @param $itemsPerPageRequested
     *
     * @return $this
     */
    public function setItemsPerPageRequested($itemsPerPageRequested)
    {
        $this->itemsPerPageRequested = $itemsPerPageRequested;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @param $currentPage
     *
     * @return $this
     */
    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPages()
    {
        return $this->totalPages;
    }

    /**
     * @param $totalPages
     *
     * @return $this
     */
    public function setTotalPages($totalPages)
    {
        $this->totalPages = $totalPages;

        return $this;
    }
}
