<?php

namespace App\Core\Support\Pagination;

use App\Libraries\Cast\Cast;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class PaginationOptions
{
    public function __construct()
    {
        $this->perPage = config('params.pagination_per_page');
    }

    /**
     * @var
     */
    private $perPage;

    /**
     * @var
     */
    private $pageNum = 1;

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @return int
     */
    public function getPageNum()
    {
        return $this->pageNum;
    }

    /**
     * @param int $perPage
     *
     * @return $this
     */
    public function setPerPage($perPage)
    {
        $this->perPage = Cast::int($perPage);

        return $this;
    }

    /**
     * @param int $pageNum
     *
     * @return $this
     */
    public function setPageNum($pageNum)
    {
        $this->pageNum = Cast::int($pageNum);

        return $this;
    }
}
