<?php

namespace App\Core\Support\Pagination;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait PaginationOptionsTrait
{
    /**
     * @var
     */
    private $paginationOptions;

    public function setPaginationOptions($paginationOptions)
    {
        $this->paginationOptions = $paginationOptions;

        return $this;
    }

    /**
     * @return PaginationOptions
     */
    public function getPaginationOptions()
    {
        return $this->paginationOptions;
    }
}
