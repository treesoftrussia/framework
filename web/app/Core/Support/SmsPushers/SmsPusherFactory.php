<?php
namespace App\Core\Support\SmsPushers;

class SmsPusherFactory
{
    public static function make($type)
    {
        if (empty($type)) {
            $pusherType = config('params.sms_pusher');
        } else {
            $pusherType = $type;
        }

        switch ($pusherType) {
            case 'TWILLO':
                return new TwilloSmsPusher();
                break;
            case 'FAKE':
                return new FakeSmsPusher();
            break;
            case 'SMSRU':
                return new SmsruSmsPusher();
                break;
            case 'WAVECELL':
                return new WavecellSmsPusher();
                break;
            default:
                return new FakeSmsPusher();
        }
    }
}