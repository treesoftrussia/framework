<?php
namespace App\Core\Support\SmsPushers;

abstract class AbstractSmsPusher
{
    public abstract function send($phone, $activationCode);
}
