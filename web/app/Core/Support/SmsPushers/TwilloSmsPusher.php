<?php
namespace App\Core\Support\SmsPushers;

use Lookups_Services_Twilio;
use Services_Twilio;
use Services_Twilio_RestException;

class TwilloSmsPusher extends AbstractSmsPusher
{
    public function send($phone, $activationCode)
    {
        //$phone = str_replace('+7', '7', $phone);

        if($phone[0] != '+'){
            $phone = '+' . $phone;
        }

        $clientLookups = new Lookups_Services_Twilio(config('params.sms_pusher_twillo_account_sid'), config('params.sms_pusher_twillo_auth_token'));
        $number = $clientLookups->phone_numbers->get($phone);
        $phone = $number->phone_number;

        $client = new Services_Twilio(config('params.sms_pusher_twillo_account_sid'), config('params.sms_pusher_twillo_auth_token'));

        try {
            $message = $client->account->messages->create(array(
                "From" => config('params.sms_pusher_twillo_from'),
                "To" => $phone,
                "Body" => $activationCode,
            ));
        }  catch (Services_Twilio_RestException $e) {
            //echo $e->getMessage();
        }
    }
}
