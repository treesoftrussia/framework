<?php
namespace App\Core\Support\SmsPushers;

use App\Core\ClientManagement\Entity\Support\ActivationCode;
use \RuntimeException as DefaultRuntimeException;
use App\Libraries\Kangaroo\Error;
use Symfony\Component\HttpFoundation\Response;
use App\Libraries\Kangaroo\Exceptions\ExceptionInterface;


class SmsPusherException extends DefaultRuntimeException implements ExceptionInterface
{

    public function getHTTPCode(){
        return Response::HTTP_BAD_REQUEST;
    }

    public function getInternalCode(){
        return Error::SMS_PUSHER_MANY_REQUESTS;
    }

    public function __construct($message = null)
    {
        parent::__construct(!$message ? trans('api.activation_code_to_many_requests', ['activation_code_refresh_time' => ActivationCode::REFRESH_TIME]) : $message);
    }

    public function getData()
    {
        return $this->getMessage();
    }
}