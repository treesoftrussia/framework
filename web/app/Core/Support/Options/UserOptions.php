<?php

namespace App\Core\Support\Options;


class UserOptions implements AbstractOptionsInterface
{

    private $withSocialNetworkEntry;

    /**
     * @return mixed
     */
    public function getWithSocialNetworkEntry()
    {
        return $this->withSocialNetworkEntry;
    }

    /**
     * @param $withSocialNetworkEntry
     * @return $this
     */
    public function setWithSocialNetworkEntry($withSocialNetworkEntry)
    {
        $this->withSocialNetworkEntry = $withSocialNetworkEntry;
        return $this;
    }
}
