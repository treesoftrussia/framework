<?php

namespace App\Core\Support\Options;

use App\Core\Support\Pagination\PaginationOptionsTrait;

class DefaultPaginationOptions implements AbstractOptionsInterface
{
    use PaginationOptionsTrait;
}
