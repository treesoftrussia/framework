<?php

namespace App\Core\Support\Options;

use App\Core\Support\Entity\Coordinates;

trait CoordinatesOptionsTrait
{
    /**
     * @var
     */
    private $coordinates = [];

    /**
     * @return mixed
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param $coordinates
     *
     * @return $this
     */
    public function setCoordinates(Coordinates $coordinates)
    {
        $this->coordinates = $coordinates;

        return $this;
    }
}
