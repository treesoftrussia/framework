<?php namespace App\Core\Support\Options;


trait SearchingOptionsTrait
{
    private $searchPhrase;

    /**
     * @return mixed
     */
    public function getSearchPhrase()
    {
        return $this->searchPhrase;
    }

    /**
     * @param mixed $searchPhrase
     * @return RetailChainListOptions
     */
    public function setSearchPhrase($searchPhrase)
    {
        $this->searchPhrase = $searchPhrase;
        return $this;
    }
}