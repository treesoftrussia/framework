<?php

namespace App\Core\Support\Options;

use App\Core\Support\Filters\IdentificatorSearchOptionsTrait;
use App\Core\Support\Pagination\PaginationOptionsTrait;
use App\Core\Support\Sorting\SortingOptionsTrait;


class UserListOptions implements AbstractOptionsInterface
{
    use PaginationOptionsTrait;
    use SearchingOptionsTrait;
    use SortingOptionsTrait;
    use IdentificatorSearchOptionsTrait;

    private $managers;

    /**
     * @return mixed
     */
    public function getManagers()
    {
        return $this->managers;
    }

    /**
     * @param mixed $managers
     *
     * @return UserListOptions
     */
    public function setManagers($managers)
    {
        $this->managers = $managers;

        return $this;
    }
}
