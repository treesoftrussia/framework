<?php
namespace App\Core\Support\Behaviours;
use App\Libraries\Cast\Cast;

trait TimesTrait
{
    /**
     * @var
     */
    protected $createdAt;

    /**
     * @var
     */
    protected $updatedAt;

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $created_at
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->createdAt = Cast::int($created_at);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = Cast::int($updatedAt);
        return $this;
    }
}