<?php

namespace App\Core\Support\Entity;

class Image
{
    /**
     * @var string
     */
    private $originalURL;

    /**
     * @var string
     */
    private $thumbnailURL;

    /**
     * @return mixed
     */
    public function getOriginalURL()
    {
        return $this->originalURL;
    }

    /**
     * @param mixed $originalURL
     * @return Image
     */
    public function setOriginalURL($originalURL)
    {
        $this->originalURL = $originalURL;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getThumbnailURL()
    {
        return $this->thumbnailURL;
    }

    /**
     * @param mixed $thumbnailURL
     * @return Image
     */
    public function setThumbnailURL($thumbnailURL)
    {
        $this->thumbnailURL = $thumbnailURL;
        return $this;
    }
}