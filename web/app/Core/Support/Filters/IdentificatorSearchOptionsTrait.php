<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace App\Core\Support\Filters;


trait IdentificatorSearchOptionsTrait
{
    /**
     * @var array
     */
    private $identificators;

    /**
     * @return array
     */
    public function getIdentificators()
    {
        return $this->identificators;
    }

    /**
     * @param string $identificators
     * @return IdentificatorSearchOptionsTrait
     */
    public function setIdentificators($identificators)
    {
        $this->identificators = explode(',',$identificators);
        return $this;
    }



}