<?php

namespace App\Core\Support\Container;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class ServiceProvider extends IlluminateServiceProvider
{
    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerImplementations();
    }

    /**
     * Binds all implementations.
     */
    private function registerImplementations()
    {
        $implementations = Config::get('container.implementations', []);

        foreach ($implementations as $interface => $implementation) {
            $this->app->singleton($interface, $implementation);
        }
    }
}
