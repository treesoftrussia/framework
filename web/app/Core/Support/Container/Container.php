<?php

namespace App\Core\Support\Container;

use Illuminate\Container\Container as IlluminateContainer;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class Container implements ContainerInterface
{
    /**
     * @var IlluminateContainer
     */
    private $container;

    /**
     * @param IlluminateContainer $container
     */
    public function __construct(IlluminateContainer $container)
    {
        $this->container = $container;
    }

    /**
     * @param string              $abstract
     * @param string|object|array $callerOrParameters
     * @param array               $parameters
     *
     * @return object
     */
    public function get($abstract, $parameters = [])
    {
        return $this->container->make($abstract, $parameters);
    }
}
