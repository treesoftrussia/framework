<?php

namespace App\Core\Support\Sorting;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class SortingOptions
{
    /**
     * @var
     */
    private $direction = 'ASC';

    /**
     * @var
     */
    private $field;

    /**
     * @return string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param int $direction
     *
     * @return $this
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * @param string $field
     *
     * @return $this
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }
}
