<?php

namespace App\Core\Support\Sorting;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait SortingOptionsTrait
{
    /**
     * @var
     */
    private $sortingOptions;

    public function setSortingOptions($sortingOptions)
    {
        $this->sortingOptions = $sortingOptions;

        return $this;
    }

    public function getSortingOptions()
    {
        return $this->sortingOptions;
    }
}
