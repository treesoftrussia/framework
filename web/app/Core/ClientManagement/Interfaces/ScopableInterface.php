<?php

namespace App\Core\ClientManagement\Interfaces;

interface ScopableInterface
{
    /**
     * @return mixed
     */
    public function getScopes();

    /**
     * @param $scope
     * @return mixed
     */
    public function hasScope($scope);

    /**
     * @return mixed
     */
    public function getScopeIds();
}