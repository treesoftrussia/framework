<?php

namespace App\Core\ClientManagement\Interfaces;

use App\Core\ClientManagement\Entity\Users\User;

interface UserRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function getUser($id);

    /**
     * @param $email
     * @return mixed
     */
    public function getUserByCredentials($email);

    /**
     * @param User $object
     * @return mixed
     */
    public function create(User $object);

    /**
     * @param $id
     * @param User $object
     * @return mixed
     */
    public function update($id, User $object);

}