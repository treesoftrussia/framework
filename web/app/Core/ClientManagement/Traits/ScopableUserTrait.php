<?php

namespace App\Core\ClientManagement\Traits;

use Illuminate\Support\Collection;
use RuntimeException;

trait ScopableUserTrait
{
    /**
     * @return array
     */
    public function getScopes()
    {
        $scopes = new Collection();

        foreach ($this->getRoles() as $role) {
            foreach ($role->getScopes() as $scope) {
                $scopes->put($scope->getId(), $scope);
            }
        }

        return $scopes;
    }

    /**
     * @return array
     */
    public function getScopeIds()
    {
        $scopeIds = [];
        foreach ($this->getRoles() as $role) {
            foreach ($role->getScopes() as $scope) {
                if (!in_array($scope->getId(), $scopeIds)) {
                    $scopeIds[] = $scope->getId();
                }
            }
        }

        return $scopeIds;
    }

    /**
     * @param $scopeId
     * @return mixed
     * @throws RuntimeException
     */
    public function hasScope($scopeId)
    {
        if(is_array($scopeId)){
            throw new RuntimeException('Scope arrays not supported yet.');
        } else {
            return in_array($scopeId, $this->getScopeIds());
        }
    }
}