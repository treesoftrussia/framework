<?php 
namespace App\Core\ClientManagement\Services\SocialNetworkProviders;

use App\Core\ClientManagement\Entity\Support\Token;
use App\Core\ClientManagement\Entity\Users\SocialNetworkProfile;
use App\Core\ClientManagement\Entity\Users\User;
use App\Core\Support\Entity\Image;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;


class WechatSocialNetworkProvider implements SocialNetworkProviderInterface
{
    /**
     * @var SocialNetworkProfile $profile
     */
    private $profile;

    public function requestAccess($code)
    {

        $clientId = config('wechat.client_id');
        $clientSecret = config('wechat.client_secret');

        $client = new Client();
        $res = $client->request('GET', 'https://api.wechat.com/sns/oauth2/access_token?appid=' . $clientId . '&secret=' . $clientSecret . '&code=' . $code . '&grant_type=authorization_code');

        $wechatTokenData = json_decode($res->getBody()->getContents());
        
        if ($res->getStatusCode() == 200 && $wechatTokenData && is_object($wechatTokenData) && isset($wechatTokenData->access_token)) {
            $tokenData = (new Token())->setAccessToken($wechatTokenData->access_token)->setExpiresIn($wechatTokenData->expires_in)->setTokenType('Bearer');
            $this->profile = new SocialNetworkProfile();
            $this->profile->setInternalId($wechatTokenData->openid)->setType('wechat')->setTokenData($tokenData);
            return $this->profile;
        }

        throw new OAuthServerErrorException(is_object($wechatTokenData) && $wechatTokenData->errmsg ? sprintf('Wechat auth server returned error: %s.', $wechatTokenData->errmsg) : 'Unknown wechat auth server error.');
    }

    public function getUser(){
        $client = new Client();

        //https://api.wechat.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID
        $res = $client->request('GET', sprintf('https://api.wechat.com/sns/userinfo?access_token=%s&openid=%s', $this->profile->getTokenData()->getAccessToken(), $this->profile->getInternalId()));

        $wechatUserInfo = json_decode($res->getBody());
        if($res->getStatusCode() == 200 && $wechatUserInfo && is_object($wechatUserInfo) && isset($wechatUserInfo->access_token)) {
            $user = new User();

            $user->setInternalUsername($wechatUserInfo->nickname);
            $user->setUserName(str_random(40));
            $user->setSex($wechatUserInfo->sex);
            $user->setAvatar(!empty($wechatUserInfo->headimgurl) ? (new Image())->setOriginalURL($wechatUserInfo->headimgurl)->setThumbnailURL($wechatUserInfo->headimgurl) : null);
            $user->setPassword(str_random(40));
            $user->setPasswordHash(Hash::make($user->getPassword()));

            return $user;
        }

        throw new OAuthServerErrorException(is_object($wechatUserInfo) && $wechatUserInfo->errmsg ? sprintf('Wechat auth server returned error: %s.', $wechatUserInfo->errmsg) : 'Unknown wechat auth server error.');
    }
}