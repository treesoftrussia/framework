<?php
namespace App\Core\ClientManagement\Services\SocialNetworkProviders;

interface SocialNetworkProviderInterface
{
    public function requestAccess($code);

    public function getUser();
}