<?php
namespace App\Core\ClientManagement\Services\SocialNetworkProviders;

use \RuntimeException as DefaultRuntimeException;
use App\Libraries\Kangaroo\Error;
use Symfony\Component\HttpFoundation\Response;
use App\Libraries\Kangaroo\Exceptions\ExceptionInterface;


class OAuthServerErrorException extends DefaultRuntimeException implements ExceptionInterface
{

    public function getHTTPCode(){
        return Response::HTTP_UNAUTHORIZED;
    }

    public function getInternalCode(){
        return Error::INVALID_CREDENTIALS;
    }

    public function __construct($message = 'Auth server don\'t accepted authorization.')
    {
        parent::__construct($message);
    }

    public function getData()
    {
        return $this->getMessage();
    }
}