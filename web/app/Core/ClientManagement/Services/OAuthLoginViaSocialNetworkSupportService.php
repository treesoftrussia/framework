<?php

namespace App\Core\ClientManagement\Services;

use App\Core\ClientManagement\Entity\Support\WebLogin;
use App\Core\ClientManagement\Interfaces\UserRepositoryInterface;
use App\Core\ClientManagement\Services\SocialNetworkProviders\WechatSocialNetworkProvider;
use App\Core\Support\Container\ContainerInterface;
use App\Core\Support\Options\UserOptions;
use App\Core\Support\Service\AbstractService;
use App\DAL\Repository\UserRepository;
use App\Libraries\Kangaroo\Exceptions\RuntimeException;
use League\OAuth2\Server\Exception\InvalidCredentialsException;

class OAuthLoginViaSocialNetworkSupportService extends AbstractService
{
    /**
     * @var UserRepository $userRepository
     */
    private $userRepository;

    /**
     * OAuthLoginViaSocialNetworkSupportService constructor.
     * @param ContainerInterface $container
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(ContainerInterface $container, UserRepositoryInterface $userRepository)
    {
        parent::__construct($container);
        $this->userRepository = $userRepository;
    }

    /**
     * @param $code
     * @param $type
     * @return mixed
     * @throws InvalidCredentialsException
     */
    public function getToken($code, $type)
    {
        $provider = $this->getProvider($type);

        $socialNetworkProfile = $provider->requestAccess($code);
        $user = $provider->getUser();

        $user = $this->userRepository->createOrUpdateIfExistsSocialNetworkEntry($user, $socialNetworkProfile, (new UserOptions())->setWithSocialNetworkEntry(true));

        return (new OauthSupportService())->getTokenByPasswordGrantType((new WebLogin())->setLogin($user->getUserName())->setPassword($user->getPassword()));
    }


    private function getProvider($type){
        switch ($type){
            case 'wechat':
                return $this->container->get(WechatSocialNetworkProvider::class);
            default:
                throw new RuntimeException('Specified login type not supported.');
        }
    }
}