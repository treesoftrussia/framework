<?php

namespace App\Core\ClientManagement\Services;

use App\Core\ClientManagement\Entity\Support\Token;
use App\Core\ClientManagement\Entity\Support\WebLogin;
use App\Libraries\Kangaroo\Exceptions\RuntimeException;
use Illuminate\Http\Request;
use League\OAuth2\Server\Exception\InvalidCredentialsException;
use Symfony\Component\HttpFoundation\ParameterBag;

class OauthSupportService
{
    /**
     * @param WebLogin $webLoginEntity
     * @return Token
     * @throws InvalidCredentialsException
     */
    public function getTokenByPasswordGrantType(WebLogin $webLoginEntity)
    {
        $requestData = [
            'grant_type' => 'password',
            'client_id' => config('params.web_client_id'),
            'client_secret' => config('params.web_client_secret'),
            'login' => $webLoginEntity->getLogin(),
            'password' => $webLoginEntity->getPassword()
        ];

        return $this->makeTokenEntity((array)$this->requestToken($requestData));
    }

    public function getTokenByActivationCodeGrantType($credentials){
        $requestData = [
            'grant_type' => 'activation_code',
            'client_id' => config('params.mobile_client_id'),
            'client_secret' => config('params.mobile_client_secret'),
            'phone' => $credentials['phone'],
            'activation_code' => $credentials['activation_code']
        ];

        return $this->makeTokenEntity((array)$this->requestToken($requestData));
    }

    private function requestToken($requestData){
        $oauthRequest = Request::create('/oauth/access_token', 'POST', $requestData);
        $oauthRequest->request = new ParameterBag($requestData);

        $response = app()->handle($oauthRequest);

        $responseData = json_decode($response->getContent());

        if (empty($responseData->access_token)) {
            throw new InvalidCredentialsException('The user credentials were incorrect');
        }

        return $responseData;
    }

    public function getTokenByGrantType($credentialsEntity, $grantType = 'password'){
        switch ($grantType){
            case 'password':
                return $this->getTokenByPasswordGrantType($credentialsEntity);
            case 'activation_code':
                return $this->getTokenByActivationCodeGrantType($credentialsEntity);
        }
        throw new RuntimeException('Wrong grant type.');
    }

    private function makeTokenEntity(array $tokenData){
        $tokenEntity = new Token();
        $tokenEntity->setAccessToken($tokenData['access_token']);
        $tokenEntity->setExpiresIn($tokenData['expires_in']);
        $tokenEntity->setTokenType($tokenData['token_type']);
        return $tokenEntity;
    }
}