<?php

namespace App\Core\ClientManagement\Services;

use App\Core\ClientManagement\Entity\Rights\Role;
use App\Core\ClientManagement\Entity\Support\ActivationCode;
use App\Core\ClientManagement\Entity\Support\Registration;
use App\Core\ClientManagement\Entity\Support\WebLoginResponse;
use App\Core\ClientManagement\Entity\Support\WebLogoutResponse;
use App\Core\Support\Container\ContainerInterface;
use App\Core\Support\Options\UserListOptions;
use App\Core\Support\Service\AbstractService;
use App\Core\ClientManagement\Interfaces\UserRepositoryInterface;
use App\Core\Support\SmsPushers\InvalidActivationCodeException;
use App\DAL\Repository\UserRepository;
use App\Libraries\Kangaroo\Exceptions\AccessDeniedException;
use App\Libraries\Kangaroo\Exceptions\InvalidInputException;
use App\Libraries\Kangaroo\Exceptions\RuntimeException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Hashing\Hasher;
use Mockery\CountValidator\Exception;

class UserService extends AbstractService
{

    /**
     * @var UserRepository
     */
    private $repository;

    public function __construct(UserRepositoryInterface $repository, ContainerInterface $container)
    {
        parent::__construct($container);
        $this->repository = $repository;
    }

    /**
     * @param string $id
     * @return null
     */
    public function getUser($id = 'current')
    {
        $authorizer = $this->container->get('oauth2-server.authorizer');
        $ownerId = $authorizer->getResourceOwnerId();
        $ownerType = $authorizer->getResourceOwnerType();
        /*$ownerId = Authorizer::getResourceOwnerId();
        $ownerType = Authorizer::getResourceOwnerType();*/

        switch ($ownerType) {
            case 'user':
                $user = $this->repository->getUser($ownerId);

                if ($id == 'current') {
                    break;
                }

                if ($user->hasScope('user-public-show-any')) {
                    $user = $this->repository->getUser(intval($id));
                } else {
                    throw new AccessDeniedException();
                }
                break;
            case 'client':
                $user = $this->repository->getClient($ownerId);
                break;
            default:
                $user = null;
        }

        return $user;
    }
    

    /**
     * @param $login
     * @param $password
     * @return null
     */
    public function getUserByCredentials($login, $password)
    {
        $user = $this->repository->getUserByCredentials($login);

        $hasher = $this->container->get(Hasher::class);

        if (!is_null($user) && $hasher->check($password, $user->getPasswordHash())) {
            return $user;
        }

        return null;
    }


    /**
     * @param Registration $registration
     * @param ProducerService $producerService
     * @param bool $login
     * @return \App\Core\ClientManagement\Entity\Support\Token|mixed
     */
    public function registerUser(Registration $registration, ProducerService $producerService, $login = true)
    {
        $producerId = $producerService->getProducerIdByClientCredentials($registration->getClientId(), $registration->getClientSecret());

        if (is_null($producerId)) {
            throw new AccessDeniedException();
        }

        $user = $registration->getUser();

        if ($this->repository->checkOnExistUserByPhone($user->getPhone())) {
            throw new InvalidInputException(['phone' => trans('api.phone_already_has_been_taken')]);
        }

        if ($this->repository->checkOnExistUserByEmail($user->getEmail())) {
            throw new InvalidInputException(['email' => trans('api.email_already_has_been_taken')]);
        }

        $activationCodeIsActive = $this->repository->checkOnActivityAndExistenceActivationCode($user->getPhone(), $user->getPassword());
        if (!$activationCodeIsActive) {
            throw new InvalidActivationCodeException(trans('api.activation_code_is_invalid'));
        }

        $user->setProducerId($producerId);
        $user->setPasswordHash(Hash::make($user->getPassword()));

        $userEntity = $this->repository->create($user);

        if (!$userEntity) {
            throw new RuntimeException();
        }

        $userEntity = $this->repository->setRoles($userEntity, [Role::ROLE_MOBILE_USER]);

        if (!$userEntity) {
            $this->repository->delete($userEntity->getId());
            throw new RuntimeException();
        }

        return $login ? $this->requestUserToken(['phone' => $userEntity->getPhone(), 'activation_code' => $user->getPassword()], true) : $userEntity;
    }

    /**
     * @param array $credentials
     * @param $userEntity
     * @param bool $removeIfFailed
     * @return \App\Core\ClientManagement\Entity\Support\Token|mixed
     */
    protected function requestUserToken(array $credentials, $userEntity, $removeIfFailed = false){
        $oauthSupportService = new OauthSupportService();
        try {
            $tokenEntity = $oauthSupportService->getTokenByGrantType($credentials, 'activation_code');
        } catch (Exception $e){
            if($removeIfFailed){
                $this->repository->delete($userEntity->getId());
            }
            throw $e;
        }

        return $tokenEntity;
    }

    /**
     * @param $phone
     * @param $password
     * @return mixed
     */
    public function checkOnActivityAndExistenceActivationCode($phone, $password)
    {
        return $this->repository->checkOnActivityAndExistenceActivationCode($phone, $password);
    }

    /**
     * @param ActivationCode $activationCodeEntity
     * @param ProducerService $producerService
     * @return ActivationCode
     * @throws SmsPusherException
     */
    public function createActivationCode(ActivationCode $activationCodeEntity, ProducerService $producerService)
    {
        $producerId = $producerService->getProducerIdByClientCredentials($activationCodeEntity->getClientId(), $activationCodeEntity->getClientSecret());
        if (is_null($producerId)) {
            throw new AccessDeniedException();
        }

        $activationCodeEntityExist = $this->repository->getActivationCodeByPhone($activationCodeEntity->getPhone());

        if ($activationCodeEntityExist) {
            $activationCodeIsActive = $this->repository->checkOnActivityCode($activationCodeEntity->getPhone());

            if ($activationCodeIsActive) {

                $activationCodeIsAvailableForRefresh = $this->repository->checkActivationCodeOnAvailabilityForRefresh($activationCodeEntity->getPhone());

                if (!$activationCodeIsAvailableForRefresh) {
                    throw new SmsPusherException();
                }

                $password = $activationCodeEntityExist->getActivationCode();
                $expiresAtTime = $activationCodeEntityExist->getExpiresAt();

            } else {
                $password = rand(1000, 9999);
                $expiresAtTime = time() + ActivationCode::EXPIRES_TIME;
            }

        } else {
            $password = rand(1000, 9999);
            $expiresAtTime = time() + ActivationCode::EXPIRES_TIME;
        }

        $isUserExist = $this->repository->checkOnExistUserByPhone($activationCodeEntity->getPhone());

        $activationCodeEntity->setActivationCode($password);
        $activationCodeEntity->setExpiresAt($expiresAtTime);
        $activationCodeEntity->setRefreshAt(time() + ActivationCode::REFRESH_TIME);

        $activationCodeEntity = $this->repository->createOrUpdateActivationCode($activationCodeEntity);

        if (!$activationCodeEntity) {
            throw new RuntimeException();
        }

        $activationCodeEntity->setIsUserExists($isUserExist ? 1 : 0);
        $activationCodeEntity->setRefreshExpiresIn(ActivationCode::REFRESH_TIME);

        $this->sendActivationCode($activationCodeEntity->getPhone(), $activationCodeEntity->getActivationCode());

        return $activationCodeEntity;
    }


    public function updatePasswordByPhone($phone, $password)
    {
        $this->repository->updatePasswordByPhone($phone, (Hash::make($password)));
    }

    /**
     * @param $phone
     * @param $activationCode
     */
    private function sendActivationCode($phone, $activationCode)
    {
        $smsPusher = SmsPusherFactory::make('FAKE');
        $smsPusher->send($phone, $activationCode);

        $smsPusher = SmsPusherFactory::make('WAVECELL');
        $smsPusher->send($phone, $activationCode);
    }

    /**
     * @param $status
     * @param string $redirectUrl
     * @return WebLoginResponse
     */
    public function createWebLoginResponse($status, $redirectUrl = '/dashboard')
    {
        $webLoginResponse = new WebLoginResponse();
        $webLoginResponse->setStatus($status);
        $webLoginResponse->setRedirectUrl($redirectUrl);
        return $webLoginResponse;
    }

    /**
     * @param string $redirectUrl
     * @return WebLogoutResponse
     */
    public function logout($redirectUrl = '/')
    {
        $authorizer = $this->container->get('oauth2-server.authorizer');
        $authorizer->getAccessToken()->expire();
        $webLogoutResponse = new WebLogoutResponse();
        $webLogoutResponse->setRedirectUrl($redirectUrl);
        return $webLogoutResponse;
    }


    /**
     * @param $options
     * @return mixed
     */
    public function getAll(UserListOptions $options)
    {
        return $this->repository->getAll($options);
    }

    /**
     * @param $options
     * @return mixed
     */
    public function getTotal(UserListOptions $options)
    {
        return $this->repository->getTotal($options);
    }

}