<?php

namespace App\Core\ClientManagement\Entity\Rights;
use Illuminate\Support\Collection;
use App\Libraries\Cast\Cast;

class Role
{
    /**
     *
     */
    const ROLE_MOBILE_USER = 'mobile_user';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $machineName;

    /**
     * @var
     */
    private $scopes;

    /**
     * @var
     */
    private $users;

    /**
     * @param int $id
     * @return string
     */
    public function setId($id)
    {
        $this->id = Cast::int($id);
        return $this;
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @param $machineName
     * @return $this
     */
    public function setMachineName($machineName)
    {
        $this->machineName = $machineName;
        return $this;
    }

    /**
     * @return string
     */
    public function getMachineName()
    {
        return $this->machineName;
    }

    /**
     * @param Collection $scopes
     * @return $this
     */
    public function setScopes(Collection $scopes)
    {
        $this->scopes = $scopes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param Collection $users
     * @return $this
     */
    public function setUsers(Collection $users)
    {
        $this->users = $users;
        return $this;
    }
}