<?php
namespace App\Core\ClientManagement\Entity\Support;


class WebLogoutResponse
{
    /**
     * @var
     */
    private $redirectUrl;

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param mixed $redirectUrl
     * @return WebLoginResponse
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }



}