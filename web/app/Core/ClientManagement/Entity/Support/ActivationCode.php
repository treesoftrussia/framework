<?php
namespace App\Core\ClientManagement\Entity\Support;


use App\Libraries\Cast\Cast;

class ActivationCode
{
    const EXPIRES_TIME = 3600;

    const REFRESH_TIME = 10;

    /**
     * @var
     */
    private $clientId;

    /**
     * @var
     */
    private $clientSecret;

    /**
     * @var
     */
    private $phone;

    /**
     * @var
     */
    private $activationCode;

    /**
     * @var
     */
    private $expiresAt;

    /**
     * @var
     */
    private $refreshAt;

    /**
     * @var
     */
    private $isUserExists;

    /**
     * @var
     */
    private $refreshExpiresIn;

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param $clientId
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @param $clientSecret
     * @return $this
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = preg_replace("/[^0-9]/","", Cast::string($phone));
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActivationCode()
    {
        return $this->activationCode;
    }

    /**
     * @param $activationCode
     * @return $this
     */
    public function setActivationCode($activationCode)
    {
        $this->activationCode = $activationCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @param $expiresAt
     * @return $this
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = Cast::int($expiresAt);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsUserExists()
    {
        return $this->isUserExists;
    }

    /**
     * @param $isUserExists
     * @return $this
     */
    public function setIsUserExists($isUserExists)
    {
        $this->isUserExists = Cast::int($isUserExists);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRefreshAt()
    {
        return $this->refreshAt;
    }

    /**
     * @param mixed $refreshAt
     * @return ActivationCode
     */
    public function setRefreshAt($refreshAt)
    {
        $this->refreshAt = $refreshAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRefreshExpiresIn()
    {
        return $this->refreshExpiresIn;
    }

    /**
     * @param mixed $refreshExpiresIn
     * @return ActivationCode
     */
    public function setRefreshExpiresIn($refreshExpiresIn)
    {
        $this->refreshExpiresIn = $refreshExpiresIn;
        return $this;
    }





}