<?php
namespace App\Core\ClientManagement\Entity\Support;

use App\Libraries\Cast\Cast;

class WebLoginResponse
{
    /**
     * @var
     */
    private $status;

    /**
     * @var
     */
    private $redirectUrl;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return WebLoginResponse
     */
    public function setStatus($status)
    {
        $this->status = Cast::int($status);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param mixed $redirectUrl
     * @return WebLoginResponse
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }



}