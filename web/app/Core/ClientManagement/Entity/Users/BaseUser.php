<?php

namespace App\Core\ClientManagement\Entity\Users;

use App\Core\ClientManagement\Interfaces\ScopableInterface;
use App\Core\Support\Behaviours\TimesTrait;

abstract class BaseUser implements ScopableInterface
{
    use TimesTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @param int $id
     * @return string
     */
    public function setId($id){
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }
}