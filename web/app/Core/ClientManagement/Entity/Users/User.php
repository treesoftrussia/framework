<?php

namespace App\Core\ClientManagement\Entity\Users;

use App\Core\ClientManagement\Traits\ScopableUserTrait;
use App\Core\Support\Entity\Image;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Collection;
use App\Libraries\Cast\Cast;


class User extends BaseUser
{
    use ScopableUserTrait;

    /**
     * @var
     */
    private $email;

    /**
     * @var
     */
    private $internalUsername;

    /**
     * @var
     */
    private $userName;

    /**
     * @var
     */
    private $phone;

    /**
     * @var
     */
    private $roles;

    /**
     * @var
     */
    private $passwordHash;

    /**
     * @var
     */
    private $password;

    /**
     * @var
     */
    private $sex;

    /**
     * @var
     */
    private $avatar;

    /**
     * @var
     */
    private $isActive;

    /**
     * @var
     */
    private $socialNetworkProfiles;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     * @return User
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param mixed $passwordHash
     * @return User
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInternalUsername()
    {
        return $this->internalUsername;
    }

    /**
     * @param mixed $internalUsername
     * @return User
     */
    public function setInternalUsername($internalUsername)
    {
        $this->internalUsername = $internalUsername;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param $sex
     * @return $this
     */
    public function setSex($sex)
    {
        $this->sex = Cast::int($sex);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param $avatar
     * @return $this
     */
    public function setAvatar(Image $avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSocialNetworkProfiles()
    {
        return $this->socialNetworkProfiles;
    }

    /**
     * @param Collection $socialNetworkProfiles
     * @return $this
     */
    public function setSocialNetworkProfiles(Collection $socialNetworkProfiles)
    {
        $this->socialNetworkProfiles = $socialNetworkProfiles;
        return $this;
    }
}