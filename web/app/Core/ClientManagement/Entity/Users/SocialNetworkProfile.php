<?php namespace App\Core\ClientManagement\Entity\Users;

use App\Core\ClientManagement\Entity\Support\Token;

class SocialNetworkProfile
{
    private $id;

    private $userId;

    private $internalId;

    private $type;

    private $tokenData;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return SocialNetworkProfile
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     * @return SocialNetworkProfile
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInternalId()
    {
        return $this->internalId;
    }

    /**
     * @param mixed $internalId
     * @return SocialNetworkProfile
     */
    public function setInternalId($internalId)
    {
        $this->internalId = $internalId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return SocialNetworkProfile
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Token $token
     */
    public function getTokenData()
    {
        return $this->tokenData;
    }

    /**
     * @param mixed $tokenData
     * @return SocialNetworkProfile
     */
    public function setTokenData(Token $tokenData)
    {
        $this->tokenData = $tokenData;
        return $this;
    }
}