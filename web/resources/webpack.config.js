var path = require('path');
var webpack = require('webpack');

//console.log(require.resolve("style-loader")+'|'+require.resolve("css-loader"));

module.exports = {
    //devtool: 'cheap-module-eval-source-map',
    entry: [
        './src/index'
    ],
    output: {
        path: path.join(__dirname, '../public/assets'),
        filename: 'bundle.js',
        publicPath: '/assets/'
    },
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        //new webpack.NoErrorsPlugin()
    ],
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel', // 'babel-loader' is also a legal name to reference
                query: {
                    presets: ['react']
                }

            },
            {test: /\.css$/, loaders: [require.resolve("style-loader"), require.resolve("css-loader")]},
            {test: /\.jpe?g$|\.gif$|\.png$/, loader: require.resolve("file-loader")},
            {test: /.(woff(2)?)(\?[a-z0-9=\.]+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
            {test: /.(eot)(\?[a-z0-9=\.]+)?$/, loader: 'file'},
            {test: /.(ttf)(\?[a-z0-9=\.]+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
            {test: /.(svg)(\?[a-z0-9=\.]+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'},
            {
                test: /\.sass$/,
                loaders: [require.resolve("style-loader"), require.resolve("css-loader"), require.resolve("sass-loader")]
            }
        ]
    },
    watchOptions: {
        poll: true
    }
}
