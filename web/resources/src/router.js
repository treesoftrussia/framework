var $ = require('jquery');
var routes = [
    {uri: /^\/?$/, controller: 'dashboard-controller'},
    {uri: /^\/dashboard\/?/, controller: 'dashboard-controller'},
    {uri: /^\/login\/?/, controller: 'login-controller'},
    {uri: /^\/products\/?/, controller: 'products-controller'},
    {uri: /^\/priority-lists\/?/, controller: 'priority-lists-controller'},
    {uri: /^\/sale-points\/?/, controller: 'sale-points-controller'},
    {uri: /^\/import\/?/, controller: 'import-controller'},
    {uri: /^\/retail-chains\/?/, controller: 'retail-chains-controller'},
    {uri: /^\/zones\/?/, controller: 'zones-controller'},
    {uri: /^\/users\/?/, controller: 'users-controller'},
    {uri: /^\/reviews\/?/, controller: 'reviews-controller'}
];

function startRouting() {
    var currentURI = window.location.pathname;
    
    $.each(routes, function(k, v) {
        if (v.uri.test(currentURI)) {
            loadController(v.controller, currentURI);
            return;
        }
    });
}

function loadController(controllerName, currentURI) {
    require(['./controllers/' + controllerName], function(Controller) {
        $(document).ready(function(){
            var controller = new Controller(currentURI[0] == '/' ? currentURI.substring(1, currentURI.length) : currentURI);
            controller.init();
        });
    });
}


module.exports.startRouting = startRouting;
