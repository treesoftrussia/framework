var DatatableModule = require('../modules/datatable');

module.exports = function(params){
    var that = this;

    this.all = function(){
        var datatableModule = new DatatableModule(params['all-table-id']);
    };

    this.add = function(){
    };

    this.edit = function(){
    };

    function route(currentRoute){
        var currentRouteParts = currentRoute.split('/');

        if(currentRouteParts[1] && currentRouteParts[1].length){
            switch(currentRouteParts[1]){
                case 'add':
                    that.add();
                    break;
                case 'edit':
                    that.edit(currentRouteParts[2]);
                    break;
            }
        }
        else if (currentRouteParts[0] && currentRouteParts[0].length){
            that.all();
        }
    }
    
    this.registerRouter(route);
};