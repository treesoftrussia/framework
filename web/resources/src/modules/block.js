$.fn.showLoader = function() {
    this.block({
        message: '',
        css: {
            border: 'none',
            padding: '0px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: .9,
            cursor: 'wait'
        }
    });
};

$.fn.hideLoader = function() {
    this.unblock();
};

module.exports = $;