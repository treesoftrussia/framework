var $ = require('jquery');
var loadGoogleMapsAPI = require('load-google-maps-api').default;

module.exports = function (selector, addressSelector, defaultCenter, mediator) {

    var that = this;
    this.mapsInstance = null;
    defaultCenter = defaultCenter || {
            lat: 55.33948, lng: 9.55373
        };

    addressSelector = addressSelector || '[name=address]';

    this.container = $(selector);
    this.inputs = {
        lng: $('[name=lng]', that.container),
        ltd: $('[name=ltd]', that.container)
    };
    this.map = null;
    this.marker = null;
    this.addressInput = $(addressSelector)[0];


    this.isInitialValuesSet = function(){
        return that.inputs.lng.val().length && that.inputs.ltd.val().length;
    };

    this.getCurrentCoordinates = function () {
        if (that.inputs.lng.val().length && that.inputs.ltd.val().length) {
            return {
                lat: parseFloat(that.inputs.ltd.val()),
                lng: parseFloat(that.inputs.lng.val())
            };
        } else {
            return defaultCenter;
        }
    };

    this.setCoordinates = function (coordinates) {
        that.inputs.lng.val(coordinates.lng);
        that.inputs.ltd.val(coordinates.lat);

        if (that.marker) {
            that.marker.setVisible(true);
            that.marker.setPosition(coordinates);
        }
    };

    this.resetCoordinates = function () {
        that.inputs.lng.val('');
        that.inputs.ltd.val('');

        if (that.marker) {
            that.marker.setVisible(false);
        }
    };

    this.getAddressInput = function () {
        return that.addressInput;
    };


    $(that.addressInput).on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });


    loadGoogleMapsAPI({
        key: 'AIzaSyDyXX_hQFlAwLw7duFDdkPQFDfgxsQpWH4',
        v: '3.23',
        libraries: ['places']
    }).then(function (maps) {
        that.mapsInstance = maps;
        that.map = new that.mapsInstance.Map($('.map-container', that.container)[0], {
            center: that.getCurrentCoordinates(),
            zoom: 13
        });

        that.createMarker(that.getCurrentCoordinates());

        if(that.isInitialValuesSet()){
            that.setCoordinates(that.getCurrentCoordinates());
        } else {
            that.map.setZoom(8)
        }

        var autocomplete = new that.mapsInstance.places.Autocomplete(that.getAddressInput());
        autocomplete.bindTo('bounds', that.map);

        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                that.resetCoordinates();
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                that.map.fitBounds(place.geometry.viewport);
            } else {
                that.map.setCenter(place.geometry.location);
                that.map.setZoom(17);  // Why 17? Because it looks good.
            }

            that.setCoordinates({
                lng:  parseFloat(place.geometry.location.lng().toFixed(5)),
                lat:  parseFloat(place.geometry.location.lat().toFixed(5))
            });
        });
    });


    that.createMarker = function (position) {
        var marker = new that.mapsInstance.Marker({
            map: that.map,
            anchorPoint: new that.mapsInstance.Point(0, -29),
            draggable: true
        });
        marker.setVisible(false);
        marker.addListener('dragend', function (event) {
            that.setCoordinates({
                lng:  parseFloat(event.latLng.lng().toFixed(5)),
                lat:  parseFloat(event.latLng.lat().toFixed(5))
            });
        });

        that.marker = marker;
        return marker;
    };

};
