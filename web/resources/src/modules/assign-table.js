var Mediator = require('./mediator');
var Select = require('./select');
var c = require('./const');

module.exports = function (containerSelector, id, mediator) {
    (new Mediator()).installTo(this);

    var that = this;
    this.container = $(containerSelector);

    new Select(that.firstSelectSelector, that.firstSource);
    new Select(that.secondSelectSelector, that.secondSource);

    this.serializeInputFields = function () {
        return $('.input-fields input', that.container).serializeArray();
    };

    this.clearForm = function () {
        $('.input-fields input', that.container).select2("val", "");
    };

    this.showEmpty = function () {
        $('.empty', that.container).show();
    };

    this.hideEmpty = function () {
        $('.empty', that.container).hide();
    };

    this.attachRow = function (v) {
        that.hideEmpty();
        var row = $('<tr \>')
            .append($('<td \>', {
                html: v[that.firstObjectName].name + ' <a href="' + that.firstPageSource + '/' + v[that.firstObjectName].id + '" target="_blank">[show]</a>',
                class: 'item'
            }))
            .append($('<td \>', {
                html: v[that.secondObjectName].name + ' <a href="' + that.secondPageSource + '/' + v[that.secondObjectName].id + '" target="_blank">[show]</a>',
                class: 'item'
            }))
            .append($('<td \>', {html: '<button class="btn btn-danger remove-button"><i class="entypo-trash"></i></button>'})
                .on('click', function () {
                    that.removeAssign(v.id, function () {
                        row.remove();
                        if ($('tbody tr', that.container).length == 1) {
                            that.showEmpty();
                        }
                    });
                })
            );
        $('tbody', that.container).append(row);
    };

    this.addAssign = function (scb) {
        var values = that.serializeInputFields();

        if (!values[0].value.length) {
            mediator.publish('showNotification', that.firstParamName + ' parameter is required.', null, false);
            return;
        }

        if (!values[1].value.length) {
            mediator.publish('showNotification', that.secondParamName + ' parameter is required.', null, false);
            return;
        }

        var data = {};
        data[that.firstParamIdName] = values[0].value;
        data[that.secondParamIdName] = values[1].value;

        $.ajax({
                method: "POST",
                url: that.assignmentSource + "/" + id + "/priority-list-assigns",
                contentType: "application/json",
                data: JSON.stringify(data)
            })
            .done(function (result) {
                mediator.publish('showNotification', 'Assign was created with success.');
                that.clearForm();
                that.attachRow(result);
            }).error(function (err) {
            mediator.publish('serverError', err.responseJSON);
        });
    };

    this.removeAssign = function (assignId, scb) {

        if (!confirm('Are you sure?')) {
            return;
        }

        $.ajax({
                method: "DELETE",
                url: that.assignmentSource + "/priority-list-assigns/" + assignId,
                contentType: "application/json"
            })
            .done(function (result) {
                scb();
                mediator.publish('showNotification', 'Removed with success');
            }).error(function (err) {

            if(err.responseJSON.errorCode == c.ROUTE_NOT_FOUND){
                scb();
            }

            mediator.publish('serverError', err.responseJSON);
        });
    };

    $('.assign-button', that.container).on('click', function () {
        that.addAssign();
    });


    $.ajax({
            method: "GET",
            url: that.assignmentSource + "/" + id + "/priority-list-assigns",
            contentType: "application/json"
        })
        .done(function (result) {
            if (result.length) {
                $.each(result, function (k, v) {
                    that.attachRow(v);
                });
            } else {
            }
        }).error(function (err) {

    });

};