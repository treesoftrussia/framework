var $ = require('jquery');
var loadGoogleMapsAPI = require('load-google-maps-api').default;

module.exports = function (selector, defaultCenter) {
    var that = this;
    this.mapsInstance = null;
    defaultCenter = defaultCenter || {
            lat: 55.33948, lng: 9.55373
        };
    this.container = $(selector);
    this.map = null;
    this.marker = null;

    this.getCurrentCoordinates = function () {
        return defaultCenter;
    };

    this.setMarkerCoordinates = function (coordinates) {
        if (that.marker) {
            that.marker.setVisible(true);
            that.marker.setPosition(coordinates);
        }
    };

    loadGoogleMapsAPI({
        key: 'AIzaSyDyXX_hQFlAwLw7duFDdkPQFDfgxsQpWH4',
        v: '3.23',
        libraries: ['places']
    }).then(function (maps) {
        that.mapsInstance = maps;
        that.map = new that.mapsInstance.Map($('.map-container', that.container)[0], {
            center: that.getCurrentCoordinates(),
            zoom: 13
        });

        that.createMarker(that.getCurrentCoordinates());
        that.setMarkerCoordinates(that.getCurrentCoordinates());
    });

    that.createMarker = function (position) {
        var marker = new that.mapsInstance.Marker({
            map: that.map,
            anchorPoint: new that.mapsInstance.Point(0, -29),
            draggable: false
        });
        marker.setVisible(false);
        that.marker = marker;
        return marker;
    };

};
