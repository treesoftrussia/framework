var Mediator = require('./mediator');

module.exports = function (selector, url, labelName) {
    (new Mediator()).installTo(this);

    var that = this;

    var options = {
    };

    labelName = labelName || 'name';

    console.log(labelName);

    if (url) {
        var ajax = {
            url: url,
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) { // page is the one-based page number tracked by Select2
                return {
                    "options[searchPhrase]": term, //search term
                    "options[pageNum]": page, // page number
                    "options[pageTotal]": 30 // page number
                };
            },
            results: function (data, page) {
                var results = [];
                $.each(data.items, function (index, item) {
                    results.push({
                        id: item.id,
                        text: item[labelName]
                    });
                });

                var more = (page * data.pagination.items_per_page_requested) < data.pagination.total_items; // whether or not there are more results available

                // notice we return the value of more so Select2 knows if more results can be loaded
                return {results: results, more: more};
            }
        };

        options.ajax = ajax;
        options.initSelection = function (element, callback) {
            var elementText = $(element).attr('data-init-text');
            callback({"text": elementText, "id": elementText});
        };
        options.minimumInputLength = 0;
        options.cache = true;
    } else {
        options.minimumResultsForSearch = -1;
    }

    var select = $(selector).select2(options).on('change', function(e){
        that.publish('change', e.val);
    });

    this.val = function(){
        return select.select2('val');
    };
};