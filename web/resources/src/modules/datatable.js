var $ = require('jquery');
require('datatables.net');
require('datatables/media/css/jquery.dataTables.css');

module.exports = function (sourceTableId) {
    var table = $('#' + sourceTableId)

    var datatable = table.DataTable({
        "columns": [
            null,
            null,
            {
                "orderable": false,
            },
            {
                "orderable": false,
            }
        ],
        "serverSide": true,
        "ajax": function (data, callback) {
            console.log(data);


            var fields = {
                0: 'id',
                1: 'name',
            };
            var pageInfo = table.DataTable().page.info();

            console.log(pageInfo);

            var orderBy = {
                field: fields[data.order[0].column],
                direction: data.order[0].dir
            };

            var orderbyQuery = orderBy.field ? "&options[field]=" + orderBy.field + "&options[direction]=" + orderBy.direction : "";
            var searchQuery = data.search.value.length ? "&options[searchPhrase]="+data.search.value : '';

            $.ajax({
                    method: "GET",
                    url: "/api/v1/retail-chains?options[perPage]=" + pageInfo.length + "&options[pageNum]=" + (pageInfo.page + 1) + orderbyQuery + searchQuery,
                    contentType: "application/json"
                })
                .done(function (result) {
                    callback({
                        data: result.items.map(function (v) {
                            return [
                                v.id,
                                v.name,
                                v.logo ? '<img src="' + v.logo.thumbnailUrl + '" class="img-thumbnail" style="width: 100px; height: 100px;">' : '',
                                '<a href="/retail-chains/edit/' + v.id + '" class="btn btn-xs btn-black"><i class="entypo-eye"></i> edit</a>'
                            ];
                        }),
                        recordsTotal: result.pagination.total_items,
                        recordsFiltered: result.pagination.total_items
                    });
                }).error(function (err) {
            });
        },
        "initComplete": function(){
            $('select').select2({
                minimumResultsForSearch: -1
            });
        }
    });

};
