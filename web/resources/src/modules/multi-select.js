var Mediator = require('./mediator');

module.exports = function (selector, url, labelName) {
    (new Mediator()).installTo(this);

    var that = this;

    labelName = labelName || 'name';

    var select = $(selector).select2({
        minimumInputLength: 0,
        multiple:true,
        allowClear: true,
        closeOnSelect: false,
        ajax: {
            url: url,
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) { // page is the one-based page number tracked by Select2
                return {
                    "options[searchPhrase]": term, //search term
                    "options[pageNum]": page, // page number
                    "options[pageTotal]": 30 // page number
                };
            },
            results: function (data, page) {
                var results = [];
                $.each(data.items, function(index, item){
                    results.push({
                        id: item.id,
                        text: item[labelName]
                    });
                });

                var more = (page * data.pagination.items_per_page_requested) < data.pagination.total_items; // whether or not there are more results available

                // notice we return the value of more so Select2 knows if more results can be loaded
                return { results: results, more: more };
            }
        },
        cache: true,
        initSelection : function (element, callback) {
            var elementText = $(element).attr('data-init-text');
            callback({"text":elementText,"id":elementText});
        }
    }).on('select2-selecting', function(val, choice){
    }).on('change', function(val){
        that.publish('change', val);
    });

    this.val = function(){
        return select.select2('val');
    };
};