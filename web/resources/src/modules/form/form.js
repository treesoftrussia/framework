var toastr = require('../../lib/js/toastr');

require('babel-polyfill');
var parsleyjs = require('parsleyjs');
require('parsleyjs/src/parsley.css');
var c = require('../const');

module.exports = function (id, selector, resourceWebURL, resourceAPIURL, mode, mediator) {
    var that = this;

    this.form = $(selector);
    this.additionalFields = {};
    this.handleErrorCallback = null;

    this.form.parsley().on('field:validated', function () {
    }).on('form:submit', function (e) {
        that.postData();
        return false;
    });

    $('.reset-form', this.form).on('click', function (e) {
        e.preventDefault();
        if (confirm(that.form.data('reset-confirm'))) {
            that.resetForm();
        }
    });


    $.fn.serializeDisabled = function () {
        var obj = {};

        $.each(that.additionalFields, function (k, v) {
            obj[k] = v;
        });

        $(':disabled[name]', this).each(function () {
            obj[this.name] = $(this).val();
        });
        $(':enabled[name]', this).each(function () {
            obj[this.name] = $(this).val();
        });
        return obj;
    };

    this.disableButtons = function () {
        $('.form-buttons button', that.form).attr('disabled', true);
    };

    this.enableButtons = function () {
        $('.form-buttons button', that.form).attr('disabled', false);
    };

    this.postData = function () {
        that.disableButtons();
        var data = that.form.serializeDisabled();
        $.ajax({
                method: mode == 'create' ? "POST" : "PUT",
                url: resourceAPIURL + (mode == 'create' ? '' : '/' + id),
                data: JSON.stringify(data),
                contentType: "application/json"
            })
            .done(function (entity) {
                if (mode == 'create') {
                    mediator.publish('showNotification', that.form.data('create-alert'));

                    setTimeout(function () {
                        location.href = resourceWebURL + '/edit/' + entity.id;
                    }, 3000);
                } else {
                    mediator.publish('showNotification', that.form.data('update-alert'));

                    that.enableButtons();
                }
            }).error(function (err) {
            that.handleFormError(err);
            that.enableButtons();
        });
    };

    this.setHandleErrorCallback = function(cb){
        that.handleErrorCallback = cb;
    };

    this.handleFormError = function (err) {
        var body = err.responseJSON;

        that.handleErrorCallback ? that.handleErrorCallback(body) : null;

        switch (body.errorCode) {
            case c.VALIDATION_ERROR:
                //validation error
                $.each(body.errorData, function (k, v) {
                    var field = $('[name="' + k + '"]', that.form).parsley();
                    window.ParsleyUI.removeError(field, k);
                    window.ParsleyUI.addError(field, k, v);
                    field.on('field:error', function () {
                        window.ParsleyUI.removeError(field, k);
                    });
                });
                break;
            default:
                mediator.publish('serverError', body, null, false);
                break;
        }
    };

    this.resetForm = function () {
        $(':enabled[name]', that.form).each(function () {
            $(this).val('');
        });
    };

    this.addField = function (id, value) {
        that.additionalFields[id] = value;
    };
};