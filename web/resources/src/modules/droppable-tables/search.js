var Mediator = require('../mediator');
require('../../lib/js/jquery.debounce.js');

module.exports = function(container){
    var that = this;
    (new Mediator()).installTo(this);

    console.log(this);

    var currentValue = null;
    $('.search-phrase-input', container).on('keyup', $.debounce(function () {
        currentValue = this.value;
        that.publish('refresh', currentValue);
    }, 200));

    this.getCurrentValue = function(){
        return currentValue;
    }
};