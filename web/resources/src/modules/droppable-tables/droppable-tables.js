var toastr = require('../../lib/js/toastr');
require('../../lib/js/jquery.multisortable');
var c = require('../const');

module.exports = function (selector, updateUrl, urlAll, urlSelected, createRowCallback, mediator) {
    var that = this;

    this.container = $(selector);
    this.allList = $($('.droppable-tables-group tbody')[0], that.container);
    this.selectedList = $($('.droppable-tables-group tbody')[1], that.container);
    this.page = 1;
    this.loadBlock = false;
    this.search = new (require('./search'))(that.container);
    this.previousResult = null;

    this.search.on('refresh', function (phrase) {
        that.resetPageNum();
        that.clearAllList();
        that.page.page = 1;
        that.loadAllDataByPage(that.page, phrase, function (isEnd) {
            that.loadBlock = isEnd;
        });
    });

    this.clearAllList = function () {
        $('tr:not(.empty-placeholder)', that.allList).remove();
    };

    this.resetPageNum = function () {
        that.page = 1;
    };

    // Sortable rows
    $('.droppable-tables-group tbody').multisortable({
        items: "tr:not(.empty-placeholder)",
        selectedClass: "selected",
        click: function (e) {
        },
        stop: function (e, ui) {
            var currentResult = that.serializeResultList();
            if (JSON.stringify(that.previousResult) != JSON.stringify(currentResult)) {
                console.log("I've been sorted.");
                that.previousResult = currentResult;
                that.updateServerData(currentResult);
            } else {
                console.log('not changed');
            }

        },
        remove: function (event, ui) {
            setTimeout(function () {
                var length = $('tr', event.target).length;
                if (length <= 1) {
                    $('.empty-placeholder', event.target).show();
                }
            }, 1);
        },
        update: function (event, ui) {
            $('.empty-placeholder', event.target).hide();
        },
    });

    this.allList.sortable({
        'connectWith': this.selectedList
    });
    this.selectedList.sortable({
        'connectWith': this.allList
    });


    this.updateServerData = function (data) {
        $.ajax({
                method: "PUT",
                url: updateUrl,
                contentType: "application/json",
                data: JSON.stringify(data)
            })
            .done(function (result) {
                that.deselectSelectedElements();
                mediator.publish('showNotification', 'Updated!');
            }).error(function (err) {
            that.deselectSelectedElements();

            if (err.responseJSON.errorCode == c.SALE_POINT_ATTACH_TO_ZONE_ERROR || err.responseJSON.errorCode == c.SALE_POINT_ATTACH_TO_RETAIL_CHAIN_ERROR) {
                that.moveFromSelectedToAllByIds(err.responseJSON.errorData.ids);
            }

            mediator.publish('serverError', err.responseJSON);
        });
    };


    this.moveFromSelectedToAllByIds = function (ids) {
        $.each(ids, function (k, v) {
            $('tr[data-id=' + v + ']').addClass('removed');
        });
        $.each(ids, function (k, v) {
            $('tr[data-id=' + v + ']').prependTo(that.allList);
            setTimeout(function () {
                $('tr[data-id=' + v + ']').removeClass('removed');
            }, 2000);
        });

        $('.empty-placeholder', that.allList).hide();

        var length = $('tr', that.selectedList).length;
        if (length <= 1) {
            $('.empty-placeholder', that.selectedList).show();
        }

        that.previousResult = that.serializeResultList();
    };

    this.deselectSelectedElements = function () {
        $('tr.selected:not(.empty-placeholder)', that.selectedList).each(function (k, v) {
            $(this).removeClass('selected');
        });

        $('tr.selected:not(.empty-placeholder)', that.allList).each(function (k, v) {
            $(this).removeClass('selected');
        });
    };


    this.getSelectedElements = function () {
        return $('tr.selected:not(.empty-placeholder)', that.selectedList);
    };

    this.serializeResultList = function () {
        var result = [];
        $('tr:not(.empty-placeholder)', that.selectedList).each(function (k, v) {
            result.push({id: $(this).data('id'), order: k});
        });
        return result;
    };


    this.createRow = createRowCallback || function (v) {
            return $('<tr />', {'data-id': v.id})
                .append($('<td />', {text: v.id}))
                .append($('<td />', {text: v.name}))
                .append($('<td />', {
                    html: '<a class="btn btn-xs btn-white" href="/sale-points/edit/' + v.id + '" target="_blank"><i class="entypo-eye"></i>show</a>',
                    class: 'nrw'
                }));
        };

    $.ajax({
            method: "GET",
            url: urlSelected,
            contentType: "application/json"
        })
        .done(function (result) {
            if (result.items.length) {
                $('.empty-placeholder', that.selectedList).hide();
                $.each(result.items, function (k, v) {
                    that.selectedList.append(that.createRow(v));
                });
                that.previousResult = that.serializeResultList();
            } else {
                $('.empty-placeholder', that.selectedList).show();
            }

            that.previousResult = that.serializeResultList();
        })
        .error(function (err) {
            mediator.publish('serverError', err.responseJSON);
        });

    this.loadAllDataByPage = function (pageNum, search, cb) {
        $.ajax({
                method: "GET",
                url: urlAll(pageNum, search),
                contentType: "application/json"
            })
            .done(function (result) {
                if (result.items.length) {
                    $('.empty-placeholder', that.allList).hide();
                    $.each(result.items, function (k, v) {
                        console.log(v);
                        that.allList.append(that.createRow(v));
                    });
                } else if (that.page == 1) {
                    $('.empty-placeholder', that.allList).show();
                }
                if (cb) cb(result.items.length ? false : true);
            }).error(function (err) {
            mediator.publish('serverError', err.responseJSON);
        });
    };

    that.loadAllDataByPage(that.page);

    $('.droppable-tables-group-wrapper').scroll(function () {
        if (($(this).scrollTop() + $(this).height()) > $('table', this).height() && !that.loadBlock) {
            that.loadBlock = true;
            that.page++;
            that.loadAllDataByPage(that.page, that.search.getCurrentValue(), function (isEnd) {
                that.loadBlock = isEnd;
            });
        }
    });


};