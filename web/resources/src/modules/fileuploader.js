var Mediator = require('./mediator');

module.exports = function(containerSelector, uploadInputSelector, url, name, onChange){
    (new Mediator()).installTo(this);

    var that = this;
    this.container = $(containerSelector);

    this.error = null;

    if(onChange || onChange === undefined){
        $(that.container).on('change', uploadInputSelector, function () {
            that.send();
        });
    }

    this.send = function(){

        (function(){
            that.publish('start');
            if (this.files && this.files[0]) {
                var file = this.files[0];

                var reader = new FileReader();
                //Пилюля для слабоумия IE
                reader.onload = function (e) {
                    var error = e.target.error;
                    if (error != null) {
                        switch (error.code) {
                            case error.ENCODING_ERR:
                                error.message = 'Ошибка парсинга кодировки файла.';
                                break;

                            case error.NOT_FOUND_ERR:
                                error.message = 'Файл не найден.';
                                break;

                            case error.NOT_READABLE_ERR:
                                error.message = 'Файл не может быть прочитан.';
                                break;

                            default:
                                error.message = 'Не изестная ошибка файла.';
                        }
                        that.publish('error', error);
                    } else {
                        var binary = "";
                        var bytes = new Uint8Array(reader.result);
                        var length = bytes.byteLength;
                        for (var i = 0; i < length; i++) {
                            binary += String.fromCharCode(bytes[i]);
                        }
                        that.upload(binary, file);
                    }
                };
                reader.readAsArrayBuffer(file);
            } else {
                that.publish('finish');
            }
        }).call($(uploadInputSelector, that.container)[0]);

    };

    this.upload = function(data){

        that.publish('beforeUpload');

        var xhr = new XMLHttpRequest();

        xhr.upload.addEventListener("progress", function (event) {
            var percentage = (event.loaded * 100) / event.total;
            that.publish('progress', Math.floor(percentage));
        }, false);

        xhr.onreadystatechange = function () {

            if (this.readyState == 4) {
                if(this.status == 200) {
                    that.publish('success', JSON.parse(this.response));
                } else {
                    that.publish('error', {message: JSON.parse(this.responseText)['errorData'], error: JSON.parse(this.responseText)});
                }
                that.publish('afterUpload');
                that.publish('finish');
            }
        };

        xhr.open("POST", url);

        // Составляем заголовки и тело запроса к серверу,  в котором и отправим файл.
        var boundary = "xxxxxxxxx";
        // Устанавливаем заголовки.
        xhr.setRequestHeader('Content-type', 'multipart/form-data; boundary="' + boundary + '"');
        xhr.setRequestHeader('Cache-Control', 'no-cache');

        // Формируем тело запроса.
        var body = "--" + boundary + "\r\n";
        body += "Content-Disposition: form-data; name='"+name+"'; filename='"+name+"'\r\n";
        body += "Content-Type: application/octet-stream\r\n\r\n";
        body += data + "\r\n";
        body += "--" + boundary + "--";

        // Пилюля от слабоумия для Chrome, который гад портит файлы в процессе загрузки.
        if (!XMLHttpRequest.prototype.sendAsBinary) {
            XMLHttpRequest.prototype.sendAsBinary = function(datastr) {
                function byteValue(x) {
                    return x.charCodeAt(0) & 0xff;
                }
                var ords = Array.prototype.map.call(datastr, byteValue);
                var ui8a = new Uint8Array(ords);
                this.send(ui8a.buffer);
            }
        }

        if(xhr.sendAsBinary) {
            xhr.sendAsBinary(body);
        } else {
            xhr.send(body);
        }
    };
}