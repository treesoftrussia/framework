var toastr = require('../lib/js/toastr');
var c = require('./const');

module.exports = function(mediator){

    var opts = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-left",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "10000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    mediator.on('showNotification', function(message, title, success){

        if(Array.isArray(message)){
            message = message.join('<br>');
        }
        
        if(success == undefined || success){
            toastr.success(message, title, opts);
        } else {
            toastr.warning(message, title, opts);
        }
    });

    mediator.on('showNoticeNotification', function(message, title) {
        toastr.info(message, title, opts);
    });

    mediator.on('serverError', function(error){
        if(!error.errorData){
            toastr.warning('We had received nothing from the server (no errorData). So something going wrong.', null, opts);
            return;
        }

        if(error.errorCode == c.PERMISSION_DENIED){
            toastr.info('Session was expired. You will redirected to the login page in 5 seconds.', null, opts);
            setTimeout(function(){
                location.href='/login';
            }, 5000);
            return;
        } else if(error.errorCode == c.ROUTE_NOT_FOUND){
            toastr.info('Requested item was already removed by someone else.', null, opts);
            return;
        }

        if((typeof error.errorData) == 'object'){
            //validation error, do nothing
            toastr.warning(error.errorData.file.join('<br>'), null, opts);
        } else if (error.errorData.message) {
            toastr.warning(error.errorData.message, null, opts);
        } else if(error.errorData){
            toastr.warning(error.errorData, null, opts);
        }
    });

};