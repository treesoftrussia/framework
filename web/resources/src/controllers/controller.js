module.exports = function(currentRoute){
    this.mediator = require('../modules/mediator')();
    var routeRegisters = [];
    this.registerRouter = function(routeRegisterCallback){
        routeRegisters.push(routeRegisterCallback);
    };

    this.route = function(){
        routeRegisters.forEach(function(routeRegisterCallback){
            routeRegisterCallback(currentRoute);
        });
    }
};