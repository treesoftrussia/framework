<?php

return [
    'activation_code_to_many_requests'    => 'You can request for the code once in :activation_code_refresh_time seconds.',
    'phone_already_has_been_taken'        => 'User with this phone already exist',
    'email_already_has_been_taken'        => 'User with this email already exist',
    'activation_code_is_invalid'          => 'Activation code is invalid',
    'latitude_validation'                 => 'The latitude field must follow the format of geographical latitude',
    'longitude_validation'                => 'The longitude field must follow the format of geographical longitude',
    'int_array_validation'                => 'The :attribute field must be an array consisting of the numbers',
    'product_ids_validation'              => 'Products with these Ids not found in the database',
    'photo_for_review_not_found'          => 'Photo for review not found',
    'coordinates_ltd_required_validation' => 'The latitude field is required',
    'coordinates_lng_required_validation' => 'The longitude field is required'
];
