<?php
return [
    'SALE_POINT_ATTACH_TO_RETAIL_CHAIN_ERROR' => 'Cannot link a sale points <b>:ids</b> to the retail chain for the next reasons: <ul>:message</ul>',
    'SALE_POINT_ATTACH_TO_ZONE_ERROR' => 'Cannot link a sale points <b>:ids</b> to the zone for the next reasons: <ul>:message</ul>',

    'SALE_POINT_SINGLE_ATTACH_TO_ZONE_AND_RETAIL_CHAIN_ERROR' => 'Cannot link a sale point to the retail chain and zone: there is no priority list for retail chain <b>:retail_chain_name</b>  and zone <b>:zone_name</b>. Please link a list first using the Zones or Retail Chains form.',
    'SALE_POINT_SINGLE_ATTACH_TO_RETAIL_CHAIN_ERROR' => 'Cannot link a sale point to the retail chain: there is no one priority list for retail chain <b>:retail_chain_name</b>. Please link a list first using the Zones or Retail Chains form.',
    'SALE_POINT_SINGLE_ATTACH_TO_ZONE_ERROR' => 'Cannot link a sale point to the zone: there is no one priority list for zone <b>:zone_name</b>. Please link a list first using the Zones or Retail Chains form.',

    'PRIORITY_LIST_DETACH_TO_ZONE_ERROR' => 'Cannot remove link: a sale points <b>:ids</b> for the retail chain <b>:retail_chain_name</b>  and priority list <b>:priority_list_name</b> exist. Please reassign that sale points to another chain or priority list first.',
    'PRIORITY_LIST_DETACH_TO_RETAIL_CHAIN_ERROR' => 'Cannot remove link: a sale points <b>:ids</b> for the zone <b>:zone_name</b> and priority list <b>:priority_list_name</b> exist. Please reassign that sale points to another zone or priority list first.',
    'PRIORITY_LIST_ATTACH_TO_ZONE_ERROR' => 'Assignment for selected retail chain already exists',
    'PRIORITY_LIST_ATTACH_TO_RETAIL_CHAIN_ERROR' => 'Assignment for selected zone already exists',
    'SALE_POINT_ATTACH_TO_RETAIL_CHAIN_ERROR_PART_WITH_ZONE' => '<li><b>:sale_point_name</b>: no priority list for retail chain <b>:retail_chain_name</b> and zone <b>:zone_name</b> found</li>',
    'SALE_POINT_ATTACH_TO_RETAIL_CHAIN_ERROR_PART_WITH_NO_ZONE' => '<li><b>:sale_point_name</b>: no one priority list for retail chain <b>:retail_chain_name</b> found</li>',

    'SALE_POINT_ATTACH_TO_ZONE_ERROR_PART_WITH_RETAIL_CHAIN' => '<li><b>:sale_point_name</b>: no priority list for zone <b>:zone_name</b> and retail chain <b>:retail_chain_name</b> found</li>',
    'SALE_POINT_ATTACH_TO_ZONE_ERROR_PART_WITH_NO_RETAIL_CHAIN' => '<li><b>:sale_point_name</b>: no one priority list for zone <b>:zone_name</b> found</li>'
];