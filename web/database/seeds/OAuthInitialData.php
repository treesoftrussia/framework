<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class OAuthInitialData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Model::unguard();

        //activation_codes
        DB::table('activation_codes')->insert([
            'phone' => config('params.auth_test_phone_number'),
            'activation_code' => '1234',
            'expires_at' => time() + 3600
        ]);

        //users
        DB::table('users')->insert([
            'email' => 'some@email.com',
            'first_name' => 'Bam',
            'last_name' => 'Margera',
            'username' => 'bam8',
            'phone' => config('params.auth_test_phone_number'),
            'password' => Hash::make('1234'),
            'created_at' => time()-100,
            'updated_at' => time()-100
        ]);
        DB::table('users')->insert([
            'email' => 'admin@email.com',
            'first_name' => 'Jack',
            'last_name' => 'Johnson',
            'username' => 'jacksjo',
            'phone' => '880055523535',
            'password' => Hash::make('admin'),
            'created_at' => time()-100,
            'updated_at' => time()-100
        ]);

        
        DB::table('oauth_clients')->insert([
            'id' => config('params.mobile_client_id'), 'secret' => config('params.mobile_client_secret'), 'name' => 'mobileClient'
        ]);
        DB::table('oauth_clients')->insert([
            'id' => config('params.web_client_id'), 'secret' => config('params.web_client_secret'), 'name' => 'webClient'
        ]);


        //oauth_grants
        DB::table('oauth_grants')->insert([
            'id' => 'client_credentials'
        ]);
        DB::table('oauth_grants')->insert([
            'id' => 'password'
        ]);
        DB::table('oauth_grants')->insert([
            'id' => 'activation_code'
        ]);


        DB::table('oauth_client_grants')->insert([
            'client_id' => config('params.mobile_client_id'), 'grant_id' => 'activation_code'
        ]);
        DB::table('oauth_client_grants')->insert([
            'client_id' => config('params.web_client_id'), 'grant_id' => 'password'
        ]);


        //oauth_scopes
        DB::table('oauth_scopes')->insert([
            'id' => 'test_Get', 'description' => 'Access for get resources.'
        ]);
        DB::table('oauth_scopes')->insert([
            'id' => 'test_Store', 'description' => 'Access for store resources.'
        ]);
        DB::table('oauth_scopes')->insert([
            'id' => 'test_Update', 'description' => 'Access for update resources.'
        ]);
        DB::table('oauth_scopes')->insert([
            'id' => 'test_Delete', 'description' => 'Access for delete resources.'
        ]);

        DB::table('oauth_scopes')->insert([
            'id' => 'integration-api-access', 'description' => 'Integration APi basic access.'
        ]);
        DB::table('oauth_scopes')->insert([
            'id' => 'mobile-api-access', 'description' => 'Access for mobile api endpoints.'
        ]);
        DB::table('oauth_scopes')->insert([
            'id' => 'web-api-access', 'description' => 'Access for web api endpoints.'
        ]);


        //oauth_grant_scopes
        DB::table('oauth_grant_scopes')->insert([
            'grant_id' => 'activation_code', 'scope_id' => 'test_Get'
        ]);
        DB::table('oauth_grant_scopes')->insert([
            'grant_id' => 'activation_code', 'scope_id' => 'test_Store'
        ]);
        DB::table('oauth_grant_scopes')->insert([
            'grant_id' => 'activation_code', 'scope_id' => 'test_Update'
        ]);
        DB::table('oauth_grant_scopes')->insert([
            'grant_id' => 'activation_code', 'scope_id' => 'test_Delete'
        ]);


        DB::table('oauth_grant_scopes')->insert([
            'grant_id' => 'activation_code', 'scope_id' => 'mobile-api-access'
        ]);
        DB::table('oauth_grant_scopes')->insert([
            'grant_id' => 'password', 'scope_id' => 'web-api-access'
        ]);
        DB::table('oauth_grant_scopes')->insert([
            'grant_id' => 'client_credentials', 'scope_id' => 'integration-api-access'
        ]);


        //MOBILE
        DB::table('oauth_client_scopes')->insert([
            'client_id' => config('params.mobile_client_id'), 'scope_id' => 'test_Get'
        ]);
        DB::table('oauth_client_scopes')->insert([
            'client_id' => config('params.mobile_client_id'), 'scope_id' => 'test_Store'
        ]);
        DB::table('oauth_client_scopes')->insert([
            'client_id' => config('params.mobile_client_id'), 'scope_id' => 'test_Update'
        ]);
        DB::table('oauth_client_scopes')->insert([
            'client_id' => config('params.mobile_client_id'), 'scope_id' => 'test_Delete'
        ]);


        //WEB
        DB::table('oauth_client_scopes')->insert([
            'client_id' => config('params.mobile_client_id'), 'scope_id' => 'mobile-api-access'
        ]);
        DB::table('oauth_client_scopes')->insert([
            'client_id' => config('params.web_client_id'), 'scope_id' => 'web-api-access'
        ]);
        
        //roles
        DB::table('roles')->insert([
            'machine_name' => 'mobile_user'
        ]);
        DB::table('roles')->insert([
            'machine_name' => 'admin'
        ]);


        //user_roles
        DB::table('user_roles')->insert([
            'user_id' => '1', 'role_id' => 1
        ]);
        DB::table('user_roles')->insert([
            'user_id' => '2', 'role_id' => 2
        ]);


        //role_oauth_scopes
        DB::table('role_oauth_scopes')->insert([
            'role_id' => '1', 'oauth_scope_id' => 'test_Get'
        ]);
        DB::table('role_oauth_scopes')->insert([
            'role_id' => '1', 'oauth_scope_id' => 'test_Store'
        ]);
        DB::table('role_oauth_scopes')->insert([
            'role_id' => '1', 'oauth_scope_id' => 'test_Update'
        ]);
        DB::table('role_oauth_scopes')->insert([
            'role_id' => '1', 'oauth_scope_id' => 'test_Delete'
        ]);
        DB::table('role_oauth_scopes')->insert([
            'role_id' => '1', 'oauth_scope_id' => 'mobile-api-access'
        ]);
        DB::table('role_oauth_scopes')->insert([
            'role_id' => '2', 'oauth_scope_id' => 'web-api-access'
        ]);


        Model::reguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
