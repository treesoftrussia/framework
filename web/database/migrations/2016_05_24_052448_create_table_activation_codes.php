<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableActivationCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activation_codes', function (Blueprint $table) {
            $table->string('phone')->unique();
            $table->string('activation_code')->unique();
            $table->unsignedInteger('refresh_at')->useCurrent();
            $table->unsignedInteger('expires_at')->useCurrent();

            $table->primary(['phone']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activation_codes');
    }
}
