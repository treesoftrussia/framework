<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->nullable()->unique();
            $table->string('phone')->unique()->nullable();
            $table->string('username')->unique()->unique();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('password');
            $table->unsignedInteger('created_at')->useCurrent();
            $table->unsignedInteger('updated_at')->useCurrent();
        });

        Schema::table('oauth_sessions', function (Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('no action');
        });

        Schema::table('user_roles', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('no action');
        });

        Schema::table('user_social_networks_data', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oauth_sessions', function (Blueprint $table) {
            $table->dropForeign(['owner_id']);
        });

        Schema::table('user_roles', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::table('user_social_networks_data', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::drop('users');
    }
}