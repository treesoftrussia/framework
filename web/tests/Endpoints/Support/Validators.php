<?php
namespace App\Tests\Endpoints\Support;

use App\Http\Support\Validator\Validators as HttpValidators;
use App\QA\Endpoints\Validator\DataCollectorTrait;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Validators extends HttpValidators
{
    use DataCollectorTrait;

    /**
     * Original validator makes remote requests to USPS service and we don't want that while testing
     *
     * @param string $attr
     * @param string$value
     * @param array $params
     * @return bool
     */
    public function validateZip($attr, $value, $params = [])
    {
        return true;
    }
} 