<?php
namespace App\Tests\Endpoints\Support;

use App\QA\Endpoints\AbstractApplicationFactory;
use Illuminate\Foundation\Application;
use App\QA\Endpoints\Entity\AbstractEntity;
use App\Tests\Endpoints\Support\Faker\FakerFactory;
use App\QA\Endpoints\Faker\FakerFactoryInterface;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ApplicationFactory extends AbstractApplicationFactory
{
    /**
     * @return Application
     */
    protected function getApplication()
    {
        return require __DIR__ . '/../../../bootstrap/app.php';
    }

    /**
     * @param string $type
     * @param callable $booted
     * @return Application
     */
    public function create($type = null, callable $booted = null)
    {
        $app = parent::create($type, $booted);

        if ($type === static::HTTP) {
            $app->booted(function ($app) {
                $app->make('validator')->resolver(function ($translator, $data, $rules, $messages) {
                    return new Validators($translator, $data, $rules, $messages);
                });
            });
        }

        if ($type === static::TEST) {
            $app->singleton(AbstractEntity::class, function() use($app) {
                return new Entity($app);
            });
            $app->singleton(FakerFactoryInterface::class, FakerFactory::class);
        }

        return $app;
    }
}