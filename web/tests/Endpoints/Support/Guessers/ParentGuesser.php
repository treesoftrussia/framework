<?php
namespace App\Tests\Endpoints\Support\Guessers;

use App\Libraries\Elegant\Collection;
use Faker\Generator;
use Illuminate\Database\Eloquent\Model;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ParentGuesser
{
    /**
     * @var Generator
     */
    private $faker;

    /**
     * @var Collection
     */
    private $models;

    /**
     * @var array
     */
    private $values;

    /**
     * @param Generator $faker
     * @param Collection $models
     * @param string $field
     */
    public function __construct(Generator $faker, Collection $models, $field = null)
    {
        $this->faker = $faker;
        $this->models = $models;

        if ($field === null) {
            $this->values = $this->models->modelKeys();
        } else {
            $this->values = [];

            /**
             * @var Model $model
             */
            foreach ($this->models as $model) {
                $this->values[] = $model->getAttribute($field);
            }
        }
    }

    /**
     * @return int
     */
    function __invoke()
    {
        return $this->faker->randomElement($this->values);
    }


}