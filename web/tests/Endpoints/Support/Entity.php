<?php
namespace App\Tests\Endpoints\Support;

use App\QA\Endpoints\Entity\AbstractEntity;
use App\Tests\Endpoints\Support\Generators\Integration\IntegrationGenerator;
use App\Tests\Endpoints\Support\Generators\TestGenerator;
use App\Tests\Endpoints\Support\Generators\GoodGenerator;
use App\Tests\Endpoints\Support\Generators\ParcelGenerator;
use App\Tests\Endpoints\Support\Generators\SalePointGenerator;
use App\Tests\Endpoints\Support\Generators\RetailChainGenerator;
use App\Tests\Endpoints\Support\Generators\ProductGenerator;
use App\Tests\Endpoints\Support\Generators\PriorityListGenerator;

class Entity extends AbstractEntity
{
    /**
     * @return array
     */
    protected function generators()
    {
        return [
            'salePoints' => SalePointGenerator::class,
            'retailChains' => RetailChainGenerator::class,
            'products' => ProductGenerator::class,
            'priorityLists' => PriorityListGenerator::class,
            'integrationData' => IntegrationGenerator::class,
            'testEntries' => TestGenerator::class
        ];
    }
}