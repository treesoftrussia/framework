<?php
namespace App\Tests\Endpoints\Support\Generators;

use App\DAL\Models\Test;
use App\Libraries\Elegant\Elegant;
use App\QA\Endpoints\Entity\AbstractGenerator;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestGenerator extends AbstractGenerator
{
    /**
     * @param int $times
     * @param Elegant $owner
     * @return TestModel[]
     */
    public function generate($times = 1)
    {
        return $this->entity->make(Test::class, $times, [
            'name' => $this->faker->name,
            'description' => $this->faker->text
            ]
        );
    }


}