<?php
namespace App\Tests\Endpoints\Support\Faker;

use App\QA\Endpoints\Faker\DefaultFakerFactory;
use App\Tests\Endpoints\Support\Faker\Providers\Tax;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class FakerFactory extends DefaultFakerFactory
{
    /**
     * Creates new instansce of Faker\Generator object.
     * @return Faker\Generator
     */
    public function create()
    {
        $faker = parent::create();
        $faker->addProvider(new Tax($faker));
        return $faker;
    }
}
