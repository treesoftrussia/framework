<?php namespace App\Tests\Endpoints\Support\Asserts\Test;

use App\Libraries\Cast\Cast;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class GoodEntityConstraint extends EntityConstraint
{

    /**
     * @param array $other
     * @return bool
     */
    protected function matches($other)
    {

        return
            $this->checkValue($this->data, 'id', $other['id'], $this->errors) &&
            $this->checkValue($this->data, 'auctionId', $other['auction_id'], $this->errors) &&
            $this->checkValue($this->data, 'parcelId', $other['parcel_id'], $this->errors) &&
            $this->checkValue($this->data, 'customerId', $other['customer_id'], $this->errors) &&
            $this->checkValue($this->data, 'name.ru', $other['name_ru'], $this->errors) &&
            $this->checkValue($this->data, 'name.en', $other['name_en'], $this->errors) &&
            $this->checkValue($this->data, 'name.ja', $other['name_ja'], $this->errors) &&
            $this->checkValue($this->data, 'price', $other['price'], $this->errors) &&
            $this->checkValue($this->data, 'quantity', $other['quantity'], $this->errors) &&
            $this->checkValue($this->data, 'dimension.weight', $other['weight'], $this->errors) &&
            $this->checkValue($this->data, 'dimension.length', $other['dim_l'], $this->errors) &&
            $this->checkValue($this->data, 'dimension.height', $other['dim_h'], $this->errors) &&
            $this->checkValue($this->data, 'dimension.width', $other['dim_w'], $this->errors) &&
            $this->checkValue($this->data, 'isDeleted', $other['is_deleted'], $this->errors) &&
            $this->checkValue($this->data, 'createdDate', Cast::dateString($other['created_date']), $this->errors) &&
            $this->checkValue($this->data, 'deliveredDate', Cast::dateString($other['delivered_date']), $this->errors) &&
            $this->checkValue($this->data, 'photoDate', Cast::dateString($other['photo_date']), $this->errors) &&
            $this->checkValue($this->data, 'testingSubject.ru', $other['testing_subject_ru'], $this->errors) &&
            $this->checkValue($this->data, 'testingSubject.en', $other['testing_subject_en'], $this->errors) &&
            $this->checkValue($this->data, 'testingSubject.ja', $other['testing_subject_ja'], $this->errors) &&
            $this->checkValue($this->data, 'testingResult.ru', $other['testing_result_ru'], $this->errors) &&
            $this->checkValue($this->data, 'testingResult.en', $other['testing_result_en'], $this->errors) &&
            $this->checkValue($this->data, 'testingResult.ja', $other['testing_result_ja'], $this->errors) &&
            $this->checkValue($this->data, 'shippingRequestedDate', Cast::dateString($other['shipping_requested_date']), $this->errors) &&
            $this->checkValue($this->data, 'shippingCode', $other['shipping_code'], $this->errors);
    }

}