<?php
namespace App\Tests\Endpoints\Support\Asserts\Test;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait CheckValueTrait
{
    /**
     * @var string[] $fields
     */
    protected $fields;

    /**
     * Check if two elements in 'actual' and 'expected' array are equals
     * @param array $expectedArray
     * @param string $expected
     * @param array $actualArray
     * @param string $actual
     * @param array $errors
     * @return bool
     */
    protected function checkValueInArray($expectedArray, $expected, $actualArray,
                                         $actual, &$errors)
    {
        if (!array_has($expectedArray, $expected)) {
            $errors[] = "expected:{$expected} key not exists";
        }

        if (!array_has($actualArray, $actual)) {
            $errors[] = "actual:{$actual} key not exists";
        }

        $lhs = array_get($expectedArray, $expected);
        $rhs = array_get($actualArray, $actual);

        if ($lhs != $rhs) {
            $lhsFormatted = $this->exporter->export($lhs);
            $rhsFormatted = $this->exporter->export($rhs);

            $errors[] = "expected[{$expected}] = {$lhsFormatted} isn't equal actual[{$actual}] = {$rhsFormatted}";
        }

        return count($errors) == 0;
    }

    /**
     * Check if elements in 'expected' array is equal $rhs
     * @param array $expectedArray
     * @param string $expected
     * @param mixed $rhs
     * @param array $errors
     * @return bool
     */
    protected function checkValue($expectedArray, $expected, $rhs, &$errors)
    {

        if ($this->isFieldExcluded($expected)) {
            return true;
        }

        if (!array_has($expectedArray, $expected)) {
            //return true;
            $errors[] = "expected:{$expected} key not exists";
        }

        $lhs = array_get($expectedArray, $expected);

        if ($lhs != $rhs) {
            $lhsFormatted = $this->exporter->export($lhs);
            $rhsFormatted = $this->exporter->export($rhs);

            $errors[] = "expected[{$expected}] = {$lhsFormatted} isn't equal actual value = {$rhsFormatted}";
        }

        return count($errors) == 0;
    }

    /**
     * @param string $field
     * @return bool
     */
    protected function isFieldExcluded($field)
    {
        return ($this->fields !== null) && !in_array($field, $this->fields);
    }
}