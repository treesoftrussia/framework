<?php namespace App\Tests\Endpoints\Support\Asserts\Test;

use PHPUnit_Framework_Constraint;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class EntityConstraint extends PHPUnit_Framework_Constraint
{
    use CheckValueTrait;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @param $data
     */
    public function __construct($data)
    {
        parent::__construct();

        if (!is_array($data)) {
            throw PHPUnit_Util_InvalidArgumentHelper::factory(1, 'array');
        }

        $this->data = $data;
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return $this->exporter->export($this->errors);
    }
}