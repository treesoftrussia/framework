<?php namespace App\Tests\Endpoints\Specifications;

use App\QA\Endpoints\Specification\AbstractSpecification;
use App\QA\Endpoints\Specification\ValueType;


class TokenResponseSpecification extends AbstractSpecification
{
    /**
     * @return array
     */
    public function specification()
    {
        return [
            'access_token' => ValueType::STRING,
            'token_type' => 'Bearer',
            'expires_in' => ValueType::INT,
        ];
    }
}