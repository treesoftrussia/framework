<?php namespace App\Tests\Endpoints\Specifications;

use App\QA\Endpoints\Specification\AbstractSpecification;
use App\QA\Endpoints\Specification\ValueType;

/**
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 */
class TestSpecification extends AbstractSpecification
{
    /**
     * @return array
     */
    public function specification()
    {
        return [
            'id' => ValueType::INT,
            'name' => ValueType::STRING,
            'description' => ValueType::STRING,
            'children:collection?' => TestChildSpecification::class,
        ];
    }
}