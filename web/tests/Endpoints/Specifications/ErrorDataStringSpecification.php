<?php namespace App\Tests\Endpoints\Specifications;

use App\QA\Endpoints\Specification\AbstractSpecification;
use App\QA\Endpoints\Specification\ValueType;


class ErrorDataStringSpecification extends AbstractSpecification
{
    /**
     * @return array
     */
    public function specification()
    {
        return [
            'errorData' => ValueType::STRING,
            'errorCode' => ValueType::INT
        ];
    }
}