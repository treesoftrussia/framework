<?php namespace App\Tests\Endpoints\Specifications;

use App\Libraries\Kangaroo\Error;
use App\QA\Endpoints\Specification\AbstractSpecification;
use App\QA\Endpoints\Specification\ValueType;


class ErrorValidationSpecification extends AbstractSpecification
{
    /**
     * @return array
     */
    public function specification()
    {
        return [
            'errorData' => ValueType::ARRAYTYPE,
            'errorCode' => ValueType::INT
        ];
    }
}