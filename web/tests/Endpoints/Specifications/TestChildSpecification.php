<?php namespace App\Tests\Endpoints\Specifications;

use App\QA\Endpoints\Specification\AbstractSpecification;
use App\QA\Endpoints\Specification\ValueType;

/**
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 */
class TestChildSpecification extends AbstractSpecification
{
    /**
     * @return array
     */
    public function specification()
    {
        return [
            'id' => ValueType::INT,
            'title' => ValueType::STRING,
            'parent_id?' => ValueType::INT,
        ];
    }
}