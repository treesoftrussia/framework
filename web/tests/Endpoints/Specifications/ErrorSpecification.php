<?php namespace App\Tests\Endpoints\Specifications;

use App\QA\Endpoints\Specification\AbstractSpecification;
use App\QA\Endpoints\Specification\ValueType;


class ErrorSpecification extends AbstractSpecification
{
    /**
     * @return array
     */
    public function specification()
    {
        return [
            'errorCode' => ValueType::INT
        ];
    }
}