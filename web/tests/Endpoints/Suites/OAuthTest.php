<?php namespace App\Tests\Endpoints\Suites;

use App\DAL\Models\Test;
use App\Tests\Endpoints\Specifications\ErrorSpecification;
use App\Tests\Endpoints\Specifications\TokenResponseSpecification;
use App\Tests\Endpoints\Support\Asserts\Test\TestEntityConstraint;
use Illuminate\Database\Eloquent;
use Tests\TestCase;
use Illuminate\Http\Response;
use App\Tests\Endpoints\Specifications\TestSpecification;

class OAuthTest extends TestCase
{
    /**
     * Successful response for receive token. Right credentials
     * GrantType : Password
     */
    public function testTest1x()
    {
        $tokenResponse = $this->json('POST', '/oauth/access_token', [
            'grant_type' => 'password',
            'client_id' => config('params.web_client_id'),
            'client_secret' => config('params.web_client_secret'),
            'login' => 'admin@email.com',
            'password' => 'admin',
        ]);

        $this->assertStructure($tokenResponse->decodeResponseJson(), new TokenResponseSpecification());

        $this->assertResponseOk();
    }


    /**
     * Failed response for receive token. Wrong credentials
     * GrantType : Password
     */
    public function testTest2()
    {
        $tokenResponse = $this->json('POST', '/oauth/access_token', [
            'grant_type' => 'password',
            'client_id' => config('params.web_client_id'),
            'client_secret' => config('params.web_client_secret'),
            'login' => 'some@email.com',
            'password' => '6666666',
        ]);

        $this->assertStructure($tokenResponse->decodeResponseJson(), new ErrorSpecification());

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }


    /**
     * Failed response for receive token. Wrong scopes
     * GrantType : Password
     */
    public function testTest3()
    {
        $tokenResponse = $this->json('POST', '/oauth/access_token', [
            'grant_type' => 'password',
            'client_id' => config('params.web_client_id'),
            'client_secret' => config('params.web_client_secret'),
            'login' => 'some@email.com',
            'password' => '123456789',
            'scope' => 'wrong-scope'
        ]);

        $this->assertStructure($tokenResponse->decodeResponseJson(), new ErrorSpecification());
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }


    /**
     * Failed response for receive token. Wrong client_id & client_secret
     * GrantType : Password
     */
    public function testTest4()
    {
        $tokenResponse = $this->json('POST', '/oauth/access_token', [
            'grant_type' => 'password',
            'client_id' => '222222222-x',
            'client_secret' => '987654321-x',
            'login' => 'some@email.com',
            'password' => '123456789',
        ]);

        $this->assertStructure($tokenResponse->decodeResponseJson(), new ErrorSpecification());
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }


    /**
     * Failed response for receive token. Wrong scopes for GrantType
     * GrantType : Password
     */
    public function testTest5()
    {
        $tokenResponse = $this->json('POST', '/oauth/access_token', [
            'grant_type' => 'password',
            'client_id' => config('params.web_client_id'),
            'client_secret' => config('params.web_client_secret'),
            'login' => 'some@email.com',
            'password' => '123456789',
            'scope' => 'test_FillData'
        ]);

        $this->assertStructure($tokenResponse->decodeResponseJson(), new ErrorSpecification());
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }


    /**
     * Successful response for receive Test object
     * Successful authorization
     * GrantType : Password
     * RequiredScopes : test_Get
     */
    public function testTest6()
    {
        $this->withAuth();

        $testEntries = $this->entity->testEntries(5);

        $response = $this->json('GET', '/oauthtest/'.$testEntries[0]->id);

        $this->assertResponseOk();
        $this->assertStructure($response->decodeResponseJson(), new TestSpecification());
    }

    /**
     * Successful response for create Test object
     * Successful authorization
     * GrantType : Password
     * RequiredScopes : test_Post
     */
    public function testTest7()
    {
        $this->withAuth();

        $requestData = [
            'name' => 'test nameooo',
            'description' => 'test description#1',
            'children' => [
                [
                    'title' => 'test child name!'
                ]
            ]
        ];

        $responseData = $this->json('POST', '/oauthtest', $requestData)->decodeResponseJson();

        $this->assertResponseStatus(Response::HTTP_CREATED);

        $this->assertStructure($responseData, new TestSpecification());
        $this->assertEntity($requestData, $responseData['id'], Test::class, TestEntityConstraint::class);
        $this->assertEntity($requestData, $responseData, Test::class, TestEntityConstraint::class);
    }

    /**
     * Successful response for update Test object
     * Successful authorization
     * GrantType : Password
     * RequiredScopes : test_Update
     */
    public function testTest8()
    {
        $this->withAuth();

        $testEntries = $this->entity->testEntries(5);

        $requestData = [
            'name' => 'test name#2',
            'description' => 'test description#2'
        ];

        $responseData = $this->json('PUT', '/oauthtest/'.$testEntries[0]->id, $requestData);

        $this->assertResponseOk();
        $this->assertStructure($responseData->decodeResponseJson(), new TestSpecification());
        $this->assertEntity($requestData, $testEntries[0]->id, Test::class, TestEntityConstraint::class);
    }

    /**
     * Successful response for update Test object (patch)
     * Successful authorization
     * GrantType : Password
     * RequiredScopes : test_Update
     */
    public function testTest9()
    {
        $this->withAuth();

        $testEntries = $this->entity->testEntries(5);

        $requestData = [
            'description' => 'test description updated'
        ];

        $responseData = $this->json('PATCH', '/oauthtest/'.$testEntries[0]->id, $requestData);

        $this->assertResponseOk();
        $this->assertStructure($responseData->decodeResponseJson(), new TestSpecification());
    }


    /**
     * Successful response for delete Test object
     * Successful authorization
     * GrantType : Password
     * RequiredScopes : test_Delete
     */
    public function testTest10()
    {
        $this->withAuth();

        $testEntries = $this->entity->testEntries(5);

        $this->json('DELETE', '/oauthtest/'.$testEntries[0]->id);

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);

        $this->json('GET', '/oauthtest/'.$testEntries[0]->id);

        $this->assertResponseStatus(Response::HTTP_NOT_FOUND);
    }

}
