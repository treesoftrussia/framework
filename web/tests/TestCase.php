<?php
namespace Tests;

use App\QA\Endpoints\TestCase as BaseTestCase;
use App\Tests\Endpoints\Specifications\TokenResponseSpecification;
use DatabaseSeeder;
use App\Core\ClientManagement\Entity\Users\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\ParameterBag;

class TestCase extends BaseTestCase
{
    public $baseUrl = 'http://oos.local';

    const INTEGRATION = 1;
    const MOBILE = 2;
    const WEB = 3;

    protected $foreign_key_checks = false;

    protected $authHeader;

    protected function beforeTest()
    {
        parent::beforeTest();

        $dbseeder = new DatabaseSeeder();
        $dbseeder->run();

    }

    /**
     * Call the given URI and return the Response.
     *
     * @param  string  $method
     * @param  string  $uri
     * @param  array   $parameters
     * @param  array   $cookies
     * @param  array   $files
     * @param  array   $server
     * @param  string  $content
     * @return \Illuminate\Http\Response
     */
    public function call($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null)
    {
        $kernel = $this->app->make('Illuminate\Contracts\Http\Kernel');

        $this->currentUri = $this->prepareUrlForRequest($uri);

        $this->resetPageContext();

        $request = Request::create(
            $this->currentUri, $method, $parameters,
            $cookies, $files, array_replace($this->serverVariables, $server), $content
        );

        if($this->authHeader){
            $request->headers->set('Authorization', $this->authHeader);
        }

        if($request->isJson()){
            $request->request = $request->json();
        }

        $response = $kernel->handle($request);

        $kernel->terminate($request, $response);

        return $this->response = $response;
    }

    protected function getGrantTypePasswordData(User $user){
        return [
            'grant_type' => 'password',
            'client_id' => config('params.web_client_id'),
            'client_secret' => config('params.web_client_secret'),
            'login' => $user->getEmail(),
            'password' => $user->getPassword(),
        ];
    }

    protected function getGrantTypeActivationCodeData(){
        return [
            'grant_type' => 'activation_code',
            'client_id' => config('params.mobile_client_id'),
            'client_secret' => config('params.mobile_client_secret'),
            'phone' => config('params.auth_test_phone_number'),
            'activation_code' => '1234',
        ];
    }


    /**
     * @param int $type
     * @param array $scopes
     * @return mixed
     */
    protected function withAuth($type = self::MOBILE, $scopes = [])
    {
        switch ($type) {
            case self::MOBILE:
                $requestData = $this->getGrantTypeActivationCodeData();
                break;
            case self::WEB:
                $requestData = $this->getGrantTypePasswordData();
                $this->json('POST', '/api/v1/login', $requestData);
                $cookies = $this->response->headers->getCookies();
                $accessToken = $cookies[0];

                $authHeader = [
                    'Authorization' => 'Bearer ' . ' ' . $accessToken->getValue()
                ];
                $this->presetHeaders = array_merge($this->presetHeaders, $authHeader);
                return $accessToken->getValue();
            default:
                $user = null;
        }

        $requestData['scope'] = implode(',', $scopes);

        $tokenResponse = $this->json('POST', '/oauth/access_token', $requestData)->decodeResponseJson();

        $this->assertStructure($tokenResponse, new TokenResponseSpecification());

        $this->authHeader = $tokenResponse['token_type'] . ' ' . $tokenResponse['access_token'];

        return $tokenResponse['access_token'];
    }


    /**
     * @deprecated
     */
    protected function authorize()
    {
        $this->withAuth(self::MOBILE);
    }

    public function getEntity(){
        return $this->entity;
    }

    public function getFaker(){
        return $this->faker;
    }

    public function assertArrayContainsArray($needle, $haystack)
    {
        foreach ($needle as $key => $val) {
            $this->assertArrayHasKey($key, $haystack);

            if (is_array($val)) {
                $this->assertArrayContainsArray($val, $haystack[$key]);
            } else {
                $this->assertEquals($val, $haystack[$key]);
            }
        }
    }

}
