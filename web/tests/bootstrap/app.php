<?php
use App\QA\Endpoints\Application;
use App\Tests\Endpoints\Support\ApplicationFactory;

require __DIR__ . '/../../bootstrap/autoload.php';

Application::setConfig([
    'suitesNamespace' => 'App\Tests\Endpoints\Suites',
    'suitesName' => 'endpoints'
]);

Application::setFactory(new ApplicationFactory());