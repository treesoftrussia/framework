<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */
return [
    App\QA\Endpoints\Specification\CustomTypes\PhoneCustomType::class => \App\QA\Support\FakeGenerators\Generators\PhoneFakeGenerator::class,
    App\QA\Endpoints\Specification\CustomTypes\UnixTimeCustomType::class => \App\QA\Support\FakeGenerators\Generators\UnixTimeFakeGenerator::class
];