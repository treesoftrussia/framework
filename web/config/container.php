<?php
return [
    'implementations' => [
        App\Core\Support\Container\ContainerInterface::class => App\Core\Support\Container\Container::class,
        App\Core\ClientManagement\Interfaces\UserRepositoryInterface::class => App\DAL\Repository\UserRepository::class,
        App\Core\ClientManagement\Interfaces\RoleRepositoryInterface::class => App\DAL\Repository\RoleRepository::class,
        App\Core\Test\Interfaces\TestRepositoryInterface::class => \App\DAL\Repository\TestRepository::class,
        App\Core\Test\Interfaces\TestChildRepositoryInterface::class => \App\DAL\Repository\TestChildRepository::class
    ]
];