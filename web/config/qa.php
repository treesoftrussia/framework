<?php
/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
return [
    'endpoints' => [
        'suitesNamespace' => 'App\Tests\Endpoints\Suites',
        'dbName' => 'dp_dev_test'
    ]
];
