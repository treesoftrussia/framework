#!/bin/bash

echo "Starting for deploy script for ${ENVIRONMENT_NAME}..."

echo "Copying .env file..."

cp ./env/.env.${ENVIRONMENT_NAME} ./.env

echo "Copied."

echo "Installing composer dependencies..."

composer install

echo "Sleeping for a 15 seconds..."
sleep 15

echo "Running migrations..."
php artisan migrate

echo "Starting env scripts execution..."

if [ ${ENVIRONMENT_NAME} == "dev" ]
then
	echo "DEV"
elif [ ${ENVIRONMENT_NAME} == "local" ]
then
	echo "LOCAL"
elif [ ${ENVIRONMENT_NAME} == "stage" ]
then
	echo "STAGE"
elif [ ${ENVIRONMENT_NAME} == "test" ]
then
	echo "TEST"
elif [ ${ENVIRONMENT_NAME} == "prod" ]
then
	echo "PROD"
fi

echo "Removing temporary files..."
rm -rf ./rsa

echo "DONE"

exec "$@"

#Emulate daemon
while true
    do
    sleep 5
done